//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトワーク
//========================================
using namespace opal;

//----------------------------------------
// construct & destructo
//----------------------------------------
OBJ::OBJ(){}
OBJ::~OBJ(){}

OBJ::FLAG::FLAG() :
	s(),
	v(),
	t(0)
{}

template<typename TYPE>OBJ::FLAG::ELEMENT<TYPE>::ELEMENT() :
	func( 0 )
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OBJ::Init( void ){

	state = OBJ_STATE_NULL;

	new ( &flag ) FLAG;

	flag.v->func 	= true;

	flag.s.func		= FLAG_SUPER_AND;
}

void OBJ::Free( void ){}

//----------------------------------------
// フラグ
//----------------------------------------
void OBJ::SetFunc( int f, UINT t ){

	FLAG_CTRL( flag.v->func, f );
	flag.t = t;
}
bool OBJ::GetFunc( void ) const { return flag.v->func ? true : false;	}
bool OBJ::GetFunc( UINT vIndex ) const {

	if ( vIndex < std::size( flag.v ) ) {
		return flag.v[vIndex].func ? true : false;
	}
	return false;
}

void OBJ::SetSuperFunc( int m ){ FLAG_CTRL( flag.s.func,  m );	}
void OBJ::SetSuperFunc( void ){ flag.s.func	= FLAG_SUPER_NULL;	}
int  OBJ::GetSuperFunc( void ) const { return flag.s.func;		}

//----------------------------------------
// ウエイト
//----------------------------------------
void OBJ::SetWait( UINT t ) { flag.t = t;			}
UINT OBJ::GetWait( void   ) const { return flag.t;	}

//----------------------------------------
// 状態更新
//----------------------------------------
void OBJ::Coordinate( const OBJ* p ) {

	SIGL::Coordinate( p );

	auto&	v0 = flag.v[0];
	auto&	v1 = flag.v[1];

	if ( TICK::push( flag.t ) ) {
		v1.func = false;
	} else if ( p == nullptr ) {
		v1.func = v0.func;
	} else {
		auto&	vp = p->flag.v[1];

		v1.func	= FLAG_COORDINATE( flag.s.func, v0.func, vp.func );
	}
}

// End Of File
