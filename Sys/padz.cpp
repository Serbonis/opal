//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// ���̓V�X�e��
//========================================
using namespace opal;

//----------------------------------------
// MOUSE�̖��O
//----------------------------------------
const char* PADX::MouseName( UINT vc ) {

	switch ( vc ) {
	case MOUSE_C0:return "L";
	case MOUSE_C1:return "R";
	case MOUSE_C2:return "C";
	case MOUSE_C3:return "3";
	case MOUSE_C4:return "4";
	case MOUSE_C5:return "5";
	case MOUSE_C6:return "6";
	case MOUSE_C7:return "7";
	}
	return nullptr;
}

//----------------------------------------
// JOYPAD�̖��O
//----------------------------------------
const char* PADX::JoyName( UINT vb ) {

	switch ( vb ) {
	case PAD_Y :return "��";
	case PAD_B :return "��";
	case PAD_A :return "�~";
	case PAD_X :return "��";
	case PAD_L2:return "L2";
	case PAD_R2:return "R2";
	case PAD_L1:return "L1";
	case PAD_R1:return "R1";
	case PAD_ST:return "ST";
	case PAD_SL:return "SL";
	case PAD_L3:return "L3";
	case PAD_R3:return "R3";
	case PAD_U :return "��";
	case PAD_R :return "�E";
	case PAD_D :return "��";
	case PAD_L :return "��";
	}

	return nullptr;
}

//----------------------------------------
// KEYBOARD�̖��O
//----------------------------------------
const char* PADX::KeyName( UINT vk ) {

	static const char*	keyname[] = {
		"",					// 0x00
		"DIK_ESCAPE",		// 0x01
		"DIK_1",			// 0x02
		"DIK_2",			// 0x03
		"DIK_3",			// 0x04
		"DIK_4",			// 0x05
		"DIK_5",			// 0x06
		"DIK_6",			// 0x07
		"DIK_7",			// 0x08
		"DIK_8",			// 0x09
		"DIK_9",			// 0x0A
		"DIK_0",			// 0x0B
		"DIK_MINUS",		// 0x0C - on main keyboard
		"DIK_EQUALS",		// 0x0D
		"DIK_BACK",			// 0x0E backspace
		"DIK_TAB",			// 0x0F
		"DIK_Q",			// 0x10
		"DIK_W",			// 0x11
		"DIK_E",			// 0x12
		"DIK_R",			// 0x13
		"DIK_T",			// 0x14
		"DIK_Y",			// 0x15
		"DIK_U",			// 0x16
		"DIK_I",			// 0x17
		"DIK_O",			// 0x18
		"DIK_P",			// 0x19
		"DIK_LBRACKET",		// 0x1A
		"DIK_RBRACKET",		// 0x1B
		"DIK_RETURN",		// 0x1C Enter on main keyboard
		"DIK_LCONTROL",		// 0x1D
		"DIK_A",			// 0x1E
		"DIK_S",			// 0x1F
		"DIK_D",			// 0x20
		"DIK_F",			// 0x21
		"DIK_G",			// 0x22
		"DIK_H",			// 0x23
		"DIK_J",			// 0x24
		"DIK_K",			// 0x25
		"DIK_L",			// 0x26
		"DIK_SEMICOLON",	// 0x27
		"DIK_APOSTROPHE",	// 0x28
		"DIK_GRAVE",		// 0x29 accent grave
		"DIK_LSHIFT",		// 0x2A
		"DIK_BACKSLASH",	// 0x2B
		"DIK_Z",			// 0x2C
		"DIK_X",			// 0x2D
		"DIK_C",			// 0x2E
		"DIK_V",			// 0x2F
		"DIK_B",			// 0x30
		"DIK_N",			// 0x31
		"DIK_M",			// 0x32
		"DIK_COMMA",		// 0x33
		"DIK_PERIOD",		// 0x34 . on main keyboard
		"DIK_SLASH",		// 0x35 / on main keyboard
		"DIK_RSHIFT",		// 0x36
		"DIK_MULTIPLY",		// 0x37 * on numeric keypad
		"DIK_LMENU",		// 0x38 left Alt
		"DIK_SPACE",		// 0x39
		"DIK_CAPITAL",		// 0x3A
		"DIK_F1",			// 0x3B
		"DIK_F2",			// 0x3C
		"DIK_F3",			// 0x3D
		"DIK_F4",			// 0x3E
		"DIK_F5",			// 0x3F
		"DIK_F6",			// 0x40
		"DIK_F7",			// 0x41
		"DIK_F8",			// 0x42
		"DIK_F9",			// 0x43
		"DIK_F10",			// 0x44
		"DIK_NUMLOCK",		// 0x45
		"DIK_SCROLL",		// 0x46 Scroll Lock
		"DIK_NUMPAD7",		// 0x47
		"DIK_NUMPAD8",		// 0x48
		"DIK_NUMPAD9",		// 0x49
		"DIK_SUBTRACT",		// 0x4A - on numeric keypad
		"DIK_NUMPAD4",		// 0x4B
		"DIK_NUMPAD5",		// 0x4C
		"DIK_NUMPAD6",		// 0x4D
		"DIK_ADD",			// 0x4E + on numeric keypad
		"DIK_NUMPAD1",		// 0x4F
		"DIK_NUMPAD2",		// 0x50
		"DIK_NUMPAD3",		// 0x51
		"DIK_NUMPAD0",		// 0x52
		"DIK_DECIMAL",		// 0x53 . on numeric keypad
		"",					// 0x54
		"",					// 0x55
		"DIK_OEM_102",		// 0x56 <> or \| on RT 102-key keyboard (Non-U.S.)
		"DIK_F11",			// 0x57
		"DIK_F12",			// 0x58
		"",					// 0x59
		"",					// 0x5A
		"",					// 0x5B
		"",					// 0x5C
		"",					// 0x5D
		"",					// 0x5E
		"",					// 0x5F
		"",					// 0x60
		"",					// 0x61
		"",					// 0x62
		"",					// 0x63
		"DIK_F13",			// 0x64 (NEC PC98)
		"DIK_F14",			// 0x65 (NEC PC98)
		"DIK_F15",			// 0x66 (NEC PC98)
		"",					// 0x67
		"",					// 0x68
		"",					// 0x69
		"",					// 0x6A
		"",					// 0x6B
		"",					// 0x6C
		"",					// 0x6D
		"",					// 0x6E
		"",					// 0x6F
		"DIK_KANA",			// 0x70 (Japanese keyboard)
		"",					// 0x71
		"",					// 0x72
		"DIK_ABNT_C1",		// 0x73 /? on Brazilian keyboard
		"",					// 0x74
		"",					// 0x75
		"",					// 0x76
		"",					// 0x77
		"",					// 0x78
		"DIK_CONVERT",		// 0x79 (Japanese keyboard)
		"",					// 0x7A
		"DIK_NOCONVERT",	// 0x7B (Japanese keyboard)
		"",					// 0x7C
		"DIK_YEN",			// 0x7D (Japanese keyboard)
		"DIK_ABNT_C2",		// 0x7E Numpad . on Brazilian keyboard
		"",					// 0x7F
		"",					// 0x80
		"",					// 0x81
		"",					// 0x82
		"",					// 0x83
		"",					// 0x84
		"",					// 0x85
		"",					// 0x86
		"",					// 0x87
		"",					// 0x88
		"",					// 0x89
		"",					// 0x8A
		"",					// 0x8B
		"",					// 0x8C
		"DIK_NUMPADEQUALS",	// 0x8D		= on numeric keypad (NEC PC98)
		"",					// 0x8E
		"",					// 0x8F
		"DIK_PREVTRACK",	// 0x90 Previous Track (DIK_CIRCUMFLEX on Japanese keyboard)
		"DIK_AT",			// 0x91 (NEC PC98)
		"DIK_COLON",		// 0x92 (NEC PC98)
		"DIK_UNDERLINE",	// 0x93 (NEC PC98)
		"DIK_KANJI",		// 0x94 (Japanese keyboard)
		"DIK_STOP",			// 0x95 (NEC PC98)
		"DIK_AX",			// 0x96 (Japan AX)
		"DIK_UNLABELED",	// 0x97 (J3100)
		"",					// 0x98
		"DIK_NEXTTRACK",	// 0x99 Next Track
		"",					// 0x9A
		"",					// 0x9B
		"DIK_NUMPADENTER",	// 0x9C Enter on numeric keypad
		"DIK_RCONTROL",		// 0x9D
		"",					// 0x9E
		"",					// 0x9F
		"DIK_MUTE",			// 0xA0 Mute
		"DIK_CALCULATOR",	// 0xA1 Calculator
		"DIK_PLAYPAUSE",	// 0xA2 Play / Pause
		"",					// 0xA3
		"DIK_MEDIASTOP",	// 0xA4 Media Stop
		"",					// 0xA5
		"",					// 0xA6
		"",					// 0xA7
		"",					// 0xA8
		"",					// 0xA9
		"",					// 0xAA
		"",					// 0xAB
		"",					// 0xAC
		"",					// 0xAD
		"DIK_VOLUMEDOWN",	// 0xAE Volume -
		"",					// 0xAF
		"DIK_VOLUMEUP",		// 0xB0 Volume +
		"",					// 0xB1
		"DIK_WEBHOME",		// 0xB2 Web home
		"DIK_NUMPADCOMMA",	// 0xB3, on numeric keypad (NEC PC98)
		"",					// 0xB4
		"DIK_DIVIDE",		// 0xB5 / on numeric keypad
		"",					// 0xB6
		"DIK_SYSRQ",		// 0xB7
		"DIK_RMENU",		// 0xB8 right Alt
		"",					// 0xB9
		"",					// 0xBA
		"",					// 0xBB
		"",					// 0xBC
		"",					// 0xBD
		"",					// 0xBE
		"",					// 0xBF
		"",					// 0xC0
		"",					// 0xC1
		"",					// 0xC2
		"",					// 0xC3
		"",					// 0xC4
		"DIK_PAUSE",		// 0xC5 Pause
		"",					// 0xC6
		"DIK_HOME",			// 0xC7 Home on arrow keypad
		"DIK_UP",			// 0xC8 UpArrow on arrow keypad
		"DIK_PRIOR",		// 0xC9 PgUp on arrow keypad
		"",					// 0xCA
		"DIK_LEFT",			// 0xCB LeftArrow on arrow keypad
		"",					// 0xCC
		"DIK_RIGHT",		// 0xCD RightArrow on arrow keypad
		"",					// 0xCE
		"DIK_END",			// 0xCF End on arrow keypad
		"DIK_DOWN",			// 0xD0 DownArrow on arrow keypad
		"DIK_NEXT",			// 0xD1 PgDn on arrow keypad
		"DIK_INSERT",		// 0xD2 Insert on arrow keypad
		"DIK_DELETE",		// 0xD3 Delete on arrow keypad
		"",					// 0xD4
		"",					// 0xD5
		"",					// 0xD6
		"",					// 0xD7
		"",					// 0xD8
		"",					// 0xD9
		"",					// 0xDA
		"DIK_LWIN",			// 0xDB Left Windows key
		"DIK_RWIN",			// 0xDC Right Windows key
		"DIK_APPS",			// 0xDD AppMenu key
		"DIK_POWER",		// 0xDE System Power
		"DIK_SLEEP",		// 0xDF System Sleep
		"",					// 0xE0
		"",					// 0xE1
		"",					// 0xE2
		"DIK_WAKE",			// 0xE3 System Wake
		"",					// 0xE4
		"DIK_WEBSEARCH",	// 0xE5 Web Search
		"DIK_WEBFAVORITES",	// 0xE6 Web Favorites
		"DIK_WEBREFRESH",	// 0xE7 Web Refresh
		"DIK_WEBSTOP",		// 0xE8 Web Stop
		"DIK_WEBFORWARD",	// 0xE9 Web Forward
		"DIK_WEBBACK",		// 0xEA Web Back
		"DIK_MYCOMPUTER",	// 0xEB My Computer
		"DIK_MAIL",			// 0xEC Mail
		"DIK_MEDIASELECT",	// 0xED Media Select
		"",					// 0xEE
		"",					// 0xEF
		"KEY_FUNCTION",		// 0xF0
		"KEY_ALPHABET",		// 0xF1
		"KEY_NUMBER	", 		// 0xF2
		"KEY_SHIFT",		// 0xF3
		"KEY_CONTROL", 		// 0xF4
		"KEY_ALT", 			// 0xF5
		"KEY_ARROW", 		// 0xF6
		"KEY_NUMPAD",		// 0xF7
		"KEY_SUPER",		// 0xF8
		"",					// 0xF9
		"",					// 0xFA
		"",					// 0xFB
		"",					// 0xFC
		"",					// 0xFD
		"",					// 0xFE
		"KEY_ANY",			// 0xFF
	};

	return keyname[vk];
}

#ifdef OPAL_DEBUG
//----------------------------------------
// �f�o�b�O
//----------------------------------------
void PADX::Debug( UINT m ){

	printd( "��SYSTEM KEY\n" );
	printd( "HOME    :%c%s \n", sysfunc[SYSFUNC_HOME   ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_HOME   ] ) );
	printd( "EXIT    :%c%s \n", sysfunc[SYSFUNC_EXIT   ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_EXIT   ] ) );
	printd( "FULL    :%c%s \n", sysfunc[SYSFUNC_FULL   ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_FULL   ] ) );
	printd( "RESET   :%c%s \n", sysfunc[SYSFUNC_RESET  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_RESET  ] ) );
	printd( "PAUSE   :%c%s \n", sysfunc[SYSFUNC_PAUSE  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_PAUSE  ] ) );
	printd( "BREAK   :%c%s \n", sysfunc[SYSFUNC_BREAK  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_BREAK  ] ) );
	printd( "CAPTURE :%c%s \n", sysfunc[SYSFUNC_CAPTURE] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_CAPTURE] ) );
	printd( "FRAME   :%c%s \n", sysfunc[SYSFUNC_FRAME  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_FRAME  ] ) );
	printd( "TIMES   :%c%s \n", sysfunc[SYSFUNC_TIMES  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_TIMES  ] ) );
	printd( "TIMEC   :%c%s \n", sysfunc[SYSFUNC_TIMES  ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_TIMEC  ] ) );
	printd( "RANDOM  :%c%s \n", sysfunc[SYSFUNC_RANDOM ] ? '!' : ' ', KeyName( sysfkey[SYSFUNC_RANDOM ] ) );
	printd( "\n" );

	const auto	w = data->Work();
	const auto	t = w->Head()->Last<PADT>();

	printd( "��KEYBOARD\n" );
	if ( const auto	p = t->Keyboard() ) {
		for ( auto i = 0UL; i < KEY_MAX; i++ ) {
			if ( p->stroke[i] ) {
				printd( "%02x : %-18s %d\n", i, KeyName( i ),count.GetCountKeyboard( i ) );
			}
		}
	}
	printd( "\n" );

	printd( "��MOUSE\n" );
	if ( const auto p = t->Mouse() ) {
		printd( "CURSOR( %d, %d )\n", p->point.x, p->point.y );
		printd( "MOUSE ( %d, %d, %d )\n",  p->mx, p->my, p->mz );

		for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
			const auto	c = 1 << i;

			printd( "%s :%4d\n", MouseName( c ), count.GetCountMouse( i ) );
		}
	}
	printd( "\n" );

	printd( "��JOYPAD\n" );
	printd( "%d/%d\n", joymax, JOY_MAX );
	for ( auto i = 0UL; i < JOY_MAX; i++ ) {
		if ( const auto p = t->Joypad( i ) ) {
			printd( "[%02d]( %+04d%%, %+04d%% )( %+04d%%, %+04d%% ) %d\n", i,
					int( p->lx * 100 ),
					int( p->ly * 100 ),
					int( p->rx * 100 ),
					int( p->ry * 100 ),
					*p->pov );
		} else {
			printd( "[%d]( -%%, -%% )( -%%, -%% ) -\n", i );
		}
	}
	printd( "\n" );

	printd( "   | " );
	for ( auto i = 0UL; i < JOY_MAX; i++ ) { printd( "[%02d] ", i ); }
	printd( "\n" );
	printd( "---+-" );
	for ( auto i = 0UL; i < JOY_MAX; i++ ) { printd( "-----" ); }
	printd( "\n" );
	for ( auto i = 0UL; i < PAD_MAX; i++ ) {
		const auto	c = 1 << i;

		printd( "%s | ", JoyName( c ) );
		for ( auto j = 0UL; j < JOY_MAX; j++ ) {
			printd( "%04d ", count.GetCountJoypad( j, i ) );
		}
		printd( "\n" );
	}
}
#endif

// End Of File
