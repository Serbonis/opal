//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

//========================================
// シグナルシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void SIGX::Debug( UINT m ) {

	switch ( m ) {
	case 0:
		{
			static const char* sig[SIGX::MAX] = {
												 "NORMAL",
												 "SUSPEND",
												 "DESTROY",
												 "RESET",
												 "PAUSE",
			};

			for ( auto i = 0UL; i < std::size( sig ); i++ ) {
				const auto& sf = SIGX::signal[i].func;

				printd( "%-8s : %d\n", sig[i], sf.size() );
				for ( const auto& [f,v] : SIGX::signal[i].func ) {
					printd( " %s\n", f );
				}
			}
		}
		break;

	case 1:
		printd( "RESET  : %d %d\n", ResetLevel(), SYSTEM::Reset() );
		printd( "PAUSE  : %d %s\n", PauseLevel(), Pause() ? "!" : "-" );
		break;
	}
}
#endif

// End Of File
