//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_DS;

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::CALLX_DS {
	DEVICE			device;
}

//----------------------------------------
// システム
//----------------------------------------
namespace opal::CALLX_DS {
	void SysInitDevice( void ){ device.Init();	}
	void SysFreeDevice( void ){	device.Free();	}
}

// End Of File
