//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// �O���t�B�b�N�^�X�N
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWT::DRAWT(){}
DRAWT::~DRAWT(){}

//----------------------------------------
// ����������n��
//----------------------------------------
void DRAWT::Init( const char* p ){

	ZOBJ::Init( p );
	ZOBJ::Prio( DRAWX::Prio() );
	ZOBJ::Parent( DRAWX::Parent() );

	DRAWS::Init();
}

void DRAWT::Free( void ){

	DRAWS::Free();
	ZOBJ::Free();
}

void DRAWT::Open_(  const std::string_view p, DLINX* l ){ ZOBJ::Open( p.data(), l );	}
void DRAWT::Close_(                           DLINX* l ){ ZOBJ::Close(          l );	}

void DRAWT::Open( const std::string_view p, const char* ){ Init( p.data() ); Register( "" );	}

//----------------------------------------
// �o�^�����
//----------------------------------------
void DRAWT::Register( LINX* l ){

	if ( l ) {
		ZOBJ::Register( l );
	} else if ( DRAWX::sysdef.linx ) {
		ZOBJ::Register( DRAWX::sysdef.linx );	// �f�t�H���g�o�^��
	} else {
		ZOBJ::Register( DRAWX::index );			// �V�X�e���o�^��
	}
}

void DRAWT::Release( LINX* l ){

	if ( l ) {
		ZOBJ::Release( l );
	} else {
		ZOBJ::Release( Linx() );
	}
}

void DRAWT::Register_( DLINX* l ){ DRAWT::Register( ( LINX* )l ); }
void DRAWT::Release_(  DLINX* l ){ DRAWT::Release(  ( LINX* )l ); }

void DRAWT::Register( const char* ){ ZOBJ::Register( DRAWX::index ); }

//----------------------------------------
// ����
//----------------------------------------
DLINX* DRAWT::GetBranch( void ) const { return dynamic_cast<DLINX*>( ZOBJ::Branch() );	}
void   DRAWT::SetBranch( DLINX* l ){ ZOBJ::Branch( l );	}

//----------------------------------------
// �`��p���
//----------------------------------------
const MATRIX* DRAWT::View( void ) const { return DRAWX::View();	}
const MATRIX* DRAWT::Vinv( void ) const { return DRAWX::Vinv();	}
const MATRIX* DRAWT::Bill( void ) const { return DRAWX::Bill();	}
const MATRIX* DRAWT::Proj( void ) const { return GetPers() ? DRAWX::Pers() : DRAWX::Orth(); }

const VECTOR3* DRAWT::LightPosition(  UINT n ) const { return DRAWX::LightPosition(  n ); }
const VECTOR3* DRAWT::LightDirection( UINT n ) const { return DRAWX::LightDirection( n ); }
const COLOR    DRAWT::Ambient( void ) const { return DRAWX::Ambient(); }

//----------------------------------------
// �o�^����
//----------------------------------------
DLINX* DRAWT::DLinx( void ) const  { return dynamic_cast<DLINX*>( Linx() );	}
DLINX* DRAWT::TLinx( const DRAWT* ){ return dynamic_cast<DLINX*>( DRAWX::sysdef.linx ? DRAWX::sysdef.linx : DRAWX::index ); }
LINX*  DRAWT::TLinx( void ){ return DRAWX::index; }

// End Of File
