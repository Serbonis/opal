//========================================
// OPAL Game Library
//========================================
#ifdef OPAL_DEBUG
#include "com.hpp"
#include "pad.hpp"

//========================================
// デバッグシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ変数
//----------------------------------------
namespace {
	constexpr size_t		Max = 0x100000;
	char					Buff[Max];
	wchar_t					BuffW[Max];
	UINT					Line;
	UINT					Leng;
	bool					Lock;
	UINT					KeyPrev;
	UINT					KeyNext;
}

//----------------------------------------
// デバッグ関数
//----------------------------------------
namespace opal::DEBUGX_ {
	void DebugLock( bool vFlag ){ Lock = vFlag;	}
	bool DebugLock( void ){ return Lock;		}

	void DebugPrint( UINT kp, UINT kn ){

		Line = 0;
		Leng = 0;
		Lock = false;

		KeyPrev = kp;
		KeyNext = kn;
	}

	const char* DebugPrint( void ){

		const auto	max = strline_s( Buff );

		if ( PADX::KeyPush( KeyPrev ) ) { if ( Line < max ) { ++Line;	} }
		if ( PADX::KeyPush( KeyNext ) ) { if ( Line > 0   ) { --Line;	} }

		const auto*	buff = strcnt_s( Buff, "\n", Line );

		if ( buff == nullptr ) {
			buff = Buff;
		} else if ( buff != Buff ) {
			buff++;
		}
		Leng = 0;

		return buff;
	}

	const wchar_t* DebugPrintW( void ){

		const auto	buff = std::string{DebugPrint()};

		::mbstowcs( BuffW, buff.c_str(), buff.length() );

		return BuffW;
	}
}


//========================================
// print
//========================================
//----------------------------------------
// 汎用プリント
//----------------------------------------
int printd( const char* format, ... ){

	if ( Lock ) {
		return -1;
	}

	{
		va_list	list;

		va_start( list, format );
		if ( const auto leng = strlenf_s( format, list ) ) {
			if ( Leng + leng < Max ) {
				std::vsprintf( &Buff[Leng], format, list );
				Leng += leng;
			}
		}
		va_end( list );
	}

	return Leng;
}

//----------------------------------------
// VECCTOR
//----------------------------------------
#define FORM_V2	"( %f %f )\n"
#define FORM_V3	"( %f %f %f )\n"
#define FORM_V4	"( %f %f %f %f )\n"

#define ARG_V2( a )	(a)->x, (a)->y
#define ARG_V3( a )	(a)->x, (a)->y, (a)->z
#define ARG_V4( a )	(a)->x, (a)->y, (a)->z, (a)->w

int  printd( const VECTOR2* a ){ return ( a ) ? printd( FORM_V2, ARG_V2(  a ) ) : 0;	}
int  printd( const VECTOR3* a ){ return ( a ) ? printd( FORM_V3, ARG_V3(  a ) ) : 0;	}
int  printd( const VECTOR4* a ){ return ( a ) ? printd( FORM_V4, ARG_V4(  a ) ) : 0;	}
int  printd( const VECTOR2& a ){ return         printd( FORM_V2, ARG_V2( &a ) );		}
int  printd( const VECTOR3& a ){ return         printd( FORM_V3, ARG_V3( &a ) );		}
int  printd( const VECTOR4& a ){ return         printd( FORM_V4, ARG_V4( &a ) );		}
void printf( const VECTOR2& a ){                printf( FORM_V2, ARG_V2( &a ) );		}
void printf( const VECTOR3& a ){                printf( FORM_V3, ARG_V3( &a ) );		}
void printf( const VECTOR4& a ){                printf( FORM_V4, ARG_V4( &a ) );		}

//----------------------------------------
// MATRIX
//----------------------------------------
#define FORM_MT	\
"[ %f %f %f %f ]\n"\
"[ %f %f %f %f ]\n"\
"[ %f %f %f %f ]\n"\
"[ %f %f %f %f ]\n"

#define ARG_MT( a )	\
(a)->_11, (a)->_12, (a)->_13, (a)->_14,\
(a)->_21, (a)->_22, (a)->_23, (a)->_24,\
(a)->_31, (a)->_32, (a)->_33, (a)->_34,\
(a)->_41, (a)->_42, (a)->_43, (a)->_44

int  printd( const MATRIX* a ){ return ( a ) ? printd( FORM_MT, ARG_MT(  a ) ) : 0;	}
int  printd( const MATRIX& a ){ return         printd( FORM_MT, ARG_MT( &a ) );		}
void printf( const MATRIX& a ){                printf( FORM_MT, ARG_MT( &a ) );		}

#endif

// End Of File
