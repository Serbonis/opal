//========================================
// OPAL Game Library
//========================================
#include "thrd.hpp"

//========================================
// スレッドシステム
//========================================
using namespace opal;

//----------------------------------------
// タスクの登録
//----------------------------------------
THRDT* THRDX::Throw( UINT        p, std::function<void*(THRDT*)> f, void* a, size_t s ){ return Throw( ( const char* )NAME( "%x", p ), f, a, s ); }
THRDT* THRDX::Throw( const void* p, std::function<void*(THRDT*)> f, void* a, size_t s ){ return Throw( ( const char* )NAME( "%p", p ), f, a, s ); }
THRDT* THRDX::Throw( const char* p, std::function<void*(THRDT*)> f, void* a, size_t s ){

	if ( p == nullptr    ) { return nullptr;	}
	if ( tmap.count( p ) ) { Kill( p );			}
	if ( f == nullptr    ) { return nullptr;	}

	if ( const auto t = NEWS( tmap[p] ) ) {
		t->Open( p, &index );
		t->func = f;
		t->size = s;
		t->argv = a;
		return t;
	}

	return nullptr;
}

//----------------------------------------
// タスクの取得
//----------------------------------------
std::size_t THRDX::Task( void ) {

	return index.Elements();
}

THRDT* THRDX::Task( UINT        p ){ return Task( ( const char* )NAME( "%x", p ) ); }
THRDT* THRDX::Task( const void* p ){ return Task( ( const char* )NAME( "%p", p ) ); }
THRDT* THRDX::Task( const char* p ){

	return ( p && tmap.count( p ) ) ? tmap[p] : nullptr;
}

//----------------------------------------
// タスクの削除
//----------------------------------------
bool THRDX::Kill( UINT        p, UINT c ){ return Kill( ( const char* )NAME( "%x", p ), c ); }
bool THRDX::Kill( const void* p, UINT c ){ return Kill( ( const char* )NAME( "%p", p ), c ); }
bool THRDX::Kill( const char* p, UINT c ){

	if ( const auto t = Task( p ) ) {
		::TerminateThread( t->hand, t->code = c );
		return true;
	}
	return false;
}

// End Of File
