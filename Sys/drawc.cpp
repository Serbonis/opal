//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;

//----------------------------------------
// 取得
//----------------------------------------
void DRAWX::Capture( const char* p ){

	DRAWX_::Capture( p );
}

namespace opal::DRAWX_ {
	const char* screenshot( char* b, size_t s, const char* e ){

		struct tm	t;

		timelocaltime( &t );

		::sprintf_s( b, s, "%04d%02d%02d-%02d%02d%02d-%s.%s",
					 t.tm_year+1900,
					 t.tm_mon+1,
					 t.tm_mday,
					 t.tm_hour,
					 t.tm_min,
					 t.tm_sec,
					 guid_name().c_str(),
					 e ? e : "jpg" );

		return b;
	}
}

// End Of File
