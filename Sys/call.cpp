//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( CALLX::index  ){};
DECLVAL( CALLX::linx   ){};
DECLVAL( CALLX::parent ){};

#ifdef OPAL_DEBUG
DECLVAL( CALLX::debug  ){};
#endif

//----------------------------------------
// システム初期化
//----------------------------------------
bool CALLX::SysInit( void ){

	if ( ( index = NEWS( (*SUPERX::Linx())["CALLX"] ) ) ) {
		index->Init( "CLASS-CALLW" );
	}

	linx	= nullptr;
	parent	= nullptr;

	CALLX_::SysInitDevice();

	CALLT::callSysInit();

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool CALLX::SysTaskInit(  void ){ CALLT::callInit();	SUPERX::TaskInit(  index ); return true;	}
bool CALLX::SysTaskEnter( void ){ 						SUPERX::TaskEnter( index );	return true;	}
bool CALLX::SysTaskMain(  void ){ CALLT::callExec();	SUPERX::TaskMain(  index ); return true;	}
bool CALLX::SysTaskExit(  void ){ 						SUPERX::TaskExit(  index );	return true;	}
bool CALLX::SysTaskFree(  void ){ CALLT::callFree();	SUPERX::TaskFree(  index ); return true;	}

//----------------------------------------
// システム後始末
//----------------------------------------
bool CALLX::SysFree( void ){

	CALLT::callSysFree();
	CALLX_::SysFreeDevice();

	linx	= nullptr;

	if ( index ) {
		index->Free();
		DELETES( index );
		SUPERX::Linx()->erase( "CALLX" );
	}

	return true;
}

// End Of File
