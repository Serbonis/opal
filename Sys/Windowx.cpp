//========================================
// OPAL Game Library
//========================================
#include "window.hpp"

//========================================
// ウインドマネージャー
//========================================
using namespace opal;

//----------------------------------------
// ウィンドウシステム
//----------------------------------------
#define WI	WINDOW::Instance()

void WINDOWX::Open(  void ){ WI->Open();	}
void WINDOWX::Close( void ){ WI->Close();	}

HWND WINDOWX::Handle(  void ){ return WI->Handle();	}
UINT WINDOWX::Message( void ){ return WI->Message();	}
int  WINDOWX::Process( void ){ return WI->Process();	}

void WINDOWX::Active( int vFlag ){ return WI->Active( vFlag );	}
bool WINDOWX::Active( void      ){ return WI->Active();			}

void WINDOWX::Focus( int vFlag ){ return WI->Focus( vFlag );	}
bool WINDOWX::Focus( void      ){ return WI->Focus();			}

void WINDOWX::Move( int X, int Y, int W, int H ){ WI->Move( X, Y, W, H ); }
bool WINDOWX::Moving( void ){ return WI->Moving(); }
void WINDOWX::Reset(  void ){ WI->Reset(); }

void WINDOWX::Sizing( int S ){ WI->Sizing( S );			}
int  WINDOWX::Sizing( void  ){ return WI->Sizing();		}

int WINDOWX::WindowX( void ){ return WI->WindowX(); 	}
int WINDOWX::WindowY( void ){ return WI->WindowY(); 	}
int WINDOWX::WindowW( void ){ return WI->WindowW(); 	}
int WINDOWX::WindowH( void ){ return WI->WindowH(); 	}

int WINDOWX::FrameX( void ){ return WI->WindowX() + ::GetSystemMetrics( SM_CXFRAME );	}
int WINDOWX::FrameY( void ){ return WI->WindowY() + ::GetSystemMetrics( SM_CYFRAME ) + ::GetSystemMetrics( SM_CYCAPTION );		}
int WINDOWX::FrameW( void ){ return WI->WindowW() - ::GetSystemMetrics( SM_CXFRAME )*2;	}
int WINDOWX::FrameH( void ){ return WI->WindowH() - ::GetSystemMetrics( SM_CYFRAME )*2 - ::GetSystemMetrics( SM_CYCAPTION );	}

int WINDOWX::ScreenW(  void ){ return WI->ScreenW(); 	}
int WINDOWX::ScreenH(  void ){ return WI->ScreenH();	}
int WINDOWX::DisplayW( void ){ return WI->DisplayW();	}
int WINDOWX::DisplayH( void ){ return WI->DisplayH();	}
int WINDOWX::ScreenW(  float px ){ return ScreenW()  - px; }
int WINDOWX::ScreenH(  float py ){ return ScreenH()  - py; }
int WINDOWX::DisplayW( float px ){ return DisplayW() - px; }
int WINDOWX::DisplayH( float py ){ return DisplayH() - py; }
int WINDOWX::ScreenW(  float px, float cx ){ return ScreenW() /2 + cx + px; }
int WINDOWX::ScreenH(  float py, float cy ){ return ScreenH() /2 + cy + py; }
int WINDOWX::DisplayW( float px, float cx ){ return DisplayW()/2 + cx + px; }
int WINDOWX::DisplayH( float py, float cy ){ return DisplayH()/2 + cy + py; }

float WINDOWX::ScreenR(  void ){ return ( float )ScreenW() /ScreenH();	}
float WINDOWX::DisplayR( void ){ return ( float )DisplayW()/DisplayH();	}

void WINDOWX::Screen( int vFlag ){

	switch ( vFlag ) {
	case true : WI->Screen( true  );		break;
	case false: WI->Screen( false );		break;
	default   : WI->Screen( !Screen() );	break;
	}
}
bool WINDOWX::Screen( void ){ return WI->Screen(); }

void WINDOWX::Cursor( int vFlag ){

	switch ( vFlag ) {
	case true : WI->Cursor( true  );		break;
	case false: WI->Cursor( false );		break;
	default   : WI->Cursor( !Cursor() );	break;
	}
}
bool WINDOWX::Cursor( void ){ return WI->Cursor(); }

void WINDOWX::SetCursor(  LONG vPx, LONG vPy ){ ::SetCursorPos( vPx, vPy ); }
void WINDOWX::SetCursorX( LONG vPx ){ SetCursor( vPx, GetCursorY() );	}
void WINDOWX::SetCursorY( LONG vPy ){ SetCursor( GetCursorX(), vPy );	}

void WINDOWX::AddCursor(  LONG vPx, LONG vPy ){ SetCursor( vPx+GetCursorX(), vPy+GetCursorY() ); }
void WINDOWX::AddCursorX( LONG vPx ){ AddCursor( vPx, 0 ); }
void WINDOWX::AddCursorY( LONG vPy ){ AddCursor( 0, vPy ); }

void WINDOWX::GetCursor(  LONG* vPx, LONG* vPy ){

	::POINT pt;

	::GetCursorPos( &pt );

	if ( vPx ){ *vPx = pt.x; }
	if ( vPy ){ *vPy = pt.y; }
}
LONG WINDOWX::GetCursorX( void ){ LONG x; GetCursor( &x, nullptr ); return x; }
LONG WINDOWX::GetCursorY( void ){ LONG y; GetCursor( nullptr, &y ); return y; }

#undef WI

//----------------------------------------
// 汎用ウィンドウ
//----------------------------------------
namespace {
	LRESULT CALLBACK event( HWND hWnd, UINT msg, WPARAM wp, LPARAM lp ){

		switch ( msg ) {
		case WM_DESTROY:
			::PostQuitMessage( 0 );
			return 0;
		}
		return ::DefWindowProc( hWnd, msg, wp, lp );
	}
}

void WINDOWX::Open( HWND& handle, UINT vW, UINT vH, const char* pName, WNDPROC pFunc ){

	WNDCLASSEX	wc;

	wc.cbSize		= sizeof( wc );
	wc.style		= CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc	= pFunc ? pFunc : event;
	wc.cbClsExtra	= 0;
	wc.cbWndExtra	= 0;
	wc.hInstance	= ::GetModuleHandle( nullptr );
	wc.hIcon		= ::LoadIcon( wc.hInstance, OPAL::ICON() );
	wc.hIconSm		= wc.hIcon;
	wc.hCursor		= ( HCURSOR )::LoadImage( nullptr,IDC_ARROW, IMAGE_CURSOR,0,0,LR_DEFAULTSIZE|LR_SHARED );
	wc.hbrBackground= ( HBRUSH  )::GetStockObject( BLACK_BRUSH );
	wc.lpszMenuName	= nullptr;
	wc.lpszClassName= ( pName ) ? pName : OPAL::PROJECT();

	::RegisterClassEx( &wc );

	handle = ::CreateWindow(wc.lpszClassName,
							wc.lpszClassName,
							WS_OVERLAPPEDWINDOW,
							CW_USEDEFAULT,
							CW_USEDEFAULT,
							vW + ::GetSystemMetrics( SM_CXFRAME ) * 2,
							vH + ::GetSystemMetrics( SM_CYFRAME ) * 2 + ::GetSystemMetrics( SM_CYCAPTION ),
							nullptr,nullptr,wc.hInstance,nullptr );

	::ShowWindow(   handle, SW_SHOW );
	::UpdateWindow( handle );
}

void WINDOWX::Close( HWND& handle ){

	if ( handle ) {
		::DestroyWindow( handle );
	}
}

// End Of File
