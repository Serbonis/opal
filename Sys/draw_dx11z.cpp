//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// インデックスバッファ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateIndexBuffer( ID3D11Buffer*& buffer, const void* list, size_t size, bool flag )->D3D11_BUFFER_DESC{

		D3D11_BUFFER_DESC			desc{};

		desc.ByteWidth				= size;
		desc.Usage					= D3D11_USAGE_DEFAULT;
		desc.BindFlags				= D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags			= 0;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;

		D3D11_SUBRESOURCE_DATA		data{};

		data.pSysMem				= list;
		data.SysMemPitch			= 0;
		data.SysMemSlicePitch		= 0;

		DRAWX_DX11::device()->CreateBuffer( &desc, &data, &buffer );

		return desc;
	}
	void DestroyIndexBuffer( ID3D11Buffer*& buffer ){ RELEASE( buffer );	}
}

//----------------------------------------
// 頂点バッファ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateVertexBuffer( ID3D11Buffer*& buffer, const void* list, size_t size, bool flag )->D3D11_BUFFER_DESC{

		D3D11_BUFFER_DESC			desc{};

		desc.ByteWidth				= size;
		desc.Usage					= D3D11_USAGE_DEFAULT;
		desc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags			= 0;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;

		if ( flag ) {
			desc.Usage				= D3D11_USAGE_DYNAMIC;
			desc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
		}

		D3D11_SUBRESOURCE_DATA		data{};

		data.pSysMem          		= list;
		data.SysMemPitch      		= 0;
		data.SysMemSlicePitch		= 0;

		DRAWX_DX11::device()->CreateBuffer( &desc, &data, &buffer );

		return desc;
	}
	void DestroyVertexBuffer( ID3D11Buffer*& buffer ){ RELEASE( buffer );	}
}

//----------------------------------------
// 定数バッファ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateConstantBuffer( ID3D11Buffer*& buffer, size_t size )->D3D11_BUFFER_DESC{

		D3D11_BUFFER_DESC			desc{};

		desc.ByteWidth				= size;
		desc.Usage					= D3D11_USAGE_DEFAULT;
		desc.BindFlags				= D3D11_BIND_CONSTANT_BUFFER;
		desc.CPUAccessFlags			= 0;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;

		DRAWX_DX11::device()->CreateBuffer( &desc, nullptr, &buffer );

		return desc;
	}
	void DestroyConstantBuffer( ID3D11Buffer*& buffer ){ RELEASE( buffer );	}
}

//----------------------------------------
// 頂点レイアウト
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void CreateInputLayout( ID3D11InputLayout*& layout,  const void* vs_p, size_t vs_s, const D3D11_INPUT_ELEMENT_DESC* el_p, size_t el_s ){

		DRAWX_DX11::device()->CreateInputLayout( el_p, el_s, vs_p, vs_s, &layout );
	}
	void DestroyInputLayout( ID3D11InputLayout*& layout ){ RELEASE( layout );	}
}

//----------------------------------------
// 頂点シェーダー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void CreateVertexShader( ID3D11VertexShader*& vshader, const void* vs_p, size_t vs_s, ID3D11ClassLinkage* link ){

		DRAWX_DX11::device()->CreateVertexShader( vs_p, vs_s, link, &vshader );
	}
	void DestroyVertexShader( ID3D11VertexShader*& vshader ){ RELEASE( vshader );	}
}

//----------------------------------------
// ピクセルシェーダー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void CreatePixelShader( ID3D11PixelShader*& pshader,  const void* ps_p, size_t ps_s, ID3D11ClassLinkage* link ){

		DRAWX_DX11::device()->CreatePixelShader(  ps_p, ps_s, link, &pshader  );
	}
	void DestroyPixelShader( ID3D11PixelShader*& pshader ){ RELEASE( pshader );	}
}

//----------------------------------------
// デバイスステートの一括リセット
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void ResetState( void ){ device.ResetState();	}
}

//----------------------------------------
// レンダリングステート( UNIQUE & MAX 4096 ! )
// 同じ条件で作成すると"同じオブジェクト"が返される(ので多重解放しないように注意...)
// 作成可能なステートオブジェクトの個数は4096個
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateRasterizerState( ID3D11RasterizerState*& raster, bool line )->D3D11_RASTERIZER_DESC{

		D3D11_RASTERIZER_DESC		desc{};

		desc.FillMode				= line ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
		desc.CullMode				= D3D11_CULL_BACK;
		desc.FrontCounterClockwise	= false;
		desc.DepthBias				= D3D11_DEFAULT_DEPTH_BIAS;
		desc.DepthBiasClamp			= D3D11_DEFAULT_DEPTH_BIAS_CLAMP;
		desc.SlopeScaledDepthBias	= D3D11_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
		desc.DepthClipEnable		= true;
		desc.ScissorEnable			= true;
		desc.MultisampleEnable		= false;
		desc.AntialiasedLineEnable	= line;

		DRAWX_DX11::device()->CreateRasterizerState( &desc, &raster );

		return desc;
	}
	void DestroyRasterizerState( ID3D11RasterizerState*& raster ){ RELEASE( raster );	}

	bool CheckRasterizerState( ID3D11RasterizerState*& ra ){

		class CHECK final {
		public:
			CHECK( ID3D11RasterizerState*& ra_ ) :
				ra( ra_ ),
				rb( nullptr )
			{ DRAWX_DX11::device( 0 )->RSGetState( &rb ); }
			~CHECK(){ RELEASE( rb ); }

		private:
			ID3D11RasterizerState*& ra;
			ID3D11RasterizerState*	rb;

		public:
			operator bool( void ){ return ra == rb; }
		};

		return CHECK( ra ) ? true : false;
	}
}

//----------------------------------------
// 深度バッファステート( UNIQUE & MAX 4096 ! )
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateDepthStencilState( ID3D11DepthStencilState*& stencil )->D3D11_DEPTH_STENCIL_DESC{

		D3D11_DEPTH_STENCIL_DESC	desc{};

		desc.DepthEnable					= true;
		desc.DepthWriteMask					= D3D11_DEPTH_WRITE_MASK_ALL;
		desc.DepthFunc						= D3D11_COMPARISON_LESS_EQUAL;
		desc.StencilEnable					= false;
		desc.StencilReadMask				= D3D11_DEFAULT_STENCIL_READ_MASK;
		desc.StencilWriteMask				= D3D11_DEFAULT_STENCIL_WRITE_MASK;
		desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
		desc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;

		DRAWX_DX11::device()->CreateDepthStencilState( &desc, &stencil );

		return desc;
	}
	void DestroyDepthStencilState( ID3D11DepthStencilState*& stencil ){ RELEASE( stencil );	}

	bool CheckDepthStencilState( ID3D11DepthStencilState*& sa, UINT ra ){

		class CHECK final {
		public:
			CHECK( ID3D11DepthStencilState*& sa_, UINT ra_ ) :
				sa( sa_ ),
				ra( ra_ ),
				sb( nullptr ),
				rb( 0 )
			{ DRAWX_DX11::device( 0 )->OMGetDepthStencilState( &sb, &rb ); }
			~CHECK(){ RELEASE( sb ); }

		private:
			ID3D11DepthStencilState*&	sa;
			UINT&						ra;
			ID3D11DepthStencilState*	sb;
			UINT 						rb;

		public:
			operator bool( void ){ return sa == sb; }
		};

		return CHECK( sa, ra ) ? true : false;
	}

	void ResetDepthStencilState( void ){ device.ResetDepthStencilState();	}
}

namespace opal::DRAWX_DX11 {
	KEEP_DepthStencilState::KEEP_DepthStencilState() :
		context( DRAWX_DX11::device( 0 ) )
	{
		context->OMGetDepthStencilState( &state, &ref );
	}
	KEEP_DepthStencilState::~KEEP_DepthStencilState()
	{
		context->OMSetDepthStencilState(  state,  ref );
	}
}

//----------------------------------------
// ブレンドステート( UNIQUE & MAX 4096 ! )
//----------------------------------------
//------------------------------------------------------------
// https://msdn.microsoft.com/ja-jp/library/ee416044(v=vs.85).aspx
//------------------------------------------------------------
// D3D11_BLEND_OP_	演算
//------------------------------------------------------------
// ADD				A + B
// SUBTRACT			A - B
// REV_SUBTRACT		B - A
// MIN				min( A, B )
// MAX				max( A, B )

// https://msdn.microsoft.com/ja-jp/library/ee416042(v=vs.85).aspx
//------------------------------------------------------------
// D3D11_BLEND_		COLOR								前処理
//------------------------------------------------------------
// ZERO				黒 (0, 0, 0, 0) 					-
// ONE				白 (1, 1, 1, 1) 					-

// SRC_COLOR		ピクセルシェーダー カラー			-
// INV_SRC_COLOR	ピクセルシェーダー カラー			1 - C

// SRC_ALPHA		ピクセルシェーダー アルファ			-
// INV_SRC_ALPHA	ピクセルシェーダー アルファ			1 - A

// DEST_COLOR		レンダーターゲット カラー			-
// INV_DEST_COLOR	レンダーターゲット カラー			1 - C

// DEST_ALPHA		レンダーターゲット アルファ			-
// INV_DEST_ALPHA	レンダーターゲット アルファ			1 - A

// SRC_ALPHA_SAT	ピクセルシェーダー アルファ			1以下にクランプ

// OMSetBlendState( 第2引数 float[4] )
// BLEND_FACTOR		ブレンディング係数					-
// INV_BLEND_FACTOR	ブレンディング係数					1 - F

// デュアルソースカラーブレンディング
// SRC1_COLOR		ピクセルシェーダー出力 カラー		-
// INV_SRC1_COLOR	ピクセルシェーダー出力 カラー		1 - C
// SRC1_ALPHA		ピクセルシェーダー出力 アルファ		-
// INV_SRC1_ALPHA	ピクセルシェーダー出力 アルファ		1 - A
//------------------------------------------------------------
namespace opal::DRAWX_DX11 {

	auto CreateBlendState( ID3D11BlendState*& blend, UINT flag )->D3D11_BLEND_DESC{

		switch ( flag ) {
		case BLEND_TYPE0: return CreateBlendState( blend );
		case BLEND_TYPE1: break;
		case BLEND_TYPE2: return CreateBlendState( blend, D3D11_BLEND_ZERO,			D3D11_BLEND_ONE,			D3D11_BLEND_OP_ADD );
		case BLEND_TYPE3: return CreateBlendState( blend, D3D11_BLEND_SRC_ALPHA,	D3D11_BLEND_INV_SRC_ALPHA,	D3D11_BLEND_OP_ADD );
		case BLEND_TYPE4: return CreateBlendState( blend, D3D11_BLEND_SRC_COLOR,	D3D11_BLEND_INV_SRC_COLOR,	D3D11_BLEND_OP_ADD );
		case BLEND_TYPE5: return CreateBlendState( blend, D3D11_BLEND_ONE,			D3D11_BLEND_ONE,			D3D11_BLEND_OP_ADD );
		case BLEND_TYPE6: return CreateBlendState( blend, D3D11_BLEND_ONE,			D3D11_BLEND_ONE,			D3D11_BLEND_OP_SUBTRACT );
		case BLEND_TYPE7: return CreateBlendState( blend, D3D11_BLEND_ONE,			D3D11_BLEND_ONE,			D3D11_BLEND_OP_REV_SUBTRACT );
		};

		return D3D11_BLEND_DESC{};
	}

	auto CreateBlendState( ID3D11BlendState*& blend )->D3D11_BLEND_DESC{

		D3D11_RENDER_TARGET_BLEND_DESC		desc{};

		desc.BlendEnable			= false;
		desc.SrcBlend				= D3D11_BLEND_ONE;
		desc.DestBlend				= D3D11_BLEND_ZERO;
		desc.BlendOp				= D3D11_BLEND_OP_ADD;
		desc.SrcBlendAlpha			= D3D11_BLEND_ONE;
		desc.DestBlendAlpha			= D3D11_BLEND_ZERO;
		desc.BlendOpAlpha			= D3D11_BLEND_OP_ADD;
		desc.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;

		return CreateBlendState( blend, desc );
	}

	auto CreateBlendState( ID3D11BlendState*& blend, D3D11_BLEND sr, D3D11_BLEND dt, D3D11_BLEND_OP op )->D3D11_BLEND_DESC{

		const D3D11_BLEND		sr2[] = { sr,D3D11_BLEND_ONE	};
		const D3D11_BLEND		dt2[] = { dt,D3D11_BLEND_ZERO	};
		const D3D11_BLEND_OP	op2[] = { op,D3D11_BLEND_OP_ADD	};

		return CreateBlendState( blend, sr2, dt2, op2 );
	}

	auto CreateBlendState( ID3D11BlendState*& blend, const D3D11_BLEND* sr, const D3D11_BLEND* dt, const D3D11_BLEND_OP* op )->D3D11_BLEND_DESC{

		D3D11_RENDER_TARGET_BLEND_DESC		desc{};

		desc.BlendEnable			= true;
		desc.SrcBlend				= sr[0];
		desc.DestBlend				= dt[0];
		desc.BlendOp				= op[0];
		desc.SrcBlendAlpha			= sr[1];
		desc.DestBlendAlpha			= dt[1];
		desc.BlendOpAlpha			= op[1];
		desc.RenderTargetWriteMask	= D3D11_COLOR_WRITE_ENABLE_ALL;

		return CreateBlendState( blend, desc );
	}

	auto CreateBlendState( ID3D11BlendState*& blend, const D3D11_RENDER_TARGET_BLEND_DESC& rtbd )->D3D11_BLEND_DESC{

		D3D11_BLEND_DESC	desc{};

		desc.AlphaToCoverageEnable	= true;
		desc.IndependentBlendEnable	= false;

		for ( auto i = 0UL; i < std::size( desc.RenderTarget ); i++ ) {
			desc.RenderTarget[i] = rtbd;
		}

		DRAWX_DX11::device()->CreateBlendState( &desc, &blend );

		return desc;
	}

	void DestroyBlendState( ID3D11BlendState*& blend ){ RELEASE( blend );	}

	bool CheckBlendState( ID3D11BlendState*& ba, const float* fa, UINT ma ){

		class CHECK final {
		public:
			CHECK( ID3D11BlendState*& ba_, const float* fa_, UINT ma_ ) :
				ba( ba_ ),
				fa( fa_ ),
				ma( ma_ ),
				bb( nullptr ),
				fb{ 0,0,0,0 },
				mb( 0 )
			{ DRAWX_DX11::device( 0 )->OMGetBlendState( &bb, fb, &mb ); }
			~CHECK(){ RELEASE( bb ); }

		private:
			ID3D11BlendState*&	ba;
			const float*&		fa;
			UINT&				ma;
			ID3D11BlendState*	bb;
			float				fb[4];
			UINT				mb;

		public:
			operator bool( void ){ return ba == bb; }
		};

		return CHECK( ba, fa, ma ) ? true : false;
	}

	void ResetBlendState( void ){ device.ResetBlendState();		}
}

#undef BS
#undef BS_ADD
#undef BS_SUB
#undef BS_REV
#undef BS_MIN
#undef BS_MAX

//----------------------------------------
// テクスチャー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	DXGI_FORMAT Format( ID3D11Texture2D* texture ){

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	desc{};

			texture->GetDesc( &desc );

			return desc.Format;
		}
		return DXGI_FORMAT_UNKNOWN;
	}
	UINT Width( ID3D11Texture2D* texture ){

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	desc{};

			texture->GetDesc( &desc );

			return desc.Width;
		}
		return 0;
	}
	UINT Height( ID3D11Texture2D* texture ){

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	desc{};

			texture->GetDesc( &desc );

			return desc.Height;
		}
		return 0;
	}

	size_t BitsPerPixel( DXGI_FORMAT f ){

		switch ( f ) {
		case DXGI_FORMAT_R32G32B32A32_TYPELESS:
		case DXGI_FORMAT_R32G32B32A32_FLOAT:
		case DXGI_FORMAT_R32G32B32A32_UINT:
		case DXGI_FORMAT_R32G32B32A32_SINT:
			return 128;

		case DXGI_FORMAT_R32G32B32_TYPELESS:
		case DXGI_FORMAT_R32G32B32_FLOAT:
		case DXGI_FORMAT_R32G32B32_UINT:
		case DXGI_FORMAT_R32G32B32_SINT:
			return 96;

		case DXGI_FORMAT_R16G16B16A16_TYPELESS:
		case DXGI_FORMAT_R16G16B16A16_FLOAT:
		case DXGI_FORMAT_R16G16B16A16_UNORM:
		case DXGI_FORMAT_R16G16B16A16_UINT:
		case DXGI_FORMAT_R16G16B16A16_SNORM:
		case DXGI_FORMAT_R16G16B16A16_SINT:
		case DXGI_FORMAT_R32G32_TYPELESS:
		case DXGI_FORMAT_R32G32_FLOAT:
		case DXGI_FORMAT_R32G32_UINT:
		case DXGI_FORMAT_R32G32_SINT:
		case DXGI_FORMAT_R32G8X24_TYPELESS:
		case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
		case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
		case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
		case DXGI_FORMAT_Y416:
		case DXGI_FORMAT_Y210:
		case DXGI_FORMAT_Y216:
			return 64;

		case DXGI_FORMAT_R10G10B10A2_TYPELESS:
		case DXGI_FORMAT_R10G10B10A2_UNORM:
		case DXGI_FORMAT_R10G10B10A2_UINT:
		case DXGI_FORMAT_R11G11B10_FLOAT:
		case DXGI_FORMAT_R8G8B8A8_TYPELESS:
		case DXGI_FORMAT_R8G8B8A8_UNORM:
		case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
		case DXGI_FORMAT_R8G8B8A8_UINT:
		case DXGI_FORMAT_R8G8B8A8_SNORM:
		case DXGI_FORMAT_R8G8B8A8_SINT:
		case DXGI_FORMAT_R16G16_TYPELESS:
		case DXGI_FORMAT_R16G16_FLOAT:
		case DXGI_FORMAT_R16G16_UNORM:
		case DXGI_FORMAT_R16G16_UINT:
		case DXGI_FORMAT_R16G16_SNORM:
		case DXGI_FORMAT_R16G16_SINT:
		case DXGI_FORMAT_R32_TYPELESS:
		case DXGI_FORMAT_D32_FLOAT:
		case DXGI_FORMAT_R32_FLOAT:
		case DXGI_FORMAT_R32_UINT:
		case DXGI_FORMAT_R32_SINT:
		case DXGI_FORMAT_R24G8_TYPELESS:
		case DXGI_FORMAT_D24_UNORM_S8_UINT:
		case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
		case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
		case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
		case DXGI_FORMAT_R8G8_B8G8_UNORM:
		case DXGI_FORMAT_G8R8_G8B8_UNORM:
		case DXGI_FORMAT_B8G8R8A8_UNORM:
		case DXGI_FORMAT_B8G8R8X8_UNORM:
		case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
		case DXGI_FORMAT_B8G8R8A8_TYPELESS:
		case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
		case DXGI_FORMAT_B8G8R8X8_TYPELESS:
		case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
		case DXGI_FORMAT_AYUV:
		case DXGI_FORMAT_Y410:
		case DXGI_FORMAT_YUY2:
			return 32;

		case DXGI_FORMAT_P010:
		case DXGI_FORMAT_P016:
			return 24;

		case DXGI_FORMAT_R8G8_TYPELESS:
		case DXGI_FORMAT_R8G8_UNORM:
		case DXGI_FORMAT_R8G8_UINT:
		case DXGI_FORMAT_R8G8_SNORM:
		case DXGI_FORMAT_R8G8_SINT:
		case DXGI_FORMAT_R16_TYPELESS:
		case DXGI_FORMAT_R16_FLOAT:
		case DXGI_FORMAT_D16_UNORM:
		case DXGI_FORMAT_R16_UNORM:
		case DXGI_FORMAT_R16_UINT:
		case DXGI_FORMAT_R16_SNORM:
		case DXGI_FORMAT_R16_SINT:
		case DXGI_FORMAT_B5G6R5_UNORM:
		case DXGI_FORMAT_B5G5R5A1_UNORM:
		case DXGI_FORMAT_A8P8:
		case DXGI_FORMAT_B4G4R4A4_UNORM:
			return 16;

		case DXGI_FORMAT_NV12:
		case DXGI_FORMAT_420_OPAQUE:
		case DXGI_FORMAT_NV11:
			return 12;

		case DXGI_FORMAT_R8_TYPELESS:
		case DXGI_FORMAT_R8_UNORM:
		case DXGI_FORMAT_R8_UINT:
		case DXGI_FORMAT_R8_SNORM:
		case DXGI_FORMAT_R8_SINT:
		case DXGI_FORMAT_A8_UNORM:
		case DXGI_FORMAT_AI44:
		case DXGI_FORMAT_IA44:
		case DXGI_FORMAT_P8:
			return 8;

		case DXGI_FORMAT_R1_UNORM:
			return 1;

		case DXGI_FORMAT_BC1_TYPELESS:
		case DXGI_FORMAT_BC1_UNORM:
		case DXGI_FORMAT_BC1_UNORM_SRGB:
		case DXGI_FORMAT_BC4_TYPELESS:
		case DXGI_FORMAT_BC4_UNORM:
		case DXGI_FORMAT_BC4_SNORM:
			return 4;

		case DXGI_FORMAT_BC2_TYPELESS:
		case DXGI_FORMAT_BC2_UNORM:
		case DXGI_FORMAT_BC2_UNORM_SRGB:
		case DXGI_FORMAT_BC3_TYPELESS:
		case DXGI_FORMAT_BC3_UNORM:
		case DXGI_FORMAT_BC3_UNORM_SRGB:
		case DXGI_FORMAT_BC5_TYPELESS:
		case DXGI_FORMAT_BC5_UNORM:
		case DXGI_FORMAT_BC5_SNORM:
		case DXGI_FORMAT_BC6H_TYPELESS:
		case DXGI_FORMAT_BC6H_UF16:
		case DXGI_FORMAT_BC6H_SF16:
		case DXGI_FORMAT_BC7_TYPELESS:
		case DXGI_FORMAT_BC7_UNORM:
		case DXGI_FORMAT_BC7_UNORM_SRGB:
			return 8;

		case DXGI_FORMAT_UNKNOWN:
		case DXGI_FORMAT_FORCE_UINT:
		case DXGI_FORMAT_P208:
		case DXGI_FORMAT_V208:
		case DXGI_FORMAT_V408:
			break;
		}

		return 0;
	}
}

namespace opal::DRAWX_DX11 {
	CREATE_TEXTURE_::CREATE_TEXTURE_( DXGI_FORMAT f, UINT w, UINT h ) :
		format( ( f == DXGI_FORMAT_UNKNOWN ) ? DXGI_FORMAT_R8G8B8A8_UNORM : f ),
		width(  w ),
		height( h )
	{}
	CREATE_TEXTURE_::CREATE_TEXTURE_( DXGI_FORMAT f ) :
		format( ( f == DXGI_FORMAT_UNKNOWN ) ? DXGI_FORMAT_R8G8B8A8_UNORM : f ),
		width(  0 ),
		height( 0 )
	{}
	CREATE_TEXTURE_::CREATE_TEXTURE_( UINT w, UINT h ) :
		format( DXGI_FORMAT_R8G8B8A8_UNORM ),
		width(  w ),
		height( h )
	{}
	CREATE_TEXTURE_::CREATE_TEXTURE_() :
		format( DXGI_FORMAT_R8G8B8A8_UNORM ),
		width(  0 ),
		height( 0 )
	{}
}

namespace opal::DRAWX_DX11 {
	CREATE_TEXTURE_RECT::CREATE_TEXTURE_RECT( float x, float y, float w, float h ) :
		x( x ),
		y( y ),
		w( w ),
		h( h )
	{}
	CREATE_TEXTURE_RECT::CREATE_TEXTURE_RECT( float w, float h ) :
		x( 0 ),
		y( 0 ),
		w( w ),
		h( h )
	{}
	CREATE_TEXTURE_RECT::CREATE_TEXTURE_RECT( const CREATE_TEXTURE_RECT& d ) :
		x( d.x ),
		y( d.y ),
		w( d.w ),
		h( d.h )
	{}
	CREATE_TEXTURE_RECT::CREATE_TEXTURE_RECT( const CREATE_TEXTURE_RECT* d ) :
		x( d ? d->x : 0 ),
		y( d ? d->y : 0 ),
		w( d ? d->w : 0 ),
		h( d ? d->h : 0 )
	{}
	CREATE_TEXTURE_RECT::CREATE_TEXTURE_RECT() :
		x( 0 ),
		y( 0 ),
		w( 0 ),
		h( 0 )
	{}
}

namespace opal::DRAWX_DX11 {
	CREATE_TEXTURE::CREATE_TEXTURE( const char* p, const void* b, size_t s, DXGI_FORMAT f, const void* d ) :
		CREATE_TEXTURE_( f ),
		name( p ),
		adrs( b ),
		size( s ),
		rect( ( const CREATE_TEXTURE_RECT* )d )
	{}
	CREATE_TEXTURE::CREATE_TEXTURE( const char* p, const void* b, size_t s, const void* d ) :
		name( p ),
		adrs( b ),
		size( s ),
		rect( ( const CREATE_TEXTURE_RECT* )d )
	{}
	CREATE_TEXTURE::CREATE_TEXTURE( DXGI_FORMAT f, const void* d ) :
		CREATE_TEXTURE_( f ),
		name( nullptr ),
		adrs( nullptr ),
		size( 0L ),
		rect( ( const CREATE_TEXTURE_RECT* )d )
	{}
	CREATE_TEXTURE::CREATE_TEXTURE( const void* d ) :
		name( nullptr ),
		adrs( nullptr ),
		size( 0L ),
		rect( ( const CREATE_TEXTURE_RECT* )d )
	{}

	auto CreateTexture2D( ID3D11Texture2D*& texture, const CREATE_TEXTURE& data )->D3D11_TEXTURE2D_DESC{

        D3D11_TEXTURE2D_DESC	desc{};

		desc.Width				= data.width;
		desc.Height				= data.height;
		desc.MipLevels			= 1;
		desc.ArraySize			= 1;
		desc.Format				= data.format;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;
		desc.Usage				= D3D11_USAGE_DEFAULT;
		desc.BindFlags			= D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags		= 0;
		desc.MiscFlags			= 0;

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}
	auto CreateTexture2D( ID3D11Texture2D*& texture, DXGI_FORMAT f, UINT w, UINT h )->D3D11_TEXTURE2D_DESC{

		return CreateTexture2D( texture, CREATE_TEXTURE{f,w,h} );
	}
	auto CreateTexture2D( ID3D11Texture2D*& texture, UINT w, UINT h )->D3D11_TEXTURE2D_DESC{

		return CreateTexture2D( texture, CREATE_TEXTURE{DXGI_FORMAT_UNKNOWN,w,h} );
	}

	void DestroyTexture2D( ID3D11Texture2D*& texture ){ RELEASE( texture );	}
}

//----------------------------------------
// バッファー
//----------------------------------------
namespace opal::DRAWX_DX11 {
#define DEFAULT_FORMAT	DXGI_FORMAT_D24_UNORM_S8_UINT

	CREATE_BUFFER::CREATE_BUFFER( DXGI_FORMAT f, UINT w, UINT h ) :
		CREATE_TEXTURE_( ( f == DXGI_FORMAT_UNKNOWN ) ? DEFAULT_FORMAT : f, w, h )
	{}
	CREATE_BUFFER::CREATE_BUFFER( DXGI_FORMAT f ) :
		CREATE_TEXTURE_( ( f == DXGI_FORMAT_UNKNOWN ) ? DEFAULT_FORMAT : f )
	{}
	CREATE_BUFFER::CREATE_BUFFER( UINT w, UINT h ) :
		CREATE_TEXTURE_( DEFAULT_FORMAT, w, h )
	{}
	CREATE_BUFFER::CREATE_BUFFER() :
		CREATE_TEXTURE_( DEFAULT_FORMAT, 0, 0 )
	{}

	BUFFER_DESC::BUFFER_DESC( UINT w, UINT h, DXGI_FORMAT f ){

		Width              = w;
		Height             = h;
		MipLevels          = 1;
		ArraySize          = 1;
		Format             = f;
		SampleDesc.Count   = 1;
		SampleDesc.Quality = 0;
		Usage              = D3D11_USAGE_DEFAULT;
		BindFlags          = D3D11_BIND_DEPTH_STENCIL;
		CPUAccessFlags     = 0;
		MiscFlags          = 0;
	}
	BUFFER_DESC::BUFFER_DESC( CREATE_BUFFER d ) :
		BUFFER_DESC( d.width, d.height, d.format )
	{}

	auto CreateBuffer2D( ID3D11Texture2D*& texture, const CREATE_BUFFER& data )->D3D11_TEXTURE2D_DESC{

		const BUFFER_DESC	desc{data};

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}
	auto CreateBuffer2D( ID3D11Texture2D*& texture, ID3D11Texture2D* data )->D3D11_TEXTURE2D_DESC{

		return data ? CreateBuffer2D( texture, CREATE_BUFFER{Width(data),Height(data)} ) : D3D11_TEXTURE2D_DESC{};
	}
}

//----------------------------------------
// サーフェス
//----------------------------------------
namespace opal::DRAWX_DX11 {
	CREATE_SURFACE::CREATE_SURFACE( DXGI_FORMAT f, UINT w, UINT h, TYPE t ) :
		CREATE_TEXTURE_( f, w, h ),
		type( t )
	{}
	CREATE_SURFACE::CREATE_SURFACE( DXGI_FORMAT f, TYPE t ) :
		CREATE_TEXTURE_( f ),
		type( t )
	{}
	CREATE_SURFACE::CREATE_SURFACE( UINT w, UINT h, TYPE t ) :
		CREATE_TEXTURE_( w, h ),
		type( t )
	{}
	CREATE_SURFACE::CREATE_SURFACE( TYPE t ) :
		CREATE_TEXTURE_(),
		type( t )
	{}
	auto CreateSurface2D( ID3D11Texture2D*& texture, const CREATE_SURFACE& data )->D3D11_TEXTURE2D_DESC{

		switch ( data.type ) {
		case CREATE_SURFACE::RENDER: return CreateRender2D( texture, data );
		case CREATE_SURFACE::CANVAS: return CreateCanvas2D( texture, data );
		case CREATE_SURFACE::IDXGDI: return CreateIDXGDI2D( texture, data );
		default:break;
		}

		return D3D11_TEXTURE2D_DESC{};
	}

	RENDER_DESC::RENDER_DESC( UINT w, UINT h, DXGI_FORMAT f ){

		Width				= w;
		Height				= h;
		MipLevels			= 1;
		ArraySize			= 1;
		Format				= f;
		SampleDesc.Count	= 1;
		SampleDesc.Quality	= 0;
		Usage				= D3D11_USAGE_DEFAULT;
		BindFlags         	= D3D11_BIND_RENDER_TARGET|D3D11_BIND_SHADER_RESOURCE;
		CPUAccessFlags		= 0;
		MiscFlags			= 0;
	}
	RENDER_DESC::RENDER_DESC( CREATE_SURFACE d ) :
		RENDER_DESC( d.width, d.height, d.format )
	{}
	auto CreateRender2D( ID3D11Texture2D*& texture, const CREATE_SURFACE& data )->D3D11_TEXTURE2D_DESC{

		const RENDER_DESC	desc{data};

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}

	CANVAS_DESC::CANVAS_DESC( UINT w, UINT h ){

		Width				= w;
		Height				= h;
		MipLevels			= 1;
		ArraySize			= 1;
		Format				= DXGI_FORMAT_B8G8R8A8_UNORM;
		SampleDesc.Count	= 1;
		SampleDesc.Quality	= 0;
		Usage				= D3D11_USAGE_DYNAMIC;
		BindFlags          	= D3D11_BIND_SHADER_RESOURCE;
		CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
		MiscFlags			= 0;
	}
	CANVAS_DESC::CANVAS_DESC( CREATE_SURFACE d ) :
		CANVAS_DESC( d.width, d.height )
	{}
	auto CreateCanvas2D( ID3D11Texture2D*& texture, const CREATE_SURFACE& data )->D3D11_TEXTURE2D_DESC{

		const CANVAS_DESC	desc{data};

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}

	IDXGDI_DESC::IDXGDI_DESC( UINT w, UINT h ){

		Width				= w;
		Height				= h;
		MipLevels			= 1;
		ArraySize			= 1;
		Format				= DXGI_FORMAT_B8G8R8A8_UNORM;
		SampleDesc.Count	= 1;
		SampleDesc.Quality	= 0;
		Usage				= D3D11_USAGE_DEFAULT;
		BindFlags          	= D3D11_BIND_RENDER_TARGET|D3D11_BIND_SHADER_RESOURCE;
		CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
		MiscFlags			= D3D11_RESOURCE_MISC_GDI_COMPATIBLE;
	}
	IDXGDI_DESC::IDXGDI_DESC( CREATE_SURFACE d ) :
		IDXGDI_DESC( d.width, d.height )
	{}
	auto CreateIDXGDI2D( ID3D11Texture2D*& texture, const CREATE_SURFACE& data )->D3D11_TEXTURE2D_DESC{

		const IDXGDI_DESC	desc{data};

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}

	SHADER_DESC::SHADER_DESC( UINT w, UINT h ){

		Width				= w;
		Height				= h;
		MipLevels			= 1;
		ArraySize			= 1;
		Format				= DXGI_FORMAT_R8G8B8A8_UNORM;
		SampleDesc.Count	= 1;
		SampleDesc.Quality	= 0;
		Usage				= D3D11_USAGE_DYNAMIC;
		BindFlags			= D3D11_BIND_SHADER_RESOURCE;
		CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
		MiscFlags			= 0;
	}
	SHADER_DESC::SHADER_DESC( CREATE_SURFACE d ) :
		SHADER_DESC( d.width, d.height )
	{}
	auto CreateShader2D( ID3D11Texture2D*& texture, const CREATE_SURFACE& data )->D3D11_TEXTURE2D_DESC{

		const SHADER_DESC	desc{data};

		DRAWX_DX11::device()->CreateTexture2D( &desc, nullptr, &texture );

		return desc;
	}
}

//----------------------------------------
// ポリゴンデータ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	CREATE_POLYGON::CREATE_POLYGON( D3D11_PRIMITIVE_TOPOLOGY pt, const WORD* ip, size_t is, const void* vp, size_t vs ) :
		topology{  pt},
		indexlist{ ip},
		indexsize{ is},
		vertexlist{vp},
		vertexsize{vs}
	{}
	CREATE_POLYGON::CREATE_POLYGON( const WORD* ip, size_t is, const void* vp, size_t vs ) :
		CREATE_POLYGON( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, ip, is, vp, vs )
	{}
	CREATE_POLYGON::CREATE_POLYGON( const CREATE_POLYGON& b ) :
		topology{  b.topology  },
		indexlist{ b.indexlist },
		indexsize{ b.indexsize },
		vertexlist{b.vertexlist},
		vertexsize{b.vertexsize}
	{}
	CREATE_POLYGON::CREATE_POLYGON() :
		CREATE_POLYGON( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, nullptr, 0, nullptr, 0 )
	{}
}

//----------------------------------------
// ビュー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateShaderResourceView( ID3D11ShaderResourceView*& view, ID3D11Texture2D* texture )->D3D11_SHADER_RESOURCE_VIEW_DESC{

		D3D11_SHADER_RESOURCE_VIEW_DESC	desc{};

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	tdesc;

			texture->GetDesc( &tdesc );

			if  ( tdesc.BindFlags & D3D11_BIND_SHADER_RESOURCE ) {
				desc.Format						= tdesc.Format;
				desc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
				desc.Texture2D.MostDetailedMip	= 0;
				desc.Texture2D.MipLevels		= tdesc.MipLevels;

				DRAWX_DX11::device()->CreateShaderResourceView( texture, &desc, &view );
			}
		}
		return desc;
	}

	auto CreateRenderTargetView( ID3D11RenderTargetView*& view, ID3D11Texture2D* texture )->D3D11_RENDER_TARGET_VIEW_DESC{

		D3D11_RENDER_TARGET_VIEW_DESC	desc{};

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	tdesc{};

			texture->GetDesc( &tdesc );

			if ( tdesc.BindFlags & D3D11_BIND_RENDER_TARGET ) {
				desc.Format				= tdesc.Format;
				desc.ViewDimension		= D3D11_RTV_DIMENSION_TEXTURE2D;

				DRAWX_DX11::device()->CreateRenderTargetView( texture, &desc, &view );
			}
		}
		return desc;
	}

	auto CreateDepthStencilView( ID3D11DepthStencilView*& view, ID3D11Texture2D* texture )->D3D11_DEPTH_STENCIL_VIEW_DESC{

		D3D11_DEPTH_STENCIL_VIEW_DESC	desc{};

		if ( texture ) {
			D3D11_TEXTURE2D_DESC	tdesc{};

			texture->GetDesc( &tdesc );

			if ( tdesc.BindFlags & D3D11_BIND_DEPTH_STENCIL ) {
				desc.Format				= tdesc.Format;
				desc.ViewDimension		= D3D11_DSV_DIMENSION_TEXTURE2D;
				desc.Texture2D.MipSlice	= 0;

				DRAWX_DX11::device()->CreateDepthStencilView( texture, &desc, &view );
			}
		}
		return desc;
	}

	void DestroyShaderResourceView( ID3D11ShaderResourceView*& view ){ RELEASE( view );	}
	void DestroyRenderTargetView(   ID3D11RenderTargetView*&   view ){ RELEASE( view );	}
	void DestroyDepthStencilView(   ID3D11DepthStencilView*&   view ){ RELEASE( view );	}
}

//----------------------------------------
// サンプラーステート( UNIQUE & MAX 4096 ! )
//----------------------------------------
namespace opal::DRAWX_DX11 {
	auto CreateSamplerState( ID3D11SamplerState*& sampler )->D3D11_SAMPLER_DESC{

		D3D11_SAMPLER_DESC	desc{};

        desc.Filter			= D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        desc.AddressU		= D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressV		= D3D11_TEXTURE_ADDRESS_WRAP;
        desc.AddressW		= D3D11_TEXTURE_ADDRESS_WRAP;
        desc.MipLODBias		= 0.0f;
        desc.MaxAnisotropy	= 1;
        desc.ComparisonFunc	= D3D11_COMPARISON_ALWAYS;
        desc.BorderColor[0]	= 0;
        desc.BorderColor[1]	= 0;
        desc.BorderColor[2]	= 0;
        desc.BorderColor[3]	= 0;
        desc.MinLOD			= 0;
        desc.MaxLOD			= D3D11_FLOAT32_MAX;

		DRAWX_DX11::device()->CreateSamplerState( &desc, &sampler );

		return desc;
	}
	void DestroySamplerState( ID3D11SamplerState*& sampler ){ RELEASE( sampler );	}
}

//----------------------------------------
// リソースビューのスロットのリセット
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void ResetResourceView( UINT s, UINT n ){

		const auto	context	= DRAWX_DX11::device( 0 );

		const auto	tp = new ID3D11RenderTargetView*  [n];
		const auto	rp = new ID3D11ShaderResourceView*[n];

		for ( auto i = 0UL; i < n; i++ ) {
			tp[i] = nullptr;
			rp[i] = nullptr;
		}

		context->OMSetRenderTargets( n, tp, nullptr );
		context->PSSetShaderResources( s, n, rp );

		delete [] tp;
		delete [] rp;
	}
}

// End Of File
