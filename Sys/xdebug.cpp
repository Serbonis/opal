//========================================
// OPAL Game Library
//========================================
#include "xdebug.hpp"

//========================================
// デバッグシステム
//========================================
using namespace opal;

//----------------------------------------
// ベースAPI
//----------------------------------------
#if defined ( OPAL_DX11 )
#define _DEBUGX_	DEBUGX_DX11
#else
#error "デバッグシステムが定義されていません"
#endif

//========================================
// グラフィックシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal::_DEBUGX_ {
	extern void SysInit( void );
	extern void SysExec( void );
	extern void SysFree( void );
}

namespace opal::DEBUGX_ {
	void SysInit( void ){ _DEBUGX_::SysInit();	}
	void SysExec( void ){ _DEBUGX_::SysExec();	}
	void SysFree( void ){ _DEBUGX_::SysFree();	}
}

// End Of File

