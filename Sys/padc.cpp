//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADC::PADC() :
	joymax( 0 ),
	joypad( nullptr )
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void PADC::Init( UINT p ){

	for ( auto i = 0UL; i < KEY_MAX;   i++ ) { keyboard[i] = 0; }
	for ( auto i = 0UL; i < MOUSE_MAX; i++ ) { mouse[i] = 0; 	}

	joypad = NEWA<JOYS>( joymax = p );
}

void PADC::Free( void ){ DELETEA( joypad );	}

//----------------------------------------
// いろいろ
//----------------------------------------
void PADC::SetCountKeyboard( UINT k, UINT n ){ keyboard[k]  = n; }
void PADC::AddCountKeyboard( UINT k, UINT n ){ keyboard[k] += n; }
UINT PADC::GetCountKeyboard( UINT k ) const { return keyboard[k]; }

void PADC::SetCountMouse( UINT c, UINT n ){ mouse[c]  = n; }
void PADC::AddCountMouse( UINT c, UINT n ){ mouse[c] += n; }
UINT PADC::GetCountMouse( UINT c ) const { return mouse[c]; }

void PADC::SetCountJoypad( UINT p, UINT b, UINT n ){ if ( p < joymax ) { joypad[p][b]  = n; } }
void PADC::AddCountJoypad( UINT p, UINT b, UINT n ){ if ( p < joymax ) { joypad[p][b] += n; } }
UINT PADC::GetCountJoypad( UINT p, UINT b ) const { return ( p < joymax ) ? joypad[p][b] : 0; }

//----------------------------------------
// 判定関数
//----------------------------------------
UINT PADC::KeyCount(   UINT k         ) const { return GetCountKeyboard( k ); }
UINT PADC::MouseCount( UINT c         ) const { return GetCountMouse( std::log2( c ) ); }
UINT PADC::JoyCount(   UINT p, UINT b ) const { return GetCountJoypad( p, std::log2( b ) ); }
UINT PADC::Joy1Count(  UINT b ) const { return JoyCount( JOY1, b ); }
UINT PADC::Joy2Count(  UINT b ) const { return JoyCount( JOY2, b ); }

UINT PADC::JKCount(  UINT p, UINT b, UINT k ) const {

	const auto	bc = JoyCount( p, b );
	const auto	kc = KeyCount( k );

	return ( bc >= kc ) ? bc : kc;
}
UINT PADC::J1KCount( UINT b, UINT k ) const { return JKCount( JOY1, b, k ); }
UINT PADC::J2KCount( UINT b, UINT k ) const { return JKCount( JOY2, b, k ); }

UINT PADC::JKCount(  UINT p, const PADA* a ) const { return ( a ) ? JKCount( p, a->GetJoypad(), a->GetKeyboard() ) : 0; }
UINT PADC::J1KCount( const PADA* a ) const { return JKCount( JOY1, a ); }
UINT PADC::J2KCount( const PADA* a ) const { return JKCount( JOY2, a ); }

// End Of File
