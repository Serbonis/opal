//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルハンドル
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
FILEH::FILEH( const char* p ) :
	name( p ),
	flag( false ),
	buff( nullptr ),
	size( 0L ),
	seek( 0L )
{}
FILEH::~FILEH(){

	if ( flag ) {
		std::free( buff );
	}
}

//----------------------------------------
// 情報セット＆ゲット
//----------------------------------------
void FILEH::Name( const char* p ){ name.Name( p ); }
const char* FILEH::Name( void ) const { return name.Name(); }

void FILEH::Buff( void* b, size_t s, size_t h, size_t f ){

	if ( flag ) {
		std::free( buff );
	}

	if ( b && s ) {
		flag = false;
		buff = ( char* )b;
		size = s;
		head = h;
		foot = f;
		seek = 0L;
		return;
	}
	if ( b && !s ) {
		flag = false;
		buff = ( char* )b;
		size = ( UINT )-1;
		head = h;
		foot = f;
		seek = 0L;
		return;
	}
	if ( !b && s ) {
		flag = true;
		buff = ( char* )std::malloc( s + h + f );
		size = s;
		head = h;
		foot = f;
		seek = 0L;
		return;
	}
	if ( !b && !s ) {
		name = nullptr;
		flag = false;
		buff = nullptr;
		size = 0L;
		head = 0L;
		foot = 0L;
		seek = 0L;
		return;
	}
}

void*  FILEH::Buff( void ) const { return buff; }
size_t FILEH::Size( void ) const { return size;	}

void FILEH::Seek( UINT v ){ seek = v; }
UINT FILEH::Seek( void ) const { return seek; }

//----------------------------------------
// ハンドルプリント
//----------------------------------------
int FILEH::print( void ){

	::printf( "%s", ( char* )buff );

	return seek;
}

int FILEH::print( const char* format, ... ){

	if ( seek > size ){
		return 0;
	}

	va_list	list;

	va_start( list, format );
	::vsprintf( &buff[seek], format, list );
	va_end( list );

	return seek = strlen_s( buff );
}

void FILEH::flush( void ){

	memset_s( buff, 0, size );
	seek	= 0;
}

// End Of File
