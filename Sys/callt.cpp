//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// �T�E���h�^�X�N
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
CALLT::CALLT(){}
CALLT::~CALLT(){}

//----------------------------------------
// ����������n��
//----------------------------------------
void CALLT::Init( const char* p ){

	ZOBJ::Init( p );
	ZOBJ::Parent( CALLX::Parent() );

	CALLS::Init();
}

void CALLT::Free( void ){

	CALLS::Free();
	ZOBJ::Free();
}

void CALLT::Open_(  const std::string_view p, CLINX* l ){ ZOBJ::Open( p.data(), l );	}
void CALLT::Close_(                CLINX* l ){ ZOBJ::Close(   l );	}

void CALLT::Open( const std::string_view p, const char* ){ Init( p.data() ); Register( "" );	}

//----------------------------------------
// �o�^�����
//----------------------------------------
void CALLT::Register( LINX* l ){

	if ( l ) {
		ZOBJ::Register( l );
	} else if ( CALLX::linx ) {
		ZOBJ::Register( CALLX::linx );	// �f�t�H���g�o�^��
	} else {
		ZOBJ::Register( CALLX::index );	// �V�X�e���o�^��
	}
}

void CALLT::Release(  LINX* l ){

	if ( l ) {
		ZOBJ::Release( l );
	} else {
		ZOBJ::Release( Linx() );
	}
}

void CALLT::Register_( CLINX* l ){ CALLT::Register( ( LINX* )l   ); }
void CALLT::Release_(  CLINX* l ){ CALLT::Release(  ( LINX* )l   ); }

void CALLT::Register( const char* ){ ZOBJ::Register( CALLX::index ); }

//----------------------------------------
// ����
//----------------------------------------
auto CALLT::GetBranch( void ) const->CLINX*{ return dynamic_cast<CLINX*>( ZOBJ::Branch() );	}
void CALLT::SetBranch( CLINX* l ){ ZOBJ::Branch( l );	}

//----------------------------------------
// �o�^����
//----------------------------------------
CLINX* CALLT::CLinx( void ) const  { return dynamic_cast<CLINX*>( Linx() );	}
CLINX* CALLT::TLinx( const CALLT* ){ return dynamic_cast<CLINX*>( CALLX::linx ? CALLX::linx : CALLX::index ); }
LINX*  CALLT::TLinx( void ){ return CALLX::index; }

// End Of File
