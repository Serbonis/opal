//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

//========================================
// シグナルタスク
//========================================
using namespace opal;

//----------------------------------------
// シグナル登録
//----------------------------------------
void SIGX::Signal( SIGNAL s, const char* p, FUNC f, ARGV a ){

	if ( p ) {
		if ( f ) {
			signal[s].Set( p, f, a );
		} else {
			signal[s].Del( p );
		}
	} else {
		signal[s].Clear();
	}
}

void SIGX::Suspend( const char* p, FUNC f, ARGV a ){ Signal( SUSPEND, p, f, a );	}
void SIGX::Destroy( const char* p, FUNC f, ARGV a ){ Signal( DESTROY, p, f, a );	}
void SIGX::Reset(   const char* p, FUNC f, ARGV a ){ Signal( RESET,   p, f, a );	}
void SIGX::Pause(   const char* p, FUNC f, ARGV a ){ Signal( PAUSE,   p, f, a );	}
void SIGX::Exit(    const char* p, FUNC f, ARGV a ){ Reset( p, f, a ); Destroy( p, f, a ); 	}

void SIGX::Suspend( FUNC f, ARGV a ){ Suspend( "", f, a );	}
void SIGX::Destroy( FUNC f, ARGV a ){ Destroy( "", f, a );	}
void SIGX::Reset(   FUNC f, ARGV a ){ Reset(   "", f, a );	}
void SIGX::Pause(   FUNC f, ARGV a ){ Pause(   "", f, a );	}
void SIGX::Exit(    FUNC f, ARGV a ){ Exit(    "", f, a );	}

//----------------------------------------
// シグナル送出
//----------------------------------------
void SIGX::Kill( SIGNAL s, const char* p ){

	signal[s].Kill( p );

	if ( s == PAUSE && p == nullptr ) {
		count++;
	}
}

//----------------------------------------
// シグナルレベルとステータス
//----------------------------------------
void SIGX::Level( SIGNAL s, int l ){ signal[s].level = l;				}
void SIGX::Stat(  SIGNAL s, int f ){ FLAG_CTRL( signal[s].stat, f );	}

int  SIGX::Level( SIGNAL s ){ return signal[s].level;	}
bool SIGX::Stat(  SIGNAL s ){ return signal[s].stat;	}

//----------------------------------------
// ウィンドウクローズ
//----------------------------------------
void SIGX::Destroy( int f ){ Stat( DESTROY, f ); }
bool SIGX::Destroy( void ){ return Stat( DESTROY ); }

//----------------------------------------
// ポーズ＆リセット
//----------------------------------------
void SIGX::ResetLevel( UINT l ){ Level( RESET, ( int )l ); }
void SIGX::PauseLevel( UINT l ){ Level( PAUSE, ( int )l ); }

UINT SIGX::ResetLevel( void ){ return ( UINT )Level( RESET ); }
UINT SIGX::PauseLevel( void ){ return ( UINT )Level( PAUSE ); }

bool SIGX::Reset( void ){ return Stat( RESET ); }
bool SIGX::Pause( void ){ return Stat( PAUSE ); }
void SIGX::Reset( int f ){ Stat( RESET, f ); }
void SIGX::Pause( int f ){

	const auto	s = Stat( PAUSE );

	Stat( PAUSE, f );

	const auto	t = Stat( PAUSE );

	if ( !s && t  ) { resume =  -1;	count = 0; return;	}
	if (  s && !t ) { resume =  +1;	Kill( PAUSE );		}
}

bool SIGX::PauseCheck( UINT l ){ return Pause() && ( PauseLevel() > l );	}

int SIGX::Resume( void  ){ return resume; }
int SIGX::Resume( int n ){ return ( n ) ? n - count : count;	}

//----------------------------------------
// ブレーク
//----------------------------------------
void SIGX::Break( const char* a ){
#ifdef NOEXCEPT
	try {
		[]( const char* p ){ throw( p ); }( a );
	} catch ( const char* p ){
		printf( "Exception!\n" );
		( void )p;
	}
#endif
}

//----------------------------------------
// シグナルチェック＆送出
//----------------------------------------
bool SIGX::SysPause( void ) {

	if ( Pause() ) {
		Kill( SIGX::PAUSE );
		return true;
	}
	return false;
}

bool SIGX::SysReset( void ) {

	if ( Reset() ) {
		Kill( SIGX::RESET );
		return true;
	}
	return false;
}

bool SIGX::SysDestroy( void ) {

	if ( Destroy() ) {
		Kill( SIGX::DESTROY );
		return true;
	}
	return false;
}

// End Of File
