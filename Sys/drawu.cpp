//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックタスク
//========================================
using namespace opal;

//----------------------------------------
// TASK INFO
//----------------------------------------
void DRAWT::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	DrawInfo();
#endif
}

//----------------------------------------
// TASK INIT
//----------------------------------------
void DRAWT::TaskInit( void ){

	DrawInit();

	const auto	p = SIGX::PauseCheck( GetLevel() ) ? true : false;
	const auto	a = GetAnime( 1 ) ? true : false;
	const auto	f = GetFunc(  1 ) ? true : false;
	const auto	pt= ( DRAWS::state & DRAW_STATE_PAUSE ) ? true : false;
	const auto	at= ( DRAWS::state & DRAW_STATE_ANIME ) ? true : false;

	DRAWS::state = DRAW_STATE_NULL;

	if (  p      ) { DRAWS::state |= DRAW_STATE_PAUSE;		}
	if (  a      ) { DRAWS::state |= DRAW_STATE_ANIME;		}
	if (  p ^ pt ) { DRAWS::state |= DRAW_STATE_PAUSE_T;	}
	if (  a ^ at ) { DRAWS::state |= DRAW_STATE_ANIME_T;	}
	if ( !p && f ) { DRAWS::state |= DRAW_STATE_FUNC;		}

	if ( p ) {
		AddFlag( FLAG_P );
	} else {
		SubFlag( FLAG_P );
	}
}

//----------------------------------------
// TASK ENTER
//----------------------------------------
void DRAWT::TaskEnter( void ){

	DrawEnter();
}

//----------------------------------------
// TASK FUNC
//----------------------------------------
void DRAWT::TaskFunc( void ){

	if ( DRAWS::state & DRAW_STATE_PAUSE ) { return;	}
	if ( DRAWS::state & DRAW_STATE_FUNC  ) {
		Updater();
		OBJ::state &= ~OBJ_STATE_FUNC;
		DrawFunc();
	}

	{
		constexpr auto	fn = 0UL;
		constexpr auto	fa = DRAW_STATE_ANIME;
		constexpr auto	ft = DRAW_STATE_ANIME_T;
		constexpr auto	fx = fa|ft;

		switch ( DRAWS::state & fx ) {
		case fx: DrawAnime(+1);	[[fallthrough]];	// anime on  trigger
		case fa: DrawAnime(  );	break;				// anime on
		case ft: DrawAnime( 0);	[[fallthrough]];	// anime off trigger
		case fn: DrawAnime(-1);	break;				// anime off
		}
	}
}

//----------------------------------------
// TASK BEGIN
//----------------------------------------
void DRAWT::TaskBegin( void ){

	OBJT::Coordinate();
	DRAWS::Coordinate( LinxTask( this ) );

	DRAWS::Viewport( LinxTask( this ) );

	if ( GetDisp( 1 ) ) {
		DRAWS::state |= DRAW_STATE_MAIN;
		DrawBegin();
	}
}

//----------------------------------------
// TASK MAIN
//----------------------------------------
void DRAWT::particle( void ){

	if ( [&]{
			 switch ( GetPart() ) {
			 case PARTICLE	: SOBJ::CalcurateP( Vinv() );	return true;
			 case BILLBOARD	: SOBJ::CalcurateB( Bill() );	return true;
			 }
			 return false;
		 }() ) {
		SOBJ::Dirty( false );
		POBJ::Coordinate( ( const SUPERT* )nullptr );
		OOBJ::Coordinate( ( const SUPERT* )nullptr );
	}
}

void DRAWT::TaskMain(  void ){

	if ( DRAWS::state & DRAW_STATE_MAIN ) {
		if ( GetDim3() ) {
			particle();
		}
		DrawMain();
	}
}

void DRAWT::TaskMain( UINT ){

	if ( DRAWS::state & DRAW_STATE_MAIN ) {
		if ( GetDim3() ) {
			particle();
		}
		Coordinate( nullptr );	// 座標再計算
		DrawMain();
	}
}

//----------------------------------------
// TASK END
//----------------------------------------
void DRAWT::TaskEnd(  void ){

	if ( DRAWS::state & DRAW_STATE_MAIN ) {
		DrawEnd();
	}
}

//----------------------------------------
// TASK EXIT
//----------------------------------------
void DRAWT::TaskExit( void ){

	DrawExit();
}

//----------------------------------------
// TASK FREE
//----------------------------------------
void DRAWT::TaskFree( void ){

	DrawFree();
}

// End Of File
