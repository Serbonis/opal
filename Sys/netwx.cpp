//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
// ネットワークタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
NETWT::NETWT() :
	server(),				// サーバー情報
	fork( 0 ),				// 接続数
	size( SOMAXCONN )		// 最大数
{}
NETWT::~NETWT(){ free();	}

NETWT_S::NETWT_S(){}
NETWT_S::~NETWT_S(){ free(); }

NETWT_C::NETWT_C(){}
NETWT_C::~NETWT_C(){ free(); }

//----------------------------------------
// ホスト情報
//----------------------------------------
NETWT::HOST::HOST() :
	sock( INVALID_SOCKET ),	// ソケット
	adrs{},
	list( nullptr )
{}
NETWT::HOST::~HOST(){
	if ( list ) {
		::freeaddrinfo( list );
		list = nullptr;
	}
}

void NETWT::HOST::operator()( USHORT p, ULONG i ){

	adrs.sin_family			= AF_INET;
	adrs.sin_port			= ::htons( p );
	adrs.sin_addr.s_addr	= i;

#if 0
	ADDRINFO	hint;

	hint.ai_family   = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_protocol = IPPROTO_TCP;
	hint.ai_flags    = AI_PASSIVE;

	char	port[16];
	sprintf_s( port, sizeof( port ), "%d", p );

	::getaddrinfo( nullptr, port, &hint, &list );
#endif
}

//----------------------------------------
// 初期化
//----------------------------------------
void NETWT::init( void ){

	server	= HOST{};
	fork	= 0;
	size	= 0;
}

void NETWT_S::init( USHORT q, UINT s ){

	NETWT::init();

	server( q, ::htonl( INADDR_ANY ) );
	size = ( s > 0 ) ? s : SOMAXCONN;

	NEWS( client );
}

void NETWT_C::init( USHORT q, UINT s ){

	NETWT::init();

	server( q, ::inet_addr( "127.0.0.1" ) );	// とりあえず自分
	size = 1;
}

//----------------------------------------
// 後始末
//----------------------------------------
void NETWT::free( void ){}
void NETWT_S::free( void ){

	if ( client ) {
		for ( auto i = 0UL; i < client->size(); i++ ) {
			DELETES( (*client)[i] );
		}
		DELETES( client );
	}
}
void NETWT_C::free( void ){}

//----------------------------------------
// 終了処理
//----------------------------------------
void NETWT::kill( void ) {

	flag = false;

	switch ( info->kind ) {
	case NETWX::SERVER: NETWX::info_s.Clear( info->name );	break;
	case NETWX::CLIENT: NETWX::info_c.Clear( info->name );	break;
	}

	TASK::Kill( 0 );
}

void NETWT_S::kill( void ) {

	NETWT::kill();

	if ( server.sock != INVALID_SOCKET ) {
		::closesocket( server.sock );
	}

	if ( client ) {
		for ( auto i = 0UL; i < client->size(); i++ ) {
			if ( (*client)[i]->sock != INVALID_SOCKET ) {
				::closesocket( (*client)[i]->sock );
				DELETES( (*client)[i] );
			}
		}
		DELETES( client );
	}
}

void NETWT_C::kill( void ) {

	NETWT::kill();

	if ( server.sock != INVALID_SOCKET ) {
		::closesocket( server.sock );
	}
}

//----------------------------------------
// ソケット
//----------------------------------------
bool NETWT::Socket( void ){

	if ( ( server.sock = ::socket( AF_INET, SOCK_STREAM, 0 ) ) != INVALID_SOCKET ) {
		ULONG val = 1;

		::ioctlsocket( server.sock, FIONBIO, &val );
		return true;
	}
	printf( "SOCKET ERROR\n" );
	return false;
}

bool NETWT_S::Socket( void ){

	if ( NETWT::Socket() ) {
		if ( ::bind( server.sock, ( SOCKADDR* )&server.adrs, sizeof( server.adrs ) ) == SOCKET_ERROR ) {
			printf( "BIND ERROR\n" );
			return false;
		}

		if ( ::listen( server.sock, size ) == SOCKET_ERROR ) {
			printf( "LISTEN ERROR\n" );
			return false;
		}
	}
	return true;
}

bool NETWT_C::Socket( void ){

	return NETWT::Socket();
}

//----------------------------------------
// アクセプト＆コネクト
//----------------------------------------
bool NETWT_S::Attach( void ){

	if ( fork < size ) {
		HOST		host{};
		socklen_t	leng = sizeof( host.adrs );

		if ( ( host.sock = ::accept( server.sock, ( SOCKADDR* )&host.adrs, &leng ) ) != INVALID_SOCKET ) {
			ULONG val = 1;
			::ioctlsocket( host.sock, FIONBIO, &val );
			client->push_back( NEWS<HOST>( host ) );
			fork++;
		}
	}

	return fork ? true : false;
}

bool NETWT_C::Attach( void ){

	if ( fork < size ) {
		if ( ::connect( server.sock, ( SOCKADDR* )&server.adrs, sizeof( server.adrs ) ) != SOCKET_ERROR ) {
			fork++;
		}
	}

	return fork ? true : false;
}

//----------------------------------------
// ネットワークタスクの実行
//----------------------------------------
void NETWT_S::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%s ", ID() );
	printd( "[SERVER] %s:%d ",
			::inet_ntoa( server.adrs.sin_addr ),
			::ntohs(     server.adrs.sin_port ) );
	printd( "%d/%d ", fork, size );
	printd( "\n" );

	if ( client ) {
		for ( auto i = 0UL; i < client->size(); i++ ) {
			const auto	c = (*client)[i];

			printd( " [%d]%c %s:%d\n",
					i,
					( c->sock == INVALID_SOCKET ) ? '-' : '+',
					::inet_ntoa( c->adrs.sin_addr ),
					::ntohs(     c->adrs.sin_port ) );
		}
	}
#endif
}

void NETWT_C::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%s ", ID() );
	printd( "[CLIENT] %s:%d ",
			::inet_ntoa( server.adrs.sin_addr ),
			::ntohs(     server.adrs.sin_port ) );
	printd( "%c ", fork ? '+' : '-' );
	printd( "\n" );
#endif
}

void NETWT::TaskMain( void ) {

	switch ( step ) {
	case STEP_INIT:
		if ( !Socket() ) {
			step = STEP_NULL;
			break;
		}
		step = STEP_EXEC;
		[[fallthrough]];

	case STEP_EXEC:
		if ( Attach() ) {
			;
		}

		if ( 1 ) {
			break;
		}
		step = STEP_FREE;
		break;

	case STEP_FREE:
		kill();
		step = STEP_NULL;
		break;
	}
}

// End Of Netw
