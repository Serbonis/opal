//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ表示モード
//----------------------------------------
#ifdef OPAL_DEBUG
void CALLX::Debug( int m ){ debug = m;		}
int  CALLX::Debug( void  ){ return debug;	}
#endif

//----------------------------------------
// デフォルトリンク
//----------------------------------------
void  CALLX::Linx( LINX* l ){ linx =  l;	}
void  CALLX::Linx( LINX& l ){ linx = &l;	}
void  CALLX::Linx( std::shared_ptr<LINX> l ){ linx = l.get(); }
LINX* CALLX::Linx( void ){ return linx;	}

void  CALLX::Parent( ZOBJ* p ){ parent =  p;	}
void  CALLX::Parent( ZOBJ& p ){ parent = &p;	}
void  CALLX::Parent( std::shared_ptr<ZOBJ> p ){ parent = p.get(); }
ZOBJ* CALLX::Parent( void ){ return parent;	}

// End Of File
