//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスシステム
//========================================
using namespace opal;

//----------------------------------------
// 検索
//----------------------------------------
SRVC* SRVCX::Search( const char* p ){ return ( p && smap.count( p ) ) ? smap.at( p ) : nullptr;	}
SRVCX::SVAL* SRVCX::Search( void ){ return &sval;	}

//----------------------------------------
// クリア
//----------------------------------------
void SRVCX::Clear( const char* p ){

	if ( !p ) {
		skey.clear();
	} else {
		for ( auto k = skey.begin(); k != skey.end(); ++k ) {
			if ( *k == p ) {
				skey.erase( k );
				return;
			}
		}
	}
}

//----------------------------------------
// ソート関数
//----------------------------------------
void SRVCX::Sort( bool gt ){
#ifdef _MSC_VER
	struct SORT {
		static bool gt( const SRVC* l, const SRVC* r ){ return l->prio < r->prio; }
		static bool lt( const SRVC* l, const SRVC* r ){ return l->prio > r->prio; }
	};
	sort( sval.begin(), sval.end(), ( gt ) ? SORT::gt : SORT::lt );
#else
	sort( sval.begin(), sval.end(), ( gt ) ?
		  []( const SRVC* l, const SRVC* r ){ return l->prio < r->prio; } :
		  []( const SRVC* l, const SRVC* r ){ return l->prio > r->prio; } );
#endif
}

// End Of File
