//========================================
// OPAL Game Library
//========================================
#include "xpad.hpp"

#include <dinput.h>
#include <xinput.h>

//========================================
// 入力システム
//========================================
using namespace opal;

//========================================
// デバイス
//========================================
//----------------------------------------
// ワーク
//----------------------------------------
namespace {
	IDirectInput8*	input;
}

//----------------------------------------
// 初期化
//----------------------------------------
namespace opal::PADX_DX {
	void SysInit( void ){
#if 0
		XINPUT_CAPABILITIES cap[JOY_MAX] = {};

		for ( auto i = 0UL; i < JOY_MAX; i++ ) {
			switch ( const auto r = ::XInputGetCapabilities( i, XINPUT_FLAG_GAMEPAD, &cap[i] ) ) {
			case ERROR_SUCCESS					: printf( "[%ld] %s\n", i, "ERROR_SUCCESS" );				break;
			case ERROR_DEVICE_NOT_CONNECTED		: printf( "[%ld] %s\n", i, "ERROR_DEVICE_NOT_CONNETCTED" );	break;
			default								: printf( "[%ld] %s\n", i, HRES( r ) );						break;
			}
		}
#endif

		::DirectInput8Create( ::GetModuleHandle( nullptr ),
							  DIRECTINPUT_VERSION,
							  IID_IDirectInput8,
							  reinterpret_cast<void**>( &input ),
							  nullptr );
	}
}

//----------------------------------------
// 実行
//----------------------------------------
#define PADX_DEBUG	false

namespace {
	bool CheckDevice( const char* p, HRESULT hr ){
#if PADX_DEBUG
		switch ( hr ) {
		case DIERR_INPUTLOST		: printd( "CD DIERR_INPUTLOST      : %s\n", p );	break;
		case DIERR_INVALIDPARAM		: printd( "CD DIERR_INVALIDPARAM   : %s\n", p );	break;
		case DIERR_NOTACQUIRED		: printd( "CD DIERR_NOTACQUIRED    : %s\n", p );	break;
		case DIERR_NOTINITIALIZED	: printd( "CD DIERR_NOTINITIALIZED : %s\n", p );	break;
		case E_PENDING				: printd( "CD E_PENDING            : %s\n", p );	break;
		}
#endif
		return SUCCEEDED( hr );
	}

	bool CheckAquire( const char* p, HRESULT hr ){
#if PADX_DEBUG
		switch( hr ) {
		case DIERR_INVALIDPARAM		: printd( "CA DIERR_INVALIDPARAM   : %s\n", p );	break;
		case DIERR_NOTINITIALIZED	: printd( "CA DIERR_NOTINITIALIZED : %s\n", p );	break;
		case DIERR_OTHERAPPHASPRIO	: printd( "CA DIERR_OTHERAPPHASPRIO: %s\n", p );	break;
		}
#endif
		return SUCCEEDED( hr );
	}
}

namespace opal::PADX_DX {
	void SysExec( void ){
#if 0
		XINPUT_STATE 		state[JOY_MAX] = {};

		for ( auto i = 0UL; i < JOY_MAX; i++ ) {
			switch ( const auto r = ::XInputGetState( i, &state[i] ) ) {
			case ERROR_SUCCESS					: printd( "[%ld] %s\n", i, "ERROR_SUCCESS" );				break;
			case ERROR_DEVICE_NOT_CONNECTED		: printd( "[%ld] %s\n", i, "ERROR_DEVICE_NOT_CONNETCTED" );	break;
			default								: printd( "[%ld] %s\n", i, HRES( r ) );						break;
			}
		}
#endif
	}
}

//----------------------------------------
// 後始末
//----------------------------------------
namespace opal::PADX_DX {
	void SysFree( void ){

		RELEASE( input );
	}
}

//========================================
// keyboard
//========================================
//----------------------------------------
// ワーク
//----------------------------------------
namespace {
	IDirectInputDevice8*		keyboard;

	constexpr UINT	KEYBOARD_BUFFSIZE	= 8;			// キーボードバッファサイズ
}

//----------------------------------------
// 初期化
//----------------------------------------
namespace opal::PADX_DX {
	void SysInitKeyboard( void ){

		input->CreateDevice( GUID_SysKeyboard, &keyboard, nullptr );

		keyboard->SetDataFormat( &c_dfDIKeyboard );
		keyboard->SetCooperativeLevel( WINDOWX::Handle(), DISCL_FOREGROUND|DISCL_NONEXCLUSIVE|DISCL_NOWINKEY );
		{
			DIPROPDWORD	dipd = {};

			dipd.diph.dwSize       = sizeof( DIPROPDWORD  );
			dipd.diph.dwHeaderSize = sizeof( DIPROPHEADER );
			dipd.diph.dwObj        = 0;
			dipd.diph.dwHow        = DIPH_DEVICE;
			dipd.dwData            = KEYBOARD_BUFFSIZE;

			keyboard->SetProperty( DIPROP_BUFFERSIZE, &dipd.diph );
		}
		keyboard->Acquire();
	}
}

//----------------------------------------
// 実行
//----------------------------------------
namespace opal::PADX_DX {
	bool SysExecKeyboard( KEYBOARD& key ){

		if ( keyboard ) {
			if ( !CheckDevice( "keyboard", keyboard->GetDeviceState( KEY_MAX, key.stroke ) ) ){
				CheckAquire( "keyboard", keyboard->Acquire() );
				key.clear();
				return false;
			}
			return true;
		}

		return false;
	}
}

//----------------------------------------
// 後始末
//----------------------------------------
namespace opal::PADX_DX {
	void SysFreeKeyboard( void ){

		if ( keyboard ) {
			keyboard->Unacquire();
			keyboard = nullptr;
		}
	}
}

//========================================
// mouse
//========================================
//----------------------------------------
// ワーク
//----------------------------------------
namespace {
	IDirectInputDevice8*	mouse;
}

//----------------------------------------
// 初期化
//----------------------------------------
namespace opal::PADX_DX {
	void SysInitMouse( void ){

		input->CreateDevice( GUID_SysMouse, &mouse, nullptr );

		mouse->SetDataFormat( &c_dfDIMouse2 );
		mouse->SetCooperativeLevel( WINDOWX::Handle(), DISCL_FOREGROUND|DISCL_NONEXCLUSIVE );
		{
			DIPROPDWORD	dipd = {};

			dipd.diph.dwSize		= sizeof( DIPROPDWORD  );
			dipd.diph.dwHeaderSize	= sizeof( DIPROPHEADER );
			dipd.diph.dwObj			= 0;
			dipd.diph.dwHow			= DIPH_DEVICE;
			dipd.dwData				= DIPROPAXISMODE_REL;

			mouse->SetProperty( DIPROP_AXISMODE, &dipd.diph );
		}
		mouse->Acquire();
	}
}

//----------------------------------------
// 実行
//----------------------------------------
namespace opal::PADX_DX {
	bool SysExecMouse( MOUSE& mos ){

		if ( mouse ) {
			{
				const auto	handle		= WINDOWX::Handle();
				const auto	aspect_w	= WINDOWX::DisplayW()/WINDOWX::ScreenW();
				const auto	aspect_h	= WINDOWX::DisplayH()/WINDOWX::ScreenH();

				::GetCursorPos( &mos.point );
				::ScreenToClient( handle, &mos.point );

				mos.point.x *= aspect_w;
				mos.point.y *= aspect_h;
			}

			DIMOUSESTATE2 dims;

			if ( !CheckDevice( "mouse",	mouse->GetDeviceState( sizeof( DIMOUSESTATE2 ), &dims ) ) ) {
				CheckAquire( "mouse", mouse->Acquire() );
				mos.clear();
				return false;
			}

			mos.click = 0;
			for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
				if ( dims.rgbButtons[i] & 0x80 ) {
					mos.click |= ( 1 << i );
				}
			}

			mos.mx = dims.lX;
			mos.my = dims.lY;
			mos.mz = dims.lZ;

			return true;
		}

		return false;
	}
}

//----------------------------------------
// 後始末
//----------------------------------------
namespace opal::PADX_DX {
	void SysFreeMouse( void ){

		if ( mouse ) {
			mouse->Unacquire();
			mouse = nullptr;
		}
	}
}

//========================================
// joypad
//========================================
//----------------------------------------
// ワーク
//----------------------------------------
namespace {
	constexpr UINT	JOYSTICK_MAXRANGE	= 32767;		// ジョイスティック値幅
	constexpr UINT	JOYSTICK_DEADZONE	= 3000;			// ジョイスティック有効範囲	0(0%)〜10000(100%)

	UINT 					joy_max = 0;
	IDirectInputDevice8*	joy_pad[JOY_MAX];
}

//----------------------------------------
// 初期化
//----------------------------------------
namespace opal::PADX_DX {
	UINT SysJoypadMax( void ){ return joy_max;	}

	void SysInitJoypad( void ){

		struct Callback {
			static BOOL CALLBACK Object( LPCDIDEVICEOBJECTINSTANCE doi, bool* ref ){

				LPDIRECTINPUTDEVICE8	joy_pad = ( LPDIRECTINPUTDEVICE8 )( ref );

				DIPROPRANGE	dipr = {};

				dipr.diph.dwSize		= sizeof( DIPROPRANGE  );
				dipr.diph.dwHeaderSize	= sizeof( DIPROPHEADER );
				dipr.diph.dwObj			= doi->dwType;
				dipr.diph.dwHow			= DIPH_BYID;
				dipr.lMin				= -JOYSTICK_MAXRANGE;
				dipr.lMax				= +JOYSTICK_MAXRANGE;

				joy_pad->SetProperty( DIPROP_RANGE, &dipr.diph );

				DIPROPDWORD	dipd = {};

				dipd.diph.dwSize		= sizeof( dipd      );
				dipd.diph.dwHeaderSize	= sizeof( dipd.diph );
				dipd.diph.dwObj			= doi->dwType;
				dipd.diph.dwHow			= DIPH_BYID;
				dipd.dwData				= JOYSTICK_DEADZONE;

				joy_pad->SetProperty( DIPROP_DEADZONE, &dipd.diph );

				return DIENUM_CONTINUE;
			}

			static BOOL CALLBACK Device( LPDIDEVICEINSTANCE ddi, bool* ref ){

				if ( DI_OK != input->CreateDevice( ddi->guidInstance, &joy_pad[joy_max], nullptr ) ) {
					return DIENUM_CONTINUE;
				}

				joy_pad[joy_max]->SetDataFormat( &c_dfDIJoystick );
				joy_pad[joy_max]->SetCooperativeLevel( WINDOWX::Handle(), DISCL_FOREGROUND|DISCL_NONEXCLUSIVE );

				DIPROPDWORD	dipd = {};

				dipd.diph.dwSize		= sizeof( DIPROPDWORD  );
				dipd.diph.dwHeaderSize	= sizeof( DIPROPHEADER );
				dipd.diph.dwObj			= 0;
				dipd.diph.dwHow			= DIPH_DEVICE;
				dipd.dwData				= 16;

				joy_pad[joy_max]->SetProperty( DIPROP_BUFFERSIZE, &dipd.diph );
				joy_pad[joy_max]->EnumObjects( ( LPDIENUMDEVICEOBJECTSCALLBACK )Callback::Object, joy_pad[joy_max], DIDFT_AXIS );
				joy_pad[joy_max]->Acquire();

				if ( ++joy_max < JOY_MAX ) {
					return DIENUM_CONTINUE;
				}

				return DIENUM_STOP;
			}
		};
		joy_max = 0;
		input->EnumDevices( DI8DEVCLASS_GAMECTRL,( LPDIENUMDEVICESCALLBACK )Callback::Device, nullptr, DIEDFL_ATTACHEDONLY );
	}
}

//----------------------------------------
// 実行
//----------------------------------------
namespace opal::PADX_DX {
	bool SysExecJoypad( JOYPAD& joy, UINT n ){

		if ( const auto& jpad = joy_pad[n] ) {
			jpad->Poll();

			DIJOYSTATE	dijs;

			if ( !CheckDevice( "joy_pad", jpad->GetDeviceState( sizeof( DIJOYSTATE ), &dijs ) ) ) {
				CheckAquire( "joy_pad", jpad->Acquire() );
				joy.clear();
				return false;
			}

			joy.button = 0;
			for ( auto i = 0UL; i < PAD_MAX; i++ ) {
				if ( dijs.rgbButtons[i] & 0x80 ) {
					joy.button |= ( 1 << i );
				}
			}

			joy.lx = ( float )dijs.lX	/JOYSTICK_MAXRANGE;
			joy.ly = ( float )dijs.lY	/JOYSTICK_MAXRANGE;
			joy.rx = ( float )dijs.lZ	/JOYSTICK_MAXRANGE;
			joy.ry = ( float )dijs.lRz	/JOYSTICK_MAXRANGE;

			for ( auto i = 0UL; i < JOYPAD::POV_MAX; i++ ) {
				joy.pov[i] = dijs.rgdwPOV[i];
			}

			return true;
		}

		return false;
	}
}

//----------------------------------------
// 後始末
//----------------------------------------
namespace opal::PADX_DX {
	void SysFreeJoypad( void ){

	for ( auto i = 0UL; i < joy_max; i++ ) {
			if ( joy_pad[i] ) {
				joy_pad[i]->Unacquire();
				joy_pad[i] = nullptr;
			}
		}
	}
}

// End Of File
