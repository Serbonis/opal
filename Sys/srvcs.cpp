//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスシステム
//========================================
using namespace opal;

//----------------------------------------
// 検索
//----------------------------------------
SRVC* SRVCS::Search( const char* k ){ return SRVCX::Search( k ); }

//----------------------------------------
// 破棄
//----------------------------------------
UINT SRVCS::Destroy( const char* k ){

	if ( const auto s = SRVCX::Search( k ) ) {
		SRVCX::Clear( k );

		s->End();
		s->Clear();
		return 1;
	}
	return 0;
}

UINT SRVCS::Destroy( const std::string& s ){

	return Destroy( s.c_str() );
}

UINT SRVCS::Destroy( void ){

	if ( const auto v = SRVCX::Search() ) {
		for ( const auto s : *v ) {
			s->End();
			s->Clear();
		}
		SRVCX::Clear();
		return v->size();
	}
	return 0;
}

// End Of File
