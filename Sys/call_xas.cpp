//========================================
// OPAL Game Library
//========================================
#include "call_xa.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_XA;

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::CALLX_XA {
	DEVICE			device;
}

//----------------------------------------
// システム
//----------------------------------------
namespace opal::CALLX_XA {
	void SysInitDevice( void ){ device.Init();	}
	void SysFreeDevice( void ){	device.Free();	}
}

// End Of File
