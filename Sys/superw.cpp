//========================================
// OPAL Game Library
//========================================
#include "super.hpp"

//========================================
// スーパータスク
//========================================
using namespace opal;

//----------------------------------------
// 状態更新
//----------------------------------------
void SUPERT::TaskInit(  void ){ OBJT::TaskInit();	}
void SUPERT::TaskEnter( void ){ OBJT::TaskEnter();	}
void SUPERT::TaskFunc(  void ){ OBJT::TaskFunc();	}
void SUPERT::TaskBegin( void ){ OBJT::TaskBegin();	}
void SUPERT::TaskMain(  void ){ OBJT::TaskMain();	}
void SUPERT::TaskEnd(   void ){ OBJT::TaskEnd(); 	}
void SUPERT::TaskExit(  void ){ OBJT::TaskExit(); 	}
void SUPERT::TaskFree(  void ){ OBJT::TaskFree();	}

//----------------------------------------
// 実行セット
//----------------------------------------
void SUPERT::Exec( void ){

	TaskInit();
	TaskEnter();
	TaskFunc();
	Coordinate(   this );
	ReCoordinate( this );
	TaskBegin();
	TaskMain();
	TaskEnd();
	TaskExit();

	if ( Kill() ){
		TaskFree();
	}
}

// End Of File
