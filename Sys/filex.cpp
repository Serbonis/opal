//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
FILET::FILET() :
	hand( INVALID_HANDLE_VALUE ),	// ファイルハンドル
	ovlp(),							// 非同期読み書き用
	lock( nullptr ),				// 多重読み込み防止ロック
	buff( nullptr ),				// 読み込み領域確保フラグ
	func()							// 終了時コールバック関数
{}
FILET::~FILET(){

	std::free( buff );
}

//----------------------------------------
// ファイルタスクの実行
//----------------------------------------
void FILET::TaskMain( void ){

	const auto&	i_kind = info->kind;
	const auto&	i_name = info->name;
	auto&		i_adrs = info->adrs;
	auto&		i_size = info->size;
	auto&		i_code = info->code;
	auto&		i_mode = info->mode;
	auto&		i_errr = info->errr;

	switch ( step ) {
	case STEP_NULL:
		//----------------------------------------
		// 何もしない
		//----------------------------------------
		break;

	case STEP_INIT:
		//----------------------------------------
		// モードで振り分け
		//----------------------------------------
		switch ( i_mode ) {
		default: i_mode = FILEX::NORMAL; [[fallthrough]];
		case FILEX::NORMAL:
		case FILEX::MANAGE:
		case FILEX::SYSTEM:
		case FILEX::APPEND:
		case FILEX::BUFFER: step = STEP_OPEN;	break;
		case FILEX::CALLBK: step = STEP_FUNC;	break;
		case FILEX::SYNCRW:
			if ( !FILEX::info[i_kind].Head( this ) ) {
				break;
			}
		}
		break;

	case STEP_OPEN:
		//----------------------------------------
		// ファイルオープン
		//----------------------------------------
		{
			const NAME	file	= PathFile( OPAL::GAME_PATH(), i_name );
			DWORD		access	= 0;
			DWORD		share	= 0;
			DWORD		mode	= 0;
			DWORD		attr	= FILE_FLAG_OVERLAPPED;

			switch ( i_kind ) {
			case FILEI::TASK_READ:
				access 	= GENERIC_READ;
				share	= FILE_SHARE_READ;
				mode	= OPEN_EXISTING;
				attr	|= FILE_ATTRIBUTE_READONLY;
				break;

			case FILEI::TASK_WRITE:
				access	= GENERIC_WRITE;
				share	= FILE_SHARE_WRITE;
				mode	= OPEN_ALWAYS;
				break;

			default:
				i_errr = FILEI::ERROR_TASK;
				goto FILET_ERROR;
			}

			hand = ::CreateFile( file, access, share, nullptr, mode, attr, nullptr );

			if ( hand == INVALID_HANDLE_VALUE ) {
				i_errr = FILEI::ERROR_OPEN;
				i_code = ::GetLastError();
				goto FILET_ERROR;
			}
		}
		step = STEP_ADRS;
		break;

	case STEP_ADRS:
		//----------------------------------------
		// アドレスチェック
		//----------------------------------------
		switch ( i_kind ) {
		case FILEI::TASK_READ:
			i_size = GetFileSize( hand, nullptr );

			switch ( i_mode ) {
			case FILEX::NORMAL:break;
			case FILEX::MANAGE:break;
			case FILEX::SYSTEM:
				i_adrs = FILEX::adrs;
				FILEX::adrs = static_cast<INT64*>( i_adrs );
				FILEX::size = i_size;
				break;

			case FILEX::APPEND:
				i_adrs = FILEX::adrs + FILEX::size;
				FILEX::adrs = static_cast<INT64*>( i_adrs );
				FILEX::size = i_size;
				break;

			case FILEX::BUFFER:
				i_adrs = buff = ( char* )std::malloc( i_size );
				break;

			default:
				i_errr = FILEI::ERROR_MODE;
				goto FILET_ERROR;
			}
			break;

		case FILEI::TASK_WRITE:
			switch ( i_mode ) {
			case FILEX::NORMAL: break;
			case FILEX::MANAGE: break;
			case FILEX::SYSTEM:
				i_adrs = FILEX::adrs;
				FILEX::adrs = static_cast<INT64*>( i_adrs );
				FILEX::size = i_size;
				break;

			case FILEX::APPEND:
				i_adrs = FILEX::adrs + FILEX::size;
				FILEX::adrs = static_cast<INT64*>( i_adrs );
				FILEX::size = i_size;
				break;

			case FILEX::BUFFER:
				i_errr = FILEI::ERROR_MODE;
				goto FILET_ERROR;

			default:
				i_errr = FILEI::ERROR_MODE;
				goto FILET_ERROR;
			}
			break;

		default:
			i_errr = FILEI::ERROR_TASK;
			goto FILET_ERROR;
		}
		step = STEP_LOCK;
		break;

	case STEP_LOCK:
		//----------------------------------------
		// メモリロック
		//----------------------------------------
		{
			const auto	st = ( UINT64 )i_adrs;
			const auto	ed = ( UINT64 )i_adrs + i_size;

			if ( MEMORY::Locked( st, ed ) ) {
				break;
			}
			lock = MEMORY::Lock( st, ed, i_name );
		}
		step = STEP_EXEC;
		break;

	case STEP_EXEC:
		//----------------------------------------
		// 読み書き実行
		//----------------------------------------
		{
			auto		retv = true;

			switch ( i_kind ) {
			case FILEI::TASK_READ:	retv = ::ReadFileEx(  hand, i_adrs, i_size, &ovlp, nullptr );	break;
			case FILEI::TASK_WRITE:	retv = ::WriteFileEx( hand, i_adrs, i_size, &ovlp, nullptr );	break;
			default: i_errr = FILEI::ERROR_MODE;													goto FILET_ERROR;
			}

			if ( !retv ) {
				i_code = ::GetLastError();

				if ( i_code != ERROR_IO_PENDING  ) {
					MEMORY::UnLock( lock );
					i_errr = FILEI::ERROR_EXEC;
					step = STEP_OPEN;
					goto FILET_ERROR;
				}
			}
		}
		step = STEP_WAIT;
		break;

	case STEP_WAIT:
		//----------------------------------------
		// 終了待ち
		//----------------------------------------
		switch ( ::WaitForSingleObjectEx( hand, 0, false ) ) {
		case WAIT_OBJECT_0:			break;
		case WAIT_ABANDONED:		return;
		case WAIT_IO_COMPLETION:	return;
		case WAIT_TIMEOUT:			return;
		case -1:					break;
		}
		step = STEP_FUNC;
		break;

	case STEP_FUNC:
		//----------------------------------------
		// コールバック実行
		//----------------------------------------
		if ( func && func( info ) ) {
			break;
		}
		step = STEP_EXIT;
		break;

	case STEP_EXIT:
		//----------------------------------------
		// 終了処理
		//----------------------------------------
		info->time = TIME::Time() - info->time;
		kill();
		step = STEP_NULL;
		break;

	default:
		//----------------------------------------
		// 不明なステップ
		//----------------------------------------
		i_errr = FILEI::ERROR_STEP;
		goto FILET_ERROR;
	}

	return;

	//----------------------------------------
	// リトライ&エラー
	//----------------------------------------
 FILET_ERROR:
	if ( rmax-- ) {
#if OPAL_DEBUG
		printf( "%d %s\n", i_errr, ( const char* )i_name );
#endif
		i_errr = FILEI::ERROR_NULL;
	} else {
#ifdef OPAL_DEBUG
		info->Error( this );
#endif
		step = ( flag ) ? STEP_EXIT : STEP_NULL;
	}
}

//----------------------------------------
// kill
//----------------------------------------
void FILET::kill( void ){

	flag = false;

	{
		const auto	in = info->name;
		const auto	it = &FILEX::info[info->kind];

		it->Clear( in );

		{
			DELETES( FILEX::file );

			if ( const auto fi = it->Data( in ) ) {
				NEWS( FILEX::file, *fi );
			}
		}
	}

	{
		std::free( buff );
		::CloseHandle( hand );
		MEMORY::UnLock( lock );
	}

	TASK::Kill( 0 );
}

// End Of File
