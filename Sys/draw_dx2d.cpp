//========================================
// OPAL Game Library
//========================================
#include "draw_dx2d.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX2D;

//----------------------------------------
// デバイス
//----------------------------------------
DEVICE::DEVICE() :
	surface( nullptr ),
	render(  nullptr ),
	write(   nullptr )
{}
DEVICE::~DEVICE(){}

ID2D1RenderTarget*&	DEVICE::Render( void ){ return render;	}
IDWriteFactory*&	DEVICE::Write(  void ){ return write;	}

void DEVICE::Init( IDXGISwapChain*&	swapchain ){

	ID2D1Factory* factory;

    D2D1CreateFactory( D2D1_FACTORY_TYPE_SINGLE_THREADED, &factory );

    swapchain->GetBuffer( 0, IID_PPV_ARGS( &surface ) );

	float	dpiX;
	float	dpiY;

	factory->GetDesktopDpi( &dpiX, &dpiY );

	const D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties( D2D1_RENDER_TARGET_TYPE_DEFAULT,
									  D2D1::PixelFormat( DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED ),
									  dpiX, dpiY );

	factory->CreateDxgiSurfaceRenderTarget( surface, &props, &render );

	RELEASE( factory );

    DWriteCreateFactory( DWRITE_FACTORY_TYPE_SHARED, __uuidof( write ), reinterpret_cast<IUnknown**>( &write ) );
}

void DEVICE::Free( void ){

	RELEASE( write   );
	RELEASE( render  );
	RELEASE( surface );
}

void DEVICE::Begin( void ){ render->BeginDraw();	}
void DEVICE::End(   void ){ render->EndDraw();		}

// End Of File
