//========================================
// OPAL Game Library
//========================================
#include "heaps.hpp"

//========================================
// HEAP管理システム
//========================================
using namespace opal;

//----------------------------------------
// SYSTEM
//----------------------------------------
namespace {
	UINT heap_counter = 0;
}

namespace opal {
	HEAP_SYSTEM::HEAP_SYSTEM(){
		if ( !heap_counter++ ){
			;
		}
	}

	HEAP_SYSTEM::~HEAP_SYSTEM(){
		if ( !--heap_counter ){
			;
		}
	}

	UINT HEAP_SYSTEM::Counter( void ){ return heap_counter;	}
}

//----------------------------------------
// CORE
//----------------------------------------
HEAP_CORE::HEAP_CORE() :
	hash(),
	size(  0 ),
	count( 0 )
{}
HEAP_CORE::~HEAP_CORE(){}

void HEAP_CORE::Init( void ){

	hash.Init();
	size  = 0;
	count = 0;
}

void HEAP_CORE::Free( void ){

	hash.Free();
}

void HEAP_CORE::New( const void* p, const char* t, size_t s, size_t u ){

	hash.New( p, t, s, u );
	size += ( s * u );
	count++;
}

void HEAP_CORE::Del( const void* p ){

	if ( const auto node = hash.Node( p ) ) {
		count--;
		size -= ( node->val.size * node->val.unit );
		hash.Del( p );
	}
}

// End Of File
