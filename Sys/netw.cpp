//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
// ネットワークシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( NETWX::info_s   ){};
DECLVAL( NETWX::info_c   ){};
DECLVAL( NETWX::version  ){};
DECLVAL( NETWX::wsadata  ){};
DECLVAL( NETWX::hostinfo ){};
DECLVAL( NETWX::ipadrs   ){};

char	NETWX::hostname[HOSTNAME_SIZE]{};
char	NETWX::hostadrs[HOSTADRS_SIZE]{};

//----------------------------------------
// システム初期化
//----------------------------------------
bool NETWX::SysInit( void ){

	info_s.Init( "NETWORK SERVER" );
	info_c.Init( "NETWORK CLIENT" );

	version = MAKEWORD( WINSOCK_MAJOR_VER, WINSOCK_MINOR_VER );

	if ( const auto c = ::WSAStartup( NETWX::version, &wsadata ) ) {
		::WSAGetLastError();
#ifdef NOEXCEPT
		throw( "WSA CANNOT STARTUP" );
#else
		std::terminate();
#endif
	}

	for ( auto i = 0UL; i < HOSTNAME_SIZE; i++ ) { hostname[i] = 0;	}
	for ( auto i = 0UL; i < HOSTADRS_SIZE; i++ ) { hostadrs[i] = 0;	}

	::gethostname( NETWX::hostname, sizeof( *NETWX::hostname ) * NETWX::HOSTNAME_SIZE );
	hostinfo = ::gethostbyname( NETWX::hostname );
	memcpy_s( &ipadrs, hostinfo->h_addr, 4 );
	strcpy_s( NETWX::hostadrs, ::inet_ntoa( ipadrs ) );

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool NETWX::SysExec( void ){

	info_s.Exec();
	info_c.Exec();

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool NETWX::SysFree( void ){

	info_s.Free();
	info_c.Free();

	::WSACleanup();

	return true;
}

// End Of File
