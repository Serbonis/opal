//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

#include <d3d11.h>

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// スクリーンキャプチャ
//----------------------------------------
static void SaveFile( const char*, const BYTE*, UINT, UINT, UINT );

namespace opal::DRAWX_DX11 {
	void Capture( const char* pFile ){

		ID3D11Texture2D*		backbuff;

		device.SwapChain()->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( void** )&backbuff );

		D3D11_TEXTURE2D_DESC	backdesc;

		backbuff->GetDesc( &backdesc );
		backbuff->Release();

		D3D11_TEXTURE2D_DESC	textdesc;

		textdesc.ArraySize			= 1;
		textdesc.BindFlags			= 0;
		textdesc.CPUAccessFlags		= D3D11_CPU_ACCESS_READ;
		textdesc.Format				= backdesc.Format;
		textdesc.Height				= backdesc.Height;
		textdesc.Width				= backdesc.Width;
		textdesc.MipLevels			= 1;
		textdesc.MiscFlags			= 0;
		textdesc.SampleDesc.Count	= 1;
		textdesc.SampleDesc.Quality	= 0;
		textdesc.Usage				= D3D11_USAGE_STAGING;

		ID3D11Texture2D*		capture;

		device()->CreateTexture2D( &textdesc, 0, &capture );

		ID3D11Resource*			resource;

		device.Target()->GetResource( &resource );
		device(0)->CopyResource( capture, resource );
		resource->Release();

		D3D11_MAPPED_SUBRESOURCE	mapsubr;

		device(0)->Map( capture, 0, D3D11_MAP_READ, 0, &mapsubr );

		const auto	bw = backdesc.Width;
		const auto	bh = backdesc.Height;
		const auto	rp = mapsubr.RowPitch;
		const auto	sz = rp * bh;
		const auto	bp = new BYTE[sz];

		::CopyMemory( bp, mapsubr.pData, sz );

		device(0)->Unmap( capture, 0 );
		capture->Release();

		SaveFile( pFile, bp, rp, bw, bh );
		delete [] bp;
	}
}

namespace opal::DRAWX_ {
	const char* screenshot( char*, size_t, const char* );
}
using namespace opal::DRAWX_;

static void SaveFileBMP( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFileDDS( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFileDIB( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFileJPG( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFilePNG( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFilePPM( std::ofstream&, const BYTE*, UINT, UINT, UINT );
static void SaveFileTGA( std::ofstream&, const BYTE*, UINT, UINT, UINT );

static void SaveFile( const char* pFile, const BYTE* bp, UINT rp, UINT bw, UINT bh ){

	const std::unordered_map<std::string,decltype( &SaveFileBMP )>	savefile = {
		{ "bmp", SaveFileBMP	},
		{ "dds", SaveFileDDS	},
		{ "dib", SaveFileDIB	},
		{ "jpg", SaveFileJPG	},
		{ "png", SaveFilePNG	},
		{ "ppm", SaveFilePPM	},
		{ "tga", SaveFileTGA	},
	};

	char		buff[256] = { 0, };
	const auto	file = ( pFile ) ? pFile : screenshot( buff, sizeof( buff ), "bmp" );
	const auto	kind = GetFileKind( file );

	if ( savefile.count( kind ) ) {
		std::ofstream fout;

		fout.open( file, std::ios::out|std::ios::binary|std::ios::trunc );

		if ( !fout ) {
			return;
		}

		savefile.at( kind )( fout, bp, rp, bw, bh );
		fout.close();
	}
}

namespace {
	template<typename TYPE>void fwrite( std::ofstream& fout, TYPE v ){

		for ( auto i = 0UL; i < sizeof(TYPE); i++ ) {
			const char	c = ( v >> ( CHAR_BIT * i ) ) & 0xff;
			fout.write( &c, sizeof( char ) );
		}
	}
	inline void fwrite4( std::ofstream& fout, UINT   v ){ fwrite( fout, v );	}
	inline void fwrite2( std::ofstream& fout, USHORT v ){ fwrite( fout, v );	}
	inline void fwrite1( std::ofstream& fout, UCHAR  v ){ fwrite( fout, v );	}

	void fwrites( std::ofstream& fout, const char* p, size_t s = 0 ){

		fout.write( p, s ? s : std::strlen( p ) );
	}
}

static void SaveFileBMP( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){

	constexpr auto	BITMAPFILEHEADER = 14;
	constexpr auto	BITMAPINFOHEADER = 40;

	const auto	cbit = 32U;									// 色ビット
	const auto	cnum = ( cbit < 24 ) ? Pow2( cbit ) : 0;	// 色数
	const auto	head = BITMAPFILEHEADER + BITMAPINFOHEADER + cnum;
	const auto	body = ( ( bw * cbit ) * bh )/CHAR_BIT;

	// BITMAPFILEHEADER
	fwrites( fout, "BM" );				// ファイルタイプ
	fwrite4( fout, head + body );		// ファイルサイズ(byte)
	fwrite2( fout, 0 );					// 予約領域1
	fwrite2( fout, 0 );					// 予約領域2
	fwrite4( fout, head );				// ファイル先頭から画像データまでのオフセット(byte)

	// BITMAPINFOHEADER
	fwrite4( fout, BITMAPINFOHEADER );	// 情報ヘッダサイズ[byte]
	fwrite4( fout, bw );				// 画像の幅[ピクセル]
	fwrite4( fout, bh );				// 画像の高[ピクセル]
	fwrite2( fout, 1 );					// プレーン数(常に1)
	fwrite2( fout, cbit );				// 色ビット数[bit](1,4,8,(16),24,32)
	fwrite4( fout, 0 );					// 圧縮形式(0,1,2,3)
	fwrite4( fout, 0 );					// 画像データサイズ[byte]
	fwrite4( fout, 0 );					// 水平解像度[dot/m]
	fwrite4( fout, 0 );					// 垂直解像度[dot/m]
	fwrite4( fout, 0 );					// 格納パレット数[使用色数]
	fwrite4( fout, 0 );					// 重要色数

	// 画像データ
	for ( auto h = bh; h > 0; --h ) {
		const auto	p = ( const DWORD* )&bp[(h-1)*rp];
		for ( auto w = 0UL; w < bw; w++ ) {
			const auto	b = ( const BYTE* )&p[w];
			const auto	c = (b[3]<<24)|(b[0]<<16)|(b[1]<<8)|(b[2]<<0);

			fwrite4( fout, c );
		}
	}
}

static void SaveFileDDS( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}
static void SaveFileDIB( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}
static void SaveFileJPG( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}
static void SaveFilePNG( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}
static void SaveFilePPM( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}
static void SaveFileTGA( std::ofstream& fout, const BYTE* bp, UINT rp, UINT bw, UINT bh ){}

// End Of File
