//========================================
// OPAL Game Library
//========================================
#include "call_xa.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_XA;

//----------------------------------------
// スペック表示
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal::CALLX_XA {
	void DEVICE::Debug( void ) const {}
}

#endif

// End Of File
