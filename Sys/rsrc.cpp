//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"

//========================================
// リソース
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( RSRCX::rsrc ){};

//----------------------------------------
// システム初期化
//----------------------------------------
bool RSRCX::SysInit( void ){

	rsrc.clear();

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool RSRCX::SysExec( void ){

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool RSRCX::SysFree( void ){

	Release();

	return true;
}

// End Of File
