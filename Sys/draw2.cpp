//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックタスク(2D)
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWT2::DRAWT2(){}
DRAWT2::~DRAWT2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void DRAWT2::Init( const char* p ){

	DRAWT::Init( p );

	SetDim2( true  );
	SetPers( false );

	layer = 0.f;
}

void DRAWT2::Free( void ){

	DRAWT::Free();
}

//----------------------------------------
// レイヤー
//----------------------------------------
void  DRAWT2::SetLayer( float l ){

	layer = l;
}

float DRAWT2::GetLayer( void ) const {

	if ( layer < 0.f ) { return 0;	}
	if ( layer > 1.f ) { return 1;	}

	return layer;
}

//----------------------------------------
// Info
//----------------------------------------
void DRAWT2::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ",  "DRAWT2" );
	printd( "%-08s ", ID() );
	DRAWT2::DrawInfo( 0 );
	printd( "\n" );
#endif
}

//----------------------------------------
// Print
//----------------------------------------
void DRAWT2::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	printd( "%1.3f ", layer );
#endif
}
// End Of File

