//========================================
// OPAL Game Library
//========================================
#include "super.hpp"

//========================================
// スーパーシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( SUPERX::linx  ){};
DECLVAL( SUPERX::index ){};
DECLVAL( SUPERX::pmap  ){};
DECLVAL( SUPERX::cmap  ){};

//----------------------------------------
// システム初期化
//----------------------------------------
bool SUPERX::SysInit( void ){

	linx.clear();
	NEWS( linx["SUPERX"] );

	index = linx["SUPERX"];
	index->Init( "CLASS-SUPER" );

	pmap.clear();
	cmap.clear();

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool SUPERX::SysTaskInit(  void ){ return SUPERX::TaskInit(  index );	}
bool SUPERX::SysTaskEnter( void ){ return SUPERX::TaskEnter( index );	}
bool SUPERX::SysTaskMain(  void ){ return SUPERX::TaskMain(  index );	}
bool SUPERX::SysTaskExit(  void ){ return SUPERX::TaskExit(  index );	}
bool SUPERX::SysTaskFree(  void ){ return SUPERX::TaskFree(  index );	}

//----------------------------------------
// システム後始末
//----------------------------------------
bool SUPERX::SysFree( void ){

	cmap.clear();
	pmap.clear();

	if ( index ) {
		index->Free();
		DELETES( linx["SUPERX"] );
		linx.erase( "SUPERX" );
	}
	index = nullptr;

	for ( const auto& [k,v] : linx ) {
		printf( "SUPER-FREE %s %p\n", k.c_str(), v );
	}
	linx.clear();

	return true;
}

// End Of File
