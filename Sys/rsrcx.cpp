//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"

//========================================
// ���\�[�X
//========================================
using namespace opal;

//----------------------------------------
// �o�^���폜
//----------------------------------------
void RSRCX::Register( const std::string& s, std::shared_ptr<RSRC> p ){

	rsrc[s] = p;
}

void RSRCX::Release( const std::string& s ){

	if ( rsrc.count( s ) ) {
		rsrc.erase( s );
	}
}

void RSRCX::Release( void ){

	rsrc.clear();
}

//----------------------------------------
// ����
//----------------------------------------
auto RSRCX::Search( const std::string& s )->std::shared_ptr<RSRC>{

	return rsrc.count( s ) ? rsrc.at( s ) : nullptr;
}


//----------------------------------------
// �f�o�b�O
//----------------------------------------
#ifdef OPAL_DEBUG
void RSRCX::Debug( UINT m ){

	for ( const auto& [s,r] : rsrc ) {
		printd( "%s\n", s.c_str() );
	}
}
#endif

// End Of File
