//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( FILEX::info );
DECLVAL( FILEX::mode ){};
DECLVAL( FILEX::path ){};
DECLVAL( FILEX::list ){};
DECLVAL( FILEX::base ){};
DECLVAL( FILEX::adrs ){};
DECLVAL( FILEX::size ){};
DECLVAL( FILEX::file ){};

//----------------------------------------
// システム初期化
//----------------------------------------
bool FILEX::SysInit( void ){

	info[READ ].Init( "FILE READ"  );
	info[WRITE].Init( "FILE WRITE" );

	mode	= 0UL;
	path 	= OPAL::FILEPATH();
	base 	= OPAL::MAINMEMORY_ADRS<INT64>();
	adrs 	= base;
	size 	= 0L;
	file 	= nullptr;

	NEWS( list );

	Path( path );

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool FILEX::SysExec( void ){

	for ( auto i = 0UL; i < MAX; i++ ) {
		info[i].Exec();
	}

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool FILEX::SysFree( void ){

	if ( list ) {
		Flush();
		DELETES( list );
	}

	for ( auto i = 0UL; i < MAX; i++ ) {
		info[i].Free();
	}

	DELETES( file );

	return true;
}

// End Of File
