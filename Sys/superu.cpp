//========================================
// OPAL Game Library
//========================================
#include "super.hpp"
#include "Obj/oobj.hpp"

//========================================
// スーパータスク
//========================================
using namespace opal;

//----------------------------------------
// INIT
//----------------------------------------
bool SUPERX::TaskInit( LINX* index, UINT debug ){

	if ( index ) {
		index->TaskInit();
		return true;
	}

	return false;
}

//----------------------------------------
// ENTER
//----------------------------------------
bool SUPERX::TaskEnter( LINX* index, UINT debug ){

	if ( index ) {
		index->TaskEnter();
		return true;
	}

	return false;
}

//----------------------------------------
// MAIN
//----------------------------------------
bool SUPERX::TaskMain( LINX* index, UINT debug ){

	if ( index ) {
		index->TaskFunc();
		OOBJ::Coordinate( index, debug );
		index->TaskMain();
		return true;
	}

	return false;
}

//----------------------------------------
// EXIT
//----------------------------------------
bool SUPERX::TaskExit( LINX* index, UINT debug ){

	if ( index ) {
		index->TaskInfo();
		index->TaskExit();
		return true;
	}

	return false;
}

//----------------------------------------
// FREE
//----------------------------------------
bool SUPERX::TaskFree( LINX* index, UINT debug ){

	if ( index ) {
		index->TaskFree();
		return true;
	}

	return false;
}

// End Of File
