//========================================
// OPAL Game Library
//========================================
#include "call_xa.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_XA;

template<> const GUID& __mingw_uuidof<IXAudio2>(){

	static const GUID _iid = { 0x2B02E3CF, 0x2E0B, 0x4ec3, { 0xBE, 0x45, 0x1B, 0x2A, 0x3F, 0xE7, 0x21, 0x0D } };

	return _iid;
}

template<> const GUID& __mingw_uuidof<IXAudio2Extension>() {

	static const GUID _iid = { 0x84ac29bb, 0xd619, 0x44d2, { 0xb1, 0x97, 0xe4, 0xac, 0xf7, 0xdf, 0x3e, 0xd6 } };

	return _iid;
}

//----------------------------------------
// デバイス
//----------------------------------------
DEVICE::DEVICE() :
	device( nullptr ),
	voice(  nullptr )
{}
DEVICE::~DEVICE(){}

IXAudio2* DEVICE::operator()( void ){ return device;	}
DEVICE::operator IXAudio2*(   void ){ return device;	}

void DEVICE::Init( void ){

	const UINT32 flag = 0|XAUDIO2_DEBUG_ENGINE;

	::XAudio2Create( &device, flag );
	device->CreateMasteringVoice( &voice );
}

void DEVICE::Free( void ){

	if ( voice != 0 ) {
        voice->DestroyVoice();
        voice = nullptr;
    }
	RELEASE( device );
}

// End Of File
