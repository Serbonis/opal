//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"

//========================================
// プリミティブ
//========================================
using namespace opal;

//----------------------------------------
// 登録＆削除
//----------------------------------------
void PRIMX::Register( std::shared_ptr<TASK> p ){

	prim[p.get()] = p;
}

void PRIMX::Release( TASK* t ){

	if ( prim.count( t ) ) {
		if ( const auto f = t->Self() ) {
			f( t );
		}
		prim.erase( t );
	}
}

void PRIMX::Release( void ){

	for ( auto& [t,p] : prim ) {
		t->UnSelf();
	}
}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void PRIMX::Debug( UINT m ){

	printd( "%d\n", prim.size() );
}
#endif

// End Of File
