//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// パッドワーク
//----------------------------------------
void  PADX::SetData( const PADS* d ){ data[1] = ( d ) ? *d : PADS();	}
void  PADX::SetData( void ){ data[1] = data[0]; }
PADS* PADX::GetData( bool f ){ return &data[( f ) ? 0 : 1]; }

//----------------------------------------
// システム変数
//----------------------------------------
UINT PADX::JoyMax( void ){ return joymax; }

//----------------------------------------
// システムキー設定
//----------------------------------------
int  PADX::SysFunc( UINT n ){

	if ( n < SYSFUNC_MAX ) {
		if ( sysfunc[n] ) {
			return ( sysfunc[n] )();
		} else if ( sysflag[n] ) {
			return KeyPush( SysFkey( n ) );
		} else {
			return KeyTrig( SysFkey( n ) );
		}
	}
	return 0;
}

void PADX::SysFunc( UINT n, PADX::SYSFUNC f ){

	if ( n < SYSFUNC_MAX ) {
		sysfunc[n] = f;
	}
}

UINT PADX::SysFkey( UINT n ){

	return ( n < SYSFUNC_MAX ) ? sysfkey[n] : ( UINT )KEY_NULL;
}

void PADX::SysFkey( UINT n, UINT k, bool f ){

	if ( n < SYSFUNC_MAX ) {
		sysfkey[n] = k;
		sysflag[n] = f;
	}
}

int  PADX::SysHome(    void ){ return SysFunc( SYSFUNC_HOME    );	}
int  PADX::SysExit(    void ){ return SysFunc( SYSFUNC_EXIT    );	}
int  PADX::SysFull(    void ){ return SysFunc( SYSFUNC_FULL    );	}
int  PADX::SysReset(   void ){ return SysFunc( SYSFUNC_RESET   );	}
int  PADX::SysPause(   void ){ return SysFunc( SYSFUNC_PAUSE   );	}
int  PADX::SysBreak(   void ){ return SysFunc( SYSFUNC_BREAK   );	}
int  PADX::SysCapture( void ){ return SysFunc( SYSFUNC_CAPTURE );	}
int  PADX::SysFrame(   void ){ return SysFunc( SYSFUNC_FRAME   );	}
int  PADX::SysTimes(   void ){ return SysFunc( SYSFUNC_TIMES   );	}
int  PADX::SysTimec(   void ){ return SysFunc( SYSFUNC_TIMEC   );	}
int  PADX::SysRandom(  void ){ return SysFunc( SYSFUNC_RANDOM  );	}

int  PADX::SysTimes(  int n ){

	if ( n && SysTimes() ) {
		return SysTimec() ? -n : +n;
	}
	return 0;
}

void PADX::SysHome(    PADX::SYSFUNC f ){ SysFunc( SYSFUNC_HOME,    f );	}
void PADX::SysExit(    PADX::SYSFUNC f ){ SysFunc( SYSFUNC_EXIT,    f );	}
void PADX::SysFull(    PADX::SYSFUNC f ){ SysFunc( SYSFUNC_FULL,    f );	}
void PADX::SysReset(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_RESET,   f );	}
void PADX::SysPause(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_PAUSE,   f );	}
void PADX::SysBreak(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_BREAK,   f );	}
void PADX::SysCapture( PADX::SYSFUNC f ){ SysFunc( SYSFUNC_CAPTURE, f );	}
void PADX::SysFrame(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_FRAME,   f );	}
void PADX::SysTimes(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_TIMES,   f );	}
void PADX::SysTimec(   PADX::SYSFUNC f ){ SysFunc( SYSFUNC_TIMEC,   f );	}
void PADX::SysRandom(  PADX::SYSFUNC f ){ SysFunc( SYSFUNC_RANDOM,  f );	}

template<UINT KEY>static UINT DefaultKey( UINT key ){ return ( key < KEY_MAX ) ? key : KEY; }

void PADX::SetSysHome(   void ){ SysFkey( SYSFUNC_HOME,		DefaultKey<KEY_HOME  >( OPAL::PAD_HOME()    ), false );	}
void PADX::SetSysExit(   void ){ SysFkey( SYSFUNC_EXIT,		DefaultKey<KEY_SYSRQ >( OPAL::PAD_EXIT()    ), false );	}
void PADX::SetSysFull(   void ){ SysFkey( SYSFUNC_FULL,		DefaultKey<KEY_ESCAPE>( OPAL::PAD_FULL()    ), false );	}
void PADX::SetSysReset(  void ){ SysFkey( SYSFUNC_RESET,	DefaultKey<KEY_END   >( OPAL::PAD_RESET()   ), false );	}
void PADX::SetSysPause(  void ){ SysFkey( SYSFUNC_PAUSE,	DefaultKey<KEY_DELETE>( OPAL::PAD_PAUSE()   ), false );	}
void PADX::SetSysBreak(  void ){ SysFkey( SYSFUNC_BREAK,	DefaultKey<KEY_BREAK >( OPAL::PAD_BREAK()   ), false );	}
void PADX::SetSysCapture(void ){ SysFkey( SYSFUNC_CAPTURE,	DefaultKey<KEY_INSERT>( OPAL::PAD_CAPTURE() ), false );	}
void PADX::SetSysFrame(  void ){ SysFkey( SYSFUNC_FRAME,	DefaultKey<KEY_BACK  >( OPAL::PAD_FRAME()   ), false );	}
void PADX::SetSysTimes(  void ){ SysFkey( SYSFUNC_TIMES,	DefaultKey<KEY_ENTER >( OPAL::PAD_TIMES()   ), true  );	}
void PADX::SetSysTimec(  void ){ SysFkey( SYSFUNC_TIMEC,	DefaultKey<KEY_RSHIFT>( OPAL::PAD_TIMEC()   ), true  );	}
void PADX::SetSysRandom( void ){ SysFkey( SYSFUNC_RANDOM,	DefaultKey<KEY_LSHIFT>( OPAL::PAD_RANDOM()  ), true  );	}

void PADX::SetSysHome(    UINT k ) { SysFkey( SYSFUNC_HOME,    k, false ); }
void PADX::SetSysExit(    UINT k ) { SysFkey( SYSFUNC_EXIT,    k, false ); }
void PADX::SetSysFull(    UINT k ) { SysFkey( SYSFUNC_FULL,    k, false ); }
void PADX::SetSysReset(   UINT k ) { SysFkey( SYSFUNC_RESET,   k, false ); }
void PADX::SetSysPause(   UINT k ) { SysFkey( SYSFUNC_PAUSE,   k, false ); }
void PADX::SetSysBreak(   UINT k ) { SysFkey( SYSFUNC_BREAK,   k, false ); }
void PADX::SetSysCapture( UINT k ) { SysFkey( SYSFUNC_CAPTURE, k, false ); }
void PADX::SetSysFrame(   UINT k ) { SysFkey( SYSFUNC_FRAME,   k, false ); }
void PADX::SetSysTimes(   UINT k ) { SysFkey( SYSFUNC_TIMES,   k, true  ); }
void PADX::SetSysTimec(   UINT k ) { SysFkey( SYSFUNC_TIMEC,   k, true  ); }
void PADX::SetSysRandom(  UINT k ) { SysFkey( SYSFUNC_RANDOM,  k, true  ); }

int PADX::GetSysHome(    void ) { return SysFkey( SYSFUNC_HOME    ); }
int PADX::GetSysExit(    void ) { return SysFkey( SYSFUNC_EXIT    ); }
int PADX::GetSysFull(    void ) { return SysFkey( SYSFUNC_FULL    ); }
int PADX::GetSysReset(   void ) { return SysFkey( SYSFUNC_RESET   ); }
int PADX::GetSysPause(   void ) { return SysFkey( SYSFUNC_PAUSE   ); }
int PADX::GetSysBreak(   void ) { return SysFkey( SYSFUNC_BREAK   ); }
int PADX::GetSysCapture( void ) { return SysFkey( SYSFUNC_CAPTURE ); }
int PADX::GetSysFrame(   void ) { return SysFkey( SYSFUNC_FRAME   ); }
int PADX::GetSysTimes(   void ) { return SysFkey( SYSFUNC_TIMES   ); }
int PADX::GetSysTimec(   void ) { return SysFkey( SYSFUNC_TIMEC   ); }
int PADX::GetSysRandom(  void ) { return SysFkey( SYSFUNC_RANDOM  ); }

//----------------------------------------
// キー配列
//----------------------------------------
UCHAR* PADX::Keyboard( void ){

	if ( const auto k = work.GetDataKeyboard() ) {
		return k->stroke;
	}
	return nullptr;
}

//----------------------------------------
// 判定関数
//----------------------------------------
#define psc	data[1].Count()

int PADX::KeyCount(   UINT k         ){ return ( psc ) ? psc->KeyCount(   k    ) : 0; }
int PADX::MouseCount( UINT c         ){ return ( psc ) ? psc->MouseCount( c    ) : 0; }
int PADX::JoyCount(   UINT p, UINT b ){ return ( psc ) ? psc->JoyCount(   p, b ) : 0; }
int PADX::Joy1Count(          UINT b ){ return ( psc ) ? psc->Joy1Count(     b ) : 0; }
int PADX::Joy2Count(          UINT b ){ return ( psc ) ? psc->Joy2Count(     b ) : 0; }

int PADX::JKCount(  UINT p, UINT b, UINT k ){ return ( psc ) ? psc->JKCount(  p, b, k ) : 0; }
int PADX::J1KCount(         UINT b, UINT k ){ return ( psc ) ? psc->J1KCount(    b, k ) : 0; }
int PADX::J2KCount(         UINT b, UINT k ){ return ( psc ) ? psc->J2KCount(    b, k ) : 0; }

int PADX::JKCount(  UINT p, const PADA* a ){ return ( psc ) ? psc->JKCount(  p, a ) : 0; }
int PADX::J1KCount(         const PADA* a ){ return ( psc ) ? psc->J1KCount(    a ) : 0; }
int PADX::J2KCount(         const PADA* a ){ return ( psc ) ? psc->J2KCount(    a ) : 0; }

//----------------------------------------
// 判定関数
//----------------------------------------
#define psw	data[1].Work()

int PADX::KeyType( UINT t, UINT k, UINT s, UINT e ){

	switch ( t ) {
	case TYPE_NONE: return KeyNone( k, s, e );
	case TYPE_PUSH: return KeyPush( k, s, e );
	case TYPE_TRIG: return KeyTrig( k, s, e );
	case TYPE_RELS: return KeyRels( k, s, e );
	}
	return 0;
}

int PADX::KeyNone( UINT k, UINT s, UINT e ){ return 0;	}
int PADX::KeyPush( UINT k, UINT s, UINT e ){ return ( psw ) ? psw->KeyPush( k, s, e ) : 0; }
int PADX::KeyTrig( UINT k, UINT s, UINT e ){ return ( psw ) ? psw->KeyTrig( k, s, e ) : 0; }
int PADX::KeyRels( UINT k, UINT s, UINT e ){ return ( psw ) ? psw->KeyRels( k, s, e ) : 0; }

int PADX::JoyxPushO( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyPushO( p, b, s, e ) : 0; }
int PADX::JoyxTrigO( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyTrigO( p, b, s, e ) : 0; }
int PADX::JoyxRelsO( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyRelsO( p, b, s, e ) : 0; }
int PADX::JoyxPushA( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyPushA( p, b, s, e ) : 0; }
int PADX::JoyxTrigA( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyTrigA( p, b, s, e ) : 0; }
int PADX::JoyxRelsA( UINT p, UINT b, UINT s, UINT e ){ return ( psw ) ? psw->JoyRelsA( p, b, s, e ) : 0; }

float PADX::JoyxStick( UINT p, UINT s, UINT t ){ return ( psw ) ? psw->JoyStick( p, s, t ) : 0; }

int PADX::MousePush( UINT c, UINT s, UINT e ){ return ( psw ) ? psw->MousePush( c, s, e ) : 0; }
int PADX::MouseTrig( UINT c, UINT s, UINT e ){ return ( psw ) ? psw->MouseTrig( c, s, e ) : 0; }
int PADX::MouseRels( UINT c, UINT s, UINT e ){ return ( psw ) ? psw->MouseRels( c, s, e ) : 0; }

float PADX::Mouse(  UINT a, UINT t ){ return ( psw ) ? psw->Mouse( a, t ) : 0; }
float PADX::MouseX( UINT t ){ return Mouse( MOUSE_LC, t ); }
float PADX::MouseY( UINT t ){ return Mouse( MOUSE_RC, t ); }
float PADX::Scroll( UINT t ){ return Mouse( MOUSE_CC, t ); }
auto  PADX::Mouse(  UINT t )->std::pair<float,float>{

	if ( psw ) {
		return {psw->Mouse( MOUSE_LC, t ),psw->Mouse( MOUSE_RC, t )};
	}
	return {};
}

float PADX::Cursor(  UINT a, UINT t ){ return ( psw ) ? psw->Cursor( a, t ) : 0; }
float PADX::CursorX( UINT t ){ return Cursor( MOUSE_LC, t ); }
float PADX::CursorY( UINT t ){ return Cursor( MOUSE_RC, t ); }
auto  PADX::Cursor(  UINT t )->std::pair<float,float>{

	if ( psw ) {
		return {psw->Cursor( MOUSE_LC, t ),psw->Cursor( MOUSE_RC, t )};
	}
	return {};
}


//----------------------------------------
// 複合型 & (PAD1|PAD2)
//----------------------------------------
float PADX::Joy1Stick( UINT s, UINT t ){ return JoyxStick( JOY1, s, t ); }
float PADX::Joy2Stick( UINT s, UINT t ){ return JoyxStick( JOY2, s, t ); }

int PADX::Joy1PushO( UINT b, UINT s, UINT e ){ return JoyxPushO( JOY1, b, s, e ); }
int PADX::Joy2PushO( UINT b, UINT s, UINT e ){ return JoyxPushO( JOY2, b, s, e ); }
int PADX::Joy1TrigO( UINT b, UINT s, UINT e ){ return JoyxTrigO( JOY1, b, s, e ); }
int PADX::Joy2TrigO( UINT b, UINT s, UINT e ){ return JoyxTrigO( JOY2, b, s, e ); }
int PADX::Joy1RelsO( UINT b, UINT s, UINT e ){ return JoyxRelsO( JOY1, b, s, e ); }
int PADX::Joy2RelsO( UINT b, UINT s, UINT e ){ return JoyxRelsO( JOY2, b, s, e ); }
int PADX::Joy1PushA( UINT b, UINT s, UINT e ){ return JoyxPushA( JOY1, b, s, e ); }
int PADX::Joy2PushA( UINT b, UINT s, UINT e ){ return JoyxPushA( JOY2, b, s, e ); }
int PADX::Joy1TrigA( UINT b, UINT s, UINT e ){ return JoyxTrigA( JOY1, b, s, e ); }
int PADX::Joy2TrigA( UINT b, UINT s, UINT e ){ return JoyxTrigA( JOY2, b, s, e ); }
int PADX::Joy1RelsA( UINT b, UINT s, UINT e ){ return JoyxRelsA( JOY1, b, s, e ); }
int PADX::Joy2RelsA( UINT b, UINT s, UINT e ){ return JoyxRelsA( JOY2, b, s, e ); }

int PADX::JxKPushO( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxPushO( p, b, s, e ) || KeyPush( k, s, e ); }
int PADX::JxKTrigO( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxTrigO( p, b, s, e ) || KeyTrig( k, s, e ); }
int PADX::JxKRelsO( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxRelsO( p, b, s, e ) || KeyRels( k, s, e ); }
int PADX::JxKPushA( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxPushA( p, b, s, e ) || KeyPush( k, s, e ); }
int PADX::JxKTrigA( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxTrigA( p, b, s, e ) || KeyTrig( k, s, e ); }
int PADX::JxKRelsA( UINT p, UINT b, UINT k, UINT s, UINT e ){ return JoyxRelsA( p, b, s, e ) || KeyRels( k, s, e ); }

int PADX::J1KPushO( UINT b, UINT k, UINT s, UINT e ){ return JxKPushO( JOY1, b, k, s, e ); }
int PADX::J2KPushO( UINT b, UINT k, UINT s, UINT e ){ return JxKPushO( JOY2, b, k, s, e ); }
int PADX::J1KTrigO( UINT b, UINT k, UINT s, UINT e ){ return JxKTrigO( JOY1, b, k, s, e ); }
int PADX::J2KTrigO( UINT b, UINT k, UINT s, UINT e ){ return JxKTrigO( JOY2, b, k, s, e ); }
int PADX::J1KRelsO( UINT b, UINT k, UINT s, UINT e ){ return JxKRelsO( JOY1, b, k, s, e ); }
int PADX::J2KRelsO( UINT b, UINT k, UINT s, UINT e ){ return JxKRelsO( JOY2, b, k, s, e ); }
int PADX::J1KPushA( UINT b, UINT k, UINT s, UINT e ){ return JxKPushA( JOY1, b, k, s, e ); }
int PADX::J2KPushA( UINT b, UINT k, UINT s, UINT e ){ return JxKPushA( JOY2, b, k, s, e ); }
int PADX::J1KTrigA( UINT b, UINT k, UINT s, UINT e ){ return JxKTrigA( JOY1, b, k, s, e ); }
int PADX::J2KTrigA( UINT b, UINT k, UINT s, UINT e ){ return JxKTrigA( JOY2, b, k, s, e ); }
int PADX::J1KRelsA( UINT b, UINT k, UINT s, UINT e ){ return JxKRelsA( JOY1, b, k, s, e ); }
int PADX::J2KRelsA( UINT b, UINT k, UINT s, UINT e ){ return JxKRelsA( JOY2, b, k, s, e ); }

int PADX::JxKPushO( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKPushO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::JxKTrigO( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKTrigO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::JxKRelsO( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKRelsO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::JxKPushA( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKPushA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::JxKTrigA( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKTrigA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::JxKRelsA( UINT p, const PADA* a, UINT s, UINT e ){ return ( a ) ? JxKRelsA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }

int PADX::J1KPushO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KPushO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KPushO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KPushO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J1KTrigO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KTrigO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KTrigO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KTrigO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J1KRelsO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KRelsO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KRelsO( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KRelsO( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J1KPushA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KPushA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KPushA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KPushA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J1KTrigA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KTrigA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KTrigA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KTrigA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J1KRelsA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J1KRelsA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
int PADX::J2KRelsA( const PADA* a, UINT s, UINT e ){ return ( a ) ? J2KRelsA( a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }

//----------------------------------------
// デフォルト PAD1
//----------------------------------------
int PADX::JoyPushO( UINT b, UINT s, UINT e ){ return Joy1PushO( b, s, e ); }
int PADX::JoyTrigO( UINT b, UINT s, UINT e ){ return Joy1TrigO( b, s, e ); }
int PADX::JoyRelsO( UINT b, UINT s, UINT e ){ return Joy1RelsO( b, s, e ); }
int PADX::JoyPushA( UINT b, UINT s, UINT e ){ return Joy1PushA( b, s, e ); }
int PADX::JoyTrigA( UINT b, UINT s, UINT e ){ return Joy1TrigA( b, s, e ); }
int PADX::JoyRelsA( UINT b, UINT s, UINT e ){ return Joy1RelsA( b, s, e ); }

float PADX::JoyStick( UINT s, UINT t ){ return Joy1Stick( s, t ); }

int PADX::JKPushO( UINT b, UINT k, UINT s, UINT e ){ return J1KPushO( b, k, s, e ); }
int PADX::JKTrigO( UINT b, UINT k, UINT s, UINT e ){ return J1KTrigO( b, k, s, e ); }
int PADX::JKRelsO( UINT b, UINT k, UINT s, UINT e ){ return J1KRelsO( b, k, s, e ); }
int PADX::JKPushA( UINT b, UINT k, UINT s, UINT e ){ return J1KPushA( b, k, s, e ); }
int PADX::JKTrigA( UINT b, UINT k, UINT s, UINT e ){ return J1KTrigA( b, k, s, e ); }
int PADX::JKRelsA( UINT b, UINT k, UINT s, UINT e ){ return J1KRelsA( b, k, s, e ); }

int PADX::JKPushO( const PADA* p, UINT s, UINT e ){ return J1KPushO( p, s, e ); }
int PADX::JKTrigO( const PADA* p, UINT s, UINT e ){ return J1KTrigO( p, s, e ); }
int PADX::JKRelsO( const PADA* p, UINT s, UINT e ){ return J1KRelsO( p, s, e ); }
int PADX::JKPushA( const PADA* p, UINT s, UINT e ){ return J1KPushA( p, s, e ); }
int PADX::JKTrigA( const PADA* p, UINT s, UINT e ){ return J1KTrigA( p, s, e ); }
int PADX::JKRelsA( const PADA* p, UINT s, UINT e ){ return J1KRelsA( p, s, e ); }

// End Of File
