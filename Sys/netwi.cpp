//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
// ネットワークシステム
//========================================
using namespace opal;

//----------------------------------------
// ファイル情報
//----------------------------------------
#ifdef OPAL_DEBUG
static const char*	m[] = {
	"NORMAL",
};

void NETWI::Debug( void ) const {

	const char*	t = nullptr;
	const char*	e = nullptr;

	switch ( kind ) {
	case TASK_SERVER:	t = "S";	break;
	case TASK_CLIENT:	t = "C";	break;
	default:			t = "?";	break;
	}

	switch ( errr ) {
	default:			e = "-SUCCCESS-";	break;
	case ERROR_SOCKET:	e = "ERROR_SOCKET";	break;
	case ERROR_BIND:	e = "ERROR_BIND";	break;
	case ERROR_LISTEN:	e = "ERROR_LISTEN";	break;
	}

	printd( "%s %p %08d %s %s %s\n", t, adrs, size, m[mode], e, ( const char* )name );
}

void NETWI::Error( const NETWT* t ) const {

	printf( "NAME:%s\n", ( const char* )name );
	printf( "ADRS:%p\n", adrs );
	printf( "SIZE:%016x(%d)\n", ( int )size, ( int )size );
	printf( "ARGV:%016x(%d)\n", ( int )argv, ( int )argv );

	switch ( kind ) {
	case TASK_SERVER:	printf( "S/" );	break;
	case TASK_CLIENT:	printf( "C/" );	break;
	default:			printf( "?/" );	break;
	}

	switch ( errr ) {
	default:			printf( "想定外のエラーコード(%08x)\n", errr );	break;
	case ERROR_SOCKET:	printf( "SOCKET ERROR\n" );		break;
	case ERROR_BIND:	printf( "BIND ERROR\n"   );		break;
	case ERROR_LISTEN:	printf( "LISTEN ERROR\n" );		break;
	}
}
#endif

// End Of FILE
