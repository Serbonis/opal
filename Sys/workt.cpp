//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// �R�[�h�^�X�N
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
WORKT::WORKT(){}
WORKT::~WORKT(){}

//----------------------------------------
// ����������n��
//----------------------------------------
void WORKT::Init( const char* p ){

	ZOBJ::Init( p );
	ZOBJ::Parent( WORKX::Parent() );

	WORKS::Init();
}

void WORKT::Free( void ){

	WORKS::Free();
	ZOBJ::Free();
}

void WORKT::Open_(  const std::string_view p, WLINX* l ){ ZOBJ::Open( p.data(), l );	}
void WORKT::Close_(                WLINX* l ){ ZOBJ::Close(   l );	}

void WORKT::Open( const std::string_view p, const char* ){ Init( p.data() ); Register( "" );	}

//----------------------------------------
// �o�^�����
//----------------------------------------
void WORKT::Register( LINX* l ){

	if ( l ) {
		ZOBJ::Register( l );
	} else if ( WORKX::linx ) {
		ZOBJ::Register( WORKX::linx  );	// �f�t�H���g�o�^��
	} else {
		ZOBJ::Register( WORKX::index );	// �V�X�e���o�^��
	}
}

void WORKT::Release(  LINX* l ){

	if ( l ) {
		ZOBJ::Release( l );
	} else {
		ZOBJ::Release( Linx() );
	}
}

void WORKT::Register_( WLINX* l ){ WORKT::Register( ( LINX* )l );	}
void WORKT::Release_(  WLINX* l ){ WORKT::Release(  ( LINX* )l );	}

void WORKT::Register( const char* l ){ ZOBJ::Register( WORKX::index ); }

//----------------------------------------
// ����
//----------------------------------------
auto WORKT::GetBranch( void ) const->WLINX*{ return dynamic_cast<WLINX*>( ZOBJ::Branch() );	}
void WORKT::SetBranch( WLINX* l ){ ZOBJ::Branch( l );	}

//----------------------------------------
// �o�^����
//----------------------------------------
WLINX* WORKT::WLinx( void ) const  { return dynamic_cast<WLINX*>( Linx() );	}
WLINX* WORKT::TLinx( const WORKT* ){ return dynamic_cast<WLINX*>( WORKX::linx ? WORKX::linx : WORKX::index ); }
LINX*  WORKT::TLinx( void ){ return WORKX::index; }

// End Of File
