//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( PADX::joymax ){};
DECLVAL( PADX::buff   ){};
DECLVAL( PADX::work   ){};
DECLVAL( PADX::count  ){};
DECLVAL( PADX::data   );

UINT			PADX::sysfkey[PADX::SYSFUNC_MAX];
bool			PADX::sysflag[PADX::SYSFUNC_MAX];
PADX::SYSFUNC	PADX::sysfunc[PADX::SYSFUNC_MAX];

//----------------------------------------
// システム初期化
//----------------------------------------
bool PADX::SysInit( void ){

	//----------------------------------------
	// デバイス初期化
	//----------------------------------------
	PADX_::SysInit();
	PADX_::SysInitKeyboard();
	PADX_::SysInitMouse();
	PADX_::SysInitJoypad();

	joymax = PADX_::SysJoypadMax();

	//----------------------------------------
	// ワーク初期化
	//----------------------------------------
	NEWS( buff );

	work.Init( OPAL::PAD_WORK(), joymax );
	count.Init( joymax );

	for ( auto& dt : data ) {
		dt.Work(  &work  );
		dt.Count( &count );
	}

	//----------------------------------------
	// システム予約キー
	//----------------------------------------
	for ( auto i = 0; i < PADX::SYSFUNC_MAX; i++ ) {
		sysfkey[i] = KEY_NULL;
		sysflag[i] = false;
		sysfunc[i] = nullptr;
	}

	SetSysHome();
	SetSysExit();
	SetSysFull();
	SetSysReset();
	SetSysPause();
	SetSysBreak();
	SetSysCapture();
	SetSysFrame();
	SetSysTimes();
	SetSysTimec();
	SetSysRandom();

	SysHome(    nullptr );
	SysExit(    nullptr );
	SysFull(    nullptr );
	SysReset(   nullptr );
	SysPause(   nullptr );
	SysBreak(   nullptr );
	SysCapture( nullptr );
	SysFrame(   nullptr );
	SysTimes(   nullptr );
	SysTimec(   nullptr );
	SysRandom(  nullptr );

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool PADX::SysExec( void ){

	//----------------------------------------
	// デバイス実行
	//----------------------------------------
	PADX_::SysExec();

	//----------------------------------------
	// keyboard
	//----------------------------------------

	if ( const auto key = buff->Keyboard() ) {
		if ( PADX_::SysExecKeyboard( *key ) ) {
			key->stroke[KEY_FUNCTION] 	= key->Extension( KEY_FUNCTION );
			key->stroke[KEY_ALPHABET]	= key->Extension( KEY_ALPHABET );
			key->stroke[KEY_NUMBER]		= key->Extension( KEY_NUMBER );
			key->stroke[KEY_SHIFT]		= key->Extension( KEY_SHIFT );
			key->stroke[KEY_CONTROL]	= key->Extension( KEY_CONTROL );
			key->stroke[KEY_ALT]		= key->Extension( KEY_ALT );
			key->stroke[KEY_ARROW]		= key->Extension( KEY_ARROW );
			key->stroke[KEY_NUMPAD]		= key->Extension( KEY_NUMPAD );
			key->stroke[KEY_SUPER]		= key->Extension( KEY_SUPER );
			key->stroke[KEY_ANY]		= key->Extension( KEY_ANY );
		}
	}

	//----------------------------------------
	// mouse
	//----------------------------------------
	if ( const auto mos = buff->Mouse() ) {
		if ( PADX_::SysExecMouse( *mos ) ) {
			;
		}
	}

	//----------------------------------------
	// joypad
	//----------------------------------------
	for ( auto i = 0UL; i < joymax; i++ ) {
		if ( const auto joy = buff->Joypad( i ) ) {
			if ( PADX_::SysExecJoypad( joy[i], i ) ) {
				for ( auto j = 0UL; j < PAD_MAX; j++ ) {
					switch ( *joy->pov ) {
					case JOYPAD::POV( 0 ): joy->button |= (PAD_U );	break;
					case JOYPAD::POV( 1 ): joy->button |= (PAD_UR);	break;
					case JOYPAD::POV( 2 ): joy->button |= (PAD_R );	break;
					case JOYPAD::POV( 3 ): joy->button |= (PAD_DR);	break;
					case JOYPAD::POV( 4 ): joy->button |= (PAD_D );	break;
					case JOYPAD::POV( 5 ): joy->button |= (PAD_DL);	break;
					case JOYPAD::POV( 6 ): joy->button |= (PAD_L );	break;
					case JOYPAD::POV( 7 ): joy->button |= (PAD_UL);	break;
					}
				}
			}
		}
	}

	//----------------------------------------
	// カウンタ更新
	//----------------------------------------
	if ( const auto pc = data->Count() ) {
		if ( const auto key = buff->Keyboard() ) {
			for ( auto i = 0UL; i < KEY_MAX; i++ ) {
				if ( key->stroke[i] ) {
					pc->AddCountKeyboard( i, +1 );
				} else {
					pc->SetCountKeyboard( i,  0 );
				}
			}
		}

		if ( const auto mos = buff->Mouse() ) {
			for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
				if ( mos->click & ( 1 << i ) ) {
					pc->AddCountMouse( i, +1 );
				} else {
					pc->SetCountMouse( i,  0 );
				}
			}
		}

		for ( auto i = 0UL; i < joymax; i++ ) {
			if ( const auto joy = buff->Joypad( i ) ) {
				for ( auto j = 0UL; j < PAD_MAX; j++ ) {
					if ( joy->button & ( 1 << j ) ) {
						pc->AddCountJoypad( i, j, +1 );
					} else {
						pc->SetCountJoypad( i, j,  0 );
					}
				}
			}
		}
	}

	//----------------------------------------
	// ワーク更新
	//----------------------------------------
	work.SetData( buff );
	work.Next();

	buff->Clear();

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool PADX::SysFree( void ){

	//----------------------------------------
	// ワーク後始末
	//----------------------------------------
	count.Free();
	work.Free();

	DELETES( buff );

	//----------------------------------------
	// デバイス後始末
	//----------------------------------------
	PADX_::SysFreeKeyboard();
	PADX_::SysFreeMouse();
	PADX_::SysFreeJoypad();
	PADX_::SysFree();

	return true;
}

// End Of File
