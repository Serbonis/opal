//========================================
// OPAL Game Library
//========================================
#include "System.hpp"

#include "sys.hpp"

//========================================
// エンジンコアシステム
//========================================
using namespace opal;

#ifdef OPAL_DEBUG
#define DEBUG_MONITOR
#endif

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( SYSTEM::result ){};
DECLVAL( SYSTEM::reset  ){};
DECLVAL( SYSTEM::flag   ){};

namespace {
	enum PROCESS {
		STEP_NULL,
		STEP_INIT_INIT,
		STEP_INIT_EXEC,
		STEP_INIT_FREE,
		STEP_EXEC,
		STEP_FREE_INIT,
		STEP_FREE_EXEC,
		STEP_FREE_FREE,
	}		step{};

	bool	frame;
	UINT	rate;
	TIMES 	times;
	int		timec;
	bool	random;
}

//----------------------------------------
// MAIN
//----------------------------------------
UINT CALLBACK SYSTEM::process( void* ){

	do {
		if ( Active() ) {
			flag = Process();
		}
	} while ( flag );

	return 0;
}

namespace {
	void sysclear( void );
}

void SYSTEM::Main( void ){

	InitializeCriticalSection( OPAL::Lock() );

	TIME::Init();
	CoInitializeEx( nullptr, COINIT_APARTMENTTHREADED );

	SIGX::Destroy( false );

	result	= 0;
	reset	= 0UL;
	flag	= true;

	step	= STEP_INIT_INIT;
	frame	= false;
	rate	= 1;
	new ( &times ) TIMES;
	timec	= 2;
	random	= false;

	sysclear();

	{
		HANDLE		handle	= 0L;

		if ( OPAL::HANDLE() ) {
			handle = ( HANDLE )_beginthreadex( nullptr, 0, process, nullptr, 0, nullptr );
		}

		do {
			switch ( WINDOWX::Process() ) {
			case WINDOW::PROCESS_MESSAGE:
				if ( !DRAWX::DeviceLost() ) {
					break;
				}
				[[fallthrough]];

			case WINDOW::PROCESS_GAME:
				if ( ( handle && !flag ) || !Process() ) {
					goto QUIT;
				}
				break;

			case WINDOW::PROCESS_ERROR:
			case WINDOW::PROCESS_QUIT:
				SIGX::Destroy( true );
				while ( Process() ) {
					;
				}
				goto QUIT;
			}
		} while ( true );

	QUIT:
		CloseHandle( handle );
	}

	CoUninitialize();
	TIME::Free();

	DeleteCriticalSection( OPAL::Lock() );
}

//----------------------------------------
// Active
//----------------------------------------
bool SYSTEM::Active(  void ){

	if ( WINDOWX::Active() ) {
		const auto	active = OPAL::ACTIVE();

		if (  ( active & OPAL::ACTIVE_MINIMIZE  ) && ( WINDOWX::Sizing() == -1 ) ) { return true;	}
		if ( !( active & OPAL::ACTIVE_KILLFOCUS ) &&  !WINDOWX::Focus()          ) { return false;	}
		if ( !( active & OPAL::ACTIVE_MINIMIZE  ) && ( WINDOWX::Sizing() == -1 ) ) { return false;	}

		return true;
	}
	return false;
}

//----------------------------------------
// Result
//----------------------------------------
int  SYSTEM::Result( void  ){ return result;	}
void SYSTEM::Result( int v ){ result = v;		}

//----------------------------------------
// SysInit/SysExec/SysFree
//----------------------------------------
namespace {
	void sysinit( void );
	void sysexec( void );
	void sysfree( void );
}

int SYSTEM::SysInit( void ){

	result = 1;

	if ( const auto sysfunc = OPAL::SYSFUNC() ) {
		sysfunc->SysInit();
	} else if ( const auto sysinit = OPAL::SYSINIT() ) {
		sysinit();
	}

	sysinit();

	return result;
}

int SYSTEM::SysExec( void ){

	result = 0;

	if ( const auto sysfunc = OPAL::SYSFUNC() ) {
		sysfunc->SysExec();
	} else if ( const auto sysexec = OPAL::SYSEXEC() ) {
		sysexec();
	}

	sysexec();

	return result;
}

int SYSTEM::SysFree( void ){

	result = 1;

	if ( const auto sysfunc = OPAL::SYSFUNC() ) {
		sysfunc->SysFree();
	} else if ( const auto sysfree = OPAL::SYSFREE() ) {
		sysfree();
	}

	sysfree();

	return result;
}

//----------------------------------------
// Process
//----------------------------------------
bool SYSTEM::Process( void ){

	EnterCriticalSection( OPAL::Lock() );

	switch ( step ) {
	default:break;
	case STEP_INIT_INIT:
		Srand( random ? 0 : reset );

		TIME::SysInit();
		HEAP::SysInit();
		MEMORY::SysInit();
		SIGX::SysInit();

		PADX::SysInit();
		THRDX::SysInit();
		FILEX::SysInit();
		NETWX::SysInit();

		SRVCX::SysInit();

		OBJX::SysInit();
		{
			SUPERX::SysInit();
			WORKX::SysInit();
			DRAWX::SysInit();
			CALLX::SysInit();
		}

		RSRCX::SysInit();
		PRIMX::SysInit();

		step = STEP_INIT_EXEC;
		[[fallthrough]];

	case STEP_INIT_EXEC:
		if ( !SysInit() ) {
			break;
		}
		step = STEP_INIT_FREE;
		[[fallthrough]];

	case STEP_INIT_FREE:
#ifdef DEBUG_MONITOR
		DEBUGX::Window();
		DEBUGX::Begin();
#endif
		WINDOWX::Active( true );
		step = STEP_EXEC;
		[[fallthrough]];

	case STEP_EXEC:
		HEAP::SysExec();
		MEMORY::SysExec();

		PADX::SysExec();
#ifdef DEBUG_MONITOR
		DEBUGX::Monitor();
#endif
		THRDX::SysExec();
		FILEX::SysExec();
		NETWX::SysExec();

		if ( PADX::SysExit()    ) { SIGX::Destroy( true );	}
		if ( PADX::SysBreak()   ) { SIGX::Break();			}

		if ( !DRAWX::DeviceLost() ) {
			if ( PADX::SysHome()    ) { WINDOWX::Reset();				}
			if ( PADX::SysFull()    ) { WINDOWX::Screen( FLAG_FLIP );	}
			if ( PADX::SysReset()   ) {
				random = PADX::SysRandom();
				SIGX::Reset( FLAG_TRUE );
			}
			if ( PADX::SysCapture() ) { DRAWX::Capture();	}

			if ( frame ) {
				SIGX::Pause( !(frame = false ) );
			} else if ( PADX::SysFrame() ) {
				SIGX::Pause( !(frame = true  ) );
			} else if ( PADX::SysPause() ) {
				SIGX::Pause( FLAG_FLIP );
				frame = false;
			}

			if ( times( PADX::SysTimes( timec ) ) ) {
				const auto	t = times();

				for ( auto i = 0; i < t; i++ ) {
					SRVCX::SysExecBefore();
					{
						OBJX::SysExec();
						{
							SUPERX::SysTaskInit();
							WORKX::SysTaskInit();
							DRAWX::SysTaskInit();
							CALLX::SysTaskInit();
							{
								SUPERX::SysTaskEnter();
								WORKX::SysTaskEnter();
								DRAWX::SysTaskEnter();
								CALLX::SysTaskEnter();
								{
									if ( !SIGX::SysPause() ) {
										if ( SysExec() ) {
											SIGX::Destroy( true );
										}
									}
									SUPERX::SysTaskMain();
									WORKX::SysTaskMain();
									DRAWX::SysTaskMain();
									CALLX::SysTaskMain();
								}
								WORKX::SysTaskExit();
								DRAWX::SysTaskExit();
								CALLX::SysTaskExit();
								SUPERX::SysTaskExit();
							}
							WORKX::SysTaskFree();
							DRAWX::SysTaskFree();
							CALLX::SysTaskFree();
							SUPERX::SysTaskFree();
						}
						RSRCX::SysExec();
						PRIMX::SysExec();
					}
					SRVCX::SysExecAfter();
				}
			}
#ifdef DEBUG_MONITOR
			DEBUGX::Render();
#endif
			SIGX::SysExec();

			TIME::Sleep( rate );
		}
		DRAWX::Present();

		if ( !SIGX::SysReset() &&
			 !SIGX::SysDestroy() ) {
			TIME::SysExec();
			break;
		}
		step = STEP_FREE_INIT;
		[[fallthrough]];

	case STEP_FREE_INIT:
#ifdef DEBUG_MONITOR
		DEBUGX::End();
#endif
		step = STEP_FREE_EXEC;
		[[fallthrough]];

	case STEP_FREE_EXEC:
		if ( !SysFree() ) {
			break;
		}
		step = STEP_FREE_FREE;
		[[fallthrough]];

	case STEP_FREE_FREE:
		PRIMX::SysFree();
		RSRCX::SysFree();

		SRVCX::SysFree();
		{
			CALLX::SysFree();
			DRAWX::SysFree();
			WORKX::SysFree();
			SUPERX::SysFree();
		}
		OBJX::SysFree();

		NETWX::SysFree();
		FILEX::SysFree();
		THRDX::SysFree();
		PADX::SysFree();

		SIGX::SysFree();
		MEMORY::SysFree();
		HEAP::SysFree();
		TIME::SysFree();

		if ( SIGX::Destroy() == false ) {
			step = STEP_INIT_INIT;
			reset++;
			break;
		}
		step = STEP_NULL;
		break;
	}

	LeaveCriticalSection( OPAL::Lock() );

	return step ? true : false;
}

//----------------------------------------
// Suspend
//----------------------------------------
bool SYSTEM::Suspend( void ){

	switch ( WINDOWX::Message() ) {
	case WM_ENTERSIZEMOVE:	// サウンド停止
		return true;

	case WM_MOVING:
		SIGX::Kill( SIGX::SUSPEND, nullptr );
		return true;

	case WM_EXITSIZEMOVE:	// サウンド再開
		return true;
	}

	return false;
}

//----------------------------------------
// 情報取得
//----------------------------------------
UINT SYSTEM::Reset( void ){ return reset; }


//----------------------------------------
// システムワーク
//----------------------------------------
namespace {
	struct SYSFUNC final {
		SYSTEMX::sysinit	init{nullptr};
		SYSTEMX::sysexec	exec{nullptr};
		SYSTEMX::sysfree	free{nullptr};
		SYSTEMX::syswork	work{nullptr};

		void Init( void ) const { if ( init ) { init( work.get() ); } }
		void Exec( void ) const { if ( exec ) { exec( work.get() ); } }
		void Free( void ) const { if ( free ) { free( work.get() ); } }
	};

	std::unordered_map<std::string,SYSFUNC>	sysfunc;
}

namespace {
	void sysclear( void ){ sysfunc.clear(); }

	void sysinit( void ){ for ( const auto& [s,f] : sysfunc ) { f.Init(); } }
	void sysexec( void ){ for ( const auto& [s,f] : sysfunc ) { f.Exec(); } }
	void sysfree( void ){ for ( const auto& [s,f] : sysfunc ) { f.Free(); } }
}

void SYSTEMX::SysFunc( const std::string& s, sysinit fi, sysexec fe, sysfree ff, syswork fw ){

	sysfunc[s] = { fi, fe, ff, fw };
}

void SYSTEMX::SysFunc( const std::string& s ){

	if ( sysfunc.count( s ) ) {
		sysfunc.erase( s );
	}
}

void SYSTEMX::SysFuncInit( const std::string& s, sysinit fi ){ if ( sysfunc.count( s ) ) { sysfunc.at( s ).init = fi; }	}
void SYSTEMX::SysFuncExec( const std::string& s, sysexec fe ){ if ( sysfunc.count( s ) ) { sysfunc.at( s ).exec = fe; }	}
void SYSTEMX::SysFuncFree( const std::string& s, sysfree ff ){ if ( sysfunc.count( s ) ) { sysfunc.at( s ).free = ff; }	}
void SYSTEMX::SysFuncWork( const std::string& s, syswork fw ){ if ( sysfunc.count( s ) ) { sysfunc.at( s ).work = fw; }	}

auto SYSTEMX::SysFuncWork( const std::string& s )->void*{

	if ( sysfunc.count( s ) ) {
		return sysfunc.at( s ).work.get();
	}
	return nullptr;
}

// End Of File
