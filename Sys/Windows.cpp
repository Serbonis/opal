//========================================
// OPAL Game Library
//========================================
#include "window.hpp"

//========================================
// ウインドウクラス
//========================================
using namespace opal;

//----------------------------------------
// ウインドウの表示
//----------------------------------------
bool WINDOW::Active( void ) const { return active; }

void WINDOW::Active( int vFlag ){

	FLAG_CTRL( active, vFlag );

	::ShowWindow( handle, ( vFlag ) ? SW_SHOWNORMAL : SW_HIDE );
}

//----------------------------------------
// フォーカス
//----------------------------------------
bool WINDOW::Focus( void ) const { return focus; }

void WINDOW::Focus( int vFlag ){

	FLAG_CTRL( focus, vFlag );

	::SendMessage( handle, focus ? WM_SETFOCUS : WM_KILLFOCUS, 0,  0 );
}

//----------------------------------------
// ウインドウの位置
//----------------------------------------
bool WINDOW::Moving( void ) const { return moving; }

void WINDOW::Move( int X, int Y, int W, int H ){

	::MoveWindow( handle,
				  window.now.x = X,
				  window.now.y = Y,
				  window.now.w = W,
				  window.now.h = H,
				  true );
}

void WINDOW::Reset( void ) {

	Screen( false );
	Move( window.std.x,
		  window.std.y,
		  window.std.w,
		  window.std.h );
}

//----------------------------------------
// ウインドウの最大化/最小化
//----------------------------------------
int  WINDOW::Sizing( void ) const { return sizing; }

void WINDOW::Sizing( int S ){

	switch ( S ) {
	case  0: ::SendMessage( handle, WM_SYSCOMMAND, SC_RESTORE,  0 );	break;
	case +1: ::SendMessage( handle, WM_SYSCOMMAND, SC_MAXIMIZE, 0 );	break;
	case -1: ::SendMessage( handle, WM_SYSCOMMAND, SC_MINIMIZE, 0 );	break;
	default:return;
	}
	sizing = S;
}

//----------------------------------------
// ウインドウの情報
//----------------------------------------
void WINDOW::WindowX( int X ){ window.now.x = X; }
void WINDOW::WindowY( int Y ){ window.now.y = Y; }
void WINDOW::WindowW( int W ){ window.now.w = W; }
void WINDOW::WindowH( int H ){ window.now.h = H; }

int WINDOW::WindowX(  void ) const { return window.now.x; }
int WINDOW::WindowY(  void ) const { return window.now.y; }
int WINDOW::WindowW(  void ) const { return window.now.w; }
int WINDOW::WindowH(  void ) const { return window.now.h; }

int WINDOW::ScreenW(  void ) const { return screen.w; }
int WINDOW::ScreenH(  void ) const { return screen.h; }

int WINDOW::DisplayW( void ) const { return display.w; }
int WINDOW::DisplayH( void ) const { return display.h; }

//----------------------------------------
// フルスクリーン
//----------------------------------------
bool WINDOW::Screen( void ) const { return full; }

void WINDOW::Screen( int vFlag ) {

	FLAG_CTRL( full, vFlag );

	if ( full ) {
		::SetMenu( handle, nullptr );
		::SetWindowLongPtr( handle, GWL_STYLE, WS_VISIBLE|WS_POPUP );
		::ShowWindow( handle, SW_MAXIMIZE );
	} else {
		::SetMenu( handle, menu );
		::SetWindowLongPtr( handle, GWL_STYLE, WS_VISIBLE|WS_POPUP|WS_OVERLAPPEDWINDOW );
		Move( window.now.x,
			  window.now.y,
			  window.now.w,
			  window.now.h );
	}
}

//----------------------------------------
// カーソル
//----------------------------------------
bool WINDOW::Cursor( void ) const { return cursor.show; }

void WINDOW::Cursor( int vFlag ) {

	FLAG_CTRL( cursor.show, vFlag );

	if ( cursor.show ) {
		::ShowCursor( true );
	} else while ( ::ShowCursor( false ) > 0 ) {
		;
	}
}

//----------------------------------------
// ウィンドウイベント
//----------------------------------------
LRESULT CALLBACK WINDOW::Event( HWND hWnd, UINT msg, WPARAM wp, LPARAM lp ){

	auto&	wi = WINDOW::INSTANCE();

	switch ( wi.message = msg ) {
	case WM_CREATE:			wi.active = true;		break;
	case WM_DESTROY:		::PostQuitMessage( 0 );	return 0;

	case WM_SETFOCUS:		wi.focus = true;		break;
	case WM_KILLFOCUS:		wi.focus = false;		break;

	case WM_ENTERSIZEMOVE:	wi.moving = true;		break;
	case WM_EXITSIZEMOVE:	wi.moving = false;		break;
	case WM_MOVING:
		if ( const auto r = reinterpret_cast<RECT*>( lp ) ) {
			wi.WindowX( r->left );
			wi.WindowY( r->top  );
		}
		break;

	case WM_SIZING:
		if ( const auto r = reinterpret_cast<RECT*>( lp ) ) {
			wi.WindowX( r->left );
			wi.WindowY( r->top  );
			wi.WindowW( r->right  - r->left );
			wi.WindowH( r->bottom - r->top  );
		}
		break;

	case WM_SYSCOMMAND:
		switch ( wp & 0xfff0 ) {
		case SC_RESTORE	:wi.sizing =  0;	break;
		case SC_MINIMIZE:wi.sizing = -1;	break;
		case SC_MAXIMIZE:wi.sizing = +1;	break;
		case SC_MONITORPOWER:
		case SC_SCREENSAVE:
			return 0;
		}
		break;
	}

	return ::DefWindowProc( hWnd, msg, wp, lp );
}

// End Of File

