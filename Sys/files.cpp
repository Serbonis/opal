//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルシステム
//========================================
using namespace opal;

//----------------------------------------
// 動作モード
//----------------------------------------
void FILEX::Mode( UINT m ){ mode = m;	}
UINT FILEX::Mode( void ){ return mode;	}

//----------------------------------------
// デフォルトパス
//----------------------------------------
void FILEX::Path( const char* p ){

	if ( !p ) {
		path = OPAL::FILEPATH();
	}

	if ( p ) {
		std::string	s{p};

		for ( auto i = s.length()-1; s[i] == '/'; --i ) {
			s.pop_back();
		}
		s.push_back( '/' );

		path = s.c_str();
	}
}

void FILEX::Path( const std::string& p ){ Path( p.c_str() );	}
void FILEX::Path( std::nullptr_t ){ Path( ( const char* )nullptr );	}

const char* FILEX::Path( void ){ return path;	}

//----------------------------------------
// パスリスト
//----------------------------------------
void FILEX::Push( const char* p ){

	list->push( NEWS<NAME>( Path() ) );
	Path( p );
}

void FILEX::Pop( void ){

	if ( list->size() ) {
		Path( *list->top() );
		DELETES( list->top() );
		list->pop();
	}
}

void FILEX::Flush( void ){

	for ( auto i = list->size(); i; i-- ) {
		DELETES( list->top() );
		list->pop();
	}
	Path( nullptr );
}


//----------------------------------------
// ベースアドレスのセット
//----------------------------------------
void FILEX::Base( void* a ){

	FILEX::base = reinterpret_cast<INT64*>( ( a ) ? a : OPAL::MAINMEMORY_ADRS() );
	FILEX::adrs = FILEX::base;
	FILEX::size = 0L;
}

void* FILEX::Base( void ){ return FILEX::base;	}

//----------------------------------------
// タスクの数
//----------------------------------------
UINT FILEX::Size(  UINT n ){

	return info[n].Size();
}
UINT FILEX::SizeR( void ){ return Size( READ  ); }
UINT FILEX::SizeW( void ){ return Size( WRITE ); }

//----------------------------------------
// タスクの取得
//----------------------------------------
FILET* FILEX::Task( UINT n, const char* p ){

	return info[n].Task( p );
}

FILET* FILEX::TaskR( const char* p ){ return Task( READ,  p ); }
FILET* FILEX::TaskW( const char* p ){ return Task( WRITE, p ); }

//----------------------------------------
// タスクの削除
//----------------------------------------
int FILEX::Kill( UINT n, const char* p ){

	if ( const auto t = Task( n, p ) ) {
		info[n]().Release( t );
		t->kill();
		return 0;
	}
	return -1;
}
int FILEX::KillR( const char* p ) { return Kill( READ,  p ); }
int FILEX::KillW( const char* p ) { return Kill( WRITE, p ); }

//----------------------------------------
// タスクの待機
//----------------------------------------
int FILEX::Sync( UINT n, const char* p ){

	if ( !p ) {
		return Size( n );
	}

	if ( const auto t = Task( n, p ) ) {
		return t->flag;
	}

	return 0;
}
int FILEX::SyncR( const char* p ) { return Sync( READ,  p ); }
int FILEX::SyncW( const char* p ) { return Sync( WRITE, p ); }

//----------------------------------------
// タスクのタイムアウト
//----------------------------------------
int FILEX::Time( UINT n, const char* p ){

	if ( !p ) {
		return Size( n );
	}

	if ( const auto t = Task( n, p ) ) {
		return t->time;
	}

	return 0;
}
int FILEX::TimeR( const char* p ) { return Time( READ,  p ); }
int FILEX::TimeW( const char* p ) { return Time( WRITE, p ); }

//----------------------------------------
// ファイル情報の取得
//----------------------------------------
const FILEI* FILEX::File( const char* p ) {

	return ( p ) ? info[READ].Data( p ) : file;
}

void* FILEX::Adrs( const char* p ) {

	if ( p ) {
		if ( const auto f = File( p ) ) {
			return f->Adrs();
		}
		return nullptr;
	}
	return adrs;
}

UINT FILEX::Size( const char* p ) {

	if ( p ) {
		if ( const auto f = File( p ) ) {
			return f->Size();
		}
		return 0;
	}
	return size;
}

//----------------------------------------
// 読み込み情報クリア
//----------------------------------------
void FILEX::Clear( const char* p, UINT t ){

	if ( p ) {
		if ( file && !strcmp_s( file->name, p ) ) {
			file = nullptr;
		}
		info[t].Clear( p );
	}
}

void FILEX::Clear( UINT t ){

	file = nullptr;
	info[t].Clear();
}

void FILEX::Clear( void ){

	file = nullptr;
	info[READ ].Clear();
	info[WRITE].Clear();
}

//========================================
// 非同期
//========================================
//----------------------------------------
// タスクの登録
//----------------------------------------
const FILEI* FILEX::TaskASync( UINT t, const char* p, UINT m, void* a, size_t s, FILEF f, UINT v, UINT n ){

	if ( p == nullptr ) {
		return nullptr;
	}

	const auto	tk = t;
	const auto	md = m;
	const auto	id = PathFile( FILEX::path, p );
	const auto	ad = a;
	const auto	sz = s;
	const auto	av = v;

	switch ( tk ) {
	case READ:
		if ( FILEX::Mode() && n == 0 ) {
			if ( info[READ].Data( id ) ) {
#if OPAL_DEBUG
				printf( "読み込み済み %s\n", id() );
#endif
				return nullptr;
			}
		}
		break;

	case WRITE:
		break;
	}

	if ( const auto fi = info[tk].Information( id, ad, sz, av ) ) {
		fi->Task( tk, md, TIME::Time() );
		if ( const auto fm = info[tk].Message( id, fi, FILET::STEP_INIT, FILET::RETRY_MAX ) ) {
			fm->hand	= 0;
			fm->ovlp	= {};
			fm->lock	= nullptr;
			fm->buff	= nullptr;
			fm->func	= f;
		}

		return fi;
	}

	return nullptr;
}

//----------------------------------------
// LOAD
//----------------------------------------
const FILEI* FILEX::Load( const char*  p,          FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_READ, p,         SYSTEM,      nullptr,   0L, f, v, n ) : nullptr;	}
const FILEI* FILEX::Load( const char*  p, UINT  m, FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_READ, p,         m,           nullptr,   0L, f, v, n ) : nullptr;	}
const FILEI* FILEX::Load( const char*  p, void* a, FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_READ, p,         ( UINT64 )a, a,         0L, f, v, n ) : nullptr;	}
const FILEI* FILEX::Load( const FILEH* h,          FILEF f, UINT v, UINT n ){ return ( h ) ? TaskASync( FILEI::TASK_READ, h->Name(), MANAGE,      h->Buff(), 0L, f, v, n ) : nullptr; 	}

//----------------------------------------
// STORE
//----------------------------------------
const FILEI* FILEX::Store( const char*  p,                size_t s, FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_WRITE, p,         SYSTEM,      nullptr,                s,         f, v, n ) : nullptr;	}
const FILEI* FILEX::Store( const char*  p, UINT m,        size_t s, FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_WRITE, p,         m,           nullptr,                s,         f, v, n ) : nullptr;	}
const FILEI* FILEX::Store( const char*  p, const void* a, size_t s, FILEF f, UINT v, UINT n ){ return ( p ) ? TaskASync( FILEI::TASK_WRITE, p,         ( UINT64 )a, const_cast<void*>( a ), s,         f, v, n ) : nullptr;	}
const FILEI* FILEX::Store( const FILEH* h,                          FILEF f, UINT v, UINT n ){ return ( h ) ? TaskASync( FILEI::TASK_WRITE, h->Name(), MANAGE,      h->Buff(),              h->Size(), f, v, n ) : nullptr;	}

//========================================
// 同期
//========================================
//----------------------------------------
// タスクの登録
//----------------------------------------
const FILEI* FILEX::TaskSync( UINT t, const char* n ){

	if ( n ) {
		const auto	p = info[t].Task( n );

		while ( p ) {
			p->TaskMain();
			if ( !p->flag ) {
				return p->info;
			}
		}
	}

	return nullptr;
}

//----------------------------------------
// READ
//----------------------------------------
const FILEI* FILEX::Read( const char*  p,          FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Load( p,    f, v, n ) ) { return TaskSync( FILEI::TASK_READ, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Read( const char*  p, UINT  m, FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Load( p, m, f, v, n ) ) { return TaskSync( FILEI::TASK_READ, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Read( const char*  p, void* a, FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Load( p, a, f, v, n ) ) { return TaskSync( FILEI::TASK_READ, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Read( const FILEH* h,          FILEF f, UINT v, UINT n ){ if ( h ) { if ( const auto tp = Load( h,    f, v, n ) ) { return TaskSync( FILEI::TASK_READ, tp->Name() ); } } return nullptr;	}

//----------------------------------------
// WRITE
//----------------------------------------
const FILEI* FILEX::Write( const char*  p,                size_t s, FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Store( p,    s, f, v, n ) ) { return TaskSync( FILEI::TASK_WRITE, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Write( const char*  p, UINT m,        size_t s, FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Store( p, m, s, f, v, n ) ) { return TaskSync( FILEI::TASK_WRITE, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Write( const char*  p, const void* a, size_t s, FILEF f, UINT v, UINT n ){ if ( p ) { if ( const auto tp = Store( p, a, s, f, v, n ) ) { return TaskSync( FILEI::TASK_WRITE, tp->Name() ); } } return nullptr;	}
const FILEI* FILEX::Write( const FILEH* h,                          FILEF f, UINT v, UINT n ){ if ( h ) { if ( const auto tp = Store( h,       f, v, n ) ) { return TaskSync( FILEI::TASK_WRITE, tp->Name() ); } } return nullptr;	}

//========================================
// リソース読み込み
//========================================
const FILEI* FILEX::Load( const char* p, const char* t, const char* m ){

	if ( p && t ) {
		struct ARGV {
			ARGV( const char* t = nullptr, const char* m = nullptr ) :
				type(   t ),
				module( m )
			{}
			const char*	type;
			NAME		module;
		};

		const auto	f = []( auto info ){
			const auto		pn = info->Name();
			const auto		av = reinterpret_cast<const ARGV*>( info->Argv() );
			const auto		pt = av->type;
			const HMODULE	md = ::GetModuleHandle( av->module );
			const HRSRC		hr = ::FindResource( md, pn, pt );
			const HGLOBAL 	hg = ::LoadResource( md, hr );
			const LPVOID	rp = ::LockResource( hg );
			const DWORD		rs = ::SizeofResource( nullptr, hr );

			info->Adrs( rp );
			info->Size( rs );

			DELETES( av );

			return nullptr;
		};

		if ( const auto tp = TaskASync( FILEI::TASK_READ, p, CALLBK, nullptr, 0L, f, ( UINT64 )NEWS<ARGV>( t, m ), false ) ) {
			return TaskSync( FILEI::TASK_READ, tp->Name() );
		}
	}
	return nullptr;
}

const FILEI* FILEX::Read( const char* p, const char* t, const char* m ){

	return Load( p, t, m );
}

// End Of File
