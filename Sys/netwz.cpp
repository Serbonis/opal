//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
// ネットワークシステム
//========================================
using namespace opal;

#define ERROR_WSASYSNOTREADY		"ネットワークサブシステムがネットワークへの接続準備ができていない\n"
#define ERROR_WSAVERNOTSUPPORTED	"要求した WinSock のバージョンはこのシステムで提供されていない\n"
#define ERROR_WSAEINPROGRESS		"ブロッキング操作の実行中であるまたはサービスプロバイダがコールバック関数を処理している\n"
#define ERROR_WSAEPROCLIM			"WinSock が同時に処理できる最大プロセスに達成した\n"
#define ERROR_WSAEFAULT				"lpWSAData は有効なポインタではない\n"

//----------------------------------------
// エラーメッセージ
//----------------------------------------
const char* NETWX::Error( int c ){

	switch ( c ) {
	case WSASYSNOTREADY:		return ERROR_WSASYSNOTREADY;
	case WSAVERNOTSUPPORTED:	return ERROR_WSAVERNOTSUPPORTED;
	case WSAEINPROGRESS:		return ERROR_WSAEINPROGRESS;
	case WSAEPROCLIM:			return ERROR_WSAEPROCLIM;
	case WSAEFAULT:				return ERROR_WSAEFAULT;
	}
	return "不明なネットワークエラー";
}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void NETWX::Debug( UINT m ){

	printd( "VERSION  %04x\n", version  );
	printd( "HOSTNAME %s\n",   hostname );
	printd( "HOSTADRS %s\n",   hostadrs );
	printd( "\n" );
}
#endif

// End Of File
