//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( OBJX::index  ){};
DECLVAL( OBJX::linx   ){};

#ifdef OPAL_DEBUG
DECLVAL( OBJX::debug ){};
#endif

//----------------------------------------
// システム初期化
//----------------------------------------
bool OBJX::SysInit( void ){

	NEWS( index );
	index->Init( "OBJECT" );
	linx	= nullptr;

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool OBJX::SysExec( void ){

	if ( index ) {
		index->Exec();
	}

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool OBJX::SysFree( void ){

	if ( index ) {
		index->Free();
		DELETES( index );
	}

	return true;
}

// End Of File
