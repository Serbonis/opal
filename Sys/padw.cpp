//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADW::PADW() :
	size( 0 ),
	joymax( 0 ),
	state( nullptr )
{}
PADW::~PADW(){}

//----------------------------------------
// 初期化＆解放
//----------------------------------------
void PADW::Init( size_t s, UINT p ){

	size	= s;
	joymax	= p;

	LINX::Init( NAME( "PADW[%d]", p ) );

	NEWA( state, size );
	for ( auto i = 0UL; i < size; i++ ) {
		state[i].Init();
		state[i].Register( this );
	}
}

void PADW::Free( void ){

	for ( auto i = 0UL; i < size; i++ ) {
		state[i].Free();
	}
	DELETEA( state );

	LINX::Free();

	joymax	= 0;
	size	= 0;
}

//----------------------------------------
// パッドデータの情報
//----------------------------------------
size_t PADW::Size(   void ) const { return size;	}
UINT   PADW::JoyMax( void ) const { return joymax;	}

//----------------------------------------
// パッドデータのSet/Get
//----------------------------------------
void PADW::SetData( const PADD* d ){

	SetDataKeyboard( const_cast<PADD*>( d )->Keyboard()	);
	SetDataMouse(    const_cast<PADD*>( d )->Mouse()	);

	for ( auto i = 0UL; i < joymax; i++ ) {
		SetDataJoypad( i, const_cast<PADD*>( d )->Joypad( i ) );
	}
}

void PADW::SetDataKeyboard( const KEYBOARD* d ){

	if ( const auto	p = Head<PADT>()->Keyboard() ) {
		if ( d ) {
			*p = *d;
		} else {
			p->clear();
		}
	}
}

void PADW::SetDataMouse( const MOUSE* d ){

	if ( const auto p = Head<PADT>()->Mouse() ) {
		if ( d ) {
			*p = *d;
		} else {
			p->clear();
		}
	}
}

void PADW::SetDataJoypad( UINT v, const JOYPAD* d ){

	if ( const auto p = Head<PADT>()->Joypad( v ) ) {
		if ( d ) {
			*p = *d;
		} else {
			p->clear();
		}
	}
}

PADD* PADW::GetData( UINT pr ) const { return Data( pr ); }

KEYBOARD* PADW::GetDataKeyboard(         UINT r ) const { return Data( r )->Keyboard();		}
MOUSE*    PADW::GetDataMouse(            UINT r ) const { return Data( r )->Mouse();		}
JOYPAD*   PADW::GetDataJoypad(   UINT v, UINT r ) const { return Data( r )->Joypad( v );	}

// 内部用
PADT* PADW::Data( UINT s, UINT e ) const {

	if ( s + e < size ) {
		auto t = Head()->Last();

		for ( auto i = 0UL; i < s; i++ ) {
			t = t->Last();
		}
		return t->Task<PADT>();
	}
	return nullptr;
}

//----------------------------------------
// 判定関数:Keyboard
//----------------------------------------
UINT PADW::KeyPush( UINT k, UINT s, UINT e ) const {

	if ( k ) {
		auto	n = 0UL;

		if ( auto p = Data( s, e ) ) {
			for ( auto i = 0UL; i < e; i++ ) {
				if ( k < KEY_MAX ) {
					if ( p->Keyboard()->stroke[k] ) {
						n++;
					}
				}
				p = p->Last<PADT>();
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::KeyTrig( UINT k, UINT s, UINT e ) const {

	if ( k ) {
		auto	n = 0L;

		if ( auto p = Data( s, e ) ) {
			auto	q = p->Last<PADT>();

			for ( auto i = 0UL; i < e; i++ ) {
				if ( k < KEY_MAX ) {
					if (  p->Keyboard()->stroke[k] &&
						 !q->Keyboard()->stroke[k] ) {
						n++;
					}
				}
				p = q;
				q = q->Last<PADT>();
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::KeyRels( UINT k, UINT s, UINT e ) const {

	if ( k ) {
		auto	n = 0L;

		if ( auto p = Data( s, e ) ) {
			auto	q = p->Last<PADT>();

			for ( auto i = 0UL; i < e; i++ ) {
				if ( k < KEY_MAX ) {
					if ( !p->Keyboard()->stroke[k] &&
						  q->Keyboard()->stroke[k] ) {
						n++;
					}
				}
				p = q;
				q = q->Last<PADT>();
			}
		}

		return n;
	}

	return 0;
}

//----------------------------------------
// 判定関数:Joypad
//----------------------------------------
UINT PADW::JoyPushO( UINT v, UINT b, UINT s, UINT e ) const {

	if ( v >= joymax ) { return 0;	}

	if ( b ) {
		static const auto	check = []( const PADW* d, UINT v, UINT b, UINT s, UINT e )->UINT{
			auto	n = 0;

			if ( auto p = d->Data( s, e ) ) {
				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( p->Joypad( v )->button & b ) == b ) {
						n++;
					}
					p = p->Last<PADT>();
				}
			}
			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < PAD_MAX; i++ ) {
			if ( const auto x = b & ( 1 << i ) ) {
				if ( check( this, v, x, s, e ) ) {
					n |= x;
				}
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::JoyTrigO( UINT v, UINT b, UINT s, UINT e ) const {

	if ( v >= joymax ) { return 0;	}

	if ( b ) {
		static const auto	check = []( const PADW* d, UINT v, UINT b, UINT s, UINT e )->UINT{
			auto	n = 0;

			if ( auto p = d->Data( s, e ) ) {
				auto	q = p->Last<PADT>();

				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( ( p->Joypad( v )->button & b ) == b ) &&
						 ( ( q->Joypad( v )->button & b ) != b ) ) {
						n++;
					}
					p = q;
					q = q->Last<PADT>();
				}
			}

			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < PAD_MAX; i++ ) {
			if ( const auto x = b & ( 1 << i ) ) {
				if ( check( this, v, x, s, e ) ) {
					n |= x;
				}
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::JoyRelsO( UINT v, UINT b, UINT s, UINT e ) const {

	if ( v >= joymax ) { return 0;	}

	if ( b ) {
		static const auto	check = []( const PADW* d, UINT v, UINT b, UINT s, UINT e )->UINT{

			auto	n = 0;

			if ( auto p = d->Data( s, e ) ) {
				auto	q = p->Last<PADT>();

				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( ( p->Joypad( v )->button & b ) != b ) &&
						 ( ( q->Joypad( v )->button & b ) == b ) ) {
						n++;
					}
					p = q;
					q = q->Last<PADT>();
				}
			}

			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < PAD_MAX; i++ ) {
			if ( const auto x = b & ( 1 << i ) ) {
				if ( check( this, v, x, s, e ) ) {
					n |= x;
				}
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::JoyPushA( UINT v, UINT b, UINT s, UINT e ) const { return ( b ) ? b == JoyPushO( v, b, s, e ) : 0; }
UINT PADW::JoyTrigA( UINT v, UINT b, UINT s, UINT e ) const { return ( b ) ? b == JoyTrigO( v, b, s, e ) : 0; }
UINT PADW::JoyRelsA( UINT v, UINT b, UINT s, UINT e ) const { return ( b ) ? b == JoyRelsO( v, b, s, e ) : 0; }

float PADW::JoyStick( UINT v, UINT s, UINT t ) const {

	if ( v < joymax ) {
		if ( const auto p = Data( t ) ) {
			switch ( s ) {
			case STICK_LX:return p->Joypad( v )->lx;
			case STICK_LY:return p->Joypad( v )->ly;
			case STICK_RX:return p->Joypad( v )->rx;
			case STICK_RY:return p->Joypad( v )->ry;
			}
		}
	}

	return 0.0f;
}

//----------------------------------------
// 判定関数:Mouse
//----------------------------------------
UINT PADW::MousePush( UINT c, UINT s, UINT e ) const {

	if ( c ) {
		static const auto	check = []( const PADW* d, UINT c, UINT s, UINT e )->UINT{

			auto	n = 0;

			if ( auto p = d->Data( s ) ) {
				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( p->Mouse()->click & c ) == c ) {
						n++;
					}
					p = p->Last<PADT>();
				}
			}

			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
			if ( const auto x = c & ( 1 << i ) ) {
				if ( check( this, x, s, e ) ) {
					n |= x;
				}
			}
		}

		return n;
	}

	return 0;
}

UINT PADW::MouseTrig( UINT c, UINT s, UINT e ) const {

	if ( c ) {
		static const auto	check = []( const PADW* d, UINT c, UINT s, UINT e )->UINT{

			auto	n = 0;

			if ( auto p = d->Data( s ) ) {
				auto q = p->Last<PADT>();

				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( ( p->Mouse()->click & c ) == c ) &&
						 ( ( q->Mouse()->click & c ) != c ) ) {
						n++;
					}
					p = q;
					q = q->Last<PADT>();
				}
			}

			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
			if ( const auto x = c & ( 1 << i ) ) {
				if ( check( this, x, s, e ) ) {
					n |= x;
				}
			}
		}
		return n;
	}

	return 0;
}

UINT PADW::MouseRels( UINT c, UINT s, UINT e ) const {

	if ( c ) {
		static const auto	check = []( const PADW* d, UINT c, UINT s, UINT e )->UINT{

			auto 	n = 0;

			if ( auto p = d->Data( s ) ) {
				auto q = p->Last<PADT>();

				for ( auto i = 0UL; i < e; i++ ) {
					if ( ( ( p->Mouse()->click & c ) != c ) &&
						 ( ( q->Mouse()->click & c ) == c ) ) {
						n++;
					}
					p = q;
					q = q->Last<PADT>();
				}
			}

			return n;
		};

		auto	n = 0L;

		for ( auto i = 0UL; i < MOUSE_MAX; i++ ) {
			if ( const auto x = c & ( 1 << i ) ) {
				if ( check( this, x, s, e ) ) {
					n |= x;
				}
			}
		}

		return n;
	}

	return 0;
}

LONG PADW::Mouse( UINT c, UINT t ) const {

	if ( const auto p = Data( t ) ) {
		switch ( c ) {
		case MOUSE_LC:return p->Mouse()->mx;
		case MOUSE_RC:return p->Mouse()->my;
		case MOUSE_CC:return p->Mouse()->mz;
		}
	}

	return 0;
}

LONG PADW::MouseX( UINT t ) const { return Mouse( MOUSE_LC, t ); }
LONG PADW::MouseY( UINT t ) const { return Mouse( MOUSE_RC, t ); }
LONG PADW::Scroll( UINT t ) const { return Mouse( MOUSE_CC, t ); }

LONG PADW::Cursor( UINT c, UINT t ) const {

	if ( const auto p = Data( t ) ) {
		switch ( c ) {
		case MOUSE_LC:return p->Mouse()->point.x;
		case MOUSE_RC:return p->Mouse()->point.y;
		}
	}

	return 0;
}

LONG PADW::CursorX( UINT t ) const { return Cursor( MOUSE_LC, t ); }
LONG PADW::CursorY( UINT t ) const { return Cursor( MOUSE_RC, t ); }

//----------------------------------------
// 複合型 & (PAD1|PAD2)
//----------------------------------------
float PADW::Joy1Stick( UINT s, UINT t ) const { return JoyStick( JOY1, s, t ); }
float PADW::Joy2Stick( UINT s, UINT t ) const { return JoyStick( JOY2, s, t ); }

UINT PADW::Joy1PushO( UINT b, UINT s, UINT e ) const { return JoyPushO( JOY1, b, s, e ); }
UINT PADW::Joy2PushO( UINT b, UINT s, UINT e ) const { return JoyPushO( JOY2, b, s, e ); }
UINT PADW::Joy1TrigO( UINT b, UINT s, UINT e ) const { return JoyTrigO( JOY1, b, s, e ); }
UINT PADW::Joy2TrigO( UINT b, UINT s, UINT e ) const { return JoyTrigO( JOY2, b, s, e ); }
UINT PADW::Joy1RelsO( UINT b, UINT s, UINT e ) const { return JoyRelsO( JOY1, b, s, e ); }
UINT PADW::Joy2RelsO( UINT b, UINT s, UINT e ) const { return JoyRelsO( JOY2, b, s, e ); }
UINT PADW::Joy1PushA( UINT b, UINT s, UINT e ) const { return JoyPushA( JOY1, b, s, e ); }
UINT PADW::Joy2PushA( UINT b, UINT s, UINT e ) const { return JoyPushA( JOY2, b, s, e ); }
UINT PADW::Joy1TrigA( UINT b, UINT s, UINT e ) const { return JoyTrigA( JOY1, b, s, e ); }
UINT PADW::Joy2TrigA( UINT b, UINT s, UINT e ) const { return JoyTrigA( JOY2, b, s, e ); }
UINT PADW::Joy1RelsA( UINT b, UINT s, UINT e ) const { return JoyRelsA( JOY1, b, s, e ); }
UINT PADW::Joy2RelsA( UINT b, UINT s, UINT e ) const { return JoyRelsA( JOY2, b, s, e ); }

UINT PADW::JKPushO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyPushO( p, b, s, e ) || KeyPush( k, s, e ); }
UINT PADW::JKTrigO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyTrigO( p, b, s, e ) || KeyTrig( k, s, e ); }
UINT PADW::JKRelsO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyRelsO( p, b, s, e ) || KeyRels( k, s, e ); }
UINT PADW::JKPushA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyPushA( p, b, s, e ) || KeyPush( k, s, e ); }
UINT PADW::JKTrigA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyTrigA( p, b, s, e ) || KeyTrig( k, s, e ); }
UINT PADW::JKRelsA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return JoyRelsA( p, b, s, e ) || KeyRels( k, s, e ); }

UINT PADW::J1KPushO( UINT b, UINT k, UINT s, UINT e ) const { return JKPushO( JOY1, b, k, s, e ); }
UINT PADW::J2KPushO( UINT b, UINT k, UINT s, UINT e ) const { return JKPushO( JOY2, b, k, s, e ); }
UINT PADW::J1KTrigO( UINT b, UINT k, UINT s, UINT e ) const { return JKTrigO( JOY1, b, k, s, e ); }
UINT PADW::J2KTrigO( UINT b, UINT k, UINT s, UINT e ) const { return JKTrigO( JOY2, b, k, s, e ); }
UINT PADW::J1KRelsO( UINT b, UINT k, UINT s, UINT e ) const { return JKRelsO( JOY1, b, k, s, e ); }
UINT PADW::J2KRelsO( UINT b, UINT k, UINT s, UINT e ) const { return JKRelsO( JOY2, b, k, s, e ); }
UINT PADW::J1KPushA( UINT b, UINT k, UINT s, UINT e ) const { return JKPushA( JOY1, b, k, s, e ); }
UINT PADW::J2KPushA( UINT b, UINT k, UINT s, UINT e ) const { return JKPushA( JOY2, b, k, s, e ); }
UINT PADW::J1KTrigA( UINT b, UINT k, UINT s, UINT e ) const { return JKTrigA( JOY1, b, k, s, e ); }
UINT PADW::J2KTrigA( UINT b, UINT k, UINT s, UINT e ) const { return JKTrigA( JOY2, b, k, s, e ); }
UINT PADW::J1KRelsA( UINT b, UINT k, UINT s, UINT e ) const { return JKRelsA( JOY1, b, k, s, e ); }
UINT PADW::J2KRelsA( UINT b, UINT k, UINT s, UINT e ) const { return JKRelsA( JOY2, b, k, s, e ); }

UINT PADW::JKPushO( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKPushO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::JKTrigO( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKTrigO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::JKRelsO( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKRelsO( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::JKPushA( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKPushA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::JKTrigA( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKTrigA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::JKRelsA( UINT p, const PADA* a, UINT s, UINT e ) const { return a ? JKRelsA( p, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }

UINT PADW::J1KPushO( const PADA* a, UINT s, UINT e ) const { return a ? JKPushO( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KPushO( const PADA* a, UINT s, UINT e ) const { return a ? JKPushO( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J1KTrigO( const PADA* a, UINT s, UINT e ) const { return a ? JKTrigO( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KTrigO( const PADA* a, UINT s, UINT e ) const { return a ? JKTrigO( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J1KRelsO( const PADA* a, UINT s, UINT e ) const { return a ? JKRelsO( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KRelsO( const PADA* a, UINT s, UINT e ) const { return a ? JKRelsO( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J1KPushA( const PADA* a, UINT s, UINT e ) const { return a ? JKPushA( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KPushA( const PADA* a, UINT s, UINT e ) const { return a ? JKPushA( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J1KTrigA( const PADA* a, UINT s, UINT e ) const { return a ? JKTrigA( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KTrigA( const PADA* a, UINT s, UINT e ) const { return a ? JKTrigA( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J1KRelsA( const PADA* a, UINT s, UINT e ) const { return a ? JKRelsA( JOY1, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }
UINT PADW::J2KRelsA( const PADA* a, UINT s, UINT e ) const { return a ? JKRelsA( JOY2, a->GetJoypad(), a->GetKeyboard(), s, e ) : 0; }

// End Of File
