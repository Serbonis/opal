//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"
#include "draw_dx2d.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;
using namespace opal::DRAWX_DX2D;

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::DRAWX_DX11 {
	DEVICE			device;
	VIEW_PORT		viewport;
	CLEAR_BUFFER	clearbuffer;
}

namespace opal::DRAWX_DX2D {
	DEVICE			device;
}

//----------------------------------------
// システム
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void SysInit( void ){}
	void SysExec( void ){}
	void SysFree( void ){}

	void SysInitAdapter( void ){}
	void SysFreeAdapter( void ){}

	void SysInitDevice( HWND window, UINT w, UINT h ){

		DRAWX_DX11::device.Init( window, w, h );
		DRAWX_DX2D::device.Init( DRAWX_DX11::device.SwapChain() );
	}

	void SysFreeDevice( void ){

		DRAWX_DX2D::device.Free();
		DRAWX_DX11::device.Free();
	}

	void SysInitMonitor( void ){}
	void SysFreeMonitor( void ){}

	void SysInitViewport( void ){ viewport.Init( DRAWX_DX11::device );	}
	void SysFreeViewport( void ){ viewport.Free();			}

	void SysInitClearbuffer( void ){ clearbuffer.Init( DRAWX_DX11::device );	}
	void SysFreeClearbuffer( void ){ clearbuffer.Free();	}

	void SysBegin( void ){

		DRAWX_DX11::device.Begin();
		DRAWX_DX2D::device.Begin();
		viewport.Begin();
		viewport.Begin(    DRAWX_DX11::device );
		clearbuffer.Clear( DRAWX_DX11::device );
		SCISSOR::Clear(    DRAWX_DX11::device );
	}

	void SysEnd( void ){

		viewport.End( DRAWX_DX11::device );
		viewport.End();
		DRAWX_DX2D::device.End();
		DRAWX_DX11::device.End();
	}

	void Present( void ){

		DRAWX_DX11::device.Present();
	}
}

// End Of File
