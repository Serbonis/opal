//========================================
// OPAL Game Library
//========================================
#include "heapx.hpp"

//========================================
// HEAP管理
//========================================
using namespace opal;

namespace {
	constexpr size_t	NODE_SIZE	= 16;

	inline HEAP_KEY	hash_k( const void* p ){ return reinterpret_cast<HEAP_KEY>( p );	}
	inline UINT		hash_n( const HEAP_KEY k, const size_t s ){ return (k>>4)%s;		}
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
HEAP_HASH::HEAP_HASH() :
	size( 0 ),
	keys( 0 ),
	node(  nullptr )
{}
HEAP_HASH::~HEAP_HASH(){ Free(); }

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void HEAP_HASH::Init( void ){

	size	= NODE_SIZE;
	keys	= 0;
	node	= ( HEAP_NODE** )std::calloc( size, sizeof( HEAP_NODE* ) );
}

void HEAP_HASH::Free( void ){

	if ( node ) {
		for ( auto i = 0UL; i < size; i++ ) {
			auto n = node[i];

			while ( const auto x = n ) {
				n = n->next;
				x->Free();
			}
			node[i] = nullptr;
		}
		keys = 0;

		std::free( node );
		node = nullptr;
	}
}

//----------------------------------------
// 要素取得
//----------------------------------------
size_t HEAP_HASH::Size( void ) const { return size; }
UINT   HEAP_HASH::Keys( void ) const { return keys; }

//----------------------------------------
// NODE取得
//----------------------------------------
HEAP_NODE* HEAP_HASH::Node( const void* p ) const {

	if ( node ) {
		const auto	k = hash_k( p );
		const auto	h = hash_n( k, size );

		for ( auto n = node[h]; n; n = n->next ) {
			if ( k == n->key ) {
				return n;
			}
		}
	}
	return nullptr;
}

//----------------------------------------
// NODE作成
//----------------------------------------
HEAP_NODE* HEAP_HASH::New( const void* p, const char* t, size_t s, size_t u ){

	if ( node ) {
		const auto	k = hash_k( p );
		const auto	h = hash_n( k, size );

		for ( auto n = node[h]; n; n = n->next ) {
			if ( k == n->key ) {
				return n;
			}
		}

		const auto	n = ( HEAP_NODE* )std::malloc( sizeof( HEAP_NODE ) );

		n->Init( k, p,t,s,u, node[h] );
		node[h] = n;
		keys++;

		return n;
	}

	return nullptr;
}

//----------------------------------------
// NODE削除
//----------------------------------------
void HEAP_HASH::Del( const void* p ){

	if ( node ) {
		const auto	k = hash_k( p );
		const auto	h = hash_n( k, size );
		HEAP_NODE*	m = nullptr;

		for ( auto n = node[h]; n; n = n->next ) {
			if ( k == n->key ) {
				if ( n == node[h] ) {
					node[h] = n->next;
				} else {
					m->next = n->next;
				}
				n->Free();
				std::free( n );
				keys--;
				break;
			}
			m = n;
		}
	}
}

//----------------------------------------
// デバッグ表示
//----------------------------------------
#ifdef OPAL_DEBUG
void HEAP_HASH::Debug( bool f ) const {

	if ( node ){
		const auto print = []( auto f )->int ( * )( const char*, ... ){

			if ( f ) {
				return printf;
			} else {
				return printd;
			}
		}( f );

		for ( auto i = 0UL; i < size; i++ ) {
			if ( auto n = node[i] ) {
				print( "%p %08d[%04d] %s\n", n->val.adrs, n->val.size, n->val.unit, n->val.type );
				for ( n = n->next; n; n = n->next ) {
					print( "%p %08d[%04d] %s\n", n->val.adrs, n->val.size, n->val.unit, n->val.type );
				}
			}
		}
	}
}
#endif


//========================================
// データ
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
HEAP_DATA::HEAP_DATA() :
	adrs( nullptr ),
	type( nullptr ),
	size( 0 ),
	unit( 0 )
{}
HEAP_DATA::HEAP_DATA( const void* p, const char* t, size_t s, size_t u ) :
	adrs( p ),
	size( s ),
	unit( u )
{
	type = ( char* )std::malloc( std::strlen( t )+1 );
	std::strcpy( type, t );
}

HEAP_DATA::~HEAP_DATA()
{
	std::free( type );
}

//========================================
// ノード
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
HEAP_NODE::HEAP_NODE() :
	key(),
	val(),
	next( nullptr )
{}
HEAP_NODE::~HEAP_NODE(){ Free(); }

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void HEAP_NODE::Init( HEAP_KEY k, const void* p, const char* t, size_t s, size_t u, HEAP_NODE* n ) {

	key  = k;
	new ( &val ) HEAP_DATA( p, t, s, u );
	next = n;
}

void HEAP_NODE::Free( void ){

	val.~HEAP_DATA();
}

// End Of File
