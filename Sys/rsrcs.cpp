//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"

//========================================
// リソース
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RSRC::RSRC() :
	file()
{}
RSRC::~RSRC(){}

//----------------------------------------
// 初期化/後始末
//----------------------------------------
// 外部で読み込み終了後に呼ぶ
void RSRC::Init( const FILED* p, const void* d ){

	if ( p ) {
		Init( p->Adrs() ? p->Adrs() : reinterpret_cast<void*>( p->Argv() ), p->Size(), p->Name(), d );
	}
}

// 内部で同期読み込みを自動で行う
void RSRC::Init( const std::string_view n, const std::string_view t, const void* d ){

	if ( const auto p = n.data() ) {
		if ( const auto s = t.data() ) {
			FILEX::Read( p, s );
		} else {
			FILEX::Read( p );
		}
		Init( FILEX::Adrs(), FILEX::Size(), p, d );
	}
}
void RSRC::Init( const std::string_view n, const void* d ){ Init( n, {},  d ); }
void RSRC::Init( void ){ Init( {}, {}, nullptr );	}

//----------------------------------------
// 情報取得
//----------------------------------------
const char* RSRC::File( void ) const { return file.Name(); }
const void* RSRC::Adrs( void ) const { return file.Adrs(); }
size_t      RSRC::Size( void ) const { return file.Size(); }

// End Of File
