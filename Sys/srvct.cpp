//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SRVC::SRVC( const char* p ) :
	name( p ),
	prio( 0 ),
	clear( nullptr )
{}
SRVC::~SRVC(){}

//----------------------------------------
// サービス名
//----------------------------------------
const char* SRVC::GetServiceName( void ) const { return name; }
void SRVC::SetServiceName( const char* p ){

	if ( !SRVCX::smap.count( p ) ) {
		name = p;
		SRVCX::smap[p] = this;
		SRVCX::skey.emplace_back( p );
		SRVCX::sval.emplace_back( this );
	}
}

//----------------------------------------
// 優先順位
//----------------------------------------
UINT SRVC::GetServicePrio( void ) const { return prio; }
void SRVC::SetServicePrio( UINT n ){

	prio = n;
	SRVCX::Sort();
}

//----------------------------------------
// 削除関数
//----------------------------------------
void SRVC::SetServiceClear( CFUNC f ){ clear = f; }
SRVC::CFUNC SRVC::GetServiceClear( void ) const { return clear; }

void SRVC::Clear( void ){ if ( clear ) { clear(); }	}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void SRVC::Debug( const char* s ) const {}
#endif

// End Of File
