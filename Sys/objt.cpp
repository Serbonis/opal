//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトタスク
//========================================
using namespace opal;

//----------------------------------------
// construct & destructo
//----------------------------------------
OBJT::OBJT(){}
OBJT::~OBJT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OBJT::Init( const char* p ){

	TASK::Init( p );
	OBJ::Init();

	step 	= 0;
	time 	= 0;
	debug	= false;

	updater.begin.clear();
	updater.end.clear();
}

void OBJT::Free( void ){

	OBJ::Free();
	TASK::Free();
}

//----------------------------------------
// 登録＆解放
//----------------------------------------
void OBJT::Register( LINX* l ){

	if ( l ) {
		TASK::Register( l );
	} else if ( OBJX::linx ) {
		TASK::Register( OBJX::linx );	// デフォルト登録先
	} else {
		TASK::Register( OBJX::index );	// システム登録先
	}
}

void OBJT::Release(  LINX* l ){

	if ( l ) {
		TASK::Release( l );
	} else {
		TASK::Release( Linx() );
	}
}

void OBJT::Register( OLINX* l ){ Register( ( LINX* )l );		}
void OBJT::Release(  OLINX* l ){ Release(  ( LINX* )l );		}
void OBJT::Register( std::shared_ptr<OLINX> l ){ OBJT::Register( ( LINX* )l.get() ); }
void OBJT::Release(  std::shared_ptr<OLINX> l ){ OBJT::Release(  ( LINX* )l.get() ); }
void OBJT::Register( void     ){ Register( ( LINX* )nullptr );	}
void OBJT::Release(  void     ){ Release(  ( LINX* )nullptr );	}

//----------------------------------------
// 登録先情報
//----------------------------------------
OLINX* OBJT::TLinx( const OBJT* ){ return dynamic_cast<OLINX*>( OBJX::linx ? OBJX::linx : OBJX::index ); }
LINX*  OBJT::TLinx( void ){ return OBJX::index; }

//----------------------------------------
// インスタンス制御
//----------------------------------------
void OBJT::SetSelf( SELF f ){ Self( f );	}
auto OBJT::GetSelf( void ) const->SELF{ return Self(); }

//----------------------------------------
// デバッグ
//----------------------------------------
void OBJT::SetDebug( int f ){ FLAG_CTRL( debug, f );	}
bool OBJT::GetDebug( void ) const { return debug;		}

//----------------------------------------
// 名前
//----------------------------------------
void OBJT::SetID_( const char* p ){ TASK::ID( p );	}

void OBJT::SetID( const char* p, ...   ){

	if ( p ) {
		va_list	l;

		va_start( l, p );
		{
			std::vector<char>	b;

			if ( const auto s = strlenf_s( p, l ) ) {
				b.resize( s+1 );
				std::vsprintf( &b[0], p, l );
				SetID_( &b[0] );
			} else {
				SetID_( "" );
			}
		}
		va_end( l );
	} else {
		SetID_( nullptr );
	}
}
void OBJT::SetID( const std::string& s ){ SetID_( s.c_str() );	}
void OBJT::SetID( std::nullptr_t       ){ SetID_( nullptr );	}

auto OBJT::GetID( void ) const->const char*{ return TASK::ID();	}

//----------------------------------------
// 情報
//----------------------------------------
void OBJT::SetInfo( UINT v ){ TASK::Info( v );			}
UINT OBJT::GetInfo( void ) const { return TASK::Info(); }

//----------------------------------------
// 登録順位
//----------------------------------------
void OBJT::SetPrio( UINT n, bool f ){ TASK::Prio( n, f );	}
UINT OBJT::GetPrio( void ) const { return TASK::Prio(); 	}

//----------------------------------------
// LINX剪定
//----------------------------------------
void OBJT::SetPrune( bool f ){ TASK::Prune( f );			}
bool OBJT::GetPrune( void ) const { return TASK::Prune();	}

//----------------------------------------
// アップデーター
//----------------------------------------
void OBJT::SetUpdater(      void ){ SetUpdaterBegin(); SetUpdaterEnd(); }
void OBJT::SetUpdaterBegin( void ){ for ( auto& [s,f] : updater.begin ) { f = {}; };	}
void OBJT::SetUpdaterEnd(   void ){ for ( auto& [s,f] : updater.end   ) { f = {}; };	}

void OBJT::SetUpdater(      const std::string& s, std::nullptr_t, std::nullptr_t ){ SetUpdaterBegin( s ); SetUpdaterEnd( s ); }
void OBJT::SetUpdaterBegin( const std::string& s, std::nullptr_t ){ if ( updater.begin.count( s ) ) { updater.begin.at( s ) = {}; }	}
void OBJT::SetUpdaterEnd(   const std::string& s, std::nullptr_t ){ if ( updater.end.count(   s ) ) { updater.end.at(   s ) = {}; }	}

UINT OBJT::GetUpdater(      void ) const { return GetUpdaterBegin()+GetUpdaterEnd();	}
UINT OBJT::GetUpdaterBegin( void ) const { return updater.begin.size();	}
UINT OBJT::GetUpdaterEnd(   void ) const { return updater.end.size();	}

auto OBJT::GetUpdater(      const std::string& s ) const->UPDATERS{ return { GetUpdaterBegin( s ),GetUpdaterEnd( s ) }; }
auto OBJT::GetUpdaterBegin( const std::string& s ) const->UPDATER { return updater.begin.count( s ) ? updater.begin.at( s ) : UPDATER{};	}
auto OBJT::GetUpdaterEnd(   const std::string& s ) const->UPDATER { return updater.end.count(   s ) ? updater.end.at(   s ) : UPDATER{};	}

void OBJT::Updater( void ){

	const auto	updater_func = [&]( auto& func ){
		std::stack<std::string>	v;

		for ( const auto& [s,f] : func ) {
			if ( f.func0 ) {       f.func0();							continue;	}
			if ( f.func1 ) {       f.func1( this );						continue;	}
			if ( f.func2 ) { if ( !f.func2()       ) { v.push( s ); }	continue;	}
			if ( f.func3 ) { if ( !f.func3( this ) ) { v.push( s ); }	continue;	}

			v.push( s );
		}
		while ( !v.empty() ) { func.erase( v.top() ); v.pop(); }
	};

	updater_func( updater.begin );
	ObjFunc();
	updater_func( updater.end   );
}

#if OPAL_DEBUG
void OBJT::UpdaterDebug( void ){

	const auto	updater_func = [&]( auto& func ){

		for ( const auto& [s,f] : func ) {
			if ( f.func0 ) { printd( "-(-) %s\n", s.c_str() ); continue;	}
			if ( f.func1 ) { printd( "-(+) %s\n", s.c_str() ); continue;	}
			if ( f.func2 ) { printd( "+(-) %s\n", s.c_str() ); continue;	}
			if ( f.func3 ) { printd( "+(+) %s\n", s.c_str() ); continue;	}
		}
	};

	printd( "BEGIN\n" );
	updater_func( updater.begin );
	printd( "\n" );
	printd( "END\n" );
	updater_func( updater.end   );
	printd( "\n" );
}
#endif

//----------------------------------------
// 計算
//----------------------------------------
void OBJT::Coordinate( void ) {

	OBJ::Coordinate( LinxTask( this ) );
}

// End Of File
