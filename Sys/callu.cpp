//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// サウンドタスク
//========================================
using namespace opal;

//----------------------------------------
// TASK INFO
//----------------------------------------
void CALLT::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	CallInfo();
#endif
}

//----------------------------------------
// TASK INIT
//----------------------------------------
void CALLT::TaskInit( void ){

	CallInit();

	const auto	p = SIGX::PauseCheck( GetLevel() ) ? true : false;
	const auto	f = GetFunc( 1 ) ? true : false;
	const auto	pt= ( CALLS::state & CALL_STATE_PAUSE ) ? true : false;

	CALLS::state = CALL_STATE_NULL;

	if (  p      ) { CALLS::state |= CALL_STATE_PAUSE;		}
	if (  p ^ pt ) { CALLS::state |= CALL_STATE_PAUSE_T;	}
	if ( !p && f ) { CALLS::state |= CALL_STATE_FUNC;		}

	if ( p ) {
		AddFlag( FLAG_P );
	} else {
		SubFlag( FLAG_P );
	}
}

//----------------------------------------
// TASK ENTER
//----------------------------------------
void CALLT::TaskEnter( void ){

	CallEnter();
}

//----------------------------------------
// TASK FUNC
//----------------------------------------
void CALLT::TaskFunc( void ){

	if ( CALLS::state & CALL_STATE_PAUSE ) { return;	}
	if ( CALLS::state & CALL_STATE_FUNC  ) {
		Updater();
		OBJ::state &= ~OBJ_STATE_FUNC;
		CallFunc();
	}
}

//----------------------------------------
// TASK BEGIN
//----------------------------------------
void CALLT::TaskBegin( void ){

	OBJT::Coordinate();
	CALLS::Coordinate( LinxTask( this ) );

	if ( true ) {
		CALLS::state |= CALL_STATE_MAIN;
		CallBegin();
	}
}

//----------------------------------------
// TASK MAIN
//----------------------------------------
void CALLT::TaskMain( void ){

	if ( CALLS::state & CALL_STATE_MAIN ) {
		CallMain();
	}
}

//----------------------------------------
// TASK END
//----------------------------------------
void CALLT::TaskEnd(  void ){

	if ( CALLS::state & CALL_STATE_MAIN ) {
		CallEnd();
	}
}

//----------------------------------------
// TASK EXIT
//----------------------------------------
void CALLT::TaskExit( void ){

	CallExit();
}

//----------------------------------------
// TASK FREE
//----------------------------------------
void CALLT::TaskFree( void ){

	CallFree();
}

// End Of File
