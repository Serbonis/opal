//========================================
// OPAL Game Library
//========================================
#include "xpad.hpp"
#include "padx.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
KEYBOARD::KEYBOARD() :
	stroke{0}
{}
KEYBOARD::~KEYBOARD(){}

MOUSE::MOUSE() :
	point{0,0},
	click( 0 ),
	mx( 0 ),
	my( 0 ),
	mz( 0 )
{}
MOUSE::~MOUSE(){}

JOYPAD::JOYPAD() :
	button( 0 ),
	lx( 0 ),ly( 0 ),
	rx( 0 ),ry( 0 ),
	pov{0}
{}
JOYPAD::~JOYPAD(){}

//----------------------------------------
// データクリア
//----------------------------------------
void KEYBOARD::clear( void ){}
void MOUSE::clear( void ){}
void JOYPAD::clear( void ){}

//----------------------------------------
// 複合キー
//----------------------------------------
bool KEYBOARD::Extension( UINT s ) const {

	switch ( s ) {
	case KEY_FUNCTION:
		return ( stroke[KEY_F1 ]	||
				 stroke[KEY_F2 ]	||
				 stroke[KEY_F3 ]	||
				 stroke[KEY_F4 ]	||
				 stroke[KEY_F5 ]	||
				 stroke[KEY_F6 ]	||
				 stroke[KEY_F7 ]	||
				 stroke[KEY_F8 ]	||
				 stroke[KEY_F9 ]	||
				 stroke[KEY_F10]	||
				 stroke[KEY_F11]	||
				 stroke[KEY_F12]	||
				 stroke[KEY_F13]	||
				 stroke[KEY_F14]	||
				 stroke[KEY_F15]	);

	case KEY_ALPHABET:
		return ( stroke[KEY_A] || stroke[KEY_B] || stroke[KEY_C] || stroke[KEY_D] ||
				 stroke[KEY_E] || stroke[KEY_F] || stroke[KEY_G] || stroke[KEY_H] ||
				 stroke[KEY_I] || stroke[KEY_J] || stroke[KEY_K] || stroke[KEY_L] ||
				 stroke[KEY_M] || stroke[KEY_N] || stroke[KEY_O] || stroke[KEY_P] ||
				 stroke[KEY_Q] || stroke[KEY_R] || stroke[KEY_S] || stroke[KEY_T] ||
				 stroke[KEY_U] || stroke[KEY_V] || stroke[KEY_W] || stroke[KEY_X] ||
				 stroke[KEY_Y] || stroke[KEY_Z]	);

	case KEY_NUMBER:
		return ( stroke[KEY_0]	||
				 stroke[KEY_1]	||
				 stroke[KEY_2]	||
				 stroke[KEY_3]	||
				 stroke[KEY_4]	||
				 stroke[KEY_5]	||
				 stroke[KEY_6]	||
				 stroke[KEY_7]	||
				 stroke[KEY_8]	||
				 stroke[KEY_9]	);

	case KEY_SHIFT:
		return ( stroke[KEY_LSHIFT]	||
				 stroke[KEY_RSHIFT]	);

	case KEY_CONTROL:
		return ( stroke[KEY_LCONTROL]	||
				 stroke[KEY_RCONTROL]	);

	case KEY_ALT:
		return ( stroke[KEY_LALT]	||
				 stroke[KEY_RALT]	);

	case KEY_ARROW:
		return ( stroke[KEY_UPARROW]		||
				 stroke[KEY_LEFTARROW]		||
				 stroke[KEY_RIGHTARROW]	||
				 stroke[KEY_DOWNARROW]		);

	case KEY_NUMPAD:
		return ( stroke[KEY_NUMPAD0]	||
				 stroke[KEY_NUMPAD1]	||
				 stroke[KEY_NUMPAD2]	||
				 stroke[KEY_NUMPAD3]	||
				 stroke[KEY_NUMPAD4]	||
				 stroke[KEY_NUMPAD5]	||
				 stroke[KEY_NUMPAD6]	||
				 stroke[KEY_NUMPAD7]	||
				 stroke[KEY_NUMPAD8]	||
				 stroke[KEY_NUMPAD9]	);

	case KEY_SUPER:
		return ( stroke[KEY_LWIN]	||
				 stroke[KEY_RWIN]	);

	case KEY_ANY:
		for ( auto i = 0UL; i < KEY_MAX-1; i++ ) {
			if ( stroke[i] ) {
				return true;
			}
		}
	}

	return false;
}

// End Of File

