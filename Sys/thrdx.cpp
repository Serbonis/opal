//========================================
// OPAL Game Library
//========================================
#include "thrd.hpp"

//========================================
// スレッドシステム
//========================================
using namespace opal;

namespace {
	enum {
		STEP_NULL,
		STEP_OPEN,
		STEP_EXEC,
		STEP_CLOSE,
		STEP_END,
	};
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
THRDT::THRDT() :
	hand( ( void* )-1L ),	// スレッドハンドル
	step( STEP_NULL ),		// 内部ステップ
	argv( nullptr ),		// 拡張データ
	size( 0 ),				// データサイズ
	sect( false ),			// クリティカルセクション
	thid( 0 ),				// スレッドID
	code( 0 ),				// スレッドの終了コード
	func( nullptr ),		// スレッドで実行する関数用ワーク
	level( 0 )				// 実行レベル
{}
THRDT::~THRDT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void THRDT::Init( const char* p ){

	TASK::Init( p );

	hand	= 0;
	step	= STEP_OPEN;
	argv	= nullptr;
	size	= 0;
	sect	= false;
	thid	= 0;
	code	= STILL_ACTIVE;
	func	= nullptr;
	level	= 0;
}

void THRDT::Free( void ){

	argv = nullptr;
	func = nullptr;

	TASK::Free();
}

//----------------------------------------
// 情報取得
//----------------------------------------
HANDLE THRDT::Handle( void ) const { return hand; }
void*  THRDT::Argv(   void ) const { return argv; }

//----------------------------------------
// 実行レベル
//----------------------------------------
void THRDT::Level( UINT n ){ level = n; }
UINT THRDT::Level( void ) const { return level; }

//----------------------------------------
// スレッドタスクの実行
//----------------------------------------
UINT CALLBACK THRDT::process( void* p ){

	if ( const auto t = reinterpret_cast<THRDT*>( p ) ) {
		while ( t->func( t ) ) {
			;
		}
	}
	// _endthreadex( retv );	// 返す値はこの関数に渡す
	return 0;					// _endthreadexを書くならここへは来ない
}

void THRDT::async( void ) {}
void THRDT::kill(  void ) {}

void THRDT::TaskMain( void ) {

	switch ( step ) {
	default: break;

	case STEP_OPEN:
		if ( hand = ( HANDLE )::_beginthreadex( nullptr, 0, process, this, 0, &thid ); hand == ( void* )-1L ) {
			break;
		}
		step = STEP_EXEC;
		[[fallthrough]];

	case STEP_EXEC:
		async();

		switch ( ::WaitForSingleObjectEx( hand, 0, false ) ) {
		case WAIT_OBJECT_0:			break;
		case WAIT_ABANDONED:		return;
		case WAIT_IO_COMPLETION:	return;
		case WAIT_TIMEOUT:			return;
		case -1:					break;
		}

		::GetExitCodeThread( hand, &code );

		kill();

		step = STEP_CLOSE;
		[[fallthrough]];

	case STEP_CLOSE:
		::SuspendThread( hand );
		::CloseHandle( hand );

		if ( const auto id = ID() ) {
			THRDX::tmap.erase( id );
		}
		TASK::Kill( 0 );

		step = STEP_NULL;
		break;
	}
}

// End Of File
