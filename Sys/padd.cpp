//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADD::PADD() :
	keyboard(),
	mouse(),
	joypad()
{}
PADD::~PADD(){}
PADT::PADT(){}
PADT::~PADT(){}

//----------------------------------------
// データのクリア
//----------------------------------------
void PADD::Clear( void ){

	keyboard.clear();
	mouse.clear();

	for ( auto i = 0UL; i < JOY_MAX; i++ ) {
		joypad[i].clear();
	}
}

//----------------------------------------
// データ領域へのアクセス
//----------------------------------------
KEYBOARD* PADD::Keyboard( void ){ return &keyboard;	}
MOUSE*    PADD::Mouse(    void ){ return &mouse;		}
JOYPAD*   PADD::Joypad(   void ){ return joypad;		}
JOYPAD*   PADD::Joypad( UINT p ){ return ( p < JOY_MAX ) ? &joypad[p] : nullptr;	}

// End Of File

