//========================================
// OPAL Game Library
//========================================
#ifdef OPAL_DEBUG
#include "debug.hpp"
#include "sys.hpp"

//========================================
// �f�o�b�O�V�X�e��
//========================================
using namespace opal;

//----------------------------------------
// �f�o�b�O���[�h
//----------------------------------------
namespace {
	UINT				cnt = 0;
	std::list<float>	fps(60);

	struct {
		const UINT	code;
		bool		flag;
	} key[] = {
		{ KEY_1, 	false, },	// 0
		{ KEY_2, 	false, },	// 1
		{ KEY_3, 	false, },	// 2
		{ KEY_4, 	false, },	// 3
		{ KEY_5, 	false, },	// 4
		{ KEY_6, 	false, },	// 5
		{ KEY_7, 	false, },	// 6
		{ KEY_8, 	false, },	// 7
		{ KEY_9, 	false, },	// 8
		{ KEY_0, 	false, },	// 9
		{ KEY_MINUS,false, },	// 10

		{ KEY_F1, 	false, },	// 11
		{ KEY_F2, 	false, },	// 12
		{ KEY_F3, 	false, },	// 13
		{ KEY_F4, 	false, },	// 14

		{ KEY_F5, 	false, },	// 15
		{ KEY_F6, 	false, },	// 16
		{ KEY_F7, 	false, },	// 17
		{ KEY_F8, 	false, },	// 18

		{ KEY_F9, 	false, },	// 19
		{ KEY_F10, 	false, },	// 20
		{ KEY_F11, 	false, },	// 21
		{ KEY_F12, 	false, },	// 22
	};

	PADS*	pad;
}

//----------------------------------------
// �V�X�e�����j�^�[
//----------------------------------------
void DEBUGX::Monitor( void ){

	pad = PADX::GetData( true );

	for ( auto i = 0UL; i < std::size( key ); i++ ) {
		if ( pad->KeyTrig( key[i].code ) ) {
			FLAG_CTRL( key[i].flag, FLAG_FLIP );
		}
	}

	Fps();

	if ( SIGX::Pause() ) {
		printd( "PAUSE\n" );
	}

	auto	n = 0;

	if ( key[n++].flag ) {
		Build();
		TIME::Debug( 0 );
		SIGX::Debug( 1 );
		TIME::Debug( 1 );
		TIME::Debug( 2 );
		WINDOWX::Debug();
		HEAP::Debug( 1 );
		Rand();
	}

	if ( key[n++].flag ) { Gauge();		}
	if ( key[n++].flag ) { Heap();		}
	if ( key[n++].flag ) { Sigx();		}
	if ( key[n++].flag ) { Padx();		}
	if ( key[n++].flag ) { Thrdx();		}
	if ( key[n++].flag ) { Filex(); 	}
	if ( key[n++].flag ) { Netwx();		}
	if ( key[n++].flag ) { Srvcx(); 	}
	if ( key[n++].flag ) { Primx(); 	}
	if ( key[n++].flag ) { Rsrcx(); 	}
	if ( key[n++].flag ) { Timer();		}
	if ( key[n++].flag ) { Camera();	}

	Objx(  key[n++].flag );
	Workx( key[n++].flag );
	Drawx( key[n++].flag );
	Callx( key[n++].flag );

	if ( key[n++].flag ) { Device();	}

	cnt++;
}

//----------------------------------------
// Build
//----------------------------------------
void DEBUGX::Build( void ){
#ifdef _MSC_VER
	printd( "BUILD  : MSC %d\n", _MSC_VER );
#else
	printd( "BUILD  : MinGW-64\n" );
#endif
}

//----------------------------------------
// Rand
//----------------------------------------
void DEBUGX::Rand( void ){

	printd( "SEED   : %ld\n", Seed() );
}

//----------------------------------------
// FPS
//----------------------------------------
void DEBUGX::Fps( void ){

	static auto			n = 0UL;
	static const char*	t[] = { "��","�^","��","�_","��","�^","��","�_", };
	static auto			s = 0.0f;

	const auto	p = TIME::Fps();

	fps.pop_front();
	fps.push_back( p );

	const auto	c = fps.size();

	if ( !(cnt % c) ) {
		s = 0.0f;
		for ( auto i = fps.begin(); i != fps.end(); ++i ) {
			s += *i;
		}
		s /= c;
	}

	printd( "FPS%s :%2.1f(%2.1f)\n", t[++n&7], p, s );
}

//----------------------------------------
// GAUGE
//----------------------------------------
void DEBUGX::Gauge( void ){

	for ( auto i = 0; i < 6; i++ ) {
		printd( "%d---------", i );
	}
	printd( "\n" );

	auto	n = 20;
	for ( const auto& i : fps ) {
		printd( "% 2.1f:", i );
		for ( auto j = ( UINT )i; j; --j ) {
			printd( "*" );
		}
		printd( "\n" );
		if ( !--n ) {
			break;
		}
	}
}

//----------------------------------------
// HEAP
//----------------------------------------
void DEBUGX::Heap( void ){

	printd( "��HEAP\n" );
	HEAP::Debug();
}

//----------------------------------------
// SIG
//----------------------------------------
void DEBUGX::Sigx( void ){

	printd( "��SIG\n" );
	SIGX::Debug();
}

//----------------------------------------
// PAD
//----------------------------------------
void DEBUGX::Padx( void ){

	printd( "��PAD\n" );
	PADX::Debug();
}

//----------------------------------------
// THREAD
//----------------------------------------
void DEBUGX::Thrdx( void ){

	printd( "��THREAD\n" );
	THRDX::Debug();
}

//----------------------------------------
// FILE
//----------------------------------------
void DEBUGX::Filex( void ){

	printd( "��FILE\n" );
	FILEX::Debug();
}

//----------------------------------------
// NETWORK
//----------------------------------------
void DEBUGX::Netwx( void ){

	printd( "��NETWORK\n" );
	NETWX::Debug();
}

//----------------------------------------
// SERVICE
//----------------------------------------
void DEBUGX::Srvcx( void ){

	printd( "��SERVICE\n" );
	SRVCX::Debug();
}

//----------------------------------------
// RESOURCE
//----------------------------------------
void DEBUGX::Rsrcx( void ){

	printd( "��RESOURCE\n" );
	RSRCX::Debug();
}

//----------------------------------------
// PRIMITIVE
//----------------------------------------
void DEBUGX::Primx( void ){

	printd( "��PRIMITIVE\n" );
	PRIMX::Debug();
}

//----------------------------------------
// TIMER
//----------------------------------------
void DEBUGX::Timer( void ){

	printd( "��TIMER\n" );
	TIME::Debug( 2 );
}

//----------------------------------------
// DEVICE
//----------------------------------------
void DEBUGX::Device( void ){

	printd( "��GRAPHIC DEVICE\n" );
	DRAWX::Device();
}

//----------------------------------------
// SPEC
//----------------------------------------
void DEBUGX::Spec( void ){

	printd( "��GRAPHIC SPEC\n" );
	DRAWX::Spec();
}

//----------------------------------------
// OBJECT
//----------------------------------------
template<typename TYPE>void debugmode( const PADS* p = pad ){

	if ( const auto md = []( auto p ){
		if ( p->KeyTrig( KEY_DECIMAL     ) ) { return   0;	}
		if ( p->KeyTrig( KEY_NUMPAD0     ) ) { return   1;	}
		if ( p->KeyTrig( KEY_NUMPAD1     ) ) { return   2;	}
		if ( p->KeyTrig( KEY_NUMPAD2     ) ) { return   3;	}
		if ( p->KeyTrig( KEY_NUMPAD3     ) ) { return   4;	}
		if ( p->KeyTrig( KEY_NUMPAD4     ) ) { return   5;	}
		if ( p->KeyTrig( KEY_NUMPAD5     ) ) { return   6;	}
		if ( p->KeyTrig( KEY_NUMPAD6     ) ) { return   7;	}
		if ( p->KeyTrig( KEY_NUMPAD7     ) ) { return   8;	}
		if ( p->KeyTrig( KEY_NUMPAD8     ) ) { return   9;	}
		if ( p->KeyTrig( KEY_NUMPAD9     ) ) { return  10;	}
		if ( p->KeyTrig( KEY_DIVIDE      ) ) { return  11;	}
		if ( p->KeyTrig( KEY_MULTIPLY    ) ) { return  12;	}
		if ( p->KeyTrig( KEY_SUBTRACT    ) ) { return  13;	}
		if ( p->KeyTrig( KEY_ADD         ) ) { return  14;	}
		if ( p->KeyTrig( KEY_NUMPADENTER ) ) { return  15;	}
		return -1;
	}( p ); md >= 0 ) {
		TYPE::Debug( TYPE::Debug() != md ? md : -1 );
	}
}

template<typename TYPE>void DEBUGX::debugobj( const char* obj, bool f ){

	TYPE::index->Info( f );

	if ( f ) {
		const auto	l0 = TYPE::index;
		const auto	l1 = TYPE::Linx();

		printd( "��%s\n", obj );
		printd( "INDEX : %d\n", l0->Elements() );
		printd( "LINX  : %d\n", l1 ? l1->Elements() : 0 );
		printd( "----------------------------------------\n" );
		debugmode<TYPE>();
	}
}

void DEBUGX::Objx(  bool f ){ debugobj<OBJX >( "OBJ",  f );	}
void DEBUGX::Workx( bool f ){ debugobj<WORKX>( "WORK", f );	}
void DEBUGX::Drawx( bool f ){ debugobj<DRAWX>( "DRAW", f );	}
void DEBUGX::Callx( bool f ){ debugobj<CALLX>( "CALL", f );	}

//----------------------------------------
// CAMERA
//----------------------------------------
void DEBUGX::Camera( void ){

	const auto	cm = DRAWX::Camera();
	const auto	ca = DRAWX::CameraAngle();

	printd( "��CAMERA\n" );
	printd( "SYSTEM%c:%p\n", DRAWX::SysCamera() ? '+' : '-', cm );
	printd( "----------------------------------------\n" );
	printd( "[SYSTEM]\n" );
	printd( "----------------------------------------\n" );
	printd( "CLIP  :%f �` %f\n", DRAWX::CameraClipN(), DRAWX::CameraClipF() );
	printd( "ANGLE :%f(%f)\n", DEG( ca ), ca );
	printd( "\n" );

	if ( cm ) {
		const auto	ca = cm->GetFov();

		printd( "----------------------------------------\n" );
		printd( "[USER] %p\n", cm );
		printd( "----------------------------------------\n" );
		printd( "CLIP  :%f �` %f\n", cm->GetNear(), cm->GetFar() );
		printd( "ANGLE :%f\n", DEG( ca ), ca );
		printd( "\n" );
	}
}
#endif

// End Of File
