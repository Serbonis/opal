//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void SRVCX::Debug( UINT m ){

	for ( const auto& s : sval ) {
		printd( "%04d %s\n", s->GetServicePrio(), s->GetServiceName() );
	}
}
#endif

// End Of File
