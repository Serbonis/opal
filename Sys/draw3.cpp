//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックタスク(3D)
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWT3::DRAWT3(){}
DRAWT3::~DRAWT3(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void DRAWT3::Init( const char* p ){

	DRAWT::Init( p );

	SetDim3( true );
	SetPers( true );
}

void DRAWT3::Free( void ){

	DRAWT::Free();
}

//----------------------------------------
// Info
//----------------------------------------
void DRAWT3::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "DRAWT3" );
	printd( "%-08s ", ID() );
	DRAWT3::DrawInfo( 0 );
	printd( "\n" );
#endif
}

void DRAWT3::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
#endif
}

// End Of File
