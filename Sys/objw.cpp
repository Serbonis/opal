//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトタスク
//========================================
using namespace opal;

//----------------------------------------
// Etc
//----------------------------------------
void OBJT::ObjFunc(  void ){}
void OBJT::ObjBegin( void ){}
void OBJT::ObjMain(  void ){}
void OBJT::ObjEnd(   void ){}
void OBJT::ObjKill(  void ){}

//----------------------------------------
// Info
//----------------------------------------
void OBJT::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "OBJT" );
	printd( "%-08s ", ID() );
	OBJT::ObjInfo( OBJX::Debug() );
	printd( "\n" );
#endif
}

void OBJT::ObjInfo( int m ) const {
#ifdef OPAL_DEBUG
	switch ( m ) {
	default:break;
	case  1: printd( "Prio %02x|%02d ", GetPrio(), GetLevel() );	break;
	case  2: printd( "Level %d|%d %c ", GetLevel( SIGL::PRIMAL ), GetLevel( SIGL::FINAL  ), GetFunc( 0 ) ? '+' : '-' );	break;
	}
#endif
}

// End Of File
