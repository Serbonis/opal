//========================================
// OPAL Game Library
//========================================
#include "xcall.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;

//========================================
// DirectSound
//========================================
namespace opal::CALLX_DS {
	extern void SysInitDevice( void );
	extern void SysFreeDevice( void );
}

//========================================
// XAudio2
//========================================
namespace opal::CALLX_XA {
	extern void SysInitDevice( void );
	extern void SysFreeDevice( void );
}

//========================================
// サウンドシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal::CALLX_ {
	void SysInitDevice( void ){
		CALLX_DS::SysInitDevice();
		CALLX_XA::SysInitDevice();
	}
	void SysFreeDevice( void ){
		CALLX_DS::SysFreeDevice();
		CALLX_XA::SysFreeDevice();
	}
}

// End Of File

