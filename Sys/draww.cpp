//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックタスク
//========================================
using namespace opal;

//----------------------------------------
// Etc
//----------------------------------------
void DRAWT::DrawInit(  void ){ ZOBJ::TaskInit();	}
void DRAWT::DrawEnter( void ){ ZOBJ::TaskEnter();	}
void DRAWT::DrawFunc(  void ){ ZOBJ::TaskFunc();	}
void DRAWT::DrawBegin( void ){ ZOBJ::TaskBegin();	}
void DRAWT::DrawMain(  void ){ ZOBJ::TaskMain();	}
void DRAWT::DrawEnd(   void ){ ZOBJ::TaskEnd();		}
void DRAWT::DrawExit(  void ){ ZOBJ::TaskExit();	}
void DRAWT::DrawFree(  void ){ ZOBJ::TaskFree();	}

void DRAWT::DrawAnime( void ){}
void DRAWT::DrawAnime( int  ){}

//----------------------------------------
// Info
//----------------------------------------
void DRAWT::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "DRAWT" );
	printd( "%-08s ", ID() );
	DRAWT::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
	}
#endif

void DRAWT::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	switch ( m ) {
	default:break;
	case  1: printd( "Prio %02x|%02d ", GetPrio(), GetLevel() );	break;
	case  2: printd( "Level %d|%d %c ", GetLevel( SIGL::PRIMAL ), GetLevel( SIGL::FINAL  ), GetFunc( 0 ) ? '+' : '-' );	break;
	case  3: printd( "Parent %c%c ",  Parent()  ? 'P' : '-', Adopter() ? 'A' : '-' );	break;
	case  4: printd( "DISP %c%c ",  GetDisp(  0 ) ? '+' : '-', GetDisp(  1 ) ? '+' : '-' );	break;
	case  5: printd( "ANIME %c%c ", GetAnime( 0 ) ? '+' : '-', GetAnime( 1 ) ? '+' : '-' );	break;
	}
#endif
}

// End Of File
