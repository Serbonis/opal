//========================================
// OPAL Game Library
//========================================
#include "Time.hpp"

//========================================
// タイマーシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
namespace {
	constexpr auto	PRC = 1000;	// 1 sec = 1000 msec
}

DECLVAL( TIME::fps   ){};
DECLVAL( TIME::seed  ){};
DECLVAL( TIME::freq  ){};
DECLVAL( TIME::now	 ){};
DECLVAL( TIME::old   ){};
DECLVAL( TIME::dlt   ){};
DECLVAL( TIME::time  ){};
DECLVAL( TIME::frame ){};

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void TIME::Init( void ){

	::timeBeginPeriod( 1 );

	::QueryPerformanceFrequency( &freq );	// 周波数取得
	::QueryPerformanceCounter(   &seed );	// 計測開始時間

	now.LowPart		= 0;
	now.HighPart	= 0;

	switch ( OPAL::FRAME() ) {
	default:				fps = 60;	break;
	case OPAL::FIXED60:		fps = 60;	break;
	case OPAL::FIXED30:		fps = 30;	break;
	case OPAL::FIXED20:		fps = 20;	break;
	case OPAL::FIXED10:		fps = 10;	break;
	}

	old	= seed;
	dlt	= PRC/fps;	// 1000 ms/60 frame	= 16.6 ms
}

void TIME::Free( void ){

	::timeEndPeriod( 1 );
}

//----------------------------------------
// システム初期化
//----------------------------------------
bool TIME::SysInit( void ){

	time	= Time();
	frame	= 0;

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool TIME::SysExec( void ){

	++frame;

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool TIME::SysFree( void ){

	return true;
}

//----------------------------------------
// タイマー計測
//----------------------------------------
void TIME::Sleep( UINT r ){

	do {
		::QueryPerformanceCounter( &now );

		dlt = static_cast<double>( ( now.QuadPart - old.QuadPart ) * PRC / freq.QuadPart );

		if ( const auto s = r * PRC / fps - dlt; s >= 0 ) {
			::timeBeginPeriod( 1 );
			::Sleep( s ? s : 1 );
			::timeEndPeriod( 1 );
		} else {
			break;
		}
	} while ( 1 );
	old = now;
}

//----------------------------------------
// 情報取得
//----------------------------------------
DWORD TIME::Seed( void ){ return seed.LowPart; }
DWORD TIME::Time( void ){

	LARGE_INTEGER	time;

	::QueryPerformanceCounter( &time );

	return static_cast<DWORD>( ( time.QuadPart - seed.QuadPart ) * PRC / freq.QuadPart );
}

//----------------------------------------
// FPS
//----------------------------------------
float TIME::FPS( void ){ return static_cast<float>( fps     );	}
float TIME::Fps( void ){ return static_cast<float>( PRC/dlt );	}

//----------------------------------------
// 差分
//----------------------------------------
float TIME::Delta( void    ){ return static_cast<float>( dlt   );	}
float TIME::Delta( float v ){ return static_cast<float>( v/dlt );	}

//----------------------------------------
// 時間
//----------------------------------------
float TIME::Second( float s ){ return fps *  1 * s; 	}
float TIME::Minute( float m ){ return fps * 60 * m;		}
int   TIME::Second( void    ){ return fps *  1;			}
int   TIME::Minute( void    ){ return fps * 60;			}

//----------------------------------------
// フレーム調整
//----------------------------------------
float TIME::Frame( float s ){

	switch ( OPAL::FRAME() ) {
	default:break;
	case OPAL::FIXED60:		return s * 1;
	case OPAL::FIXED30:		return s * 2;
	case OPAL::FIXED20:		return s * 3;
	case OPAL::FIXED10:		return s * 6;
	}

	return s * 60 / Fps();
}

DWORD TIME::Frame( void ){

	return frame;
}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void TIME::Debug( UINT m ) {

	switch ( m ) {
	case 0:
		{
			static const char*	f[] = { "VARIABLE", "FIXED60", "FIXED30", "FIXED20", "FIXED10", };

			printd( "RATE   : %s\n", f[OPAL::FRAME()] );
		}
		break;

	case 1:
		{
			static const auto	debug = []( auto v, auto t ){

									t[3] = v % PRC;	v /= PRC;
									t[2] = v % fps;	v /= fps;
									t[1] = v % 60;	v /= 60;
									t[0] = v;
								};

			DWORD		ts[4];
			DWORD		tg[4];

			debug( Time(),      ts );
			debug( Time()-time, tg );

			printd( "TIME   : %d\'%02d\"%02d.%04d(%d\'%02d\"%02d.%04d)\n",
					ts[0],ts[1],ts[2],ts[3],
					tg[0],tg[1],tg[2],tg[3] );
		}
		break;

	case 2:
		printd( "FRAME  : %ld\n", frame );
		break;
	}
}
#endif

// End Of File
