//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

//========================================
// シグナルタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SIGT::SIGT() :
	func(),
	argv(),
	stat( false ),
	level( 0 )
{}

//----------------------------------------
// リセット＆クリア
//----------------------------------------
void SIGT::Reset( void ){

	func.clear();
	argv.clear();
	stat  = false;
	level = 0;
}

void SIGT::Clear( void ){

	func.clear();
	argv.clear();
	stat  = false;
}

//----------------------------------------
// 設定＆削除
//----------------------------------------
void SIGT::Set( const char* p, SIGX::FUNC f, SIGX::ARGV a ){

	if ( p ) {
		func[p] = f;
		argv[p] = a;
	}
}

void SIGT::Del( const char* p ){

	if ( p ) {
		func.erase( p );
		argv.erase( p );
	}
}

//----------------------------------------
// 送出
//----------------------------------------
void SIGT::Kill( const char* p ){

	if ( p ) {
		if ( const auto f = func[p] ) {
			switch ( f( argv[p] ) ) {
			case 0:
				Del( p );
				break;

			default:
				break;
			}
		}
	} else {
		std::vector<std::string>	name;

		for ( const auto& [s,v] : func ) {
			( void )v;
			name.push_back( s );
		}

		for ( const auto n : name ) {
			Kill( n.c_str() );
		}
	}
}

//========================================
// シグナルレベル
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SIGL::SIGL() :
	level{}
{}

//----------------------------------------
// レベル設定
//----------------------------------------
void SIGL::SetLevel( UINT v, LEVEL l ){ level[l] = v;		}
void SIGL::SetLevel( void ){ SetLevel( LEVEL_INFINITE );	}

UINT SIGL::GetLevel( LEVEL l ) const { return level[l];		}
UINT SIGL::GetLevel( void ) const { return ( level[FORCE] > 0 ) ? level[FORCE] : level[FINAL];	}

// End Of File
