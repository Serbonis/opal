//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

namespace {
	struct CACHE final {
		explicit CACHE() :
			material{MATERIAL()}
		{}

		MATERIAL	material;
	}	cache;	// SysInitでクリア
}

#if 0	// DX9 に合わせていたダミー
//----------------------------------------
// 行列設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void Proj(  const MATRIX* p ){}
	void View(  const MATRIX* p ){}
	void World( const MATRIX* p ){}
}

//----------------------------------------
// FVF設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void FVF( DWORD vFvf ){}
}

//----------------------------------------
// カリング
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void Culling( DWORD vCull ){}
}

//----------------------------------------
// マテリアル設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void Material( const MATERIAL* pMate ){}
}

//----------------------------------------
// テクスチャ設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void Texture( DWORD vStg, const TEXTURES* pText ){}
}

//----------------------------------------
// αブレンド
//----------------------------------------
void DRAWB::Alpha( bool af ) const {}
#endif

// End Of File
