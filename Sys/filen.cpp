//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルシステム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
FILEN::FILEN( const char* f ) :
	path( nullptr ),
	file( f )
{}
FILEN::FILEN( const char* p, const char* f ) :
	path( p ),
	file( f )
{}

//----------------------------------------
// ファイル名データ
//----------------------------------------
NAME FILEN::operator()( void    ) const { return path + file;	}
NAME FILEN::Path( const char* f ) const { return path + f;		}
NAME FILEN::File( const char* p ) const { return p    + file;	}
void FILEN::SetPath( const char* p ){ path = p; }
void FILEN::SetFile( const char* f ){ file = f; }
const char* FILEN::GetPath( void ) const { return path; }
const char* FILEN::GetFile( void ) const { return file; }

//----------------------------------------
// ファイルデータ
//----------------------------------------
FILEN_S::FILEN_S( const char* f, UINT w, UINT h ) : FILEN( f ), CSIZEU( w, h ) {}
FILEN_S::FILEN_S( const char* f, const CSIZEU& s ): FILEN( f ), CSIZEU( s ) {}
FILEN_S::FILEN_S( const char* f, const SSIZEU& s ): FILEN( f ), CSIZEU( s.w, s.h ) {}
FILEN_S::FILEN_S( const char* f ) : FILEN( f ) {}
FILEN_S::FILEN_S( UINT w, UINT h  ) : CSIZEU( w, h ) {}
FILEN_S::FILEN_S( const CSIZEU& s ) : CSIZEU( s ) {}
FILEN_S::FILEN_S( const SSIZEU& s ) : CSIZEU( s.w, s.h ) {}

FILEN_R::FILEN_R( const char* f, UINT w, UINT h, UINT u, UINT v ) : FILEN( f ), CRECTU( w, h, u, v ) {}
FILEN_R::FILEN_R( const char* f, const CRECTU& s ) : FILEN( f ), CRECTU( s.X(), s.Y(), s.W(), s.H() ) {}
FILEN_R::FILEN_R( const char* f, const SRECTU& s ) : FILEN( f ), CRECTU( s.x, s.y, s.w, s.h ) {}
FILEN_R::FILEN_R( const char* f ) : FILEN( f ) {}
FILEN_R::FILEN_R( UINT w, UINT h, UINT u, UINT v ) : CRECTU( w, h, u, v ) {}
FILEN_R::FILEN_R( const CRECTU& s ) : CRECTU( s.X(), s.Y(), s.W(), s.H() ) {}
FILEN_R::FILEN_R( const SRECTU& s ) : CRECTU( s.x, s.y, s.w, s.h ) {}

FILEN_R::FILEN_R( const char* f, UINT w, UINT h ) : FILEN( f ), CRECTU( w, h ) {}
FILEN_R::FILEN_R( const char* f, const CSIZEU& s ): FILEN( f ), CRECTU( s.W(), s.H() ) {}
FILEN_R::FILEN_R( const char* f, const SSIZEU& s ): FILEN( f ), CRECTU( s.w, s.h ) {}
FILEN_R::FILEN_R( UINT w, UINT h  ) : CRECTU( w, h ) {}
FILEN_R::FILEN_R( const CSIZEU& s ) : CRECTU( s.W(), s.H() ) {}
FILEN_R::FILEN_R( const SSIZEU& s ) : CRECTU( s.w, s.h ) {}

float FILEN_R::U( void ) const { return X() ? ( float )W()/X() : 0.0f;	}
float FILEN_R::V( void ) const { return Y() ? ( float )H()/Y() : 0.0f;	}

opal::SSIZE FILEN_R::Unit( void ) const { return SSIZE{ U(), V() };	}

//----------------------------------------
// 拡張子判定
//----------------------------------------
bool FILEN::Kind( const char* p, const char* k ){

	if ( const auto n = strlen_s( p ) ) {
		if ( const auto s = strlen_s( k ) ) {
			if ( !strncmp( &p[n-s], k, s ) ) {
				return true;
			}
		}
	}
	return false;
}

// End Of File
