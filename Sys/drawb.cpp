//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include <d3d11.h>

//========================================
// αブレンド
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWB::DRAWB() :
	test(),
	blend()
{}
DRAWB::~DRAWB(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void DRAWB::Init( void ){

	new ( &test  ) TEST;
	new ( &blend ) BLEND;
}
void DRAWB::Free( void ){}

//----------------------------------------
// αテスト
//----------------------------------------
DRAWB::TEST::TEST() :
	flag{true},
	ref{0x80},
	func{D3DCMP_GREATEREQUAL}
{}

void  DRAWB::SetAlphaTest(     int   f ){ FLAG_CTRL( test.flag, f );	}
void  DRAWB::SetAlphaTestRef(  UCHAR v ){ test.ref	= v; 				}
void  DRAWB::SetAlphaTestFunc( DWORD v ){ test.func	= v; 				}

bool  DRAWB::GetAlphaTest(     void ) const { return test.flag;			}
UCHAR DRAWB::GetAlphaTestRef(  void ) const { return test.ref;			}
DWORD DRAWB::GetAlphaTestFunc( void ) const { return test.func;			}

void  DRAWB::SetAlphaTest( UCHAR vr, DWORD vf ){

	test.ref	= vr;
	test.func	= vf;
}

//----------------------------------------
// αブレンド
//----------------------------------------
DRAWB::BLEND::BLEND() :
	flag{true},
	type{ALPHA_NONE},
	c{ D3DBLENDOP_ADD, D3DBLEND_ONE, D3DBLEND_ONE },
	a{ D3DBLENDOP_ADD, D3DBLEND_ONE, D3DBLEND_ONE },
	factor{ 1.0f, 1.0f, 1.0f, 1.0f },
	mask{D3D11_DEFAULT_SAMPLE_MASK}
{}

void  DRAWB::SetAlphaBlend( int f ){ FLAG_CTRL( blend.flag, f );	}
bool  DRAWB::GetAlphaBlend( void ) const { return blend.flag;		}

void  DRAWB::SetAlphaBlendType( UINT t ){ blend.type = t;			}
UINT  DRAWB::GetAlphaBlendType( void ) const { return blend.type;	}

void DRAWB::SetAlphaBlendDesc( DWORD d0, DWORD d1, DWORD d2, UINT n ){

	if ( !(n%2) ) {
		blend.c[0] = d0;
		blend.c[1] = d1;
		blend.c[2] = d2;
	} else {
		blend.a[0] = d0;
		blend.a[1] = d1;
		blend.a[2] = d2;
	}
}

void DRAWB::SetAlphaBlendDesc( DWORD c0, DWORD c1, DWORD c2, DWORD a0, DWORD a1, DWORD a2 ){

	blend.c[0] = c0;
	blend.c[1] = c1;
	blend.c[2] = c2;
	blend.a[0] = a0;
	blend.a[1] = a1;
	blend.a[2] = a2;
}

auto DRAWB::GetAlphaBlendDesc( UINT n ) const->std::tuple<DWORD,DWORD,DWORD>{

	if ( !(n%2) ) {
		return { blend.c[0],blend.c[1],blend.c[2] };
	} else {
		return { blend.a[0],blend.a[1],blend.c[2] };
	}
}

auto DRAWB::GetAlphaBlendDesc( void ) const->std::tuple<DWORD,DWORD,DWORD,DWORD,DWORD,DWORD>{

	return { blend.c[0],blend.c[1],blend.c[2], blend.a[0],blend.a[1],blend.c[2] };
}

void   DRAWB::SetAlphaBlendFactor( float r, float g, float b, float a ){

	blend.factor[0] = r;
	blend.factor[1] = g;
	blend.factor[2] = b;
	blend.factor[3] = a;
}
void   DRAWB::SetAlphaBlendFactor( float r, float g, float b ){

	blend.factor[0] = r;
	blend.factor[1] = g;
	blend.factor[2] = b;
}
void   DRAWB::SetAlphaBlendFactor( float a ){

	blend.factor[3] = a;
}
auto DRAWB::GetAlphaBlendFactor( void ) const->std::array<float,4>{

	return { blend.factor[0],
			 blend.factor[1],
			 blend.factor[2],
			 blend.factor[3] };
}

auto DRAWB::GetAlphaBlendFactor( bool f ) const->const float*{

	if ( f ) {
		return blend.factor;
	}

	if ( blend.c[1] == D3D11_BLEND_BLEND_FACTOR ||
		 blend.c[2] == D3D11_BLEND_BLEND_FACTOR ||
		 blend.a[1] == D3D11_BLEND_INV_BLEND_FACTOR ||
		 blend.a[2] == D3D11_BLEND_INV_BLEND_FACTOR ) {
		return blend.factor;
	}

	return nullptr;
}

void DRAWB::SetAlphaBlendMask( UINT v ){ blend.mask = v; 			}
UINT DRAWB::GetAlphaBlendMask( void ) const { return blend.mask;	}

// End Of File
