//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "Draw/lights.hpp"

//========================================
// システムライト
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
LIGHTS*	DRAWX::light;

//----------------------------------------
// システムライト
//----------------------------------------
void DRAWX::InitLight( void ){ NEWS( light );DRAWX_::InitLight( light );	}
void DRAWX::FreeLight( void ){ DRAWX_::FreeLight( light );DELETES( light );	}
void DRAWX::ExecLight( void ){ DRAWX_::ExecLight( light );	}

//----------------------------------------
// システムライトのON/OFF
//----------------------------------------
void DRAWX::SysLight( bool vFlag ){  light->SysLight( vFlag );	}
bool DRAWX::SysLight( void ){ return light->SysLight(); }

//----------------------------------------
// システムライトの個数
//----------------------------------------
UINT DRAWX::SysLights( void ){ return light->SysLights();	}

//----------------------------------------
// システムライトの取得/設定
//----------------------------------------
const LIGHTX* DRAWX::Light( const LIGHTX* pLight, UINT vID ){ return light->Light( pLight, vID );	}
const LIGHTX* DRAWX::Light( UINT vID ){ return light->Light( vID );	}
void DRAWX::Lighting( UINT vID, bool vFlag ){ light->Lighting( vID, vFlag );	}
bool DRAWX::Lighting( UINT vID ){ return light->Lighting( vID );	}

//----------------------------------------
// 環境光
//----------------------------------------
COLOR DRAWX::Ambient( COLOR vCol ){ return light->Ambient( vCol );	}
COLOR DRAWX::Ambient( UCHAR vR, UCHAR vG, UCHAR vB, UCHAR vA ){ return light->Ambient( vR, vG, vB, vA  );	}
COLOR DRAWX::Ambient( void ){ return light->Ambient(); }

//----------------------------------------
// システムライト位置と方向
//----------------------------------------
void DRAWX::LightPosition(  UINT vID, const VECTOR3* pPos ){ light->LightPosition(  vID, pPos );	}
void DRAWX::LightDirection( UINT vID, const VECTOR3* pDir ){ light->LightDirection( vID, pDir );	}

void DRAWX::LightPosition(  const VECTOR3* pPos ){ light->LightPosition(  pPos );	}
void DRAWX::LightDirection( const VECTOR3* pDir ){ light->LightDirection( pDir );	}
void DRAWX::LightPosition(  UINT vID, float vX, float vY, float vZ ){ light->LightPosition(  vX, vY, vZ );	}
void DRAWX::LightDirection( UINT vID, float vX, float vY, float vZ ){ light->LightDirection( vX, vY, vZ );	}
void DRAWX::LightPosition(  float vX, float vY, float vZ ){ light->LightPosition(  vX, vY, vZ );	}
void DRAWX::LightDirection( float vX, float vY, float vZ ){ light->LightDirection( vX, vY, vZ );	}
const VECTOR3* DRAWX::LightPosition(  UINT vID ){ return light->LightPosition(  vID );	}
const VECTOR3* DRAWX::LightDirection( UINT vID ){ return light->LightDirection( vID );	}

VECTOR3 DRAWX::LightPosition(  void ){ return LIGHTS::LightPosition();	}
VECTOR3 DRAWX::LightDirection( void ){ return LIGHTS::LightDirection();	}
COLOR   DRAWX::LightDiffuse(   void ){ return LIGHTS::LightDiffuse();	}

//----------------------------------------
// システムライト計算
//----------------------------------------
void DRAWX::DrawLight( void ){ ExecLight(); }

void DRAWX::DrawLight( const VECTOR3& pos,
					   const VECTOR3& dir,
					   DLIGHT& prop ){

	light->DrawLight( pos, dir, prop );
}

// End Of File
