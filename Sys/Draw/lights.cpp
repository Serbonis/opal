//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "Draw/lights.hpp"

//========================================
// システムライト
//========================================
using namespace opal;

//----------------------------------------
// システムライト
//----------------------------------------
namespace opal::LIGHTS_C {
	const DLIGHT	property;
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
LIGHTS::LIGHTS(){}
LIGHTS::~LIGHTS(){}

//----------------------------------------
// システムライトのON/OFF
//----------------------------------------
void LIGHTS::SysLight( bool vFlag ){  system = vFlag;	}
bool LIGHTS::SysLight( void ) const { return system; }

//----------------------------------------
// システムライトの個数
//----------------------------------------
UINT LIGHTS::SysLights( void ) const { return instance.size();	}

//----------------------------------------
// システムライトの取得/設定
//----------------------------------------
const LIGHTX* LIGHTS::Light( const LIGHTX* pLight, UINT vID ){

	const auto	n = instance.size();

	if ( vID < n ) {
		auto&		p = instance[vID];
		const auto	a = p.active;

		if ( ( p.active = pLight ) ) {
			p.flag	= &pLight->active;
			p.prop	= &pLight->light;
			p.pos	= &pLight->pos;
			p.dir	= &pLight->dir;
		} else {
			p.flag	= &p.flag_s;
			p.prop	= &p.prop_s;
			p.pos	= &p.pos_s;
			p.dir	= &p.dir_s;
		}
		return a;
	}
	return nullptr;
}

const LIGHTX* LIGHTS::Light( UINT vID ) const {

	return ( vID < instance.size() ) ? instance[vID].active : nullptr;
}

void LIGHTS::Lighting( UINT vID, bool vFlag ){

	if ( vID < instance.size() ) {
		instance[vID].flag_s = vFlag;
	}
}

bool LIGHTS::Lighting( UINT vID ) const {

	return ( vID < instance.size() ) ? instance[vID].flag_s : false;
}

//----------------------------------------
// 環境光
//----------------------------------------
COLOR LIGHTS::Ambient( COLOR vCol ){

	const auto	c = ambient;

	ambient = vCol;

	return c;
}

COLOR LIGHTS::Ambient( UCHAR vR, UCHAR vG, UCHAR vB, UCHAR vA ){

	return Ambient( COLOR( vR, vG, vB, vA ) );
}

COLOR LIGHTS::Ambient( void ) const { return ambient; }

//----------------------------------------
// システムライト位置と方向
//----------------------------------------
void LIGHTS::LightPosition(  UINT vID, const VECTOR3* pPos ){

	if ( vID < instance.size() ) {
		instance[vID].pos_s = ( pPos ) ? *pPos : LIGHTS_C::property.Position;
	}
}

void LIGHTS::LightDirection( UINT vID, const VECTOR3* pDir ){

	if ( vID < instance.size() ) {
		instance[vID].dir_s = ( pDir ) ? *pDir : LIGHTS_C::property.Direction;
	}
}

void LIGHTS::LightPosition(  const VECTOR3* pPos ){ LightPosition(  0, pPos );	}
void LIGHTS::LightDirection( const VECTOR3* pDir ){ LightDirection( 0, pDir );	}
void LIGHTS::LightPosition(  UINT vID, float vX, float vY, float vZ ){ VECTOR3 p( vX, vY, vZ ); LightPosition(  0, &p );	}
void LIGHTS::LightDirection( UINT vID, float vX, float vY, float vZ ){ VECTOR3 d( vX, vY, vZ ); LightDirection( 0, &d );	}
void LIGHTS::LightPosition(  float vX, float vY, float vZ ){ LightPosition(  0, vX, vY, vZ );	}
void LIGHTS::LightDirection( float vX, float vY, float vZ ){ LightDirection( 0, vX, vY, vZ );	}

const VECTOR3* LIGHTS::LightPosition(  UINT vID ) const { return ( vID < instance.size() ) ? &instance[vID].pos_s : nullptr;	}
const VECTOR3* LIGHTS::LightDirection( UINT vID ) const { return ( vID < instance.size() ) ? &instance[vID].dir_s : nullptr;	}

VECTOR3 LIGHTS::LightPosition(  void ){ return LIGHTS_C::property.Position; 	}
VECTOR3 LIGHTS::LightDirection( void ){ return LIGHTS_C::property.Direction;	}
COLOR   LIGHTS::LightDiffuse(   void ){ return LIGHTS_C::property.Diffuse;		}

//----------------------------------------
// システムライト計算
//----------------------------------------
void LIGHTS::DrawLight( const VECTOR3& pos,
						const VECTOR3& dir,
						DLIGHT& prop ){

	prop.Position.x		= pos.x;
	prop.Position.y		= pos.y;
	prop.Position.z		= pos.z;
	prop.Direction.x	= dir.x;
	prop.Direction.y	= dir.y;
	prop.Direction.z	= dir.z;
}

//----------------------------------------
// システムライト
//----------------------------------------
void LIGHTS::InitLight( LIGHTS* light ){
#if 0
	light->system	= false;
	light->ambient	= COLOR_RGB( 0xff,0xff,0xff );

	if ( const auto n = device.Caps() ) {
		for ( auto i = 0UL; i < n; i++ ) {
			light->instance.emplace_back();
		}

		for ( auto i = 0UL; i < n; i++ ) {
			auto&	p	= light->instance[i];

			p.active	= nullptr;
			p.flag		= &p.flag_s;
			p.prop	 	= &p.prop_s;
			p.pos	 	= &p.pos_s;
			p.dir	 	= &p.dir_s;
			p.flag_s	= false;
			p.prop_s 	= LIGHTS_C::property;
			p.pos_s		= LIGHTS_C::property.Position;
			p.dir_s		= LIGHTS_C::property.Direction;
		}
	}
#endif
}

void LIGHTS::ExecLight( LIGHTS* light ){
#if 0
	const auto	dv = device();

	dv->SetRenderState( D3DRS_LIGHTING, light->system  );
	dv->SetRenderState( D3DRS_AMBIENT,  light->ambient );

	if ( light->system ) {
		const auto	n = light->instance.size();

		for ( auto i = 0UL; i < n; i++ ) {
			auto&	p = light->instance[i];

			if ( *p.flag ) {
				auto	l = *p.prop;

				LIGHTS::DrawLight( *p.pos, *p.dir, l );

				dv->SetLight( i, &l );
				dv->LightEnable( i, true );

			} else {
				dv->LightEnable( i, false );
			}
		}
	}
#endif
}

void LIGHTS::FreeLight( LIGHTS* light ){
#if 0
	light->instance.clear();
#endif
}

// End Of File
