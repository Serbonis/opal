//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "Draw/cameras.hpp"

//========================================
// システムカメラ
//========================================
using namespace opal;

//----------------------------------------
// システムカメラ
//----------------------------------------
namespace opal::CAMERAS_C {
	const VECTOR3	pos		= VECTOR3( 0.0f, 0.0f, -10.0f );
	const float 	fov		= RADIAN( 45.0f );
	const CLIPF 	clip	= CLIPF( 1.0f, 1000.0f );
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
CAMERAS::CAMERAS(){}
CAMERAS::~CAMERAS(){}

//----------------------------------------
// システムカメラのON/OFF
//----------------------------------------
void CAMERAS::SysCamera( bool f ){ system = f;	}
bool CAMERAS::SysCamera( void ) const { return system; 	}

//----------------------------------------
// システムカメラの取得/設定
//----------------------------------------
const CAMERAX* CAMERAS::Camera( const CAMERAX* p ){

	const auto	a = active;

	if ( ( active = p ) ) {
		view	= &p->view;
		pers	= &p->pers;	vpers	= &p->vpers;
		orth	= &p->orth;	vorth 	= &p->vorth;
		vinv	= &p->vinv;
		bill	= &p->bill;
	} else {
		view	= &view_s;
		pers	= &pers_s;	vpers	= &vpers_s;
		orth	= &orth_s;	vorth	= &vorth_s;
		vinv	= &vinv_s;
		bill	= &bill_s;
	}

	return a;
}

const CAMERAX* CAMERAS::Camera( void ) const { return active; }

//----------------------------------------
// システムカメラのアングル
//----------------------------------------
void  CAMERAS::CameraAngle( float v ){ fov = v; 	}
float CAMERAS::CameraAngle( void ) const { return fov; 	}

//----------------------------------------
// システムカメラの位置
//----------------------------------------
void CAMERAS::CameraPos( float x, float y, float z ){ pos = VECTOR3{x,y,z};	}
VECTOR3 CAMERAS::CameraPos( void ) const { return pos;	}

void  CAMERAS::CameraPosX( float v ){ pos.x = v;	}
void  CAMERAS::CameraPosY( float v ){ pos.y = v;	}
void  CAMERAS::CameraPosZ( float v ){ pos.z = v;	}
float CAMERAS::CameraPosX( void ) const { return pos.x;	}
float CAMERAS::CameraPosY( void ) const { return pos.y;	}
float CAMERAS::CameraPosZ( void ) const { return pos.z;	}


//----------------------------------------
// システムカメラのクリップ
//----------------------------------------
void  CAMERAS::CameraClip(  float n, float f ){ clip = CLIPF( n, f ); }
void  CAMERAS::CameraClipN( float n ){ clip.N( n ); }
void  CAMERAS::CameraClipF( float f ){ clip.F( f ); }

CLIPF CAMERAS::CameraClip(  void ) const { return clip;	}
float CAMERAS::CameraClipN( void ) const { return clip.N();	}
float CAMERAS::CameraClipF( void ) const { return clip.F();	}

//----------------------------------------
// 射影行列
//----------------------------------------
const MATRIX* CAMERAS::View( void ) const { return view; }
const MATRIX* CAMERAS::Pers( void ) const { return pers; }
const MATRIX* CAMERAS::Orth( void ) const { return orth; }
const MATRIX* CAMERAS::Vinv( void ) const { return vinv; }
const MATRIX* CAMERAS::Bill( void ) const { return bill; }
const MATRIX* CAMERAS::Proj(     bool f ) const { return ( f ) ? pers  : orth;  }
const MATRIX* CAMERAS::ViewProj( bool f ) const { return ( f ) ? vpers : vorth; }

//----------------------------------------
// Ｚ値
//----------------------------------------
float CAMERAS::ZValue( const VECTOR3* w, const MATRIX* v ) const {

	VECTOR4	pv{0,0,0,1};

	//Vec3Transform( &pv, w, v );

	return ( abs( pv.w ) > FLT_EPSILON ) ? pv.z/pv.w : 0.0f;
}

float CAMERAS::ZValue( const VECTOR3* w ) const {

	if ( active ) {
		return active->ZValue( w );
	}
	return ZValue( w, view );
}

//----------------------------------------
// システムカメラ計算
//----------------------------------------
void CAMERAS::DrawCamera( const VECTOR3& pv,
						  const VECTOR3& fd,
						  const VECTOR3& ud,
						  float sw, float sh, float fov, const CLIPF& clip,
						  MATRIX* view,
						  MATRIX* pers, MATRIX* vpers,
						  MATRIX* orth, MATRIX* vorth,
						  MATRIX* vinv,
						  MATRIX* bill ){

	if ( view ) { CameraView( *view, pv, fd, ud ); if ( vinv ) { CameraVinv( *vinv, *view ); }	}
	if ( pers ) { CameraPers( *pers, sw, sh, fov, clip.N(), clip.F() ); if ( vpers ) { MatrixMultiply( vpers, view, pers ); }	}
	if ( orth ) { CameraOrth( *orth, sw, sh,      clip.N(), clip.F() ); if ( vorth ) { MatrixMultiply( vorth, view, orth ); }	}
	if ( bill ) { CameraBill( *bill, fd ); }
}

MATRIX* CAMERAS::CameraView( MATRIX& view, const VECTOR3& pv, const VECTOR3& fd, const VECTOR3& ud ){

	const auto	la = pv + fd;

	return MatrixLookAtLH( &view, &pv, &la, &ud );
}

MATRIX* CAMERAS::CameraVinv( MATRIX& vinv, const MATRIX& view ){

	MatrixInverse( &vinv, nullptr, &view );

	vinv._41 = 0.0f;
	vinv._42 = 0.0f;
	vinv._43 = 0.0f;
	vinv._44 = 1.0f;

	return &vinv;
}

MATRIX* CAMERAS::CameraPers( MATRIX& pers, float sw, float sh, float fov, float cn, float cf ){

	return MatrixPerspectiveFovLH( &pers, fov, sw/sh, cn, cf );
}

MATRIX* CAMERAS::CameraOrth( MATRIX& orth, float sw, float sh, float cn, float cf ){

	return MatrixOrthoLH( &orth, sw, sh, cn, cf );
}

MATRIX* CAMERAS::CameraBill( MATRIX& bill, const VECTOR3& fd ){

	if (      fd.x > 0.0f ) { MatrixRotationY( &bill, -::atanf( fd.z/fd.x ) + OPAL_PI/2 ); }
	else if ( fd.x < 0.0f ) { MatrixRotationY( &bill, -::atanf( fd.z/fd.x ) - OPAL_PI/2 ); }
	else { MatrixIdentity( &bill ); }

	return &bill;
}

//----------------------------------------
// システムカメラ
//----------------------------------------
void CAMERAS::InitCamera( CAMERAS* camera ){

	camera->system	= false;
	camera->active	= nullptr;

	camera->pos		= CAMERAS_C::pos;
	camera->fov		= CAMERAS_C::fov;
	camera->clip	= CAMERAS_C::clip;

	camera->view	= &camera->view_s;
	camera->pers	= &camera->pers_s;	camera->vpers	= &camera->vpers_s;
	camera->orth	= &camera->orth_s;	camera->vorth	= &camera->vorth_s;
	camera->vinv	= &camera->vinv_s;
	camera->bill	= &camera->bill_s;
}

void CAMERAS::ExecCamera( CAMERAS* camera ){

	if ( camera->system ) {
		CAMERAS::DrawCamera( camera->pos, OPAL_UNIT_Z, OPAL_UNIT_Y,
							 ( float )WINDOWX::ScreenW(),
							 ( float )WINDOWX::ScreenH(),
							 camera->fov, camera->clip,
							 &camera->view_s,
							 &camera->pers_s,	&camera->vpers_s,
							 &camera->orth_s,	&camera->vorth_s,
							 &camera->vinv_s,
							 &camera->bill_s );
	}
}

void CAMERAS::FreeCamera( CAMERAS* camera ){}

// End Of File
