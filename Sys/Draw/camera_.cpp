//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "Draw/cameras.hpp"

//========================================
// システムカメラ
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
CAMERAS*	DRAWX::camera;

//----------------------------------------
// システムカメラ
//----------------------------------------
void DRAWX::InitCamera( void ){ NEWS( camera );DRAWX_::InitCamera( camera );	}
void DRAWX::FreeCamera( void ){ DRAWX_::FreeCamera( camera );DELETES( camera );	}
void DRAWX::ExecCamera( void ){ DRAWX_::ExecCamera( camera );	}

//----------------------------------------
// システムカメラのON/OFF
//----------------------------------------
void DRAWX::SysCamera( bool v ){ camera->SysCamera( v );	}
bool DRAWX::SysCamera( void ){ return camera->SysCamera(); 	}

//----------------------------------------
// システムカメラの取得/設定
//----------------------------------------
const CAMERAX* DRAWX::Camera( const CAMERAX* p ){ return camera->Camera( p );	}
const CAMERAX* DRAWX::Camera( void ){ return camera->Camera(); }

//----------------------------------------
// システムカメラのアングル
//----------------------------------------
void  DRAWX::CameraAngle( float v ){ camera->CameraAngle( v ); 	}
float DRAWX::CameraAngle( void ){ return camera->CameraAngle(); }

//----------------------------------------
// システムカメラの位置
//----------------------------------------
void    DRAWX::CameraPos( float x, float y, float z ){ camera->CameraPos( x, y, z );	}
VECTOR3 DRAWX::CameraPos( void ){ return camera->CameraPos();	}

void  DRAWX::CameraPosX( float v ){ camera->CameraPosX( v );	}
void  DRAWX::CameraPosY( float v ){ camera->CameraPosY( v );	}
void  DRAWX::CameraPosZ( float v ){ camera->CameraPosZ( v );	}
float DRAWX::CameraPosX( void ){ return camera->CameraPosX(); 	}
float DRAWX::CameraPosY( void ){ return camera->CameraPosY(); 	}
float DRAWX::CameraPosZ( void ){ return camera->CameraPosZ(); 	}

//----------------------------------------
// システムカメラのクリップ
//----------------------------------------
void  DRAWX::CameraClip(  float n, float f ){ camera->CameraClip( n, f ); }
void  DRAWX::CameraClipN( float n ){ camera->CameraClipN( n ); }
void  DRAWX::CameraClipF( float f ){ camera->CameraClipF( f ); }

CLIPF DRAWX::CameraClip(  void ){ return camera->CameraClip();	}
float DRAWX::CameraClipN( void ){ return camera->CameraClipN();	}
float DRAWX::CameraClipF( void ){ return camera->CameraClipF();	}

//----------------------------------------
// 変換行列
//----------------------------------------
const MATRIX* DRAWX::View(  void ){ return camera->View(); }
const MATRIX* DRAWX::Pers(  void ){ return camera->Pers(); }
const MATRIX* DRAWX::Orth(  void ){ return camera->Orth(); }
const MATRIX* DRAWX::Vinv(  void ){ return camera->Vinv(); }
const MATRIX* DRAWX::Bill(  void ){ return camera->Bill(); }
const MATRIX* DRAWX::Proj(     bool f ){ return camera->Proj( f );		}
const MATRIX* DRAWX::ViewProj( bool f ){ return camera->ViewProj( f );	}

//----------------------------------------
// Ｚ値
//----------------------------------------
float DRAWX::ZValue( const VECTOR3* w, const MATRIX* v ){ return camera->ZValue( w, v );	}
float DRAWX::ZValue( const VECTOR3* w ){ return camera->ZValue( w );	}

//----------------------------------------
// システムカメラ計算
//----------------------------------------
void DRAWX::DrawCamera( void ){ ExecCamera(); }

void DRAWX::DrawCamera( const VECTOR3& pv,
						const VECTOR3& fd,
						const VECTOR3& ud,
						float sw, float sh, float fov, const CLIPF& clip,
						MATRIX* view,
						MATRIX* pers, MATRIX* vpers,
						MATRIX* orth, MATRIX* vorth,
						MATRIX* vinv,
						MATRIX* bill ){

	camera->DrawCamera( pv, fd, ud, sw, sh, fov, clip, view, pers, vpers, orth, vorth, vinv, bill );
}

MATRIX* DRAWX::CameraView( MATRIX& view, const VECTOR3& pv, const VECTOR3& fd, const VECTOR3& ud ){

	return camera->CameraView( view, pv, fd, ud );
}

MATRIX* DRAWX::CameraVinv( MATRIX& vinv, const MATRIX& view ){

	return camera->CameraVinv( vinv, view );
}

MATRIX* DRAWX::CameraPers( MATRIX& pers, float sw, float sh, float fov, float cn, float cf ){

	return camera->CameraPers( pers, sw, sh, fov, cn, cf );
}

MATRIX* DRAWX::CameraOrth( MATRIX& orth, float sw, float sh, float cn, float cf ){

	return camera->CameraOrth( orth, sw, sh, cn, cf );
}

MATRIX* DRAWX::CameraBill( MATRIX& bill, const VECTOR3& fd ){

	return camera->CameraBill( bill, fd );
}

// End Of File
