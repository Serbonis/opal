//========================================
// OPAL Game Library
//========================================
#include "Draw/camerax.hpp"
#include "Draw/cameras.hpp"

//========================================
// �`��V�X�e���J����
//========================================
using namespace opal;

//----------------------------------------
// ����������n��
//----------------------------------------
void CAMERAX::Init( void ){

	fov			= CAMERAS_C::fov;
	clip		= CAMERAS_C::clip;
	screen		= CSIZEF( ( float )WINDOWX::ScreenW(),( float )WINDOWX::ScreenH() );
	port.X		= 0;
	port.Y		= 0;
	port.Width	= WINDOWX::DisplayW();
	port.Height	= WINDOWX::DisplayH();
	port.MinZ	= 0.0f;
	port.MaxZ	= 1.0f;
}

void CAMERAX::Free( void ){}

//----------------------------------------
// �r���[���ˉe�s��
//----------------------------------------
const MATRIX* CAMERAX::View( void ) const { return &view; }
const MATRIX* CAMERAX::Proj( bool f ) const { return ( f ) ? &pers : &orth; }
const MATRIX* CAMERAX::ViewProj( bool f ) const { return ( f ) ? &vpers : &vorth; }

//----------------------------------------
// �y�l
//----------------------------------------
float CAMERAX::ZValue( const VECTOR3* pWorld ) const {

	return DRAWX::ZValue( pWorld, &view );
}

//----------------------------------------
// �\���̈�
//----------------------------------------
void CAMERAX::SetViewPort( DWORD x, DWORD y, DWORD w, DWORD h ){

	port.X		= x;
	port.Y		= y;
	port.Width	= w;
	port.Height	= h;
}

const D3DVIEWPORT9* CAMERAX::GetViewPort( void ) const { return &port; }

//----------------------------------------
// ����p
//----------------------------------------
void  CAMERAX::SetFov( void ){ fov = CAMERAS_C::fov; }
void  CAMERAX::SetFov( float v ){ fov  = v; }
void  CAMERAX::AddFov( float v ){ fov += v; }
float CAMERAX::GetFov( void ) const { return fov;	}

//----------------------------------------
// �N���b�v�̈�
//----------------------------------------
void  CAMERAX::SetClip( void ){ clip = CAMERAS_C::clip;	}
void  CAMERAX::SetClip( CLIPF v ){ clip = v;	}
void  CAMERAX::SetClip( float n, float f ){ clip = CLIPF( n, f ); }
CLIPF CAMERAX::GetClip( void ) const { return clip;	}

void  CAMERAX::SetNear( void ){ clip.N( CAMERAS_C::clip.N() );	}
void  CAMERAX::SetFar(  void ){ clip.N( CAMERAS_C::clip.F() );	}
void  CAMERAX::SetNear( float v ){ clip.N( v );	}
void  CAMERAX::SetFar(  float v ){ clip.F( v );	}
float CAMERAX::GetNear( void ) const { return clip.N();	}
float CAMERAX::GetFar(  void ) const { return clip.F(); }

//----------------------------------------
// ��ԍX�V
//----------------------------------------
void CAMERAX::Update( const VECTOR3& pv,
					  const VECTOR3& fd,
					  const VECTOR3& ud ){

	DRAWX::DrawCamera( pv, fd, ud,
					   screen.W(),screen.H(), fov, clip,
					   &view,
					   &pers, &vpers,
					   &orth, &vorth,
					   &vinv,
					   &bill );
}

// End Of File
