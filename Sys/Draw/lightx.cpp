//========================================
// OPAL Game Library
//========================================
#include "Draw/lightx.hpp"
#include "Draw/lights.hpp"

//========================================
// 描画システムライト
//========================================
using namespace opal;

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LIGHTX::Init( void ){

	light	= LIGHTS_C::property;
	pos		= LIGHTS_C::property.Position;
	dir		= LIGHTS_C::property.Direction;
}

void LIGHTX::Free( void ){}

//----------------------------------------
// 位置と向きを取得
//----------------------------------------
const VECTOR3* LIGHTX::Position(  void ) const { return &pos;	}
const VECTOR3* LIGHTX::Direction( void ) const { return &dir;	}

//----------------------------------------
// ライト
//----------------------------------------
LIGHTX::operator DLIGHT*( void ){ return &light; }

// End Of File
