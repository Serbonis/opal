//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// コードシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( WORKX::index  ){};
DECLVAL( WORKX::linx   ){};
DECLVAL( WORKX::parent ){};

#ifdef OPAL_DEBUG
DECLVAL( WORKX::debug  ){};
#endif

//----------------------------------------
// システム初期化
//----------------------------------------
bool WORKX::SysInit( void ){

	if ( ( index = NEWS( (*SUPERX::Linx())["WORKX"] ) ) ) {
		index->Init( "CLASS-WORK" );
	}

	linx	= nullptr;
	parent	= nullptr;

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool WORKX::SysTaskInit(  void ){ return SUPERX::TaskInit(  index );	}
bool WORKX::SysTaskEnter( void ){ return SUPERX::TaskEnter( index );	}
bool WORKX::SysTaskMain(  void ){ return SUPERX::TaskMain(  index );	}
bool WORKX::SysTaskExit(  void ){ return SUPERX::TaskExit(  index );	}
bool WORKX::SysTaskFree(  void ){ return SUPERX::TaskFree(  index );	}

//----------------------------------------
// システム後始末
//----------------------------------------
bool WORKX::SysFree( void ){

	linx	= nullptr;

	if ( index ) {
		index->Free();
		DELETES( index );
		SUPERX::Linx()->erase( "WORKX" );
	}

	return true;
}

// End Of File
