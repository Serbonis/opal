//========================================
// OPAL Game Library
//========================================
#include "Obj/vobj.hpp"

//========================================
// ベクターオブジェクト
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
VOBJ::VOBJ(){}
VOBJ::~VOBJ(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void VOBJ::Init( const char* p ){

	SOBJ::Init( p );

	ud = OPAL_UNIT_Y;
	fd = OPAL_UNIT_Z;
	rd = OPAL_UNIT_X;
}

void VOBJ::Free( void ){ SOBJ::Free(); }

//----------------------------------------
// オブジェ状態更新
//----------------------------------------
void VOBJ::TaskInit(  void ){ SOBJ::TaskInit();		}
void VOBJ::TaskEnter( void ){ SOBJ::TaskEnter();	}
void VOBJ::TaskFunc(  void ){ SOBJ::TaskFunc();		}
void VOBJ::TaskBegin( void ){ SOBJ::TaskBegin();	}
void VOBJ::TaskMain(  void ){ SOBJ::TaskMain(); 	}
void VOBJ::TaskEnd(   void ){ SOBJ::TaskEnd();		}
void VOBJ::TaskExit(  void ){ SOBJ::TaskExit();		}
void VOBJ::TaskFree(  void ){ SOBJ::TaskFree();		}

//----------------------------------------
// オブジェ情報
//----------------------------------------
void VOBJ::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// オブジェ状態計算
//----------------------------------------
void VOBJ::Coordinate( const SUPERT* o ){

	if ( df & OC::FLAG_R ) {
		MATRIX		mx;

		MatrixRotationYawPitchRoll( &mx, rv.y, rv.x, rv.z );

		Vec3TransformCoord( &ud, &OPAL_UNIT_Y, &mx );
		Vec3TransformCoord( &fd, &OPAL_UNIT_Z, &mx );
		Vec3TransformCoord( &rd, &OPAL_UNIT_X, &mx );
	}

	{
		const auto&	wv = world.trans;
		const auto	la = wv + fd;

		MatrixLookAtLH( &vw, &wv, &la, &ud );
	}
}

//----------------------------------------
// メンバー取得
//----------------------------------------
const VECTOR3& VOBJ::Ud( void   ) const { return ud; }
const VECTOR3& VOBJ::Fd( void   ) const { return fd; }
const VECTOR3& VOBJ::Rd( void   ) const { return rd; }
const VECTOR3& VOBJ::Ad( UINT a ) const {

	switch ( a ) {
	case AXIS_X: return rd;
	case AXIS_Y: return ud;
	case AXIS_Z: return fd;
	}
	return OPAL_ZERO;
}

//----------------------------------------
// オブジェ視線行列
//----------------------------------------
const MATRIX* VOBJ::Look( void ) const { return &vw; }

// End Of File
