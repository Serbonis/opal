//========================================
// OPAL Game Library
//========================================
#include "Obj/zobj.hpp"

//========================================
// 最終ゲームオブジェクト
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
ZOBJ::ZOBJ(){}
ZOBJ::~ZOBJ(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void ZOBJ::Init( const char* p ){

	SUPERX::Register( nullptr, this );
	XOBJ::Init( p );

	size = 0;
	track.clear();
}

void ZOBJ::Free( void ){

	track.clear();
	size = 0;

	SUPERX::Release( this );
	XOBJ::Free();
}

//----------------------------------------
// オブジェ状態更新
//----------------------------------------
void ZOBJ::TaskInit(  void ){ XOBJ::TaskInit();		}
void ZOBJ::TaskEnter( void ){ XOBJ::TaskEnter();	}
void ZOBJ::TaskFunc(  void ){ XOBJ::TaskFunc();		}
void ZOBJ::TaskBegin( void ){ XOBJ::TaskBegin();	}
void ZOBJ::TaskMain(  void ){ XOBJ::TaskMain(); 	}
void ZOBJ::TaskEnd(   void ){ XOBJ::TaskEnd();		}
void ZOBJ::TaskExit(  void ){ XOBJ::TaskExit();		}
void ZOBJ::TaskFree(  void ){ XOBJ::TaskFree();		}

//----------------------------------------
// オブジェ情報
//----------------------------------------
void ZOBJ::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// オブジェ状態計算
//----------------------------------------
void ZOBJ::Coordinate( const SUPERT* o ){

	if ( size ) {
		const auto	s = track.size();

		if ( size >= s ) {
			track.push_front( world.trans );

			if ( size == s ) {
				track.pop_back();
			}
		}
	}
}

//----------------------------------------
// 連結
//----------------------------------------
const ZOBJ* ZOBJ::Parent(  const ZOBJ* zobj, FUNC func, bool flag ){

	if ( !flag || !zobj || SUPERX::Register( zobj ) ) {
		if ( OOBJ::Parent( zobj, flag ) ) {
			SUPERX::Register( this, zobj, func );
			return zobj;
		}
	}
	return nullptr;
}

const ZOBJ* ZOBJ::Adopter( const ZOBJ* zobj, FUNC func, bool flag ){

	if ( !flag || !zobj || SUPERX::Register( zobj ) ) {
		if ( OOBJ::Adopter( zobj, flag ) ) {
			SUPERX::Register( this, zobj, func );
			return zobj;
		}
	}
	return nullptr;
}

const ZOBJ* ZOBJ::Parent(                  const ZOBJ* zobj,            bool flag ){ return ZOBJ::Parent(  zobj,       nullptr, flag ); }
const ZOBJ* ZOBJ::Adopter(                 const ZOBJ* zobj,            bool flag ){ return ZOBJ::Adopter( zobj,       nullptr, flag ); }
const ZOBJ* ZOBJ::Parent(  std::shared_ptr<const ZOBJ> zobj,            bool flag ){ return ZOBJ::Parent(  zobj.get(), nullptr, flag ); }
const ZOBJ* ZOBJ::Adopter( std::shared_ptr<const ZOBJ> zobj,            bool flag ){ return ZOBJ::Adopter( zobj.get(), nullptr, flag ); }
const ZOBJ* ZOBJ::Parent(  std::shared_ptr<const ZOBJ> zobj, FUNC func, bool flag ){ return ZOBJ::Parent(  zobj.get(), func,    flag ); }
const ZOBJ* ZOBJ::Adopter( std::shared_ptr<const ZOBJ> zobj, FUNC func, bool flag ){ return ZOBJ::Adopter( zobj.get(), func,    flag ); }

const ZOBJ* ZOBJ::Parent(  void ) const { return dynamic_cast<const ZOBJ*>( parent ); }
const ZOBJ* ZOBJ::Adopter( void ) const { return dynamic_cast<const ZOBJ*>( parent ); }

const ZOBJ* ZOBJ::Child(                   ZOBJ* zobj, FUNC func, bool flag ){ return zobj ? zobj->Parent(  this, func,    flag ) : nullptr; }
const ZOBJ* ZOBJ::Adopted(                 ZOBJ* zobj, FUNC func, bool flag ){ return zobj ? zobj->Parent(  this, func,    flag ) : nullptr; }
const ZOBJ* ZOBJ::Child(                   ZOBJ* zobj,            bool flag ){ return zobj ? zobj->Parent(  this, nullptr, flag ) : nullptr; }
const ZOBJ* ZOBJ::Adopted(                 ZOBJ* zobj,            bool flag ){ return zobj ? zobj->Adopter( this, nullptr, flag ) : nullptr; }
const ZOBJ* ZOBJ::Child(   std::shared_ptr<ZOBJ> zobj,            bool flag ){ return zobj ? zobj->Parent(  this, nullptr, flag ) : nullptr; }
const ZOBJ* ZOBJ::Adopted( std::shared_ptr<ZOBJ> zobj,            bool flag ){ return zobj ? zobj->Adopter( this, nullptr, flag ) : nullptr; }
const ZOBJ* ZOBJ::Child(   std::shared_ptr<ZOBJ> zobj, FUNC func, bool flag ){ return zobj ? zobj->Parent(  this, func,    flag ) : nullptr; }
const ZOBJ* ZOBJ::Adopted( std::shared_ptr<ZOBJ> zobj, FUNC func, bool flag ){ return zobj ? zobj->Adopter( this, func,    flag ) : nullptr; }

//----------------------------------------
// 位置保存
//----------------------------------------
void ZOBJ::SetTrack( UINT n ){ track.resize( size = n ); }

UINT ZOBJ::GetTrack( void ) const { return size; }

VECTOR3 ZOBJ::GetTrack( UINT n ) const {

	if ( n == 0 ) {
		return VECTOR3{world.trans};
	}

	if ( n <= size ) {
		return track[n-1];
	}

	return VECTOR3{};
}

// End Of File
