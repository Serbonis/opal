//========================================
// OPAL Game Library
//========================================
#include "Obj/oobj.hpp"

//========================================
// オリジナルオブジェクト
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
OOBJ::OOBJ(){}
OOBJ::~OOBJ(){}

//----------------------------------------
// constructor & destructor
//----------------------------------------
 OOBJ::POSE::POSE() :
	 matrix( OPAL_IDM ),
	 scale(  OPAL_IDV ),
	 rotate( OPAL_ZERO ),
	 trans(  OPAL_ORIGIN ),
	 axisx(  OPAL_UNIT_X ),
	 axisy(  OPAL_UNIT_Y ),
	 axisz(  OPAL_UNIT_Z )
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OOBJ::Init( const char* p ){

	SUPERT::Init( p );

	attach	= nullptr;
	parent	= nullptr;
	adopter	= nullptr;

	flag &= OC::FLAG_DEFAULT1;
	flag |= OC::FLAG_DEFAULT2;

	new ( &world ) POSE;
	new ( &local ) POSE;
}

void OOBJ::Free( void ){ SUPERT::Free(); }

//----------------------------------------
// オブジェ状態更新
//----------------------------------------
void OOBJ::TaskInit(  void ){ SUPERT::TaskInit();	}
void OOBJ::TaskEnter( void ){ SUPERT::TaskEnter();	}
void OOBJ::TaskFunc(  void ){ SUPERT::TaskFunc();	}
void OOBJ::TaskBegin( void ){ SUPERT::TaskBegin();	}
void OOBJ::TaskMain(  void ){ SUPERT::TaskMain();	}
void OOBJ::TaskEnd(   void ){ SUPERT::TaskEnd(); 	}
void OOBJ::TaskExit(  void ){ SUPERT::TaskExit(); 	}
void OOBJ::TaskFree(  void ){ SUPERT::TaskFree();	}

//----------------------------------------
// オブジェ情報
//----------------------------------------
void OOBJ::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// オブジェ状態計算
//----------------------------------------
static const MATRIX* srt( MATRIX* mx, UINT flag, const MATRIX* pm );

void OOBJ::Coordinate( const SUPERT* o ){

	if ( flag & OC::FLAG_W ) {
		ReCoordinate();
		flag &= ~OC::FLAG_W;
	} else {
		world.matrix = local.matrix;
	}

	if ( attach ) {
		MatrixMultiply( &world.matrix, attach );
	}

	if ( parent ) {
		if ( const auto fs = ( flag & OC::FLAG_SRT ) ) {
			if ( fs == OC::FLAG_SRT ) {
				MatrixMultiply( &world.matrix, &parent->world.matrix );
			} else {
				MATRIX	pm;

				MatrixMultiply( &world.matrix, srt( &pm, flag, &parent->world.matrix ) );
			}
		}
	}
	local();
	world();
}

void OOBJ::ReCoordinate( const SUPERT* o ){

	if ( flag & OC::FLAG_RW ) {
		if ( ( parent = adopter ) ) {
			MATRIX	im;

			MatrixMultiply( &local.matrix, MatrixInverse( &im, nullptr, &parent->world.matrix ) );
		} else {
			local.matrix = world.matrix;
		}
		flag &= ~OC::FLAG_RW;
	}
}

void OOBJ::ReCoordinate( void ){

	MATRIX	im{OPAL_IDM};

	for ( const auto* p = parent; p; p = p->parent ) {
		MatrixMultiply( &im, &p->world.matrix );
	}
	local.matrix = *MatrixMultiply( &world.matrix, MatrixInverse( &im, nullptr, &im ) );
}

UINT OOBJ::Coordinate( const LINX* index, UINT debug ){

	using POBJ = OOBJ;
	using COBJ = OOBJ;
	using CNUM = std::pair<std::vector<COBJ*>,UINT>;
	using CMAP = std::unordered_map<const POBJ*,CNUM>;
	using PMAP = std::vector<const POBJ*>;

	if ( index->Elements() ) {
		struct OOBJ_ {
			static void coordinate1( const LINX* l, CMAP* cm ){

				auto	p = l->Head();

				for ( auto i = l->Elements(); i; --i ) {
					const auto cp = p->template Task<COBJ>();

					++(*cm)[cp->parent].second;

					if ( const auto b = cp->Branch() ) {
						if ( b->Elements() ) {
							coordinate1( b, cm );
						}
					}
					p = p->Next();
				}
			}

			static void coordinate2(const LINX* l, CMAP* cm, PMAP* pm ){

				auto	p = l->Head();

				for ( auto i = l->Elements(); i; --i ) {
					const auto	cp = p->template Task<COBJ>();

					{
						const auto	pp = cp->parent;
						auto&		[cv,cn] = cm->at( pp );

						pm->emplace_back( pp );
						cv[cn++] = cp;
					}

					if ( const auto b = cp->Branch() ) {
						if ( b->Elements() ) {
							coordinate2( b, cm, pm );
						}
					}
					p = p->Next();
				}
			}

			static void coordinate3( CMAP* cm, const POBJ* pp ){

				if ( cm->count( pp ) ) {
					const auto&	[om,ov] = cm->at( pp );

					for ( auto op : om ) {
						op->Coordinate(   ( const SUPERT* )op );
						op->ReCoordinate( ( const SUPERT* )op );

						coordinate3( cm, op );
					}
					cm->erase( pp );
				}
			}
		};

		CMAP	cm(2048);
		PMAP	pm(2048);

		OOBJ_::coordinate1( index, &cm );

		for ( auto& [p,c] : cm ) {
			auto& [cv,cn] = c;

			cv.resize( cn );
			cn = 0;
		}

		OOBJ_::coordinate2( index, &cm, &pm );

		for ( const auto& k : pm ) {
			OOBJ_::coordinate3( &cm, k );
		}
	}

	return debug & 1;
}

void OOBJ::POSE::operator()( void ){

	axisx.x = matrix._11;
	axisx.y = matrix._12;
	axisx.z = matrix._13;

	axisy.x = matrix._21;
	axisy.y = matrix._22;
	axisy.z = matrix._23;

	axisz.x = matrix._31;
	axisz.y = matrix._32;
	axisz.z = matrix._33;

	scale.x = 1.0f;
	scale.y = 1.0f;
	scale.z = 1.0f;

	rotate.x = 0.0f;
	rotate.y = 0.0f;
	rotate.z = 0.0f;

	trans.x = matrix._41;
	trans.y = matrix._42;
	trans.z = matrix._43;
}

static const MATRIX* srt( MATRIX* mx, UINT flag, const MATRIX* pm ){

	VECTOR3		sv;
	VECTOR3		tv;
	VECTOR3		rv;
	QUATERNION	rq;
	MATRIX		mr;
	MATRIX		ms;
	MATRIX		mt;

	MatrixDecompose( &sv, &rq, &tv, pm );
	MatrixRotationQuaternion( &mr, &rq );

	rv = MatrixAngle( &mr );

	if ( flag & OC::FLAG_S ) {
		if ( !( flag & OC::FLAG_SX ) ) { sv.x = 1.0f; }
		if ( !( flag & OC::FLAG_SY ) ) { sv.y = 1.0f; }
		if ( !( flag & OC::FLAG_SZ ) ) { sv.z = 1.0f; }
	} else {
		sv = VECTOR3( 1.0f, 1.0f, 1.0f );
	}

	if ( flag & OC::FLAG_R ) {
		if ( !( flag & OC::FLAG_RX ) ) { rv.x = 0.0f;	}
		if ( !( flag & OC::FLAG_RY ) ) { rv.y = 0.0f;	}
		if ( !( flag & OC::FLAG_RZ ) ) { rv.z = 0.0f;	}
	} else {
		rv = VECTOR3( 0.0f, 0.0f, 0.0f );
	}

	if ( flag & OC::FLAG_T ) {
		if ( !( flag & OC::FLAG_TX ) ) { tv.x = 0.0f; }
		if ( !( flag & OC::FLAG_TY ) ) { tv.y = 0.0f; }
		if ( !( flag & OC::FLAG_TZ ) ) { tv.z = 0.0f; }
	} else {
		tv = VECTOR3( 0.0f, 0.0f, 0.0f );
	}

	MatrixScale(  &ms, sv.x, sv.y, sv.z );
	MatrixRotate( &mr, rv.x, rv.y, rv.z );
	MatrixTrans(  &mt, tv.x, tv.y, tv.z );

	return &( *mx = ms * mr * mt );
}

//----------------------------------------
// フラグ
//----------------------------------------
void OOBJ::SetFlag( void ){

	flag &= OC::FLAG_DEFAULT1;
	flag |= OC::FLAG_DEFAULT2;
}

void OOBJ::SetFlag( UINT f ){ flag =   f; 	}
void OOBJ::AddFlag( UINT f ){ flag |=  f;	}
void OOBJ::SubFlag( UINT f ){ flag &= ~f;	}

UINT OOBJ::GetFlag( void   ) const { return flag;		}
UINT OOBJ::GetFlag( UINT f ) const { return flag & f;	}
bool OOBJ::AndFlag( UINT f ) const { return ( ( flag & f ) == f ) ? true : false;	}
bool OOBJ::OrFlag(  UINT f ) const { return ( ( flag & f )      ) ? true : false;	}
bool OOBJ::EquFlag( UINT f ) const { return ( ( flag     ) == f ) ? true : false;	}

//----------------------------------------
// 連結
//----------------------------------------
const MATRIX* OOBJ::Attach( void ) const { return attach; 	}
const MATRIX* OOBJ::Attach( const MATRIX* m ){ return attach = m; }

const OOBJ* OOBJ::Parent(  void ) const { return parent; 	}
const OOBJ* OOBJ::Adopter( void ) const { return adopter;	}

const OOBJ* OOBJ::Parent( const OOBJ* o, bool f ){

	if ( f ) {
		for ( auto p = o; p; p = p->parent ) {
			if ( this == p ) {
				return nullptr;
			}
		}
	}
	return parent = o;
}

const OOBJ* OOBJ::Adopter( const OOBJ* o, bool f ){

	if ( f ) {
		for ( auto p = o; p; p = p->adopter ) {
			if ( this == p ) {
				return nullptr;
			}
		}
	}
	flag |= OC::FLAG_RC;
	return adopter = o;
}

const OOBJ* OOBJ::Parent(  std::shared_ptr<const OOBJ> o, bool f ){ return OOBJ::Parent(  o.get(), f );	}
const OOBJ* OOBJ::Adopter( std::shared_ptr<const OOBJ> o, bool f ){ return OOBJ::Adopter( o.get(), f );	}

//----------------------------------------
// 情報
//----------------------------------------
MATRIX OOBJ::World( void ) const { return world.matrix;	}
MATRIX OOBJ::Local( void ) const { return local.matrix;	}

const MATRIX*  OOBJ::MatrixWorld( void ) const { return &world.matrix;	}
const MATRIX*  OOBJ::MatrixLocal( void ) const { return &local.matrix;	}
const VECTOR3* OOBJ::VectorWorld( void ) const { return &world.trans;	}
const VECTOR3* OOBJ::VectorLocal( void ) const { return &local.trans;	}

void   OOBJ::WorldMatrix( const MATRIX& m ){ world.matrix = m;	}
void   OOBJ::LocalMatrix( const MATRIX& m ){ local.matrix = m;	}
MATRIX OOBJ::WorldMatrix( void ) const { return world.matrix;	}
MATRIX OOBJ::LocalMatrix( void ) const { return local.matrix;	}

void    OOBJ::WorldVector( const VECTOR3& v ){ world.trans = v;	}
void    OOBJ::LocalVector( const VECTOR3& v ){ local.trans = v;	}
void    OOBJ::WorldVector( const VECTOR2& v ){ world.trans = v;	}
void    OOBJ::LocalVector( const VECTOR2& v ){ local.trans = v;	}
VECTOR3 OOBJ::WorldVector( void ) const { return world.trans;	}
VECTOR3 OOBJ::LocalVector( void ) const { return local.trans;	}

OOBJ::operator       MATRIX(   void ) const { return WorldMatrix(); }
OOBJ::operator const MATRIX*(  void ) const { return MatrixWorld(); }
OOBJ::operator       VECTOR3(  void ) const { return WorldVector();	}
OOBJ::operator       VECTOR2(  void ) const { return WorldVector();	}
OOBJ::operator const VECTOR3*( void ) const { return VectorWorld();	}
OOBJ::operator const VECTOR2*( void ) const { return VectorWorld();	}

VECTOR3 OOBJ::WorldAxisX(  void ) const { return world.axisx;	}
VECTOR3 OOBJ::WorldAxisY(  void ) const { return world.axisy;	}
VECTOR3 OOBJ::WorldAxisZ(  void ) const { return world.axisz;	}
VECTOR3 OOBJ::LocalAxisX(  void ) const { return local.axisx;	}
VECTOR3 OOBJ::LocalAxisY(  void ) const { return local.axisy;	}
VECTOR3 OOBJ::LocalAxisZ(  void ) const { return local.axisz;	}

VECTOR3 OOBJ::WorldScale(  void ) const { return world.scale;	}
VECTOR3 OOBJ::LocalScale(  void ) const { return local.scale;	}
VECTOR3 OOBJ::WorldRotate( void ) const { return world.rotate;	}
VECTOR3 OOBJ::LocalRotate( void ) const { return local.rotate;	}
VECTOR3 OOBJ::WorldTrans(  void ) const { return world.trans;	}
VECTOR3 OOBJ::LocalTrans(  void ) const { return local.trans;	}

float OOBJ::WorldScaleX(  void ) const { return world.scale.x;	}
float OOBJ::WorldScaleY(  void ) const { return world.scale.y;	}
float OOBJ::WorldScaleZ(  void ) const { return world.scale.z;	}
float OOBJ::LocalScaleX(  void ) const { return local.scale.x;	}
float OOBJ::LocalScaleY(  void ) const { return local.scale.y;	}
float OOBJ::LocalScaleZ(  void ) const { return local.scale.z;	}
float OOBJ::WorldRotateX( void ) const { return world.rotate.x;	}
float OOBJ::WorldRotateY( void ) const { return world.rotate.y;	}
float OOBJ::WorldRotateZ( void ) const { return world.rotate.z;	}
float OOBJ::LocalRotateX( void ) const { return local.rotate.x;	}
float OOBJ::LocalRotateY( void ) const { return local.rotate.y;	}
float OOBJ::LocalRotateZ( void ) const { return local.rotate.z;	}
float OOBJ::WorldTransX(  void ) const { return world.trans.x;	}
float OOBJ::WorldTransY(  void ) const { return world.trans.y;	}
float OOBJ::WorldTransZ(  void ) const { return world.trans.z;	}
float OOBJ::LocalTransX(  void ) const { return local.trans.x;	}
float OOBJ::LocalTransY(  void ) const { return local.trans.y;	}
float OOBJ::LocalTransZ(  void ) const { return local.trans.z;	}

//----------------------------------------
// 先祖にいるか調査
//----------------------------------------
OOBJ* OOBJ::Search( OOBJ* r ) const {

	for ( const auto* p = parent; p; p = p->parent ) {
		if ( p == r ) {
			return r;
		}
	}
	return nullptr;
}

// End Of File
