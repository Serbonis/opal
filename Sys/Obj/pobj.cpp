//========================================
// OPAL Game Library
//========================================
#include "Obj/pobj.hpp"

//========================================
// ピボット＆オフセット
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
POBJ::POBJ(){}
POBJ::~POBJ(){}

//----------------------------------------
// 状態更新
//----------------------------------------
void POBJ::Coordinate( const SUPERT* o ){

	om._41 = +ov.x;
	om._42 = +ov.y;
	om._43 = +ov.z;

	pm._41 = -pv.x;
	pm._42 = -pv.y;
	pm._43 = -pv.z;
}

//========================================
// ピボット
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
POBJ_P_D::POBJ_P_D() :
	pv( OPAL_ORIGIN ),
	pm( OPAL_IDM )
{}

//----------------------------------------
// 直接操作
//----------------------------------------
void POBJ_P::Pivot( const VECTOR3* vp ){

	if ( vp ) {
		pv = VECTOR3( vp->x, vp->y, vp->z );
	}
}

const VECTOR3* POBJ_P::Pivot( void ) const { return &pv;	}

//----------------------------------------
// 基底操作
//----------------------------------------
void POBJ_P::SetPivot_( float  px, float  py, float  pz ){

	pv.x = px;
	pv.y = py;
	pv.z = pz;
}

void POBJ_P::AddPivot_( float  px, float  py, float  pz ){

	pv.x += px;
	pv.y += py;
	pv.z += pz;
}

void POBJ_P::GetPivot_( float* px, float* py, float* pz ) const {

	if ( px ) { *px = pv.x; }
	if ( py ) { *py = pv.y; }
	if ( pz ) { *pz = pv.z; }
}

//----------------------------------------
// インターフェース
//----------------------------------------
#define np	nullptr
#define zr	0.0f

#define PX	[this]{ float x; GetPivot_( &x, np, np ); return x; }()
#define PY	[this]{ float y; GetPivot_( np, &y, np ); return y; }()
#define PZ	[this]{ float z; GetPivot_( np, np, &z ); return z; }()

void POBJ_P_I::SetPivot( float px, float py, float pz ){ SetPivot_( px, py, pz );	}
void POBJ_P_I::SetPivot( float px, float py           ){ SetPivot_( px, py, PZ );	}
void POBJ_P_I::SetPivot( float pa                     ){ SetPivot_( pa, pa, PZ );	}
void POBJ_P_I::SetPivot( void                         ){ SetPivot_( zr, zr, zr );	}

void POBJ_P_I::AddPivot( float px, float py, float pz ){ AddPivot_( px, py, pz );	}
void POBJ_P_I::AddPivot( float px, float py           ){ AddPivot_( px, py, zr );	}
void POBJ_P_I::AddPivot( float pa                     ){ AddPivot_( pa, pa, zr );	}

void POBJ_P_I::GetPivot( float* px, float* py, float* pz ) const { GetPivot_( px, py, pz );	}
void POBJ_P_I::GetPivot( float* px, float* py            ) const { GetPivot_( px, py, np ); }
auto POBJ_P_I::GetPivot( void ) const->VECTOR3{ VECTOR3 v; GetPivot_( &v.x, &v.y, &v.z );	return v;	}

void POBJ_P_I::SetPivot( const POBJ* sp ) { if ( sp ) { SetPivot_(  sp->pv.x,  sp->pv.y,  sp->pv.z ); } }
void POBJ_P_I::GetPivot( POBJ* sp ) const { if ( sp ) { GetPivot_( &sp->pv.x, &sp->pv.y, &sp->pv.z ); } }
void POBJ_P_I::SetPivot( const POBJ& sp ) { SetPivot_(  sp.pv.x,  sp.pv.y,  sp.pv.z ); }
void POBJ_P_I::GetPivot( POBJ& sp ) const { GetPivot_( &sp.pv.x, &sp.pv.y, &sp.pv.z ); }

void POBJ_P_I::SetPivot( const VECTOR3* vp ) { if ( vp ){ SetPivot_(  vp->x,  vp->y,  vp->z ); }	}
void POBJ_P_I::SetPivot( const VECTOR2* vp ) { if ( vp ){ SetPivot_(  vp->x,  vp->y,  PZ    ); }	}
void POBJ_P_I::SetPivot( const DVECTOR* vp ) { if ( vp ){ SetPivot_(  vp->x,  vp->h,  vp->y ); }	}
void POBJ_P_I::AddPivot( const VECTOR3* vp ) { if ( vp ){ AddPivot_(  vp->x,  vp->y,  vp->z ); }	}
void POBJ_P_I::AddPivot( const VECTOR2* vp ) { if ( vp ){ AddPivot_(  vp->x,  vp->y,  zr    ); }	}
void POBJ_P_I::AddPivot( const DVECTOR* vp ) { if ( vp ){ AddPivot_(  vp->x,  vp->h,  vp->y ); }	}
void POBJ_P_I::GetPivot( VECTOR3* vp ) const { if ( vp ){ GetPivot_( &vp->x, &vp->y, &vp->z ); }	}
void POBJ_P_I::GetPivot( VECTOR2* vp ) const { if ( vp ){ GetPivot_( &vp->x, &vp->y,  np    ); }	}
void POBJ_P_I::GetPivot( DVECTOR* vp ) const { if ( vp ){ GetPivot_( &vp->x, &vp->h, &vp->y ); }	}

void POBJ_P_I::SetPivot( const VECTOR3& vp ) { SetPivot_(  vp.x,  vp.y,  vp.z ); }
void POBJ_P_I::SetPivot( const VECTOR2& vp ) { SetPivot_(  vp.x,  vp.y,  PZ   ); }
void POBJ_P_I::SetPivot( const DVECTOR& vp ) { SetPivot_(  vp.x,  vp.h,  vp.y ); }
void POBJ_P_I::AddPivot( const VECTOR3& vp ) { AddPivot_(  vp.x,  vp.y,  vp.z ); }
void POBJ_P_I::AddPivot( const VECTOR2& vp ) { AddPivot_(  vp.x,  vp.y,  zr   ); }
void POBJ_P_I::AddPivot( const DVECTOR& vp ) { AddPivot_(  vp.x,  vp.h,  vp.y ); }
void POBJ_P_I::GetPivot( VECTOR3& vp ) const { GetPivot_( &vp.x, &vp.y, &vp.z ); }
void POBJ_P_I::GetPivot( VECTOR2& vp ) const { GetPivot_( &vp.x, &vp.y,  np   ); }
void POBJ_P_I::GetPivot( DVECTOR& vp ) const { GetPivot_( &vp.x, &vp.h, &vp.y ); }

void POBJ_P_I::SetPivotX( float  px ){ SetPivot_( px, PY, PZ ); }
void POBJ_P_I::SetPivotY( float  py ){ SetPivot_( PX, py, PZ ); }
void POBJ_P_I::SetPivotZ( float  pz ){ SetPivot_( PX, PY, pz ); }
void POBJ_P_I::AddPivotX( float  px ){ AddPivot_( px, zr, zr ); }
void POBJ_P_I::AddPivotY( float  py ){ AddPivot_( zr, py, zr ); }
void POBJ_P_I::AddPivotZ( float  pz ){ AddPivot_( zr, zr, pz ); }
void POBJ_P_I::GetPivotX( float* px ) const { GetPivot_( px, np, np ); }
void POBJ_P_I::GetPivotY( float* py ) const { GetPivot_( np, py, np ); }
void POBJ_P_I::GetPivotZ( float* pz ) const { GetPivot_( np, np, pz ); }
auto POBJ_P_I::GetPivotX( void ) const->float{ float x; GetPivot_( &x, np, np ); return x; }
auto POBJ_P_I::GetPivotY( void ) const->float{ float y; GetPivot_( np, &y, np ); return y; }
auto POBJ_P_I::GetPivotZ( void ) const->float{ float z; GetPivot_( np, np, &z ); return z; }

#undef PX
#undef PY
#undef PZ

#undef np
#undef zr

//========================================
// オフセット
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
POBJ_O_D::POBJ_O_D() :
	ov( OPAL_ORIGIN ),
	om( OPAL_IDM )
{}

//----------------------------------------
// 直接操作
//----------------------------------------
void POBJ_O::Offset( const VECTOR3* vo ){

	if ( vo ) {
		ov = VECTOR3( vo->x, vo->y, vo->z );
	}
}

const VECTOR3* POBJ_O::Offset( void ) const { return &ov;	}

//----------------------------------------
// 基底操作
//----------------------------------------
void POBJ_O::SetOffset_( float  ox, float  oy, float  oz ){

	ov.x = ox;
	ov.y = oy;
	ov.z = oz;
}

void POBJ_O::AddOffset_( float  ox, float  oy, float  oz ){

	ov.x += ox;
	ov.y += oy;
	ov.z += oz;
}

void POBJ_O::GetOffset_( float* ox, float* oy, float* oz ) const {

	if ( ox ) { *ox = ov.x; }
	if ( oy ) { *oy = ov.y; }
	if ( oz ) { *oz = ov.z; }
}

//----------------------------------------
// インターフェース
//----------------------------------------
#define np	nullptr
#define zr	0.0f

#define OX	[this]{ float x; GetOffset_( &x, np, np ); return x; }()
#define OY	[this]{ float y; GetOffset_( np, &y, np ); return y; }()
#define OZ	[this]{ float z; GetOffset_( np, np, &z ); return z; }()

void POBJ_O_I::SetOffset( float ox, float oy, float oz ){ SetOffset_( ox, oy, oz );	}
void POBJ_O_I::SetOffset( float ox, float oy           ){ SetOffset_( ox, oy, OZ );	}
void POBJ_O_I::SetOffset( float oa                     ){ SetOffset_( oa, oa, OZ ); }
void POBJ_O_I::SetOffset( void                         ){ SetOffset_( zr, zr, zr );	}

void POBJ_O_I::AddOffset( float ox, float oy, float oz ){ AddOffset_( ox, oy, oz );	}
void POBJ_O_I::AddOffset( float ox, float oy           ){ AddOffset_( ox, oy, zr ); }
void POBJ_O_I::AddOffset( float oa                     ){ AddOffset_( oa, oa, zr ); }

void POBJ_O_I::GetOffset( float* ox, float* oy, float* oz ) const { GetOffset_( ox, oy, oz );	}
void POBJ_O_I::GetOffset( float* ox, float* oy            ) const { GetOffset_( ox, oy, np );	}
auto POBJ_O_I::GetOffset( void ) const->VECTOR3{ VECTOR3 v; GetOffset_( &v.x, &v.y, &v.z );	return v;	}

void POBJ_O_I::SetOffset( const POBJ* sp ) { if ( sp ) { SetOffset_(  sp->ov.x,  sp->ov.y,  sp->ov.z ); } }
void POBJ_O_I::GetOffset( POBJ* sp ) const { if ( sp ) { GetOffset_( &sp->ov.x, &sp->ov.y, &sp->ov.z ); } }
void POBJ_O_I::SetOffset( const POBJ& sp ) { SetOffset_(  sp.ov.x,  sp.ov.y,  sp.ov.z ); }
void POBJ_O_I::GetOffset( POBJ& sp ) const { GetOffset_( &sp.ov.x, &sp.ov.y, &sp.ov.z ); }

void POBJ_O_I::SetOffset( const VECTOR3* vo ) { if ( vo ) { SetOffset_(  vo->x,  vo->y,  vo->z ); }	}
void POBJ_O_I::SetOffset( const VECTOR2* vo ) { if ( vo ) { SetOffset_(  vo->x,  vo->y,  OZ    ); }	}
void POBJ_O_I::SetOffset( const DVECTOR* vo ) { if ( vo ) { SetOffset_(  vo->x,  vo->h,  vo->y ); }	}
void POBJ_O_I::AddOffset( const VECTOR3* vo ) { if ( vo ) { AddOffset_(  vo->x,  vo->y,  vo->z ); }	}
void POBJ_O_I::AddOffset( const VECTOR2* vo ) { if ( vo ) { AddOffset_(  vo->x,  vo->y,  zr    ); }	}
void POBJ_O_I::AddOffset( const DVECTOR* vo ) { if ( vo ) { AddOffset_(  vo->x,  vo->h,  vo->y ); }	}
void POBJ_O_I::GetOffset( VECTOR3* vo ) const { if ( vo ) { GetOffset_( &vo->x, &vo->y, &vo->z ); }	}
void POBJ_O_I::GetOffset( VECTOR2* vo ) const { if ( vo ) { GetOffset_( &vo->x, &vo->y,  np    ); }	}
void POBJ_O_I::GetOffset( DVECTOR* vo ) const { if ( vo ) { GetOffset_( &vo->x, &vo->h, &vo->y ); }	}

void POBJ_O_I::SetOffset( const VECTOR3& vo ) { SetOffset_(  vo.x,  vo.y,  vo.z ); }
void POBJ_O_I::SetOffset( const VECTOR2& vo ) { SetOffset_(  vo.x,  vo.y,  OZ   ); }
void POBJ_O_I::SetOffset( const DVECTOR& vo ) { SetOffset_(  vo.x,  vo.h,  vo.y ); }
void POBJ_O_I::AddOffset( const VECTOR3& vo ) { AddOffset_(  vo.x,  vo.y,  vo.z ); }
void POBJ_O_I::AddOffset( const VECTOR2& vo ) { AddOffset_(  vo.x,  vo.y,  zr   ); }
void POBJ_O_I::AddOffset( const DVECTOR& vo ) { AddOffset_(  vo.x,  vo.h,  vo.y ); }
void POBJ_O_I::GetOffset( VECTOR3& vo ) const { GetOffset_( &vo.x, &vo.y, &vo.z ); }
void POBJ_O_I::GetOffset( VECTOR2& vo ) const { GetOffset_( &vo.x, &vo.y,  np   ); }
void POBJ_O_I::GetOffset( DVECTOR& vo ) const { GetOffset_( &vo.x, &vo.h, &vo.y ); }

void POBJ_O_I::SetOffsetX( float  ox ){ SetOffset_( ox, OY, OZ ); }
void POBJ_O_I::SetOffsetY( float  oy ){ SetOffset_( OX, oy, OZ ); }
void POBJ_O_I::SetOffsetZ( float  oz ){ SetOffset_( OX, OY, oz ); }
void POBJ_O_I::AddOffsetX( float  ox ){ AddOffset_( ox, zr, zr ); }
void POBJ_O_I::AddOffsetY( float  oy ){ AddOffset_( zr, oy, zr ); }
void POBJ_O_I::AddOffsetZ( float  oz ){ AddOffset_( zr, zr, oz ); }
void POBJ_O_I::GetOffsetX( float* ox ) const { GetOffset_( ox, np, np ); }
void POBJ_O_I::GetOffsetY( float* oy ) const { GetOffset_( np, oy, np ); }
void POBJ_O_I::GetOffsetZ( float* oz ) const { GetOffset_( np, np, oz ); }
auto POBJ_O_I::GetOffsetX(  void ) const->float{ float x; GetOffset_( &x, np, np ); return x; }
auto POBJ_O_I::GetOffsetY(  void ) const->float{ float y; GetOffset_( np, &y, np ); return y; }
auto POBJ_O_I::GetOffsetZ(  void ) const->float{ float z; GetOffset_( np, np, &z ); return z; }

#undef OX
#undef OY
#undef OZ

#undef np
#undef zr

// End Of File
