//========================================
// OPAL Game Library
//========================================
#include "Obj/xobj.hpp"

//========================================
// 原点＆座標系
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
XOBJ::XOBJ() {}
XOBJ::~XOBJ(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void XOBJ::Init( const char* p ){

	VOBJ::Init( p );

	origin	= nullptr;
	dim3	= false;
	axis[0]	= false;
	axis[1]	= false;
	orig[0]	= VECTOR3{};
	orig[1]	= VECTOR3{};
}

void XOBJ::Free( void ){

	VOBJ::Free();
}

//----------------------------------------
// オブジェ状態更新
//----------------------------------------
void XOBJ::TaskInit(  void ){ VOBJ::TaskInit();		}
void XOBJ::TaskEnter( void ){ VOBJ::TaskEnter();	}
void XOBJ::TaskFunc(  void ){ VOBJ::TaskFunc();		}
void XOBJ::TaskBegin( void ){ VOBJ::TaskBegin();	}
void XOBJ::TaskMain(  void ){ VOBJ::TaskMain(); 	}
void XOBJ::TaskEnd(   void ){ VOBJ::TaskEnd();		}
void XOBJ::TaskExit(  void ){ VOBJ::TaskExit();		}
void XOBJ::TaskFree(  void ){ VOBJ::TaskFree();		}

//----------------------------------------
// オブジェ情報
//----------------------------------------
void XOBJ::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// オブジェ状態計算
//----------------------------------------
void XOBJ::Coordinate( const SUPERT* o ){

	if ( origin ) {
		axis[1]	= origin->axis[1];
		orig[1]	= origin->orig[1];
	} else {
		axis[1]	= axis[0];
		orig[1]	= orig[0];
	}
}

//----------------------------------------
// 座標系
//----------------------------------------
void XOBJ::SetDim2( bool f ){ dim3 = f ? false : true;	}
void XOBJ::SetDim3( bool f ){ dim3 = f ? true : false;	}
bool XOBJ::GetDim2( void ) const { return dim3 ? false : true;	}
bool XOBJ::GetDim3( void ) const { return dim3 ? true : false;	}

void XOBJ::SetCoordinate( bool f ){ axis[0] = f;	}
bool XOBJ::GetCoordinate( void ) const { return axis[1];	}

//----------------------------------------
// 原点
//----------------------------------------
void XOBJ::SetOrigin_( float  x, float  y, float  z ){

	orig[0].x = x;
	orig[0].y = y;
	orig[0].z = z;
}

void XOBJ::GetOrigin_( float* x, float* y, float* z ) const {

	if ( x ) { *x = orig[1].x; }
	if ( y ) { *y = orig[1].y; }
	if ( z ) { *z = orig[1].z; }
}

void XOBJ::SetOrigin( float x, float y, float z ){ SetOrigin_( x, y, z );	}
void XOBJ::SetOrigin( const VECTOR3& v ){ SetOrigin_( v.x, v.y, v.z );	}
void XOBJ::SetOrigin( const VECTOR2& v ){ SetOrigin_( v.x, v.y, 0.f );	}
void XOBJ::SetOrigin( const VECTOR3* v ){ if ( v ) { SetOrigin_( v->x, v->y, v->z ); }	}
void XOBJ::SetOrigin( const VECTOR2* v ){ if ( v ) { SetOrigin_( v->x, v->y, 0.0f ); }	}
void XOBJ::SetOrigin( void ){ SetOrigin_( 0, 0, 0 );	}

void XOBJ::GetOrigin( float* x, float* y, float* z ) const { GetOrigin_( x, y, z );	}
void XOBJ::GetOrigin( VECTOR3& v ) const { GetOrigin_( &v.x, &v.y, &v.z    );	}
void XOBJ::GetOrigin( VECTOR2& v ) const { GetOrigin_( &v.x, &v.y, nullptr );	}
void XOBJ::GetOrigin( VECTOR3* v ) const { if ( v ) { GetOrigin_( &v->x, &v->y, &v->z   ); }	}
void XOBJ::GetOrigin( VECTOR2* v ) const { if ( v ) { GetOrigin_( &v->x, &v->y, nullptr ); }	}
auto XOBJ::GetOrigin( void ) const->VECTOR3{ VECTOR3 v; GetOrigin_( &v.x, &v.y, &v.z ); return v;	}

//----------------------------------------
// 連結
//----------------------------------------
const XOBJ* XOBJ::Origin( const XOBJ* o ){ return origin = o;	}
const XOBJ* XOBJ::Origin( std::shared_ptr<const XOBJ> o ){ return origin = o.get();	}
const XOBJ* XOBJ::Origin( void ) const { return origin;	};

//----------------------------------------
// デフォルト回転軸
//----------------------------------------
#define np	nullptr
#define zr	0.0f

#define RX	[this]{ float x; GetRotate_( &x, np, np ); return x; }()
#define RY	[this]{ float y; GetRotate_( np, &y, np ); return y; }()
#define RZ	[this]{ float z; GetRotate_( np, np, &z ); return z; }()

void XOBJ::SetRotate( float  rv        ){ dim3 ? SetRotate_( RX, rv, RZ ) : SetRotate_( RX, RY, rv ); }
void XOBJ::AddRotate( float  rv        ){ dim3 ? AddRotate_( zr, rv, zr ) : AddRotate_( zr, zr, rv ); }
void XOBJ::GetRotate( float* rv ) const { dim3 ? GetRotate_( np, rv, np ) : GetRotate_( np, np, rv ); }
auto XOBJ::GetRotate( void ) const->float{ return dim3 ? RY : RZ; }

#undef RX
#undef RY
#undef RZ

#undef np
#undef zr

//----------------------------------------
// ワールド変換
//----------------------------------------
auto XOBJ::World( void ) const->MATRIX{

	auto	m = VOBJ::World();

	if ( axis[1] ) {
		m._42 *= -1;
		if ( dim3 ) {
			std::swap( m._42, m._43 );
		}
	}

	m._41 += orig[1].x;
	m._42 += orig[1].y;
	m._43 += orig[1].z;

	return m;
}

// End Of File
