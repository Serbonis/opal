//========================================
// OPAL Game Library
//========================================
#include "Obj/sobj.hpp"

//========================================
// スカラーオブジェクト
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SOBJ::SOBJ(){}
SOBJ::~SOBJ(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void SOBJ::Init( const char* p ){

	OOBJ::Init( p );

	df = OC::FLAG_N;

	if ( flag & OC::FLAG_CS ) {
		SOBJ_S::df = OC::FLAG_S;
		df |= OC::FLAG_S;
	} else {
		SOBJ_S::df = OC::FLAG_N;
		sv = OPAL_IDV;
	}

	if ( flag & OC::FLAG_CR ) {
		SOBJ_R::df = OC::FLAG_R;
		df |= OC::FLAG_R;
	} else {
		SOBJ_R::df = OC::FLAG_N;
		rv = OPAL_ZERO;
	}

	if ( flag & OC::FLAG_CT ) {
		SOBJ_T::df = OC::FLAG_T;
		df |= OC::FLAG_T;
	} else {
		SOBJ_T::df = OC::FLAG_N;
		tv = OPAL_ORIGIN;
	}

	MatrixScale(  &sm, sv.x, sv.y, sv.z );
	MatrixRotate( &rm, rv.x, rv.y, rv.z );
	MatrixTrans(  &tm, tv.x, tv.y, tv.z );

	MatrixMultiply( &sr, &sm, &rm );
}

void SOBJ::Free( void ){ OOBJ::Free(); }

//----------------------------------------
// オブジェ状態更新
//----------------------------------------
void SOBJ::TaskInit(  void ){ OOBJ::TaskInit(); 	}
void SOBJ::TaskEnter( void ){ OOBJ::TaskEnter(); 	}
void SOBJ::TaskFunc(  void ){ OOBJ::TaskFunc();		}
void SOBJ::TaskBegin( void ){ OOBJ::TaskBegin();	}
void SOBJ::TaskMain(  void ){ OOBJ::TaskMain(); 	}
void SOBJ::TaskEnd(   void ){ OOBJ::TaskEnd(); 		}
void SOBJ::TaskExit(  void ){ OOBJ::TaskExit();		}
void SOBJ::TaskFree(  void ){ OOBJ::TaskFree(); 	}

//----------------------------------------
// オブジェ情報
//----------------------------------------
void SOBJ::ObjInfo( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// オブジェ状態計算
//----------------------------------------
void SOBJ::Coordinate( const SUPERT* o ){

	df |= (SOBJ_S::df|SOBJ_R::df|SOBJ_T::df);

	if ( flag & OC::FLAG_L ) {
		ReCoordinate();
		flag &= ~OC::FLAG_L;
		df |= OC::FLAG_SRT;
	} else {
		flag &= ~OC::FLAG_BOOT;
		if ( df & OC::FLAG_S ) { MatrixScale(  &sm, +sv.x, +sv.y, +sv.z );	}
		if ( df & OC::FLAG_R ) { MatrixRotate( &rm, +rv.x, +rv.y, +rv.z );	}
		if ( df & OC::FLAG_T ) { MatrixTrans(  &tm, +tv.x, +tv.y, +tv.z );	}
	}

	SOBJ_S::df = OC::FLAG_N;
	SOBJ_R::df = OC::FLAG_N;
	SOBJ_T::df = OC::FLAG_N;
}

void SOBJ::ReCoordinate( const SUPERT* o ){

	if ( flag & OC::FLAG_RL ) {
		ReCoordinate();
		flag &= ~OC::FLAG_RL;
	}
}

void SOBJ::ReCoordinate( void ){

	QUATERNION	rq;

	MatrixDecompose( &sv, &rq, &tv, &local.matrix );

	MatrixScale( &sm, sv.x, sv.y, sv.z );
	MatrixTrans( &tm, tv.x, tv.y, tv.z );

	MatrixRotationQuaternion( &rm, &rq );

	rv = MatrixAngle( &rm );
}

//----------------------------------------
// ダーティーフラグ
//----------------------------------------
void SOBJ::Dirty( UINT f ){

	if ( f ) {
		df |= f;
	} else {
		df &= ~(OC::FLAG_SRT);
	}
}

//----------------------------------------
// SRT計算
//----------------------------------------
void SOBJ::CalcurateN( void ){

	if ( df & OC::FLAG_SR  ) { MatrixMultiply( &sr,           &sm, &rm );	}
	if ( df & OC::FLAG_SRT ) { MatrixMultiply( &local.matrix, &sr, &tm );	}
}

void SOBJ::CalcurateP( const MATRIX* vinv ){

	if ( df & OC::FLAG_SR ) { MatrixMultiply( &sr, &sm, &rm );	}

	MatrixMultiply( &local.matrix, &sr, vinv );
	MatrixMultiply( &local.matrix, &tm );
}

void SOBJ::CalcurateB( const MATRIX* bill ){

	if ( df & OC::FLAG_RY  ) { MatrixRotationY( &rm, rv.y     ); }
	if ( df & OC::FLAG_SRY ) { MatrixMultiply(  &sr, &sm, &rm ); }

	MatrixMultiply( &local.matrix, &sr, bill );
	MatrixMultiply( &local.matrix, &tm );
}

//----------------------------------------
// 更新チェック
//----------------------------------------
UINT SOBJ::SRT(    void ) const { return df; }
bool SOBJ::SRT(  UINT f ) const { return ( df & f ) ? true : false; }
bool SOBJ::SRT_S(  void ) const { return SRT( FLAG_S  ); }
bool SOBJ::SRT_R(  void ) const { return SRT( FLAG_R  ); }
bool SOBJ::SRT_T(  void ) const { return SRT( FLAG_T  ); }
bool SOBJ::SRT_SX( void ) const { return SRT( FLAG_SX ); }
bool SOBJ::SRT_SY( void ) const { return SRT( FLAG_SY ); }
bool SOBJ::SRT_SZ( void ) const { return SRT( FLAG_SZ ); }
bool SOBJ::SRT_RX( void ) const { return SRT( FLAG_RX ); }
bool SOBJ::SRT_RY( void ) const { return SRT( FLAG_RY ); }
bool SOBJ::SRT_RZ( void ) const { return SRT( FLAG_RZ ); }
bool SOBJ::SRT_TX( void ) const { return SRT( FLAG_TX ); }
bool SOBJ::SRT_TY( void ) const { return SRT( FLAG_TY ); }
bool SOBJ::SRT_TZ( void ) const { return SRT( FLAG_TZ ); }

//========================================
// TRANS
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SOBJ_T_D::SOBJ_T_D() :
	tv( OPAL_ORIGIN ),
	tm( OPAL_IDM )
{}

//----------------------------------------
// 直接操作
//----------------------------------------
const VECTOR3* SOBJ_T::Trans(  void ) const { return &tv; }

void SOBJ_T::Trans( const VECTOR3* vt ){ if ( vt ){ tv = *vt; } }
void SOBJ_T::Trans( const VECTOR3& vt ){ Trans(  &vt );	}

//----------------------------------------
// 基底操作
//----------------------------------------
void SOBJ_T::SetTrans_( float  tx, float  ty, float  tz ){

	if ( !FEQUAL( tv.x, tx ) ) { tv.x = tx; df |= OC::FLAG_TX; }
	if ( !FEQUAL( tv.y, ty ) ) { tv.y = ty; df |= OC::FLAG_TY; }
	if ( !FEQUAL( tv.z, tz ) ) { tv.z = tz; df |= OC::FLAG_TZ; }
}

void SOBJ_T::AddTrans_( float  tx, float  ty, float  tz ){

	if ( !FEQUAL( tv.x, tx+tv.x ) ) { tv.x += tx; df |= OC::FLAG_TX; }
	if ( !FEQUAL( tv.y, ty+tv.y ) ) { tv.y += ty; df |= OC::FLAG_TY; }
	if ( !FEQUAL( tv.z, tz+tv.z ) ) { tv.z += tz; df |= OC::FLAG_TZ; }
}

void SOBJ_T::GetTrans_( float* tx, float* ty, float* tz ) const {

	if ( tx ) { *tx = tv.x; }
	if ( ty ) { *ty = tv.y; }
	if ( tz ) { *tz = tv.z; }
}

//----------------------------------------
// インターフェース
//----------------------------------------
#define np	nullptr
#define zr	0.0f

#define TX	[this]{ float x; GetTrans_( &x, np, np ); return x; }()
#define TY	[this]{ float y; GetTrans_( np, &y, np ); return y; }()
#define TZ	[this]{ float z; GetTrans_( np, np, &z ); return z; }()

void SOBJ_T_I::SetTrans( float tx, float ty, float  tz ){ SetTrans_( tx, ty, tz ); }
void SOBJ_T_I::SetTrans( float tx, float ty            ){ SetTrans_( tx, ty, TZ ); }
void SOBJ_T_I::SetTrans( void                          ){ SetTrans_( zr, zr, zr ); }

void SOBJ_T_I::AddTrans( float tx, float ty, float tz ){ AddTrans_( tx, ty, tz ); }
void SOBJ_T_I::AddTrans( float tx, float ty           ){ AddTrans_( tx, ty, zr ); }

void SOBJ_T_I::GetTrans( float* tx, float* ty, float* tz ) const { GetTrans_( tx, ty, tz ); }
void SOBJ_T_I::GetTrans( float* tx, float* ty            ) const { GetTrans_( tx, ty, np ); }
auto SOBJ_T_I::GetTrans( void ) const->VECTOR3{ VECTOR3 v; GetTrans_( &v.x, &v.y, &v.z ); return v; }

void SOBJ_T_I::SetTrans( const std::tuple<float,float,float>& vt ){ SetTrans_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_T_I::AddTrans( const std::tuple<float,float,float>& vt ){ AddTrans_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_T_I::SetTrans( const std::pair< float,float      >& vt ){ SetTrans_( std::get<0>(vt),std::get<1>(vt),TZ );	}
void SOBJ_T_I::AddTrans( const std::pair< float,float      >& vt ){ AddTrans_( std::get<0>(vt),std::get<1>(vt),zr );	}

void SOBJ_T_I::SetTrans( const SOBJ* sp ) { if ( sp ) { SetTrans_(  sp->tv.x,  sp->tv.y,  sp->tv.z ); } }
void SOBJ_T_I::GetTrans( SOBJ* sp ) const { if ( sp ) { GetTrans_( &sp->tv.x, &sp->tv.y, &sp->tv.z ); } }
void SOBJ_T_I::SetTrans( const SOBJ& sp ) { SetTrans_(  sp.tv.x,  sp.tv.y,  sp.tv.z ); }
void SOBJ_T_I::GetTrans( SOBJ& sp ) const { GetTrans_( &sp.tv.x, &sp.tv.y, &sp.tv.z ); }

void SOBJ_T_I::SetTrans( const VECTOR3* vt ) { if ( vt ) { SetTrans_(  vt->x,  vt->y,  vt->z ); } }
void SOBJ_T_I::SetTrans( const VECTOR2* vt ) { if ( vt ) { SetTrans_(  vt->x,  vt->y,  TZ    ); } }
void SOBJ_T_I::SetTrans( const DVECTOR* vt ) { if ( vt ) { SetTrans_(  vt->x,  vt->h,  vt->y ); } }
void SOBJ_T_I::AddTrans( const VECTOR3* vt ) { if ( vt ) { AddTrans_(  vt->x,  vt->y,  vt->z ); } }
void SOBJ_T_I::AddTrans( const VECTOR2* vt ) { if ( vt ) { AddTrans_(  vt->x,  vt->y,  zr    ); } }
void SOBJ_T_I::AddTrans( const DVECTOR* vt ) { if ( vt ) { AddTrans_(  vt->x,  vt->h,  vt->y ); } }
void SOBJ_T_I::GetTrans( VECTOR3* vt ) const { if ( vt ) { GetTrans_( &vt->x, &vt->y, &vt->z ); } }
void SOBJ_T_I::GetTrans( VECTOR2* vt ) const { if ( vt ) { GetTrans_( &vt->x, &vt->y,  np    ); } }
void SOBJ_T_I::GetTrans( DVECTOR* vt ) const { if ( vt ) { GetTrans_( &vt->x, &vt->h, &vt->y ); } }

void SOBJ_T_I::SetTrans( const VECTOR3& vt ) { SetTrans_(  vt.x,  vt.y,  vt.z ); }
void SOBJ_T_I::SetTrans( const VECTOR2& vt ) { SetTrans_(  vt.x,  vt.y,  TZ   ); }
void SOBJ_T_I::SetTrans( const DVECTOR& vt ) { SetTrans_(  vt.x,  vt.h,  vt.y ); }
void SOBJ_T_I::AddTrans( const VECTOR3& vt ) { AddTrans_(  vt.x,  vt.y,  vt.z ); }
void SOBJ_T_I::AddTrans( const VECTOR2& vt ) { AddTrans_(  vt.x,  vt.y,  zr   ); }
void SOBJ_T_I::AddTrans( const DVECTOR& vt ) { AddTrans_(  vt.x,  vt.h,  vt.y ); }
void SOBJ_T_I::GetTrans( VECTOR3& vt ) const { GetTrans_( &vt.x, &vt.y, &vt.z ); }
void SOBJ_T_I::GetTrans( VECTOR2& vt ) const { GetTrans_( &vt.x, &vt.y,  np   ); }
void SOBJ_T_I::GetTrans( DVECTOR& vt ) const { GetTrans_( &vt.x, &vt.h, &vt.y ); }

void SOBJ_T_I::SetTransX( float  tx ){ SetTrans_( tx, TY, TZ ); }
void SOBJ_T_I::SetTransY( float  ty ){ SetTrans_( TX, ty, TZ ); }
void SOBJ_T_I::SetTransZ( float  tz ){ SetTrans_( TX, TY, tz ); }
void SOBJ_T_I::AddTransX( float  tx ){ AddTrans_( tx, zr, zr ); }
void SOBJ_T_I::AddTransY( float  ty ){ AddTrans_( zr, ty, zr ); }
void SOBJ_T_I::AddTransZ( float  tz ){ AddTrans_( zr, zr, tz ); }
void SOBJ_T_I::GetTransX( float* tx ) const { GetTrans_( tx, np, np ); }
void SOBJ_T_I::GetTransY( float* ty ) const { GetTrans_( np, ty, np ); }
void SOBJ_T_I::GetTransZ( float* tz ) const { GetTrans_( np, np, tz ); }
auto SOBJ_T_I::GetTransX( void ) const->float{ return TX; }
auto SOBJ_T_I::GetTransY( void ) const->float{ return TY; }
auto SOBJ_T_I::GetTransZ( void ) const->float{ return TZ; }

#undef TX
#undef TY
#undef TZ

#undef np
#undef zr

//========================================
// ROTATE
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SOBJ_R_D::SOBJ_R_D() :
	rv( OPAL_ZERO ),
	rm( OPAL_IDM  )
{}

//----------------------------------------
// 直接操作
//----------------------------------------
const VECTOR3* SOBJ_R::Rotate( void ) const { return &rv; }

void SOBJ_R::Rotate( const VECTOR3* vr ){ if ( vr ){ rv = *vr; } }
void SOBJ_R::Rotate( const VECTOR3& vr ){ Rotate( &vr ); }

//----------------------------------------
// 基底操作
//----------------------------------------
void SOBJ_R::SetRotate_( float  rx, float  ry, float  rz ){

	if ( !FEQUAL( rv.x, rx ) ) { rv.x = rx; df |= OC::FLAG_RX; }
	if ( !FEQUAL( rv.y, ry ) ) { rv.y = ry; df |= OC::FLAG_RY; }
	if ( !FEQUAL( rv.z, rz ) ) { rv.z = rz; df |= OC::FLAG_RZ; }
}

void SOBJ_R::AddRotate_( float  rx, float  ry, float  rz ){

	if ( !FEQUAL( rv.x, rx+rv.x ) ) { rv.x += rx; df |= OC::FLAG_RX; }
	if ( !FEQUAL( rv.y, ry+rv.y ) ) { rv.y += ry; df |= OC::FLAG_RY; }
	if ( !FEQUAL( rv.z, rz+rv.z ) ) { rv.z += rz; df |= OC::FLAG_RZ; }
}

void SOBJ_R::GetRotate_( float* rx, float* ry, float* rz ) const {

	if ( rx ) { *rx = rv.x; }
	if ( ry ) { *ry = rv.y; }
	if ( rz ) { *rz = rv.z; }
}

//----------------------------------------
// インターフェース
//----------------------------------------
#define np	nullptr
#define zr	0.0f

#define RX	[this]{ float x; GetRotate_( &x, np, np ); return x; }()
#define RY	[this]{ float y; GetRotate_( np, &y, np ); return y; }()
#define RZ	[this]{ float z; GetRotate_( np, np, &z ); return z; }()

void SOBJ_R_I::SetRotate( float rx, float ry, float rz ){ SetRotate_( rx, ry, rz ); }
void SOBJ_R_I::SetRotate( float rz                     ){ SetRotate_( RX, RY, rz ); }
void SOBJ_R_I::SetRotate( void                         ){ SetRotate_( zr, zr, zr ); }

void SOBJ_R_I::AddRotate( float rx, float ry, float rz ){ AddRotate_( rx, ry, rz ); }
void SOBJ_R_I::AddRotate( float rz                     ){ AddRotate_( zr, zr, rz ); }

void SOBJ_R_I::GetRotate( float* rx, float* ry, float* rz ) const { GetRotate_( rx, ry, rz ); }
void SOBJ_R_I::GetRotate( float* rz                       ) const { GetRotate_( np, np, rz ); }
auto SOBJ_R_I::GetRotate( void ) const->float{ return RZ; }

void SOBJ_R_I::SetRotate( const std::tuple<float,float,float>& vt ){ SetRotate_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_R_I::AddRotate( const std::tuple<float,float,float>& vt ){ AddRotate_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_R_I::SetRotate( const std::pair< float,float      >& vt ){ SetRotate_( std::get<0>(vt),std::get<1>(vt),RZ );	}
void SOBJ_R_I::AddRotate( const std::pair< float,float      >& vt ){ AddRotate_( std::get<0>(vt),std::get<1>(vt),zr );	}

void SOBJ_R_I::SetRotate( const SOBJ* sp ) { if ( sp ) { SetRotate_(  sp->rv.x,  sp->rv.y,  sp->rv.z ); } }
void SOBJ_R_I::GetRotate( SOBJ* sp ) const { if ( sp ) { GetRotate_( &sp->rv.x, &sp->rv.y, &sp->rv.z ); } }
void SOBJ_R_I::SetRotate( const SOBJ& sp ) { SetRotate_(  sp.rv.x,  sp.rv.y,  sp.rv.z ); }
void SOBJ_R_I::GetRotate( SOBJ& sp ) const { GetRotate_( &sp.rv.x, &sp.rv.y, &sp.rv.z ); }

void SOBJ_R_I::SetRotate( const VECTOR3* vr ) { if ( vr ) { SetRotate_(  vr->x,  vr->y,  vr->z ); } }
void SOBJ_R_I::SetRotate( const VECTOR2* vr ) { if ( vr ) { SetRotate_(  vr->x,  vr->y,  RZ    ); } }
void SOBJ_R_I::SetRotate( const DVECTOR* vr ) { if ( vr ) { SetRotate_(  vr->x,  vr->h,  vr->y ); } }
void SOBJ_R_I::AddRotate( const VECTOR3* vr ) { if ( vr ) { AddRotate_(  vr->x,  vr->y,  vr->z ); } }
void SOBJ_R_I::AddRotate( const VECTOR2* vr ) { if ( vr ) { AddRotate_(  vr->x,  vr->y,  zr    ); } }
void SOBJ_R_I::AddRotate( const DVECTOR* vr ) { if ( vr ) { AddRotate_(  vr->x,  vr->h,  vr->y ); } }
void SOBJ_R_I::GetRotate( VECTOR3* vr ) const { if ( vr ) { GetRotate_( &vr->x, &vr->y, &vr->z ); } }
void SOBJ_R_I::GetRotate( VECTOR2* vr ) const { if ( vr ) { GetRotate_( &vr->x, &vr->y,  np    ); } }
void SOBJ_R_I::GetRotate( DVECTOR* vr ) const { if ( vr ) { GetRotate_( &vr->x, &vr->h, &vr->y ); } }

void SOBJ_R_I::SetRotate( const VECTOR3& vr ) { SetRotate_(  vr.x,  vr.y,  vr.z ); }
void SOBJ_R_I::SetRotate( const VECTOR2& vr ) { SetRotate_(  vr.x,  vr.y,  RZ   ); }
void SOBJ_R_I::SetRotate( const DVECTOR& vr ) { SetRotate_(  vr.x,  vr.h,  vr.y ); }
void SOBJ_R_I::AddRotate( const VECTOR3& vr ) { AddRotate_(  vr.x,  vr.y,  vr.z ); }
void SOBJ_R_I::AddRotate( const VECTOR2& vr ) { AddRotate_(  vr.x,  vr.y,  zr   ); }
void SOBJ_R_I::AddRotate( const DVECTOR& vr ) { AddRotate_(  vr.x,  vr.h,  vr.y ); }
void SOBJ_R_I::GetRotate( VECTOR3& vr ) const { GetRotate_( &vr.x, &vr.y, &vr.z ); }
void SOBJ_R_I::GetRotate( VECTOR2& vr ) const { GetRotate_( &vr.x, &vr.y,  np   ); }
void SOBJ_R_I::GetRotate( DVECTOR& vr ) const { GetRotate_( &vr.x, &vr.h, &vr.y ); }

void SOBJ_R_I::SetRotateX( float  rx ){ SetRotate_( rx, RY, RZ ); }
void SOBJ_R_I::SetRotateY( float  ry ){ SetRotate_( RX, ry, RZ ); }
void SOBJ_R_I::SetRotateZ( float  rz ){ SetRotate_( RX, RY, rz ); }
void SOBJ_R_I::AddRotateX( float  rx ){ AddRotate_( rx, zr, zr ); }
void SOBJ_R_I::AddRotateY( float  ry ){ AddRotate_( zr, ry, zr ); }
void SOBJ_R_I::AddRotateZ( float  rz ){ AddRotate_( zr, zr, rz ); }
void SOBJ_R_I::GetRotateX( float* rx ) const { GetRotate_( rx, np, np ); }
void SOBJ_R_I::GetRotateY( float* ry ) const { GetRotate_( np, ry, np ); }
void SOBJ_R_I::GetRotateZ( float* rz ) const { GetRotate_( np, np, rz ); }
auto SOBJ_R_I::GetRotateX( void ) const->float{ return RX; }
auto SOBJ_R_I::GetRotateY( void ) const->float{ return RY; }
auto SOBJ_R_I::GetRotateZ( void ) const->float{ return RZ; }

#undef RX
#undef RY
#undef RZ

#undef np
#undef zr

//========================================
// SCALE
//========================================
// constructor & destructor
//----------------------------------------
SOBJ_S_D::SOBJ_S_D() :
	sv( OPAL_IDV ),
	sm( OPAL_IDM )
{}

//----------------------------------------
// 直接操作
//----------------------------------------
const VECTOR3* SOBJ_S::Scale(  void ) const { return &sv; }

void SOBJ_S::Scale( const VECTOR3* vs ){ if ( vs ){ sv = *vs; } }
void SOBJ_S::Scale( const VECTOR3& vs ){ Scale(  &vs ); }

//----------------------------------------
// 基底操作
//----------------------------------------
void SOBJ_S::SetScale_( float  sx, float  sy, float  sz ){

	if ( !FEQUAL( sv.x, sx ) ) { sv.x = sx; df |= OC::FLAG_SX; }
	if ( !FEQUAL( sv.y, sy ) ) { sv.y = sy; df |= OC::FLAG_SY; }
	if ( !FEQUAL( sv.z, sz ) ) { sv.z = sz; df |= OC::FLAG_SZ; }
}

void SOBJ_S::AddScale_( float  sx, float  sy, float  sz ){

	if ( !FEQUAL( sv.x, sx+sv.x ) ) { sv.x += sx; df |= OC::FLAG_SX; }
	if ( !FEQUAL( sv.y, sy+sv.y ) ) { sv.y += sy; df |= OC::FLAG_SY; }
	if ( !FEQUAL( sv.z, sz+sv.z ) ) { sv.z += sz; df |= OC::FLAG_SZ; }
}

void SOBJ_S::GetScale_( float* sx, float* sy, float* sz ) const {

	if ( sx ) { *sx = sv.x; }
	if ( sy ) { *sy = sv.y; }
	if ( sz ) { *sz = sv.z; }
}

//----------------------------------------
// インターフェース
//----------------------------------------
#define np	nullptr
#define zr	0.0f
#define on	1.0f

#define SX	[this]{ float x; GetScale_( &x, np, np ); return x; }()
#define SY	[this]{ float y; GetScale_( np, &y, np ); return y; }()
#define SZ	[this]{ float z; GetScale_( np, np, &z ); return z; }()

void SOBJ_S_I::SetScale( float sx, float sy, float sz ){ SetScale_( sx, sy, sz ); }
void SOBJ_S_I::SetScale( float sx, float sy           ){ SetScale_( sx, sy, SZ ); }
void SOBJ_S_I::SetScale( float sa                     ){ SetScale_( sa, sa, sa ); }
void SOBJ_S_I::SetScale( void                         ){ SetScale_( on, on, on ); }

void SOBJ_S_I::AddScale( float sx, float sy, float sz ){ AddScale_( sx, sy, sz ); }
void SOBJ_S_I::AddScale( float sx, float sy           ){ AddScale_( sx, sy, zr ); }
void SOBJ_S_I::AddScale( float sa                     ){ AddScale_( sa, sa, sa ); }

void SOBJ_S_I::GetScale( float* sx, float* sy, float* sz ) const { GetScale_( sx, sy, sz ); }
void SOBJ_S_I::GetScale( float* sx, float* sy            ) const { GetScale_( sx, sy, np ); }
void SOBJ_S_I::GetScale( float* sa                       ) const { GetScale_( np, np, sa ); }
auto SOBJ_S_I::GetScale( void ) const->float{ return SZ; }

void SOBJ_S_I::SetScale( const std::tuple<float,float,float>& vt ){ SetScale_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_S_I::AddScale( const std::tuple<float,float,float>& vt ){ AddScale_( std::get<0>(vt),std::get<1>(vt),std::get<2>(vt) );	}
void SOBJ_S_I::SetScale( const std::pair< float,float      >& vt ){ SetScale_( std::get<0>(vt),std::get<1>(vt),SZ );	}
void SOBJ_S_I::AddScale( const std::pair< float,float      >& vt ){ AddScale_( std::get<0>(vt),std::get<1>(vt),zr );	}

void SOBJ_S_I::SetScale( const SOBJ* sp ) { if ( sp ) { SetScale_(  sp->rv.x,  sp->rv.y,  sp->rv.z ); } }
void SOBJ_S_I::GetScale( SOBJ* sp ) const { if ( sp ) { GetScale_( &sp->rv.x, &sp->rv.y, &sp->rv.z ); } }
void SOBJ_S_I::SetScale( const SOBJ& sp ) { SetScale_(  sp.rv.x,  sp.rv.y,  sp.rv.z ); }
void SOBJ_S_I::GetScale( SOBJ& sp ) const { GetScale_( &sp.rv.x, &sp.rv.y, &sp.rv.z ); }

void SOBJ_S_I::SetScale( const VECTOR3* vs ) { if ( vs ) { SetScale_(  vs->x,  vs->y,  vs->z ); } }
void SOBJ_S_I::SetScale( const VECTOR2* vs ) { if ( vs ) { SetScale_(  vs->x,  vs->y,  SZ    ); } }
void SOBJ_S_I::SetScale( const DVECTOR* vs ) { if ( vs ) { SetScale_(  vs->x,  vs->h,  vs->y ); } }
void SOBJ_S_I::AddScale( const VECTOR3* vs ) { if ( vs ) { AddScale_(  vs->x,  vs->y,  vs->z ); } }
void SOBJ_S_I::AddScale( const VECTOR2* vs ) { if ( vs ) { AddScale_(  vs->x,  vs->y,  zr    ); } }
void SOBJ_S_I::AddScale( const DVECTOR* vs ) { if ( vs ) { AddScale_(  vs->x,  vs->h,  vs->y ); } }
void SOBJ_S_I::GetScale( VECTOR3* vs ) const { if ( vs ) { GetScale_( &vs->x, &vs->y, &vs->z ); } }
void SOBJ_S_I::GetScale( VECTOR2* vs ) const { if ( vs ) { GetScale_( &vs->x, &vs->y,  np    ); } }
void SOBJ_S_I::GetScale( DVECTOR* vs ) const { if ( vs ) { GetScale_( &vs->x, &vs->h, &vs->y ); } }

void SOBJ_S_I::SetScale( const VECTOR3& vs ) { SetScale_(  vs.x,  vs.y,  vs.z ); }
void SOBJ_S_I::SetScale( const VECTOR2& vs ) { SetScale_(  vs.x,  vs.y,  SZ   ); }
void SOBJ_S_I::SetScale( const DVECTOR& vs ) { SetScale_(  vs.x,  vs.h,  vs.y ); }
void SOBJ_S_I::AddScale( const VECTOR3& vs ) { AddScale_(  vs.x,  vs.y,  vs.z ); }
void SOBJ_S_I::AddScale( const VECTOR2& vs ) { AddScale_(  vs.x,  vs.y,  zr   ); }
void SOBJ_S_I::AddScale( const DVECTOR& vs ) { AddScale_(  vs.x,  vs.h,  vs.y ); }
void SOBJ_S_I::GetScale( VECTOR3& vs ) const { GetScale_( &vs.x, &vs.y, &vs.z ); }
void SOBJ_S_I::GetScale( VECTOR2& vs ) const { GetScale_( &vs.x, &vs.y,  np   ); }
void SOBJ_S_I::GetScale( DVECTOR& vs ) const { GetScale_( &vs.x, &vs.h, &vs.y ); }

void SOBJ_S_I::SetScaleX( float  sx ){ SetScale_( sx, SY, SZ ); }
void SOBJ_S_I::SetScaleY( float  sy ){ SetScale_( SX, sy, SZ ); }
void SOBJ_S_I::SetScaleZ( float  sz ){ SetScale_( SX, SY, sz ); }
void SOBJ_S_I::AddScaleX( float  sx ){ AddScale_( sx, zr, zr ); }
void SOBJ_S_I::AddScaleY( float  sy ){ AddScale_( zr, sy, zr ); }
void SOBJ_S_I::AddScaleZ( float  sz ){ AddScale_( zr, zr, sz ); }
void SOBJ_S_I::GetScaleX( float* sx ) const { GetScale_( sx, np, np ); }
void SOBJ_S_I::GetScaleY( float* sy ) const { GetScale_( np, sy, np ); }
void SOBJ_S_I::GetScaleZ( float* sz ) const { GetScale_( np, np, sz ); }
auto SOBJ_S_I::GetScaleX( void ) const->float{ return SX; }
auto SOBJ_S_I::GetScaleY( void ) const->float{ return SY; }
auto SOBJ_S_I::GetScaleZ( void ) const->float{ return SZ; }

#undef SX
#undef SY
#undef SZ

#undef np
#undef zr
#undef on

// End Of File
