//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ表示モード
//----------------------------------------
#ifdef OPAL_DEBUG
void DRAWX::Debug( int m ){ debug = m;		}
int  DRAWX::Debug( void  ){ return debug;	}
#endif

//----------------------------------------
// デフォルト設定
//----------------------------------------
void  DRAWX::Linx( LINX* l ){ sysdef.linx =  l;	}
void  DRAWX::Linx( LINX& l ){ sysdef.linx = &l;	}
void  DRAWX::Linx( std::shared_ptr<LINX> l ){ sysdef.linx = l.get(); }
LINX* DRAWX::Linx( void ){ return sysdef.linx;	}

void  DRAWX::Parent( ZOBJ* p ){ sysdef.parent =  p;	}
void  DRAWX::Parent( ZOBJ& p ){ sysdef.parent = &p;	}
void  DRAWX::Parent( std::shared_ptr<ZOBJ> p ){ sysdef.parent = p.get(); }
ZOBJ* DRAWX::Parent( void ){ return sysdef.parent;	}

void DRAWX::Disp( int f ){ FLAG_CTRL( sysdef.disp, f );		}
bool DRAWX::Disp( void ){ return sysdef.disp;				}

void DRAWX::Anime( int f ){ FLAG_CTRL( sysdef.anime, f );	}
bool DRAWX::Anime( void ){ return sysdef.anime;				}

void DRAWX::Alpha( int f ){ FLAG_CTRL( sysdef.alpha, f );	}
bool DRAWX::Alpha( void ){ return sysdef.alpha;				}

void DRAWX::Prio( int v ){ sysdef.prio = v;					}
UINT DRAWX::Prio( void ){ return sysdef.prio;				}

//----------------------------------------
// スクリーン座標⇒ワールド座標
//----------------------------------------
VECTOR3* DRAWX::Screen2World( VECTOR3* wv, const VECTOR3* sv, const MATRIX* vw, const MATRIX* pj, const MATRIX* vp ){

	if ( wv && sv && vw && pj ) {
		MATRIX	ivw;
		MATRIX	ipj;
		MATRIX	ivp;

		MatrixInverse( &ivw, nullptr, vw );
		MatrixInverse( &ipj, nullptr, pj );
		MatrixInverse( &ivp, nullptr, vp ? vp : DRAWX::Viewport() );

		const auto	im = ivp * ipj * ivw;

		Vec3TransformCoord( wv, sv, &im );
	}
	return wv;
}

MATRIX* DRAWX::Screen2World( MATRIX* sm, const MATRIX* wm, const MATRIX* vw, const MATRIX* pj, const MATRIX* vp ){

	if ( sm && wm && vw && pj ) {
		const auto	sv = VECTOR3( wm->_41, wm->_42, wm->_43 );
		VECTOR3		wv;

		Screen2World( &wv, &sv, vw, pj, vp );

		sm->_11 = wm->_11;
		sm->_12 = wm->_12;
		sm->_13 = wm->_13;
		sm->_14 = wm->_14;
		sm->_21 = wm->_21;
		sm->_22 = wm->_22;
		sm->_23 = wm->_23;
		sm->_24 = wm->_24;
		sm->_31 = wm->_31;
		sm->_32 = wm->_32;
		sm->_33 = wm->_33;
		sm->_14 = wm->_34;
		sm->_41 = wv.x;
		sm->_42 = wv.y;
		sm->_43 = wv.z;
		sm->_44 = wm->_44;
	}
	return sm;
}

// End Of File
