//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// サウンドタスク
//========================================
using namespace opal;

//----------------------------------------
// Etc
//----------------------------------------
void CALLT::CallInit(  void ){ ZOBJ::TaskInit();	}
void CALLT::CallEnter( void ){ ZOBJ::TaskEnter();	}
void CALLT::CallFunc(  void ){ ZOBJ::TaskFunc();	}
void CALLT::CallBegin( void ){ ZOBJ::TaskBegin();	}
void CALLT::CallMain(  void ){ ZOBJ::TaskMain();	}
void CALLT::CallEnd(   void ){ ZOBJ::TaskEnd();		}
void CALLT::CallExit(  void ){ ZOBJ::TaskExit();	}
void CALLT::CallFree(  void ){ ZOBJ::TaskFree();	}

//----------------------------------------
// Info
//----------------------------------------
void CALLT::CallInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "CALLT" );
	printd( "%-08s ", ID() );
	CALLT::CallInfo( CALLX::Debug() );
	printd( "\n" );
#endif
}

void CALLT::CallInfo( int m ) const {
#ifdef OPAL_DEBUG
	static const char*	playmode[] = { "||", ">|", "|>", "<>", ">>", };

	switch ( m ) {
	default: break;
	case  1: printd( "Prio %02x|%02d ", GetPrio(), GetLevel() );	break;
	case  2: printd( "Level %d|%d %c ", GetLevel( SIGL::PRIMAL ), GetLevel( SIGL::FINAL  ), GetFunc( 0 ) ? '+' : '-' );	break;
	case  3: printd( "Parent %c%c ",  Parent()  ? 'P' : '-', Adopter() ? 'A' : '-' );	break;
	case  4: printd( "Play %c[%s] ", GetPlay() ? '+' : '-', playmode[PlayMode()] );	break;
	case  5: printd( "Time %08d ", PlayTime() );	break;
	case  6: printd( "Repeat %02d/%02d ", GetRepeat(), PlayLoop() );	break;
	case  7: printd( "Loop %c%c ", GetLoop( 0 ) ? '+' : '-', GetLoop( 1 ) ? '+' : '-' );	break;
	case  8: printd( "Mute %c%c ", GetMute( 0 ) ? '+' : '-', GetMute( 1 ) ? '+' : '-' );	break;
	case  9: printd( "Volume %f ", GetVolume() );	break;
	case 10: printd( "Tempo %f ",  GetTempo() );		break;
	case 11: printd( "Fade %d ",   GetFade() );		break;
	}
#endif
}

// End Of File
