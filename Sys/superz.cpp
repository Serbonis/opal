//========================================
// OPAL Game Library
//========================================
#include "super.hpp"
#include "Obj/zobj.hpp"

//========================================
// �X�[�p�[�^�X�N
//========================================
using namespace opal;

//----------------------------------------
// �o�^
//----------------------------------------
auto SUPERX::Register( const POBJ* p )->const FMAP*{

	return ( p && pmap.count( p ) ) ? pmap.at( p ).get() : nullptr;
}

void SUPERX::Register( COBJ* c, const POBJ* p, FUNC f ){

	if ( p && !pmap.count( p ) ) {
		pmap[p] = std::make_shared<FMAP>();
	}

	if ( c && ( cmap[c] = p ) ) {
		(*pmap[p])[c] = f;
	}
}

//----------------------------------------
// ���
//----------------------------------------
void SUPERX::Release( ZOBJ* x ){

	if ( pmap.count( x ) ) {
		for ( const auto [c,f] : *pmap.at( x ) ) {
			if ( f ) {
				f( c );
			}

			if ( cmap.count( c ) ) {
				cmap.erase( c );
			}
		}
		pmap.erase( x );
	}

	if ( cmap.count( x ) ) {
		if ( const auto p = cmap.at( x ) ) {
			if ( pmap.count( p ) ) {
				if ( auto& m = pmap.at( p ) ) {
					if ( const auto f = m->at( x ) ) {
						f( x );
					}

					m->erase( x );

					if ( !m->size() ) {
						pmap.erase( p );
					}
				}
			}
		}
		cmap.erase( x );
	}
}

// End Of File
