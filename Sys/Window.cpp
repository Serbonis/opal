//========================================
// OPAL Game Library
//========================================
#include "window.hpp"

//========================================
// ウインドウクラス
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
WINDOW::WINDOW() :
	handle( nullptr ),
	message( 0 ),
	style(   0 ),
	active( false ),
	focus(  true  ),
	moving( false ),
	sizing( 0 ),
	full(   false ),
	menu(   nullptr ),
	window(),
	screen(),
	display(),
	cursor()
{}

//----------------------------------------
// ウィンドウハンドル/メッセージ取得
//----------------------------------------
HWND WINDOW::Handle(  void ) const { return handle;		}
UINT WINDOW::Message( void ) const { return message;	}

//----------------------------------------
// ウィンドウオープン/クローズ
//----------------------------------------
void WINDOW::Open( void ){

	WNDCLASSEX	wc;

	wc.cbSize		= sizeof( wc );
	wc.style		= CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc	= Event;
	wc.cbClsExtra	= 0;
	wc.cbWndExtra	= 0;
	wc.hInstance	= ::GetModuleHandle( nullptr );
	wc.hIcon		= ::LoadIcon( wc.hInstance, OPAL::ICON() );
	wc.hIconSm		= wc.hIcon;
	wc.hCursor		= ( HCURSOR )::LoadImage( nullptr,IDC_ARROW, IMAGE_CURSOR,0,0,LR_DEFAULTSIZE|LR_SHARED );
	wc.hbrBackground= ( HBRUSH  )::GetStockObject( BLACK_BRUSH );
	wc.lpszMenuName	= nullptr;
	wc.lpszClassName= OPAL::PROJECT();

	RegisterClassEx( &wc );

	style		= OPAL::WINSTYLE();
	screen.w	= OPAL::SCREEN_W();
	screen.h	= OPAL::SCREEN_H();
	display.w	= OPAL::DISPLAY_W();
	display.h	= OPAL::DISPLAY_H();

	window.std.x = window.now.x = ( ::GetSystemMetrics( SM_CXSCREEN ) - screen.w )/ 2;
	window.std.y = window.now.y = ( ::GetSystemMetrics( SM_CYSCREEN ) - screen.h )/ 2;
	window.std.w = window.now.w = screen.w + ::GetSystemMetrics( SM_CXFRAME ) * 2;
	window.std.h = window.now.h = screen.h + ::GetSystemMetrics( SM_CYFRAME ) * 2 + ::GetSystemMetrics( SM_CYCAPTION );

	handle = ::CreateWindow(wc.lpszClassName,
							wc.lpszClassName,
							style,
							window.std.x,window.std.y,
							window.std.w,window.std.h,
							nullptr,nullptr,wc.hInstance,nullptr );

	full = false;
	menu = ::GetMenu( handle );

	if ( !( cursor.show = OPAL::CURSOR() ) ) {
		::ShowCursor( false );
	}

	::ShowWindow(   handle, SW_HIDE );
	::UpdateWindow( handle );
}


void WINDOW::Close( void ){}

//----------------------------------------
// メッセージキューのチェック
//----------------------------------------
int WINDOW::Process( void ){

	MSG	msg;

	if ( ::PeekMessage( &msg, nullptr, 0, 0, PM_NOREMOVE ) ) {
		switch ( ::GetMessage( &msg, nullptr, 0, 0 ) ) {
		case  0: return PROCESS_QUIT;
		case -1: return PROCESS_ERROR;
		}
		::TranslateMessage( &msg );
		::DispatchMessage( &msg );
		return PROCESS_MESSAGE;
	}
	return PROCESS_GAME;
}

// End Of File
