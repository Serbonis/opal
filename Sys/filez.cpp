//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルシステム
//========================================
using namespace opal;

#ifdef OPAL_DEBUG
//----------------------------------------
// デバッグ
//----------------------------------------
void FILEX::Debug( UINT m ){

	printd( "MODE %08x\n", mode );
	printd( "PATH %s\n",   path() );
	printd( "BASE %08x\n", base );
	printd( "ADRS %08x\n", adrs );
	printd( "SIZE %d\n",   size );
	printd( "INFO %p\n",   file );
	printd( "\n" );

	static const char*	name[] = {
		"FILE-READ",
		"FILE-WRITE",
	};

	for ( auto i = 0UL; i < MAX; i++ ) {
		printd( "[%s]\n", name[i] );
		for ( const auto& [k,v] : info[i].Data() ) {
			printd( "%p %8d %s\n", v->Adrs(), v->Size(), k.c_str() );
		}
		printd( "\n" );
	}
}
#endif

// End Of File
