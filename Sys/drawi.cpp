//========================================
// OPAL Game Library
//========================================
#include "debug.hpp"
#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;

//----------------------------------------
// メソッド
//----------------------------------------
namespace opal::DRAWX_ {
	extern void Device( void );
	extern void Spec(   void );

	extern void DeviceLost( const DEVICE_NAME, XDEVICE* );
	extern void DeviceLost( const DEVICE_NAME );
	extern UINT DeviceLost( void );

	extern const MATRIX* Viewport( void );
	extern const MATRIX* VportInv( void );
	extern void SetViewport( const SRECTF& );
	extern void SetViewport( float, float, float, float );
	extern void SetViewport( const VECTOR2&, float, float );
	extern void SetViewport( float, float, const SSIZE& );
	extern void SetViewport( void );
	extern auto GetViewport( void )->SRECTF;
	extern void SetViewportPosition( float, float );
	extern void SetViewportPosition( const VECTOR2& );
	extern void SetViewportPosition( void );
	extern auto GetViewportPosition( void )->VECTOR2;
	extern void SetViewportSize( float, float );
	extern void SetViewportSize( const SSIZE& );
	extern void SetViewportSize( void );
	extern auto GetViewportSize( void )->SSIZE;
	extern void ViewportBegin( void );
	extern void ViewportBegin( const SRECTF& );
	extern void ViewportBegin( float, float, float, float );
	extern void ViewportBegin( const VECTOR2&, float, float );
	extern void ViewportBegin( float, float, const SSIZE& );
	extern void ViewportBegin( const VECTOR2&, const SSIZE& );
	extern void ViewportEnd( void );

	extern void SetScissor( const SRECTF& );
	extern void SetScissor( float, float, float, float );
	extern void SetScissor( const VECTOR2&, float, float );
	extern void SetScissor( float, float, const SSIZE& );
	extern void SetScissor( const VECTOR2&, const SSIZE& );
	extern void SetScissor( void );
	extern auto GetScissor( void )->SRECTF;

	extern COLOR Color( void );
	extern void  Color( COLOR );
	extern void  Color( UCHAR, UCHAR, UCHAR );
}

void DRAWX::Device( void ){ DRAWX_::Device();	}
void DRAWX::Spec(   void ){ DRAWX_::Spec(); 	}

void DRAWX::DeviceLost( const DEVICE_NAME n, XDEVICE* d ){ DRAWX_::DeviceLost( n, d );	}
void DRAWX::DeviceLost( const DEVICE_NAME n ){ DRAWX_::DeviceLost( n );	}
void DRAWX::DeviceLost( const XDEVICE* n, XDEVICE* d ){  DRAWX_::DeviceLost( ( const char* )NAME( "%08x", n ), d );	}
void DRAWX::DeviceLost( const XDEVICE* n ){ DRAWX_::DeviceLost( ( const char* )NAME( "%08x", n ) );	}
UINT DRAWX::DeviceLost( void ){ return DRAWX_::DeviceLost();	}

const MATRIX* DRAWX::Viewport( void ){ return DRAWX_::Viewport();	}
const MATRIX* DRAWX::VportInv( void ){ return DRAWX_::VportInv();	}
void DRAWX::SetViewport( const SRECTF& v ){ DRAWX_::SetViewport();	}
void DRAWX::SetViewport( float x, float y, float w, float h ){ DRAWX_::SetViewport( x, y, w, h );	}
void DRAWX::SetViewport( const VECTOR2& p, float w, float h ){ DRAWX_::SetViewport( p, w, h );	}
void DRAWX::SetViewport( float x, float y, const SSIZE& s ){ DRAWX_::SetViewport( x, y, s );	}
void DRAWX::SetViewport( void ){ DRAWX_::SetViewport();	}
auto DRAWX::GetViewport( void )->SRECTF{ return DRAWX_::GetViewport();	}
void DRAWX::SetViewportPosition( float x, float y ){ DRAWX_::SetViewportPosition( x, y );	}
void DRAWX::SetViewportPosition( const VECTOR2& p ){ DRAWX_::SetViewportPosition( p );	}
void DRAWX::SetViewportPosition( void ){ DRAWX_::SetViewportPosition();	}
auto DRAWX::GetViewportPosition( void )->VECTOR2{ return DRAWX_::GetViewportPosition();	}
void DRAWX::SetViewportSize( float w, float h ){ DRAWX_::SetViewportSize( w, h );	}
void DRAWX::SetViewportSize( const SSIZE& s ){ DRAWX_::SetViewportSize( s );	}
void DRAWX::SetViewportSize( void ){ DRAWX_::SetViewportSize();	}
auto DRAWX::GetViewportSize( void )->SSIZE{ return DRAWX_::GetViewportSize();	}
void DRAWX::ViewportBegin( void ){ DRAWX_::ViewportBegin();	}
void DRAWX::ViewportBegin( const SRECTF& v ){ DRAWX_::ViewportBegin( v );	}
void DRAWX::ViewportBegin( float x, float y, float w, float h ){ DRAWX_::ViewportBegin( x, y, w, h );	}
void DRAWX::ViewportBegin( const VECTOR2& p, float w, float h ){ DRAWX_::ViewportBegin( p, w, h );	}
void DRAWX::ViewportBegin( float x, float y, const SSIZE& s ){ DRAWX_::ViewportBegin( x, y, s );	}
void DRAWX::ViewportBegin( const VECTOR2& p, const SSIZE& s ){ DRAWX_::ViewportBegin( p, s );	}
void DRAWX::ViewportEnd( void ){ DRAWX_::ViewportEnd();	}

void DRAWX::SetScissor( const SRECTF& v ){ DRAWX_::SetScissor( v );	}
void DRAWX::SetScissor( float x, float y, float w, float h ){ DRAWX_::SetScissor( x, y, w, h );	}
void DRAWX::SetScissor( const VECTOR2& p, float w, float h ){ DRAWX_::SetScissor( p, w, h );	}
void DRAWX::SetScissor( float x, float y, const SSIZE& s ){ DRAWX_::SetScissor( x, y, s );	}
void DRAWX::SetScissor( const VECTOR2& p, const SSIZE& s ){ DRAWX_::SetScissor( p, s );	}
void DRAWX::SetScissor( void ){ DRAWX_::SetScissor();	}
auto DRAWX::GetScissor( void )->SRECTF{ return DRAWX_::GetScissor();	}

COLOR DRAWX::Color( void ){ return DRAWX_::Color();	}
void  DRAWX::Color( COLOR c ){ DRAWX_::Color( c );	}
void  DRAWX::Color( UCHAR r, UCHAR g, UCHAR b ){ DRAWX_::Color( r, g, b );	}

// End Of File
