//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// デバイスの情報
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal::DRAWX_DX11 {
	void Device( void ){

		const auto	dv = device();

		printd( "DEVICE  %p\n", dv );
		printd( "\n" );

		printd( "DEVICE LOST\n" );
		for ( const auto [d,p] : device.lost() ) {
			printd( "%p %s\n", p, d.c_str() );
		}
		printd( "\n" );
	}
}
#endif

//----------------------------------------
// デバイスの取得
//----------------------------------------
namespace opal::DRAWX_DX11 {
	UINT TextureStage( void ){ return 0; } // device.TextureStage();	}
}

//----------------------------------------
// デバイスロスト
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void DeviceLost( const DEVICE_NAME n, XDEVICE* d ){ device.DeviceLost( n, d );	}
	void DeviceLost( const DEVICE_NAME n ){ device.DeviceLost( n );	}
	UINT DeviceLost( void ){ return device.DeviceLost(); }
}

//----------------------------------------
// ビューポートの取得/設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	const MATRIX* Viewport( void ){ return viewport.Matrix();	}
	const MATRIX* VportInv( void ){ return viewport.Inverse();	}

	void SetViewport( const SRECTF& v ){ viewport.Set( v );	}
	void SetViewport( float x, float y, float w, float h ){ viewport.Set( x, y, w, h );	}
	void SetViewport( const VECTOR2& p, float w, float h ){ viewport.Set( p, w, h );	}
	void SetViewport( float x, float y, const SSIZE& s ){ viewport.Set( x, y, s );	}
	void SetViewport( const VECTOR2& p, const SSIZE& s ){ viewport.Set( p, s );	}
	void SetViewport( void ){ viewport.Set();	}
	auto GetViewport( void )->SRECTF{ return viewport.Get();	}

	void SetViewportPosition( float x, float y ){ viewport.SetPosition( x, y );	}
	void SetViewportPosition( const VECTOR2& p ){ viewport.SetPosition( p );	}
	void SetViewportPosition( void ){ viewport.SetPosition();	}
	auto GetViewportPosition( void )->VECTOR2{ return viewport.GetPosition();	}

	void SetViewportSize( float w, float h ){ viewport.SetSize( w, h );	}
	void SetViewportSize( const SSIZE& s ){ viewport.SetSize( s );	}
	void SetViewportSize( void ){ viewport.SetSize();	}
	auto GetViewportSize( void )->SSIZE{ return viewport.GetSize();	}

	void ViewportBegin( void ){ viewport.Begin( device );	}
	void ViewportBegin( const SRECTF& v ){ SetViewport( v ); viewport.Begin( device );	}
	void ViewportBegin( float x, float y, float w, float h ){ SetViewport( x, y, w, h ); viewport.Begin( device );	}
	void ViewportBegin( const VECTOR2& p, float w, float h ){ SetViewport( p, w, h ); viewport.Begin( device );	}
	void ViewportBegin( float x, float y, const SSIZE& s ){ SetViewport( x, y, s );	viewport.Begin( device );	}
	void ViewportBegin( const VECTOR2& p, const SSIZE& s ){ SetViewport( p, s );	viewport.Begin( device );	}
	void ViewportEnd( void ){ viewport.End( device );	}
}

//----------------------------------------
// シザーの取得/設定
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void SetScissor( const SRECTF& v ){ SCISSOR::Set( v );	}
	void SetScissor( float x, float y, float w, float h ){ SCISSOR::Set( x, y, w, h );	}
	void SetScissor( const VECTOR2& p, float w, float h ){ SCISSOR::Set( p, w, h );	}
	void SetScissor( float x, float y, const SSIZE& s ){ SCISSOR::Set( x, y, s );	}
	void SetScissor( const VECTOR2& p, const SSIZE& s ){ SCISSOR::Set( p, s );	}
	void SetScissor( void ){ SCISSOR::Set();	}
	auto GetScissor( void )->SRECTF{ return SCISSOR::Get();	}
}

//----------------------------------------
// 背景色
//----------------------------------------
namespace opal::DRAWX_DX11 {
	COLOR Color( void ){ return clearbuffer.Color(); }
	void Color( COLOR vCol ){ clearbuffer.Color( vCol );	}
	void Color( UCHAR vR, UCHAR vG, UCHAR vB ){ clearbuffer.Color( vR, vG, vB );	}
}

//----------------------------------------
// テクスチャー退避/復帰
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void SaveTexture( DWORD vStg ){}
	void LoadTexture( DWORD vStg ){}
}

// End Of File
