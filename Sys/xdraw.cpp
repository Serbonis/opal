//========================================
// OPAL Game Library
//========================================
#include "xdraw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;

//----------------------------------------
// ベースAPI
//----------------------------------------
#if defined ( OPAL_DX11 )
#define _DRAWX_	DRAWX_DX11
#else
#error "描画システムが定義されていません"
#endif

//========================================
// グラフィックシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal::_DRAWX_ {
	extern void SysInit( void );
	extern void SysExec( void );
	extern void SysFree( void );

	extern void SysInitAdapter( void );
	extern void SysFreeAdapter( void );

	extern void SysInitDevice( HWND, UINT, UINT );
	extern void SysFreeDevice( void );

	extern void SysInitMonitor( void );
	extern void SysFreeMonitor( void );

	extern void SysInitViewport( void );
	extern void SysFreeViewport( void );

	extern void SysInitClearbuffer( void );
	extern void SysFreeClearbuffer( void );

	extern void SysBegin( void );
	extern void SysEnd(   void );
	extern void Present(  void );

	extern void Capture( const char* );
}

namespace opal::DRAWX_ {
	void SysInit( void ){ _DRAWX_::SysInit();	}
	void SysExec( void ){ _DRAWX_::SysExec();	}
	void SysFree( void ){ _DRAWX_::SysFree();	}

	void SysInitAdapter( void ){ _DRAWX_::SysInitAdapter();	}
	void SysFreeAdapter( void ){ _DRAWX_::SysFreeAdapter();	}

	void SysInitDevice( HWND window, UINT w, UINT h ){ _DRAWX_::SysInitDevice( window, w, h );	}
	void SysFreeDevice( void ){ _DRAWX_::SysFreeDevice();	}

	void SysInitMonitor( void ){ _DRAWX_::SysInitMonitor();	}
	void SysFreeMonitor( void ){ _DRAWX_::SysFreeMonitor();	}

	void SysInitViewport( void ){ _DRAWX_::SysInitViewport();	}
	void SysFreeViewport( void ){ _DRAWX_::SysFreeViewport();	}

	void SysInitClearbuffer( void ){ _DRAWX_::SysInitClearbuffer();	}
	void SysFreeClearbuffer( void ){ _DRAWX_::SysFreeClearbuffer();	}

	void SysBegin( void ){ _DRAWX_::SysBegin();	}
	void SysEnd(   void ){ _DRAWX_::SysEnd();	}
	void Present(  void ){ _DRAWX_::Present();	}

	void Capture( const char* file ){ _DRAWX_::Capture( file );	}
}

//----------------------------------------
// メソッド
//----------------------------------------
namespace opal::_DRAWX_ {
	extern void Device( void );
	extern void Spec(   void );

	extern void DeviceLost( const DEVICE_NAME, XDEVICE* );
	extern void DeviceLost( const DEVICE_NAME );
	extern UINT DeviceLost( void );

	extern const MATRIX* Viewport( void );
	extern const MATRIX* VportInv( void );
	extern void SetViewport( const SRECTF& );
	extern void SetViewport( float, float, float, float );
	extern void SetViewport( const VECTOR2&, float, float );
	extern void SetViewport( float, float, const SSIZE& );
	extern void SetViewport( void );
	extern auto GetViewport( void )->SRECTF;
	extern void SetViewportPosition( float, float );
	extern void SetViewportPosition( const VECTOR2& );
	extern void SetViewportPosition( void );
	extern auto GetViewportPosition( void )->VECTOR2;
	extern void SetViewportSize( float, float );
	extern void SetViewportSize( const SSIZE& );
	extern void SetViewportSize( void );
	extern auto GetViewportSize( void )->SSIZE;
	extern void ViewportBegin( void );
	extern void ViewportBegin( const SRECTF& );
	extern void ViewportBegin( float, float, float, float );
	extern void ViewportBegin( const VECTOR2&, float, float );
	extern void ViewportBegin( float, float, const SSIZE& );
	extern void ViewportBegin( const VECTOR2&, const SSIZE& );
	extern void ViewportEnd( void );

	extern void SetScissor( const SRECTF& );
	extern void SetScissor( float, float, float, float );
	extern void SetScissor( const VECTOR2&, float, float );
	extern void SetScissor( float, float, const SSIZE& );
	extern void SetScissor( const VECTOR2&, const SSIZE& );
	extern void SetScissor( void );
	extern auto GetScissor( void )->SRECTF;

	extern COLOR Color( void );
	extern void  Color( COLOR );
	extern void  Color( UCHAR, UCHAR, UCHAR );
}

namespace opal::DRAWX_ {
	void Device( void ){ _DRAWX_::Device();	}
	void Spec(   void ){ _DRAWX_::Spec();	}

	void DeviceLost( const DEVICE_NAME n, XDEVICE* d ){ _DRAWX_::DeviceLost( n, d );	}
	void DeviceLost( const DEVICE_NAME n ){ _DRAWX_::DeviceLost( n );	}
	UINT DeviceLost( void ){ return _DRAWX_::DeviceLost();	}

	const MATRIX* Viewport( void ){ return _DRAWX_::Viewport();	}
	const MATRIX* VportInv( void ){ return _DRAWX_::VportInv();	}
	void SetViewport( const SRECTF& v ){ _DRAWX_::SetViewport( v );	}
	void SetViewport( float x, float y, float w, float h ){ _DRAWX_::SetViewport( x, y, w, h );	}
	void SetViewport( const VECTOR2& p, float w, float h ){ _DRAWX_::SetViewport( p, w, h );	}
	void SetViewport( float x, float y, const SSIZE& s ){ _DRAWX_::SetViewport( x, y, s );	}
	void SetViewport( void ){ _DRAWX_::SetViewport();	}
	auto GetViewport( void )->SRECTF{ return _DRAWX_::GetViewport();	}
	void SetViewportPosition( float x, float y ){ _DRAWX_::SetViewportPosition( x, y );	}
	void SetViewportPosition( const VECTOR2& p ){ _DRAWX_::SetViewportPosition( p );	}
	void SetViewportPosition( void ){ _DRAWX_::SetViewportPosition( );	}
	auto GetViewportPosition( void )->VECTOR2{ return _DRAWX_::GetViewportPosition();	}
	void SetViewportSize( float w, float h ){ _DRAWX_::SetViewportSize( w, h );	}
	void SetViewportSize( const SSIZE& s ){ _DRAWX_::SetViewportSize( s );	}
	void SetViewportSize( void ){ _DRAWX_::SetViewportSize();	}
	auto GetViewportSize( void )->SSIZE{ return _DRAWX_::GetViewportSize();	}
	void ViewportBegin( void ){ _DRAWX_::ViewportBegin();	}
	void ViewportBegin( const SRECTF& v ){ _DRAWX_::ViewportBegin( v );	}
	void ViewportBegin( float x, float y, float w, float h ){ _DRAWX_::ViewportBegin( x, y, w, h );	}
	void ViewportBegin( const VECTOR2& p, float w, float h ){ _DRAWX_::ViewportBegin( p, w, h );	}
	void ViewportBegin( float x, float y, const SSIZE& s ){ _DRAWX_::ViewportBegin( x, y, s );	}
	void ViewportBegin( const VECTOR2& p, const SSIZE& s ){ _DRAWX_::ViewportBegin( p, s );	}
	void ViewportEnd( void ){ _DRAWX_::ViewportEnd();	}

	void SetScissor( const SRECTF& v ){ _DRAWX_::SetScissor( v );	}
	void SetScissor( float x, float y, float w, float h ){ _DRAWX_::SetScissor( x, y, w, h );	}
	void SetScissor( const VECTOR2& p, float w, float h ){ _DRAWX_::SetScissor( p, w, h );	}
	void SetScissor( float x, float y, const SSIZE& s ){ _DRAWX_::SetScissor( x, y, s );	}
	void SetScissor( const VECTOR2& p, const SSIZE& s ){ _DRAWX_::SetScissor( p, s );	}
	void SetScissor( void ){ _DRAWX_::SetScissor();	}
	auto GetScissor( void )->SRECTF{ return _DRAWX_::GetScissor();	}

	COLOR Color( void ){ return _DRAWX_::Color();	}
	void  Color( COLOR c ){ _DRAWX_::Color( c );	}
	void  Color( UCHAR r, UCHAR g, UCHAR b ){ _DRAWX_::Color( r, g, b );	}
}

//----------------------------------------
// システムインスタンス
//----------------------------------------
#include "Draw/cameras.hpp"
#include "Draw/lights.hpp"

namespace opal::DRAWX_ {
	void InitCamera( CAMERAS* p ){ CAMERAS::InitCamera( p );	}
	void ExecCamera( CAMERAS* p ){ CAMERAS::ExecCamera( p );	}
	void FreeCamera( CAMERAS* p ){ CAMERAS::FreeCamera( p );	}

	void InitLight( LIGHTS* p ){ LIGHTS::InitLight( p );	}
	void ExecLight( LIGHTS* p ){ LIGHTS::ExecLight( p );	}
	void FreeLight( LIGHTS* p ){ LIGHTS::FreeLight( p );	}
}

// End Of File

