//========================================
// OPAL Game Library
//========================================
#include "heap.hpp"

//========================================
// HEAP管理システム
//========================================
using namespace opal;

#ifdef OPAL_DEBUG
//----------------------------------------
// デバッグ
//----------------------------------------
void HEAP::Debug( UINT m ){

	const auto	core = Core();

	printd( "HEAP   : %4.2f(MB) %zu(byte)/%d \n", core->size/(1024.f*1024.f), core->size, core->count );

	switch ( m ) {
	case 0:
		printd( "----------------------------------------\n" );
		core->hash.Debug( false );
		break;

	case 1:
		break;

	case 2:
		core->hash.Debug( true );
		break;
	}
}
#endif

// End Of File
