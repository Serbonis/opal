//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// サウンドワーク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
CALLS::CALLS(){}
CALLS::~CALLS(){}

CALLS::FLAG::FLAG() :
	mute{false},
	loop{false}
{}

CALLS::PROP::PROP() :
	volume( 1.0f ),
	tempo(  1.0f ),
	repeat( 0 ),
	fade(   0 ),
	ctrl(),
	state()
{}
CALLS::PROP::CTRL::CTRL() :
	play( false ),
	stop( false ),
	volm( false ),
	tmpo( false )
{}
CALLS::PROP::STATE::STATE() :
	mode( MODE_NULL ),
	play( false ),
	time( 0 ),
	loop( 0 )
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void CALLS::Init( void ){

	state	= CALL_STATE_DEFAULT;

	new ( &flag ) FLAG;
	new ( &prop ) PROP;

	flag.mute.s	= FLAG_SUPER_NULL;
	flag.loop.s	= FLAG_SUPER_NULL;
}

void CALLS::Free( void ){}

//----------------------------------------
// 発音フラグ
//----------------------------------------
void CALLS::SetMute( int f ){ FLAG_CTRL( flag.mute	.v[0], f ); prop.ctrl.volm  = true;	}
void CALLS::SetLoop( int f ){ FLAG_CTRL( flag.loop	.v[0], f );							}

bool CALLS::GetMute( void   ) const { return flag.mute	.v[ 0 ] ? true : false;	}
bool CALLS::GetLoop( void   ) const { return flag.loop	.v[ 0 ] ? true : false;	}
bool CALLS::GetMute( UINT n ) const { return flag.mute	.v[n%2] ? true : false;	}
bool CALLS::GetLoop( UINT n ) const { return flag.loop	.v[n%2] ? true : false;	}

void CALLS::SetSuperMute( int m ){ FLAG_CTRL( flag.mute	.s, m );	}
void CALLS::SetSuperLoop( int m ){ FLAG_CTRL( flag.loop	.s, m );	}

void CALLS::SetSuperMute( void  ){ flag.mute	.s = FLAG_SUPER_NULL;	}
void CALLS::SetSuperLoop( void  ){ flag.loop	.s = FLAG_SUPER_NULL;	}

int  CALLS::GetSuperMute( void ) const { return flag.mute	.s;	}
int  CALLS::GetSuperLoop( void ) const { return flag.loop	.s;	}

//----------------------------------------
// 発音プロパティ
//----------------------------------------
void  CALLS::SetVolume( float v ){ prop.volume = v; prop.ctrl.volm = true;	}
void  CALLS::SetTempo(  float v ){ prop.tempo  = v; prop.ctrl.tmpo = true;	}
void  CALLS::SetRepeat( UINT  v ){ prop.repeat = v; 							}
void  CALLS::SetFade(   UINT  v ){ prop.fade   = v; 							}

float CALLS::GetVolume( void ) const { return prop.volume;	}
float CALLS::GetTempo(  void ) const { return prop.tempo;	}
UINT  CALLS::GetRepeat( void ) const { return prop.repeat;	}
UINT  CALLS::GetFade(   void ) const { return prop.fade;		}

//----------------------------------------
// 再生/停止
//----------------------------------------
void CALLS::SetPlay( UINT v ){ prop.ctrl.play = true; prop.ctrl.stop = false; prop.fade = v; }
void CALLS::SetStop( UINT v ){ prop.ctrl.stop = true; prop.ctrl.play = false; prop.fade = v; }

bool CALLS::GetPlay( void ) const { return prop.state.play ? true  : false; }
bool CALLS::GetStop( void ) const { return prop.state.play ? false : true ; }

//----------------------------------------
// 発音ステータス
//----------------------------------------
UINT CALLS::PlayMode( void ) const { return prop.state.mode;	}
UINT CALLS::PlayTime( void ) const { return prop.state.time;	}
UINT CALLS::PlayLeng( void ) const { return prop.state.leng;	}
UINT CALLS::PlayLoop( void ) const { return prop.state.loop;	}

//----------------------------------------
// 状態更新
//----------------------------------------
void CALLS::Coordinate( const CALLS* p ) {

	if ( p == nullptr ) {
		flag.mute.v[1]	= flag.mute.v[0];
		flag.loop.v[1]	= flag.loop.v[0];
	} else {
		flag.mute.v[1]	= FLAG_COORDINATE( flag.mute.s, flag.mute.v[0], p->flag.mute.v[1] );
		flag.loop.v[1]	= FLAG_COORDINATE( flag.loop.s, flag.loop.v[0], p->flag.loop.v[1] );
	}
}

// End Of File
