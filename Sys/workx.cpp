//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// コードワーク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
WORKS::WORKS(){}
WORKS::~WORKS(){}

WORKS::FLAG::FLAG(){}
WORKS::PROP::PROP(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void WORKS::Init( void ){

	state	= WORK_STATE_DEFAULT;

	new ( &flag ) FLAG;
	new ( &prop ) PROP;
}

void WORKS::Free( void ){}

//----------------------------------------
// 状態更新
//----------------------------------------
void WORKS::Coordinate( const WORKS* p ) {

	//if ( p == nullptr ) {} else {}
}

// End Of File
