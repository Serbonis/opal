//========================================
// OPAL Game Library
//========================================
#include "Attr/size.hpp"

//========================================
// サイズリソース
//========================================
using namespace opal;

//----------------------------------------
// SIZE1
//----------------------------------------
SIZE1::SIZE1( float w ) : CSIZE1F( w ) {}
SIZE1::SIZE1(){}
SIZE1::~SIZE1(){}

void SIZE1::MulSize_( float w ){ SetSize_( this->w * w ); }
void SIZE1::AddSize_( float w ){ SetSize_( this->w + w ); }

void SIZE1::MulSize(  float w ){ MulSize_( w ); }
void SIZE1::MulSizeW( float w ){ MulSize_( w ); }
void SIZE1::MulSize(  const SIZE1* v ){ if ( v ) { MulSize_( v->w ); }	}
void SIZE1::MulSizeW( const SIZE1* v ){ if ( v ) { MulSize_( v->w ); }	}
void SIZE1::MulSize(  const SIZE1& v ){            MulSize_( v.w  );	}
void SIZE1::MulSizeW( const SIZE1& v ){            MulSize_( v.w  );	}

void SIZE1::AddSize(  float w ){ AddSize_( w ); }
void SIZE1::AddSizeW( float w ){ AddSize_( w ); }
void SIZE1::AddSize(  const SIZE1* v ){ if ( v ) { AddSize_( v->w ); }	}
void SIZE1::AddSizeW( const SIZE1* v ){ if ( v ) { AddSize_( v->w ); }	}
void SIZE1::AddSize(  const SIZE1& v ){            AddSize_( v.w  );	}
void SIZE1::AddSizeW( const SIZE1& v ){            AddSize_( v.w  );	}

//----------------------------------------
// SIZE2
//----------------------------------------
SIZE2::SIZE2( float w, float h ) : CSIZE2F( w, h ) {}
SIZE2::SIZE2( float s          ) : CSIZE2F( s    ) {}
SIZE2::SIZE2(){}
SIZE2::~SIZE2(){}

void SIZE2::MulSize_( float w, float h ){ SetSize_( this->w * w, this->h * h ); }
void SIZE2::AddSize_( float w, float h ){ SetSize_( this->w + w, this->h + h ); }

void SIZE2::MulSize(  float w, float h ){ MulSize_( w, h ); }
void SIZE2::MulSize(  float s          ){ MulSize_( s, s ); }
void SIZE2::MulSizeW( float w          ){ MulSize_( w, 1 ); }
void SIZE2::MulSizeH( float h          ){ MulSize_( 1, h ); }
void SIZE2::MulSize(  const SIZE2* v ){ if ( v ) { MulSize_( v->w, v->h ); }	}
void SIZE2::MulSizeW( const SIZE2* v ){ if ( v ) { MulSize_( v->w, 1.0f ); }	}
void SIZE2::MulSizeH( const SIZE2* v ){ if ( v ) { MulSize_( 1.0f, v->h ); }	}
void SIZE2::MulSize(  const SIZE2& v ){            MulSize_( v.w,  v.h  );		}
void SIZE2::MulSizeW( const SIZE2& v ){            MulSize_( v.w,  1.f  );		}
void SIZE2::MulSizeH( const SIZE2& v ){            MulSize_( 1.f,  v.h  );		}

void SIZE2::AddSize(  float w, float h ){ AddSize_( w, h ); }
void SIZE2::AddSize(  float s          ){ AddSize_( s, s ); }
void SIZE2::AddSizeW( float w          ){ AddSize_( w, 0 ); }
void SIZE2::AddSizeH( float h          ){ AddSize_( 0, h ); }
void SIZE2::AddSize(  const SIZE2* v ){ if ( v ) { AddSize_( v->w, v->h ); }	}
void SIZE2::AddSizeW( const SIZE2* v ){ if ( v ) { AddSize_( v->w, 0.0f ); }	}
void SIZE2::AddSizeH( const SIZE2* v ){ if ( v ) { AddSize_( 0.0f, v->h ); }	}
void SIZE2::AddSize(  const SIZE2& v ){            AddSize_( v.w,  v.h  );		}
void SIZE2::AddSizeW( const SIZE2& v ){            AddSize_( v.w,  0.f  );		}
void SIZE2::AddSizeH( const SIZE2& v ){            AddSize_( 0.f,  v.h  );		}

//----------------------------------------
// SIZE3
//----------------------------------------
SIZE3::SIZE3( float w, float h, float d ) : CSIZE3F( w, h, d ) {}
SIZE3::SIZE3( float s                   ) : CSIZE3F( s       ) {}
SIZE3::SIZE3(){}
SIZE3::~SIZE3(){}

void SIZE3::MulSize_( float w, float h, float d ){ SetSize_( this->w * w, this->h * h, this->d * d ); }
void SIZE3::AddSize_( float w, float h, float d ){ SetSize_( this->w + w, this->h + h, this->d * d ); }

void SIZE3::MulSize(  float w, float h, float d ){ MulSize_( w, h, d ); }
void SIZE3::MulSize(  float s                   ){ MulSize_( s, s, s ); }
void SIZE3::MulSizeW( float w                   ){ MulSize_( w, 1, 1 ); }
void SIZE3::MulSizeH( float h                   ){ MulSize_( 1, h, 1 ); }
void SIZE3::MulSizeD( float h                   ){ MulSize_( 1, 1, d ); }
void SIZE3::MulSize(  const SIZE3* v ){ if ( v ) { MulSize_( v->w, v->h, v->d ); }	}
void SIZE3::MulSizeW( const SIZE3* v ){ if ( v ) { MulSize_( v->w, 1.0f, 1.0f ); }	}
void SIZE3::MulSizeH( const SIZE3* v ){ if ( v ) { MulSize_( 1.0f, v->h, 1.0f ); }	}
void SIZE3::MulSizeD( const SIZE3* v ){ if ( v ) { MulSize_( 1.0f, 1.0f, v->d ); }	}
void SIZE3::MulSize(  const SIZE3& v ){            MulSize_( v.w,  v.h,  v.d  );	}
void SIZE3::MulSizeW( const SIZE3& v ){            MulSize_( v.w,  1.f,  1.f  );	}
void SIZE3::MulSizeH( const SIZE3& v ){            MulSize_( 1.f,  v.h,  1.f  );	}
void SIZE3::MulSizeD( const SIZE3& v ){            MulSize_( 1.f,  1.f,  v.d  );	}

void SIZE3::AddSize(  float w, float h, float d ){ AddSize_( w, h, d ); }
void SIZE3::AddSize(  float s                   ){ AddSize_( s, s, s ); }
void SIZE3::AddSizeW( float w                   ){ AddSize_( w, 0, 0 ); }
void SIZE3::AddSizeH( float h                   ){ AddSize_( 0, h, 0 ); }
void SIZE3::AddSizeD( float h                   ){ AddSize_( 0, h, d ); }
void SIZE3::AddSize(  const SIZE3* v ){ if ( v ) { AddSize_( v->w, v->h, v->d ); }	}
void SIZE3::AddSizeW( const SIZE3* v ){ if ( v ) { AddSize_( v->w, 0.0f, 0.0f ); }	}
void SIZE3::AddSizeH( const SIZE3* v ){ if ( v ) { AddSize_( 0.0f, v->h, 0.0f ); }	}
void SIZE3::AddSizeD( const SIZE3* v ){ if ( v ) { AddSize_( 0.0f, 0.0f, v->d ); }	}
void SIZE3::AddSize(  const SIZE3& v ){            AddSize_( v.w,  v.h,  v.d  );	}
void SIZE3::AddSizeW( const SIZE3& v ){            AddSize_( v.w,  0.f,  0.f  );	}
void SIZE3::AddSizeH( const SIZE3& v ){            AddSize_( 0.f,  v.h,  0.f  );	}
void SIZE3::AddSizeD( const SIZE3& v ){            AddSize_( 0.f,  0.f,  v.d  );	}

// End Of File
