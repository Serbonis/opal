//========================================
// OPAL Game Library
//========================================
#include "Attr/color.hpp"

//========================================
// カラーリソース
//========================================
using namespace opal;

using VC = VBANKA<COLOR>;

//----------------------------------------
// constructor & destructor
//----------------------------------------
COLORS::COLORS(){}
COLORS::~COLORS(){}

//----------------------------------------
// バンク設定
//----------------------------------------
COLOR* COLORS::Color( UINT n ) const { return VC::Get( n ); }
COLOR* COLORS::Color( void   ) const { return VC::Get(   );	}

void COLORS::NowColor( UINT n ){ VC::Index( n ); 			}
UINT COLORS::NowColor( void   ) const { return VC::Index();	}

void COLORS::SizeColor( UINT s ){ VC::Size( s );			}
UINT COLORS::SizeColor( void   ) const { return VC::Size();	}

//----------------------------------------
// 設定
//----------------------------------------
void  COLORS::SetColor_( UINT n, COLOR v ){ VC::Set( n, v );	}
void  COLORS::SetColor_(         COLOR v ){ VC::Set(    v );	}

COLOR COLORS::GetColor_( UINT n ) const { if ( const auto p = Color( n ) ){ return *p;	} return COLOR(); 	}
COLOR COLORS::GetColor_( void   ) const { if ( const auto p = Color( 0 ) ){ return *p;	} return COLOR(); 	}

void  COLORS::SetColor( UINT n, COLOR v ){ SetColor_( n, v );	}
void  COLORS::SetColor(         COLOR v ){ SetColor_(    v );	}

void  COLORS::SetColor( UINT n, UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ SetColor_( n, COLOR( r, g, b, a           ) );	}
void  COLORS::SetColor( UINT n, UCHAR r, UCHAR g, UCHAR b, void*   ){ SetColor_( n, COLOR( r, g, b, GetColorA() ) );	}
void  COLORS::SetColor(         UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ SetColor_(    COLOR( r, g, b, a           ) );	}
void  COLORS::SetColor(         UCHAR r, UCHAR g, UCHAR b          ){ SetColor_(    COLOR( r, g, b, GetColorA() ) );	}

void  COLORS::SetColor( UINT n, RGB v ){ SetColor_( n, COLOR( v ) );	}
void  COLORS::SetColor( UINT n, HSV v ){ SetColor_( n, COLOR( v ) );	}
void  COLORS::SetColor(         RGB v ){ SetColor_(    COLOR( v ) );	}
void  COLORS::SetColor(         HSV v ){ SetColor_(    COLOR( v ) );	}

void  COLORS::SetColor( void ){ SetColor_( COLOR( 0, 0, 0, -1 ) );	}

COLOR COLORS::GetColor( UINT n ) const { return GetColor_( n ); }
COLOR COLORS::GetColor( void   ) const { return GetColor_(   );	}
COLORS::operator COLOR( void   ) const { return GetColor_(   );	}

//----------------------------------------
// 各色対応
//----------------------------------------
void  COLORS::SetColorR( UINT n, UCHAR r ){ if ( const auto c = Color( n ) ) { c->SetColorR( r ); SetColor_( n, *c );	}	}
void  COLORS::SetColorG( UINT n, UCHAR g ){ if ( const auto c = Color( n ) ) { c->SetColorG( g ); SetColor_( n, *c );	}	}
void  COLORS::SetColorB( UINT n, UCHAR b ){ if ( const auto c = Color( n ) ) { c->SetColorB( b ); SetColor_( n, *c );	}	}
void  COLORS::SetColorA( UINT n, UCHAR a ){ if ( const auto c = Color( n ) ) { c->SetColorA( a ); SetColor_( n, *c );	}	}
void  COLORS::AddColorR( UINT n, short r ){ if ( const auto c = Color( n ) ) { c->AddColorR( r ); SetColor_( n, *c );	}	}
void  COLORS::AddColorG( UINT n, short g ){ if ( const auto c = Color( n ) ) { c->AddColorG( g ); SetColor_( n, *c );	}	}
void  COLORS::AddColorB( UINT n, short b ){ if ( const auto c = Color( n ) ) { c->AddColorB( b ); SetColor_( n, *c );	}	}
void  COLORS::AddColorA( UINT n, short a ){ if ( const auto c = Color( n ) ) { c->AddColorA( a ); SetColor_( n, *c );	}	}
void  COLORS::SetColorR(         UCHAR r ){ if ( const auto c = Color(   ) ) { c->SetColorR( r ); SetColor_(    *c );	}	}
void  COLORS::SetColorG(         UCHAR g ){ if ( const auto c = Color(   ) ) { c->SetColorG( g ); SetColor_(    *c );	}	}
void  COLORS::SetColorB(         UCHAR b ){ if ( const auto c = Color(   ) ) { c->SetColorB( b ); SetColor_(    *c );	}	}
void  COLORS::SetColorA(         UCHAR a ){ if ( const auto c = Color(   ) ) { c->SetColorA( a ); SetColor_(    *c );	}	}
void  COLORS::AddColorR(         short r ){ if ( const auto c = Color(   ) ) { c->AddColorR( r ); SetColor_(    *c );	}	}
void  COLORS::AddColorG(         short g ){ if ( const auto c = Color(   ) ) { c->AddColorG( g ); SetColor_(    *c );	}	}
void  COLORS::AddColorB(         short b ){ if ( const auto c = Color(   ) ) { c->AddColorB( b ); SetColor_(    *c );	}	}
void  COLORS::AddColorA(         short a ){ if ( const auto c = Color(   ) ) { c->AddColorA( a ); SetColor_(    *c );	}	}

UCHAR COLORS::GetColorR( UINT n ) const { return GetColor_( n ).GetColorR();	}
UCHAR COLORS::GetColorG( UINT n ) const { return GetColor_( n ).GetColorG();	}
UCHAR COLORS::GetColorB( UINT n ) const { return GetColor_( n ).GetColorB();	}
UCHAR COLORS::GetColorA( UINT n ) const { return GetColor_( n ).GetColorA();	}
UCHAR COLORS::GetColorR( void   ) const { return GetColor_(   ).GetColorR();	}
UCHAR COLORS::GetColorG( void   ) const { return GetColor_(   ).GetColorG();	}
UCHAR COLORS::GetColorB( void   ) const { return GetColor_(   ).GetColorB();	}
UCHAR COLORS::GetColorA( void   ) const { return GetColor_(   ).GetColorA();	}

// End Of File
