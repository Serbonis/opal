//========================================
// OPAL Game Library
//========================================
#include "super.hpp"

//========================================
// スーパーシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ表示モード
//----------------------------------------
#ifdef OPAL_DEBUG
void SUPERX::Debug( UINT m ){

	printd( "ZOBJ : %d\n", pmap.size() );
	printd( "PMAP : %d\n", Parent() );
	printd( "CMAP : %d\n", cmap.size() );
	printd( "\n" );

	for ( const auto& [k,v] : linx ) {
		printd( "%-7s : ", k.c_str() );
		if ( v ) {
			printd( "%p %03d", v, v->TaskCount() );
		}
		printd( "\n" );
	}
	index->Info();
}
#endif

// End Of File
