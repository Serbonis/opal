//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// コードタスク
//========================================
using namespace opal;

//----------------------------------------
// Etc
//----------------------------------------
void WORKT::WorkInit(  void ){ ZOBJ::TaskInit();	}
void WORKT::WorkEnter( void ){ ZOBJ::TaskEnter();	}
void WORKT::WorkFunc(  void ){ ZOBJ::TaskFunc();	}
void WORKT::WorkBegin( void ){ ZOBJ::TaskBegin();	}
void WORKT::WorkMain(  void ){ ZOBJ::TaskMain();	}
void WORKT::WorkEnd(   void ){ ZOBJ::TaskEnd();		}
void WORKT::WorkExit(  void ){ ZOBJ::TaskExit();	}
void WORKT::WorkFree(  void ){ ZOBJ::TaskFree();	}

//----------------------------------------
// Info
//----------------------------------------
void WORKT::WorkInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "WORKT" );
	printd( "%-08s ", ID() );
	WORKT::WorkInfo( WORKX::Debug() );
	printd( "\n" );
#endif
}

void WORKT::WorkInfo( int m ) const {
#ifdef OPAL_DEBUG
	switch ( m ) {
	default: break;
	case  1: printd( "Prio %02x|%02d ", GetPrio(), GetLevel() );	break;
	case  2: printd( "Level %d|%d %c ", GetLevel( SIGL::PRIMAL ), GetLevel( SIGL::FINAL  ), GetFunc( 0 ) ? '+' : '-' );	break;
	case  3: printd( "Parent %c%c ",  Parent()  ? 'P' : '-', Adopter() ? 'A' : '-' );	break;
	}
#endif
}

// End Of File
