//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトタスク
//========================================
using namespace opal;

//----------------------------------------
// TASK INFO
//----------------------------------------
void OBJT::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	ObjInfo();
#endif
}

//----------------------------------------
// TASK INIT
//----------------------------------------
void OBJT::TaskInit( void ){

	const auto	p  = SIGX::PauseCheck( GetLevel() ) ? true : false;
	const auto	f  = GetFunc( 1 ) ? true : false;
	const auto	pt = ( state & OBJ_STATE_PAUSE ) ? true : false;

	state = OBJ_STATE_NULL;

	if (  p      ) { state |= OBJ_STATE_PAUSE;		}
	if (  p ^ pt ) { state |= OBJ_STATE_PAUSE_T;	}
	if ( !p && f ) { state |= OBJ_STATE_FUNC;		}
}

//----------------------------------------
// TASK ENTER
//----------------------------------------
void OBJT::TaskEnter( void ){}

//----------------------------------------
// TASK FUNC
//----------------------------------------
void OBJT::TaskFunc( void ){

	if ( state & OBJ_STATE_PAUSE ) { return;	}
	if ( state & OBJ_STATE_FUNC  ) { Updater();	}
}

//----------------------------------------
// TASK BEGIN
//----------------------------------------
void OBJT::TaskBegin( void ){

	Coordinate();

	if ( true ) {
		state |= OBJ_STATE_MAIN;
		ObjBegin();
	}
}

//----------------------------------------
// TASK MAIN
//----------------------------------------
void OBJT::TaskMain( void ){

	if ( state & OBJ_STATE_MAIN ) {
		ObjMain();
	}
}

//----------------------------------------
// TASK END
//----------------------------------------
void OBJT::TaskEnd(  void ){

	if ( state & OBJ_STATE_MAIN ) {
		ObjEnd();
	}
}

//----------------------------------------
// TASK EXIT
//----------------------------------------
void OBJT::TaskExit( void ){}

//----------------------------------------
// TASK FREE
//----------------------------------------
void OBJT::TaskFree( void ){

	ObjKill();
}

// End Of File
