//========================================
// OPAL Game Library
//========================================
#include "heap.hpp"

//----------------------------------------
// HEAP管理システム
//----------------------------------------
//----------------------------------------
// new オーバロード
//----------------------------------------
namespace opal {
	void* New( void* p, const char* t, size_t s, size_t u ){
#ifdef OPAL_HEAP
		if ( p ) {
			HEAP::Core()->New( p, t, s, u );
		}
#endif
		return p;
	}
}

//----------------------------------------
// delete オーバロード
//----------------------------------------
namespace opal {
	void Del( const void* p ){
#ifdef OPAL_HEAP
		if ( p ) {
			HEAP::Core()->Del( p );
		}
#endif
	}
}

// End Of File
