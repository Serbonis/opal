//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( SRVCX::smap ){};
DECLVAL( SRVCX::skey ){};
DECLVAL( SRVCX::sval ){};

//----------------------------------------
// システム初期化
//----------------------------------------
bool SRVCX::SysInit( void ){

	smap.clear();
	skey.clear();
	sval.clear();

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool SRVCX::SysExecBefore( void ){

	if ( skey.size() != sval.size() ) {
		const SMAP	sm = smap;

		smap.clear();
		sval.clear();

		for ( const auto& s : skey ) {
			sval.emplace_back( smap[s] = sm.at( s ) );
		}
	}

	for ( const auto& s : sval ) {
		if ( s ) {
			s->Before();
		}
	}

	return true;
}

bool SRVCX::SysExecAfter( void ){

	for ( const auto& s : sval ) {
		if ( s ) {
			s->After();
		}
	}

	return true;
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool SRVCX::SysFree( void ){

	for ( const auto& k : skey ) {
		if ( const auto s = smap.at( k ) ) {
			s->End();
			s->Clear();
		}
	}

	smap.clear();
	skey.clear();
	sval.clear();

	return true;
}

// End Of File
