//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルシステム
//========================================
using namespace opal;

//----------------------------------------
// ファイル情報
//----------------------------------------
#ifdef OPAL_DEBUG
namespace {
	const char*	m[] = {
		"NORMAL",
		"MANAGE",
		"SYSTEM",
		"APPEND",
		"BUFFER",
		"SYNCRW",
		"CALLBK",
	};
}

void FILEI::Debug( void ) const {

	const char*	t = nullptr;
	const char*	e = nullptr;

	switch ( kind ) {
	case TASK_READ :	t = "R";	break;
	case TASK_WRITE:	t = "W";	break;
	default:			t = "?";	break;
	}

	switch ( errr ) {
	default:			e = "-SUCCCESS-";	break;
	case ERROR_TASK:	e = "ERROR_TASK";	break;
	case ERROR_STEP:	e = "ERROR_STEP";	break;
	case ERROR_MODE:	e = "ERROR_MODE";	break;
	case ERROR_INIT:	e = "ERROR_INIT";	break;
	case ERROR_OPEN:	e = "ERROR_OPEN";	break;
	case ERROR_EXEC:	e = "ERROR_EXEC";	break;
	}

	printd( "%s %p %08d %08d %s %s %s\n", t, adrs, size, time, m[mode], e, ( const char* )name );
}

void FILEI::Error( const FILET* t ) const {

	printf( "FILE:%016llx\n", ( UINT64 )t->hand );
	printf( "MODE:%s\n", m[mode] );
	printf( "PATH:%s\n", FILEX::Path() );
	printf( "NAME:%s\n", ( const char* )name );
	printf( "ADRS:%p\n", adrs );
	printf( "SIZE:%016x(%d)\n", ( int )size, ( int )size );
	printf( "ARGV:%016x(%d)\n", ( int )argv, ( int )argv );
	printf( "BUFF:%016llx-%016llx\n",
			( UINT64 )OPAL::MAINMEMORY_ADRS(),
			( UINT64 )OPAL::MAINMEMORY_ADRS()+OPAL::MAINMEMORY_SIZE() );

	switch ( kind ) {
	case TASK_READ :	printf( "R/" );	break;
	case TASK_WRITE:	printf( "W/" );	break;
	default:			printf( "?/" );	break;
	}

	switch ( errr ) {
	default:			printf( "想定外のエラーコード(%08x)\n", errr );					break;
	case ERROR_TASK:	printf( "想定外の処理タスクです(%d)\n", kind );					break;
	case ERROR_STEP:	printf( "想定外の処理ステップです(%d)\n", t->step );			break;
	case ERROR_MODE:	printf( "想定外の処理モードです(%d-%d)\n", t->step, mode );		break;
	case ERROR_INIT:	printf( "タスクが初期化されていません\n" );						break;
	case ERROR_OPEN:	printf( "ファイルオープンに失敗しました:%s", FMES( code ) );	break;
	case ERROR_EXEC:	printf( "読み書きに失敗しました:%s", FMES( code ) );			break;
	}
}
#endif

// End Of File
