//========================================
// OPAL Game Library
//========================================
#if 0
#include "xpad.hpp"

#define DIRECTINPUT_VERSION	0x0800

#include <dinput.h>
#include <xinput.h>

//========================================
// 入力システム
//========================================
using namespace opal;

//========================================
// joypad
//========================================
namespace opal {
	const UINT JOY1 		= 0;
	const UINT JOY2			= 1;
	const UINT JOY3			= 2;
	const UINT JOY4			= 3;

	const UINT PAD_NULL		= 0;

	const UINT PAD_A		= ( 1 <<  0 );		// ×ボタン
	const UINT PAD_B		= ( 1 <<  1 );		// ○ボタン
	const UINT PAD_X		= ( 1 <<  2 );		// □ボタン
	const UINT PAD_Y		= ( 1 <<  3 );		// △ボタン

	const UINT PAD_L1		= ( 1 <<  4 );		// L1トリガ
	const UINT PAD_R1		= ( 1 <<  5 );		// R1トリガ
	const UINT PAD_L2		= ( 1 <<  6 );		// L2トリガ
	const UINT PAD_R2		= ( 1 <<  7 );		// R2トリガ

	const UINT PAD_SL		= ( 1 <<  8 );		// SELECTボタン
	const UINT PAD_ST		= ( 1 <<  9 );		// STARTボタン
	const UINT PAD_L3		= ( 1 << 10 );		// L3トリガ
	const UINT PAD_R3		= ( 1 << 11 );		// R3トリガ

	const UINT PAD_U		= ( 1 << 12 );		// 方向キー上
	const UINT PAD_R		= ( 1 << 13 );		// 方向キー右
	const UINT PAD_D		= ( 1 << 14 );		// 方向キー下
	const UINT PAD_L		= ( 1 << 15 );		// 方向キー左

	const UINT PAD_UR		= PAD_U|PAD_R;
	const UINT PAD_DR		= PAD_D|PAD_R;
	const UINT PAD_UL		= PAD_U|PAD_L;
	const UINT PAD_DL		= PAD_D|PAD_L;

	const UINT PAD_UP		= PAD_U;
	const UINT PAD_DOWN		= PAD_D;
	const UINT PAD_LEFT		= PAD_L;
	const UINT PAD_RIGHT	= PAD_R;
	const UINT PAD_START	= PAD_ST;
	const UINT PAD_SELECT	= PAD_SL;
	const UINT PAD_BU		= PAD_A;
	const UINT PAD_BL		= PAD_B;
	const UINT PAD_BR		= PAD_X;
	const UINT PAD_BD		= PAD_Y;

	const UINT PAD_CROSS	= PAD_U|PAD_D|PAD_L|PAD_R;
	const UINT PAD_BUTTON	= PAD_A|PAD_B|PAD_X|PAD_Y;
	const UINT PAD_TRIG1	= PAD_L1|PAD_R1;
	const UINT PAD_TRIG2	= PAD_L2|PAD_R2;
	const UINT PAD_TRIGGER	= PAD_TRIG1|PAD_TRIG2;

	// Stick
	const UINT STICK_LX		= ( 1 <<  0 );
	const UINT STICK_LY		= ( 1 <<  1 );
	const UINT STICK_RX		= ( 1 <<  2 );
	const UINT STICK_RY		= ( 1 <<  3 );
}

//========================================
// mouse
//========================================
namespace opal {
	const UINT	MOS_NULL	= 0UL;

	const UINT	MOS_LC		= ( 1 <<  0 );
	const UINT	MOS_RC		= ( 1 <<  1 );
	const UINT	MOS_CC		= ( 1 <<  2 );
	const UINT	MOS_XC		= ( 1 <<  3 );
	const UINT	MOS_SC		= ( 1 <<  4 );

	const UINT	MOS_CX		= ( 1 <<  0 );
	const UINT	MOS_CY		= ( 1 <<  1 );
	const UINT	MOS_CZ		= ( 1 <<  2 );
}

//========================================
// keyboard
//========================================
namespace opal {
	const UINT KEY_ESCAPE		= DIK_ESCAPE;		// 0x01
	const UINT KEY_1			= DIK_1;			// 0x02
	const UINT KEY_2			= DIK_2;			// 0x03
	const UINT KEY_3			= DIK_3;			// 0x04
	const UINT KEY_4			= DIK_4;			// 0x05
	const UINT KEY_5			= DIK_5;			// 0x06
	const UINT KEY_6			= DIK_6;			// 0x07
	const UINT KEY_7			= DIK_7;			// 0x08
	const UINT KEY_8			= DIK_8;			// 0x09
	const UINT KEY_9			= DIK_9;			// 0x0A
	const UINT KEY_0			= DIK_0;			// 0x0B
	const UINT KEY_MINUS		= DIK_MINUS;		// 0x0C - on main keyboard
	const UINT KEY_EQUALS		= DIK_EQUALS;		// 0x0D
	const UINT KEY_BACK			= DIK_BACK;			// 0x0E backspace
	const UINT KEY_TAB			= DIK_TAB;			// 0x0F
	const UINT KEY_Q			= DIK_Q;			// 0x10
	const UINT KEY_W			= DIK_W;			// 0x11
	const UINT KEY_E			= DIK_E;			// 0x12
	const UINT KEY_R			= DIK_R;			// 0x13
	const UINT KEY_T			= DIK_T;			// 0x14
	const UINT KEY_Y			= DIK_Y;			// 0x15
	const UINT KEY_U			= DIK_U;			// 0x16
	const UINT KEY_I			= DIK_I;			// 0x17
	const UINT KEY_O			= DIK_O;			// 0x18
	const UINT KEY_P			= DIK_P;			// 0x19
	const UINT KEY_LBRACKET		= DIK_LBRACKET;		// 0x1A
	const UINT KEY_RBRACKET		= DIK_RBRACKET;		// 0x1B
	const UINT KEY_RETURN		= DIK_RETURN;		// 0x1C Enter on main keyboard
	const UINT KEY_LCONTROL		= DIK_LCONTROL;		// 0x1D
	const UINT KEY_A			= DIK_A;			// 0x1E
	const UINT KEY_S			= DIK_S;			// 0x1F
	const UINT KEY_D			= DIK_D;			// 0x20
	const UINT KEY_F			= DIK_F;			// 0x21
	const UINT KEY_G			= DIK_G;			// 0x22
	const UINT KEY_H			= DIK_H;			// 0x23
	const UINT KEY_J			= DIK_J;			// 0x24
	const UINT KEY_K			= DIK_K;			// 0x25
	const UINT KEY_L			= DIK_L;			// 0x26
	const UINT KEY_SEMICOLON	= DIK_SEMICOLON;	// 0x27
	const UINT KEY_APOSTROPHE	= DIK_APOSTROPHE;	// 0x28
	const UINT KEY_GRAVE		= DIK_GRAVE;		// 0x29 accent grave
	const UINT KEY_LSHIFT		= DIK_LSHIFT;		// 0x2A
	const UINT KEY_BACKSLASH	= DIK_BACKSLASH;	// 0x2B
	const UINT KEY_Z			= DIK_Z;			// 0x2C
	const UINT KEY_X			= DIK_X;			// 0x2D
	const UINT KEY_C			= DIK_C;			// 0x2E
	const UINT KEY_V			= DIK_V;			// 0x2F
	const UINT KEY_B			= DIK_B;			// 0x30
	const UINT KEY_N			= DIK_N;			// 0x31
	const UINT KEY_M			= DIK_M;			// 0x32
	const UINT KEY_COMMA		= DIK_COMMA;		// 0x33
	const UINT KEY_PERIOD		= DIK_PERIOD;		// 0x34 . on main keyboard
	const UINT KEY_SLASH		= DIK_SLASH;		// 0x35 / on main keyboard
	const UINT KEY_RSHIFT		= DIK_RSHIFT;		// 0x36
	const UINT KEY_MULTIPLY		= DIK_MULTIPLY;		// 0x37 * on numeric keypad
	const UINT KEY_LMENU		= DIK_LMENU;		// 0x38 left Alt
	const UINT KEY_SPACE		= DIK_SPACE;		// 0x39
	const UINT KEY_CAPITAL		= DIK_CAPITAL;		// 0x3A
	const UINT KEY_F1			= DIK_F1;			// 0x3B
	const UINT KEY_F2			= DIK_F2;			// 0x3C
	const UINT KEY_F3			= DIK_F3;			// 0x3D
	const UINT KEY_F4			= DIK_F4;			// 0x3E
	const UINT KEY_F5			= DIK_F5;			// 0x3F
	const UINT KEY_F6			= DIK_F6;			// 0x40
	const UINT KEY_F7			= DIK_F7;			// 0x41
	const UINT KEY_F8			= DIK_F8;			// 0x42
	const UINT KEY_F9			= DIK_F9;			// 0x43
	const UINT KEY_F10			= DIK_F10;			// 0x44
	const UINT KEY_NUMLOCK		= DIK_NUMLOCK;		// 0x45
	const UINT KEY_SCROLL		= DIK_SCROLL;		// 0x46 Scroll Lock
	const UINT KEY_NUMPAD7		= DIK_NUMPAD7;		// 0x47
	const UINT KEY_NUMPAD8		= DIK_NUMPAD8;		// 0x48
	const UINT KEY_NUMPAD9		= DIK_NUMPAD9;		// 0x49
	const UINT KEY_SUBTRACT		= DIK_SUBTRACT;		// 0x4A - on numeric keypad
	const UINT KEY_NUMPAD4		= DIK_NUMPAD4;		// 0x4B
	const UINT KEY_NUMPAD5		= DIK_NUMPAD5;		// 0x4C
	const UINT KEY_NUMPAD6		= DIK_NUMPAD6;		// 0x4D
	const UINT KEY_ADD			= DIK_ADD;			// 0x4E + on numeric keypad
	const UINT KEY_NUMPAD1		= DIK_NUMPAD1;		// 0x4F
	const UINT KEY_NUMPAD2		= DIK_NUMPAD2;		// 0x50
	const UINT KEY_NUMPAD3		= DIK_NUMPAD3;		// 0x51
	const UINT KEY_NUMPAD0		= DIK_NUMPAD0;		// 0x52
	const UINT KEY_DECIMAL		= DIK_DECIMAL;		// 0x53 . on numeric keypad
	const UINT KEY_OEM_102		= DIK_OEM_102;		// 0x56 <> or \| on RT 102-key keyboard (Non-U.S.)
	const UINT KEY_F11			= DIK_F11;			// 0x57
	const UINT KEY_F12			= DIK_F12;			// 0x58
	const UINT KEY_F13			= DIK_F13;			// 0x64 (NEC PC98)
	const UINT KEY_F14			= DIK_F14;			// 0x65 (NEC PC98)
	const UINT KEY_F15			= DIK_F15;			// 0x66 (NEC PC98)
	const UINT KEY_KANA			= DIK_KANA;			// 0x70 (Japanese keyboard)
	const UINT KEY_ABNT_C1		= DIK_ABNT_C1;		// 0x73 /? on Brazilian keyboard
	const UINT KEY_CONVERT		= DIK_CONVERT;		// 0x79 (Japanese keyboard)
	const UINT KEY_NOCONVERT	= DIK_NOCONVERT;	// 0x7B (Japanese keyboard)
	const UINT KEY_YEN			= DIK_YEN;			// 0x7D (Japanese keyboard)
	const UINT KEY_ABNT_C2		= DIK_ABNT_C2;		// 0x7E Numpad . on Brazilian keyboard
	const UINT KEY_NUMPADEQUALS	= DIK_NUMPADEQUALS;	// 0x8D		= on numeric keypad (NEC PC98)
	const UINT KEY_CIRCUMFLEX	= DIK_CIRCUMFLEX;	// 0x90 Previous Track (DIK_PREVTRACK on English keyboard)
	const UINT KEY_AT			= DIK_AT;			// 0x91 (NEC PC98)
	const UINT KEY_COLON		= DIK_COLON;		// 0x92 (NEC PC98)
	const UINT KEY_UNDERLINE	= DIK_UNDERLINE;	// 0x93 (NEC PC98)
	const UINT KEY_KANJI		= DIK_KANJI;		// 0x94 (Japanese keyboard)
	const UINT KEY_STOP			= DIK_STOP;			// 0x95 (NEC PC98)
	const UINT KEY_AX			= DIK_AX;			// 0x96 (Japan AX)
	const UINT KEY_UNLABELED	= DIK_UNLABELED;	// 0x97 (J3100)
	const UINT KEY_NEXTTRACK	= DIK_NEXTTRACK;	// 0x99 Next Track
	const UINT KEY_NUMPADENTER	= DIK_NUMPADENTER;	// 0x9C Enter on numeric keypad
	const UINT KEY_RCONTROL		= DIK_RCONTROL;		// 0x9D
	const UINT KEY_MUTE			= DIK_MUTE;			// 0xA0 Mute
	const UINT KEY_CALCULATOR	= DIK_CALCULATOR;	// 0xA1 Calculator
	const UINT KEY_PLAYPAUSE	= DIK_PLAYPAUSE;	// 0xA2 Play / Pause
	const UINT KEY_MEDIASTOP	= DIK_MEDIASTOP;	// 0xA4 Media Stop
	const UINT KEY_VOLUMEDOWN	= DIK_VOLUMEDOWN;	// 0xAE Volume -
	const UINT KEY_VOLUMEUP		= DIK_VOLUMEUP;		// 0xB0 Volume +
	const UINT KEY_WEBHOME		= DIK_WEBHOME;		// 0xB2 Web home
	const UINT KEY_NUMPADCOMMA	= DIK_NUMPADCOMMA;	// 0xB3; on numeric keypad (NEC PC98)
	const UINT KEY_DIVIDE		= DIK_DIVIDE;		// 0xB5 / on numeric keypad
	const UINT KEY_SYSRQ		= DIK_SYSRQ;		// 0xB7 PrintScreen|SysRq
	const UINT KEY_RMENU		= DIK_RMENU;		// 0xB8 right Alt
	const UINT KEY_PAUSE		= DIK_PAUSE;		// 0xC5 Pause|Break
	const UINT KEY_HOME			= DIK_HOME;			// 0xC7 Home on arrow keypad
	const UINT KEY_UP			= DIK_UP;			// 0xC8 UpArrow on arrow keypad
	const UINT KEY_PRIOR		= DIK_PRIOR;		// 0xC9 PgUp on arrow keypad
	const UINT KEY_LEFT			= DIK_LEFT;			// 0xCB LeftArrow on arrow keypad
	const UINT KEY_RIGHT		= DIK_RIGHT;		// 0xCD RightArrow on arrow keypad
	const UINT KEY_END			= DIK_END;			// 0xCF End on arrow keypad
	const UINT KEY_DOWN			= DIK_DOWN;			// 0xD0 DownArrow on arrow keypad
	const UINT KEY_NEXT			= DIK_NEXT;			// 0xD1 PgDn on arrow keypad
	const UINT KEY_INSERT		= DIK_INSERT;		// 0xD2 Insert on arrow keypad
	const UINT KEY_DELETE		= DIK_DELETE;		// 0xD3 Delete on arrow keypad
	const UINT KEY_LWIN			= DIK_LWIN;			// 0xDB Left Windows key
	const UINT KEY_RWIN			= DIK_RWIN;			// 0xDC Right Windows key
	const UINT KEY_APPS			= DIK_APPS;			// 0xDD AppMenu key
	const UINT KEY_POWER		= DIK_POWER;		// 0xDE System Power
	const UINT KEY_SLEEP		= DIK_SLEEP;		// 0xDF System Sleep
	const UINT KEY_WAKE			= DIK_WAKE;			// 0xE3 System Wake
	const UINT KEY_WEBSEARCH	= DIK_WEBSEARCH;	// 0xE5 Web Search
	const UINT KEY_WEBFAVORITES	= DIK_WEBFAVORITES;	// 0xE6 Web Favorites
	const UINT KEY_WEBREFRESH	= DIK_WEBREFRESH;	// 0xE7 Web Refresh
	const UINT KEY_WEBSTOP		= DIK_WEBSTOP;		// 0xE8 Web Stop
	const UINT KEY_WEBFORWARD	= DIK_WEBFORWARD;	// 0xE9 Web Forward
	const UINT KEY_WEBBACK		= DIK_WEBBACK;		// 0xEA Web Back
	const UINT KEY_MYCOMPUTER	= DIK_MYCOMPUTER;	// 0xEB My Computer
	const UINT KEY_MAIL			= DIK_MAIL;			// 0xEC Mail
	const UINT KEY_MEDIASELECT	= DIK_MEDIASELECT;	// 0xED Media Select
	const UINT KEY_FUNCTION		= 0xF0;
	const UINT KEY_ALPHABET		= 0xF1;
	const UINT KEY_NUMBER		= 0xF2;
	const UINT KEY_SHIFT		= 0xF3;
	const UINT KEY_CONTROL		= 0xF4;
	const UINT KEY_ALT			= 0xF5;
	const UINT KEY_ARROW		= 0xF6;
	const UINT KEY_NUMPAD		= 0xF7;
	const UINT KEY_SUPER		= 0xF8;
	const UINT KEY_ANY			= 0xff;
}
#endif
// End Of File
