//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
// ネットワークシステム
//========================================
using namespace opal;

//----------------------------------------
// タスクの登録
//----------------------------------------
NETWI* NETWX::Server( const char* p, USHORT q, int s ){

	if ( p == nullptr ) {
		return nullptr;
	}

	const auto	md = 0;
	const auto	id = p;
	const auto	ad = nullptr;
	const auto	sz = q;
	const auto	av = s;

	if ( info_s.Data( id ) ) {
		return nullptr;
	}

	if ( const auto fi = info_s.Information( id, ad, sz, av ) ) {
		fi->Task( SERVER, md );
		if ( const auto fm = info_s.Message( id, fi, NETWT::STEP_INIT ) ) {
			fm->init( q, s );
		}
		return fi;
	}

	return nullptr;
}

NETWI* NETWX::Client( const char* p, USHORT q ){

	if ( p == nullptr ) {
		return nullptr;
	}

	const auto	md = 0;
	const auto	id = p;
	const auto	ad = nullptr;
	const auto	sz = q;
	const auto	av = 0;

	if ( info_c.Data( id ) ) {
		return nullptr;
	}

	if ( const auto fi = info_c.Information( id, ad, sz, av ) ) {
		fi->Task( CLIENT, md );
		if ( const auto fm = info_c.Message( id, fi, NETWT::STEP_INIT ) ) {
			fm->init( q );
		}
		return fi;
	}

	return nullptr;
}

//----------------------------------------
// タスクの数
//----------------------------------------
UINT NETWX::Size(  UINT n ){

	switch ( n ) {
	case SERVER: return info_s.Size();
	case CLIENT: return info_c.Size();
	}
	return 0;
}
UINT NETWX::SizeS( void ){ return Size( SERVER ); }
UINT NETWX::SizeC( void ){ return Size( CLIENT ); }

//----------------------------------------
// タスクの取得
//----------------------------------------
NETWT* NETWX::Task( UINT n, const char* p ){

	switch ( n ) {
	case SERVER: return info_s.Task( p );
	case CLIENT: return info_c.Task( p );
	}
	return nullptr;
}
NETWT_S* NETWX::TaskS( const char* p ){ return dynamic_cast<NETWT_S*>( Task( SERVER, p ) ); }
NETWT_C* NETWX::TaskC( const char* p ){ return dynamic_cast<NETWT_C*>( Task( CLIENT, p ) ); }

//----------------------------------------
// タスクの削除
//----------------------------------------
int NETWX::Kill( UINT n, const char* p ){

	if ( const auto t = Task( n, p ) ) {
		switch ( n ) {
		case SERVER: info_s().Release( t );	break;
		case CLIENT: info_c().Release( t );	break;
		}
		t->kill();
		return 0;
	}
	return -1;
}
int NETWX::KillS( const char* p ) { return Kill( SERVER, p ); }
int NETWX::KillC( const char* p ) { return Kill( CLIENT, p ); }

//----------------------------------------
// タスクの待機
//----------------------------------------
int NETWX::Sync( UINT n, const char* p ){

	if ( !p ) {
		return Size( n );
	}

	if ( const auto t = Task( n, p ) ) {
		return t->flag;
	}

	return 0;
}
int NETWX::SyncS( const char* p ) { return Sync( SERVER, p ); }
int NETWX::SyncC( const char* p ) { return Sync( CLIENT, p ); }

// End Of File

