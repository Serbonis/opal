//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADS::PADS( const PADS* p ) :
	work(  ( p ) ? p->work  : nullptr ),
	count( ( p ) ? p->count : nullptr )
{}

//----------------------------------------
// データ Set/Get
//----------------------------------------
PADW* PADS::Work(  void ) const { return work; }
PADC* PADS::Count( void ) const { return count; }
void  PADS::Work(  PADW* p ){ work  = p; }
void  PADS::Count( PADC* p ){ count = p; }

//----------------------------------------
// 判定関数:PADC
//----------------------------------------
int PADS::KeyCount(   UINT k         ) const { return ( count ) ? count->KeyCount(   k    ) : 0; }
int PADS::MouseCount( UINT c         ) const { return ( count ) ? count->MouseCount( c    ) : 0; }
int PADS::JoyCount(   UINT p, UINT b ) const { return ( count ) ? count->JoyCount(   p, b ) : 0; }
int PADS::Joy1Count(          UINT b ) const { return ( count ) ? count->Joy1Count(     b ) : 0; }
int PADS::Joy2Count(          UINT b ) const { return ( count ) ? count->Joy2Count(     b ) : 0; }

int PADS::JKCount(  UINT p, UINT b, UINT k ) const { return ( count ) ? count->JKCount(  p, b, k ) : 0; }
int PADS::J1KCount(         UINT b, UINT k ) const { return ( count ) ? count->J1KCount(    b, k ) : 0; }
int PADS::J2KCount(         UINT b, UINT k ) const { return ( count ) ? count->J2KCount(    b, k ) : 0; }

int PADS::JKCount(  UINT  p, const PADA* a ) const { return ( count ) ? count->JKCount( p, a ) : 0; }
int PADS::J1KCount(          const PADA* a ) const { return ( count ) ? count->J1KCount(   a ) : 0; }
int PADS::J2KCount(          const PADA* a ) const { return ( count ) ? count->J2KCount(   a ) : 0; }

//----------------------------------------
// 判定関数:PADW
//----------------------------------------
int PADS::KeyPush( UINT k, UINT ts, UINT te ) const { return ( work ) ? work->KeyPush( k, ts, te ) : 0; }
int PADS::KeyTrig( UINT k, UINT ts, UINT te ) const { return ( work ) ? work->KeyTrig( k, ts, te ) : 0; }
int PADS::KeyRels( UINT k, UINT ts, UINT te ) const { return ( work ) ? work->KeyRels( k, ts, te ) : 0; }

int PADS::JoyPushO( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyPushO( p, b, ts, te ) : 0; }
int PADS::JoyTrigO( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyTrigO( p, b, ts, te ) : 0; }
int PADS::JoyRelsO( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyRelsO( p, b, ts, te ) : 0; }
int PADS::JoyPushA( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyPushA( p, b, ts, te ) : 0; }
int PADS::JoyTrigA( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyTrigA( p, b, ts, te ) : 0; }
int PADS::JoyRelsA( UINT p, UINT b, UINT ts, UINT te ) const { return ( work ) ? work->JoyRelsA( p, b, ts, te ) : 0; }

float PADS::JoyStick(  UINT p, UINT s, UINT t ) const { return ( work ) ? work->JoyStick( p, s, t ) : 0; }

int PADS::MousePush( UINT c, UINT ts, UINT te ) const { return ( work ) ? work->MousePush( c, ts, te ) : 0; }
int PADS::MouseTrig( UINT c, UINT ts, UINT te ) const { return ( work ) ? work->MouseTrig( c, ts, te ) : 0; }
int PADS::MouseRels( UINT c, UINT ts, UINT te ) const { return ( work ) ? work->MouseRels( c, ts, te ) : 0; }

float PADS::Mouse(  UINT a, UINT t ) const { return ( work ) ? work->Mouse( a, t ) : 0; }
float PADS::MouseX( UINT t ) const { return ( work ) ? work->MouseX( t ) : 0; }
float PADS::MouseY( UINT t ) const { return ( work ) ? work->MouseY( t ) : 0; }
float PADS::Scroll( UINT t ) const { return ( work ) ? work->Scroll( t ) : 0; }

float PADS::Cursor(  UINT a, UINT t ) const { return ( work ) ? work->Cursor( a, t ) : 0; }
float PADS::CursorX( UINT t ) const { return ( work ) ? work->CursorX( t ) : 0; }
float PADS::CursorY( UINT t ) const { return ( work ) ? work->CursorY( t ) : 0; }

//----------------------------------------
// 複合型 & (PAD1|PAD2)
//----------------------------------------
float PADS::Joy1Stick( UINT s, UINT t ) const { return ( work ) ? work->Joy1Stick( s, t ) : 0; }
float PADS::Joy2Stick( UINT s, UINT t ) const { return ( work ) ? work->Joy2Stick( s, t ) : 0; }

int PADS::Joy1PushO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1PushO( b, s, e ) : 0; }
int PADS::Joy2PushO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2PushO( b, s, e ) : 0; }
int PADS::Joy1TrigO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1TrigO( b, s, e ) : 0; }
int PADS::Joy2TrigO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2TrigO( b, s, e ) : 0; }
int PADS::Joy1RelsO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1RelsO( b, s, e ) : 0; }
int PADS::Joy2RelsO( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2RelsO( b, s, e ) : 0; }
int PADS::Joy1PushA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1PushA( b, s, e ) : 0; }
int PADS::Joy2PushA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2PushA( b, s, e ) : 0; }
int PADS::Joy1TrigA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1TrigA( b, s, e ) : 0; }
int PADS::Joy2TrigA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2TrigA( b, s, e ) : 0; }
int PADS::Joy1RelsA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy1RelsA( b, s, e ) : 0; }
int PADS::Joy2RelsA( UINT b, UINT s, UINT e ) const { return ( work ) ? work->Joy2RelsA( b, s, e ) : 0; }

int PADS::JKPushO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKPushO( p, b, k, s, e ) : 0; }
int PADS::JKTrigO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKTrigO( p, b, k, s, e ) : 0; }
int PADS::JKRelsO( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKRelsO( p, b, k, s, e ) : 0; }
int PADS::JKPushA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKPushA( p, b, k, s, e ) : 0; }
int PADS::JKTrigA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKTrigA( p, b, k, s, e ) : 0; }
int PADS::JKRelsA( UINT p, UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->JKRelsA( p, b, k, s, e ) : 0; }

int PADS::J1KPushO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KPushO( b, k, s, e ) : 0; }
int PADS::J2KPushO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KPushO( b, k, s, e ) : 0; }
int PADS::J1KTrigO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KTrigO( b, k, s, e ) : 0; }
int PADS::J2KTrigO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KTrigO( b, k, s, e ) : 0; }
int PADS::J1KRelsO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KRelsO( b, k, s, e ) : 0; }
int PADS::J2KRelsO( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KRelsO( b, k, s, e ) : 0; }
int PADS::J1KPushA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KPushA( b, k, s, e ) : 0; }
int PADS::J2KPushA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KPushA( b, k, s, e ) : 0; }
int PADS::J1KTrigA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KTrigA( b, k, s, e ) : 0; }
int PADS::J2KTrigA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KTrigA( b, k, s, e ) : 0; }
int PADS::J1KRelsA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J1KRelsA( b, k, s, e ) : 0; }
int PADS::J2KRelsA( UINT b, UINT k, UINT s, UINT e ) const { return ( work ) ? work->J2KRelsA( b, k, s, e ) : 0; }

int PADS::JKPushO( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKPushO( p, a, s, e ) : 0; }
int PADS::JKTrigO( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKTrigO( p, a, s, e ) : 0; }
int PADS::JKRelsO( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKRelsO( p, a, s, e ) : 0; }
int PADS::JKPushA( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKPushA( p, a, s, e ) : 0; }
int PADS::JKTrigA( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKTrigA( p, a, s, e ) : 0; }
int PADS::JKRelsA( UINT p, const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->JKRelsA( p, a, s, e ) : 0; }

int PADS::J1KPushO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KPushO( a, s, e ) : 0; }
int PADS::J2KPushO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J2KPushO( a, s, e ) : 0; }
int PADS::J1KTrigO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KTrigO( a, s, e ) : 0; }
int PADS::J2KTrigO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J2KTrigO( a, s, e ) : 0; }
int PADS::J1KRelsO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KRelsO( a, s, e ) : 0; }
int PADS::J2KRelsO( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J2KRelsO( a, s, e ) : 0; }
int PADS::J1KPushA( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KPushA( a, s, e ) : 0; }
int PADS::J2KPushA( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J2KPushA( a, s, e ) : 0; }
int PADS::J1KTrigA( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KTrigA( a, s, e ) : 0; }
int PADS::J2KTrigA( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J2KTrigA( a, s, e ) : 0; }
int PADS::J1KRelsA( const PADA* a, UINT s, UINT e ) const { return ( work ) ? work->J1KRelsA( a, s, e ) : 0; }

// End Of File
