//========================================
// OPAL Game Library
//========================================
#ifdef OPAL_DEBUG
#include "debug.hpp"

//========================================
// デバッグシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ変数
//----------------------------------------
namespace {
	HWND	handle = nullptr;
}

//----------------------------------------
// デバッグ関数
//----------------------------------------
namespace opal::DEBUGX_ {
	HWND DebugHandle( void ){ return handle;	}
}

//----------------------------------------
// デバッグウインドウ
//----------------------------------------
void DEBUGX::Window( void ){

	if ( handle == nullptr ) {
		int	w = 1600;
		int	h = 1200;

		switch ( OPAL::DEBUG_MODE() ) {
		case  1: w =  800; h =  600;	break;
		case  2: w =  960; h =  720;	break;
		case  4: w = 1024; h =  768;	break;
		case  8: w = 1200; h =  900;	break;
		case 16: w = 1600; h = 1200;	break;
		}
		WINDOWX::Open( handle, w, h, "DEBUG MONITOR" );
	}
}

//----------------------------------------
// 描画領域
//----------------------------------------
namespace {
	std::pair<UINT,UINT>	window;
}

namespace opal::DEBUGX_ {
	void DebugWindow( UINT w, UINT h ){

		switch ( OPAL::DEBUG_MODE() ) {
		case  1: w = 1600; h = 1200;	break;
		case  2: w = 1920; h = 1440;	break;
		case  4: w = 2048; h = 1536;	break;
		case  8: w = 2400; h = 1800;	break;
		case 16: w = 3200; h = 2400;	break;
		}

		window = {w,h};
	}

	auto DebugWindow( void )->std::pair<UINT,UINT>{ return window;	}
}

#endif

// End Of File
