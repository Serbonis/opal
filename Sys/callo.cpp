//========================================
// OPAL Game Library
//========================================
#include "call.hpp"
#include "rsrcx.hpp"

//========================================
// サウンドタスク
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( CALLT::call_list ){};
DECLVAL( CALLT::call_task ){};
DECLVAL( CALLT::call_file ){};

//----------------------------------------
// TASK FUNCTIONAL
//----------------------------------------
void CALLT::callSysInit( void ){

	call_list.clear();
	call_task.clear();
	call_file.clear();
}

void CALLT::callSysFree( void ){

	call_list.clear();

	for ( auto& [p,s] : call_task ) {
		p->Close();
	}
	call_task.clear();

	for ( auto& [s,f] : call_file ) {
		f->Free();
	}
	call_file.clear();
}

//----------------------------------------
//
//----------------------------------------
void CALLT::callInit( void ){
}

void CALLT::callFree( void ){

	std::vector<CALLT*>	call_kill;

	for ( auto& [p,s] : call_task ) {
		if ( !p->GetPlay() ) {
			p->Close();
			call_kill.emplace_back( p );
		}
	}

	for ( auto& p : call_kill ) {
		call_task.erase( p );
	}
}

void CALLT::callExec( void ){

	for ( auto& [p,s,r,f,n] : call_list ) {
		p->Open();

		if ( r ) {
			if ( !call_file.count( s ) ) {
				call_file[s] = r( p.get(), s.c_str(), nullptr );
			} else {
				r( p.get(), nullptr, call_file[s] );
			}
		}

		if ( f ) {
			f( p.get() );
		}

		if ( n ) {
			p->SetPlay();
		}
		call_task[p.get()] = p;
	}

	call_list.clear();
}

//----------------------------------------
//
//----------------------------------------
void CALLT::Call( DTP p, const char* s, DTI r, DTF f ){

	call_list.emplace_back( CALL_LIST{p,s ? s : "",r,f,1} );
}

// End Of File
