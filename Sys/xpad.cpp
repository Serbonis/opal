//========================================
// OPAL Game Library
//========================================
#include "xpad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

#define _PADX_	PADX_DX

namespace opal::PADX_DX {
	extern void SysInit( void );
	extern void SysExec( void );
	extern void SysFree( void );

	extern void SysInitKeyboard( void );
	extern bool SysExecKeyboard( KEYBOARD& );
	extern void SysFreeKeyboard( void );

	extern void SysInitMouse( void );
	extern bool SysExecMouse( MOUSE& );
	extern void SysFreeMouse( void );

	extern void SysInitJoypad( void );
	extern bool SysExecJoypad( JOYPAD&, UINT );
	extern void SysFreeJoypad( void );
	extern UINT SysJoypadMax(  void );
}

namespace opal::PADX_ {
	void SysInit( void ){ _PADX_::SysInit();	}
	void SysExec( void ){ _PADX_::SysExec();	}
	void SysFree( void ){ _PADX_::SysFree();	}

	void SysInitKeyboard( void        ){ 		_PADX_::SysInitKeyboard();		}
	bool SysExecKeyboard( KEYBOARD& p ){ return	_PADX_::SysExecKeyboard( p );	}
	void SysFreeKeyboard( void        ){ 		_PADX_::SysFreeKeyboard();		}

	void SysInitMouse( void     ){ 			_PADX_::SysInitMouse();		}
	bool SysExecMouse( MOUSE& p ){ return	_PADX_::SysExecMouse( p );	}
	void SysFreeMouse( void     ){ 			_PADX_::SysFreeMouse();		}

	void SysInitJoypad( void              ){ 		_PADX_::SysInitJoypad();		}
	bool SysExecJoypad( JOYPAD& p, UINT n ){ return	_PADX_::SysExecJoypad( p,n );	}
	void SysFreeJoypad( void              ){ 		_PADX_::SysFreeJoypad();		}
	UINT SysJoypadMax(  void              ){ return	_PADX_::SysJoypadMax();			}
}

// End Of File
