//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// コードタスク
//========================================
using namespace opal;

//----------------------------------------
// TASK INFO
//----------------------------------------
void WORKT::TaskInfo( void ) const {
#ifdef OPAL_DEBUG
	WorkInfo();
#endif
}

//----------------------------------------
// TASK INIT
//----------------------------------------
void WORKT::TaskInit( void ){

	WorkInit();

	const auto	p = SIGX::PauseCheck( GetLevel() ) ? true : false;
	const auto	f = GetFunc( 1 )  ? true : false;
	const auto	pt= ( WORKS::state & WORK_STATE_PAUSE ) ? true : false;

	WORKS::state = WORK_STATE_NULL;

	if (  p      ) { WORKS::state |= WORK_STATE_PAUSE;		}
	if (  p ^ pt ) { WORKS::state |= WORK_STATE_PAUSE_T;	}
	if ( !p && f ) { WORKS::state |= WORK_STATE_FUNC;		}

	if ( p ) {
		AddFlag( FLAG_P );
	} else {
		SubFlag( FLAG_P );
	}
}

//----------------------------------------
// TASK ENTER
//----------------------------------------
void WORKT::TaskEnter( void ){

	WorkEnter();
}

//----------------------------------------
// TASK FUNC
//----------------------------------------
void WORKT::TaskFunc( void ){

	if ( WORKS::state & WORK_STATE_PAUSE ) { return;	}
	if ( WORKS::state & WORK_STATE_FUNC  ) {
		Updater();
		OBJ::state &= ~OBJ_STATE_FUNC;
		WorkFunc();
	}
}

//----------------------------------------
// TASK BEGIN
//----------------------------------------
void WORKT::TaskBegin( void ){

	OBJT::Coordinate();
	WORKS::Coordinate( LinxTask( this ) );

	if ( true ) {
		WORKS::state |= WORK_STATE_MAIN;
		WorkBegin();
	}
}

//----------------------------------------
// TASK MAIN
//----------------------------------------
void WORKT::TaskMain( void ){

	if ( WORKS::state & WORK_STATE_MAIN ) {
		WorkMain();
	}
}

//----------------------------------------
// TASK END
//----------------------------------------
void WORKT::TaskEnd(  void ){

	if ( WORKS::state & WORK_STATE_MAIN ) {
		WorkEnd();
	}
}

//----------------------------------------
// TASK EXIT
//----------------------------------------
void WORKT::TaskExit( void ){

	WorkExit();
}

//----------------------------------------
// TASK FREE
//----------------------------------------
void WORKT::TaskFree( void ){

	WorkFree();
}

// End Of File
