//========================================
// OPAL Game Library
//========================================
#include "debug.hpp"
#include "draw.hpp"

//========================================
// グラフィックシステム
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
DECLVAL( DRAWX::index  ){};
DECLVAL( DRAWX::sysdef ){};

#ifdef OPAL_DEBUG
DECLVAL( DRAWX::debug  ){};
#endif

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWX::SYSDEF::SYSDEF() :
	linx( nullptr ),	// オブジェの登録先
	disp(  true   ),	// オブジェ表示状態
	anime( false  ),	// オブジェアニメ状態
	alpha( false  ),	// オブジェ半透明状態
	prio(  0UL    )		// オブジェ表示優先
{}

//----------------------------------------
// システム初期化
//----------------------------------------
bool DRAWX::SysInit( void ){

	//----------------------------------------
	// タスクシステム初期化
	//----------------------------------------
	if ( ( index = NEWS( (*SUPERX::Linx())["DRAWX"] ) ) ) {
		index->Init( "CLASS-DRAW" );
	}

	//----------------------------------------
	// デフォルト設定
	//----------------------------------------
	new ( &sysdef ) SYSDEF;

	//----------------------------------------
	// デバイス初期化
	//----------------------------------------
	DRAWX_::SysInit();
	DRAWX_::SysInitAdapter();
	DRAWX_::SysInitDevice( WINDOWX::Handle(), WINDOWX::DisplayW(), WINDOWX::DisplayH() );
	DRAWX_::SysInitMonitor();
	DRAWX_::SysInitViewport();
	DRAWX_::SysInitClearbuffer();

	//----------------------------------------
	// システムインスタンス
	//----------------------------------------
	InitCamera();
	InitLight();

	SysCamera( true );
	SysLight( false );

	return true;
}

//----------------------------------------
// システム実行
//----------------------------------------
bool DRAWX::SysTaskInit( void ){

	SUPERX::TaskInit( index );

	return true;
}

bool DRAWX::SysTaskEnter( void ){

	DRAWX_::SysBegin();
	SUPERX::TaskEnter( index );

	return true;
}

bool DRAWX::SysTaskMain( void ){

	ExecLight();
	ExecCamera();

	SUPERX::TaskMain( index );

	return true;
}

bool DRAWX::SysTaskExit( void ){

	SUPERX::TaskExit( index );
	DRAWX_::SysEnd();

	return true;
}

bool DRAWX::SysTaskFree( void ){

	SUPERX::TaskFree( index );

	return true;
}

//----------------------------------------
// 表示切替
//----------------------------------------
void DRAWX::Present( void ){

	DRAWX_::Present();
}

//----------------------------------------
// システム後始末
//----------------------------------------
bool DRAWX::SysFree( void ){

	//----------------------------------------
	// システムインスタンス
	//----------------------------------------
	FreeCamera();
	FreeLight();

	//----------------------------------------
	// デバイス後始末
	//----------------------------------------
	DRAWX_::SysFreeClearbuffer();
	DRAWX_::SysFreeViewport();
	DRAWX_::SysFreeMonitor();
	DRAWX_::SysFreeDevice();
	DRAWX_::SysFreeAdapter();
	DRAWX_::SysFree();

	//----------------------------------------
	// デフォルト設定
	//----------------------------------------
	new ( &sysdef ) SYSDEF;

	//----------------------------------------
	// タスクシステム後始末
	//----------------------------------------
	if ( index ) {
		index->Free();
		DELETES( index );
		SUPERX::Linx()->erase( "DRAWX" );
	}

	return true;
}

// End Of File
