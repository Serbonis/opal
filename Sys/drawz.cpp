//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックタスク
//========================================
using namespace opal;

//----------------------------------------
// 計算＆逆計算
//----------------------------------------
void DRAWT::Coordinate( const SUPERT* o ){

	ZOBJ::Coordinate( o );
	XOBJ::Coordinate( o );
	VOBJ::Coordinate( o );
	SOBJ::Coordinate( o );
	SOBJ::CalcurateN();
	SOBJ::Dirty( false );
	POBJ::Coordinate( o );
	OOBJ::Coordinate( o );
}

void DRAWT::ReCoordinate( const SUPERT* o ){

	OOBJ::ReCoordinate( o );
	SOBJ::ReCoordinate( o );
}

// End Of File
