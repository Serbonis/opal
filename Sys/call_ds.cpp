//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_DS;

//----------------------------------------
// デバイス
//----------------------------------------
DEVICE::DEVICE() :
	device( nullptr )
{}
DEVICE::~DEVICE(){}

IDirectSound8* DEVICE::operator()( void ){ return device;	}
DEVICE::operator IDirectSound8*(   void ){ return device;	}

void DEVICE::Init( void ){

	::DirectSoundCreate8( nullptr, &device, nullptr );
	device->SetCooperativeLevel( WINDOWX::Handle(), DSSCL_PRIORITY );
}

void DEVICE::Free( void ){

	RELEASE( device );
}

// End Of File
