//========================================
// OPAL Game Library
//========================================
#include "super.hpp"

//========================================
// スーパータスク
//========================================
using namespace opal;

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void SUPERT::Init( const char* p ){

	OBJT::Init( p );
}

void SUPERT::Free( void ){

	OBJT::Free();
}

//----------------------------------------
// 登録＆解放
//----------------------------------------
void SUPERT::Register( const char* p ){ SUPERT::Register( p ? SUPERX::Linx( p ) : SUPERX::Linx( "SUPERX" ) );	}
void SUPERT::Release(  const char* p ){ SUPERT::Release(  p ? SUPERX::Linx( p ) : SUPERX::Linx( "SUPERX" ) ); }
void SUPERT::Register( LINX* l ){ if ( const auto p = l ? l : SUPERX::Linx( "SUPERX" ) ) { p->Register( this );	}	}
void SUPERT::Release(  LINX* l ){

	if ( const auto p = l ? l : SUPERX::Linx( "SUPERX" ) ) {
		p->Release( this );
	}
}

//----------------------------------------
// オブジェクト管理情報
//----------------------------------------
SUPERX::LMAP* SUPERX::Linx( void  ){ return &linx;	}
LINX* SUPERX::Linx( const char* s ){ return index;	}

// End Of File
