//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

//========================================
// �T�E���h�^�X�N
//========================================
using namespace opal;

//----------------------------------------
// �v�Z���t�v�Z
//----------------------------------------
void CALLT::Coordinate( const SUPERT* o ){

	ZOBJ::Coordinate( o );
	XOBJ::Coordinate( o );
	VOBJ::Coordinate( o );
	SOBJ::Coordinate( o );
	SOBJ::CalcurateN();
	SOBJ::Dirty( false );
	OOBJ::Coordinate( o );
}

void CALLT::ReCoordinate( const SUPERT* o ){

	OOBJ::ReCoordinate( o );
	SOBJ::ReCoordinate( o );
}

// End Of File
