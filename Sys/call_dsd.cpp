//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"

//========================================
// サウンドシステム
//========================================
using namespace opal;
using namespace opal::CALLX_DS;

//----------------------------------------
// スペック表示
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal::CALLX_DS {
	void DEVICE::Debug( void ) const {}
}

#endif

// End Of File
