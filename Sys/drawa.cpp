//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// アニメ
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWA::DRAWA() {}
DRAWA::~DRAWA(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void DRAWA::InitAnime( void ){

	anime_id	= 0L;
	anime_step	= 0;
	anime_stat	= STAT_NULL;
	anime_play	= true;
	anime_loop	= 0.0f;
	anime_time	= 0.0f;
	anime_seek	= 0.0f;
	anime_rate	= 1.0f;
}

void DRAWA::FreeAnime( void ){}

//----------------------------------------
// 設定＆取得
//----------------------------------------
void  DRAWA::SetAnimeID( UINT v ){ anime_id = v; anime_step	= 0;	}

void  DRAWA::SetAnimePlay(  void ){ anime_play = true; 	}
void  DRAWA::SetAnimeStop(  void ){ anime_play = false;	}
void  DRAWA::SetAnimePause( void ){ anime_play = !anime_play;	}
void  DRAWA::SetAnimeLoop(  float v ){ anime_loop = v;	}
void  DRAWA::SetAnimeTime(  float v ){ anime_time = v;	}
void  DRAWA::SetAnimeSeek(  float v ){ anime_seek = v;	}
void  DRAWA::SetAnimeRate(  float v ){ anime_rate = v;	}

void  DRAWA::AddAnimeTime(  float v ){ anime_time += v;	}
void  DRAWA::AddAnimeSeek(  float v ){ anime_seek += v;	}
void  DRAWA::AddAnimeRate(  float v ){ anime_rate += v;	}

UINT  DRAWA::GetAnimeID(    void ) const { return  anime_id;	}
bool  DRAWA::GetAnimePlay(  void ) const { return  anime_play; 	}
bool  DRAWA::GetAnimeStop(  void ) const { return !anime_play; 	}
bool  DRAWA::GetAnimePause( void ) const { return  anime_play; 	}
float DRAWA::GetAnimeLoop(  void ) const { return  anime_loop;	}
float DRAWA::GetAnimeTime(  void ) const { return  anime_time;	}
float DRAWA::GetAnimeSeek(  void ) const { return  anime_seek;	}
float DRAWA::GetAnimeRate(  void ) const { return  anime_rate;	}

bool DRAWA::AnimeStatPlay(  void ) const { return ( anime_stat & STAT_PLAY  ) ? true : false; }
bool DRAWA::AnimeStatStop(  void ) const { return ( anime_stat & STAT_PLAY  ) ? false : true; }
bool DRAWA::AnimeStatBegin( void ) const { return ( anime_stat & STAT_BEGIN ) ? true : false; }
bool DRAWA::AnimeStatShift( void ) const { return ( anime_stat & STAT_SHIFT ) ? true : false; }
bool DRAWA::AnimeStatEnd(   void ) const { return ( anime_stat & STAT_END   ) ? true : false; }

float& DRAWA::AnimeTime( void ){ return anime_time;	}

// End Of File
