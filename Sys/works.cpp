//========================================
// OPAL Game Library
//========================================
#include "work.hpp"

//========================================
// コードシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ表示モード
//----------------------------------------
#ifdef OPAL_DEBUG
void WORKX::Debug( int m ){ debug = m;		}
int  WORKX::Debug( void  ){ return debug;	}
#endif

//----------------------------------------
// デフォルトリンク
//----------------------------------------
void  WORKX::Linx( LINX* l ){ linx = l;		}
void  WORKX::Linx( LINX& l ){ linx = &l;	}
void  WORKX::Linx( std::shared_ptr<LINX> l ){ linx = l.get(); }
LINX* WORKX::Linx( void ){ return linx;	}

void  WORKX::Parent( ZOBJ* p ){ parent =  p;	}
void  WORKX::Parent( ZOBJ& p ){ parent = &p;	}
void  WORKX::Parent( std::shared_ptr<ZOBJ> p ){ parent = p.get(); }
ZOBJ* WORKX::Parent( void ){ return parent;	}

// End Of File
