//========================================
// OPAL Game Library
//========================================
#include "Opal.hpp"

//========================================
// OPAL ゲームライブラリ
//========================================
using namespace opal;

//========================================
// 各種設定
//========================================
static bool	lock = false;

#ifdef  OPAL_DEBUG
#define _OPAL_DEBUG_MODE		1
#else
#define _OPAL_DEBUG_MODE		0
#endif

#ifndef _OPAL_SPLASH
#define _OPAL_SPLASH			0
#endif

#ifndef _OPAL_MUTEX
#define _OPAL_MUTEX				1
#endif

#ifndef _OPAL_PROJECT
#define _OPAL_PROJECT			"OPAL"
#endif

#ifndef _OPAL_ICON
#define _OPAL_ICON				"OPAL"
#endif

#ifndef _OPAL_HANDLE
#define _OPAL_HANDLE			false
#endif

#ifndef _OPAL_ACTIVE
#define _OPAL_ACTIVE			OPAL::ACTIVE_DEFAULT
#endif

#ifndef _OPAL_WINSTYLE
#define _OPAL_WINSTYLE	 		WS_OVERLAPPEDWINDOW
#endif

#ifndef _OPAL_SCREEN_W
#define _OPAL_SCREEN_W	 		800
#endif
#ifndef _OPAL_SCREEN_H
#define _OPAL_SCREEN_H			600
#endif

#ifndef _OPAL_DISPLAY_W
#define _OPAL_DISPLAY_W			1600
#endif
#ifndef _OPAL_DISPLAY_H
#define _OPAL_DISPLAY_H			1200
#endif

#ifndef _OPAL_SUSPEND
#define _OPAL_SUSPEND			0
#endif

#ifndef	_OPAL_MAINMEMORY_SIZE
#define	_OPAL_MAINMEMORY_SIZE	0x02000000	// 32MB
#endif

#ifndef _OPAL_PADW_MAX
#define _OPAL_PADW_MAX			30			//( 60*60 )
#endif

#ifndef OPAL_COLOR
#define OPAL_COLOR				0xff0000ff
#endif
#define _OPAL_BGCOLOR_A			((OPAL_COLOR)>>24)&0xff
#define _OPAL_BGCOLOR_R			((OPAL_COLOR)>>16)&0xff
#define _OPAL_BGCOLOR_G			((OPAL_COLOR)>> 8)&0xff
#define _OPAL_BGCOLOR_B			((OPAL_COLOR)>> 0)&0xff
#define _OPAL_BGCOLOR			OPAL_COLOR

#ifndef _OPAL_FRAME_RATE
#define _OPAL_FRAME_RATE		OPAL::FIXED60
#endif

//========================================
// コンフィグ
//========================================
OPAL::CONFIG::CONFIG(){}
OPAL::CONFIG::CONFIG( const char* p ) :
	project( p )
{}
OPAL::CONFIG::CONFIG( const std::string& p ) : CONFIG( p.c_str() ) {}
OPAL::CONFIG::CONFIG( std::nullptr_t ) : CONFIG( ( const char* )nullptr ){}
OPAL::CONFIG::~CONFIG(){}

//========================================
// ユーザー定義関数
//========================================
OPAL::FUNC::FUNC(){}
OPAL::FUNC::~FUNC(){}

OPAL::FUNC::operator FUNC*(){ return this; }

void OPAL::FUNC::SysInit( void ){}
void OPAL::FUNC::SysExec( void ){}
void OPAL::FUNC::SysFree( void ){}

void OPAL::FUNC::SysConf( void ){

	if ( project ) {
		OPAL::PROJECT( project );
	}
}

static OPAL::FUNC*		SysFunc = nullptr;
static OPAL::sysinit	SysInit = nullptr;
static OPAL::sysexec	SysExec = nullptr;
static OPAL::sysfree	SysFree = nullptr;

void OPAL::SYSINIT( OPAL::sysinit f ){ SysInit = f; }
void OPAL::SYSEXEC( OPAL::sysexec f ){ SysExec = f; }
void OPAL::SYSFREE( OPAL::sysfree f ){ SysFree = f; }
OPAL::sysinit OPAL::SYSINIT( void ){ return SysInit; }
OPAL::sysexec OPAL::SYSEXEC( void ){ return SysExec; }
OPAL::sysfree OPAL::SYSFREE( void ){ return SysFree; }

void OPAL::SYSFUNC( sysinit fi, sysexec fe, sysfree ff ){

	if ( !lock ) {
		SYSINIT( fi );
		SYSEXEC( fe );
		SYSFREE( ff );
	}
}

void OPAL::SYSFUNC( sysexec f ){

	SYSFUNC( nullptr, f, nullptr );
}

void OPAL::SYSFUNC( FUNC* p ){

	if ( !lock ) {
		SysFunc = p;
	}
}

void OPAL::SYSFUNC( FUNC& p ){

	SYSFUNC( &p );
}

OPAL::FUNC* OPAL::SYSFUNC( void ){ return SysFunc; }

//========================================
// スプラッシュ
//========================================
static UINT	OPAL_SPLASH = _OPAL_SPLASH;

void OPAL::SPLASH( UINT t ){ if ( !lock ) { OPAL_SPLASH = t; }	}
UINT OPAL::SPLASH( void ){ return OPAL_SPLASH;	}

//========================================
// バージョン
//========================================
#ifndef _OPAL_VERSION
#define _OPAL_VERSION	1.00
#endif
float OPAL::VERSION( void ){ return _OPAL_VERSION; }

//========================================
// 引数＆実行ファイル
//========================================
static UINT		OPAL_ARGC = 0;
static char**	OPAL_ARGV = nullptr;

static char OPAL_GAME_EXE_PATH[764+1];
static char OPAL_GAME_EXE_FILE[260+1];
static char OPAL_GAME_EXE_CURD[764+1];
static char OPAL_GAME_EXE_RELP[764+1];

UINT OPAL::ARGC( void ){ return OPAL_ARGC-1; }
const char* OPAL::ARGV( void   ){ return *OPAL_ARGV; }
const char* OPAL::ARGV( UINT n ){ return ( n < OPAL_ARGC-1 ) ? OPAL_ARGV[n+1] : nullptr; }
const char* OPAL::GAME_PATH( void ){ return OPAL_GAME_EXE_PATH; }
const char* OPAL::GAME_FILE( void ){ return OPAL_GAME_EXE_FILE; }
const char* OPAL::GAME_CURD( void ){ return OPAL_GAME_EXE_CURD; }
const char* OPAL::GAME_RELP( void ){ return OPAL_GAME_EXE_RELP; }

static void ARGUMENT( void ){

	if ( OPAL_ARGC == 0 ) {
		WCHAR**	p = ::CommandLineToArgvW( ::GetCommandLineW(), ( int* )&OPAL_ARGC );

		OPAL_ARGV = ( char** )std::malloc( sizeof( char* ) * OPAL_ARGC );

		for ( auto i = 0UL; i < OPAL_ARGC; i++ ) {
			const auto	n = ::wcslen( p[i] ) << 1;

			OPAL_ARGV[i] = ( char* )std::malloc( sizeof( char ) * n );

			::wcstombs( OPAL_ARGV[i], p[i], n );
		}

		{
			TCHAR exe_n[1024+1];
			TCHAR exe_d[   4+1];
			TCHAR exe_p[ 760+1];
			TCHAR exe_f[ 256+1];
			TCHAR exe_e[   4+1];

			::GetModuleFileName( nullptr, exe_n, std::size( exe_n ) );
			::_tsplitpath( exe_n, exe_d, exe_p, exe_f, exe_e );
			::sprintf( OPAL_GAME_EXE_PATH, "%s%s", exe_d, exe_p );
			::sprintf( OPAL_GAME_EXE_FILE, "%s%s", exe_f, exe_e );

			::GetCurrentDirectory( std::size( OPAL_GAME_EXE_CURD ), OPAL_GAME_EXE_CURD );
			::PathRelativePathTo( OPAL_GAME_EXE_RELP,
								  OPAL_GAME_EXE_CURD,
								  FILE_ATTRIBUTE_DIRECTORY,
								  OPAL_GAME_EXE_PATH,
								  FILE_ATTRIBUTE_DIRECTORY );
		}
	}
}

static void _ARGUMENT( void ) {

	for ( auto i = 0UL; i < OPAL_ARGC; i++ ) {
		std::free( OPAL_ARGV[i] );
	}
	std::free( OPAL_ARGV );
}

//========================================
// ファイルパス
//========================================
static const char* OPAL_FILEPATH = nullptr;
static char _OPAL_FILEPATH[1024+1];

const char* OPAL::FILEPATH( void ){ return OPAL_FILEPATH;	}
void OPAL::FILEPATH( const char* p ){

	if ( p ) {
		OPAL_FILEPATH = _OPAL_FILEPATH;
		std::strcpy( _OPAL_FILEPATH, p );
	} else {
		OPAL_FILEPATH = nullptr;
		*_OPAL_FILEPATH = '\0';
	}
}

//========================================
// コンソール
//========================================
static bool	OPAL_CONSOLE = true;

#ifdef OPAL_DEBUG
static void CONSOLE( void ) {

	if ( OPAL_CONSOLE ) {
		::AllocConsole();
		std::freopen( "CONOUT$", "w", stdout );
		std::freopen( "CONIN$",  "r", stdin  );
	}
}

static void _CONSOLE( void ) {

	if ( OPAL_CONSOLE ) {
		::FreeConsole();
	}
}
#else
static void  CONSOLE( void ) {}
static void _CONSOLE( void ) {}
#endif

void OPAL::CONSOLE( bool f ){ if ( !lock ) { OPAL_CONSOLE = f; } }
bool OPAL::CONSOLE( void ){ return OPAL_CONSOLE; }

//========================================
// デバッグモード
//========================================
static UINT	OPAL_DEBUG_MODE = _OPAL_DEBUG_MODE;

void OPAL::DEBUG_MODE( UINT m ){

	if ( !lock ) {
		if ( ( OPAL_DEBUG_MODE = m ) > 0 ) {
			CONSOLE();
			ARGUMENT();
		}
	}
}

UINT OPAL::DEBUG_MODE( void ){ return OPAL_DEBUG_MODE; }

//========================================
// 多重起動
//========================================
static UINT OPAL_MUTEX = _OPAL_MUTEX;

void OPAL::MUTEX( UINT s ){ if ( !lock ) { OPAL_MUTEX = s; } }
UINT OPAL::MUTEX( void ){ return OPAL_MUTEX; }

static HANDLE	mutex;
static char		mname[16];

static bool PROCESS(  void ){

	if ( OPAL_MUTEX ) {
		SECURITY_DESCRIPTOR	sd{};

		if ( !::InitializeSecurityDescriptor( &sd, SECURITY_DESCRIPTOR_REVISION ) ) {
			return false;
		}
		if ( !::SetSecurityDescriptorDacl( &sd, true, 0, false ) ) {
			return false;
		}

		for ( auto i = 0UL; i < OPAL_MUTEX; i++ ) {
			SECURITY_ATTRIBUTES sa{};

			sa.nLength				= sizeof( SECURITY_ATTRIBUTES );
			sa.lpSecurityDescriptor	= &sd;
			sa.bInheritHandle		= true;

			::sprintf_s( mname, sizeof( mname ), "OPAL:%08x", ( int )i );

			if ( !( mutex = ::CreateMutex( &sa, false, mname ) ) ) {
				return false;
			}

			if ( ::GetLastError() != ERROR_ALREADY_EXISTS ) {
				return true;
			}
		}
		return false;
	}

	return true;
}

static void _PROCESS( void ){

	if ( OPAL_MUTEX ) {
		if ( mutex ) {
			::ReleaseMutex( mutex );
			::CloseHandle(  mutex );
		}
	}
}

//========================================
// ウィンドウタイトル
//========================================
static NAME	OPAL_PROJECT = _OPAL_PROJECT;

void OPAL::PROJECT( const char* p ){

	if ( !lock ) {
		if ( p && *p ) {
			OPAL_PROJECT = p;
		}
	}
}
void OPAL::PROJECT( const std::string& p ){ PROJECT( p.c_str() ); }
void OPAL::PROJECT( std::nullptr_t       ){ PROJECT( ( const char* )nullptr ); }

const char* OPAL::PROJECT( void ){ return OPAL_PROJECT; }

//========================================
// ウィンドウアイコン
//========================================
static NAME	OPAL_ICON = _OPAL_ICON;

void OPAL::ICON( const char* p ){

	if ( !lock ) {
		OPAL_ICON = p;
	}
}
void OPAL::ICON( const std::string& p ){ ICON( p.c_str() ); }
void OPAL::ICON( std::nullptr_t       ){ ICON( ( const char* )nullptr ); }

const char* OPAL::ICON( void ){ return OPAL_ICON; }

//========================================
// ハンドル
//========================================
static bool OPAL_HANDLE = _OPAL_HANDLE;

void OPAL::HANDLE( bool f ){ OPAL_HANDLE = f;	}
bool OPAL::HANDLE( void ){	return OPAL_HANDLE;	}

//========================================
// 動作モード
//========================================
static UINT OPAL_ACTIVE = _OPAL_ACTIVE;

void OPAL::ACTIVE( UINT a ){ OPAL_ACTIVE = a;	}
UINT OPAL::ACTIVE( void ){	return OPAL_ACTIVE;	}

//========================================
// ウィンドウスタイル
//========================================
static UINT OPAL_WINSTYLE	= _OPAL_WINSTYLE;

void OPAL::WINSTYLE( UINT s ){

	if ( !lock ) {
		switch ( s ) {
		case 0:
			OPAL_WINSTYLE ^= WS_THICKFRAME;
			break;

		case 1:
			OPAL_WINSTYLE ^= (WS_THICKFRAME|WS_MAXIMIZEBOX);
			break;

		default:
			OPAL_WINSTYLE = s;
			break;
		}
	}
}

UINT OPAL::WINSTYLE( void ){ return OPAL_WINSTYLE;		}

//========================================
// スクリーンサイズ
//========================================
static UINT OPAL_SCREEN_W	= _OPAL_SCREEN_W;
static UINT OPAL_SCREEN_H	= _OPAL_SCREEN_H;

void OPAL::SCREEN( UINT w, UINT h ){

	if ( !lock ) {
		OPAL_SCREEN_W = w;
		OPAL_SCREEN_H = h;
	}
}

void OPAL::SCREEN_W( UINT s ){ if ( !lock ) { OPAL_SCREEN_W = s; } }
void OPAL::SCREEN_H( UINT s ){ if ( !lock ) { OPAL_SCREEN_H = s; } }

UINT OPAL::SCREEN_W( void ){ return OPAL_SCREEN_W;		}
UINT OPAL::SCREEN_H( void ){ return OPAL_SCREEN_H;		}

//========================================
// 描画領域サイズ
//========================================
static UINT OPAL_DISPLAY_W	= _OPAL_DISPLAY_W;
static UINT OPAL_DISPLAY_H	= _OPAL_DISPLAY_H;

void OPAL::DISPLAY( UINT w, UINT h ){

	if ( !lock ) {
		OPAL_DISPLAY_W = w;
		OPAL_DISPLAY_H = h;
	}
}

void OPAL::DISPLAY_W( UINT s ){ if ( !lock ) { OPAL_DISPLAY_W = s; } }
void OPAL::DISPLAY_H( UINT s ){ if ( !lock ) { OPAL_DISPLAY_H = s; } }

UINT OPAL::DISPLAY_W( void ){ return OPAL_DISPLAY_W;	}
UINT OPAL::DISPLAY_H( void ){ return OPAL_DISPLAY_H;	}

//========================================
// カーソル
//========================================
static bool	OPAL_CURSOR = true;

void OPAL::CURSOR( bool f ){ OPAL_CURSOR = f;	}
bool OPAL::CURSOR( void ){ return OPAL_CURSOR;	}

//========================================
// 背景色
//========================================
static UCHAR	OPAL_BGCOLOR_A = 0xff;
static UCHAR	OPAL_BGCOLOR_R = _OPAL_BGCOLOR_R;
static UCHAR	OPAL_BGCOLOR_G = _OPAL_BGCOLOR_G;
static UCHAR	OPAL_BGCOLOR_B = _OPAL_BGCOLOR_B;

void OPAL::BGCOLOR( UCHAR r, UCHAR g, UCHAR b ) {

	if ( !lock ) {
		OPAL_BGCOLOR_A = 0xff;
		OPAL_BGCOLOR_R = r;
		OPAL_BGCOLOR_G = g;
		OPAL_BGCOLOR_B = b;
	}
};

void OPAL::BGCOLOR( UINT c ){

	if ( !lock ) {
		OPAL_BGCOLOR_A = 0xff;
		OPAL_BGCOLOR_R = ( c >> 16 ) & 0xff;
		OPAL_BGCOLOR_G = ( c >>  8 ) & 0xff;
		OPAL_BGCOLOR_B = ( c >>  0 ) & 0xff;
	}
}

UCHAR OPAL::BGCOLOR_R( void ){ return OPAL_BGCOLOR_R; }
UCHAR OPAL::BGCOLOR_G( void ){ return OPAL_BGCOLOR_G; }
UCHAR OPAL::BGCOLOR_B( void ){ return OPAL_BGCOLOR_B; }

UINT OPAL::BGCOLOR( void ){

	return (((OPAL_BGCOLOR_R<<16)&0xff)|
			((OPAL_BGCOLOR_G<< 8)&0xff)|
			((OPAL_BGCOLOR_B<< 0)&0xff));
}

//========================================
// フレームレート
//========================================
static OPAL::FRAME_RATE	OPAL_FRAME_RATE = OPAL::FIXED60;

void OPAL::FRAME( FRAME_RATE r ){ OPAL_FRAME_RATE = r;	}
OPAL::FRAME_RATE OPAL::FRAME( void ){ return OPAL_FRAME_RATE;	}

//========================================
// メモリサイズ
//========================================
static size_t	OPAL_MAINMEMORY_SIZE = _OPAL_MAINMEMORY_SIZE;
static int*		OPAL_MAINMEMORY_ADRS = nullptr;

void   OPAL::MAINMEMORY_SIZE( size_t s ){ if ( !lock ) { OPAL_MAINMEMORY_SIZE = s; } }
size_t OPAL::MAINMEMORY_SIZE( void ){ return OPAL_MAINMEMORY_SIZE; }

void* OPAL::MAINMEMORY_ADRS( void ){ return OPAL_MAINMEMORY_ADRS; }

//========================================
// パッドバッファサイズ
//========================================
static UINT OPAL_PADW_MAX = _OPAL_PADW_MAX;

void OPAL::PAD_WORK( UINT s ){ if ( !lock ) { OPAL_PADW_MAX = s; } }
UINT OPAL::PAD_WORK( void ){ return OPAL_PADW_MAX; }

static UINT OPAL_PAD_HOME	= ( UINT )-1;
static UINT OPAL_PAD_EXIT	= ( UINT )-1;
static UINT OPAL_PAD_FULL	= ( UINT )-1;
static UINT OPAL_PAD_RESET	= ( UINT )-1;
static UINT OPAL_PAD_PAUSE	= ( UINT )-1;
static UINT OPAL_PAD_BREAK	= ( UINT )-1;
static UINT OPAL_PAD_CAPTURE= ( UINT )-1;
static UINT OPAL_PAD_FRAME	= ( UINT )-1;
static UINT OPAL_PAD_TIMES	= ( UINT )-1;
static UINT OPAL_PAD_TIMEC	= ( UINT )-1;
static UINT OPAL_PAD_RANDOM	= ( UINT )-1;

void OPAL::PAD_HOME(    UINT k ){ if ( !lock ) { OPAL_PAD_HOME		= k;	} }
void OPAL::PAD_EXIT(    UINT k ){ if ( !lock ) { OPAL_PAD_EXIT		= k;	} }
void OPAL::PAD_FULL(    UINT k ){ if ( !lock ) { OPAL_PAD_FULL		= k;	} }
void OPAL::PAD_RESET(   UINT k ){ if ( !lock ) { OPAL_PAD_RESET		= k;	} }
void OPAL::PAD_PAUSE(   UINT k ){ if ( !lock ) { OPAL_PAD_PAUSE		= k;	} }
void OPAL::PAD_BREAK(   UINT k ){ if ( !lock ) { OPAL_PAD_BREAK		= k;	} }
void OPAL::PAD_CAPTURE( UINT k ){ if ( !lock ) { OPAL_PAD_CAPTURE	= k;	} }
void OPAL::PAD_FRAME(   UINT k ){ if ( !lock ) { OPAL_PAD_FRAME		= k;	} }
void OPAL::PAD_TIMES(   UINT k ){ if ( !lock ) { OPAL_PAD_TIMES		= k;	} }
void OPAL::PAD_TIMEC(   UINT k ){ if ( !lock ) { OPAL_PAD_TIMEC		= k;	} }
void OPAL::PAD_RANDOM(  UINT k ){ if ( !lock ) { OPAL_PAD_RANDOM	= k;	} }
UINT OPAL::PAD_HOME(    void ){ return OPAL_PAD_HOME; 		}
UINT OPAL::PAD_EXIT(    void ){ return OPAL_PAD_EXIT; 		}
UINT OPAL::PAD_FULL(    void ){ return OPAL_PAD_FULL; 		}
UINT OPAL::PAD_RESET(   void ){ return OPAL_PAD_RESET;		}
UINT OPAL::PAD_PAUSE(   void ){ return OPAL_PAD_PAUSE; 		}
UINT OPAL::PAD_BREAK(   void ){ return OPAL_PAD_BREAK; 		}
UINT OPAL::PAD_CAPTURE( void ){ return OPAL_PAD_CAPTURE; 	}
UINT OPAL::PAD_FRAME(   void ){ return OPAL_PAD_FRAME; 		}
UINT OPAL::PAD_TIMES(   void ){ return OPAL_PAD_TIMES; 		}
UINT OPAL::PAD_TIMEC(   void ){ return OPAL_PAD_TIMEC; 		}
UINT OPAL::PAD_RANDOM(  void ){ return OPAL_PAD_RANDOM; 	}

//========================================
// クリティカルセクション
//========================================
static CRITICAL_SECTION	section;

OPAL::CRITICAL::CRITICAL() { EnterCriticalSection( &section ); }
OPAL::CRITICAL::~CRITICAL(){ LeaveCriticalSection( &section ); }

CRITICAL_SECTION* OPAL::Lock( void ){ return &section;	}

//========================================
// メッセージ
//========================================
void OPAL::MESSAGE( int(*func)( const char*, ... ) ) {

	const auto	print = func ? func : ( decltype( func ) )printf;

	print( "================================================================\n" );
	print( "OPAL Game Library Ver %1.3f  \n", _OPAL_VERSION );
	print( "================================================================\n" );
	print( "DEBUG      %s\n",  OPAL_DEBUG_MODE ? "ON" : "OFF");
	print( "MUTEX      %s\n",  OPAL_MUTEX  ? "+" : "-" );
	print( "PROJECT    %s\n",  ( const char* )OPAL_PROJECT );
	print( "ICON       %s\n",  ( const char* )OPAL_ICON );
	print( "SCREEN_W   %d\n",  OPAL_SCREEN_W );
	print( "SCREEN_H   %d\n",  OPAL_SCREEN_H );
	print( "DISPLAY_W  %d\n",  OPAL_DISPLAY_W );
	print( "DISPLAY_H  %d\n",  OPAL_DISPLAY_H );
	print( "MEMSIZE    %3.1f(M)\n",  ( float )( OPAL_MAINMEMORY_SIZE/(1024*1024) ) );
	print( "MEMADRS    0x%p\n",OPAL_MAINMEMORY_ADRS );
	print( "PADWORK    %d\n",  OPAL_PADW_MAX);
	print( "BGCOLOR    %02x|%02x|%02x|%02x\n",OPAL_BGCOLOR_A,OPAL_BGCOLOR_R,OPAL_BGCOLOR_G,OPAL_BGCOLOR_B );
	print( "EXE_PATH   %s\n", OPAL_GAME_EXE_PATH );
	print( "EXE_FILE   %s\n", OPAL_GAME_EXE_FILE );
	print( "EXE_CURD   %s\n", OPAL_GAME_EXE_CURD );
	print( "EXE_RELP   %s\n", OPAL_GAME_EXE_RELP );
	print( "ARGUMNET   %d\n", OPAL_ARGC-1 );
	for ( auto i = 1UL; i < OPAL_ARGC; i++ ) {
		print( "  [%d] %s\n", ( int )i-1, OPAL_ARGV[i] );
	}
}

//========================================
// OPAL SYSTEM
//========================================
int Opal( void ){

	::setlocale( LC_ALL, "JPN" );

	CONSOLE();
	ARGUMENT();

	OPAL_MAINMEMORY_ADRS = ( int* )std::malloc( OPAL_MAINMEMORY_SIZE );

	if ( const auto opal = OPAL::SYSFUNC() ) {
		opal->SysConf();
	}

	lock = true;

	if ( PROCESS() ) {
		WINDOWX::Open();
#if defined( OPAL_DEBUG ) && defined( NOEXCEPT )
		try {
			SYSTEM::Main();
		} catch( const char* error ) {
			Printf( "%s", error );
		}
#else
		SYSTEM::Main();
#endif
		WINDOWX::Close();
	}

	std::free( OPAL_MAINMEMORY_ADRS );

	_PROCESS();
	_ARGUMENT();
	_CONSOLE();

	return EXIT_SUCCESS;
}

using OSI = OPAL::sysinit;
using OSE = OPAL::sysexec;
using OSF = OPAL::sysfree;
using OFP = OPAL::FUNC*;
using OFR = OPAL::FUNC&;
using CCP = const char*;
using CSS = const std::string&;
using NLP = std::nullptr_t;

#define np	nullptr

int Opal( OSI fi, OSE fe, OSF ff ){ OPAL::SYSFUNC(  fi, fe, ff ); return Opal();	}
int Opal( OSI fi, OSE fe         ){ OPAL::SYSFUNC(  fi, fe, np ); return Opal();	}
int Opal(         OSE fe         ){ OPAL::SYSFUNC(  np, fe, np ); return Opal();	}

int Opal( OFP ps ){ OPAL::SYSFUNC(  ps ); return Opal();	}
int Opal( OFR ps ){ OPAL::SYSFUNC( &ps ); return Opal();	}

int Opal( CCP pp ){ OPAL::PROJECT( pp ); return Opal();	}
int Opal( CSS pp ){ OPAL::PROJECT( pp ); return Opal();	}
int Opal( NLP pp ){ OPAL::PROJECT( pp ); return Opal();	}

int Opal( CCP pp, OSI fi, OSE fe, OSF ff ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, ff ); return Opal();	}
int Opal( CSS pp, OSI fi, OSE fe, OSF ff ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, ff ); return Opal();	}
int Opal( NLP pp, OSI fi, OSE fe, OSF ff ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, ff ); return Opal();	}
int Opal( CCP pp, OSI fi, OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, np ); return Opal();	}
int Opal( CSS pp, OSI fi, OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, np ); return Opal();	}
int Opal( NLP pp, OSI fi, OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( fi, fe, np ); return Opal();	}
int Opal( CCP pp,         OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( np, fe, np ); return Opal();	}
int Opal( CSS pp,         OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( np, fe, np ); return Opal();	}
int Opal( NLP pp,         OSE fe         ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( np, fe, np ); return Opal();	}

int Opal( CCP pp, OFP ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC(  ps ); return Opal();	}
int Opal( CSS pp, OFP ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC(  ps ); return Opal();	}
int Opal( NLP pp, OFP ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC(  ps ); return Opal();	}
int Opal( CCP pp, OFR ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( &ps ); return Opal();	}
int Opal( CSS pp, OFR ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( &ps ); return Opal();	}
int Opal( NLP pp, OFR ps  ){ OPAL::PROJECT( pp ); OPAL::SYSFUNC( &ps ); return Opal();	}

// End Of File
