//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

#undef OPAL_TEMP

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// スペック表示
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal::DRAWX_DX11 {
	void Spec( void ){
		printd("\n" );
		device.Debug();		printd( "\n" );
		viewport.Debug();	printd( "\n" );
	}
}

namespace opal::DRAWX_DX11 {
	void DEVICE::Debug( void ) const {

		printd( "FEATURE LEVEL %s\n", Features( feature ) );
	}

	//void MONITOR::Debug( void ) const {}

	void VIEW_PORT::Debug( void ) const {

		const auto		context = device(0);
		constexpr UINT	n = 4;
		UINT 			v = n;
		VIEWPORT		p[n];

		context->RSGetViewports( &v, p );

		printd( "VIEWPORT\n" );
		for ( auto i = 0UL; i < v; i++ ) {
			printd( "----------------------------------------\n" );
			printd( "TopLeftX : %f\n", p[i].TopLeftX	);
			printd( "TopLeftY : %f\n", p[i].TopLeftY	);
			printd( "Width    : %f\n", p[i].Width		);
			printd( "Height   : %f\n", p[i].Height		);
			printd( "MinDepth : %f\n", p[i].MinDepth	);
			printd( "MaxDepth : %f\n", p[i].MaxDepth	);
		}
	}

	void CLEAR_BUFFER::Debug( void ) const {}
	//void RENDER_STATE::Debug( void ) const {}
	//void TEXTURE_STAGE::Debug( void ) const {}
}

//----------------------------------------
// Shader Reflection
//----------------------------------------
// (UINT Version)
// (LPCSTR Creator)
// UINT Flags;
// (UINT ConstantBuffers)
// UINT BoundResources;
// (UINT InputParameters)
// UINT OutputParameters;
// UINT InstructionCount;
// UINT TempRegisterCount;
// UINT TempArrayCount;
// UINT DefCount;
// UINT DclCount;
// UINT TextureNormalInstructions;
// UINT TextureLoadInstructions;
// UINT TextureCompInstructions;
// UINT TextureBiasInstructions;
// UINT TextureGradientInstructions;
// UINT FloatInstructionCount;
// UINT IntInstructionCount;
// UINT UintInstructionCount;
// UINT StaticFlowControlCount;
// UINT DynamicFlowControlCount;
// UINT MacroInstructionCount;
// UINT ArrayInstructionCount;
// UINT CutInstructionCount;
// UINT EmitInstructionCount;
// D3D10_PRIMITIVE_TOPOLOGY GSOutputTopology;
// UINT GSMaxOutputVertexCount;
// D3D11_PRIMITIVE InputPrimitive;
// UINT PatchConstantParameters;
// UINT cGSInstanceCount;
// UINT cControlPoints;
// D3D11_TESSELLATOR_OUTPUT_PRIMITIVE HSOutputPrimitive;
// D3D11_TESSELLATOR_PARTITIONING HSPartitioning;
// D3D11_TESSELLATOR_DOMAIN TessellatorDomain;
// UINT cBarrierInstructions;
// UINT cInterlockedInstructions;
// UINT cTextureStoreInstructions;

namespace {
	const char* SystemValueType( D3D_NAME t );
	const char* ComponentType( D3D_REGISTER_COMPONENT_TYPE );
#ifdef OPAL_TEMP
	const char* MinPrecision( D3D_MIN_PRECISION );
#endif
	const char* BufferType( D3D11_CBUFFER_TYPE );
	const char* VariableuFlags( UINT );
}

namespace opal::DRAWX_DX11 {
	void ShaderReflection( const void* sp, size_t ss, const char* sn ){

		ID3D11ShaderReflection*	rp = nullptr;

		::D3DReflect( sp, ss, IID_ID3D11ShaderReflection, ( void** )&rp );

		if ( !rp ) {
			return;
		}

		D3D11_SHADER_DESC shader{};

		rp->GetDesc( &shader );

		printf( "================================================================================\n" );
		printf( "= Shader  : %s\n", sn );
		printf( "= Version : %x\n", shader.Version );
		printf( "= Creator : %s\n", shader.Creator );
		printf( "================================================================================\n" );
		printf( "--------------------------------------------------------------------------------\n" );
		printf( "- Input Parameters\n" );
		printf( "--------------------------------------------------------------------------------\n" );
		for ( auto i = 0UL; i < shader.InputParameters; i++ ) {
			D3D11_SIGNATURE_PARAMETER_DESC	signature{};

			rp->GetInputParameterDesc( i, &signature );
			printf( "[%02ld] %03d reg%02d %s %s %02x %02x %d %s %s\n",
					i,
					signature.SemanticIndex,
					signature.Register,
					SystemValueType( signature.SystemValueType ),
					ComponentType( signature.ComponentType ),
					signature.Mask,
					signature.ReadWriteMask,
					signature.Stream,
#ifdef OPAL_TEMP
					MinPrecision( signature.MinPrecision ),
#else
					"has no member MinPrecision",
#endif
					signature.SemanticName );
		}
		printf( "\n" );

		printf( "--------------------------------------------------------------------------------\n" );
		printf( "- Constant Buffers\n" );
		printf( "--------------------------------------------------------------------------------\n" );
		for ( auto i = 0UL; i < shader.ConstantBuffers; i++ ) {
			D3D11_SHADER_BUFFER_DESC 		buffer{};

			auto cp = rp->GetConstantBufferByIndex( i );

			cp->GetDesc( &buffer );

			printf( "[%02ld] %s %03d %03d %08x %s\n", i,
					BufferType( buffer.Type ),
					buffer.Variables,
					buffer.Size,
					buffer.uFlags,
					buffer.Name );

			for ( auto j = 0UL; j < buffer.Variables; j++ ) {
				D3D11_SHADER_VARIABLE_DESC	variable{};

				auto vp = cp->GetVariableByIndex( j );

				vp->GetDesc( &variable );

				printf( " %02ld %03d %03d %s %p %03d %03d %03d %03d %s\n",
						j,
						variable.StartOffset,
						variable.Size,
						VariableuFlags( variable.uFlags ),
						variable.DefaultValue,
						variable.StartTexture,
						variable.TextureSize,
						variable.StartSampler,
						variable.SamplerSize,
						variable.Name );
			}
		}
		printf( "\n" );

		RELEASE( rp );
	}
}

namespace {
	const char* SystemValueType( D3D_NAME t ) {

		switch ( t ) {
		case D3D_NAME_UNDEFINED						: return "UNDEFINED                    ";
		case D3D_NAME_POSITION                      : return "POSITION                     ";
		case D3D_NAME_CLIP_DISTANCE                 : return "CLIP_DISTANCE                ";
		case D3D_NAME_CULL_DISTANCE                 : return "CULL_DISTANCE                ";
		case D3D_NAME_RENDER_TARGET_ARRAY_INDEX     : return "RENDER_TARGET_ARRAY_INDEX    ";
		case D3D_NAME_VIEWPORT_ARRAY_INDEX          : return "VIEWPORT_ARRAY_INDEX         ";
		case D3D_NAME_VERTEX_ID                     : return "VERTEX_ID                    ";
		case D3D_NAME_PRIMITIVE_ID                  : return "PRIMITIVE_ID                 ";
		case D3D_NAME_INSTANCE_ID                   : return "INSTANCE_ID                  ";
		case D3D_NAME_IS_FRONT_FACE                 : return "IS_FRONT_FACE                ";
		case D3D_NAME_SAMPLE_INDEX                  : return "SAMPLE_INDEX                 ";
		case D3D_NAME_FINAL_QUAD_EDGE_TESSFACTOR    : return "FINAL_QUAD_EDGE_TESSFACTOR   ";
		case D3D_NAME_FINAL_QUAD_INSIDE_TESSFACTOR  : return "FINAL_QUAD_INSIDE_TESSFACTOR ";
		case D3D_NAME_FINAL_TRI_EDGE_TESSFACTOR     : return "FINAL_TRI_EDGE_TESSFACTOR    ";
		case D3D_NAME_FINAL_TRI_INSIDE_TESSFACTOR   : return "FINAL_TRI_INSIDE_TESSFACTOR  ";
		case D3D_NAME_FINAL_LINE_DETAIL_TESSFACTOR  : return "FINAL_LINE_DETAIL_TESSFACTOR ";
		case D3D_NAME_FINAL_LINE_DENSITY_TESSFACTOR	: return "FINAL_LINE_DENSITY_TESSFACTOR";
		case D3D_NAME_TARGET						: return "TARGET                       ";
		case D3D_NAME_DEPTH                         : return "DEPTH                        ";
		case D3D_NAME_COVERAGE                      : return "COVERAGE                     ";
		case D3D_NAME_DEPTH_GREATER_EQUAL           : return "DEPTH_GREATER_EQUAL          ";
		case D3D_NAME_DEPTH_LESS_EQUAL              : return "DEPTH_LESS_EQUAL             ";
#ifdef OPAL_TEMP
		case D3D_NAME_STENCIL_REF                   : return "STENCIL_REF                  ";
		case D3D_NAME_INNER_COVERAGE                : return "INNER_COVERAGE               ";
#endif
		default: break;
		}
		return nullptr;
	}

	const char* ComponentType( D3D_REGISTER_COMPONENT_TYPE t ){

		switch ( t ) {
		case D3D_REGISTER_COMPONENT_UNKNOWN	: return "UNKNOWN";
		case D3D_REGISTER_COMPONENT_UINT32	: return "UINT32 ";
		case D3D_REGISTER_COMPONENT_SINT32	: return "SINT32 ";
		case D3D_REGISTER_COMPONENT_FLOAT32	: return "FLOAT32";
		}
		return nullptr;
	}
#ifdef OPAL_TEMP
	const char* MinPrecision( D3D_MIN_PRECISION t ){

		switch ( t ) {
		case D3D_MIN_PRECISION_DEFAULT		: return "DEFAULT  ";
		case D3D_MIN_PRECISION_FLOAT_16		: return "FLOAT_16 ";
		case D3D_MIN_PRECISION_FLOAT_2_8	: return "FLOAT_2_8";
		case D3D_MIN_PRECISION_RESERVED		: return "RESERVED ";
		case D3D_MIN_PRECISION_SINT_16		: return "SINT_16  ";
		case D3D_MIN_PRECISION_UINT_16		: return "UINT_16  ";
		case D3D_MIN_PRECISION_ANY_16		: return "ANY_16   ";
		case D3D_MIN_PRECISION_ANY_10		: return "ANY_10   ";
		}
		return nullptr;
	}
#endif
	const char* BufferType( D3D11_CBUFFER_TYPE t ){

		switch ( t ) {
		case D3D11_CT_CBUFFER				: return "CBUFFER			";
		case D3D11_CT_TBUFFER				: return "TBUFFER			";
		case D3D11_CT_INTERFACE_POINTERS	: return "INTERFACE_POINTERS";
		case D3D11_CT_RESOURCE_BIND_INFO	: return "RESOURCE_BIND_INFO";
		}
		return nullptr;
	}

	const char* VariableuFlags( UINT f ){

		static char	flag[9+1] = {};

		flag[0] = ( f & D3D_SVF_USERPACKED             ) ? '+' : '-';
		flag[1] = ( f & D3D_SVF_USED                   ) ? '+' : '-';
		flag[2] = ( f & D3D_SVF_INTERFACE_POINTER      ) ? '+' : '-';
		flag[3] = ( f & D3D_SVF_INTERFACE_PARAMETER    ) ? '+' : '-';
		flag[4] = '|';
		flag[5] = ( f & D3D10_SVF_USERPACKED           ) ? '+' : '-';
		flag[6] = ( f & D3D10_SVF_USED                 ) ? '+' : '-';
		flag[7] = ( f & D3D11_SVF_INTERFACE_POINTER    ) ? '+' : '-';
		flag[8] = ( f & D3D11_SVF_INTERFACE_PARAMETER  ) ? '+' : '-';

		return flag;
	}
}

#endif

// End Of File
