//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックワーク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
DRAWS::DRAWS(){}
DRAWS::~DRAWS(){}

DRAWS::FLAG::FLAG() :
	pers(   0 ),
	disp(   0 ),
	anime(  0 ),
	render( 0 ),
	revu(   0 ),
	revv(   0 )
{}

DRAWS::PROP::PROP() :
	part( NORMAL )
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void DRAWS::Init( void ){

	state	= DRAW_STATE_DEFAULT;

	new ( &flag ) FLAG;
	new ( &prop ) PROP;

	flag.disp .v[0]	= DRAWX::Disp();
	flag.anime.v[0]	= DRAWX::Anime();
	flag.alpha.v[0]	= DRAWX::Alpha();

	flag.pers.s		= FLAG_SUPER_NULL;
	flag.disp.s		= FLAG_SUPER_AND;
	flag.anime.s	= FLAG_SUPER_AND;
	flag.alpha.s	= FLAG_SUPER_P_TRUE;
	flag.render.s	= FLAG_SUPER_NULL;
	flag.revu.s		= FLAG_SUPER_XOR;
	flag.revv.s		= FLAG_SUPER_XOR;
}

void DRAWS::Free( void ){}

//----------------------------------------
// 描画プロパティ
//----------------------------------------
void DRAWS::SetPart( UINT t ){ prop.part = t;	}

UINT DRAWS::GetPart( void ) const { return prop.part;	}

//----------------------------------------
// 描画フラグ
//----------------------------------------
void DRAWS::SetPers(    int f ){ FLAG_CTRL( flag.pers	.v[0], f );	}
void DRAWS::SetDisp(    int f ){ FLAG_CTRL( flag.disp	.v[0], f );	}
void DRAWS::SetAnime(   int f ){ FLAG_CTRL( flag.anime	.v[0], f );	}
void DRAWS::SetAlpha(   int f ){ FLAG_CTRL( flag.alpha	.v[0], f );	}
void DRAWS::SetRender(  int f ){ FLAG_CTRL( flag.render	.v[0], f );	}
void DRAWS::SetReverse( int f ){ FLAG_CTRL( flag.revu	.v[0], f );	}
void DRAWS::SetRevU(    int f ){ FLAG_CTRL( flag.revu	.v[0], f );	}
void DRAWS::SetRevV(    int f ){ FLAG_CTRL( flag.revv	.v[0], f );	}

bool DRAWS::GetPers(    void   ) const { return flag.pers	.v[ 0 ] ? true : false;	}
bool DRAWS::GetDisp(    void   ) const { return flag.disp	.v[ 0 ] ? true : false;	}
bool DRAWS::GetAnime(   void   ) const { return flag.anime	.v[ 0 ] ? true : false;	}
bool DRAWS::GetAlpha(   void   ) const { return flag.alpha	.v[ 0 ] ? true : false;	}
bool DRAWS::GetRender(  void   ) const { return flag.render	.v[ 0 ] ? true : false;	}
bool DRAWS::GetReverse( void   ) const { return flag.revu	.v[ 0 ] ? true : false;	}
bool DRAWS::GetRevU(    void   ) const { return flag.revu	.v[ 0 ] ? true : false;	}
bool DRAWS::GetRevV(    void   ) const { return flag.revv	.v[ 0 ] ? true : false;	}
bool DRAWS::GetPers(    UINT n ) const { return flag.pers	.v[n%2] ? true : false;	}
bool DRAWS::GetDisp(    UINT n ) const { return flag.disp	.v[n%2] ? true : false;	}
bool DRAWS::GetAnime(   UINT n ) const { return flag.anime	.v[n%2] ? true : false;	}
bool DRAWS::GetAlpha(   UINT n ) const { return flag.alpha	.v[n%2] ? true : false;	}
bool DRAWS::GetRender(  UINT n ) const { return flag.render	.v[n%2] ? true : false;	}
bool DRAWS::GetRevU(    UINT n ) const { return flag.revu	.v[n%2] ? true : false;	}
bool DRAWS::GetRevV(    UINT n ) const { return flag.revv	.v[n%2] ? true : false;	}

void DRAWS::SetSuperPers(   int m ){ FLAG_CTRL( flag.pers	.s, m );	}
void DRAWS::SetSuperDisp(   int m ){ FLAG_CTRL( flag.disp	.s, m );	}
void DRAWS::SetSuperAnime(  int m ){ FLAG_CTRL( flag.anime	.s, m );	}
void DRAWS::SetSuperAlpha(  int m ){ FLAG_CTRL( flag.alpha	.s, m );	}
void DRAWS::SetSuperRender( int m ){ FLAG_CTRL( flag.render	.s, m );	}
void DRAWS::SetSuperRevU(   int m ){ FLAG_CTRL( flag.revu	.s, m );	}
void DRAWS::SetSuperRevV(   int m ){ FLAG_CTRL( flag.revv	.s, m );	}

void DRAWS::SetSuperPers(   void ){ flag.pers	.s = FLAG_SUPER_NULL;	}
void DRAWS::SetSuperDisp(   void ){ flag.disp	.s = FLAG_SUPER_AND; 	}
void DRAWS::SetSuperAnime(  void ){ flag.anime	.s = FLAG_SUPER_AND; 	}
void DRAWS::SetSuperAlpha(  void ){ flag.alpha	.s = FLAG_SUPER_NULL;	}
void DRAWS::SetSuperRender( void ){ flag.render	.s = FLAG_SUPER_NULL;	}
void DRAWS::SetSuperRevU(   void ){ flag.revu	.s = FLAG_SUPER_XOR;	}
void DRAWS::SetSuperRevV(   void ){ flag.revv	.s = FLAG_SUPER_XOR;	}

int  DRAWS::GetSuperPers(   void ) const { return flag.pers		.s;	}
int  DRAWS::GetSuperDisp(   void ) const { return flag.disp		.s;	}
int  DRAWS::GetSuperAnime(  void ) const { return flag.anime		.s;	}
int  DRAWS::GetSuperAlpha(  void ) const { return flag.alpha		.s;	}
int  DRAWS::GetSuperRender( void ) const { return flag.render	.s;	}
int  DRAWS::GetSuperRevU(   void ) const { return flag.revu		.s;	}
int  DRAWS::GetSuperRevV(   void ) const { return flag.revv		.s;	}

//----------------------------------------
// 隠面消去
//----------------------------------------
bool DRAWS::Culling(  void ) const {

	return ( GetRevU()^GetRevV() );
}

//----------------------------------------
// ポリゴン反転
//----------------------------------------
void DRAWS::Reverse( VECTOR2 tv[4], float rx, float ry, float rw, float rh ) const {

	if ( !GetRevU( 1 ) ) {
		tv[0].x = rx;
		tv[1].x = rx + rw;
		tv[2].x = rx + rw;
		tv[3].x = rx;
	} else {
		tv[0].x = rx + rw;
		tv[1].x = rx;
		tv[2].x = rx;
		tv[3].x = rx + rw;
	}

	if ( !GetRevV( 1 ) ) {
		tv[0].y = ry;
		tv[1].y = ry;
		tv[2].y = ry + rh;
		tv[3].y = ry + rh;
	} else {
		tv[0].y = ry + rh;
		tv[1].y = ry + rh;
		tv[2].y = ry;
		tv[3].y = ry;
	}
}
//----------------------------------------
// 状態更新
//----------------------------------------
void DRAWS::Coordinate( const DRAWS* p ) {

	if ( p == nullptr ) {
		flag.pers	.v[1] = flag.pers	.v[0];
		flag.anime	.v[1] = flag.anime	.v[0];
		flag.alpha	.v[1] = flag.alpha	.v[0];
		flag.render	.v[1] = flag.render	.v[0];
		flag.revu	.v[1] = flag.revu	.v[0];
		flag.revv	.v[1] = flag.revv	.v[0];
	} else {
		flag.pers	.v[1] = FLAG_COORDINATE( flag.pers		.s,	flag.pers	.v[0],	p->flag.pers	.v[1] );
		flag.anime	.v[1] = FLAG_COORDINATE( flag.anime		.s,	flag.anime	.v[0],	p->flag.anime	.v[1] );
		flag.alpha	.v[1] = FLAG_COORDINATE( flag.alpha		.s,	flag.alpha	.v[0],	p->flag.alpha	.v[1] );
		flag.render	.v[1] = FLAG_COORDINATE( flag.render	.s,	flag.render	.v[0],	p->flag.render	.v[1] );
		flag.revu	.v[1] = FLAG_COORDINATE( flag.revu		.s,	flag.revu	.v[0],	p->flag.revu	.v[1] );
		flag.revv	.v[1] = FLAG_COORDINATE( flag.revv		.s,	flag.revv	.v[0],	p->flag.revv	.v[1] );
	}
}

void DRAWS::Viewport( const DRAWS* p ) {

	flag.disp.v[1] = !p ? flag.disp.v[0] : FLAG_COORDINATE( flag.disp.s, flag.disp.v[0], p->flag.disp.v[1] );
}

// End Of File
