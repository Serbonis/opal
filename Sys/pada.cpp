//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力システム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADA::PADA() :
	keyboard( KEY_NULL ),
	joypad(   PAD_NULL )
{}
PADA::PADA( UINT b, UINT k ) :
	keyboard( k ),
	joypad(   b )
{}

//----------------------------------------
// いろいろ
//----------------------------------------
void PADA::SetKeyboard( UINT k ){ keyboard = k;	}
void PADA::SetJoypad(   UINT b ){ joypad   = b;	}
void PADA::SetJoyKey(   UINT b, UINT k ){ SetJoypad( b ); SetKeyboard( k );	}

UINT PADA::GetKeyboard( void ) const { return keyboard;	}
UINT PADA::GetJoypad(   void ) const { return joypad;	}

// End Of File
