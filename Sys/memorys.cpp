//========================================
// OPAL Game Library
//========================================
#include "memory.hpp"

//========================================
// メモリ管理システム
//========================================
using namespace opal;

//----------------------------------------
// ロック確認
//----------------------------------------
const char* MEMORY::Locked( ADRS s, ADRS e ){

	auto	p = lock.Head();

	for ( auto i = lock.Elements(); i; --i ) {
		if ( const auto t = p->Task<MEMT>() ) {
			if ( t->st < e && s < t->ed ) {
				return t->ID();
			}
		}
		p = p->Next();
	}

	return nullptr;
}

//----------------------------------------
// メモリロック
//----------------------------------------
MEMT* MEMORY::Lock( ADRS s, ADRS e, const char* f ){

	if ( const auto p = NEWS<MEMT>() ) {
		p->Init( f );
		p->st = s;
		p->ed = e;

		lock.Register( p );

		return p;
	}

	return nullptr;
}

//----------------------------------------
// ロック解除
//----------------------------------------
void MEMORY::UnLock( MEMT* pLock ){

	if ( pLock ) {
		lock.Release( pLock );
		DELETES( pLock );
	}
}

// End Of File
