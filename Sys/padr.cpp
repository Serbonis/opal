//========================================
// padr.cpp
//========================================
#include "padr.hpp"

//----------------------------------------
// PAD REPEAT
//----------------------------------------
using namespace opal;

namespace {
	const auto	Key = []( auto k, auto n ){ return PADX::KeyPush(     k );	};
	const auto	Pad = []( auto p, auto n ){ return PADX::JoyxPush( n, p );	};
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
PADR::PADR( TIME t, KEY k, PAD p ) :
	step( 0 ),
	time{ t, TIME{} },
	key( k ),
	pad( p ),
	func{ k ? ( FUNC )std::bind( Key, k, std::placeholders::_1 ) : nullptr,
		  p ? ( FUNC )std::bind( Pad, p, std::placeholders::_1 ) : nullptr,
		  nullptr }
{}
PADR::PADR( TIME t, FUNC f ) :
	step( 0 ),
	time{ t, TIME{} },
	key(),
	pad(),
	func{ nullptr, nullptr, f }
{}
PADR::PADR() :
	step( 0 ),
	time(),
	func()
{}

PADR::TIME::TIME( UINT d, UINT i ) :
	d( d ),
	i( i )
{}
PADR::TIME::TIME( UINT i ) :
	d( i ),
	i( i )
{}
PADR::TIME::TIME() :
	d( 0 ),
	i( 0 )
{}

//----------------------------------------
// リセット
//----------------------------------------
void PADR::Reset( void ){ step = 0;	}

//----------------------------------------
// 時間設定
//----------------------------------------
void PADR::SetTime( TIME t         ){ time[0] = t;		}
void PADR::SetTime( UINT d, UINT i ){ time[0] = {d,i};	}
void PADR::SetTime(         UINT i ){ time[0] = {i,i};	}
auto PADR::GetTime( void ) const->TIME{ return time[0];	}

//----------------------------------------
// キー設定
//----------------------------------------
void PADR::SetKey(  KEY  k ){ key = k; func[0] = k ? ( FUNC )std::bind( Key, k, std::placeholders::_1 ) : nullptr;	}
void PADR::SetPad(  PAD  p ){ pad = p; func[1] = p ? ( FUNC )std::bind( Pad, p, std::placeholders::_1 ) : nullptr;	}
void PADR::SetFunc( FUNC f ){ func[2] = f; }

auto PADR::GetKey(  void ) const->KEY { return key;		}
auto PADR::GetPad(  void ) const->PAD { return pad;		}
auto PADR::GetFunc( void ) const->FUNC{ return func[2];	}

//----------------------------------------
// リピート処理
//----------------------------------------
auto PADR::operator()( UINT p ) const->REPEAT{

	for ( const auto& f : func ) {
		if ( f && f( p ) ) {
			switch ( step ) {
			case 0:
				time[1] = time[0];
				step++;
				return REPEAT_TRIG;

			case 1:
				if ( !time[1].d-- ) {
					step++;
					return REPEAT_PUSH;
				}
				break;

			case 2:
				if ( !time[1].i-- ) {
					time[1].i = time[0].i;
					return REPEAT_PUSH;
				}
				break;
			}
			return REPEAT_NULL;
		}
	}

	step = 0;

	return REPEAT_NULL;
}

// End Of File
