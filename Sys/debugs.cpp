//========================================
// OPAL Game Library
//========================================
#if OPAL_DEBUG
#include "debug.hpp"

//========================================
// デバッグシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ変数
//----------------------------------------
namespace {
	UINT			thread;
	bool			object;
	volatile UINT	mutex;
	UINT			timeout;
	HANDLE			handle;
}

//----------------------------------------
// デバッグ関数
//----------------------------------------
namespace opal::DEBUGX_ {
	extern void DebugLock( bool );
	extern bool DebugLock( void );
}
using namespace opal::DEBUGX_;


//========================================
// デバッグマネージャー
//========================================
//----------------------------------------
// Begin
//----------------------------------------
UINT CALLBACK DEBUGX::process( void* ){

	DEBUGX::SysInit();

	while ( object ) {
		if ( mutex ) {
			DEBUGX::SysExec();
			mutex = 0;
			DebugLock( false );
		}
	}
	return 0;
}

void DEBUGX::Begin( void ){

	object	= true;
	mutex	= 0;
	timeout	= 300;
	handle	= ( HANDLE )::_beginthreadex( nullptr, 0, process,  nullptr, 0, &thread );
}

//----------------------------------------
// Render
//----------------------------------------
void DEBUGX::Render( void ){

	mutex++;
	DebugLock( true );
}

//----------------------------------------
// End
//----------------------------------------
void DEBUGX::End( void ){

	object	= false;
	mutex	= 0;

	if ( handle ) {
		switch ( ::WaitForSingleObjectEx( handle, timeout, false ) ) {
		case WAIT_OBJECT_0:			break;
		case WAIT_ABANDONED:		break;
		case WAIT_IO_COMPLETION:	break;
		case WAIT_TIMEOUT:			break;
		case -1:
			::TerminateThread( handle, false );
			break;
		}
		::CloseHandle( handle );
		DEBUGX::SysFree();
	}
}

#endif

// End Of File
