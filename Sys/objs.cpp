//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"

//========================================
// オブジェクトシステム
//========================================
using namespace opal;

//----------------------------------------
// デバッグ表示モード
//----------------------------------------
#ifdef OPAL_DEBUG
void OBJX::Debug( int m ){ debug = m;		}
int  OBJX::Debug( void  ){ return debug;	}
#endif

//----------------------------------------
// オブジェクト管理情報
//----------------------------------------
void  OBJX::Linx( LINX* l ){ linx =  l;		}
void  OBJX::Linx( LINX& l ){ linx = &l;		}
void  OBJX::Linx( std::shared_ptr<LINX> l ){ linx = l.get(); }
LINX* OBJX::Linx( void    ){ return linx;	}

// End Of File
