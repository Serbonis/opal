//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

#undef OPAL_TEMP

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// パラメーター
//----------------------------------------
PARAMETERS::PARAMETERS()
{
	BufferDesc.Width					= 0;
	BufferDesc.Height					= 0;
	BufferDesc.RefreshRate.Numerator	= 60;
	BufferDesc.RefreshRate.Denominator	= 1;
	BufferDesc.Format					= DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	BufferDesc.ScanlineOrdering			= DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	BufferDesc.Scaling					= DXGI_MODE_SCALING_UNSPECIFIED;
	SampleDesc.Count					= 1;
	SampleDesc.Quality					= 0;
	BufferUsage							= DXGI_USAGE_RENDER_TARGET_OUTPUT;
	BufferCount							= 1;
	OutputWindow						= 0;
	Windowed							= true;
	SwapEffect							= DXGI_SWAP_EFFECT_DISCARD;
	Flags								= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
}
PARAMETERS::~PARAMETERS(){}

DXGI_SWAP_CHAIN_DESC& PARAMETERS::operator ()( HWND wd, UINT dw, UINT dh ){

	BufferDesc.Width	= dw;
	BufferDesc.Height	= dh;
	OutputWindow		= wd;

	return *this;
}

DXGI_SWAP_CHAIN_DESC& PARAMETERS::operator ()( HWND wd ){

	RECT rc;

	::GetClientRect( wd, &rc );

	BufferDesc.Width	= rc.right  - rc.left;
	BufferDesc.Height	= rc.bottom - rc.top;
	OutputWindow		= wd;

	return *this;
}

//----------------------------------------
// デバイス
//----------------------------------------
DEVICE::DEVICE() :
	device(    nullptr ),
	debug(     nullptr ),
	context(   nullptr ),
	swapchain( nullptr ),
	texture(   nullptr ),
	target(    nullptr ),
	buffer(    nullptr ),
	stencil(   nullptr ),
	feature(),
	window(),
	param(),
	desc(),
	lost()
{}
DEVICE::~DEVICE(){}

ID3D11Device*&				DEVICE::operator()( void ){ return device;		}
ID3D11DeviceContext*&		DEVICE::operator()( UINT ){ return context;		}
ID3D11RenderTargetView*&	DEVICE::Target(     void ){ return target;		}
ID3D11DepthStencilView*&	DEVICE::Stencil(    void ){ return stencil;		}
IDXGISwapChain*&			DEVICE::SwapChain(  void ){ return swapchain;	}

const D3D_FEATURE_LEVEL DEVICE::features[] = {
#ifndef _MSC_VER
	D3D_FEATURE_LEVEL_12_1,
	D3D_FEATURE_LEVEL_12_0,
	D3D_FEATURE_LEVEL_11_1,
#endif
	D3D_FEATURE_LEVEL_11_0,
	D3D_FEATURE_LEVEL_10_1,
	D3D_FEATURE_LEVEL_10_0,
	//D3D_FEATURE_LEVEL_9_3,
	//D3D_FEATURE_LEVEL_9_2,
	//D3D_FEATURE_LEVEL_9_1,
};

UINT DEVICE::Features( void ){ return std::size( features ); }

const char* DEVICE::Features( D3D_FEATURE_LEVEL n ){

	switch ( n ) {
	case D3D_FEATURE_LEVEL_9_1  : return "9_1 ";
	case D3D_FEATURE_LEVEL_9_2  : return "9_2 ";
	case D3D_FEATURE_LEVEL_9_3  : return "9_3 ";
	case D3D_FEATURE_LEVEL_10_0 : return "10_0";
	case D3D_FEATURE_LEVEL_10_1 : return "10_1";
	case D3D_FEATURE_LEVEL_11_0 : return "11_0";
#ifndef _MSC_VER
	case D3D_FEATURE_LEVEL_11_1 : return "11_1";
	case D3D_FEATURE_LEVEL_12_0 : return "12_0";
	case D3D_FEATURE_LEVEL_12_1 : return "12_1";
#endif
	default:break;
	};
	return nullptr;
}

void DEVICE::Init( HWND wd, UINT dw, UINT dh ){

	{
		const UINT	flags =
			D3D11_CREATE_DEVICE_DEBUG|
			D3D11_CREATE_DEVICE_BGRA_SUPPORT;	// DirectX11上でDirect2Dを使用するために必要

		::D3D11CreateDevice( ( IDXGIAdapter* )nullptr,
							 D3D_DRIVER_TYPE_HARDWARE, ( HMODULE )nullptr,
							 flags,
							 features, Features(),
							 D3D11_SDK_VERSION,
							 &device,
							 &feature,
							 &context );

		IDXGIDevice1*	queryif;
		IDXGIAdapter*	adapter;
		IDXGIFactory*	factory;

		device->QueryInterface( IID_IDXGIDevice1, ( void** )&queryif );
		queryif->GetAdapter( &adapter );
		adapter->GetParent( IID_IDXGIFactory, ( void** )&factory );
		factory->CreateSwapChain( device, &param( window = wd, dw, dh ), &swapchain );

		RELEASE( factory );
		RELEASE( adapter );
		RELEASE( queryif );
	}

#if OPAL_DEBUG
	{
		const GUID	IID_D3D11Debug	= { 0x79cf2233,0x7536,0x4948,0x9d,0x36,0x1e,0x46,0x92,0xdc,0x57,0x60, };

		device->QueryInterface( IID_D3D11Debug, ( void** )&debug );
	}
#endif

	{
		swapchain->GetBuffer( 0, IID_PPV_ARGS( &texture ) );	// __uuidof( ID3D11Texture2D )
		device->CreateRenderTargetView( texture, nullptr, &target );
		texture->GetDesc( &desc );
	}

	CreateBuffer2D( buffer, CREATE_BUFFER( desc.Width, desc.Height ) );
	CreateDepthStencilView( stencil, buffer );
	CreateDepthStencilState( state.stencil );
	CreateRasterizerState(   state.raster  );

	for ( auto i = 0UL; i < BLEND_TYPE_MAX; i++ ) {
		CreateBlendState( state.blend[i], i );
	}

	lost.Clear();
}

void DEVICE::Free( void ){

	lost.DestroyDevice();
	lost.Clear();

	DestroyTexture2D( buffer );
	DestroyDepthStencilView( stencil );
	DestroyDepthStencilState( state.stencil );
	DestroyRasterizerState(   state.raster  );

	for ( auto& [n,b] : state.blend ) {
		DestroyBlendState( b );
	}

	RELEASE( texture   );
	RELEASE( target    );
	RELEASE( swapchain );
	RELEASE( context   );
#if OPAL_DEBUG
	debug->ReportLiveDeviceObjects( D3D11_RLDO_DETAIL );
	RELEASE( debug );
#endif
	RELEASE( device    );
}

void DEVICE::Begin( void ){

	context->OMSetRenderTargets( 1, &target, stencil );

	ResetState();
}

void DEVICE::End( void ){}

void DEVICE::Present( void ){

	swapchain->Present( 0, 0 );
}

UINT DEVICE::Width(  void ) const { return desc.Width;	}
UINT DEVICE::Height( void ) const { return desc.Height;	}

void DEVICE::DeviceLost( const DEVICE_NAME n, XDEVICE* d ){ lost.DeviceLost( n, d );	}
void DEVICE::DeviceLost( const DEVICE_NAME n ){ lost.DeviceLost( n );	}
UINT DEVICE::DeviceLost( void ) const { return lost.DeviceLost();	}

void DEVICE::ResetState( void ){

	ResetRasterizerState();
	ResetDepthStencilState();
	ResetBlendState();
}

void DEVICE::ResetRasterizerState(   void ){ context->RSSetState( state.raster ); }
void DEVICE::ResetDepthStencilState( void ){ context->OMSetDepthStencilState( state.stencil, 0 );	}
void DEVICE::ResetBlendState(        void ){ context->OMSetBlendState( state.blend[BLEND_TYPE0], nullptr, 0xffffffff );	}

//----------------------------------------
// ビューポート
//----------------------------------------
VIEWPORT::VIEWPORT()
{
	TopLeftX	= 0;
	TopLeftY	= 0;
	Width		= 0;
	Height		= 0;
	MinDepth	= 0.0f;
	MaxDepth	= 1.0f;
}
VIEWPORT::VIEWPORT( float w, float h )
{
	TopLeftX	= 0;
	TopLeftY	= 0;
	Width		= w;
	Height		= h;
	MinDepth	= 0.0f;
	MaxDepth	= 1.0f;
}
VIEWPORT::VIEWPORT( float x, float y, float w, float h )
{
	TopLeftX	= x;
	TopLeftY	= y;
	Width		= w;
	Height		= h;
	MinDepth	= 0.0f;
	MaxDepth	= 1.0f;
}

VIEWPORT& VIEWPORT::operator()( float w, float h ){

	Width		= w;
	Height		= h;

	return *this;
}

VIEWPORT& VIEWPORT::operator()( float x, float y, float w, float h ){

	TopLeftX	= x;
	TopLeftY	= y;
	Width		= w;
	Height		= h;

	return *this;
}

MATRIX* VIEWPORT::Matrix(  MATRIX* m ) const { return IVIEWPORT::Matrix(  m, Width, Height );	}
MATRIX* VIEWPORT::Inverse( MATRIX* m ) const { return IVIEWPORT::Inverse( m, Width, Height );	}

VIEW_PORT::VIEW_PORT() :
	port(),
	save(),
	back(),
	matrix(),
	inverse()
{}
VIEW_PORT::~VIEW_PORT(){}

void VIEW_PORT::Init( DEVICE& dv ){

	back.emplace_back( dv.Width(), dv.Height() );
}

void VIEW_PORT::Free( void ){

	back.clear();
}

void VIEW_PORT::Begin( void ){ save = port = back.front();	}
void VIEW_PORT::End( void ){ back.resize( 1 ); }

void VIEW_PORT::Begin( DEVICE& dv ){

	const auto	context = dv(0);

	back.emplace_back( save );
	context->RSSetViewports( 1, &port );
	save = port;

	port.Matrix(  &matrix  );
	port.Inverse( &inverse );
}

void VIEW_PORT::End( DEVICE& dv ){

	const auto	context = dv(0);

	context->RSSetViewports( 1, &back.back() );
	back.pop_back();
}

const MATRIX* VIEW_PORT::Matrix(  void ) const { return &matrix;	}
const MATRIX* VIEW_PORT::Inverse( void ) const { return &inverse;	}

void VIEW_PORT::Set( const SRECTF& v ){

	port.TopLeftX	= v.x;
	port.TopLeftY	= v.y;
	port.Width		= v.w;
	port.Height		= v.h;
}
void VIEW_PORT::Set( float x, float y, float w, float h ){

	port.TopLeftX	= x;
	port.TopLeftY	= y;
	port.Width		= w;
	port.Height		= h;
}
void VIEW_PORT::Set( const VECTOR2& p, float w, float h ){

	port.TopLeftX	= p.x;
	port.TopLeftY	= p.y;
	port.Width		= w;
	port.Height		= h;
}
void VIEW_PORT::Set( float x, float y, const SSIZE& s ){

	port.TopLeftX	= x;
	port.TopLeftY	= y;
	port.Width		= s.w;
	port.Height		= s.h;
}
void VIEW_PORT::Set( const VECTOR2& p, const SSIZE& s ){

	port.TopLeftX	= p.x;
	port.TopLeftY	= p.y;
	port.Width		= s.w;
	port.Height		= s.h;
}
void VIEW_PORT::Set( void ){

	port.TopLeftX	= back.front().TopLeftX;
	port.TopLeftY	= back.front().TopLeftY;
	port.Width		= back.front().Width;
	port.Height		= back.front().Height;
}
auto VIEW_PORT::Get( void ) const->SRECTF{

	return {port.TopLeftX,
			port.TopLeftY,
			port.Width,
			port.Height};
}

void VIEW_PORT::SetPosition( float x, float y ){

	port.TopLeftX	= x;
	port.TopLeftY	= y;
}
void VIEW_PORT::SetPosition( const VECTOR2& p ){

	port.TopLeftX	= p.x;
	port.TopLeftY	= p.y;
}
void VIEW_PORT::SetPosition( void ){

	port.TopLeftX	= back.front().TopLeftX;
	port.TopLeftY	= back.front().TopLeftY;
}
auto VIEW_PORT::GetPosition( void ) const->VECTOR2{

	return {port.TopLeftX,
			port.TopLeftY};
}

void VIEW_PORT::SetSize( float w, float h ){

	port.Width		= w;
	port.Height		= h;
}
void VIEW_PORT::SetSize( const SSIZE& s ){

	port.Width		= s.w;
	port.Height		= s.h;
}
void VIEW_PORT::SetSize( void ){

	port.Width		= back.front().Width;
	port.Height		= back.front().Height;
}
auto VIEW_PORT::GetSize( void ) const->SSIZE{

	return{port.Width,
		   port.Height};
}

//----------------------------------------
// シザー
//----------------------------------------
SCISSOR::SCISSOR()
{
	top		= 0;	bottom	= 0;
	left	= 0;	right	= 0;
}
SCISSOR::SCISSOR( float w, float h )
{
	top		= 0;	bottom	= h;
	left	= 0;	right	= w;
}
SCISSOR::SCISSOR( float x, float y, float w, float h )
{
	top		= y;	bottom	= y + h;
	left	= x;	right	= x + w;
}

void SCISSOR::Clear( DEVICE& dv ){

	const auto	context = dv(0);
	const auto	scissor	= SCISSOR{0,0,( float )device.Width(),( float )device.Height()};

	context->RSSetScissorRects( 1, &scissor );
}

void SCISSOR::Set( const SRECTF& v ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{v.x,v.y,v.w,v.h};

	context->RSSetScissorRects( 1, &scissor );
}
void SCISSOR::Set( float x, float y, float w, float h ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{x,y,w,h};

	context->RSSetScissorRects( 1, &scissor );
}
void SCISSOR::Set( const VECTOR2& p, float w, float h ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{p.x,p.y,w,h};

	context->RSSetScissorRects( 1, &scissor );
}
void SCISSOR::Set( float x, float y, const SSIZE& s ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{x,y,s.w,s.h};

	context->RSSetScissorRects( 1, &scissor );
}
void SCISSOR::Set( const VECTOR2& p, const SSIZE& s ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{p.x,p.y,s.w,s.h};

	context->RSSetScissorRects( 1, &scissor );
}
void SCISSOR::Set( void ){

	const auto	context = device(0);
	const auto	scissor	= SCISSOR{0,0,( float )device.Width(),( float )device.Height()};

	context->RSSetScissorRects( 1, &scissor );
}
auto SCISSOR::Get( void )->SRECTF{

	const auto	context = device(0);
	UINT	number;
	SCISSOR	scissor;

	context->RSGetScissorRects( &number, &scissor );

	return {( float )scissor.top,
			( float )scissor.left,
			( float )scissor.bottom	- scissor.top,
			( float )scissor.right	- scissor.left};
}

//----------------------------------------
// バッファクリア
//----------------------------------------
CLEAR_BUFFER::CLEAR_BUFFER() :
	color{0,0,0,1}
{}
CLEAR_BUFFER::~CLEAR_BUFFER(){}

void CLEAR_BUFFER::Init( DEVICE& dv ){

	color[0] =	( float )OPAL::BGCOLOR_R()/0xff;
	color[1] =	( float )OPAL::BGCOLOR_G()/0xff;
	color[2] =	( float )OPAL::BGCOLOR_B()/0xff;
	color[3] =	1.0f;

	Clear( dv );
}
void CLEAR_BUFFER::Free( void ){}

void CLEAR_BUFFER::Clear( DEVICE& dv, ID3D11RenderTargetView*& target, ID3D11DepthStencilView*& stencil ){

	const auto	context = dv(0);

	context->ClearRenderTargetView( target,  color );
	context->ClearDepthStencilView( stencil, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0 );
}
void CLEAR_BUFFER::Clear( DEVICE& dv, ID3D11DepthStencilView*& stencil ){ Clear( dv, dv.Target(), stencil ); }
void CLEAR_BUFFER::Clear( DEVICE& dv, ID3D11RenderTargetView*& target  ){ Clear( dv, target, dv.Stencil() ); }
void CLEAR_BUFFER::Clear( DEVICE& dv ){ Clear( dv, dv.Target(), dv.Stencil() ); }

COLOR CLEAR_BUFFER::Color( void ) const {

	const auto	r = 0xff * color[0];
	const auto	g = 0xff * color[1];
	const auto	b = 0xff * color[2];

	return COLOR( r, g, b, 1.0f );
}

void  CLEAR_BUFFER::Color( COLOR c ){

	color[0] =	( float )c.GetColorR()/0xff;
	color[1] =	( float )c.GetColorG()/0xff;
	color[2] =	( float )c.GetColorB()/0xff;
	color[3] =	1.0f;
}

void  CLEAR_BUFFER::Color( UCHAR r, UCHAR g, UCHAR b ){

	color[0] =	( float )r/0xff;
	color[1] =	( float )g/0xff;
	color[2] =	( float )b/0xff;
	color[3] =	1.0f;
}

CLEAR_BUFFER::operator float*( void ){ return color;	}
float& CLEAR_BUFFER::operator []( UINT n ){ return color[n%std::size( color )];	}

// End Of File
