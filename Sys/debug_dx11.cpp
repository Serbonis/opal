//========================================
// OPAL Game Library
//========================================
#include "debug_dx11.hpp"

#include <d2d1.h>
#include <directxcolors.h>

//========================================
// グラフィックシステム
//========================================
using namespace opal;
using namespace opal::DEBUGX_DX11;

//----------------------------------------
//
//----------------------------------------
namespace opal::DEBUGX_ {
	extern HWND DebugHandle( void );
	extern auto DebugWindow( void )->std::pair<UINT,UINT>;
	extern auto DebugPrintW( void )->const wchar_t*;
}

//----------------------------------------
//
//----------------------------------------
namespace {
	ID3D11Device*			Device;
	ID3D11DeviceContext*	Context;
	IDXGISwapChain*			SwapChain;
	ID3D11RenderTargetView*	RenderTargetView;

	std::pair<UINT,UINT>	WindowRect;

	extern void init_font( void );
	extern void free_font( void );
	extern void exec_font( void );
}

//----------------------------------------
// システム
//----------------------------------------
namespace opal::DEBUGX_DX11 {
	void SysInit( void ){
		//----------------------------------------
		// デバイスの生成
		//----------------------------------------
		const D3D_FEATURE_LEVEL	features[] = {
#ifndef _MSC_VER
			D3D_FEATURE_LEVEL_12_1,
			D3D_FEATURE_LEVEL_12_0,
			D3D_FEATURE_LEVEL_11_1,
#endif
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		D3D_FEATURE_LEVEL		feature;

		::D3D11CreateDevice( ( IDXGIAdapter* )nullptr,
							 D3D_DRIVER_TYPE_HARDWARE, ( HMODULE )nullptr,
							 D3D11_CREATE_DEVICE_BGRA_SUPPORT|D3D11_CREATE_DEVICE_DEBUG,
							 features, std::size( features ),
							 D3D11_SDK_VERSION,
							 &Device,
							 &feature,
							 &Context );


		//----------------------------------------
		// ウインドウの情報
		//----------------------------------------
		const HWND handle	= opal::DEBUGX_::DebugHandle();
		const auto [w,h]	= opal::DEBUGX_::DebugWindow();

		WindowRect = { w, h };

		//----------------------------------------
		// スワップチェインの作成
		//----------------------------------------
		{
			IDXGIFactory1*	dxgi_factory;

			{
				IDXGIDevice*	dxgi_device;

				Device->QueryInterface( __uuidof( IDXGIDevice ), reinterpret_cast<void**>( &dxgi_device ) );

				IDXGIAdapter* dxgi_adapter;

				dxgi_device->GetAdapter( &dxgi_adapter );

				dxgi_adapter->GetParent( __uuidof( IDXGIFactory1 ), reinterpret_cast<void**>( &dxgi_factory ) );

				RELEASE( dxgi_adapter );
				RELEASE( dxgi_device );
			}

			{
				DXGI_SWAP_CHAIN_DESC sd = {};

				sd.BufferCount = 1;
				sd.BufferDesc.Width = w;
				sd.BufferDesc.Height = h;
				sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				sd.BufferDesc.RefreshRate.Numerator = 60;
				sd.BufferDesc.RefreshRate.Denominator = 1;
				sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				sd.OutputWindow = handle;
				sd.SampleDesc.Count = 1;
				sd.SampleDesc.Quality = 0;
				sd.Windowed = true;

				dxgi_factory->CreateSwapChain( Device, &sd, &SwapChain );
				dxgi_factory->MakeWindowAssociation( handle, DXGI_MWA_NO_ALT_ENTER );
			}

			RELEASE( dxgi_factory );
		}

		//----------------------------------------
		// レンダーターゲットの作成
		//----------------------------------------
		{
			ID3D11Texture2D* back_buffer;

			SwapChain->GetBuffer(0, __uuidof( ID3D11Texture2D ), reinterpret_cast<void**>( &back_buffer ) );
			Device->CreateRenderTargetView( back_buffer, nullptr, &RenderTargetView );

			RELEASE( back_buffer );

			Context->OMSetRenderTargets( 1, &RenderTargetView, nullptr );
		}

		//----------------------------------------
		// ビューポートの設定
		//----------------------------------------
		{
			D3D11_VIEWPORT vp;

			vp.Width	= ( FLOAT )w;
			vp.Height	= ( FLOAT )h;
			vp.MinDepth = 0.0f;
			vp.MaxDepth = 1.0f;
			vp.TopLeftX = 0;
			vp.TopLeftY = 0;

			Context->RSSetViewports( 1, &vp );
		}

		//----------------------------------------
		// フォント
		//----------------------------------------
		init_font();
	}

	void SysFree( void ){

		free_font();

		RELEASE( RenderTargetView );
		RELEASE( SwapChain );
		RELEASE( Context );
		RELEASE( Device );
	}

	void SysExec( void ){

		Context->ClearRenderTargetView( RenderTargetView, DirectX::Colors::Black );

		exec_font();

		SwapChain->Present( 0, 0 );
	}
}

//----------------------------------------
// フォント
//----------------------------------------
#include <dwrite.h>

namespace {
	ID2D1Factory*		D2DFactory;
	IDXGISurface*		BackBuffer;
	ID2D1RenderTarget* 	RenderTarget;

	IDWriteFactory*		WriteFactory;

	IDWriteTextFormat*		TextFormat;
	ID2D1SolidColorBrush* 	SolidBrush;

	void init_font( void ){
		//----------------------------------------
		// Direct2Dの初期化
		//----------------------------------------
		D2D1CreateFactory( D2D1_FACTORY_TYPE_SINGLE_THREADED, &D2DFactory );

		SwapChain->GetBuffer( 0, IID_PPV_ARGS( &BackBuffer ) );

		const D2D1_RENDER_TARGET_PROPERTIES props = []( auto d2d_factory ){

			FLOAT dpi_x;
			FLOAT dpi_y;

			d2d_factory->GetDesktopDpi( &dpi_x, &dpi_y );

			return D2D1::RenderTargetProperties( D2D1_RENDER_TARGET_TYPE_DEFAULT,
										  D2D1::PixelFormat( DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED ),
										  dpi_x, dpi_y );
		}( D2DFactory );

		D2DFactory->CreateDxgiSurfaceRenderTarget( BackBuffer, &props, &RenderTarget );

		//----------------------------------------
		// DirectWriteの初期化
		//----------------------------------------
		DWriteCreateFactory( DWRITE_FACTORY_TYPE_SHARED, __uuidof( IDWriteFactory ), reinterpret_cast<IUnknown * *>( &WriteFactory ) );

		//----------------------------------------
		// フォントの設定
		//----------------------------------------
		WriteFactory->CreateTextFormat( L"ＭＳ 明朝",
										nullptr,
										DWRITE_FONT_WEIGHT_NORMAL,
										DWRITE_FONT_STYLE_NORMAL,
										DWRITE_FONT_STRETCH_NORMAL,
										48,
										L"",
										&TextFormat );

		TextFormat->SetTextAlignment( DWRITE_TEXT_ALIGNMENT_LEADING );

		RenderTarget->CreateSolidColorBrush( D2D1::ColorF( D2D1::ColorF::Lime ), &SolidBrush );
	}

	void free_font( void ){

		RELEASE( SolidBrush );
		RELEASE( TextFormat );

		RELEASE( WriteFactory );

		RELEASE( RenderTarget );
		RELEASE( BackBuffer );
		RELEASE( D2DFactory );
	}

	void exec_font( void ){

		const auto	text	= std::wstring{opal::DEBUGX_::DebugPrintW()};
		const auto&	[w,h]	= WindowRect;

		RenderTarget->BeginDraw();
		RenderTarget->DrawText( text.c_str(), text.length(), TextFormat, D2D1::RectF(0,0,w,h), SolidBrush, D2D1_DRAW_TEXT_OPTIONS_NONE );
		RenderTarget->EndDraw();
	}
}

// End Of File
