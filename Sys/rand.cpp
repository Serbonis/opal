//========================================
// OPAL Game Library
//========================================
#include "Rand.hpp"

//========================================
// 乱数生成
//========================================
using namespace opal;

//----------------------------------------
// システム変数
//----------------------------------------
namespace {
	std::mt19937	 	mt{};
	long long			sd{};
}

//----------------------------------------
// シード
//----------------------------------------
namespace opal {
	long long Seed( void ){ return sd; }
	void  Srand( UINT n ){ mt.seed( ( n == 0 ) ? sd = ::clock() : sd );	}
}

//----------------------------------------
// int
//----------------------------------------
namespace opal {
	UINT  Urand( void ){ return ( UINT )Irand(); 		}
	UINT  Urand( UINT n, UINT m ){ return ( UINT )Irand( n, m );	}

	int   Irand( void ){ return mt(); }
	int   Irand( int n, int m ){

		if ( n == m ) {
			return n;
		}

		if ( n > m ) {
			std::swap( n, m );
		}

		std::uniform_int_distribution<int>	dist( n, m );

		return dist( mt );
	}
}

//----------------------------------------
// float
//----------------------------------------
namespace opal {
	float Frand( void ){ return mt(); }
	float Frand( float n, float m ){

		if ( FEQUAL( n, m ) ) {
			return n;
		}

		if ( n > m ) {
			std::swap( n, m );
		}

		std::uniform_real_distribution<float>	dist( n, m );

		return dist( mt );
	}
}

//----------------------------------------
// 数列
//----------------------------------------
namespace opal {
	void Vrand( std::vector<int>& v ){

#ifndef _MSC_VER
		std::iota( v.begin(), v.end(), 0 );
#else
		const auto	n = v.size();

		for ( auto i = 0UL; i < n; i++ ) {
			v[i] = i;
		}
#endif
		std::shuffle( v.begin(), v.end(), std::mt19937(::clock()) );
	}

	void Vrand( std::vector<int>& v, int s ){

		v.resize( s );
		Vrand( v );
	}

	auto Vrand( int s )->std::vector<int>{

		std::vector<int>	v;

		Vrand( v, s );

		return std::move( v );
	}
}

//----------------------------------------
// 重み付き
//----------------------------------------
namespace opal {
	UINT Drand( const std::vector<double>& v ){

		std::discrete_distribution<std::size_t>	dist( v.begin(), v.end() );

		return dist( mt );
	}
}

// End Of File
