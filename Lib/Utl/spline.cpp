//========================================
// OPAL Game Library
//========================================
#include "Utl/spline.hpp"

//========================================
// スプライン
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
template<typename TYPE>SPLINE<TYPE>::SPLINE() :
	s( 0 ),
	a{},
	b{},
	c{},
	d{}
{}
template<typename TYPE>SPLINE<TYPE>::~SPLINE(){}

//----------------------------------------
// 係数
//----------------------------------------
template<typename TYPE>void SPLINE<TYPE>::Coefficient( const std::vector<TYPE>& p ){

	s = p.size();
	a.resize( s );
	b.resize( s );
	c.resize( s );
	d.resize( s );

	const auto	n = s-1;

	for ( auto i = 0UL; i < s; i++ ) {
		a[i] = p[i];
	}

	c[0] = c[n] = TYPE{};
	for ( auto i = 1UL; i < n; i++ ) {
		c[i] = 3.0f * ( a[i-1] - 2.0f * a[i] + a[i+1] );
	}

	std::vector<float>	w( s );

	w[0] = 0.0f;
	for ( auto i = 1UL; i < n; i++ ) {
		const auto	t = 4.0f - w[i-1];

		c[i] = (c[i] - c[i-1])/t;
		w[i] = 1.0f/t;
	}

	for ( auto i = n; i > 0UL; i-- ) {
		c[i] = c[i] - c[i+1] * w[i];
	}

	b[n] = d[n] = TYPE{};
	for ( auto i = 0UL; i < n; i++ ) {
		d[i] = (c[i+1] - c[i])/3.0f;
		b[i] =  a[i+1] - a[i] - c[i] - d[i];
	}
}

//----------------------------------------
// 補間
//----------------------------------------
template<typename TYPE>TYPE SPLINE<TYPE>::Interpolate( float t ) const {

	const auto	n = ARRAYROUND( std::floor( t ), s-1 );
	const auto	r = t - n;

	return a[n] + ( b[n] + ( c[n] + ( d[n] ) * r ) * r ) * r;
}

//----------------------------------------
// 明示的インスタンス化
//----------------------------------------
#include "Ext/vector.hpp"

template class SPLINE<float>;
template class SPLINE<VECTOR2>;
template class SPLINE<VECTOR3>;

// End Of File
