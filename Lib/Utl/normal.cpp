//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Utl/degree.hpp"
#include "Utl/normal.hpp"

using namespace opal;

//========================================
// ���K��
//========================================
//----------------------------------------
// ���K��
//----------------------------------------
namespace opal {
	float Normal( float v, float n ){

		if ( !FZERO( n ) ) {
			return ( v > 0 ) ? ( int( v/n + 0.5f ) * n ) : ( int( v/n - 0.5f ) * n );
		}
		return 0.0f;
	}
}

//----------------------------------------
// ��]�̐��K��( 0���`360��)
//----------------------------------------
namespace opal {
	float NormalRadian360( float r ){

		auto	f = ::fmodf( r, OPAL_PI*2 );

		if ( f < 0.0f ) {
			f += OPAL_PI*2;
		}
		return f;
	}

	float NormalDegree360( float d ){

		auto	f = ::fmodf( d, 360.0f );

		if ( f < 0.0f ) {
			f += 360.0f;
		}
		return f;
	}
}

//----------------------------------------
// ��]�̐��K��( -180���`+180��)
//----------------------------------------
namespace opal {
	float NormalRadian180( float r ){

		auto	f = ::fmodf( r, OPAL_PI*2 );

		if ( f < -OPAL_PI ) return f + OPAL_PI*2;
		if ( f > +OPAL_PI ) return f - OPAL_PI*2;

		return f;
	}

	float NormalDegree180( float d ){

		auto	f = ::fmodf( d, 360.0f );

		if ( f < -180.0f ) return f + 360.0f;
		if ( f > +180.0f ) return f - 360.0f;

		return f;
	}
}

//----------------------------------------
// �W�����ړ����x���K��
//----------------------------------------
namespace opal {
	bool NormalSpeed( UINT d, float& x, float& y ){

		constexpr auto sq2 = OPAL_SQ2;

		switch ( d ) {
		case DIR_N:	x =  0.0f;	y = +1.0f;	return true;
		case DIR_E:	x = +1.0f;	y =  0.0f;	return true;
		case DIR_S:	x =  0.0f;	y = -1.0f;	return true;
		case DIR_W:	x = -1.0f;	y =  0.0f;	return true;
		case DIR_NE:x = +1/sq2;	y = +1/sq2;	return true;
		case DIR_NW:x = -1/sq2;	y = +1/sq2;	return true;
		case DIR_SE:x = +1/sq2;	y = -1/sq2;	return true;
		case DIR_SW:x = -1/sq2;	y = -1/sq2;	return true;
		default:break;
		}

		return false;
	}
}

//----------------------------------------
// 2�������W��1�����z��
//----------------------------------------
namespace opal {
	UINT Point2Index( const UINT x, const UINT y, const UINT w, const UINT h ){

		return ( y % h ) * w + ( x % w );
	}

	UINT Point2Index( const UINT x, const UINT y, const UINT w ){

		return y * w + ( x % w );
	}
}

// End Of File
