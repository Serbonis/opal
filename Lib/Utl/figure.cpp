//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Utl/figure.hpp"
#include "Ext/xmath.hpp"

using namespace opal;

//========================================
// �}�`
//========================================
//----------------------------------------
// ������
//----------------------------------------
namespace opal {
	VECTOR3 EquationLine( const VECTOR2& p1, const VECTOR2& p2 ){

		const auto	a = +(p2.y - p1.y);
		const auto	b = -(p2.x - p1.x);
		const auto	c = -(a*p1.x + b*p1.y);

		return VECTOR3{ a, b, c };
	}

	VECTOR3 EquationCircle( const VECTOR2& p1, const VECTOR2& p2, const VECTOR2& p3 ){

		const auto	c = VECTOR4( p1.x*p1.x + p1.y*p1.y,
								 p2.x*p2.x + p2.y*p2.y,
								 p3.x*p3.x + p3.y*p3.y,
								 0.0f );
		const auto	m = MATRIX( p1.x, p1.y, 1.0f, 0.0f,
								p2.x, p2.y, 1.0f, 0.0f,
								p3.x, p3.y, 1.0f, 0.0f,
								0.0f, 0.0f, 0.0f, 0.0f );
		const auto	v = MATRIX::Inverse( m ) * c;
		const auto	x = v.x/2;
		const auto	y = v.y/2;
		const auto	r = x*x + y*y + v.z;

		return VECTOR3{ -x, -y, r };
	}
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	extern float DistancePoint( const VECTOR2& pp ){

		return DistancePointPoint( pp, VECTOR2{0,0} );
	}

	extern float DistancePointPoint( const VECTOR2& sp, const VECTOR2& ep ){

		const auto	dx = ep.x - sp.x;
		const auto	dy = ep.y - sp.y;

		return std::sqrt( dx*dx + dy*dy );
	}

	extern float DistancePointLine( const VECTOR2& pp, const VECTOR2& sp, const VECTOR2& ep ){

		if ( sp == ep ) {
			return DistancePointPoint( sp, pp );
		} else {
			const auto  v = EquationLine( sp, ep );
			const auto&	a = v.x;
			const auto&	b = v.y;
			const auto&	c = v.z;

			return ( a*pp.x+b*pp.y+c )/std::sqrt( a*a+b*b );
		}
	}

	extern float DistancePoint2( const VECTOR2& pp ){

		return DistancePointPoint2( pp, VECTOR2{0,0} );
	}

	extern float DistancePointPoint2( const VECTOR2& sp, const VECTOR2& ep ){

		const auto	dx = ep.x - sp.x;
		const auto	dy = ep.y - sp.y;

		return dx*dx + dy*dy;
	}

	extern float DistancePointLine2( const VECTOR2& pp, const VECTOR2& sp, const VECTOR2& ep ){

		if ( sp == ep ) {
			return DistancePointPoint2( sp, pp );
		} else {
			const auto  v = EquationLine( sp, ep );
			const auto&	a = v.x;
			const auto&	b = v.y;
			const auto&	c = v.z;
			const auto	r = a*pp.x+b*pp.y+c;

			return ( r*r )/( a*a+b*b );
		}
	}
}

//----------------------------------------
// �p�x
//----------------------------------------
namespace opal {
	float AngleLine( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	va = ea-sa;
		const auto	vb = eb-sb;
		const auto	la = float{va};
		const auto	lb = float{vb};

		return ( FZERO( la ) || FZERO( lb ) ) ? 0 : std::atan2( VectorCross( va, vb ), VectorDot( va, vb ) );
	}

	float AngleLine( const VECTOR2& sx, const VECTOR2& ea, const VECTOR2& eb ){

		return AngleLine( sx, ea, sx, eb );
	}
}

//----------------------------------------
// �P��/����
//----------------------------------------
namespace opal {
	VECTOR2 UnitLine( const VECTOR2& sp, const VECTOR2& ep ){

		const auto	d = ep - sp;

		return bool{d} ? VECTOR2{} : d/DistancePoint( d );
	}

	VECTOR2 OrthLine( const VECTOR2& sp, const VECTOR2& ep ){

		const auto	e = UnitLine( sp, ep );

		return VECTOR2{ -e.y, +e.x };
	}
}

//----------------------------------------
// �ʒu
//----------------------------------------
namespace {
	inline auto relation( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb )->std::tuple<float,float,float>{

		const auto	a = ea - sa;
		const auto	b = eb - sb;
		const auto	d = sb - sa;

		return {VectorCross( b, a ),VectorCross( b, d ),VectorCross( a, d )};
	}
}
namespace opal {
	std::vector<float> RelationLineLine( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			if ( FZERO( s ) && FZERO( t ) ) {
				return std::vector<float>{DistancePointLine2(sb,sa,ea)};
			}
			return std::vector<float>{};
		}
		return std::vector<float>{s/m,t/m};
	}

	bool RelationLineLineAA( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			return ( FZERO( s ) && FZERO( t ) ) ? true : false;
		}

		return true;
	}

	bool RelationLineLineAR( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			if ( ( FZERO( s ) && FZERO( t ) ) ) {
				return true;
			}
			return false;
		}

		return ( 0 < m )
			? ( 0 <= t )
			: ( t <= 0 );
	}

	bool RelationLineLineAS( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			if ( FZERO( s ) && FZERO( t ) ) {
				return true;
			}
			return false;
		}

		return ( 0 < m )
			? ( 0 <= t && t <= m )
			: ( m <= t && t <= 0 );
	}

	bool RelationLineLineRR( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			if ( FZERO( s ) && FZERO( t ) ) {
				return (( SequencePoint( sa, ea, sb ) >= 0 ) ||
						( SequencePoint( sb, eb, sa ) >= 0 )) ?
					true : false;
			}
			return false;
		}

		return ( 0 < m )
			? ( 0 <= s && 0 <= t )
			: ( s <= 0 && t <= 0 );
	}

	bool RelationLineLineRS( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	[m,s,t] = relation( sa, ea, sb, eb );

		if ( FZERO( m ) ) {
			if ( FZERO( s ) && FZERO( t ) ) {
				return (( SequencePoint( sa, ea, sb ) >= 0 ) ||
						( SequencePoint( sa, ea, eb ) >= 0 )) ?
					true : false;
			}
			return false;
		}

		return ( 0 < m )
			? ( 0 <= s && ( 0 <= t && t <= m ) )
			: ( s <= 0 && ( m <= t && t <= 0 ) );
	}

	bool RelationLineLineSS( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto	a = ea - sa;
		const auto	b = eb - sb;
		const auto	m = VectorCross( b, a );

		const auto	d = sb - sa;
		const auto	s = VectorCross( b, d );
		const auto	t = VectorCross( a, d );

		if ( FZERO( m ) ) {
			if ( FZERO( s ) && FZERO( t ) ) {
				return (( !SequencePoint( sa, ea, sb ) ) ||
						( !SequencePoint( sa, ea, eb ) ) ||
						( !SequencePoint( sb, eb, sa ) ) ||
						( !SequencePoint( sb, eb, ea ) )) ?
					true : false;
			}
			return false;
		}

		return ( 0 < m )
			? ( ( 0 <= s && s <= m ) && ( 0 <= t && t <= m ) )
			: ( ( m <= s && s <= 0 ) && ( m <= t && t <= 0 ) );
	}
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	int SequencePoint( const VECTOR2& sp, const VECTOR2& ep, const VECTOR2& pp ){

		if ( sp == ep ) {
			return ( sp == pp ) ? 0 : 1;
		} else if ( pp == sp ) {
			return 0;
		} else if ( pp == ep ) {
			return 1;
		} else {
			if ( VectorDot( pp-sp, ep-sp ) < 0 ) { return -1;	}
			if ( VectorDot( sp-pp, ep-pp ) < 0 ) { return  0;	}
			if ( VectorDot( pp-ep, sp-ep ) < 0 ) { return +1;	}
		}
		return -1;
	}
}

//----------------------------------------
// ��_
//----------------------------------------
namespace opal {
	std::vector<VECTOR2> IntersectionLineLine( const VECTOR2& sa, const VECTOR2& ea, const VECTOR2& sb, const VECTOR2& eb ){

		const auto  v1 = EquationLine( sa, ea );
		const auto  v2 = EquationLine( sb, eb );
		const auto&	a = v1.x;
		const auto&	b = v1.y;
		const auto&	c = v1.z;
		const auto&	p = v2.x;
		const auto&	q = v2.y;
		const auto&	r = v2.z;
		const auto	m = b*p-a*q;
		const auto	s = c*q-r*b;;
		const auto	t = c*p-r*a;

		std::vector<VECTOR2>	v;

		if ( !FZERO( m ) ) {
			v.emplace_back( s/m, t/m );
		} else if ( FZERO( s ) || FZERO( t ) ) {
			v.push_back( sb );
			v.push_back( eb );
		}

		return v;
	}

	std::vector<VECTOR2> IntersectionLineCircle( const VECTOR2& sp, const VECTOR2& ep, const VECTOR2& cp, float cr ){

		const auto	v1 = EquationLine( sp, ep );
		const auto	v2 = VECTOR3{cp,cr};
		const auto&	a = v1.x;
		const auto&	b = v1.y;
		const auto&	c = v1.z;
		const auto&	x = v2.x;
		const auto&	y = v2.y;
		const auto&	r = v2.z;
		const auto	l = a*a+b*b;
		const auto	k = a*x+b*y+c;
		const auto	d = l*r*r-k*k;

		std::vector<VECTOR2>	v;

		const auto	al = a/l;
		const auto	bl = b/l;
		const auto	px = x-al*k;
		const auto	py = y-bl*k;

		if ( FZERO( d ) ) {
			v.emplace_back( px, py );
		} else if ( d > 0 ) {
			const auto	ds = std::sqrt( d );
			const auto	qx = bl*ds;
			const auto	qy = al*ds;

			v.emplace_back( px-qx, py+qy );
			v.emplace_back( px+qx, py-qy );
		}

		return v;
	}
}

// End Of File
