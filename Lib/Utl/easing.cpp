//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Utl/easing.hpp"

//========================================
// イージング
//========================================
using namespace opal;

//----------------------------------------
// EASING
//----------------------------------------
namespace opal::EASING {
	float easing( FUNC f, float c, float t ){

		return f ? f( c, t ) : 0.0f;
	}
}

//----------------------------------------
// LINEAR
//----------------------------------------
namespace opal::EASING {
	float linear( float c, float t ) {

		return ( c > 0 && t > 0 ) ? c/t : 0;
	};
}

//----------------------------------------
// QUADRATIC
//----------------------------------------
namespace opal::EASING {
	float quadratic_in( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return r * r;
		}
		return 0;
	}

	float quadratic_out( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return -r * (r-2);
		}
		return 0;
	}

	float quadratic_inout( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			auto	r = 2 * c/t;

			if ( r < 1 ) {
				return +0.5f * r * r;
			} else {
				r -= 1;
				return -0.5f * ( r * (r-2) - 1 );
			}
		}
		return 0;
	}
}

//----------------------------------------
// CUBIC
//----------------------------------------
namespace opal::EASING {
	float cubic_in( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return r * r * r;
		}
		return 0;
	}

	float cubic_out( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t - 1;

			return 1 + r * r * r;
		}
		return 0;
	}

	float cubic_inout( float c, float t ) {

		if ( c > 0 && t > 0 ) {
			auto	r = 2 * c/t;

			if ( r < 1 ) {
				return +0.5f * r * r * r;
			} else {
				r -= 2;
				return -0.5f * ( r * r * r + 2 );
			}
		}
		return 0;
	}
}

//----------------------------------------
// QUARTIC
//----------------------------------------
namespace opal::EASING {
	float quartic_in( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return r * r * r * r;
		}
		return 0;
	}

	float quartic_out( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t - 1;

			return 1 - r * r * r * r;
		}
		return 0;
	}

	float quartic_inout( float c, float t ){

		if ( c > 0 && t > 0 ) {
			auto	r = 2 * c/t;

			if ( r < 1 ) {
				return +0.5f * r * r * r * r;
			} else {
				r -= 2;
				return -0.5f * ( r * r * r * r - 2 );
			}
		}
		return 0;
	}
}

//----------------------------------------
// QUINTIC
//----------------------------------------
namespace opal::EASING {
	float quintic_in( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return r * r * r * r * r;
		}
		return 0;
	}

	float quintic_out( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t - 1;

			return 1 + r * r * r * r * r;
		}
		return 0;
	}

	float quintic_inout( float c, float t ){

		if ( c > 0 && t > 0 ) {
			auto	r = 2 * c/t;

			if ( r < 1 ) {
				return +0.5f * r * r * r * r * r;
			} else {
				r -= 2;
				return +0.5f * ( r * r * r * r * r + 2 );
			}
		}
		return 0;
	}
}

//----------------------------------------
// SINUSOIDAL
//----------------------------------------
namespace opal::EASING {
	float sinusoidal_in(    float c, float t ){ return ( c > 0 && t > 0 ) ? -std::cos( c/t * OPAL_PI/2 ) + 1 : 0;	}
	float sinusoidal_out(   float c, float t ){ return ( c > 0 && t > 0 ) ? +std::sin( c/t * OPAL_PI/2 ) + 0 : 0;	}
	float sinusoidal_inout( float c, float t ){ return ( c > 0 && t > 0 ) ? -0.5f * ( std::cos( c/t * OPAL_PI ) - 1 ) : 0;	}
}

//----------------------------------------
// EXPONENTIAL
//----------------------------------------
namespace opal::EASING {
	float exponential_in(    float c, float t ){ return ( c > 0 && t > 0 ) ? +std::pow( 2, +10 * ( c/t - 1 ) ) + 0 : 0;	}
	float exponential_out(   float c, float t ){ return ( c > 0 && t > 0 ) ? -std::pow( 2, -10 * ( c/t - 0 ) ) + 1 : 0;	}
	float exponential_inout( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = 2 * c/t - 1;

			return ( r < 0 )
				? +0.5f * std::pow( 2, +10 * r ) + 0
				: -0.5f * std::pow( 2, -10 * r ) + 1;
		}
		return 0;
	}
}

//----------------------------------------
// CIRCULAR
//----------------------------------------
namespace opal::EASING {
	float circular_in( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t;

			return -std::sqrt( 1 - r*r ) + 1;
		}
		return 0;
	}

	float circular_out( float c, float t ){

		if ( c > 0 && t > 0 ) {
			const auto	r = c/t - 1;

			return +std::sqrt( 1 - r*r );
		}
		return 0;
	}

	float circular_inout( float c, float t ){

		if ( c > 0 && t > 0 ) {
			auto	r = 2 * c/t;

			if ( r < 1 ) {
				return -0.5f * ( std::sqrt( 1 - r * r ) - 1 );
			} else {
				r -= 2;
				return +0.5f * ( std::sqrt( 1 - r * r ) + 1 );
			}
		}
		return 0;
	}
}

// End Of File
