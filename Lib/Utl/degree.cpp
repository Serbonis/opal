//========================================
// OPAL Game Library
//========================================
#include "Utl/degree.hpp"
#include "Utl/normal.hpp"

using namespace opal;

//========================================
// �p�x
//========================================
//----------------------------------------
// �ی�����
//----------------------------------------
namespace opal {
	template<int n, typename DEGREE>static inline int divide( DEGREE d ){

		return ( static_cast<int>( NormalDegree360( d ) * n / 180.0f ) + 1 ) / 2 % n;
	}

	int Quadrant( float d ){ return divide<4>( d ); }
	int Octant(   float d ){ return divide<8>( d ); }

	int Quadrant( float x, float y ){

		if ( !FZERO( x ) || !FZERO( y ) ) {
			if ( std::abs( y ) > std::abs( x ) ) { return ( y > 0 ) ? DIQ_0 : DIQ_2; }
			if ( std::abs( y ) < std::abs( x ) ) { return ( x > 0 ) ? DIQ_1 : DIQ_3; }
		}
		return DIQ_MAX;
	}

	int Octant(   float x, float y ){

		if ( y > 0 && x > 0 ) { return ( std::abs( y ) > std::abs( x ) ) ? DIO_0 : DIO_1; }
		if ( y < 0 && x > 0 ) { return ( std::abs( y ) < std::abs( x ) ) ? DIO_2 : DIO_3; }
		if ( y < 0 && x < 0 ) { return ( std::abs( y ) > std::abs( x ) ) ? DIO_4 : DIO_5; }
		if ( y > 0 && x < 0 ) { return ( std::abs( y ) < std::abs( x ) ) ? DIO_6 : DIO_7; }

		return DIO_MAX;
	}

	int Quadrant( const VECTOR2* v ){ return v ? Quadrant( v->x, v->y ) : DIQ_MAX;	}
	int Octant(   const VECTOR2* v ){ return v ? Octant(   v->x, v->y ) : DIO_MAX;	}
	int Quadrant( const VECTOR2& v ){ return Quadrant( v.x, v.y );	}
	int Octant(   const VECTOR2& v ){ return Octant(   v.x, v.y );	}
}

namespace {
	template<UINT n>UINT normal(  int d ){ return ( d < 0 ) ? d-(-d)%n : d%n;	}
	template<UINT n>UINT counter( int d ){ return ( normal<n>( d ) + ( n/2 ) ) % n; }
}

namespace opal {
	UINT NormalDix( int d ){ return normal<DIX_MAX>( d );	}
	UINT NormalDio( int d ){ return normal<DIO_MAX>( d );	}

	UINT CounterDix( int d ) { return counter<DIX_MAX>( d ); }
	UINT CounterDio( int d ) { return counter<DIO_MAX>( d ); }
}

//----------------------------------------
// Radian �� DIX
//----------------------------------------
#ifdef _MSC_VER
namespace opal {
	auto Dix2Rad( int d )->float{ return ( ( d < 0 ? DIX_MAX    : 0 ) + d%DIX_MAX                  )*RAD( 90.f );	}
	auto Rad2Dix( float r )->int{ return ( ( r < 0 ? RAD( 360 ) : 0 ) + std::fmod( r, RAD( 360 ) ) )/RAD( 90.f );	}
	auto Rad2Dio( float r )->int{ return ( ( r < 0 ? RAD( 360 ) : 0 ) + std::fmod( r, RAD( 360 ) ) )/RAD( 45.f );	}
}
#endif

// End Of File
