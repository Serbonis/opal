//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Utl/tween.hpp"

//========================================
// ���
//========================================
using namespace opal;

//----------------------------------------
// BEZIER
//----------------------------------------
namespace opal::TWEEN {
	float bezier( float ax, float ay, float bx, float by, float vx ){

		static constexpr auto LOOP		= 100;
		static constexpr auto EPSILON	= 0.0001f;

		auto	t = 0.5f;
		auto	s = 0.5f;

		for ( auto i = 0UL; i < LOOP; i++ ) {
			const auto	v = 3*s*s*t*ax + 3*s*t*t*bx + t*t*t  - vx;

			if ( ::fabsf( v ) < EPSILON ) {
				break;
			}
			if ( v > 0 ) {
				t -= ( float )1.0f/( 4 << i );
			} else {
				t += ( float )1.0f/( 4 << i );
			}
			s = 1 - t;
		}
		return 3*s*s*t*ay + 3*s*t*t*by + t*t*t;
	}
}

//----------------------------------------
// LERP
//----------------------------------------
namespace opal::TWEEN {
	float lerp( float a, float b, float t ){

		return ( 1 - t )*a + t*b;
	}
}

// End Of File
