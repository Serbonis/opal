//========================================
// OPAL Game Library
//========================================
#include "Std/task.hpp"
#include "Std/linx.hpp"

//========================================
// タスクシステム
//========================================
using namespace opal;


//========================================
// タスククラス
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
TASK::TASK() :
	tasq( nullptr ),
	task_{},
	self{}
{}
TASK::~TASK(){}

TASK::TASK_::TASK_() :
	name( nullptr ),	// 識別名
	info( 0 ),			// 情報表示
	live{},				// 生存フラグ
	kill{},				// 死亡フラグ
	retire{},			// 引退フラグ
	sleep{}				// 休眠フラグ
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void TASK::Init( const char* p ){

	if ( !tasq ) {
		tasq		= std::make_shared<TASQ>( this );

		task_.name	= p;
		task_.info	= 1;
		task_.live	= {true, 0};
		task_.kill	= {false,0};
		task_.retire= {false,0};
		task_.sleep	= {false,0};

		self.clear();
	}
}

void TASK::Free( void ){

	if ( tasq ) {
		tasq		= nullptr;

		task_.name	= nullptr;
		task_.info 	= 0;
		task_.live	= {false,0};
		task_.kill 	= {true, 0};
		task_.retire= {false,0};
		task_.sleep	= {false,0};

		self.clear();
	}
}

//----------------------------------------
// 登録＆解放
//----------------------------------------
void TASK::Register( LINX* l ){

	if ( tasq ) {
		tasq->Register( l );
		if ( const auto f = Self( "REGISTER" ) ) {
			f( this );
		}
	}
}

void TASK::Release( LINX* l ){

	if ( tasq ) {
		if ( const auto f = Self( "RELEASE" ) ) {
			f( this );
		}
		tasq->Release( l );
	}
}

//----------------------------------------
// (初期化+登録)＆(解放+後始末)
//----------------------------------------
void TASK::Open( const char* p, LINX* l ){

	if ( !tasq ) {
		Init( p );
		Register( l );
	}
}

void TASK::Close( LINX* l ){

	if ( tasq ) {
		Release( l );
		Free();
	}
}

void TASK::Open( const char* p ){ Open( p, nullptr ); }
void TASK::Open( LINX* l       ){ Open( nullptr, l ); }

void TASK::Open(  void ){ Open(  nullptr, nullptr ); }
void TASK::Close( void ){ Close( nullptr ); }

//----------------------------------------
// インスタンス制御
//----------------------------------------
void TASK::Self( SELF f ){ Self( "KILL", f ); }
auto TASK::Self( void ) const->SELF{ return Self("KILL" ); }

void TASK::Self( const std::string& s, SELF f ){ self[s] = f; }
auto TASK::Self( const std::string& s ) const->SELF{ return self.count( s ) ? self.at( s ) : nullptr; }

void TASK::UnSelf( void ){

	const auto	fp = self["KILL"];

	TaskExit();
	Release();
	Free();

	if ( fp ) {
		fp( this );
	}
}

//----------------------------------------
// リンクリスト
//----------------------------------------
auto TASK::Task( void ) const->TASQ*{ return tasq.get();	}

auto TASK::Next( void ) const->TASK*{

	if ( tasq ) {
		if ( const auto t = tasq->Next() ) {
			return t->Task();
		}
	}
	return nullptr;
}

auto TASK::Last( void ) const->TASK*{

	if ( tasq ) {
		if ( const auto t = tasq->Last() ) {
			return t->Task();
		}
	}
	return nullptr;
}

auto TASK::Linx( void ) const->LINX*{ return tasq ? tasq->Linx() : nullptr; }
void TASK::Linx( LINX* l ){ if ( tasq ) { tasq->Linx( l ); }	}

//----------------------------------------
// 分岐元
//----------------------------------------
auto TASK::Root( void  ) const->TASK*{ return tasq ? tasq->Root()    : nullptr; }
auto TASK::Root( int n ) const->TASK*{ return tasq ? tasq->Root( n ) : nullptr; }

//----------------------------------------
// リスト分岐
//----------------------------------------
auto TASK::Branch( void    ) const->LINX*{ return tasq ? tasq->Branch() : nullptr; 	}
auto TASK::Prune(  void    ) const->bool { return tasq ? tasq->Prune()  : false; 	}
void TASK::Branch( LINX* l ){ if ( tasq ) { tasq->Branch( l ); } }
void TASK::Prune(  bool  f ){ if ( tasq ) { tasq->Prune(  f ); } }

//----------------------------------------
// 生死他フラグ
//----------------------------------------
bool TASK::Live(   void  ) const { return task_.live.flag;		}
bool TASK::Kill(   void  ) const { return task_.kill.flag;		}
bool TASK::Retire( void  ) const { return task_.retire.flag;	}
bool TASK::Sleep(  void  ) const { return task_.sleep.flag;		}

void TASK::Kill(   int t ){ task_.kill   = { true,  ( UINT )t };	}
void TASK::Retire( int t ){ task_.retire = { true,  ( UINT )t };	}
void TASK::Sleep(  int t ){ task_.sleep  = ( t > 0 ) ? FAT{ true, ( UINT )t } : FAT{false,0};	}

UINT TASK::LiveTime(   void ) const { return ( task_.live.flag   ) ? task_.live.time  +1 : 0;	}
UINT TASK::KillTime(   void ) const { return ( task_.kill.flag   ) ? task_.kill.time  +1 : 0;	}
UINT TASK::RetireTime( void ) const { return ( task_.retire.flag ) ? task_.retire.time+1 : 0;	}
UINT TASK::SleepTime(  void ) const { return ( task_.sleep.flag  ) ? task_.sleep.time +1 : 0;	}

bool TASK::LiveTime(   bool f ){ return ( f ) ?  ++task_.live.time   : task_.live.time++;	}
bool TASK::KillTime(   bool f ){ return ( f ) ?  --task_.kill.time   : task_.kill.time--;	}
bool TASK::RetireTime( bool f ){ return ( f ) ?  --task_.retire.time : task_.retire.time--;	}
bool TASK::SleepTime(  bool f ){ return ( f ) ?  --task_.sleep.time  : task_.sleep.time--;	}

bool TASK::Die( int t ){ Kill( t ); return false;	}

//----------------------------------------
// デバッグ情報
//----------------------------------------
UINT TASK::Info( void   ) const { return task_.info;	}
void TASK::Info( UINT v ){ task_.info = v;				}

//----------------------------------------
// 識別名
//----------------------------------------
auto TASK::ID( void ) const->const char*{ return task_.name.Name();	}
void TASK::ID( const char* p ){ task_.name.Name( p ); 				}

//----------------------------------------
// 登録順位
//----------------------------------------
UINT TASK::Prio( void ) const { return tasq ? tasq->Prio() : 0; }
void TASK::Prio( UINT n, bool f ){ if ( tasq ) { tasq->Prio( n, f );	}	}

//----------------------------------------
// Task関数
//----------------------------------------
void TASK::TaskInfo(  void ) const {}
void TASK::TaskInit(  void ){}
void TASK::TaskEnter( void ){}
void TASK::TaskFunc(  void ){}
void TASK::TaskBegin( void ){}
void TASK::TaskMain(  void ){}
void TASK::TaskMain(  UINT ){}
void TASK::TaskEnd(   void ){}
void TASK::TaskExit(  void ){}
void TASK::TaskFree(  void ){}


//========================================
// タスク用コンテナ
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
TASQ::TASQ( TASK* t ) :
	task( t ),			// 管理するタスク
	next( nullptr ),	// 次のTASK
	last( nullptr ),	// 前のTASK
	linx( nullptr ),	// 所属するLINX
	branch( nullptr ),	// LINX分岐
	prune( true ),		// LINX剪定
	prio( 0 )			// 優先順位
{}

//----------------------------------------
// 登録＆解放
//----------------------------------------
void TASQ::Register( LINX* l ){

	if ( const auto pl = ( l ) ? l : nullptr ) {
		pl->Register( task );
	}
}

void TASQ::Release( LINX* l ){

	if ( const auto pl = ( l ) ? l : linx ) {
		pl->Release( task );
	}
}

//----------------------------------------
// リンクリスト
//----------------------------------------
auto TASQ::Task( void ) const->TASK*{ return task; }
auto TASQ::Next( void ) const->TASQ*{ return next; }
auto TASQ::Last( void ) const->TASQ*{ return last; }
auto TASQ::Linx( void ) const->LINX*{ return linx; }

void TASQ::Task( TASK* t ){ task = t; }
void TASQ::Next( TASQ* q ){ next = q; }
void TASQ::Last( TASQ* q ){ last = q; }
void TASQ::Linx( LINX* l ){ linx = l; }

//----------------------------------------
// 分岐元
//----------------------------------------
auto TASQ::Root( void  ) const->TASK*{

	if ( linx ) {
		if ( const auto q = linx->Task() ) {
			return q->Task();
		}
	}
	return nullptr;
}

auto TASQ::Root( int n ) const->TASK*{

	std::vector<TASK*>	r;

	for ( auto p = Root(); p; p = p->Root() ) {
		r.push_back( p );
	}

	if ( const auto s = ( int )r.size() ) {
		if ( 0 <= +n && +n <  s ) { return r[0+n]; }
		if ( 0 <  -n && -n <= s ) { return r[s-n]; }
	}

	return nullptr;
}

//----------------------------------------
// リスト分岐
//----------------------------------------
auto TASQ::Branch( void ) const->LINX*{ return branch; 	}
auto TASQ::Prune(  void ) const->bool { return prune;	}

void TASQ::Branch( LINX* l ){

	if ( branch != l ) {
		if ( branch ) { branch->Task( nullptr );	}
		branch = l;
		if ( branch ) { branch->Task( this    );	}
	}
}
void TASQ::Prune( bool f ){ prune = f;	}

//----------------------------------------
// 登録順位
//----------------------------------------
UINT TASQ::Prio( void ) const { return prio; }
void TASQ::Prio( UINT n, bool f ){

	prio = n;

	if ( linx ) {
		const auto	l = linx;

		l->Release( task );
		if ( f ) {
			l->RegisterS( task );
		} else {
			l->RegisterQ( task );
		}
	}
}

// End Of File
