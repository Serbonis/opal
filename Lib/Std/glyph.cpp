//========================================
// OPAL Game Library
//========================================
#include "Std/glyph.hpp"

//========================================
// GLYPH
//========================================
using namespace opal;


//----------------------------------------
// constructor & destructor
//----------------------------------------
GLYPH::GLYPH( LONG s, int g, const char* f ) :
	logfont{Logfont( s, f )},
	format{g},
	grad{Grad( g )},
	hdc{0}
{
	Register();
}

GLYPH::~GLYPH()
{
	Release();
}

void GLYPH::Register( void ) {

	if ( !hdc ) {
		nft = CreateFontIndirect( &logfont );
		hdc = GetDC( nullptr );
		oft = ( HFONT )SelectObject( hdc, nft );

		GetTextMetrics( hdc, &tmetrix );
	}
}

void GLYPH::Release( void ) {

	if ( hdc ) {
		SelectObject( hdc, oft );
		DeleteObject( nft );
		ReleaseDC( nullptr, hdc );

		hdc = 0;
	}
}

//----------------------------------------
//
//----------------------------------------
void GLYPH::operator()( const char* p, int sz, BYTE* pb, DWORD rp, DWORD fc, DWORD bc ){

	( *this )( p );
	( *this )( sz, pb, rp, fc, bc );
}

void GLYPH::operator()( const std::string& s, int sz, BYTE* pb, DWORD rp, DWORD fc, DWORD bc ){

	( *this )( s.c_str(), sz, pb, rp, fc, bc );
}

void GLYPH::operator()( std::nullptr_t, int sz, BYTE* pb, DWORD rp, DWORD fc, DWORD bc ){}

void GLYPH::operator()( const char* p, int sz, BYTE* pb, DWORD rp, DWORD fc ){

	( *this )( p );
	( *this )( sz, pb, rp, fc );
}

void GLYPH::operator()( const std::string& s, int sz, BYTE* pb, DWORD rp, DWORD fc ){

	( *this )( s.c_str(), sz, pb, rp, fc );
}

void GLYPH::operator()( std::nullptr_t, int sz, BYTE* pb, DWORD rp, DWORD fc ){}

//----------------------------------------
//
//----------------------------------------
void GLYPH::operator()( const char* p ){

	Register();

	auto c = p;

	while ( *c ) {
		auto&	pb = buffer.emplace_back();
		auto&	gm = gmetrix.emplace_back();
		auto	db = dbcs.emplace_back( IsDBCSLeadByte( *c ) );

		constexpr MAT2	mat2 = { {0,1},{0,0},{0,0},{0,1} };
		const auto		code = db ? (( BYTE )c[0]<<8|( BYTE )c[1] ) : c[0];

		if ( const auto	size = GetGlyphOutline( hdc, code, format, &gm, 0, nullptr, &mat2 ) ) {
			pb.resize( size );
			GetGlyphOutline( hdc, code, format, &gm, size, pb.data(), &mat2 );
		}

		c += db ? 2 : 1;
	}
}
void GLYPH::operator()( const std::string& s ){ (*this)( s.c_str() );	}
void GLYPH::operator()( std::nullptr_t ){}

//----------------------------------------
//
//----------------------------------------
void GLYPH::operator()( int sz, BYTE* pb, DWORD rp, DWORD fc, DWORD bc ){

	back( sz, pb, rp, bc );
	font( sz, pb, rp, fc );
}

void GLYPH::operator()( int sz, BYTE* pb, DWORD rp, DWORD fc ){

	font( sz, pb, rp, fc );
}

void GLYPH::back( int sz, BYTE* pb, DWORD rp, DWORD bc ){

	const auto	c = bc ? (0x80<<24)|( bc & 0x00ffffff ) : 0;

	for ( auto i = 0; i < sz; i++ ) {
		for ( auto j = 0; j < sz; j++ ) {
			std::memcpy( &pb[rp*i+4*j], &c, sizeof( DWORD ) );
		}
	}
}

void GLYPH::font( int sz, BYTE* pb, DWORD rp, DWORD fc ){

	const auto	fn = buffer.size();
	const auto	fs = logfont.lfHeight;
	const auto	ln = 2 * sz/fs - 1;

	auto	col = 0;
	auto	row = 0;

	for ( auto n = 0UL; n < fn; n++ ) {
		const auto&	bf = buffer[n];
		const auto	cl = fc & 0x00ffffff;

		const auto	ps = ( rp * row + col*2 ) * fs;
		const auto	fw = dbcs[n] ? 2 : 1;

		if ( col + fw < ln ) {
			col += fw;
		} else {
			col = 0;
			row++;
		}

		if ( bf.size() ) {
			const auto&	tm = tmetrix;
			const auto&	gm = gmetrix[n];
			const auto	rx = gm.gmptGlyphOrigin.x;
			const auto	ry = tm.tmAscent - gm.gmptGlyphOrigin.y;
			const auto	rw = gm.gmBlackBoxX + (4 - (gm.gmBlackBoxX % 4)) % 4;
			const auto	rh = gm.gmBlackBoxY;

			for ( auto i = 0UL; i < rh; i++ ) {
				for ( auto j = 0UL; j < rw; j++ ) {
					if ( const auto a = bf[i*rw+j] ) {
						const auto	c = ( ( 255 * a/grad ) << 24 )|cl;
						std::memcpy( &pb[ps+rp*(i+ry)+4*(j+rx)], &c, sizeof( DWORD ) );
					}
				}
			}
		}
	}
}

//----------------------------------------
//
//----------------------------------------
LOGFONT GLYPH::Logfont( LONG s, const char* p ){

	LOGFONT	lf;

	lf.lfHeight			= s;						// 文字セルまたは文字の高さ
	lf.lfWidth			= 0;						// 平均文字幅
	lf.lfEscapement		= 0;            			// 文字送りの方向とX軸との角度
	lf.lfOrientation	= 0;            			// ベースラインとX軸との角度
	lf.lfWeight			= FW_DONTCARE;  			// フォントの太さ
	lf.lfItalic			= false;        			// イタリック体指定
	lf.lfUnderline		= false;        			// 下線付き指定
	lf.lfStrikeOut		= false;					// 打ち消し線付き指定
	lf.lfCharSet		= SHIFTJIS_CHARSET;			// キャラクタセット
	lf.lfOutPrecision	= OUT_TT_ONLY_PRECIS;   	// 出力精度
	lf.lfClipPrecision	= CLIP_DEFAULT_PRECIS;		// クリッピングの精度
	lf.lfQuality		= PROOF_QUALITY;        	// 出力品質
	lf.lfPitchAndFamily	= FIXED_PITCH|FF_MODERN;	// ピッチとファミリ

	// フォント名
	const auto f = p ? p : _T( "ＭＳ 明朝" );

	std::memset( lf.lfFaceName, 0, LF_FACESIZE );
	std::memcpy( lf.lfFaceName, f, std::strlen( f ) );

	return lf;
}

//----------------------------------------
//
//----------------------------------------
int GLYPH::Grad( int g ){

	switch ( g ){
	case GGO_GRAY2_BITMAP: return  4+1;
	case GGO_GRAY4_BITMAP: return 16+1;
	case GGO_GRAY8_BITMAP: return 64+1;
	}

	return 1;
}


// End Of File
