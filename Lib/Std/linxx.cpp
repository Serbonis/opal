//========================================
// OPAL Game Library
//========================================
#include "Std/task.hpp"
#include "Std/linx.hpp"

//========================================
// �^�X�N���X�g
//========================================
using namespace opal;

//----------------------------------------
// �^�X�N�̓o�^����
//----------------------------------------
UINT LINX::TaskCount( void ) const {

	struct LINX_ {
		static void TaskCount( const LINX* l, UINT& n ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				++n;
				if ( const auto b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskCount( b, n );
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		UINT	n = 0;

		LINX_::TaskCount( this, n );

		return n;
	}

	return 0;
}

//----------------------------------------
// �j��
//----------------------------------------
void LINX::TaskDestroy( void ) {

	struct LINX_ {
		static void TaskDestroy( const LINX* l, LINX::QTASK* v ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				v->emplace_front( p->Task() );
				if ( const auto b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskDestroy( b, v );
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		QTASK	t;

		LINX_::TaskDestroy( this, &t );

		TaskDestroy( t );
	}
}

void LINX::TaskDestroy( const QTASK& t ) {

	for ( auto& p : t ) {
		if ( p->Kill() ) {
			p->UnSelf();
		} else {
			p->Release();
		}
	}
}

//----------------------------------------
// ���
//----------------------------------------
void LINX::TaskRelease( void ) {

	struct LINX_ {
		static void TaskRelease( const LINX* l, LINX::QTASK* v ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				v->emplace_front( p->Task() );
				if ( const auto b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskRelease( b, v );
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		QTASK	t;

		LINX_::TaskRelease( this, &t );

		for ( auto& p : t ) {
			p->Release();
		}
	}
}

//----------------------------------------
// ����
//----------------------------------------
UINT LINX::TaskSearch( VTASK& v, const SFUNC f ){

	struct LINX_ {
		static void TaskSearch( const LINX* l, const LINX::SFUNC f, LINX::VTASK* v ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task(); f( t ) ) {
					v->emplace_back( t );
				}

				if ( const auto	b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskSearch( b, f, v );
					}
				}

				p = p->Next();
			}
		}
	};


	if ( Elements() ) {
		LINX_::TaskSearch( this, f, &v );
		return v.size();
	}
	return 0;
}

//----------------------------------------
// �]��
//----------------------------------------
void LINX::TaskEval( const EFUNC f ){

	struct LINX_ {
		static void TaskEval( const LINX* l, const LINX::EFUNC f ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				f( p->Task() );

				if ( p->Prune() ) {
					if ( const auto	b = p->Branch() ) {
						if ( b->Elements() ) {
							TaskEval( b, f );
						}
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LINX_::TaskEval( this, f );
	}
}

// End Of File

