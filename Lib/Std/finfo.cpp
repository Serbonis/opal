//========================================
// OPAL Game Library
//========================================
#include "Std/finfo.hpp"

//========================================
// 汎用ファイル管理
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
FILEINFO::FILEINFO() :
	path(),
	name(),
	adrs( nullptr ),
	size( 0 )
{}

//----------------------------------------
// member functions
//----------------------------------------
void FILEINFO::Path( const char* p ){ path.Name( p ); }
void FILEINFO::Name( const char* p ){ name.Name( p ); }
void FILEINFO::Adrs( const void* p ){ adrs = p; }
void FILEINFO::Size(      size_t s ){ size = s; }

const char* FILEINFO::Path( void ) const { return path.Name(); }
const char* FILEINFO::Name( void ) const { return name.Name(); }
const void* FILEINFO::Adrs( void ) const { return adrs; }
size_t      FILEINFO::Size( void ) const { return size; }

void FILEINFO::Info( const char* p, const char* n, const void* v, size_t s ){

	Path( p );
	Name( n );
	Adrs( v );
	Size( s );
}

void FILEINFO::Info( void ){

	Path( nullptr );
	Name( nullptr );
	Adrs( nullptr );
	Size( 0 );
}

//----------------------------------------
// フルパスからファイル名を抜き出す(ASCIIのみ)
//----------------------------------------
namespace opal {
	static inline int scl( const char* path, const char ch ){

		if ( path ) {
			if ( auto s = strlen_s( path ) ) {
				for ( const auto* p = &path[s-1]; p >= path; p-- ) {
					if ( *p == ch ) {
						return s;
					}
					--s;
				}
			}
		}
		return 0;
	}

	int GetFilePath( const char* path ){

		const auto	s0 = scl( path, '/'  );
		const auto	s1 = scl( path, '\\' );

		return ( s0 > s1 ) ? s0 : s1;
	}

	const char* GetFileName( const char* path ){

		return ( path ) ? &path[::GetFilePath( path )] : nullptr;
	}

	const char* GetFileKind( const char* path ){

		if ( const auto* file = ::GetFileName( path ) ) {
			return &file[scl( file, '.' )];
		}
		return nullptr;
	}
}

//----------------------------------------
// 相対パスか絶対パスか判断してパスをつける
//----------------------------------------
namespace opal {
	NAME PathFile( const char* path, const char* file ){

		if ( file ) {
			if ( !path || file[0] == '/' || strstr_s( file, ":" ) ) {
				return NAME( file );
			}
			return NAME( std::string{path}+std::string{file} );
		}
		return NAME( path );
	}
}

// End Of File
