//========================================
// OPAL Game Library
//========================================
#include "Std/task.hpp"
#include "Std/linx.hpp"

//========================================
// タスクリスト
//========================================
using namespace opal;

//----------------------------------------
// タスクの情報表示
//----------------------------------------
void LINX::TaskInfo( void ){

	struct LINX_ {
		static void TaskInfo( const LINX* l, UINT n, int(*f)( const char*, ... ) ){

			const auto	v = l->Info();

			auto p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task(); v && ( v <= t->Info() ) ) {
					t->TaskInfo();

					if ( const auto b = p->Branch() ) {
						static const auto	s = []( auto f, auto n ){
							for ( auto j = n; j; --j ) { f( "　" ); }
						};

						if ( p->Prune() ) {
							s( f, ++n );
							f( "┗┓%d %p\n", b->Elements(), b );
							if ( b->Elements() ) { TaskInfo( b, n, f ); }
							s( f, n-- );
							f( "┏┛\n" );
						} else {
							s( f, ++n );
							f( "┃ %d %p\n", b->Elements(), b );
							s( f, n-- );
						}
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LINX_::TaskInfo( this, 0, printd );
	}
}

//----------------------------------------
// タスク初期化
//----------------------------------------
void LINX::TaskInit( void ){

	struct LINX_ {
		static void TaskInit( const LINX* l ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskInit();
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						if ( b->Elements() ) {
							TaskInit( b );
						}
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LINX_::TaskInit( this );
	}
}

//----------------------------------------
// タスク前処理
//----------------------------------------
void LINX::TaskEnter( void ){

	struct LINX_ {
		static void TaskEnter( const LINX* l ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskEnter();
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						if ( b->Elements() ) {
							TaskEnter( b );
						}
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LINX_::TaskEnter( this );
	}
}

//----------------------------------------
// タスクユーザー処理
//----------------------------------------
void LINX::TaskFunc( void ){

	struct LINX_ {
		static void TaskFunc( const LINX* l ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskFunc();
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						b->LinxFuncBegin();
						if ( b->Elements() ) {
							TaskFunc( b );
						}
						b->LinxFuncEnd();
					}
				}

				p = p->Next();
			}
		}
	};


	if ( Elements() ) {
		LinxFuncBegin();
		LINX_::TaskFunc( this );
		LinxFuncEnd();
	}
}

//----------------------------------------
// タスクメイン処理
//----------------------------------------
void LINX::TaskMain( void ){

	struct LINX_ {
		static void TaskMain( const LINX* l ){

			auto	p = l->Head();
			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskBegin();
						t->TaskMain();
						t->TaskEnd();
						t->LiveTime( true );
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						b->LinxMainBegin();
						if ( b->Elements() ) {
							TaskMain( b );
						}
						b->LinxMainEnd();
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LinxMainBegin();
		LINX_::TaskMain( this );
		LinxMainEnd();
	}
}

void LINX::TaskMain( UINT n ){

	struct LINX_ {
		static void TaskMain( UINT n, const LINX* l ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskBegin();
						t->TaskMain( n );
						t->TaskEnd();
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						b->LinxMainBegin();
						if ( b->Elements() ) {
							TaskMain( n, b );
						}
						b->LinxMainEnd();
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LinxMainBegin();
		LINX_::TaskMain( n, this );
		LinxMainEnd();
	}
}

//----------------------------------------
// タスク後処理
//----------------------------------------
void LINX::TaskExit( void ){

	struct LINX_ {
		static void TaskExit( const LINX* l ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					if ( !t->Sleep() ) {
						t->TaskExit();
					}
				}

				if ( p->Prune() ) {
					if ( const auto b = p->Branch() ) {
						if ( b->Elements() ) {
							TaskExit( b );
						}
					}
				}

				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		LINX_::TaskExit( this );
	}
}

//----------------------------------------
// タスク後始末
//----------------------------------------
void LINX::TaskFree( void ){

	struct LINX_ {
		static void TaskFree( const LINX* l, LINX::QTASK* v, bool f ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				const auto	t = p->Task();

				const auto	s = !t ? f : [&]{
					if ( f || ( t->Kill() && !t->KillTime( false ) ) ) {
						v->emplace_front( t );
						return true;
					}
					if ( !t->Kill() && t->Retire() && !t->RetireTime( false ) ) {
						v->emplace_front( t );
					}
					return f;
				}();

				if ( t && t->Sleep() ) {
					if ( t->SleepTime( false ) ) {
						t->Sleep( 0 );
					}
				}

				if ( const auto b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskFree( b, v, s );
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		QTASK	v;

		LINX_::TaskFree( this, &v, false );

		if ( v.size() ) {
			TaskDestroy( v );
		}
	}
}

//----------------------------------------
// タスクを殺す
//----------------------------------------
void LINX::TaskKill( void ){

	struct LINX_ {
		static void TaskKill( const LINX* l, LINX::QTASK* v ){

			auto	p = l->Head();

			for ( auto i = l->Elements(); i; --i ) {
				if ( const auto t = p->Task() ) {
					v->emplace_front( t );
				}

				if ( const auto b = p->Branch() ) {
					if ( b->Elements() ) {
						TaskKill( b, v );
					}
				}
				p = p->Next();
			}
		}
	};

	if ( Elements() ) {
		QTASK	v;

		LINX_::TaskKill( this, &v );

		if ( v.size() ) {
			TaskDestroy( v );
		}
	}
}

// End Of File
