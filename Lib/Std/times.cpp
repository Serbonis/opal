//========================================
// OPAL Game Library
//========================================
#include "Std/times.hpp"

//========================================
// 速度調整クラス
//========================================
using namespace opal;

//----------------------------------------
// constructor
//----------------------------------------
TIMES::TIMES() :
	skip( -1 ),
	loop( +1 ),
	rate(  1 )
{}

//----------------------------------------
// 回数チェック
//----------------------------------------
bool TIMES::operator ()( int n ){

	if ( skip >=0 ) {
		skip--;
		loop = +1;
	} else if ( n > 0 ) {
		skip = -1;
		loop = +n;
	} else if ( n < 0 ) {
		skip = -n;
		loop = +1;
	} else if ( rate == 0 ) {
		skip = -1;
		loop =  0;
	} else if ( rate == 1 ) {
		skip = -1;
		loop = +1;
	} else {
		(*this)( rate );
	}

	return ( skip > 0 ) ? false : true;
}

int TIMES::operator ()( void ){ return loop; }

//----------------------------------------
// デフォルト速度
//----------------------------------------
TIMES::operator int( void ) const { return rate; }
int TIMES::operator =( int n ){ return rate = n; }

// End Of File
