//========================================
// OPAL Game Library
//========================================
#include "Std/device.hpp"

#include "Std/guid.hpp"

//========================================
// デバイスクラス
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
XDEVICE::XDEVICE( const DEVICE_NAME n ) : name( n ) {}
XDEVICE::XDEVICE() : name{""} {}
XDEVICE::~XDEVICE(){}

//----------------------------------------
// デバイスの名前
//----------------------------------------
XDEVICE::operator DEVICE_NAME(   void ) const { return name;	}
DEVICE_NAME XDEVICE::DeviceName( void ) const { return name;	}
DEVICE_NAME XDEVICE::DeviceName( const DEVICE_NAME n ){ return name = n;	}
DEVICE_NAME XDEVICE::DeviceName( const char* n ){ return name = n ? n : guid_name(); }

//----------------------------------------
// デバイスの初期化＆後始末
//----------------------------------------
int XDEVICE::InitDevice( int ){ return 0; }
int XDEVICE::FreeDevice( int ){ return 0; }

//----------------------------------------
// デバイス作成用データの初期化＆後始末
//----------------------------------------
void XDEVICE::DeviceData( const void*[] ){}
void XDEVICE::DeviceData( const void*   ){}
void XDEVICE::DeviceData( void ){}

//----------------------------------------
// デバイスのリセット
//----------------------------------------
void XDEVICE::DestroyDevice( void ){}

//----------------------------------------
// デバイスの準備
//----------------------------------------
bool XDEVICE::DeviceReady( void ) const { return name == "" ? false : true;	}

//========================================
// デバイスロスト
//========================================
//----------------------------------------
// constructor & desyructor
//----------------------------------------
DEVICE_LOST::DEVICE_LOST() :
	count( 0 ),
	device{}
{}
DEVICE_LOST::~DEVICE_LOST(){}

//----------------------------------------
// クリア
//----------------------------------------
void DEVICE_LOST::Clear( void ){

	count = 0;
	device.clear();
}

//----------------------------------------
// 登録＆削除
//----------------------------------------
void DEVICE_LOST::DeviceLost( const DEVICE_NAME n, XDEVICE* d ){

	if ( !device.count( n ) ) {
		device[n] = d;
	}
}
void DEVICE_LOST::DeviceLost( const DEVICE_NAME n ){

	if ( device.count( n ) ) {
		device.erase( n );
	}
}

UINT DEVICE_LOST::DeviceLost( void ) const { return count; }

//----------------------------------------
// マップ
//----------------------------------------
const decltype( DEVICE_LOST::device )& DEVICE_LOST::operator()( void ) const { return device; }

//----------------------------------------
// カウント
//----------------------------------------
DEVICE_LOST::operator UINT( void ) const { return count;	}

UINT DEVICE_LOST::operator =( UINT n ){ return count = n;	}

UINT DEVICE_LOST::operator++( void ){ return ++count;	}
UINT DEVICE_LOST::operator--( void ){ return --count;	}
UINT DEVICE_LOST::operator++( int  ){ const auto c = count++; return c;	}
UINT DEVICE_LOST::operator--( int  ){ const auto c = count--; return c;	}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
int DEVICE_LOST::InitDevice( const int n ){

	std::vector<XDEVICE*>	x;

	for ( const auto& [f,d] : device ) {
		if ( d ) {
			x.push_back( d );
		}
	}

	for ( const auto& d : x ) {
		if ( d->InitDevice( n ) ) {
			;
		}
	}

	return 0;
}


int DEVICE_LOST::FreeDevice( const int n ){

	std::vector<XDEVICE*>	x;

	for ( const auto& [f,d] : device ) {
		if ( d ) {
			x.push_back( d );
		}
	}

	for ( const auto& d : x ) {
		if ( d->FreeDevice( n ) ) {
			;
		}
	}
	return 0;
}

void DEVICE_LOST::DestroyDevice( void ){

	std::vector<XDEVICE*>	x;

	for ( const auto& [f,d] : device ) {
		if ( d ) {
			x.push_back( d );
		}
	}

	for ( const auto& d : x ) {
		d->DestroyDevice();
	}
}

// End Of File
