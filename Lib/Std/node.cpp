//========================================
// OPAL Game Library
//========================================
#include "Std/str_s.hpp"
#include "Std/node.hpp"

//========================================
// �m�[�h
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
NODE::NODE() :
	type{nullptr},
	name{},
	data{nullptr},
	child{nullptr},
	parent{nullptr}
{}

NODE::NODE( const std::string& s, NODE* p ) :
	type{nullptr},
	name{s},
	data{nullptr},
	child{nullptr},
	parent{p}
{}

NODE::~NODE(){}

//----------------------------------------
// ���O
//----------------------------------------
const char* NODE::Type( void ) const { return type ? type->name() : nullptr;	}
const char* NODE::Name( void ) const { return name.c_str();	}
std::string NODE::Path( void ) const {

	std::string	s{""};

	for ( auto p = parent; p; p = p->parent ) {
		s = p->name + "/" + s;
	}

	return s;
}

//----------------------------------------
// ����
//----------------------------------------
void* NODE::Search( const NODE& n, const std::string& s ){

	auto	p = &n;

	for ( auto& i : split( s, "/" ) ) {
		if ( !p->child || !p->child->count( i ) ) {
			return nullptr;
		}
		p = p->child->at( i ).get();
	}

	return p->data.get();
}

//----------------------------------------
// �j��
//----------------------------------------
void NODE::Destroy( NODE& n, const std::string& s ){

	auto	p = &n;

	for ( auto& i : split( s, "/" ) ) {
		if ( i == "" ) {
			break;
		}
		if ( !p->child || !p->child->count( i ) ) {
			return;
		}
		p = p->child->at( i ).get();
	}

	Destroy( *p );
}

void NODE::Destroy( NODE& n, const std::type_info& t ){

	std::vector<NODE*>	node;

	const std::function<void(NODE&)>	destroy = [&]( auto& n ){
		if ( n.child ) {
			for ( const auto& [s,c] : *n.child ) {
				destroy( *c );
			}
		} else if ( t == *n.type ) {
			node.emplace_back( &n );
		}
	};
	destroy( n );

	for ( auto& n : node ) {
		Destroy( *n );
	}
}

void NODE::Destroy( NODE& n ){

	if ( const auto p = n.parent ) {
		p->child->erase( n.name );
	} else {
		n = NODE{};
	}
}

#ifdef OPAL_DEBUG
//----------------------------------------
// �f�o�b�O
//----------------------------------------
void NODE::Debug( NODE& n ){

	struct DEBUG {
		static void func( const NODE& n ){

			if ( n.child ) {
				for ( const auto& [s,c] : *n.child ) {
					func( *c );
				}
			} else if ( n.parent ) {
				printd( "%s%s %s\n", n.Path().c_str(), n.Name(), n.Type() );
			}
		}
	};

	DEBUG::func( n );
}
#endif

// End Of File
