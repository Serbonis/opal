//========================================
// OPAL Game Library
//========================================
#include "Std/message.hpp"

//========================================
// メッセ―ジ
//========================================
using namespace opal;

//========================================
// メッセージデータ
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
MESSAGE_DATA::MESSAGE_DATA() :
	name(),
	adrs( 0 ),
	size( 0 ),
	argv( 0 )
{}

MESSAGE_DATA::MESSAGE_DATA( const std::string_view p, void* a, size_t s, UINT v ) :
	name( p ),
	adrs( a ),
	size( s ),
	argv( v )
{}

//----------------------------------------
// ファイル情報
//----------------------------------------
const char* MESSAGE_DATA::Name( void ) const { return name;	}
void*       MESSAGE_DATA::Adrs( void ) const { return adrs;	}
size_t      MESSAGE_DATA::Size( void ) const { return size;	}
UINT        MESSAGE_DATA::Argv( void ) const { return argv;	}

void MESSAGE_DATA::Name( const char* p ){ name = p;	}
void MESSAGE_DATA::Adrs( void*       a ){ adrs = a;	}
void MESSAGE_DATA::Size( size_t      s ){ size = s;	}
void MESSAGE_DATA::Argv( UINT        v ){ argv = v;	}


//========================================
// メッセージタスク
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
MESSAGE_TASK::MESSAGE_TASK() :
	kind( 0 ),
	mode( 0 ),
	time( 0 ),
	errr( 0 ),
	code( 0 )
{}

void MESSAGE_TASK::Task( int k, UINT m, DWORD t ){

	kind	= k;
	mode	= m;
	time	= t;
}

// End Of File
