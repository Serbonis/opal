//========================================
// OPAL Game Library
//========================================
#include "Std/tick.hpp"

//========================================
// タイムインターバル
//========================================
using namespace opal;

//----------------------------------------
// constructor
//----------------------------------------
TICK::TICK( int t, int w ) :
	tick( t ),
	wait( w )
{}
TICK::TICK( int n ) :
	tick( n ),
	wait( n )
{}
TICK::TICK() :
	tick( 0 ),
	wait( 0 )
{}

//----------------------------------------
// 設定＆取得
//----------------------------------------
void TICK::SetTick( int t ){ tick = t; }
int  TICK::GetTick( void ) const { return tick; }
int  TICK::operator =( int t ){ return tick = wait = t; }

//----------------------------------------
// 状態
//----------------------------------------
int TICK::operator ()( int t ){ return wait = t;	}
TICK::operator int() const { return wait;	}

//----------------------------------------
// 更新
//----------------------------------------
bool TICK::operator ()( void ){ return Tick();	}

bool TICK::Tick( void ){

	if ( tick == 0 ) {
		return true;
	}

	if ( wait == 0 ) {
		wait = tick;
		return true;
	}

	--wait;
	return false;
}

// End Of File
