//========================================
// OPAL Game Library
//========================================
#include "Std/task.hpp"
#include "Std/linx.hpp"

//========================================
// タスクリスト
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
LINX::LINX() :
	name{},
	index( nullptr ),
	task(  nullptr ),
	elms( 0 ),
	info( 0 )
{}
LINX::~LINX(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LINX::Init( const char* p ){

	name	= p;
	index	= nullptr;
	task	= nullptr;
	elms	= 0;
	info	= 0;
}

void LINX::Free( void ){

	name	= nullptr;
	index	= nullptr;
	task	= nullptr;
	elms	= 0;
	info	= 0;
}

//----------------------------------------
// 実行
//----------------------------------------
void LINX::Exec( void ){

	TaskInit();
	TaskEnter();
	TaskFunc();
	TaskMain();
	TaskInfo();
	TaskExit();
	TaskFree();
}

//----------------------------------------
// 名前
//----------------------------------------
auto LINX::Name( void ) const->const char*{ return name.Name();	}
void LINX::Name( const char* p ){ name.Name( p ); 				}

//----------------------------------------
// 情報
//----------------------------------------
UINT LINX::Info( void ) const { return info;	}
void LINX::Info( UINT v ){ info = v;	}

//----------------------------------------
// 登録タスク数
//----------------------------------------
UINT LINX::Elements( void ) const { return elms; }

//----------------------------------------
// 接続位置
//----------------------------------------
auto LINX::Task( void ) const->TASQ*{ return task;	}
void LINX::Task( TASQ* t ){ task = t;				}

//----------------------------------------
// リンク操作
//----------------------------------------
auto LINX::Head( void ) const->TASQ*{ return index; }
auto LINX::Tail( void ) const->TASQ*{ return ( index ) ? index->Last() : nullptr; }

auto LINX::Next( void )->LINX&{ if ( index ) { index = index->Next(); } return *this; }
auto LINX::Last( void )->LINX&{ if ( index ) { index = index->Last(); } return *this; }

// End Of File
