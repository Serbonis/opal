//========================================
// OPAL Game Library
//========================================
#include "Std/mem_s.hpp"

//========================================
// NULL対応のメモリ操作関数
//========================================
namespace opal {
	void* memcpy_s( void *dest, const void *src, size_t n ){

		if ( !( dest && src && n ) ) {
			return nullptr;
		}
		return ::memcpy( dest, src, n );
	}
}

namespace opal {
	void* memset_s( void* buff, int ch, size_t n ){

		if ( !buff ) {
			return nullptr;
		}
		return ::memset( buff, ch, n );
	}
}

// End Of File
