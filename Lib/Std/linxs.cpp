//========================================
// OPAL Game Library
//========================================
#include "Std/task.hpp"
#include "Std/linx.hpp"

//========================================
// タスクリスト
//========================================
using namespace opal;

//----------------------------------------
// 登録
//----------------------------------------
namespace {
	inline bool FIFO( UINT a, UINT b ){ return a <= b; }
	inline bool LIFO( UINT a, UINT b ){ return a <  b; }
}

void LINX::Register( TASK* const t, std::function<bool(UINT,UINT)> f ){

	if ( t == nullptr ) {
		return;
	}

	if ( const auto p = t->Task() ) {
		if ( const auto l = p->Linx() ) {
			l->Release( t );
		}

		if ( f == nullptr ) {
			f = LIFO;
		}

		if ( auto q = index ) {
			do {
				if ( f( p->Prio(), q->Prio() ) ) {
					if ( q == index ) {
						index = p;
					}
					break;
				}
				q = q->Next();
			} while ( q != index );

			if ( const auto	r = q->Last() ) {
				q->Last( p );
				r->Next( p );

				p->Linx( this );
				p->Next( q );
				p->Last( r );
			}
		} else {
			index = p;

			p->Linx( this );
			p->Next( p );
			p->Last( p );
		}
		++elms;
	}
}

void LINX::RegisterQ( TASK* t ){ Register( t, FIFO ); }
void LINX::RegisterS( TASK* t ){ Register( t, LIFO ); }

//----------------------------------------
// 解放
//----------------------------------------
void LINX::Release( TASK* const t ){

	if ( t == nullptr ) {
		while ( index ) {
			Release( index->Task() );
		}
		return;
	}

	if ( const auto p = t->Task() ) {
		if ( p->Linx() == nullptr ) { return; }
		if ( p->Linx() != this    ) { return; }

		auto* const	q = p->Next();
		auto* const	r = p->Last();

		if ( !q || !r ) { return;	}

		if ( index == p ) {
			index = ( p != q ) ? q : nullptr;
		}

		q->Last( r );
		r->Next( q );

		p->Linx( nullptr );
		p->Next( nullptr );
		p->Last( nullptr );

		--elms;
	}
}

//----------------------------------------
// 分岐前後処理
//----------------------------------------
void LINX::LinxMainBegin( void ){}
void LINX::LinxMainEnd(   void ){}
void LINX::LinxFuncBegin( void ){}
void LINX::LinxFuncEnd(   void ){}

// End Of File
