//========================================
// OPAL Game Library
//========================================
#include "Std/str_s.hpp"

//========================================
// 文字列操作
//========================================
using namespace opal;

//========================================
// NULL対応の文字列操作関数
//========================================
//----------------------------------------
// 長さ
//----------------------------------------
namespace opal {
	UINT strlen_s( const char *src ){

		return ( src ) ? std::strlen( src ) : 0;
	}
}

namespace opal {
	UINT strlenf_s( const char* src, ... ){

		if ( src ) {
			va_list	list;

			va_start( list, src );
			const auto	n = std::vsnprintf( nullptr, 0, src, list );
			va_end( list );

			return n;
		}
		return 0;
	}
}

namespace opal {
	UINT strlenf_s( const char* src, va_list& list ){

		if ( src ) {
			if ( const auto p = std::fopen( "nul", "w" ) ) {
				const auto	n = std::vfprintf( p, src, list );
				std::fclose( p );
				return n;
			}
		}
		return 0;
	}
}

//----------------------------------------
// コピー
//----------------------------------------
namespace opal {
	char* strcpy_s( char *dest, const char *src ){

		return ( dest && src ) ? std::strcpy( dest, src ) : nullptr;
	}
}

namespace opal {
	char* strncpy_s( char* dest, const char* src, UINT n ){

		return ( dest && src ) ? std::strncpy( dest, src, n ) : nullptr;
	}
}

//----------------------------------------
// 比較
//----------------------------------------
namespace opal {
	int strcmp_s( const char *src1, const char *src2 ){

		if ( src1 != nullptr && src2 == nullptr ) { return +1;	}
		if ( src1 == nullptr && src2 != nullptr ) { return -1;	}
		if ( src1 == nullptr && src2 == nullptr ) { return 0;	}

		return std::strcmp( src1, src2 );
	}
}

namespace opal {
	int strncmp_s( const char* src1, const char* src2, UINT n ){

		if ( src1 != nullptr && src2 == nullptr ) { return +1;	}
		if ( src1 == nullptr && src2 != nullptr ) { return -1;	}
		if ( src1 == nullptr && src2 == nullptr ) { return 0;	}

		return std::strncmp( src1, src2, n );
	}
}

//----------------------------------------
// マッチ
//----------------------------------------
namespace opal {
	const char* strstr_s( const char* src1, const char* src2 ){

		return ( src1 ) ? std::strstr( src1, src2 ) : nullptr;
	}
}

namespace opal {
	const char* strcnt_s( const char* src1, const char* src2, UINT n ){

		if ( n ) {
			auto	c = 0UL;

			for ( const auto* p = src1; ( p = strstr_s( p, src2 ) ); p++ ) {
				if ( ++c == n ) {
					return p;
				}
			}

			return nullptr;
		}

		return src1;
	}
}

//----------------------------------------
// 位置
//----------------------------------------
namespace opal {
	const char* strptr_s( const char* src, int n ){

		auto	l = ( int )strlen_s( src );

		if ( n > 0 ) { return ( l > +n ) ? &src[0+n] : nullptr;	}
		if ( n < 0 ) { return ( l > -n ) ? &src[l+n] : nullptr;	}

		return nullptr;
	}
}

//----------------------------------------
// 行と列
//----------------------------------------
namespace opal {
	UINT strline_s( const char* src ){

		auto	n = 0UL;

		for ( const auto* p = src; ( p = strstr_s( p, "\n" ) ); p++ ) {
			n++;
		}

		return n;
	}
}

namespace opal {
	int strcoline( const char* src, UINT* colm, UINT* line ){

		if ( src ) {
			auto	mc = 0UL;
			auto	ml = 0UL;
			auto	nc = 0UL;
			auto	nl = 1UL;

			for ( const auto* p = src; *p; p++ ) {
				if ( *p == '\n' ) {
					if ( mc < nc ) {
						mc = nc;
						ml = nl;
					}
					nc = 0;
					nl++;
				} else {
					nc++;
				}
			}

			if ( mc < nc ) {
				mc = nc;
			}
			if ( mc == 0 ) {
				nl = 0;
			}

			if ( colm ) { *colm = mc; }
			if ( line ) { *line = nl; }

			return ml;
		}
		return -1;
	}
}

//========================================
// nullptr対応の文字列操作関数( WCHAR )
//========================================
namespace opal {
	wchar_t* wcsncpy_s ( wchar_t* dest, const wchar_t* src, size_t n ){

		return ( dest && src ) ? std::wcsncpy( dest, src, n ) : nullptr;
	}
}

//========================================
// wchar_t* ⇔ char* 変換
//========================================
namespace opal {
	size_t wtomlen( const wchar_t* p ){

		return ( p ) ? ::WideCharToMultiByte( CP_ACP, 0, ( const OLECHAR* )p, -1, nullptr, 0, nullptr, nullptr ) : 0;
	}
}

namespace opal {
	const char* wtomcpy( const wchar_t* p, char* q, size_t s ){

		if ( p && q ) {
			::WideCharToMultiByte( CP_ACP, 0, ( const OLECHAR* )p, -1, q, s, nullptr, nullptr );
			return q;
		}
		return nullptr;
	}
}

namespace opal {
	size_t mtowlen( const char* p ){

		return ( p ) ? ::MultiByteToWideChar( CP_ACP, 0, p, -1, nullptr, 0 ) : 0;
	}
}

namespace opal {
	const wchar_t* mtowcpy( const char* p, wchar_t* q, size_t s ){

		if ( p && q ) {
			::MultiByteToWideChar( CP_ACP, 0, p, -1, q, s );
			return q;
		}
		return nullptr;
	}
}

//========================================
// MTOW
//========================================
namespace opal {
	MTOW::MTOW() :
		size( 0 ),
		buff( nullptr )
	{}

	MTOW::MTOW( size_t s ) :
		size( s ),
		buff( new wchar_t[s] )
	{}

	MTOW::MTOW( const MTOW& mw ) :
		MTOW( mw.buff )
	{}

	MTOW::MTOW( const char* p ) :
		size( p ? mtowlen( p ) : 0 ),
		buff( size ? new wchar_t[size+1] : nullptr )
	{
		if ( buff ) {
			mtowcpy( p, buff, size );
		}
	}

	MTOW::MTOW( const wchar_t* p ) :
		size( p ? std::wcslen( p ) : 0 ),
		buff( size ? new wchar_t[size+1] : nullptr )
	{
		if ( buff ) {
			std::wcscpy( buff, p );
		}
	}

	MTOW::MTOW( const MTOW& a, const MTOW& b ) :
		size( 0 ),
		buff( nullptr )
	{
		const auto	sa = a.buff ? std::wcslen( a.buff ) : 0;
		const auto	sb = b.buff ? std::wcslen( b.buff ) : 0;

		if ( ( size = sa + sb ) ) {
			buff = new wchar_t[size+1];

			std::wmemset( buff, '0', size+1 );

			if ( sa ) { std::wcscpy( &buff[0 ], a.buff );	}
			if ( sb ) { std::wcscat( &buff[sa], b.buff );	}
		}
	}
	MTOW::MTOW( const MTOW&    a, const char*    b ) : MTOW(       a,   MTOW( b ) ) {}
	MTOW::MTOW( const char*    a, const MTOW&    b ) : MTOW( MTOW( a ),       b   ) {}
	MTOW::MTOW( const char*    a, const char*    b ) : MTOW( MTOW( a ), MTOW( b ) ) {}
	MTOW::MTOW( const MTOW&    a, const wchar_t* b ) : MTOW(       a,   MTOW( b ) ) {}
	MTOW::MTOW( const wchar_t* a, const MTOW&    b ) : MTOW( MTOW( a ),       b   ) {}
	MTOW::MTOW( const wchar_t* a, const wchar_t* b ) : MTOW( MTOW( a ), MTOW( b ) ) {}
	MTOW::MTOW( const wchar_t* a, const char*    b ) : MTOW( MTOW( a ), MTOW( b ) ) {}
	MTOW::MTOW( const char*    a, const wchar_t* b ) : MTOW( MTOW( a ), MTOW( b ) ) {}

	MTOW::~MTOW(){
		delete [] buff;
	}

	wchar_t* MTOW::operator ()( size_t s ){ delete [] buff; return new wchar_t[size=s]; }
	wchar_t* MTOW::operator []( size_t s ){ return ( s < size ) ? &buff[s] : nullptr; }
	wchar_t* MTOW::operator ()(    void ) const { return buff; }
	MTOW::operator const wchar_t*( void ) const { return buff;	}

	MTOW::operator size_t( void ) const { return size;	}

	MTOW operator + ( const MTOW&    a, const MTOW&    b ){ return MTOW( a, b ); }
	MTOW operator + ( const MTOW&    a, const char*    b ){ return MTOW( a, b ); }
	MTOW operator + ( const char*    a, const MTOW&    b ){ return MTOW( a, b ); }
	MTOW operator + ( const MTOW&    a, const wchar_t* b ){ return MTOW( a, b ); }
	MTOW operator + ( const wchar_t* a, const MTOW&    b ){ return MTOW( a, b ); }
}

//========================================
// sjisかどうか
//========================================
namespace opal {
	bool sjischeck( unsigned char c ){

		return ( ( ( 0x81 <= c ) && ( c <= 0x9f ) )||
				 ( ( 0xe0 <= c ) && ( c <= 0xfc ) ) ) ? true : false;
	}
}

//========================================
// 分割
//========================================
namespace opal {
	auto split( const std::string& st, const std::string& sp )->std::vector<std::string>{

		if ( const auto sl = sp.length(); !sl ) {
			return {st};
		} else {
			auto vs	= std::vector<std::string>();
			auto of	= std::string::size_type(0);

			while ( true ) {
				if ( const auto ps = st.find( sp, of ); ps == std::string::npos ) {
					vs.push_back( st.substr( of ) );
					break;
				} else {
					vs.push_back( st.substr( of, ps-of ) );
					of = ps + sl;
				}
			}
			return vs;
		}
	}
}

//========================================
// 数値→文字列
//========================================
namespace opal {
	std::string itos( int i ){

		const auto			n = std::snprintf( nullptr, 0, "%d", i );
		std::vector<char>	b( n+1 );

		std::sprintf( b.data(), "%d", i );

		return {b.data()};
	}
}

// End Of File
