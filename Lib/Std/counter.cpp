//========================================
// OPAL Game Library
//========================================
#include "Std/counter.hpp"

//========================================
// COUNTER
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
COUNTER::COUNTER() :
	c( 0 ),
	t( 0 )
{}
COUNTER::~COUNTER(){}

//----------------------------------------
// Setter/Getter
//----------------------------------------
void COUNTER::SetC( UINT n ){ c = n; }
void COUNTER::SetT( UINT n ){ t = n; }
UINT COUNTER::GetC( void ) const { return c; }
UINT COUNTER::GetT( void ) const { return t; }

//----------------------------------------
// 設定
//----------------------------------------
UINT COUNTER::operator =(  UINT n ){ c = 0; return t = n;	}
UINT COUNTER::operator ()( UINT n ){ c = n; return t;		}

//----------------------------------------
// 判定＆更新
//----------------------------------------
COUNTER::operator bool( void ) const { return c == t; }
COUNTER& COUNTER::operator ()( void ){ if ( c < t ) { ++c; } return *this; }

//----------------------------------------
// インクリメント/デクリメント
//----------------------------------------
UINT COUNTER::operator ++( void ){ return ++c; }
UINT COUNTER::operator --( void ){ return --c; }
UINT COUNTER::operator ++( int  ){ const auto n = c++; return n; }
UINT COUNTER::operator --( int  ){ const auto n = c--; return n; }

// End Of File
