//========================================
// OPAL Game Library
//========================================
#include "Std/guid.hpp"

//========================================
// 匿名IDを生成する
//========================================
using namespace opal;

//----------------------------------------
// GUID定義
// name AAAAAAAA-BBBB-CCCC-DDEE-FFGGHHIIJJKK
// { 0xAAAAAAAA, 0xBBBB, 0xCCCC, { 0xDD, 0xEE, 0xFF, 0xGG, 0xHH, 0xII, 0xJJ, 0xKK } }
// cywgin -> uuidgen
//----------------------------------------
namespace opal {
	auto guid_code( void )->UUID{

		UUID	u;

		::UuidCreate( &u );

		return u;
	}
}

namespace opal {
	auto guid_name( void )->std::string{

		char	p[] = "AAAAAAAA-BBBB-CCCC-DDEE-FFGGHHIIJJKK";
		UCHAR*	q;
		UUID	u;

		::UuidCreate( &u );
		::UuidToString( &u, &q );
		::sprintf_s( p, "%s", q );
		::RpcStringFree( &q );

		return std::string{p};
	}
}

// End Of File
