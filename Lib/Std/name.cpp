//========================================
// OPAL Game Library
//========================================
#include "Std/name.hpp"

//========================================
// ΔpΌOΗ
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
NAME::NAME() : name( nullptr ) {}
NAME::~NAME(){}

NAME::NAME( std::nullptr_t ) : name( nullptr ) {}

NAME::NAME( const std::string_view n ) : name( n.data() ? std::make_unique<std::string>( n.data() ) : nullptr ){}
NAME::NAME( const NAME&            n ) : name( n.name   ? std::make_unique<std::string>( *n.name  ) : nullptr ){}

NAME::NAME( const char* p, ... ){

	if ( p ) {
		va_list	list;

		va_start( list, p );

		const auto	s = vsnprintf( nullptr, 0, p, list );
		const auto	b = ( char* )std::malloc( s+1 );

		::vsprintf( b, p, list );
		va_end( list );

		name = std::make_unique<std::string>( b );

		std::free( b );
	}
}

//----------------------------------------
// ]Ώ
//----------------------------------------
const char* NAME::operator ()( void ) const { return name ? name->data() : nullptr; }

NAME::operator const char*( void ) const { return (*this)(); }

//----------------------------------------
// ]Ώ
//----------------------------------------
NAME::operator bool(   void ) const { return  (*this)();	}
bool NAME::operator !( void ) const { return !(*this)(); 	}

//----------------------------------------
// γό
//----------------------------------------
NAME& NAME::operator =( const NAME& s ){

	if ( s ) {
		name = std::make_unique<std::string>( s() );
	}
	return *this;
}

NAME& NAME::operator +=( const NAME& s ){

	if ( s ) {
		if ( const auto p = name->data() ) {
			name = std::make_unique<std::string>( std::string{p} + s() );
		} else {
			name = std::make_unique<std::string>( s() );
		}
	}
	return *this;
}


//----------------------------------------
// έθζΎ
//----------------------------------------
void NAME::Name( const NAME& n ){ new ( this ) NAME{n}; }
auto NAME::Name( void ) const->const char*{ return name ? name->data() : nullptr;	}
auto NAME::Leng( void ) const->std::size_t{ return name ? name->size() : 0;			}

//----------------------------------------
// O[o
//----------------------------------------
// δr
namespace opal {
 	bool operator ==( const NAME& a, const NAME& b ){ return std::string{a()} == std::string{b()};	}
 	bool operator ==( const NAME& a, const char* b ){ return a == NAME{b};	}
 	bool operator ==( const char* a, const NAME& b ){ return NAME{a} == b;	}
	bool operator !=( const NAME& a, const NAME& b ){ return !( a == b );	}
 	bool operator !=( const NAME& a, const char* b ){ return !( a == b );	}
 	bool operator !=( const char* a, const NAME& b ){ return !( a == b );	}
}

// A
namespace opal {
	NAME operator +( const NAME& a, const NAME& b ){

		if ( a && b ) { return { std::string{a}+std::string{b} }; }
		if ( a      ) { return a; }
		if ( b      ) { return b; }
		return {};
	}
	NAME operator +( const NAME& a, const char* b ){ return a+NAME{b};	}
	NAME operator +( const char* a, const NAME& b ){ return NAME{a}+b;	}
}

//----------------------------------------
// WideΞ
//----------------------------------------
void NAME::wide( const wchar_t* wp, size_t ws ){

	if ( wp ) {
		const auto	ms = wtomlen( wp );
		const auto	mp = ( char*    )std::malloc( sizeof( char    )*( ms+1 ) );
		const auto	bp = ( wchar_t* )std::malloc( sizeof( wchar_t )*( ws+1 ) );

		for ( auto i = 0UL; i < ws; i++ ) {
			bp[i] = wp[i];
		}
		bp[ws] = '\0';

		const auto	bs = wtomlen( bp );

		wtomcpy( bp, mp, bs );

		name = std::make_unique<std::string>( mp, 0, bs );

		std::free( bp );
		std::free( mp );
	}
}

void NAME::utf16( const char* up, size_t us ){

	wide( ( const wchar_t* )up, us );
}

void NAME::utf8(  const char* up, size_t us ){

	if ( up ) {
		const auto	sp = static_cast<LPCSTR>( up );
		const auto	ns = ::MultiByteToWideChar( CP_UTF8, 0, sp, -1, nullptr, 0 );
		const auto	nb = ( char* )std::malloc( sizeof( char ) * ( ns * 2 + 2 ) );

		::MultiByteToWideChar( CP_UTF8, 0, sp, -1, ( LPWSTR )nb, ns );

		utf16( nb, us );

		std::free( nb );
	}
}

// End Of File
