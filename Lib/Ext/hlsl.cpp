//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Ext/hlsl.hpp"

#include "Ext/xmath.hpp"

//========================================
// HLSL
//========================================
using namespace opal;

//========================================
// vector
//========================================
namespace opal {
	vector::vector() : v{0.0f,0.0f,0.0f,0.0f} {}
	vector::vector( float v0, float v1, float v2, float v3 ) : v{v0,v1,v2,v3} {}
	vector::vector( const vector*  v ) : v{v->v[0],v->v[1],v->v[2],v->v[3]} {}
	vector::vector( const vector&  v ) : v{ v.v[0], v.v[1], v.v[2], v.v[3]} {}
	vector::vector( const VECTOR2* v ) : v{v->x,v->y,0.0f,0.0f} {}
	vector::vector( const VECTOR2& v ) : v{ v.x, v.y,0.0f,0.0f} {}
	vector::vector( const VECTOR3* v ) : v{v->x,v->y,v->z,0.0f} {}
	vector::vector( const VECTOR3& v ) : v{ v.x, v.y, v.z,0.0f} {}
	vector::vector( const VECTOR4* v ) : v{v->x,v->y,v->z,v->w} {}
	vector::vector( const VECTOR4& v ) : v{ v.x, v.y, v.z, v.w} {}

	vector::operator float*( void ){ return v; }
}

//========================================
// matrix
//========================================
namespace opal {
	matrix::matrix() :
		_11( 0.0f ),_21( 0.0f ),_31( 0.0f ),_41( 0.0f ),
		_12( 0.0f ),_22( 0.0f ),_32( 0.0f ),_42( 0.0f ),
		_13( 0.0f ),_23( 0.0f ),_33( 0.0f ),_43( 0.0f ),
		_14( 0.0f ),_24( 0.0f ),_34( 0.0f ),_44( 0.0f )
	{}
	matrix::matrix( float m11, float m21, float m31, float m41,
					float m12, float m22, float m32, float m42,
					float m13, float m23, float m33, float m43,
					float m14, float m24, float m34, float m44 ) :
		_11( m11 ),_21( m21 ),_31( m31 ),_41( m41 ),
		_12( m12 ),_22( m22 ),_32( m32 ),_42( m42 ),
		_13( m13 ),_23( m23 ),_33( m33 ),_43( m43 ),
		_14( m14 ),_24( m24 ),_34( m34 ),_44( m44 )
	{}
	matrix::matrix( const float* m ) :
		_11( m[ 0] ),_21( m[ 1] ),_31( m[ 2] ),_41( m[ 3] ),
		_12( m[ 4] ),_22( m[ 5] ),_32( m[ 6] ),_42( m[ 7] ),
		_13( m[ 8] ),_23( m[ 9] ),_33( m[10] ),_43( m[11] ),
		_14( m[12] ),_24( m[13] ),_34( m[14] ),_44( m[15] )
	{}
	matrix::matrix( const matrix* m ) :
		_11( m->_11 ),_21( m->_21 ),_31( m->_31 ),_41( m->_41 ),
		_12( m->_12 ),_22( m->_22 ),_32( m->_32 ),_42( m->_42 ),
		_13( m->_13 ),_23( m->_23 ),_33( m->_33 ),_43( m->_43 ),
		_14( m->_14 ),_24( m->_24 ),_34( m->_34 ),_44( m->_44 )
	{}
	matrix::matrix( const matrix& m ) :
		_11( m._11 ),_21( m._21 ),_31( m._31 ),_41( m._41 ),
		_12( m._12 ),_22( m._22 ),_32( m._32 ),_42( m._42 ),
		_13( m._13 ),_23( m._23 ),_33( m._33 ),_43( m._43 ),
		_14( m._14 ),_24( m._24 ),_34( m._34 ),_44( m._44 )
	{}
	matrix::matrix( const MATRIX* m ) :
		_11( m->_11 ),_21( m->_21 ),_31( m->_31 ),_41( m->_41 ),
		_12( m->_12 ),_22( m->_22 ),_32( m->_32 ),_42( m->_42 ),
		_13( m->_13 ),_23( m->_23 ),_33( m->_33 ),_43( m->_43 ),
		_14( m->_14 ),_24( m->_24 ),_34( m->_34 ),_44( m->_44 )
	{}
	matrix::matrix( const MATRIX& m ) :
		_11( m._11 ),_21( m._21 ),_31( m._31 ),_41( m._41 ),
		_12( m._12 ),_22( m._22 ),_32( m._32 ),_42( m._42 ),
		_13( m._13 ),_23( m._23 ),_33( m._33 ),_43( m._43 ),
		_14( m._14 ),_24( m._24 ),_34( m._34 ),_44( m._44 )
	{}

	matrix::operator float*( void ){ return m; }
}

//========================================
// float4x3
//========================================
namespace opal {
	float4x3::float4x3() :
		_11( 0.0f ),_21( 0.0f ),_31( 0.0f ),_41( 0.0f ),
		_12( 0.0f ),_22( 0.0f ),_32( 0.0f ),_42( 0.0f ),
		_13( 0.0f ),_23( 0.0f ),_33( 0.0f ),_43( 0.0f )
	{}
	float4x3::float4x3( float m11, float m21, float m31, float m41,
						float m12, float m22, float m32, float m42,
						float m13, float m23, float m33, float m43 ) :
		_11( m11 ),_21( m21 ),_31( m31 ),_41( m41 ),
		_12( m12 ),_22( m22 ),_32( m32 ),_42( m42 ),
		_13( m13 ),_23( m23 ),_33( m33 ),_43( m43 )
	{}
	float4x3::float4x3( const float* m ) :
		_11( m[ 0] ),_21( m[ 1] ),_31( m[ 2] ),_41( m[ 3] ),
		_12( m[ 4] ),_22( m[ 5] ),_32( m[ 6] ),_42( m[ 7] ),
		_13( m[ 8] ),_23( m[ 9] ),_33( m[10] ),_43( m[11] )
	{}
	float4x3::float4x3( const matrix* m ) :
		_11( m->_11 ),_21( m->_21 ),_31( m->_31 ),_41( m->_41 ),
		_12( m->_12 ),_22( m->_22 ),_32( m->_32 ),_42( m->_42 ),
		_13( m->_13 ),_23( m->_23 ),_33( m->_33 ),_43( m->_43 )
	{}
	float4x3::float4x3( const matrix& m ) :
		_11( m._11 ),_21( m._21 ),_31( m._31 ),_41( m._41 ),
		_12( m._12 ),_22( m._22 ),_32( m._32 ),_42( m._42 ),
		_13( m._13 ),_23( m._23 ),_33( m._33 ),_43( m._43 )
	{}
	float4x3::float4x3( const MATRIX* m ) :
		_11( m->_11 ),_21( m->_21 ),_31( m->_31 ),_41( m->_41 ),
		_12( m->_12 ),_22( m->_22 ),_32( m->_32 ),_42( m->_42 ),
		_13( m->_13 ),_23( m->_23 ),_33( m->_33 ),_43( m->_43 )
	{}
	float4x3::float4x3( const MATRIX& m ) :
		_11( m._11 ),_21( m._21 ),_31( m._31 ),_41( m._41 ),
		_12( m._12 ),_22( m._22 ),_32( m._32 ),_42( m._42 ),
		_13( m._13 ),_23( m._23 ),_33( m._33 ),_43( m._43 )
	{}

	float4x3::operator float*( void ){ return m; }
}

//========================================
// ユーティリティ
//========================================
namespace  {
	inline MATRIX scale( const MATRIX& m, const SSIZEF& v, const SSIZEF& s ){

		MATRIX	a,b;

		MatrixScaling( &a,    s.w,    s.h, 1.0f );
		MatrixScaling( &b, +2/v.w, -2/v.h, 1.0f );

		return a * m * b;
	}

	inline VECTOR2 trans( const VECTOR3& v, const MATRIX& m ){

		return m * v;
	}

	inline MATRIX world( const MATRIX& m, const VECTOR3& v ){

		MATRIX	w = m;

		w._41 = v.x;
		w._42 = v.y;
		w._43 = v.z;

		return w;
	}

	inline VECTOR2 pivot( const VECTOR2& v, const SSIZEF& s ){

		return VECTOR2{ +v.x/s.w, +v.y/s.h };
	}

	inline MATRIX sampler( const VECTOR2& v, const SSIZEF& s ){

		MATRIX	m;

		MatrixScaling( &m, s.w, s.h, 1.0f );

		m._41 = v.x;
		m._42 = v.y;

		return m;
	}

	inline MATRIX sampler( const SRECTF& r ){

		MATRIX	m;

		MatrixScaling( &m, r.w, r.h, 1.0f );

		m._41 = r.x;
		m._42 = r.y;

		return m;
	}
}

namespace opal::hlsl {
	MATRIX Scale( const MATRIX& m, const SSIZEF& v,  const SSIZEF& s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF& v,  const SSIZEF* s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF& v,  float x, float y ){ return scale( {m}, { v }, {x,y} );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF* v,  const SSIZEF& s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF* v,  const SSIZEF* s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF* v,  float x, float y ){ return scale( {m}, { v }, {x,y} );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF& v,  const SSIZEF& s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF& v,  const SSIZEF* s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF& v,  float x, float y ){ return scale( {m}, { v }, {x,y} );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF* v,  const SSIZEF& s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF* v,  const SSIZEF* s  ){ return scale( {m}, { v }, { s } );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF* v,  float x, float y ){ return scale( {m}, { v }, {x,y} );	}
	MATRIX Scale( const MATRIX& m, float w, float h, const SSIZEF& s  ){ return scale( {m}, {w,h}, { s } );	}
	MATRIX Scale( const MATRIX& m, float w, float h, const SSIZEF* s  ){ return scale( {m}, {w,h}, { s } );	}
	MATRIX Scale( const MATRIX& m, float w, float h, float x, float y ){ return scale( {m}, {w,h}, {x,y} );	}
	MATRIX Scale( const MATRIX* m, float w, float h, const SSIZEF& s  ){ return scale( {m}, {w,h}, { s } );	}
	MATRIX Scale( const MATRIX* m, float w, float h, const SSIZEF* s  ){ return scale( {m}, {w,h}, { s } );	}
	MATRIX Scale( const MATRIX* m, float w, float h, float x, float y ){ return scale( {m}, {w,h}, {x,y} );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF& v                    ){ return scale( {m}, { v }, {1,1} );	}
	MATRIX Scale( const MATRIX& m, const SSIZEF* v                    ){ return scale( {m}, { v }, {1,1} );	}
	MATRIX Scale( const MATRIX& m, float w, float h                   ){ return scale( {m}, {w,h}, {1,1} );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF& v                    ){ return scale( {m}, { v }, {1,1} );	}
	MATRIX Scale( const MATRIX* m, const SSIZEF* v                    ){ return scale( {m}, { v }, {1,1} );	}
	MATRIX Scale( const MATRIX* m, float w, float h                   ){ return scale( {m}, {w,h}, {1,1} );	}

	VECTOR2 Trans( const VECTOR2* v, const MATRIX* m ){ return trans( { v }, {m} );	}
	VECTOR2 Trans( const VECTOR2* v, const MATRIX& m ){ return trans( { v }, {m} );	}
	VECTOR2 Trans( const VECTOR2& v, const MATRIX* m ){ return trans( { v }, {m} );	}
	VECTOR2 Trans( const VECTOR2& v, const MATRIX& m ){ return trans( { v }, {m} );	}
	VECTOR2 Trans( float x, float y, const MATRIX* m ){ return trans( {x,y}, {m} );	}
	VECTOR2 Trans( float x, float y, const MATRIX& m ){ return trans( {x,y}, {m} );	}

	MATRIX World( const MATRIX* m, const VECTOR3* v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX* m, const VECTOR3& v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX& m, const VECTOR3* v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX& m, const VECTOR3& v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX* m, const VECTOR2* v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX* m, const VECTOR2& v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX& m, const VECTOR2* v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX& m, const VECTOR2& v ){ return world( {m}, { v } );	}
	MATRIX World( const MATRIX* m, float x, float y, float z ){ return world( {m}, {x,y,z} );	}
	MATRIX World( const MATRIX& m, float x, float y, float z ){ return world( {m}, {x,y,z} );	}
	MATRIX World( const MATRIX* m                            ){ return world( {m}, {0,0,0} );	}
	MATRIX World( const MATRIX& m                            ){ return world( {m}, {0,0,0} );	}

	VECTOR2 Pivot( const VECTOR2* v, const SSIZEF& s  ){ return pivot( {v}, { s } );	}
	VECTOR2 Pivot( const VECTOR2& v, const SSIZEF& s  ){ return pivot( {v}, { s } );	}
	VECTOR2 Pivot( const VECTOR2* v, float w, float h ){ return pivot( {v}, {w,h} );	}
	VECTOR2 Pivot( const VECTOR2& v, float w, float h ){ return pivot( {v}, {w,h} );	}
	VECTOR2 Pivot( const VECTOR2* v                   ){ return pivot( {v}, {1,1} );	}
	VECTOR2 Pivot( const VECTOR2& v                   ){ return pivot( {v}, {1,1} );	}

	MATRIX Sampler( const VECTOR2* v, const SSIZEF* s  ){ return sampler( { v }, { s } );	}
	MATRIX Sampler( const VECTOR2* v, const SSIZEF& s  ){ return sampler( { v }, { s } );	}
	MATRIX Sampler( const VECTOR2& v, const SSIZEF* s  ){ return sampler( { v }, { s } );	}
	MATRIX Sampler( const VECTOR2& v, const SSIZEF& s  ){ return sampler( { v }, { s } );	}
	MATRIX Sampler( const VECTOR2* v, float w, float h ){ return sampler( { v }, {w,h} );	}
	MATRIX Sampler( const VECTOR2& v, float w, float h ){ return sampler( { v }, {w,h} );	}
	MATRIX Sampler( float x, float y, const SSIZEF* s  ){ return sampler( {x,y}, { s } );	}
	MATRIX Sampler( float x, float y, const SSIZEF& s  ){ return sampler( {x,y}, { s } );	}
	MATRIX Sampler( float x, float y, float w, float h ){ return sampler( {x,y}, {w,h} );	}
	MATRIX Sampler( const SRECTF* r ){ return sampler( {r} );	}
	MATRIX Sampler( const SRECTF& r ){ return sampler( {r} );	}
}

//========================================
// DEBUG
//========================================
#ifdef OPAL_DEBUG
namespace {
	inline void matrixprint( const matrix* m, int(*p)( const char*, ... ) ){

		if ( m && p ) {
			p( "matrix : %p\n", m );
			p( "[ %f %f %f %f ]\n", m->m[ 0],m->m[ 1],m->m[ 2],m->m[ 3] );
			p( "[ %f %f %f %f ]\n", m->m[ 4],m->m[ 5],m->m[ 6],m->m[ 7] );
			p( "[ %f %f %f %f ]\n", m->m[ 8],m->m[ 9],m->m[10],m->m[11] );
			p( "[ %f %f %f %f ]\n", m->m[12],m->m[13],m->m[14],m->m[15] );
			p( "\n" );
		}
	}
	inline void vectorprint( const vector* v, int(*p)( const char*, ... ) ){

		if ( v && p ) {
			p( "vector : %p [ %f %f %f ]\n", v, v->v[0], v->v[1], v->v[2], v->v[3] );
		}
	}
}

namespace opal::hlsl {
	void MatrixPrintd( const matrix* m ){ matrixprint( m, printd );	}
	void MatrixPrintf( const matrix* m ){ matrixprint( m, printf );	}
	void MatrixPrintd( const matrix& m ){ MatrixPrintd( &m ); }
	void MatrixPrintf( const matrix& m ){ MatrixPrintf( &m ); }

	void VectorPrintd( const vector* v ){ vectorprint( v, printd );	}
	void VectorPrintf( const vector* v ){ vectorprint( v, printf );	}
	void VectorPrintd( const vector& v ){ VectorPrintd( &v ); }
	void VectorPrintf( const vector& v ){ VectorPrintf( &v ); }
}
#endif

// End Of File
