//========================================
// OPAL Game Library
//========================================
#include "Ext/vector.hpp"
#include "Ext/matrix.hpp"
#include "Ext/quaternion.hpp"

//========================================
// クォータニアン
//========================================
using namespace opal;

//----------------------------------------
// クォータニアン
//----------------------------------------
//----------------------------------------
// operator
//----------------------------------------
QUATERNION::operator float*(){ return ( float* )this; }
QUATERNION::operator const float*() const { return ( const float* )this; }

QUATERNION QUATERNION::operator +() const { return QUATERNION{+x,+y,+z,+w}; }
QUATERNION QUATERNION::operator -() const { return QUATERNION{-x,-y,-z,-w}; }
QUATERNION QUATERNION::operator *( const QUATERNION& q ) const {
	return QUATERNION{+w*q.x - z*q.y + y*q.z + x*q.w,
					  +z*q.x + w*q.y - x*q.z + y*q.w,
					  -y*q.x + x*q.y + w*q.z + z*q.w,
					  -x*q.x - y*q.y - z*q.z + w*q.w};
}

QUATERNION QUATERNION::operator +( const QUATERNION& q ) const { return QUATERNION{x+q.x,y+q.y,z+q.z,w+q.w}; }
QUATERNION QUATERNION::operator -( const QUATERNION& q ) const { return QUATERNION{x-q.x,y-q.y,z-q.z,w-q.w}; }

QUATERNION QUATERNION::operator *( float r ) const { return QUATERNION{x*r,y*r,z*r,w*r};	}
QUATERNION QUATERNION::operator /( float r ) const { return QUATERNION{x/r,y/r,z/r,w/r};	}

QUATERNION operator * ( float r, const QUATERNION& q ){ return QUATERNION{q.x*r,q.y*r,q.z*r,q.w*r};	}

// End Of File
