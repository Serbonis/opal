//========================================
// OPAL Game Library
//========================================
#include "Ext/viewport.hpp"

#include "Ext/xmath.hpp"

//========================================
// r[|[g
//========================================
using namespace opal;

//========================================
// VPsń/tVPsń
//========================================
MATRIX* IVIEWPORT::Matrix( MATRIX* m, float w, float h ){

	if ( m ) {
		MatrixIdentity( m );

		m->_11 = +w/2.0f;
		m->_22 = -h/2.0f;
		m->_41 = +w/2.0f;
		m->_42 = +h/2.0f;
	}

	return m;
}

MATRIX* IVIEWPORT::Inverse( MATRIX* m, float w, float h ){

	return m ? MatrixInverse( m, nullptr, Matrix( m, w, h ) ) : nullptr;
}

MATRIX IVIEWPORT::Matrix(  float w, float h ){ MATRIX m; Matrix(  &m, w, h ); return m;	}
MATRIX IVIEWPORT::Inverse( float w, float h ){ MATRIX m; Inverse( &m, w, h ); return m;	}

// End Of File
