//========================================
// OPAL Game Library
//========================================
#include "Ext/vector.hpp"
#include "Ext/matrix.hpp"
#include "Ext/xmath.hpp"

//========================================
// �x�N�^�[
//========================================
using namespace opal;

//----------------------------------------
// DEBUG
//----------------------------------------
#ifdef OPAL_DEBUG
namespace {
	inline void vectorprint( const VECTOR2* v, int(*p)( const char*, ... ) ) {

		if ( p ) {
			if ( v ) {
				p( "�y %f %f �z\n", v->x, v->y );
			} else {
				p( "�y - - �z\n" );
			}
		}
	}

	inline void vectorprint( const VECTOR3* v, int(*p)( const char*, ... ) ) {

		if ( p ) {
			if ( v  ) {
				p( "�y %f %f %f �z\n", v->x, v->y, v->z );
			} else {
				p( "�y - - - �z\n" );
			}
		}
	}

	inline void vectorprint( const VECTOR4* v, int(*p)( const char*, ... ) ) {

		if ( p ) {
			if ( v  ) {
				p( "�y %f %f %f %f �z\n", v->x, v->y, v->z, v->w );
			} else {
				p( "�y - - - - �z\n" );
			}
		}
	}

	inline void vectorprint( const DVECTOR* v, int(*p)( const char*, ... ) ) {

		if ( p ) {
			if ( v  ) {
				p( "�y %f %f %f �z\n", v->x, v->y, v->h );
			} else {
				p( "�y - - - �z\n" );
			}
		}
	}
}

namespace opal {
	void VectorPrintd( const VECTOR2* v ){ vectorprint(  v, printd );	}
	void VectorPrintf( const VECTOR2* v ){ vectorprint(  v, printf );	}
	void VectorPrintd( const VECTOR2& v ){ vectorprint( &v, printd );	}
	void VectorPrintf( const VECTOR2& v ){ vectorprint( &v, printf );	}

	void VectorPrintd( const VECTOR3* v ){ vectorprint(  v, printd );	}
	void VectorPrintf( const VECTOR3* v ){ vectorprint(  v, printf );	}
	void VectorPrintd( const VECTOR3& v ){ vectorprint( &v, printd );	}
	void VectorPrintf( const VECTOR3& v ){ vectorprint( &v, printf );	}

	void VectorPrintd( const VECTOR4* v ){ vectorprint(  v, printd );	}
	void VectorPrintf( const VECTOR4* v ){ vectorprint(  v, printf );	}
	void VectorPrintd( const VECTOR4& v ){ vectorprint( &v, printd );	}
	void VectorPrintf( const VECTOR4& v ){ vectorprint( &v, printf );	}

	void VectorPrintd( const DVECTOR* v ){ vectorprint(  v, printd );	}
	void VectorPrintf( const DVECTOR* v ){ vectorprint(  v, printf );	}
	void VectorPrintd( const DVECTOR& v ){ vectorprint( &v, printd );	}
	void VectorPrintf( const DVECTOR& v ){ vectorprint( &v, printf );	}
}
#endif

// End Of File
