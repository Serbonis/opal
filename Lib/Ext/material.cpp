//========================================
// OPAL Game Library
//========================================
#include "Ext/material.hpp"

//========================================
// マテリアル
//========================================
using namespace opal;

//----------------------------------------
// 設定＆取得
//----------------------------------------
void MATERIAL::SetDiffuse(  float r, float g, float b, float a ){ Diffuse	= VCOLOR( r, g, b, a );	}
void MATERIAL::SetAmbient(  float r, float g, float b, float a ){ Ambient	= VCOLOR( r, g, b, a );	}
void MATERIAL::SetSpecular( float r, float g, float b, float a ){ Specular	= VCOLOR( r, g, b, a );	}
void MATERIAL::SetEmissive( float r, float g, float b, float a ){ Emissive	= VCOLOR( r, g, b, a );	}

void MATERIAL::SetDiffuse(  VCOLOR c ){ SetDiffuse(  c.r, c.g, c.b, c.a );	}
void MATERIAL::SetAmbient(  VCOLOR c ){ SetAmbient(  c.r, c.g, c.b, c.a );	}
void MATERIAL::SetSpecular( VCOLOR c ){ SetSpecular( c.r, c.g, c.b, c.a );	}
void MATERIAL::SetEmissive( VCOLOR c ){ SetEmissive( c.r, c.g, c.b, c.a );	}
void MATERIAL::SetPower( float p ){ Power = p;	}

VCOLOR MATERIAL::GetDiffuse(  void ) const { return Diffuse;	}
VCOLOR MATERIAL::GetAmbient(  void ) const { return Ambient; 	}
VCOLOR MATERIAL::GetSpecular( void ) const { return Specular;	}
VCOLOR MATERIAL::GetEmissive( void ) const { return Emissive;	}
float  MATERIAL::GetPower(    void ) const { return Power; 		}

MATERIAL& MATERIAL::DIFFUSE(  float r, float g, float b, float a ){ SetDiffuse(  r, g, b, a ); return *this; }
MATERIAL& MATERIAL::AMBIENT(  float r, float g, float b, float a ){ SetAmbient(  r, g, b, a ); return *this; }
MATERIAL& MATERIAL::SPECULAR( float r, float g, float b, float a ){ SetSpecular( r, g, b, a ); return *this; }
MATERIAL& MATERIAL::EMISSIVE( float r, float g, float b, float a ){ SetEmissive( r, g, b, a ); return *this; }
MATERIAL& MATERIAL::EMISSIVE( float r, float g, float b, float a, float p ){ SetEmissive( r, g, b, a ); SetPower( p ); return *this; }
MATERIAL& MATERIAL::DIFFUSE(  VCOLOR v ){ SetDiffuse(  v ); return *this; }
MATERIAL& MATERIAL::AMBIENT(  VCOLOR v ){ SetAmbient(  v ); return *this; }
MATERIAL& MATERIAL::SPECULAR( VCOLOR v ){ SetSpecular( v ); return *this; }
MATERIAL& MATERIAL::EMISSIVE( VCOLOR v ){ SetEmissive( v ); return *this; }
MATERIAL& MATERIAL::EMISSIVE( VCOLOR v,  float p ){ SetEmissive( v ); SetPower( p ); return *this; }

#ifdef OPAL_DEBUG
namespace opal {
	void MaterialDebug( const MATERIAL* m ){

		if ( m ) {
			MaterialDebug( *m );
		}
	}

	void MaterialDebug( const MATERIAL& m ){

		printd( "Diffuse : %f %f %f %f\n",    m.Diffuse.r, m.Diffuse.g, m.Diffuse.b, m.Diffuse.a  );
		printd( "Ambient : %f %f %f %f\n",    m.Ambient.r, m.Ambient.g, m.Ambient.b, m.Ambient.a  );
		printd( "Specular: %f %f %f %f\n",    m.Specular.r,m.Specular.g,m.Specular.b,m.Specular.a );
		printd( "Emissive: %f %f %f %f %f\n", m.Emissive.r,m.Emissive.g,m.Emissive.b,m.Emissive.a,m.Power );
	}

	void MaterialDump(  const MATERIAL* m ){
		if ( m ) {
			MaterialDump( *m );
		}
	}

	void MaterialDump(  const MATERIAL& m ){

		printf( "Diffuse : %f %f %f %f\n",    m.Diffuse.r, m.Diffuse.g, m.Diffuse.b, m.Diffuse.a  );
		printf( "Ambient : %f %f %f %f\n",    m.Ambient.r, m.Ambient.g, m.Ambient.b, m.Ambient.a  );
		printf( "Specular: %f %f %f %f\n",    m.Specular.r,m.Specular.g,m.Specular.b,m.Specular.a );
		printf( "Emissive: %f %f %f %f %f\n", m.Emissive.r,m.Emissive.g,m.Emissive.b,m.Emissive.a,m.Power );
	}
}
#endif

// End Of File
