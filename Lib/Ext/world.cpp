//========================================
// OPAL Game Library
//========================================
#include "Ext/world.hpp"

#include "Ext/xmath.hpp"

//========================================
// ワールド
//========================================
using namespace opal;

//========================================
// ワールド変換行列から各要素を抽出
//========================================
//----------------------------------------
// Scale
//----------------------------------------
namespace opal {
	VECTOR3* WorldScale(  const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			VECTOR3	lx = VECTOR3( m->_11, m->_12, m->_13 );
			VECTOR3	ly = VECTOR3( m->_21, m->_22, m->_23 );
			VECTOR3	lz = VECTOR3( m->_31, m->_32, m->_33 );

			p->x = Vec3Length ( &lx );
			p->y = Vec3Length ( &ly );
			p->z = Vec3Length ( &lz );
		}

		return p;
	}
}

//----------------------------------------
// Rotate
//----------------------------------------
namespace opal {
	VECTOR3* WorldRotate( const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			// 未実装
			p->x = 0.0f;
			p->y = 0.0f;
			p->z = 0.0f;
		}

		return p;
	}
}

//----------------------------------------
// Trans
//----------------------------------------
namespace opal {
	VECTOR3* WorldTrans(  const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			p->x = m->_41;
			p->y = m->_42;
			p->z = m->_43;
		}

		return p;
	}
}

//========================================
// ローカルの座標軸のワールドでの向き
//========================================
//----------------------------------------
// X軸
//----------------------------------------
namespace opal {
	VECTOR3* LocalAxisX( const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			p->x = m->_11;
			p->y = m->_12;
			p->z = m->_13;
		}

		return p;
	}
}

//----------------------------------------
// Y軸
//----------------------------------------
namespace opal {
	VECTOR3* LocalAxisY( const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			p->x = m->_21;
			p->y = m->_22;
			p->z = m->_23;
		}

		return p;
	}
}

//----------------------------------------
// Z軸
//----------------------------------------
namespace opal {
	VECTOR3* LocalAxisZ( const MATRIX* m, VECTOR3* v ){

		VECTOR3*	p = nullptr;

		if ( m ) {
			static VECTOR3	_v;

			p = ( v ) ? v : &_v;

			p->x = m->_31;
			p->y = m->_32;
			p->z = m->_33;
		}

		return p;
	}
}

// End Of File
