//========================================
// OPAL Game Library
//========================================
#include "Ext/polar.hpp"

//========================================
// 極座標
//========================================
using namespace opal;

#define EQ( a, b )	 FEQUAL( a, b )
#define NE( a, b )	!FEQUAL( a, b )

//========================================
// ポーラー2
//========================================
//----------------------------------------
// constructor
//----------------------------------------
POLAR2::POLAR2() : r( 0 ), p( 0 ) {}
POLAR2::POLAR2( float a, float b ) : r( a ), p( b ) {}
POLAR2::POLAR2( const POLAR2*  v ) : POLAR2( POLAR2_POLAR2(  v ) ) {}
POLAR2::POLAR2( const POLAR3*  v ) : POLAR2( POLAR3_POLAR2(  v ) ) {}
POLAR2::POLAR2( const POLAR2&  v ) : POLAR2( POLAR2_POLAR2(  v ) ) {}
POLAR2::POLAR2( const POLAR3&  v ) : POLAR2( POLAR3_POLAR2(  v ) ) {}

//----------------------------------------
// operator
//----------------------------------------
POLAR2& POLAR2::operator =( const POLAR2& v ){ r = v.r; p = v.p; return *this;	}
POLAR2& POLAR2::operator =( const POLAR3& v ){ r = v.r; p = v.p; return *this;	}

POLAR2 POLAR2::operator +() const { return POLAR2{ +r, p }; }
POLAR2 POLAR2::operator -() const { return POLAR2{ -r, p }; }
POLAR2 POLAR2::operator +( float v ) const { return POLAR2{ r+v, p };	}
POLAR2 POLAR2::operator -( float v ) const { return POLAR2{ r-v, p };	}
POLAR2 POLAR2::operator *( float v ) const { return POLAR2{ r*v, p };	}
POLAR2 POLAR2::operator /( float v ) const { return POLAR2{ r/v, p };	}
POLAR2 POLAR2::operator +( int   v ) const { return POLAR2{ r+(float)v, p };	}
POLAR2 POLAR2::operator -( int   v ) const { return POLAR2{ r-(float)v, p };	}
POLAR2 POLAR2::operator *( int   v ) const { return POLAR2{ r*(float)v, p };	}
POLAR2 POLAR2::operator /( int   v ) const { return POLAR2{ r/(float)v, p };	}

namespace opal {
	POLAR2 operator * ( float r, const POLAR2& v ){ return POLAR2{        r*v.r, v.p }; }
	POLAR2 operator * ( int   r, const POLAR2& v ){ return POLAR2{ (float)r*v.r, v.p }; }
}

bool POLAR2::operator ==( const POLAR2& v ) const { return EQ( r, v.r ) && EQ( p, v.p );	}
bool POLAR2::operator !=( const POLAR2& v ) const { return NE( r, v.r ) || NE( p, v.p );	}
bool POLAR2::operator ==( const POLAR3& v ) const { return EQ( r, v.r ) && EQ( p, v.p );	}
bool POLAR2::operator !=( const POLAR3& v ) const { return NE( r, v.r ) || NE( p, v.p );	}

POLAR2::operator float( void ) const { return r; }

//----------------------------------------
// DEBUG
//----------------------------------------
#ifdef OPAL_DEBUG
namespace {
	inline void polarprint( const POLAR2* v, int(*p)( const char*, ... ) ) {

		if ( v && p ) {
			p( "polar : %p [ %f %f ]\n", v, v->r, v->p );
		}
	}
}

namespace opal {
	void PolarPrintd( const POLAR2* v ){ polarprint( v, printd );	}
	void PolarPrintf( const POLAR2* v ){ polarprint( v, printf );	}
	void PolarPrintd( const POLAR2& v ){ PolarPrintd( &v ); }
	void PolarPrintf( const POLAR2& v ){ PolarPrintf( &v ); }
}
#endif

//========================================
// ポーラー3
//========================================
//----------------------------------------
// constructor
//----------------------------------------
POLAR3::POLAR3() : r( 0 ), p( 0 ), q( 0 ) {}
POLAR3::POLAR3( float c, float a, float b ) : r( c ), p( a ), q( b ) {}
POLAR3::POLAR3( const POLAR2*  v, float b ) : POLAR3( POLAR2_POLAR3( v, b ) ) {}
POLAR3::POLAR3( const POLAR2&  v, float b ) : POLAR3( POLAR2_POLAR3( v, b ) ) {}
POLAR3::POLAR3( const POLAR3*  v          ) : POLAR3( POLAR3_POLAR3( v ) ) {}
POLAR3::POLAR3( const POLAR3&  v          ) : POLAR3( POLAR3_POLAR3( v ) ) {}

//----------------------------------------
// operator
//----------------------------------------
POLAR3& POLAR3::operator =( const POLAR2& v ){ r = v.r; p = v.p;          return *this;	}
POLAR3& POLAR3::operator =( const POLAR3& v ){ r = v.r; p = v.p; q = v.q; return *this;	}

POLAR3 POLAR3::operator +() const { return POLAR3{ +r, p, q }; }
POLAR3 POLAR3::operator -() const { return POLAR3{ -r, p, q }; }
POLAR3 POLAR3::operator +( float v ) const { return POLAR3{ r+v, p, q };	}
POLAR3 POLAR3::operator -( float v ) const { return POLAR3{ r-v, p, q };	}
POLAR3 POLAR3::operator *( float v ) const { return POLAR3{ r*v, p, q };	}
POLAR3 POLAR3::operator /( float v ) const { return POLAR3{ r/v, p, q };	}
POLAR3 POLAR3::operator +( int   v ) const { return POLAR3{ r+(float)v, p, q };	}
POLAR3 POLAR3::operator -( int   v ) const { return POLAR3{ r-(float)v, p, q };	}
POLAR3 POLAR3::operator *( int   v ) const { return POLAR3{ r*(float)v, p, q };	}
POLAR3 POLAR3::operator /( int   v ) const { return POLAR3{ r/(float)v, p, q };	}

namespace opal {
	POLAR3 operator * ( float r, const POLAR3& v ){ return POLAR3{        r*v.r, v.p, v.q }; }
	POLAR3 operator * ( int   r, const POLAR3& v ){ return POLAR3{ (float)r*v.r, v.p, v.q }; }
}

bool POLAR3::operator ==( const POLAR2& v ) const { return EQ( r, v.r ) && EQ( p, v.p );	}
bool POLAR3::operator !=( const POLAR2& v ) const { return NE( r, v.r ) || NE( p, v.p );	}
bool POLAR3::operator ==( const POLAR3& v ) const { return EQ( r, v.r ) && EQ( p, v.p ) && EQ( q, v.q );	}
bool POLAR3::operator !=( const POLAR3& v ) const { return NE( r, v.r ) || NE( p, v.p ) || NE( q, v.q );	}

POLAR3::operator float( void ) const { return r; }

//----------------------------------------
// DEBUG
//----------------------------------------
#ifdef OPAL_DEBUG
namespace {
	inline void polarprint( const POLAR3* v, int(*p)( const char*, ... ) ) {

		if ( v && p ) {
			p( "polar : %p [ %f %f %f ]\n", v, v->r, v->p, v->q );
		}
	}
}

namespace opal {
	void PolarPrintd( const POLAR3* v ){ polarprint( v, printd );	}
	void PolarPrintf( const POLAR3* v ){ polarprint( v, printf );	}
	void PolarPrintd( const POLAR3& v ){ PolarPrintd( &v ); }
	void PolarPrintf( const POLAR3& v ){ PolarPrintf( &v ); }
}
#endif

//========================================
// 変換関数
//========================================
namespace opal {
	POLAR2  POLAR2_POLAR2(  const POLAR2& v          ){ return POLAR2( v.r, v.p    );	}
	POLAR3  POLAR2_POLAR3(  const POLAR2& v, float q ){ return POLAR3( v.r, v.p, q );	}
	POLAR3  POLAR3_POLAR3(  const POLAR3& v          ){ return POLAR3( v.r, v.p, 0 );	}
	POLAR2  POLAR3_POLAR2(  const POLAR3& v          ){ return POLAR2( v.r, v.p    );	}
	POLAR2  POLAR2_POLAR2(  const POLAR2* v          ){ return v ? POLAR2_POLAR2(  *v    ) : POLAR2{     };	}
	POLAR3  POLAR2_POLAR3(  const POLAR2* v, float q ){ return v ? POLAR2_POLAR3(  *v, q ) : POLAR3{0,0,q};	}
	POLAR3  POLAR3_POLAR3(  const POLAR3* v          ){ return v ? POLAR3_POLAR3(  *v    ) : POLAR3{     };	}
	POLAR2  POLAR3_POLAR2(  const POLAR3* v          ){ return v ? POLAR3_POLAR2(  *v    ) : POLAR2{     };	}
}

// End Of File
