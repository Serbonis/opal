//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Ext/xmath.hpp"

//========================================
// Extension
//========================================
using namespace opal;

#include <DirectXMath.h>

using namespace DirectX;

//----------------------------------------
//
//----------------------------------------
namespace opal::dx11 {
	inline auto	F(  float      v ){ return XMVECTOR{ v };	}
	inline auto V2( VECTOR2    v ){ return XMVECTOR{ v.x, v.y }; }
	inline auto V3( VECTOR3    v ){ return XMVECTOR{ v.x, v.y, v.z }; }
	inline auto V4( VECTOR4    v ){ return XMVECTOR{ v.x, v.y, v.z, v.w }; }
	inline auto Q(  QUATERNION q ){ return XMVECTOR{ q.x, q.y, q.z, q.w }; }
	inline auto M(  MATRIX     m ){

		return XMMATRIX{
			m._11,m._12,m._13,m._14,
			m._21,m._22,m._23,m._24,
			m._31,m._32,m._33,m._34,
			m._41,m._42,m._43,m._44,
		};
	}

	inline auto F(  FXMVECTOR  v ){ return float{      v.vector4_f32[0] };	}
	inline auto V2( FXMVECTOR  v ){ return VECTOR2{    v.vector4_f32[0], v.vector4_f32[1] };	}
	inline auto V3( FXMVECTOR  v ){ return VECTOR3{    v.vector4_f32[0], v.vector4_f32[1], v.vector4_f32[2] };	}
	inline auto V4( FXMVECTOR  v ){ return VECTOR4{    v.vector4_f32[0], v.vector4_f32[1], v.vector4_f32[2], v.vector4_f32[3] };	}
	inline auto Q(  FXMVECTOR  v ){ return QUATERNION{ v.vector4_f32[0], v.vector4_f32[1], v.vector4_f32[2], v.vector4_f32[3] };	}
	inline auto M(  FXMMATRIX  m ){

		return MATRIX{
			m._11,m._12,m._13,m._14,
			m._21,m._22,m._23,m._24,
			m._31,m._32,m._33,m._34,
			m._41,m._42,m._43,m._44,
		};
	}
}

//----------------------------------------
//
//----------------------------------------
namespace opal {
	using namespace dx11;

	VECTOR2* Vec2TransformCoord( VECTOR2* vo, const VECTOR2* vi, const MATRIX* m ){ return &( *vo = V2( ::XMVector2TransformCoord( V2(vi), M(m) ) ) );	}
	VECTOR3* Vec3TransformCoord( VECTOR3* vo, const VECTOR3* vi, const MATRIX* m ){ return &( *vo = V3( ::XMVector3TransformCoord( V3(vi), M(m) ) ) );	}

	VECTOR3* Vec3Normalize( VECTOR3* vo, const VECTOR3* vi ){ return &( *vo = V3( ::XMVector3Normalize( V3(vi) ) ) );	}

	float Vec2Length(   const VECTOR2* v ){ return F( ::XMVector2Length(   V2(v) ) ); }
	float Vec3Length(   const VECTOR3* v ){ return F( ::XMVector3Length(   V3(v) ) ); }
	float Vec2LengthSq( const VECTOR2* v ){ return F( ::XMVector2LengthSq( V2(v) ) ); }
	float Vec3LengthSq( const VECTOR3* v ){ return F( ::XMVector3LengthSq( V3(v) ) ); }

	float Vec2Dot( const VECTOR2* v1, const VECTOR2* v2 ){ return F( ::XMVector2Dot( V2(v1), V2(v2) ) ); }
	float Vec3Dot( const VECTOR3* v1, const VECTOR3* v2 ){ return F( ::XMVector3Dot( V3(v1), V3(v2) ) ); }

	VECTOR3* Vec3Cross( VECTOR3* v, const VECTOR3* v1, const VECTOR3* v2 ){ return &( *v = V3( ::XMVector3Cross( V3(v1), V3(v2) ) ) );	}

	VECTOR4* Vec2Transform( VECTOR4* vo, const VECTOR2* vi, const MATRIX* m ){ return &( *vo = V4( ::XMVector2Transform( V2(vi), M(m) ) ) );	}
	VECTOR4* Vec3Transform( VECTOR4* vo, const VECTOR3* vi, const MATRIX* m ){ return &( *vo = V4( ::XMVector3Transform( V3(vi), M(m) ) ) );	}
	VECTOR4* Vec4Transform( VECTOR4* vo, const VECTOR4* vi, const MATRIX* m ){ return &( *vo = V4( ::XMVector4Transform( V4(vi), M(m) ) ) );	}
}

//----------------------------------------
//
//----------------------------------------
namespace opal {
	using namespace dx11;

	MATRIX* MatrixRotationQuaternion( MATRIX* m, const QUATERNION* q ){ return &( *m = M( ::XMMatrixRotationQuaternion( Q( q) ) ));	}
	MATRIX* MatrixRotationQuaternion( MATRIX* m, const QUATERNION& q ){ return &( *m = M( ::XMMatrixRotationQuaternion( Q(&q) ) ));	}

	MATRIX* MatrixRotation( MATRIX* m ){ const QUATERNION q( 0, 0, 0, 1 ); return MatrixRotationQuaternion( m, &q );}
	MATRIX* MatrixRotationX( MATRIX* m, float x ){ return &( *m = M( ::XMMatrixRotationX( x ) ) );	}
	MATRIX* MatrixRotationY( MATRIX* m, float y ){ return &( *m = M( ::XMMatrixRotationY( y ) ) );	}
	MATRIX* MatrixRotationZ( MATRIX* m, float z ){ return &( *m = M( ::XMMatrixRotationZ( z ) ) );	}
	MATRIX* MatrixRotationAxis( MATRIX* m, const VECTOR3* v, float r ){ return &( *m = M( ::XMMatrixRotationAxis( V3(v), r ) ) );	}
	MATRIX* MatrixRotationYawPitchRoll( MATRIX* m, float y, float p, float r ){ return &( *m = M( ::XMMatrixRotationY( y ) ) * M( ::XMMatrixRotationX( p ) ) * M( ::XMMatrixRotationZ( r ) ) );	}

	MATRIX* MatrixTranslation(  MATRIX* m, float x, float y, float z ){ return &( *m = M( ::XMMatrixTranslation( x, y, z ) ) );	}
	MATRIX* MatrixScaling(      MATRIX* m, float x, float y, float z ){ return &( *m = M( ::XMMatrixScaling( x, y, z ) ) );	}

	MATRIX* MatrixIdentity( MATRIX* m ){ return &( *m = M( ::XMMatrixIdentity() ) );	}

	MATRIX* MatrixMultiply( MATRIX* m, const MATRIX* m1, const MATRIX* m2 ){ return &( *m = M( ::XMMatrixMultiply( M(m1), M(m2) ) ) );	}
	MATRIX* MatrixMultiply( MATRIX* m, const MATRIX* m1                   ){ return &( *m = M( ::XMMatrixMultiply( M(m ), M(m1) ) ) );	}

	MATRIX* MatrixLookAtLH( MATRIX* m, const VECTOR3* e, const VECTOR3* a, const VECTOR3* u ){ return &( *m = M( ::XMMatrixLookAtLH( V3(e),V3(a),V3(u) ) ) );	}
	MATRIX* MatrixInverse( MATRIX* m, float* d, const MATRIX* mi ){

		XMVECTOR	td;

		*m = M( ::XMMatrixInverse( &td, M(mi) ) );
		if ( d ) { *d = F(td); }

		return m;
	}
	MATRIX* MatrixPerspectiveFovLH( MATRIX* m, float v, float a, float n, float f ){ return &( *m = M( ::XMMatrixPerspectiveFovLH( v, a, n, f ) ) );	}
	MATRIX* MatrixOrthoLH( MATRIX* m, float w, float h, float n, float f ){ return &( *m = M( ::XMMatrixOrthographicLH( w, h, n, f ) ) );	}
	UINT    MatrixDecompose( VECTOR3* s, QUATERNION* r, VECTOR3* t, const MATRIX* m ){

		XMVECTOR	ts;
		XMVECTOR	tr;
		XMVECTOR	tt;

		bool	f = ::XMMatrixDecompose( &ts, &tr, &tt, M(m) );

		if ( s ) { *s = V3(ts);	}
		if ( r ) { *r = Q( tr);	}
		if ( t ) { *t = V3(tt);	}

		return f;
	}
	MATRIX* MatrixTranspose( MATRIX* m, const MATRIX* t ){ return &( *m = M( ::XMMatrixTranspose( M(t) ) ) );	}
};

//----------------------------------------
//
//----------------------------------------
namespace opal {
	using namespace dx11;

	QUATERNION* QuaternionRotationAxis( QUATERNION* q, const VECTOR3* v, float r ){ return &( *q = Q( ::XMQuaternionRotationAxis( V3(v), r ) ) );	}
	QUATERNION* QuaternionSlerp( QUATERNION* q, const QUATERNION* q1, const QUATERNION* q2, float t ){ return &( *q = Q( ::XMQuaternionSlerp( Q(q1), Q(q2), t ) ) );	}
}

// End Of File
