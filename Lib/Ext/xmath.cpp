//========================================
// OPAL Game Library
//========================================
#include "Std/const.hpp"
#include "Ext/xmath.hpp"

//========================================
// 座標系
//========================================
using namespace opal;

//----------------------------------------
// 変換関数
//----------------------------------------
namespace opal {
	float POLAR2_X( const POLAR2& v ){ return v.r * std::cos( v.p );	}
	float POLAR2_Y( const POLAR2& v ){ return v.r * std::sin( v.p );	}

	VECTOR2 POLAR2_VECTOR2( const POLAR2& v          ){ return VECTOR2{POLAR2_X(v),POLAR2_Y(v)   };	}
	VECTOR3 POLAR2_VECTOR3( const POLAR2& v, float z ){ return VECTOR3{POLAR2_X(v),POLAR2_Y(v), z};	}
	VECTOR2 POLAR2_VECTOR2( const POLAR2* v          ){ return v ? POLAR2_VECTOR2( *v    ) : VECTOR2{     };	}
	VECTOR3 POLAR2_VECTOR3( const POLAR2* v, float z ){ return v ? POLAR2_VECTOR3( *v, z ) : VECTOR3{0,0,z};	}
}

namespace opal {
	float POLAR3_X( const POLAR3& v ){ return v.r * std::sin( v.q ) * std::cos( v.p );	}
	float POLAR3_Y( const POLAR3& v ){ return v.r * std::sin( v.q ) * std::sin( v.p );	}
	float POLAR3_Z( const POLAR3& v ){ return v.r * std::cos( v.q );					}

	VECTOR2 POLAR3_VECTOR2( const POLAR3& v ){ return VECTOR2{POLAR3_X(v),POLAR3_Y(v)            };	}
	VECTOR3 POLAR3_VECTOR3( const POLAR3& v ){ return VECTOR3{POLAR3_X(v),POLAR3_Y(v),POLAR3_Z(v)};	}

	VECTOR2 POLAR3_VECTOR2( const POLAR3* v ){ return v ? POLAR3_VECTOR2( *v ) : VECTOR2{};	}
	VECTOR3 POLAR3_VECTOR3( const POLAR3* v ){ return v ? POLAR3_VECTOR3( *v ) : VECTOR3{};	}
}

namespace opal {
	POLAR2 VECTOR2_POLAR2( const VECTOR2& v          ){

		if ( FZERO( v.x ) || FZERO( v.y ) ) {
			const auto	r = std::sqrt( float{v} );
			const auto	p = v.x/r;

			return POLAR2{r,p};
		}
		return POLAR2{};
	}
	POLAR3 VECTOR2_POLAR3( const VECTOR2& v, float q ){

		if ( FZERO( v.x ) || FZERO( v.y ) ) {
			const auto	r = std::sqrt( float{v} );
			const auto	p = v.x/r;

			return POLAR3{r,p,q};
		}
		return POLAR3{0,0,q};
	}

	POLAR2 VECTOR2_POLAR2( const VECTOR2* v          ){ return v ? VECTOR2_POLAR2( *v    ) : POLAR2{};	}
	POLAR3 VECTOR2_POLAR3( const VECTOR2* v, float q ){ return v ? VECTOR2_POLAR3( *v, q ) : POLAR3{};	}
}

namespace opal {
	POLAR2 VECTOR3_POLAR2( const VECTOR3& v ){ return VECTOR2_POLAR2( VECTOR2{v.x,v.y} );	}
	POLAR3 VECTOR3_POLAR3( const VECTOR3& v ){

		if ( FZERO( v.x ) || FZERO( v.y ) ) {
			const auto	l = std::sqrt( float{VECTOR3{v}} );
			const auto	r = std::sqrt( float{VECTOR2{v}} );
			const auto	q =   r/l;
			const auto	p = v.x/r;

			return POLAR3{r,p,q};
		}
		return POLAR3{};
	}

	POLAR2 VECTOR3_POLAR2( const VECTOR3* v ){ return v ? VECTOR3_POLAR2( *v ) : POLAR2{};	}
	POLAR3 VECTOR3_POLAR3( const VECTOR3* v ){ return v ? VECTOR3_POLAR3( *v ) : POLAR3{};	}
}

namespace opal {
	float CYLIND_X( const CYLIND v ){ return +v.r * std::sin( v.p ); }
	float CYLIND_Y( const CYLIND v ){ return -v.r * std::cos( v.p ); }
	float CYLIND_Z( const CYLIND v ){ return v.h; }

	VECTOR2 CYLIND_VECTOR2( const CYLIND&  v ){ return VECTOR2{CYLIND_X(v),CYLIND_Y(v)            };	}
	VECTOR3 CYLIND_VECTOR3( const CYLIND&  v ){ return VECTOR3{CYLIND_X(v),CYLIND_Y(v),CYLIND_Z(v)};	}
	VECTOR2 CYLIND_VECTOR2( const CYLIND*  v ){ return v ? CYLIND_VECTOR2( *v ) : VECTOR2{};	}
	VECTOR3 CYLIND_VECTOR3( const CYLIND*  v ){ return v ? CYLIND_VECTOR3( *v ) : VECTOR3{};	}
}

namespace opal {
	CYLIND  VECTOR2_CYLIND( const VECTOR2& v, float h ){

		if ( FZERO( v.x ) || FZERO( v.y ) ) {
			const auto	r = std::sqrt( float{v} );
			const auto	p = v.x/r;

			return CYLIND{h,r,p};
		}
		return CYLIND{h};
	}
	CYLIND  VECTOR3_CYLIND( const VECTOR3& v          ){ return VECTOR2_CYLIND( VECTOR2{v.x,v.y}, v.z );	}
	CYLIND  VECTOR2_CYLIND( const VECTOR2* v, float h ){ return v ? VECTOR2_CYLIND( *v, h ) : CYLIND{};		}
	CYLIND  VECTOR3_CYLIND( const VECTOR3* v          ){ return v ? VECTOR3_CYLIND( *v    ) : CYLIND{};		}
}

//----------------------------------------
// 座標変換
//----------------------------------------
namespace opal {
	VECTOR2 operator * ( const MATRIX&  m, const VECTOR2& v ){ VECTOR4 vo; Vec2Transform( &vo, &v, &m ); return VECTOR2{vo}; }
	VECTOR2 operator * ( const VECTOR2& v, const MATRIX&  m ){ VECTOR4 vo; Vec2Transform( &vo, &v, &m ); return VECTOR2{vo}; }
	VECTOR3 operator * ( const MATRIX&  m, const VECTOR3& v ){ VECTOR4 vo; Vec3Transform( &vo, &v, &m ); return VECTOR3{vo}; }
	VECTOR3 operator * ( const VECTOR3& v, const MATRIX&  m ){ VECTOR4 vo; Vec3Transform( &vo, &v, &m ); return VECTOR3{vo}; }
	VECTOR4 operator * ( const MATRIX&  m, const VECTOR4& v ){ VECTOR4 vo; Vec4Transform( &vo, &v, &m ); return vo; }
	VECTOR4 operator * ( const VECTOR4& v, const MATRIX&  m ){ VECTOR4 vo; Vec4Transform( &vo, &v, &m ); return vo; }
};

//----------------------------------------
// 正規化
//----------------------------------------
namespace opal {
	VECTOR2 Normalize( const VECTOR2& v ){

		if ( const auto l = VectorLengthSq( v ); !FZERO( l ) ) {
			return VECTOR2{v.x/l, v.y/l };
		}
		return VECTOR2{};
	}

	VECTOR3 Normalize( const VECTOR3& v ){

		if ( const auto l = VectorLengthSq( v ); !FZERO( l ) ) {
			return VECTOR3{v.x/l,v.y/l,v.z/l };
		}
		return VECTOR3{};
	}

	VECTOR4 Normalize( const VECTOR4& v ){

		if ( const auto l = VectorLengthSq( v ); !FZERO( l ) ) {
			return VECTOR4{v.x/l,v.y/l,v.z/l,v.w/l};
		}
		return VECTOR4{};
	}

	VECTOR2 Normal( float r ){

		VECTOR4		v;
		const auto	a = VECTOR3{0,0,1};
		const auto	m = MatrixRotateY( r );

		Vec3Transform( &v, &a, &m );

		return VECTOR2{v.x,v.z};
	}
}

//----------------------------------------
// 長さ
//----------------------------------------
namespace opal {
	float VectorLength(   const VECTOR2& v ){ return v.Length();	}
	float VectorLength(   const VECTOR3& v ){ return v.Length();	}
	float VectorLength(   const VECTOR4& v ){ return v.Length();	}
	float VectorLengthSq( const VECTOR2& v ){ return v.LengthSq();	}
	float VectorLengthSq( const VECTOR3& v ){ return v.LengthSq();	}
	float VectorLengthSq( const VECTOR4& v ){ return v.LengthSq();	}

	float VectorLength(   const VECTOR2& a, const VECTOR2& b ){ return VectorLength(   a-b );	}
	float VectorLength(   const VECTOR3& a, const VECTOR3& b ){ return VectorLength(   a-b );	}
	float VectorLengthSq( const VECTOR2& a, const VECTOR2& b ){ return VectorLengthSq( a-b );	}
	float VectorLengthSq( const VECTOR3& a, const VECTOR3& b ){ return VectorLengthSq( a-b );	}
	float VectorLength(   const VECTOR2& a, const VECTOR3& b ){ return VectorLength(   VECTOR3{a-b} );	}
	float VectorLength(   const VECTOR3& a, const VECTOR2& b ){ return VectorLength(   VECTOR3{a-b} );	}
	float VectorLengthSq( const VECTOR2& a, const VECTOR3& b ){ return VectorLengthSq( VECTOR3{a-b} );	}
	float VectorLengthSq( const VECTOR3& a, const VECTOR2& b ){ return VectorLengthSq( VECTOR3{a-b} );	}
}

//----------------------------------------
// 内積＆外積
//----------------------------------------
namespace opal {
	float VectorDot( const VECTOR2& v1, const VECTOR2& v2 ){ return v1.x*v2.x + v1.y*v2.y;				}
	float VectorDot( const VECTOR3& v1, const VECTOR3& v2 ){ return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;	}

	float   VectorCross( const VECTOR2& v1, const VECTOR2& v2 ){ return v1.x*v2.y - v1.y*v2.x;	}
	VECTOR3 VectorCross( const VECTOR3& v1, const VECTOR3& v2 ){ return VECTOR3{ v1.y*v2.z-v1.z*v2.y, v1.z*v2.x-v1.x*v2.z, v1.x*v2.y-v1.y*v2.x }; }

	float VectorAdjacentSide( const VECTOR2& vp, const VECTOR2& vl ){

		const auto	lp = float{vl};
		const auto	dp = VectorDot( vp, vl );

		return FZERO( lp ) ? 0.0f : ( dp * dp )/lp;
	}
	float VectorOppositeSide( const VECTOR2& vp, const VECTOR2& vl ){

		const auto	lp = float{vl};
		const auto	cp = VectorCross( vp, vl );

		return FZERO( lp ) ? 0.0f : ( cp * cp )/lp;
	}
}

//----------------------------------------
// ベクトルの回転
//----------------------------------------
namespace opal {
	VECTOR3 VectorRotate( const VECTOR2& v, float r ){ return MatrixRotateZ( r ) * v;	}
	VECTOR3 VectorRotate( const VECTOR3& v, float r ){ return MatrixRotateY( r ) * v;	}
}

//----------------------------------------
// ベクトルの角度
//----------------------------------------
namespace {
	template<typename TYPE>float vectorangle( const TYPE& a, const TYPE& b ){

		const auto	al = std::sqrt( float{a} );
		const auto	bl = std::sqrt( float{b} );

		if ( !FZERO( al ) && !FZERO( bl ) ) {
			return std::acos( VectorDot( a, b )/( al * bl ) );
		}
		return 0;
	}

	inline float vectorangle2( const VECTOR2& a, const VECTOR2& b ){

		return vectorangle( a, b ) * ( ( VectorCross( a, b ) < 0 ) ? -1 : +1 );
	}

	inline float vectorangle3( const VECTOR3& a, const VECTOR3& b ){

		return vectorangle( a, b );
	}
}

namespace opal {
	float VectorAngle2( const VECTOR2& a, const VECTOR2& b ){ return vectorangle2( a, b );	}

	float VectorAngle2( const VECTOR2& a, float bx, float by ){ return vectorangle2( a, VECTOR2{bx,by} );	}
	float VectorAngle2( float ax, float ay, const VECTOR2& b ){ return vectorangle2( VECTOR2{ax,ay}, b );	}

	float VectorAngle2( float ax, float ay, float bx, float by ){ return vectorangle2( VECTOR2{ax,ay}, VECTOR2{bx,by} );	}

	float VectorAngle2( const VECTOR2& v ){ return vectorangle2( VECTOR2{0,1  }, v );				}
	float VectorAngle2( float x, float y ){ return vectorangle2( VECTOR2{0,1  }, VECTOR2{x,y} );	}

	float VectorAngle3( const VECTOR3& a, const VECTOR3& b ){ return vectorangle3( a, b );	}

	float VectorAngle3( const VECTOR3& a, float bx, float by, float bz ){ return vectorangle3( a, VECTOR3{bx,by,bz} );	}
	float VectorAngle3( float ax, float ay, float az, const VECTOR3& b ){ return vectorangle3( VECTOR3{ax,ay,az}, b );	}

	float VectorAngle3( float ax, float ay, float az, float bx, float by, float bz ){ return vectorangle3( VECTOR3{ax,ay,az}, VECTOR3{bx,by,bz} );	}

	float VectorAngle3( const VECTOR3& v          ){ return vectorangle3( VECTOR3{0,0,1}, v );				}
	float VectorAngle3( float x, float y, float z ){ return vectorangle3( VECTOR3{0,0,1}, VECTOR3{x,y,z} );	}

	VECTOR2 AngleVector2( float r ){ return {+std::sin( r ),-std::cos( r )};	}

	//VECTOR3 AngleVector3( float r, float q );
}

//----------------------------------------
// 線形補間
//----------------------------------------
namespace opal {
	VECTOR3 Lerp( const VECTOR3& v0, const VECTOR3& v1, float r ){ return v0 * ( 1-r ) + r * v1; }
}

//----------------------------------------
// 任意軸の移動
//----------------------------------------
namespace opal {
	VECTOR3 AxisTrans( const VECTOR3& vx, const VECTOR3& vy, const VECTOR3& vz, float tx, float ty, float tz ){

		VECTOR3	v;

		const auto	dx = tx * vx;
		const auto	dy = ty * vy;
		const auto	dz = tz * vz;

		v.x = dx.x + dy.x + dz.x;
		v.y = dx.y + dy.y + dz.y;
		v.z = dx.z + dy.z + dz.z;

		return v;
	}

	VECTOR3* AxisTrans( VECTOR3* v, const VECTOR3& vx, const VECTOR3& vy, const VECTOR3& vz, float tx, float ty, float tz ){

		return v ? &( *v = AxisTrans( vx, vy, vz, tx, ty, tz ) ) : nullptr;
	}
}

//----------------------------------------
// 任意軸の回転
//----------------------------------------
namespace opal {
	VECTOR3 AxisRotate( const VECTOR3& vx, const VECTOR3& vy, const VECTOR3& vz, float rx, float ry, float rz ){

		// 未実装...
		VECTOR3	v;

		v.x = rx;
		v.y = ry;
		v.z = rz;

		return v;
	}

	VECTOR3* AxisRotate( VECTOR3* v, const VECTOR3& vx, const VECTOR3& vy, const VECTOR3& vz, float rx, float ry, float rz ){

		return v ? &( *v = AxisRotate( vx, vy, vz, rx, ry, rz ) ) : nullptr;
	}

	MATRIX AxisRotateMatrix( const VECTOR3& v, float r ) {

		MATRIX	 	m;
		QUATERNION	q(0,0,0,1);

		QuaternionRotationAxis( &q, &v, r );
		MatrixRotationQuaternion( &m, &q );

		return m;
	}

	MATRIX*  AxisRotateMatrix( MATRIX* m, const VECTOR3& v, float r ) {

		return m ? &( *m = AxisRotateMatrix( v, r ) ) : nullptr;
	};

	VECTOR3 AxisRotateVector( const MATRIX& m ) {

		VECTOR3	v;

		Vec3TransformCoord( &v, &v, &m );
		Vec3Normalize( &v, &v );

		return v;
	}

	VECTOR3* AxisRotateVector( VECTOR3* v, const MATRIX&  m ){

		return v ? &( *v = AxisRotateVector( m ) ) : nullptr;
	}
}

//----------------------------------------
// 変換行列
//----------------------------------------
namespace opal {
	MATRIX* MatrixScale(  MATRIX* m, float x, float y, float z ){ return MatrixScaling(              m, x,   y,   z   );	}
	MATRIX* MatrixRotate( MATRIX* m, float x, float y, float z ){ return MatrixRotationYawPitchRoll( m, y,   x,   z   );	}
	MATRIX* MatrixTrans(  MATRIX* m, float x, float y, float z ){ return MatrixTranslation(          m, x,   y,   z   );	}
	MATRIX* MatrixScale(  MATRIX* m, const VECTOR3& v          ){ return MatrixScaling(              m, v.x, v.y, v.z );	}
	MATRIX* MatrixRotate( MATRIX* m, const VECTOR3& v          ){ return MatrixRotationYawPitchRoll( m, v.y, v.x, v.z );	}
	MATRIX* MatrixTrans(  MATRIX* m, const VECTOR3& v          ){ return MatrixTranslation(          m, v.x, v.y, v.z );	}

	MATRIX  MatrixScale(  float x, float y, float z ){ MATRIX m; MatrixScaling(              &m, x,   y,   z   ); return m;	}
	MATRIX  MatrixRotate( float x, float y, float z ){ MATRIX m; MatrixRotationYawPitchRoll( &m, y,   x,   z   ); return m;	}
	MATRIX  MatrixTrans(  float x, float y, float z ){ MATRIX m; MatrixTranslation(          &m, x,   y,   z   ); return m;	}
	MATRIX  MatrixScale(  const VECTOR3& v          ){ MATRIX m; MatrixScaling(              &m, v.x, v.y, v.z ); return m;	}
	MATRIX  MatrixRotate( const VECTOR3& v          ){ MATRIX m; MatrixRotationYawPitchRoll( &m, v.y, v.x, v.z ); return m;	}
	MATRIX  MatrixTrans(  const VECTOR3& v          ){ MATRIX m; MatrixTranslation(          &m, v.x, v.y, v.z ); return m;	}

	MATRIX* MatrixScaleX(  MATRIX* m, float x ){ return MatrixScale(  m, x, 1, 1 );	}
	MATRIX* MatrixScaleY(  MATRIX* m, float y ){ return MatrixScale(  m, 1, y, 1 );	}
	MATRIX* MatrixScaleZ(  MATRIX* m, float z ){ return MatrixScale(  m, 1, 1, z );	}
	MATRIX* MatrixRotateX( MATRIX* m, float x ){ return MatrixRotate( m, x, 0, 0 );	}
	MATRIX* MatrixRotateY( MATRIX* m, float y ){ return MatrixRotate( m, 0, y, 0 );	}
	MATRIX* MatrixRotateZ( MATRIX* m, float z ){ return MatrixRotate( m, 0, 0, z );	}
	MATRIX* MatrixTransX(  MATRIX* m, float x ){ return MatrixTrans(  m, x, 0, 0 );	}
	MATRIX* MatrixTransY(  MATRIX* m, float y ){ return MatrixTrans(  m, 0, y, 0 );	}
	MATRIX* MatrixTransZ(  MATRIX* m, float z ){ return MatrixTrans(  m, 0, 0, z );	}

	MATRIX MatrixScaleX(  float x ){ return MatrixScale(  x, 1, 1 );	}
	MATRIX MatrixScaleY(  float y ){ return MatrixScale(  1, y, 1 );	}
	MATRIX MatrixScaleZ(  float z ){ return MatrixScale(  1, 0, z );	}
	MATRIX MatrixRotateX( float x ){ return MatrixRotate( x, 0, 0 );	}
	MATRIX MatrixRotateY( float y ){ return MatrixRotate( 0, y, 0 );	}
	MATRIX MatrixRotateZ( float z ){ return MatrixRotate( 0, 0, z );	}
	MATRIX MatrixTransX(  float x ){ return MatrixTrans(  x, 0, 0 );	}
	MATRIX MatrixTransY(  float y ){ return MatrixTrans(  0, y, 0 );	}
	MATRIX MatrixTransZ(  float z ){ return MatrixTrans(  0, 0, z );	}
}

namespace opal {
	//----------------------------------------
	//  |  x   0   0   0  |
	//  |  0   y   0   0  |
	//  |  0   0   1   0  |
	//  |  0   0   0   1  |
	//----------------------------------------
	MATRIX MatrixScale( float x, float y ){

		MATRIX	 m{OPAL_IDM};

		m._11 = x;
		m._22 = y;

		return m;
	}

	//----------------------------------------
	//  | +c  +s   0   0  |
	//  | -s  +c   0   0  |
	//  |  0   0   1   0  |
	//  |  0   0   0   1  |
	//----------------------------------------
	MATRIX MatrixRotate( float z ){

		MATRIX	 m{OPAL_IDM};

		m._11 = +cos( z );	m._12 = +sin( z );
		m._21 = -sin( z );	m._22 = +cos( z );

		return m;
	}

	//----------------------------------------
	//  |  1  0   0   0   |
	//  |  0  1   0   0   |
	//  |  x  y   1   0   |
	//  |  0  0   0   1   |
	//----------------------------------------
	MATRIX MatrixTrans( float x, float y ){

		MATRIX	 m{OPAL_IDM};

		m._31 = x;
		m._32 = y;

		return m;
	}

	MATRIX MatrixScale( const VECTOR2& v ){ return MatrixScale( v.x, v.y ); }
	MATRIX MatrixTrans( const VECTOR2& v ){ return MatrixScale( v.x, v.y ); }
}

//----------------------------------------
// 回転行列→オイラー角
//----------------------------------------
#include "Utl/degree.hpp"

namespace opal {
	float MatrixAngleX( const MATRIX& m ){

		if ( FEQUAL( ::fabsf( m._32 ), -1.0f ) ) { return RAD( +90.0f ); }
		if ( FEQUAL( ::fabsf( m._32 ), +1.0f ) ) { return RAD( -90.0f ); }

		return ::asinf( m._32 );
	}

	float MatrixAngleY( const MATRIX& m ){

		if ( FEQUAL( ::fabsf( m._32 ), -1.0f ) ) { return 0.0f;	}
		if ( FEQUAL( ::fabsf( m._32 ), +1.0f ) ) { return 0.0f;	}

		return ::atan2f( -m._31, m._33 );
	}

	float MatrixAngleZ( const MATRIX& m ){

		if ( FEQUAL( ::fabsf( m._32 ), -1.0f ) ) { return ::atan2f( m._21, m._11 );	}
		if ( FEQUAL( ::fabsf( m._32 ), +1.0f ) ) { return ::atan2f( m._21, m._11 );	}

		return ::atan2f( -m._12, m._22 );
	}

	VECTOR3 MatrixAngle( const MATRIX& m ){

		if ( FEQUAL( ::fabsf( m._32 ), -1.0f ) ) { return VECTOR3{ RAD( +90.0f ), 0.0f, ::atan2f( m._21, m._11 ) };	}
		if ( FEQUAL( ::fabsf( m._32 ), +1.0f ) ) { return VECTOR3{ RAD( -90.0f ), 0.0f, ::atan2f( m._21, m._11 ) };	}

		return VECTOR3( ::asinf(   m._32 ),
						::atan2f( -m._31, m._33 ),
						::atan2f( -m._12, m._22 ) );
	}
}

//----------------------------------------
// 位置部分だけ代入/取得
//----------------------------------------
namespace {
	MATRIX* TransMatrix_( MATRIX* m, const VECTOR3* t ){ if ( m && t ){ m->_41 = t->x; m->_42 = t->y; m->_43 = t->z; } return m;	}
	MATRIX* TransMatrix_( MATRIX* m, const VECTOR2* t ){ if ( m && t ){ m->_41 = t->x; m->_42 = t->y;                } return m;	}
	MATRIX* TransMatrix_( MATRIX* m,          float z ){ if ( m      ){ m->_43 = z;                                  } return m;	}
	MATRIX* TransMatrix_( MATRIX* m, const VECTOR3& t ){ return TransMatrix_( m, &t );	}
	MATRIX* TransMatrix_( MATRIX* m, const VECTOR2& t ){ return TransMatrix_( m, &t );	}
}

namespace opal {
	MATRIX* TransMatrix( MATRIX* m, const VECTOR3* t          ){ return TransMatrix_( m,  t             );	}
	MATRIX* TransMatrix( MATRIX* m, const VECTOR2* t          ){ return TransMatrix_( m,  t             );	}
	MATRIX* TransMatrix( MATRIX* m, const VECTOR3& t          ){ return TransMatrix_( m, &t             );	}
	MATRIX* TransMatrix( MATRIX* m, const VECTOR2& t          ){ return TransMatrix_( m, &t             );	}
	MATRIX* TransMatrix( MATRIX* m, float x, float y, float z ){ return TransMatrix_( m, VECTOR3{x,y,z} );	}
	MATRIX* TransMatrix( MATRIX* m, float x, float y          ){ return TransMatrix_( m, VECTOR2{x,y  } );	}
	MATRIX* TransMatrix( MATRIX* m,                   float z ){ return TransMatrix_( m, float{      z} );	}
}

namespace {
	VECTOR3* TransMatrix_( VECTOR3* t, const MATRIX* m ){ if ( t ) { *t = m ? VECTOR3{m->_41,m->_42, m->_43} : VECTOR3{}; } return t; }

	VECTOR3  TransMatrix_( const MATRIX* m          ){ return m ? VECTOR3{m->_41,m->_42,m->_43} : VECTOR3{0,0,0};	}
	VECTOR3  TransMatrix_( const MATRIX* m, float z ){ return m ? VECTOR3{m->_41,m->_42,m->_43} : VECTOR3{0,0,z};	}
}

namespace opal {
	VECTOR3* TransMatrix( VECTOR3* t, const MATRIX& m ){ return TransMatrix_(  t, &m );	}
	VECTOR3* TransMatrix( VECTOR3& t, const MATRIX& m ){ return TransMatrix_( &t, &m );	}

	VECTOR3  TransMatrix( const MATRIX* m          ){ return TransMatrix_(  m    );	}
	VECTOR3  TransMatrix( const MATRIX* m, float z ){ return TransMatrix_(  m, z );	}
	VECTOR3  TransMatrix( const MATRIX& m          ){ return TransMatrix_( &m    );	}
	VECTOR3  TransMatrix( const MATRIX& m, float z ){ return TransMatrix_( &m, z );	}
}

//----------------------------------------
// 初期姿勢
//----------------------------------------
namespace opal {
	MATRIX* InitMatrix( MATRIX* m                            ){ MatrixRotation( m ); 								return m; }
	MATRIX* InitMatrix( MATRIX* m, const VECTOR3& t          ){ MatrixRotation( m ); TransMatrix( m, t          );	return m; }
	MATRIX* InitMatrix( MATRIX* m, const float*   t          ){ MatrixRotation( m ); TransMatrix( m, VECTOR3{t} );	return m; }
	MATRIX* InitMatrix( MATRIX* m, float x, float y, float z ){ MatrixRotation( m ); TransMatrix( m, x, y, z    );	return m; }
}

// End Of File
