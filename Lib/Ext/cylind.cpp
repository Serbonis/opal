//========================================
// OPAL Game Library
//========================================
#include "Ext/cylind.hpp"

//========================================
// 円柱座標
//========================================
using namespace opal;

#define EQ( a, b )	 FEQUAL( a, b )
#define NE( a, b )	!FEQUAL( a, b )

//========================================
// シリンダー
//========================================
//----------------------------------------
// constructor
//----------------------------------------
CYLIND::CYLIND() : h( 0 ), r( 0 ), p( 0 ) {}
CYLIND::CYLIND( float a, float b, float c ) : h( a ), r( b ), p( c ) {}
CYLIND::CYLIND( const CYLIND*  v ) : CYLIND( CYLIND_CYLIND(  v ) ) {}
CYLIND::CYLIND( const CYLIND&  v ) : CYLIND( CYLIND_CYLIND(  v ) ) {}

//----------------------------------------
// operator
//----------------------------------------
CYLIND& CYLIND::operator =( const CYLIND& v ){ h = v.h; r = v.r; p = v.p; return *this;	}

CYLIND CYLIND::operator +() const { return CYLIND{ +h, r, p }; }
CYLIND CYLIND::operator -() const { return CYLIND{ -h, r, p }; }
CYLIND CYLIND::operator +( float v ) const { return CYLIND{ h+v, r, p };	}
CYLIND CYLIND::operator -( float v ) const { return CYLIND{ h-v, r, p };	}
CYLIND CYLIND::operator *( float v ) const { return CYLIND{ h*v, r, p };	}
CYLIND CYLIND::operator /( float v ) const { return CYLIND{ h/v, r, p };	}
CYLIND CYLIND::operator +( int   v ) const { return CYLIND{ h+(float)v, r, p };	}
CYLIND CYLIND::operator -( int   v ) const { return CYLIND{ h-(float)v, r, p };	}
CYLIND CYLIND::operator *( int   v ) const { return CYLIND{ h*(float)v, r, p };	}
CYLIND CYLIND::operator /( int   v ) const { return CYLIND{ h/(float)v, r, p };	}

namespace opal {
	CYLIND operator * ( float r, const CYLIND& v ){ return CYLIND{        r*v.h, v.r, v.p }; }
	CYLIND operator * ( int   r, const CYLIND& v ){ return CYLIND{ (float)r*v.h, v.r, v.p }; }
}

bool CYLIND::operator ==( const CYLIND& v ) const { return EQ( h, v.h ) && EQ( r, v.r ) && EQ( p, v.p );	}
bool CYLIND::operator !=( const CYLIND& v ) const { return NE( h, v.h ) || NE( r, v.r ) || NE( p, v.p );	}

CYLIND::operator float( void ) const { return h; }

//----------------------------------------
// 変換関数
//----------------------------------------
namespace opal {
	CYLIND CYLIND_CYLIND( const CYLIND& v ){ return CYLIND( v.h, v.r, v.h );	}
	CYLIND CYLIND_CYLIND( const CYLIND* v ){ return v ? CYLIND_CYLIND( *v ) : CYLIND{};	}
}

//----------------------------------------
// DEBUG
//----------------------------------------
#ifdef OPAL_DEBUG
namespace {
	inline void cylindprint( const CYLIND* v, int(*p)( const char*, ... ) ){

		if ( v && p ) {
			p( "cylind : %p [ %f %f %f ]\n", v, v->h, v->r, v->p );
		}
	}
}

namespace opal {
	void CylindPrintd( const CYLIND* v ){ cylindprint( v, printd );	}
	void CylindPrintf( const CYLIND* v ){ cylindprint( v, printf );	}
	void CylindPrintd( const CYLIND& v ){ CylindPrintd( &v ); }
	void CylindPrintf( const CYLIND& v ){ CylindPrintf( &v ); }
}
#endif

// End Of File
