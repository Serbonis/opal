//========================================
// プリミティブ描画
//========================================
#include "rsrcs.hpp"

//----------------------------------------
// リソース確保
//----------------------------------------
template<typename TYPE> auto resource( const std::string& s ){

	if ( const auto	r = opal::RSRCS::Search<TYPE>( s ) ) {
		return r;
	}
	return opal::RSRCS::Generate<TYPE>( s );
}

//----------------------------------------
// リソース確保(SURFACE)
//----------------------------------------
template<typename TYPE> auto resource( const std::string& s, UINT vw, UINT vh ){

	if ( const auto	r = opal::RSRCS::Search<TYPE>( s ) ) {
		return r;
	}
	return opal::RSRCS::Generate<TYPE>( s, vw, vh );
}

//----------------------------------------
// リソース確保(POLYGON)
//----------------------------------------
template<typename TYPE> auto polygon( const std::string& s ){

	if ( const auto	r = opal::RSRCS::Search<TYPE>( s ) ) {
		return r;
	}
	return opal::RSRCS::Generate<TYPE>( opal::FILED{ s } );
}

// End Of File

