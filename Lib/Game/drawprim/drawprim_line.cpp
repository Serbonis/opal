//========================================
// プリミティブ描画
//========================================
#include "drawprim_line.hpp"

#include "drawprim_primitive.inl"

//----------------------------------------
// ライン
//----------------------------------------
#include "line2.hpp"

inline static void prim_line2( float n, float x, float y, float w, float h, UINT c ){

	if ( const auto	p = primitive<LINE2>( "prim_line2" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( c );
		p->SetLine( w, h );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// 描画関数
//----------------------------------------
namespace drawprim {
	void line( float n, float x, float y, float w, float h, UINT c ) { prim_line2( n, x, y, w, h, c );	}
	void line(          float x, float y, float w, float h, UINT c ) { prim_line2( 0, x, y, w, h, c );	}
};

// End Of File
