//========================================
// プリミティブ描画
//========================================
#include "drawprim_texture.hpp"

#include "drawprim_primitive.inl"
#include "drawprim_resource.inl"

//----------------------------------------
// テクスチャ
//----------------------------------------
#include "rect2.hpp"
#include "texture.hpp"

inline static void rect_texture( float n, const TEXTURE* t, float x, float y, float w, float h, float rx, float ry, float rw, float rh ){

	if ( t ) {
		if ( const auto	p = primitive<RECT2>( "rect_texture" ) ) {
			p->SetLayer( n );
			p->SetTrans( x, y, 0 );
			p->SetAlpha( true );
			p->SetTexture( t );
			p->SetRect( rx, ry, rw, rh );
			p->SetSize( w, h );
			p->Coordinate();
			p->DrawMain();
		}
	}
}

//----------------------------------------
// 描画関数
//----------------------------------------
namespace drawprim {
	void texture( float n, const TEXTURE* t, float x, float y, float w, float h, float rx, float ry, float rw, float rh ){ rect_texture( n,  t, x, y, w, h, 0, 0, rw, rh );	}
	void texture(          const TEXTURE* t, float x, float y, float w, float h, float rx, float ry, float rw, float rh ){ rect_texture( 0,  t, x, y, w, h, 0, 0, rw, rh );	}
	void texture( float n, const TEXTURE* t, float x, float y, float w, float h, float rw, float rh                     ){ rect_texture( n,  t, x, y, w, h, 0, 0, rw, rh );	}
	void texture(          const TEXTURE* t, float x, float y, float w, float h, float rw, float rh                     ){ rect_texture( 0,  t, x, y, w, h, 0, 0, rw, rh );	}
	void texture( float n, const TEXTURE* t, float x, float y, float w, float h                                         ){ rect_texture( n,  t, x, y, w, h, 0, 0,  1,  1 );	}
	void texture(          const TEXTURE* t, float x, float y, float w, float h                                         ){ rect_texture( 0,  t, x, y, w, h, 0, 0,  1,  1 );	}
	void texture( float n, const TEXTURE& t, float x, float y, float w, float h, float rx, float ry, float rw, float rh ){ rect_texture( n, &t, x, y, w, h, 0, 0, rw, rh );	}
	void texture(          const TEXTURE& t, float x, float y, float w, float h, float rx, float ry, float rw, float rh ){ rect_texture( 0, &t, x, y, w, h, 0, 0, rw, rh );	}
	void texture( float n, const TEXTURE& t, float x, float y, float w, float h, float rw, float rh                     ){ rect_texture( n, &t, x, y, w, h, 0, 0, rw, rh );	}
	void texture(          const TEXTURE& t, float x, float y, float w, float h, float rw, float rh                     ){ rect_texture( 0, &t, x, y, w, h, 0, 0, rw, rh );	}
	void texture( float n, const TEXTURE& t, float x, float y, float w, float h                                         ){ rect_texture( n, &t, x, y, w, h, 0, 0,  1,  1 );	}
	void texture(          const TEXTURE& t, float x, float y, float w, float h                                         ){ rect_texture( 0, &t, x, y, w, h, 0, 0,  1,  1 );	}
}

// End Of File
