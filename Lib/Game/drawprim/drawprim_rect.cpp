//========================================
// �v���~�e�B�u�`��
//========================================
#include "drawprim_rect.hpp"

#include "drawprim_primitive.inl"

//----------------------------------------
// ��`(�h)
//----------------------------------------
#include "rect2.hpp"

inline static void prim_rectf( float n, float x, float y, float w, float h, UINT c ) {

	if ( const auto	p = primitive<RECT2>( "prim_rectf" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( c );
		p->SetSize( w, h );
		p->SetTexture();
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// ��`(�g)
//----------------------------------------
#include "waku2.hpp"

inline static void prim_rectl( float n, float x, float y, float w, float h, UINT c ) {

	if ( const auto	p = primitive<WAKU2>( "prim_rectl" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( c );
		p->SetSize( w, h );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// ��`(�F�ŐU�蕪��)
//----------------------------------------
inline static void prim_rect( float n, float x, float y, float w, float h, UINT c ) {

	const auto	str = opal::COLOR_A( c );
	const auto	col = opal::COLOR_Alpha( c, 0xff );

	if ( str == 0xff ) {
		prim_rectf( n, x, y, w, h, col );
	} else {
		prim_rectl( n, x, y, w, h, col );
	}
}

//----------------------------------------
// �`��֐�
//----------------------------------------
namespace drawprim {
	void rect( float n, float x, float y, float w, float h, UINT c ) { prim_rect( n, x, y, w, h, c );	}
	void rect(          float x, float y, float w, float h, UINT c ) { prim_rect( 0, x, y, w, h, c );	}
}

// End Of File
