//========================================
// プリミティブ描画
//========================================
#include "drawprim_image.hpp"

#include "drawprim_primitive.inl"
#include "drawprim_resource.inl"

//----------------------------------------
// 画像
//----------------------------------------
#include "rect2.hpp"
#include "texture.hpp"

inline static void rect_image( float n, const char* s, float x, float y, float w, float h, float r, float tx, float ty, float tw, float th ){

	if ( const auto t = resource<TEXTURE>( s ) ) {
		const auto	p = primitive<RECT2>( "rect_image" );

		if ( !w ) { w = t->Width();  }
		if ( !h ) { h = t->Height(); }

		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetRotate( opal::RADIAN( r ) );
		p->SetAlpha( true );
		p->SetColor( opal::WHITE );
		p->SetTexture( t );
		p->SetSize( w, h );
		p->SetOffset( w/2, h/2 );
		p->SetPivot();
		p->SetRect( tx, ty, tw, th );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// 描画関数
//----------------------------------------
namespace drawprim {
	void image( float n, float x, float y, float w, float h, const char* s, float r, float tx, float ty, float tw, float th ){ rect_image( n, s, x, y, w, h, r, tx, ty, tw, th ); }
	void image(          float x, float y, float w, float h, const char* s, float r, float tx, float ty, float tw, float th ){ rect_image( 0, s, x, y, w, h, r, tx, ty, tw, th ); }
	void image( float n, float x, float y, float w, float h, const char* s,          float tx, float ty, float tw, float th ){ rect_image( n, s, x, y, w, h, 0, tx, ty, tw, th ); }
	void image(          float x, float y, float w, float h, const char* s,          float tx, float ty, float tw, float th ){ rect_image( 0, s, x, y, w, h, 0, tx, ty, tw, th ); }
	void image( float n, float x, float y, float w, float h, const char* s, float r                                         ){ rect_image( n, s, x, y, w, h, r, 0,  0,  1,  1  ); }
	void image(          float x, float y, float w, float h, const char* s, float r                                         ){ rect_image( 0, s, x, y, w, h, r, 0,  0,  1,  1  ); }
	void image( float n, float x, float y, float w, float h, const char* s                                                  ){ rect_image( n, s, x, y, w, h, 0, 0,  0,  1,  1  ); }
	void image(          float x, float y, float w, float h, const char* s                                                  ){ rect_image( 0, s, x, y, w, h, 0, 0,  0,  1,  1  ); }
	void image( float n, float x, float y,                   const char* s, float r                                         ){ rect_image( n, s, x, y, 0, 0, r, 0,  0,  1,  1  ); }
	void image(          float x, float y,                   const char* s, float r                                         ){ rect_image( 0, s, x, y, 0, 0, r, 0,  0,  1,  1  ); }
	void image( float n, float x, float y,                   const char* s                                                  ){ rect_image( n, s, x, y, 0, 0, 0, 0,  0,  1,  1  ); }
	void image(          float x, float y,                   const char* s                                                  ){ rect_image( 0, s, x, y, 0, 0, 0, 0,  0,  1,  1  ); }
}

//----------------------------------------
// 描画クラス
//----------------------------------------
namespace drawprim {
	struct IMAGE_PRIM : RECT2 {};
}

namespace drawprim {
	Image::Image() : prim{[](){ const auto p = new IMAGE_PRIM; p->Init(); return p; }(), []( auto p ){ p->Free(); }}
	{
		prim->SetSize();
		prim->SetPivot();
	}
	Image::~Image(){}

	void Image::Size( float w, float h ){

		prim->SetSize( w, h );
		prim->SetPivot();
	}
}

namespace drawprim {
	void Image::Rotate( float r ){

		prim->SetRotate( r );
	}
}

namespace drawprim {
	void Image::Texture( const char* s ){

		if ( const auto t = resource<TEXTURE>( s ) ) {
			prim->SetTexture( t );
		}
	}
}

namespace drawprim {
	void Image::Draw( float x, float y ){

		prim->SetTrans( x, y );
		prim->Coordinate();
		prim->DrawMain();
	}
}

// End Of File
