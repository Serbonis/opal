#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// 円形
//----------------------------------------
namespace drawprim {
	extern void circle( float n, float x, float y, float r, UINT c = FillColor() );
	extern void circle(          float x, float y, float r, UINT c = FillColor() );
}

// End Of File
