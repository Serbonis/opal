#pragma once
//========================================
// プリミティブ描画
//========================================

//----------------------------------------
// 画像
//----------------------------------------
namespace drawprim {
	extern void image( float n, float x, float y, float w, float h, const char* s, float tx, float ty, float tw, float th );
	extern void image(          float x, float y, float w, float h, const char* s, float tx, float ty, float tw, float th );
	extern void image( float n, float x, float y, float w, float h, const char* s );
	extern void image(          float x, float y, float w, float h, const char* s );
	extern void image( float n, float x, float y,                   const char* s );
	extern void image(          float x, float y,                   const char* s );

	extern void image( float n, float x, float y, float w, float h, const char* s, float r, float tx, float ty, float tw, float th );
	extern void image(          float x, float y, float w, float h, const char* s, float r, float tx, float ty, float tw, float th );
	extern void image( float n, float x, float y, float w, float h, const char* s, float r );
	extern void image(          float x, float y, float w, float h, const char* s, float r );
	extern void image( float n, float x, float y,                   const char* s, float r );
	extern void image(          float x, float y,                   const char* s, float r );
}

#include <memory>

namespace drawprim {
	class Image {
	public:
		explicit Image();
		virtual ~Image();

	protected:
		std::shared_ptr<struct IMAGE_PRIM>	prim{nullptr};

	public:
		void Size( float w, float h );
		void Rotate( float r );
		void Texture( const char* s );
		void Draw( float x, float y );
	};
}

// End Of File
