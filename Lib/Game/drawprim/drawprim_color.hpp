#pragma once
//========================================
// プリミティブ描画
//========================================
#include "draw.hpp"

//----------------------------------------
// 色指定
//----------------------------------------
namespace drawprim {
	extern UINT LineColor( UINT = opal::WHITE );
	extern UINT FillColor( UINT = opal::WHITE );
	extern UINT LineColor( UCHAR, UCHAR, UCHAR );
	extern UINT FillColor( UCHAR, UCHAR, UCHAR );
}

// End Of File
