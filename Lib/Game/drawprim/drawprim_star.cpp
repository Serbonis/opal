//========================================
// �v���~�e�B�u�`��
//========================================
#include "drawprim_star.hpp"

#include "drawprim_primitive.inl"
#include "drawprim_resource.inl"

//----------------------------------------
// ���`(�h)
//----------------------------------------
#include "shape.hpp"
#include "polygon_dx11d.hpp"

inline static void prim_starf( float n, float x, float y, float r, UINT c ){

	if ( const auto d = polygon<POLYGON_STAR>( "polygon_star" ) ) {
		if ( const auto	p = primitive<SHAPE>( "prim_starf" ) ) {
			p->SetLayer( n );
			p->SetTrans( x, y, 0 );
			p->SetColor( c );
			p->SetScale( r/100.f );
			p->SetPolygon( d );
			p->Coordinate();
			p->DrawMain();
		}
	}
}

//----------------------------------------
// ���`(�g)
//----------------------------------------
#include "line2.hpp"

class HOSHI final : public LINE2 {
private:
	static inline constexpr opal::VECTOR2	v[] =
		{
			{ opal::COS_M090 * 100.f, opal::SIN_M090 * 100.f, },	// 1
			{ opal::COS_P054 * 100.f, opal::SIN_P054 * 100.f, },	// 3
			{ opal::COS_P198 * 100.f, opal::SIN_P198 * 100.f, },	// 5
			{ opal::COS_M018 * 100.f, opal::SIN_M018 * 100.f, },	// 2
			{ opal::COS_P126 * 100.f, opal::SIN_P126 * 100.f, },	// 4
		};

public:
	void Init( void ) override {

		LINE2::Init();
		LINE2::SetLoop( true );

		LINE2::SetNode( std::size( v ) );
		for ( auto i = 0UL; i < std::size( v ); i++ ) {
			LINE2::SetNode( i, v[i] );
		}
	}
};

inline static void prim_starl( float n, float x, float y, float r, UINT c ){

	if ( const auto	p = primitive<HOSHI>( "prim_starl" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( c );
		p->SetScale( r/100.f );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// ���`(�F�ŐU�蕪��)
//----------------------------------------
inline static void prim_star( float n, float x, float y, float r, UINT c ){

	const auto	str = opal::COLOR_A( c );
	const auto	col = opal::COLOR_Alpha( c, 0xff );

	if ( str == 0xff ) {
		prim_starf( n, x, y, r, col );
	} else {
		prim_starl( n, x, y, r, col );
	}
}

//----------------------------------------
// �`��֐�
//----------------------------------------
namespace drawprim {
	void star( float n, float x, float y, float r, UINT c ){ prim_star( n, x, y, r, c );	}
	void star(          float x, float y, float r, UINT c ){ prim_star( 0, x, y, r, c );	}
}

// End Of File
