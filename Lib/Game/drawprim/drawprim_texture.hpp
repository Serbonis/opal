#pragma once
//========================================
// プリミティブ描画
//========================================
class TEXTURE;

//----------------------------------------
// テクスチャ
//----------------------------------------
namespace drawprim {
	extern void texture( float n, const TEXTURE* t, float x, float y, float w, float h, float rx, float ry, float rw, float rh );
	extern void texture(          const TEXTURE* t, float x, float y, float w, float h, float rx, float ry, float rw, float rh );
	extern void texture( float n, const TEXTURE* t, float x, float y, float w, float h, float rw, float rh                     );
	extern void texture(          const TEXTURE* t, float x, float y, float w, float h, float rw, float rh                     );
	extern void texture( float n, const TEXTURE* t, float x, float y, float w, float h                                         );
	extern void texture(          const TEXTURE* t, float x, float y, float w, float h                                         );
	extern void texture( float n, const TEXTURE& t, float x, float y, float w, float h, float rx, float ry, float rw, float rh );
	extern void texture(          const TEXTURE& t, float x, float y, float w, float h, float rx, float ry, float rw, float rh );
	extern void texture( float n, const TEXTURE& t, float x, float y, float w, float h, float rw, float rh                     );
	extern void texture(          const TEXTURE& t, float x, float y, float w, float h, float rw, float rh                     );
	extern void texture( float n, const TEXTURE& t, float x, float y, float w, float h                                         );
	extern void texture(          const TEXTURE& t, float x, float y, float w, float h                                         );
}

// End Of File
