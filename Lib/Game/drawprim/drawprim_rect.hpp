#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// 矩形
//----------------------------------------
namespace drawprim {
	extern void rect( float n, float x, float y, float w, float h, UINT c = FillColor() );
	extern void rect(          float x, float y, float w, float h, UINT c = FillColor() );
}

// End Of File
