//========================================
// プリミティブ描画
//========================================
#include "drawprim_vram.hpp"

#include "drawprim_primitive.inl"
#include "drawprim_resource.inl"

//----------------------------------------
// VRAM
//----------------------------------------
#include "rect2.hpp"
#include "surface.hpp"

inline static void rect_vram
(
 float n, const char* s,
 float x, float y, float w, float h,
 float rx, float ry, float rw, float rh,
 UINT vw, UINT vh, const BYTE* vd, const DWORD* vc,
 bool df = true
 ){

	if ( const auto t = resource<CANVAS>( s, vw, vh ) ) {
		if ( df ) {
			t->Begin();
			if ( auto r = SURFACE_LOCK( t ) ) {
				for ( auto i = 0UL; i < vh; i++ ) {
					for ( auto j = 0UL; j < vw; j++ ) {
						r.SetPixel( j, i, vc[*vd++] );
					}
				}
			}
			t->End();
		}

		if ( const auto p = primitive<RECT2>( "rect_vram" ) ) {
			p->SetLayer( n );
			p->SetTrans( x, y, 0 );
			p->SetAlpha( true );
			p->SetTexture( t );
			p->SetRect( rx, ry, rw, rh );
			p->SetSize( w, h );
			p->Coordinate();
			p->DrawMain();
		}
	}
}

//----------------------------------------
// 描画関数
//----------------------------------------
namespace drawprim {
	void vram( float n, const char* s, float x, float y, float w, float h, UINT vw, UINT vh, const BYTE* vd, const DWORD* vc ){

		const auto	rw = ( float )vw/1024;
		const auto	rh = ( float )vh/1024;

		rect_vram( n, s, x, y, w, h, 0, 0, rw, rh, vw, vh, vd, vc );
	}

	void vram( const char* s, float x, float y, float w, float h, UINT vw, UINT vh, const BYTE* vd, const DWORD* vc ){

		const auto	rw = ( float )vw/1024;
		const auto	rh = ( float )vh/1024;

		rect_vram( 0, s, x, y, w, h, 0, 0, rw, rh, vw, vh, vd, vc );
	}
}

// End Of File
