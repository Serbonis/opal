#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// 文字
//----------------------------------------
namespace drawprim {
	extern void FontColor( UINT  c = opal::WHITE );
	extern void FontSize(  float s = 12.f );
}

namespace drawprim {
	extern void print( float n, float x, float y, const char* s,  ... );
	extern void print(          float x, float y, const char* s,  ... );
};

// End Of File
