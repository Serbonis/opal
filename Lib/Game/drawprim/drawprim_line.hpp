#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// ライン
//----------------------------------------
namespace drawprim {
	extern void line( float n, float x, float y, float w, float h, UINT c = FillColor() );
	extern void line(          float x, float y, float w, float h, UINT c = FillColor() );
}

// End Of File
