//========================================
// �v���~�e�B�u�`��
//========================================
#include "drawprim_circle.hpp"

#include "drawprim_primitive.inl"
#include "drawprim_resource.inl"

//----------------------------------------
// �~�`(�h)
//----------------------------------------
#include "shape.hpp"
#include "polygon_dx11d.hpp"

inline static void prim_maruf( float n, float x, float y, float r, UINT c ){

	if ( const auto d = polygon<POLYGON_CIRCLE>( "polygon_circle" ) ) {
		if ( const auto	p = primitive<SHAPE>( "prim_maruf" ) ) {
			p->SetLayer( n );
			p->SetTrans( x, y, 0 );
			p->SetColor( c );
			p->SetScale( r/100.f );
			p->SetPolygon( d );
			p->Coordinate();
			p->DrawMain();
		}
	}
}

//----------------------------------------
// �~�`(�g)
//----------------------------------------
#include "maru2.hpp"

inline static void prim_marul( float n, float x, float y, float r, UINT c ){

	if ( const auto	p = primitive<MARU2>( "prim_marul" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( c );
		p->SetSize( r );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// �~�`(�F�ŐU�蕪��)
//----------------------------------------
inline static void prim_circle( float n, float x, float y, float r, UINT c ){

	const auto	str = opal::COLOR_A( c );
	const auto	col = opal::COLOR_Alpha( c, 0xff );

	if ( str == 0xff ) {
		prim_maruf( n, x, y, r, col );
	} else {
		prim_marul( n, x, y, r, col );
	}
}

//----------------------------------------
// �`��֐�
//----------------------------------------
namespace drawprim {
	void circle( float n, float x, float y, float r, UINT c ){ prim_circle( n, x, y, r, c );	}
	void circle(          float x, float y, float r, UINT c ){ prim_circle( 0, x, y, r, c );	}
}

// End Of File
