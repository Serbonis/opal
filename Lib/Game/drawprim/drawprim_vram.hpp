#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// VRAM
//----------------------------------------
namespace drawprim {
	extern void vram( float n, const char* s, float x, float y, float w, float h, UINT vw, UINT vh, const BYTE* vd, const DWORD* vc );
	extern void vram(          const char* s, float x, float y, float w, float h, UINT vw, UINT vh, const BYTE* vd, const DWORD* vc );
}

// End Of File
