#pragma once
//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// 星形
//----------------------------------------
namespace drawprim {
	extern void star( float n, float x, float y, float r, UINT c = FillColor() );
	extern void star(          float x, float y, float r, UINT c = FillColor() );
}

// End Of File
