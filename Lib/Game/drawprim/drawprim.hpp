#pragma once
//========================================
// プリミティブ描画
//========================================

//----------------------------------------
// 色指定
//----------------------------------------
#include "drawprim_color.hpp"

//----------------------------------------
// 描画関数
//----------------------------------------
#include "drawprim_line.hpp"
#include "drawprim_rect.hpp"
#include "drawprim_circle.hpp"
#include "drawprim_star.hpp"
#include "drawprim_font.hpp"
#include "drawprim_image.hpp"
#include "drawprim_texture.hpp"
#include "drawprim_vram.hpp"

// End Of File

