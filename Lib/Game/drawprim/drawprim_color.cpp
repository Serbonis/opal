//========================================
// プリミティブ描画
//========================================
#include "drawprim_color.hpp"

//----------------------------------------
// 色指定
//----------------------------------------
namespace drawprim {
	UINT LineColor( UINT c ){ return opal::COLOR_Alpha( c, 0x00 );	}
	UINT FillColor( UINT c ){ return opal::COLOR_Alpha( c, 0xff );	}

	UINT LineColor( UCHAR r, UCHAR g, UCHAR b ){ return opal::COLOR_Alpha( r, g, b, 0x00 );	}
	UINT FillColor( UCHAR r, UCHAR g, UCHAR b ){ return opal::COLOR_Alpha( r, g, b, 0xff );	}
}

// End Of File
