//========================================
// プリミティブ描画
//========================================
#include "drawprim_font.hpp"

#include "drawprim_primitive.inl"

//----------------------------------------
// 文字設定
//----------------------------------------
namespace {
	std::vector<char>	font_buff;

	UINT	font_color	= opal::WHITE;
	float	font_size	= 1.f;
}

namespace drawprim {
	void FontClor( UINT  c ){ font_color = c;		}
	void FontSize( float s ){ font_size  = s/12.f;	}
}

//----------------------------------------
// 文字
//----------------------------------------
#include "font.hpp"

inline static void font_print( float n, float x, float y ){

	if ( const auto p = primitive<FONT>( "prim_font" ) ) {
		p->SetLayer( n );
		p->SetTrans( x, y, 0 );
		p->SetColor( font_color );
		p->SetScale( font_size );
		p->String( font_buff.data() );
		p->Coordinate();
		p->DrawMain();
	}
}

//----------------------------------------
// 描画関数
//----------------------------------------
namespace drawprim {
	void print( float n, float x, float y, const char* format, ... ){

		if ( format ) {
			va_list	list;

			va_start( list, format );
			if ( const auto leng = opal::strlenf_s( format, list ) ) {
				font_buff.resize( leng );
				std::vsprintf( font_buff.data(), format, list );
				font_print( n, x, y );
			}
			va_end( list );
		}
	}

	void print( float x, float y, const char* format, ... ){

		if ( format ) {
			va_list	list;

			va_start( list, format );
			if ( const auto leng = opal::strlenf_s( format, list ) ) {
				font_buff.resize( leng );
				std::vsprintf( font_buff.data(), format, list );
				font_print( 0, x, y );
			}
			va_end( list );
		}
	}
};

// End Of File

