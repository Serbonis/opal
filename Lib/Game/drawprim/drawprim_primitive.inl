//========================================
// プリミティブ描画
//========================================
#include "system.hpp"


//----------------------------------------
// プリミティブ確保
//----------------------------------------
template<typename TYPE>auto primitive( const char* s )->std::shared_ptr<TYPE>{

	static std::shared_ptr<TYPE>	p;

	if ( !p ) {
		p = std::make_shared<TYPE>();
		p->Init();
		opal::SYSTEMX::SysFunc( s, nullptr, nullptr, [&](void*){ if ( p ) { p->Free(); p = nullptr; } } );
	}

	return p;
}

// End Of File
