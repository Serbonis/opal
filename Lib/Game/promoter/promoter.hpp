#pragma once
//========================================
// �쓮��
//========================================
#include "Utl/easing.hpp"

//========================================
// PROMOTER
//========================================
class PROMOTER {
public:
	explicit PROMOTER();
	virtual ~PROMOTER();

	using PTFUNC = opal::EASING::FUNC;
	using UPFUNC = std::function<void(PROMOTER*)>;
	using CBFUNC = std::function<void(PROMOTER*)>;

private:
	bool	flag;
	PTFUNC	function;
	UPFUNC	update;
	CBFUNC	callback;
	float	time;
	float	count;

public:
	virtual void   SetPromoterFunction( PTFUNC = nullptr );
	virtual PTFUNC GetPromoterFunction( void ) const;

public:
	virtual void   SetPromoterUpdate( UPFUNC = nullptr );
	virtual UPFUNC GetPromoterUpdate( void ) const;

public:
	virtual void   SetPromoterCallback( CBFUNC = nullptr );
	virtual CBFUNC GetPromoterCallback( void ) const;

public:
	virtual void  SetPromoterTime( float );
	virtual float GetPromoterTime( void ) const;

public:
	virtual void  SetPromoterCount( float = 0 );
	virtual void  AddPromoterCount( float = 1 );
	virtual float GetPromoterCount( void ) const;

public:
	virtual void  SetPromoterPause( int = opal::FLAG_FLIP );
	virtual bool  GetPromoterPause( void ) const;

public:
	virtual void  PromoterPlay( float );
	virtual void  PromoterPlay( void  );
	virtual void  PromoterStop( bool = false );

public:
	virtual float PromoterTween( void ) const;

public:
	virtual bool PromoterStatus( void ) const;
	virtual operator bool(       void ) const;

public:
	virtual float PromoterUpdate( float = 1 );
	virtual float operator ()(    float = 1 );
};

// End Of File
