//========================================
// �쓮��
//========================================
#include "promoter.hpp"

//========================================
// PROMOTER
//========================================
using namespace opal;
using namespace opal::EASING;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PROMOTER::PROMOTER() :
	flag{true},
	function( linear ),
	update( nullptr ),
	callback( nullptr ),
	time( 0 ),
	count( 0 )
{}
PROMOTER::~PROMOTER()
{}

//----------------------------------------
// ��Ԋ֐�
//----------------------------------------
void PROMOTER::SetPromoterFunction( PTFUNC f ){ function = f ? f : linear; }
PROMOTER::PTFUNC PROMOTER::GetPromoterFunction( void ) const { return function; }

//----------------------------------------
// �X�V�֐�
//----------------------------------------
void PROMOTER::SetPromoterUpdate( UPFUNC f ){ update = f; }
PROMOTER::UPFUNC PROMOTER::GetPromoterUpdate( void ) const { return update;	}

//----------------------------------------
// �R�[���o�b�N�֐�
//----------------------------------------
void PROMOTER::SetPromoterCallback( CBFUNC f ){ callback = f; }
PROMOTER::CBFUNC PROMOTER::GetPromoterCallback( void ) const { return callback;	}

//----------------------------------------
// �ݒ聕�擾
//----------------------------------------
void  PROMOTER::SetPromoterTime( float t ){ time = t; }
float PROMOTER::GetPromoterTime( void ) const { return time; }

void  PROMOTER::SetPromoterCount( float c ){ count  = c;	}
void  PROMOTER::AddPromoterCount( float c ){ count += c;	}
float PROMOTER::GetPromoterCount( void ) const { return count; }

//----------------------------------------
// �ꎞ��~
//----------------------------------------
void PROMOTER::SetPromoterPause( int f  ){ FLAG_CTRL( flag, f );	}
bool PROMOTER::GetPromoterPause( void   ) const { return flag;		}

//----------------------------------------
// ����
//----------------------------------------
void PROMOTER::PromoterPlay( float t ){ flag = true;  count = 0; time = t;	}
void PROMOTER::PromoterPlay( void    ){ flag = true;  count = 0; 			}
void PROMOTER::PromoterStop( bool  f ){

	if ( f && callback ) {
		callback( this );
	}

	flag = false;
	count = time;
}

//----------------------------------------
// ��ԗ�
//----------------------------------------
float PROMOTER::PromoterTween( void ) const { return function( count, time );	}

//----------------------------------------
// ���聕�X�V
//----------------------------------------
bool PROMOTER::PromoterStatus( void ) const { return count < time; }
PROMOTER::operator bool( void ) const {  return PromoterStatus();	}

//----------------------------------------
// ���聕�X�V
//----------------------------------------
float PROMOTER::PromoterUpdate( float c ){

	if ( !flag ) {
		return PromoterTween();
	}

	if ( count < time ) {
		if ( update ) {
			update( this );
		}
		if ( ( count += c ) >= time ) {
			count = time;
			if ( update ) {
				update( this );
			}

			if ( callback ) {
				callback( this );
			}
		}
		return PromoterTween();
	}
	return 1;
}

float PROMOTER::operator()( float c ){ return PromoterUpdate( c );	}

// End Of File
