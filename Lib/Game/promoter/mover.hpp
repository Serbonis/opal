#pragma once
//========================================
// �ړ���
//========================================
#include "promoter.hpp"

//========================================
// MOVER
//========================================
template<typename TYPE>class MOVER : public PROMOTER {
public:
	explicit MOVER();
	virtual ~MOVER();

private:
	TYPE	pos[2];	// 0 = start, 1 = destination

public:
	virtual void InitPromoterPosition( TYPE = TYPE{} );
	virtual void SetPromoterPosition(  TYPE, TYPE );
	virtual void SetPromoterPosition(  TYPE );
	virtual void AddPromoterPosition(  TYPE );
	virtual void SetPromoterPosition(  const TYPE[2] );
	virtual TYPE GetPromoterPosition(  UINT ) const;
	virtual TYPE GetPromoterPosition(  void ) const;

public:
	virtual TYPE GetPromoterPositionS( void ) const;
	virtual TYPE GetPromoterPositionD( void ) const;

public:
	virtual TYPE PromoterPosition( float = 1.0f );
	virtual TYPE operator []( float );
};

//----------------------------------------
// 1,2,3
//----------------------------------------
#include "Ext/vector.hpp"

class MOVER1 : public MOVER<float>{};

class MOVER2 : public MOVER<opal::VECTOR2>{
public:
	using MOVER::InitPromoterPosition;
	using MOVER::SetPromoterPosition;
	using MOVER::AddPromoterPosition;

	virtual void InitPromoterPosition( float, float );
	virtual void SetPromoterPosition(  float, float, float, float );
	virtual void SetPromoterPosition(  float, float );
	virtual void AddPromoterPosition(  float, float );

	virtual void SetPromoterPosition(  opal::VECTOR2, float, float );
	virtual void SetPromoterPosition(  float, float, opal::VECTOR2 );
};

class MOVER3 : public MOVER<opal::VECTOR3>{
public:
	using MOVER::InitPromoterPosition;
	using MOVER::SetPromoterPosition;
	using MOVER::AddPromoterPosition;

	virtual void InitPromoterPosition( float, float, float );
	virtual void SetPromoterPosition(  float, float, float, float, float, float );
	virtual void SetPromoterPosition(  float, float, float );
	virtual void AddPromoterPosition(  float, float, float );

	virtual void SetPromoterPosition(  opal::VECTOR3, float, float, float );
	virtual void SetPromoterPosition(  float, float, float, opal::VECTOR3 );
};

// End Of File
