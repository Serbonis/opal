//========================================
// 移動体
//========================================
#include "mover.hpp"

//========================================
// MOVER
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
template<typename TYPE>MOVER<TYPE>::MOVER() :
	pos{}
{}
template<typename TYPE>MOVER<TYPE>::~MOVER(){}

//----------------------------------------
// 設定＆取得
//----------------------------------------
template<typename TYPE>void MOVER<TYPE>::InitPromoterPosition( TYPE sv ){ pos[1] = pos[0] = sv; }

template<typename TYPE>void MOVER<TYPE>::SetPromoterPosition( TYPE sv, TYPE ev ){ pos[0] = sv; pos[1] = ev; }
template<typename TYPE>void MOVER<TYPE>::SetPromoterPosition(          TYPE ev ){ SetPromoterPosition( pos[1], ev        );	}
template<typename TYPE>void MOVER<TYPE>::AddPromoterPosition(          TYPE dv ){ SetPromoterPosition( pos[1], pos[1]+dv );	}
template<typename TYPE>void MOVER<TYPE>::SetPromoterPosition( const TYPE v[2]  ){ SetPromoterPosition(   v[0], v[1]      );	}

template<typename TYPE>TYPE MOVER<TYPE>::GetPromoterPosition( UINT n ) const { return ( n < 2 ) ? pos[n] : TYPE{}; }
template<typename TYPE>TYPE MOVER<TYPE>::GetPromoterPosition( void   ) const { return pos[0] + ( pos[1] - pos[0] ) * PromoterTween(); }

template<typename TYPE>TYPE MOVER<TYPE>::GetPromoterPositionS( void ) const { return pos[0]; }
template<typename TYPE>TYPE MOVER<TYPE>::GetPromoterPositionD( void ) const { return pos[1]; }

//----------------------------------------
// 判定＆更新
//----------------------------------------
template<typename TYPE>TYPE MOVER<TYPE>::PromoterPosition( float c ){ return pos[0] + ( pos[1] - pos[0] ) * PromoterUpdate( c ); }
template<typename TYPE>TYPE MOVER<TYPE>::operator[]( float c ){ return PromoterPosition( c );	}

//----------------------------------------
// 明示的インスタンス化
//----------------------------------------
template class MOVER<float>;
template class MOVER<VECTOR2>;
template class MOVER<VECTOR3>;

void MOVER2::InitPromoterPosition( float sx, float sy                     ){ InitPromoterPosition( {sx,sy}         );	 }
void MOVER2::SetPromoterPosition(  float sx, float sy, float ex, float ey ){ SetPromoterPosition(  {sx,sy},{ex,ey} );	 }
void MOVER2::SetPromoterPosition(                      float ex, float ey ){ SetPromoterPosition(          {ex,ey} );	 }
void MOVER2::AddPromoterPosition(                      float ex, float ey ){ AddPromoterPosition(          {ex,ey} );	 }

void MOVER2::SetPromoterPosition(  float sx, float sy, VECTOR2 ev         ){ SetPromoterPosition(  {sx,sy},  ev     );	 }
void MOVER2::SetPromoterPosition(  VECTOR2 sv,         float ex, float ey ){ SetPromoterPosition(   sv,     {ex,ey} );	 }

void MOVER3::InitPromoterPosition( float sx, float sy, float sz                               ){ InitPromoterPosition( {sx,sy,sz}            );	 }
void MOVER3::SetPromoterPosition(  float sx, float sy, float sz, float ex, float ey, float ez ){ SetPromoterPosition(  {sx,sy,sz},{ex,ey,ez} );	 }
void MOVER3::SetPromoterPosition(                                float ex, float ey, float ez ){ SetPromoterPosition(             {ex,ey,ez} );	 }
void MOVER3::AddPromoterPosition(                                float ex, float ey, float ez ){ AddPromoterPosition(             {ex,ey,ez} );	 }

void MOVER3::SetPromoterPosition(  float sx, float sy, float sz, VECTOR3 ev                   ){ SetPromoterPosition(  {sx,sy,sz},  ev        );	 }
void MOVER3::SetPromoterPosition(  VECTOR3 sv,                   float ex, float ey, float ez ){ SetPromoterPosition(   sv,        {ex,ey,ez} );	 }

// End Of File
