#pragma once
//========================================
// OPAL Game Library
//========================================
#include "workx.hpp"
#include "drawx.hpp"
#include "objx.hpp"
#include "prims.hpp"

//========================================
// アクター
//========================================
//========================================
// アクターエンティテイ
//========================================
class ACTOR : public opal::WORKT {
public:
	explicit ACTOR( void* = nullptr );
	virtual ~ACTOR() = default;

protected:
	void* 	game;

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

protected:
	std::shared_ptr<class ACTOR_VIEW>	view;

	struct VIEW final {
		bool		disp;	// 表示
		bool		anime;	// アニメ
		bool		revu;	// U反転
		bool		revv;	// V反転
	}	draw;

public:
	virtual void SetView( std::shared_ptr<class ACTOR_VIEW> = nullptr );
	virtual auto GetView( void ) const->std::shared_ptr<class ACTOR_VIEW>;

	template<typename VIEW>auto SetView( void )->std::shared_ptr<VIEW>{

		const auto	v = opal::PRIMS::SharedPrim<VIEW>();
		SetView( v );
		return v;
	}
	template<typename VIEW>auto GetView( void ) const->std::shared_ptr<VIEW>{

		return std::dynamic_pointer_cast<VIEW>( GetView() );
	}

public:
	virtual void SetDisp(  int = opal::FLAG_FLIP );
	virtual void SetAnime( int = opal::FLAG_FLIP );
	virtual void SetRevU(  int = opal::FLAG_FLIP );
	virtual void SetRevV(  int = opal::FLAG_FLIP );

	virtual bool GetDisp(  void ) const;
	virtual bool GetAnime( void ) const;
	virtual bool GetRevU(  void ) const;
	virtual bool GetRevV(  void ) const;
};

//========================================
// アクターデータ
//========================================
class ACTOR_DATA {};

//========================================
// アクタービュー
//========================================
class ACTOR_VIEW : public opal::DRAWL {
public:
	explicit ACTOR_VIEW() = default;
	virtual ~ACTOR_VIEW() = default;

protected:
	std::shared_ptr<void>			view{nullptr};

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

public:
	virtual void SetView( std::shared_ptr<void> = nullptr );
	virtual auto GetView( void ) const->std::shared_ptr<void>;

	template<typename TYPE,typename... ARGS>auto SetView( ARGS... a )->std::shared_ptr<TYPE>{

		const auto	v = std::make_shared<TYPE>( std::forward<ARGS>( a )... );

		SetView( v );

		return v;
	}
	template<typename TYPE>auto GetView( void ) const->std::shared_ptr<TYPE>{

		return std::static_pointer_cast<TYPE>( GetView() );
	}

public:
	virtual void Polling( const ACTOR* ){}
};

//========================================
// アクターマッパー
//========================================
template<typename KEY,typename VAL=void>class ACTOR_MAPPER {
public:
	explicit ACTOR_MAPPER() = default;
	virtual ~ACTOR_MAPPER() = default;

protected:
	using SKT = std::shared_ptr<KEY>;
	using SVT = std::shared_ptr<VAL>;
	using MAP = std::unordered_map<SKT,SVT>;

	MAP	map;

public:
	virtual auto operator []( SKT k )->SVT&{ return map[k];	}
	virtual auto operator ()( SKT k ) const->SVT{ return map.count( k ) ? map.at( k ) : nullptr;	}
	virtual auto operator ()( void  ) const->const MAP&{ return map;	}

public:
	virtual void erase( SKT k ){ if ( map.count( k ) ) { map.erase( k );	}	}
	virtual void clear( void  ){ map.clear();	}

public:
	virtual auto Search( SKT k ) const->SVT{ return (*this)( k ); }

public:
	virtual auto Update( void )->MAP&{

		std::vector<typename decltype( map )::key_type>	kill;

		for ( const auto& [e,p] : map ) {
			if ( e->Kill() ) {
				kill.emplace_back( e );
			}
		}

		for ( const auto& e : kill ) {
			map.erase( e );
		}

		return map;
	}
};

//========================================
// アクターマネージャー
//========================================
class MANAGER_ACTOR {
protected:
	using K = std::string;
	using A = std::shared_ptr<ACTOR>;
	using V = std::shared_ptr<ACTOR_VIEW>;
	using M = ACTOR_MAPPER<ACTOR,ACTOR_VIEW>;

	std::unordered_map<K,M>	manager;

public:
	virtual void Clear( void );

public:
	virtual void Update( std::function<void(ACTOR*)> = nullptr );

#if OPAL_DEBUG
public:
	virtual void Debug( const char* = nullptr );
#endif

public:
	virtual void Release( const K&, A );
	virtual void Release( const K& );
	virtual void Release( void );

public:
	virtual auto Search( const K& ) const->const M*;
	virtual auto Search( const K&, const A& ) const->V;
	virtual auto Search( const K&, const K& ) const->A;

	template<typename TYPE>auto Search( const K& s, const A& n ) const->std::shared_ptr<TYPE>{

		return std::dynamic_pointer_cast<TYPE>( Search( s, n ) );
	}
	template<typename TYPE>auto Search( const K& s, const K& n ) const->std::shared_ptr<TYPE>{

		return std::dynamic_pointer_cast<TYPE>( Search( s, n ) );
	}

public:
	virtual auto Register( const K&, A, V = nullptr )->A;

	template<typename VIEW>auto Register( const K& s, A a )->A{

		if ( a ) {
			if_constexpr ( std::is_void_v<VIEW> ) {
				return Register( s, a );
			} else {
				return Register( s, a, opal::PRIMS::SharedPrim<VIEW>() );
			}
		}
		return nullptr;
	}
};

class ACTOR_MANAGER :
	public opal::OBJT,
	public MANAGER_ACTOR
{
public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

protected:
	virtual void ObjFunc( void ) override;

#if OPAL_DEBUG
public:
	virtual void Debug( const char* = nullptr );
#endif
};

// End Of File
