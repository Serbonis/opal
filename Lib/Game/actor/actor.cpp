//========================================
// OPAL Game Library
//========================================
#include "actor.hpp"

//========================================
// アクター
//========================================
using namespace opal;

//========================================
// アクターエンティテイ
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
ACTOR::ACTOR( void* g ) :
	game{g}
{}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void ACTOR::Init( const char* p ){

	WORKT::Init( p );

	view		= nullptr;

	draw.disp  = DRAWX::Disp();
	draw.anime = DRAWX::Anime();
	draw.revu  = false;
	draw.revv  = false;
}

void ACTOR::Free( void ){

	draw.disp  = false;
	draw.anime = false;
	draw.revu  = false;
	draw.revv  = false;

	view		= nullptr;

	WORKT::Free();
}

//----------------------------------------
// ビュー制御
//----------------------------------------
void ACTOR::SetView( std::shared_ptr<ACTOR_VIEW> v ){

	if ( ( view = v ) ) {
		view->Parent( this );
	}
}

auto ACTOR::GetView( void ) const->std::shared_ptr<ACTOR_VIEW>{

	return view;
}

void ACTOR::SetDisp(  int f ){ FLAG_CTRL( draw.disp,  f ); if ( view ) { view->SetDisp(  f ); }	}
void ACTOR::SetAnime( int f ){ FLAG_CTRL( draw.anime, f ); if ( view ) { view->SetAnime( f ); }	}
void ACTOR::SetRevU(  int f ){ FLAG_CTRL( draw.revu,  f ); if ( view ) { view->SetRevU(  f ); }	}
void ACTOR::SetRevV(  int f ){ FLAG_CTRL( draw.revv,  f ); if ( view ) { view->SetRevV(  f ); }	}

bool ACTOR::GetDisp(  void ) const { return draw.disp;	}
bool ACTOR::GetAnime( void ) const { return draw.anime;	}
bool ACTOR::GetRevU(  void ) const { return draw.revu;	}
bool ACTOR::GetRevV(  void ) const { return draw.revv;	}

//========================================
// アクターデータ
//========================================

//========================================
// アクタービュー
//========================================
//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void ACTOR_VIEW::Init( const char* p ){

	DRAWL::Init( p );
}

void ACTOR_VIEW::Free( void ){

	DRAWL::Free();
}

void ACTOR_VIEW::SetView( std::shared_ptr<void> v ){

	view = v;
}

auto ACTOR_VIEW::GetView( void ) const->std::shared_ptr<void>{

	return view;
}


//========================================
// アクターマネージャー
//========================================
//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void ACTOR_MANAGER::Init( const char* p ){

	OBJT::Init( p );

	MANAGER_ACTOR::Clear();
}

void ACTOR_MANAGER::Free( void ){

	MANAGER_ACTOR::Clear();

	OBJT::Free();
}

//----------------------------------------
// 更新
//----------------------------------------
void ACTOR_MANAGER::ObjFunc( void ){ MANAGER_ACTOR::Update(); }

//----------------------------------------
// デバッグ
//----------------------------------------
#if OPAL_DEBUG
void ACTOR_MANAGER::Debug( const char* s ) { MANAGER_ACTOR::Debug( s );	}
#endif

//----------------------------------------
// 消去
//----------------------------------------
void MANAGER_ACTOR::Clear( void ){

	manager.clear();
}

//----------------------------------------
// 更新
//----------------------------------------
template<typename V, typename A>static void deligate( V& v, A& a ){

	v->SetDisp(  a->GetDisp()  );
	v->SetAnime( a->GetAnime() );
	v->SetRevU(  a->GetRevU()  );
	v->SetRevV(  a->GetRevV()  );

	v->Polling( a.get() );
}

void MANAGER_ACTOR::Update( std::function<void(ACTOR*)> f ){

	for ( auto& [s,mapper] : manager ) {
		for ( const auto& [a,v] : mapper.Update() ) {
			if ( v ) {
				deligate( v, a );
			} else if ( const auto p = a->GetView() ) {
				deligate( p, a );
			}

			if ( f ) {
				f( a.get() );
			}
		}
	}
}

//----------------------------------------
// デバッグ
//----------------------------------------
#if OPAL_DEBUG
void MANAGER_ACTOR::Debug( const char* ) {

	printd( "----------------------------------------\n" );
	printd( "- ACTOR MANAGER\n" );
	printd( "----------------------------------------\n" );
	for ( const auto& [s,mapper] : manager ) {
		printd( "- %s\n", s.c_str() );
		printd( "--------------------\n" );
		for ( const auto& [a,v] : mapper() ) {
			if ( const auto p = a->GetView() ) {
				printd( "%p [%p] %s\n", a.get(), p.get(), a->GetID() );
			} else {
				printd( "%p (%p) %s\n", a.get(), v.get(), a->GetID() );
			}
		}
	}
	printd( "----------------------------------------\n" );
}
#endif

//----------------------------------------
// 登録＆破棄
//----------------------------------------
auto MANAGER_ACTOR::Register( const K& s, A a, V v )->A{

	if ( a ) {
		manager[s][a] = v;
		if ( v ) {
			v->Parent( a );
		}
	}

	return a;
}


void MANAGER_ACTOR::Release( const K& s, A a ) {

	if ( manager.count( s ) ) {
		if ( auto& m = manager.at( s ); m( a ) ) {
			m.erase( a );
		}
	}
}

void MANAGER_ACTOR::Release( const K& s ) {

	if ( manager.count( s ) ) {
		manager.erase( s );
	}
}

void MANAGER_ACTOR::Release( void ) {

	manager.clear();
}

//----------------------------------------
// 検索
//----------------------------------------
auto MANAGER_ACTOR::Search( const K& s ) const->const M*{

	if ( manager.count( s ) ) {
		return &manager.at( s );
	}
	return nullptr;
}

auto MANAGER_ACTOR::Search( const K& s, const A& n ) const->V{

	if ( const auto m = Search( s ) ) {
		return m->Search( n );
	}
	return nullptr;
}

auto MANAGER_ACTOR::Search( const K& s, const K& n ) const->A{

	if ( const auto m = Search( s ) ) {
		for ( const auto& [a,v] : (*m)() ) {
			if ( const auto	p = a->GetID() ) {
				if ( n == std::string{p} ) {
					return a;
				}
			}
		}
	}

	return nullptr;
}

// End Of File
