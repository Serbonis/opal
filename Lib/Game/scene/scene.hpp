#pragma once
//========================================
// OPAL Game Library
//========================================
#include "srvcs.hpp"

//========================================
// シーン管理
//========================================
/*
  [使用方法]
  SCENE_MANAGER	scene;

  scene.Init();	// 初期化

  scene.Exec();		// 実行

  scene.Free();		// 後始末

  scene.Debug();	// デバッグ表示

  [シーン]
  SCENE_SECTION から派生するクラス、下記の関数を必要に応じてoverrideする

  void SceneGenerate( SCENE_MANAGER* );			// シーン生成時
  void SceneDestroy(  SCENE_MANAGER* );			// シーン破棄時
  bool SceneInit( SCENE_MANAGER* );				// シーン開始時	falseで終了、trueで繰り返す
  bool SceneFree( SCENE_MANAGER* );				// シーン終了時	同上
  auto SceneMain( SCENE_MANAGER* )->SCENE_NAME;	// シーン関数

  SCENE_SECTION から派生する template SCENE<TYPE> は TYPEクラスをインプリメント生成してくれる

  [ジェネレーター]
  scene.SetScene( "test", generater );			// "test"というシーンのジェネレーターを登録

  generater は下記形式の関数

  引数	const std::string&
  戻値	std::shared_ptr<SCENE_SCTION>	※派生クラスを返しても可、ジェネレーターを配列にするなら基底クラスで返そう

  scene.SetScene<SCENE_SECTION..>( "test" );	// ジェネレーター自動生成
  scene.SetScene( "test" );						// "test"というシーンのジェネレーターを削除
  scene.SetScene();								// すべてのシーンのジェネレーターを削除

  [コントロール]
  scene.Scene( "test" );	// "test"というシーンを生成・開始する
  scene.Scene();			// シーンを終了する

  SceneMainの戻り値
  ""			現在のシーンを継続
  nullptr		シーンを終了
  シーン名		そのシーンに移行する、存在しない場合は nullptr と同じ

  [ゲームワーク]
  SCENE_MANAGER scene{&game}	// 生成時にゲームワークを渡すことができる
  scene.SetGame( &game )		// これでもOK

  auto ScenMain( SCENE_MANAGER* scene ){

      scene->GetScene() 		// ゲームワークの参照(void*)
      scene->GetScene<TYPE>() 	// ゲームワークの参照(TYPEでキャスト済み)
  }
*/

//========================================
// サーバー＆クライアント
//========================================
class SCENE_SERVER;
class SCENE_CLIENT;

class SCENE_SERVER : public opal::SRVC_S<SCENE_SERVER,SCENE_CLIENT> {
protected:
	explicit SCENE_SERVER( const char* = nullptr );
public:
	virtual ~SCENE_SERVER();
};

class SCENE_CLIENT : public opal::SRVC_C<SCENE_SERVER,SCENE_CLIENT> {
protected:
	explicit SCENE_CLIENT( const char* = nullptr );
public:
	virtual ~SCENE_CLIENT();
};

class SCENES;
class SCENEC;

//========================================
// サーバー
//========================================
class SCENES :
	public SCENE_SERVER
{
	friend class opal::SRVCS;

public:
	explicit SCENES( const char* = nullptr );
	virtual ~SCENES();

public:
	enum CODE {
		CODE_NULL,
		CODE_GOTO,
		CODE_JUMP,
		CODE_CALL,
		CODE_EXIT,
	};

protected:
	using SCNS = std::string;
	using SCNP = std::shared_ptr<SCENEC>;

	struct SCNI {
		SCNS	s;
		SCNP	p;
	};

	SCNI							scene;
	std::unordered_map<SCNS,SCNP>	smap;
	std::pair<CODE,SCNS>			next;
	std::stack<SCNI>				stack;

public:
	template<typename TYPE>static TYPE* Generate( const char* p ){ return opal::SRVCS::Generate<TYPE>( p ? p : opal::guid_name() );	}
	static SCENES* Search( const char* );
	static UINT   Destroy( const char* );

	template<typename TYPE>static TYPE* Generate( const std::string& p ){ return Generate<TYPE>( p.c_str() );	}
	static SCENES* Search( const std::string& p ){ return Search(  p.c_str() );	}
	static UINT   Destroy( const std::string& p ){ return Destroy( p.c_str() );	}

	virtual void Destroy( void );

public:
	virtual void SetServiceName( const char* = nullptr );
	virtual auto GetServiceName( void ) const->const char*;

public:
	virtual void Begin(  void ) override;
	virtual void End(    void ) override;
	virtual void Before( void ) override;
	virtual void After(  void ) override;

public:
	template<typename TYPE>auto GenerateScene( const std::string& t )->std::shared_ptr<TYPE>{

		if ( !smap.count( t ) ) {
			const auto	s = std::make_shared<TYPE>();

			s->SetServiceName( t.c_str() );
			s->SetServer( this );

			smap[t] = s;
			return s;
		}
		return nullptr;
	}

	template<typename TYPE>auto GenerateScene( const char* p )->std::shared_ptr<TYPE>{

		return GenerateScene<TYPE>( std::string{ p ? p : opal::guid_name()} );
	}

	virtual auto SearchScene( const std::string& ) const->SCNP;
	virtual auto SearchScene( const char*        ) const->SCNP;
	virtual UINT SearchScene( void ) const;

	template<typename TYPE>auto SearchScene( const std::string& t ) const->std::shared_ptr<TYPE>{ return std::dynamic_pointer_cast<TYPE>( SearchScene( t ) );	}
	template<typename TYPE>auto SearchScene( const char*        p ) const->std::shared_ptr<TYPE>{ return std::dynamic_pointer_cast<TYPE>( SearchScene( p ) );	}

	virtual void DestroyScene( const std::string& );
	virtual void DestroyScene( const char* );
	virtual void DestroyScene( void );

public:
	virtual void Scene( CODE, const char* = nullptr );
	virtual void GotoScene(   const char* = nullptr );
	virtual void JumpScene(   const char* = nullptr );
	virtual void CallScene(   const char* = nullptr );
	virtual void ExitScene( void );

	virtual void Scene( CODE, const std::string& );
	virtual void GotoScene(   const std::string& );
	virtual void JumpScene(   const std::string& );
	virtual void CallScene(   const std::string& );

	template<typename TYPE>void Scene( CODE c, const char* p = nullptr ){

		if ( const auto s = GenerateScene<TYPE>( p ) ) {
			Scene( c,  s->GetName() );
		}
	}

	template<typename TYPE>void Scene( CODE c, const std::string& s ){ Scene<TYPE>( c, s.c_str() );	}

	template<typename TYPE>void GotoScene( const char* p = nullptr ){ Scene<TYPE>( CODE_GOTO, p ); }
	template<typename TYPE>void JumpScene( const char* p = nullptr ){ Scene<TYPE>( CODE_JUMP, p ); }
	template<typename TYPE>void CallScene( const char* p = nullptr ){ Scene<TYPE>( CODE_CALL, p ); }

	template<typename TYPE>void GotoScene( const std::string& s    ){ Scene<TYPE>( CODE_GOTO, s.c_str() );	}
	template<typename TYPE>void JumpScene( const std::string& s    ){ Scene<TYPE>( CODE_JUMP, s.c_str() );	}
	template<typename TYPE>void CallScene( const std::string& s    ){ Scene<TYPE>( CODE_CALL, s.c_str() );	}

protected:
	virtual void scene_begin( void ) final;
	virtual void scene_end(   void ) final;

	virtual void code_null( const std::string& ) final;
	virtual void code_goto( const std::string& ) final;
	virtual void code_jump( const std::string& ) final;
	virtual void code_call( const std::string& ) final;
	virtual void code_exit( const std::string& ) final;

#ifdef OPAL_DEBUG
public:
	virtual void Debug( const char* = nullptr );
#endif
};

//========================================
// クライアント
//========================================
class SCENEC :
	public SCENE_CLIENT
{
protected:
	explicit SCENEC( const char* = nullptr );

public:
	virtual ~SCENEC();

protected:
	SCENES*												server;
	std::map<std::string,std::function<void(SCENEC*)>>	destroy;

public:
	virtual void SetServiceName( const char* = nullptr ) override;
	virtual auto GetServiceName( void ) const->const char* override;

public:
	virtual void SetServer( SCENES* = nullptr );
	virtual auto GetServer( void ) const->SCENES*;
	template<typename TYPE>TYPE* Server( void ) const { return dynamic_cast<TYPE*>( GetServer() );	}

public:
	virtual void SetDestroy( const std::string&, std::function<void(SCENEC*)> = nullptr );
	virtual auto GetDestroy( const std::string& ) const->std::function<void(SCENEC*)>;
	virtual void SetDestroy( std::function<void(SCENEC*)> );
	virtual auto GetDestroy( void ) const->std::function<void(SCENEC*)>;
	virtual void SetDestroy( void );
	virtual void Destroy( void );

public:
	virtual void Resume(  void ){}
	virtual void Suspend( void ){}
};

//========================================
// シーン管理
//========================================
//----------------------------------------
// シーンマネージャー
//----------------------------------------
class SCENE_SECTION;
class SCENE_MANAGER {
public:
	explicit SCENE_MANAGER( void* p = nullptr ) : game{p} {}
	virtual ~SCENE_MANAGER() = default;

private:
	template<typename TYPE=SCENE_SECTION>using FUNC = std::function<std::shared_ptr<TYPE>(const std::string&)>;

	void*					game;
	UINT					step;
	std::shared_ptr<SCENES>	server;

private:
	struct CLIENT final {
		CLIENT( std::shared_ptr<SCENES> s ) :
			server{s},
			now{ std::make_shared<std::string>( "" )},
			next{std::make_shared<std::string>( "" )}
		{}

		auto Make( void )->std::shared_ptr<SCENE_SECTION>;

		void Init( SCENE_MANAGER* );
		void Free( SCENE_MANAGER* );

		bool SceneInit( SCENE_MANAGER* );
		bool SceneFree( SCENE_MANAGER* );
		bool SceneMain( SCENE_MANAGER* );

		std::shared_ptr<SCENES> const	server;
		std::shared_ptr<std::string>	now;
		std::shared_ptr<std::string>	next;
		std::map<std::string,FUNC<>>	make;
		std::shared_ptr<SCENE_SECTION>	play;
	};
	std::shared_ptr<CLIENT>				scene;

public:
	virtual void Init( const char* p = nullptr );
	virtual void Free( void );
	virtual void Exec( void );

#if OPAL_DEBUG
public:
	virtual void Debug( void ) const;
#endif

public:
	virtual void SetGame( void* );
	virtual auto GetGame( void ) const->void*;

	template<typename TYPE=void>auto Game( void ) const->TYPE*{

		if_constexpr ( !std::is_void_v<TYPE> ) {
			return static_cast<TYPE*>( GetGame() );
		} else {
			return GetGame();
		}
	}

public:
	virtual void SetScene( const std::string&, FUNC<> = nullptr );
	virtual void SetScene( const char*,        FUNC<> = nullptr );
	virtual void SetScene( std::nullptr_t,     FUNC<> = nullptr );
	virtual void SetScene( FUNC<>                               );
	virtual void SetScene( void                                 );
	virtual auto GetScene( const std::string& ) const->FUNC<>;
	virtual auto GetScene( const char*        ) const->FUNC<>;
	virtual auto GetScene( std::nullptr_t     ) const->FUNC<>;
	virtual auto GetScene( void               ) const->FUNC<>;

public:
	template<typename TYPE>void SetScene( const char*        s ){ if ( s ) { SetScene( s,  generater<TYPE>( std::string{s} ) ); }	}
	template<typename TYPE>void SetScene( const std::string& s ){            SetScene( s,  generater<TYPE>( s              ) );		}
	template<typename TYPE>void SetScene( std::nullptr_t     s ){            SetScene( s                                     );		}
	template<typename TYPE>void SetScene( void                 ){            SetScene( "", generater<TYPE>( ""             ) );		}
	template<typename TYPE>auto GetScene( const std::string& s ) const->FUNC<TYPE>{ return std::dynamic_pointer_cast<FUNC<TYPE>>( GetScene( s ) );	}

private:
	template<typename TYPE>auto generater( const std::string& s )->FUNC<>{

		return [=](auto){ return server->GenerateScene<TYPE>( s ); };
	}

public:
	virtual void Scene( const std::string& );
	virtual void Scene( const char*        );
	virtual void Scene( std::nullptr_t     );
	virtual void Scene( void               );
};

//----------------------------------------
// シーンプレイヤー
//----------------------------------------
using SCENE_NAME = opal::NAME;

class SCENE_SECTION : public SCENEC {
protected:	explicit SCENE_SECTION() = default;
public:		virtual ~SCENE_SECTION() = default;

public:
	virtual void SceneGenerate( SCENE_MANAGER* ){}
	virtual void SceneDestroy(  SCENE_MANAGER* ){}
	virtual bool SceneInit( SCENE_MANAGER* ){ return false;	}
	virtual bool SceneFree( SCENE_MANAGER* ){ return false;	}
	virtual auto SceneMain( SCENE_MANAGER* )->SCENE_NAME{ return nullptr;	}

public:
	template<typename TYPE=void>auto Game( SCENE_MANAGER* s ) const->TYPE*{ return s->Game<TYPE>();	}

public:
	template<typename TYPE>static auto Generater( void ){ return []( auto ){ return std::make_shared<TYPE>(); }; }
	template<typename TYPE>static auto Generater( const std::string& s ){ return std::pair{ s, Generater<TYPE>() }; }
};

template<typename TYPE=SCENE_SECTION>using SCENE_GENERATER = SCENE_MANAGER::FUNC<TYPE>;
using SCENE_GENERATER_MAP  = std::unordered_map<std::string,SCENE_GENERATER<>>;
using SCENE_GENERATER_PAIR = std::pair<         std::string,SCENE_GENERATER<>>;

template<typename TYPE=void> class SCENE : public SCENE_SECTION {
protected:
	SCENE() :
		work{[]{
			if_constexpr ( !std::is_void_v<TYPE> ) {
				return std::make_shared<TYPE>();
			} else {
				return nullptr;
			}
		}()} {}

public:
	virtual ~SCENE() = default;

protected:
	std::shared_ptr<TYPE>	work;
	UINT					step{0};
	UINT					time{0};
};

// End Of File
