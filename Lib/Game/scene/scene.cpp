//========================================
// OPAL Game Library
//========================================
#include "scene.hpp"

//========================================
// シーン管理
//========================================
using namespace opal;

//========================================
// サーバー＆クライアント
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SCENE_SERVER::SCENE_SERVER( const char* p ) : SRVC_S<SCENE_SERVER,SCENE_CLIENT>( p ){}
SCENE_SERVER::~SCENE_SERVER(){}

SCENE_CLIENT::SCENE_CLIENT( const char* p ) : SRVC_C<SCENE_SERVER,SCENE_CLIENT>( p ){}
SCENE_CLIENT::~SCENE_CLIENT(){}

//========================================
// サーバー
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SCENES::SCENES( const char* p ) :
	SCENE_SERVER( p ),
	scene{ "", nullptr },
	smap(),
	next(),
	stack()
{}
SCENES::~SCENES(){}

namespace {
	template<typename TYPE>auto clear( TYPE& s ){ while ( !s.empty() ) { s.pop(); } }
}

//----------------------------------------
// 検索＆破棄
//----------------------------------------
SCENES* SCENES::Search( const char* p ){

	return p ? opal::SRVCS::Search<SCENES>( p ) : nullptr;
}

UINT SCENES::Destroy( const char* p ){

	if ( p ) {
		if ( const auto	s = opal::SRVCS::Search<SCENES>( p ) ) {
			const auto	n = s->smap.size();

			s->DestroyScene();
			opal::SRVCS::Destroy( p );

			return n;
		}
	}
	return 0;
}

void SCENES::Destroy( void ){

	if ( const auto p = this->GetServiceName() ) {
		opal::SRVCS::Destroy( p );
	}
}

//----------------------------------------
// 名前
//----------------------------------------
void SCENES::SetServiceName( const char* p ){ SCENE_SERVER::SetServiceName( p ); }
auto SCENES::GetServiceName( void ) const->const char*{ return SCENE_SERVER::GetServiceName(); }

//----------------------------------------
// 開始＆終了
//----------------------------------------
void SCENES::Begin( void ){

	scene = { "", nullptr };
	smap.clear();
	next = { CODE_NULL, "" };
	::clear( stack );
}

void SCENES::End( void ){

	::clear( stack );
	next = { CODE_NULL, "" };
	smap.clear();
	scene = { "", nullptr };
}

//----------------------------------------
// 更新関数
//----------------------------------------
void SCENES::Before( void ){}
void SCENES::After(  void ){

	const auto& [c,p] = next;

	switch ( c ) {
	case CODE_NULL: code_null( p );	break;
	case CODE_GOTO: code_goto( p );	break;
	case CODE_JUMP: code_jump( p );	break;
	case CODE_CALL: code_call( p );	break;
	case CODE_EXIT: code_exit( p );	break;
	}

	next = { CODE_NULL, "" };
}

//----------------------------------------
// シーン検索＆破棄
//----------------------------------------
auto SCENES::SearchScene( const char* p ) const->SCNP{

	return ( p && smap.count( p ) ) ? smap.at( p ) : nullptr;
}

auto SCENES::SearchScene( const std::string& t ) const->SCNP{

	return SearchScene( t.c_str() );
}

UINT SCENES::SearchScene( void ) const { return smap.size();	}

void SCENES::DestroyScene( const char* p ){

	if ( p && smap.count( p ) ) {
		if ( const auto s = smap.at( p ) ) {
			if ( s == scene.p ) {
				scene = { "", nullptr };
			}
			s->Destroy();
		}
		smap.erase( p );
	}
}

void SCENES::DestroyScene( const std::string& t ){

	DestroyScene( t.c_str() );
}

void SCENES::DestroyScene( void ){

	if ( scene.p ) {
		DestroyScene( scene.s );
	}

	while ( !stack.empty() ){
		DestroyScene( stack.top().s );
		stack.pop();
	}
	smap.clear();
}

//----------------------------------------
// 遷移
//----------------------------------------
void SCENES::Scene( CODE c, const char* p ){ next = { c, p ? p : "" }; }

void SCENES::GotoScene( const char* p ){ Scene( CODE_GOTO, p ); }
void SCENES::JumpScene( const char* p ){ Scene( CODE_JUMP, p ); }
void SCENES::CallScene( const char* p ){ Scene( CODE_CALL, p ); }
void SCENES::ExitScene( void          ){ Scene( CODE_EXIT    ); }

void SCENES::Scene( CODE c, const std::string& p ){ Scene( c,  p.c_str() );	}
void SCENES::GotoScene(     const std::string& p ){ GotoScene( p.c_str() );	}
void SCENES::JumpScene(     const std::string& p ){ JumpScene( p.c_str() );	}
void SCENES::CallScene(     const std::string& p ){ CallScene( p.c_str() );	}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
void SCENES::Debug( const char* s ){

	printd( "========================================\n" );
	printd( "SCENES:%p %s\n", this, GetServiceName() );
	printd( "SCENEC:%p %s\n", scene.p, scene.s.c_str() );
	printd( "STACK:%d(%s)\n", stack.size(), stack.size() ? stack.top().s.c_str() : "" );
	for ( const auto& [mp,ms] : smap ) {
		printd( "%s%p : %s\n", ms == scene.p ? "=>" : "  ", ms, mp.c_str() );
	}
	printd( "\n" );
}
#endif

//========================================
// CODE
//========================================
//----------------------------------------
// begin - end
//----------------------------------------
void SCENES::scene_begin( void ){

	if ( const auto p = scene.p ) {
		p->Resume();
	}
}

void SCENES::scene_end( void ){

	if ( const auto p = scene.p ) {
		p->Destroy();
		p->Suspend();
	}
}

//----------------------------------------
// CODE_NULL
//----------------------------------------
void SCENES::code_null( const std::string& s ){}

//----------------------------------------
// CODE_GOTO
//----------------------------------------
void SCENES::code_goto( const std::string& s ){

	if ( s == "" ) {
		smap.erase( scene.s );
		::clear( stack );
		scene_end();
		return;
	}

	if ( smap.count( s ) ) {
		smap.erase( scene.s );
		::clear( stack );
		scene_end();

		scene = { s, smap.at( s ) };
		scene_begin();
	}
}

//----------------------------------------
// CODE_JUMP
//----------------------------------------
void SCENES::code_jump( const std::string& s ){

	if ( s == "" ) {
		smap.erase( scene.s );
		::clear( stack );
		scene_end();
		return;
	}

	if ( smap.count( s ) ) {
		::clear( stack );
		scene_end();

		scene = { s, smap.at( s ) };
		scene_begin();
	}
}

//----------------------------------------
// CODE_CALL
//----------------------------------------
void SCENES::code_call( const std::string& s ){

	if ( s == "" ) {
		stack.push( scene );
		scene_end();
		return;
	}

	if ( smap.count( s ) ) {
		stack.push( scene );
		scene_end();

		scene = { s, smap.at( s ) };
		scene_begin();
	}
}

//----------------------------------------
// CODE_EXIT
//----------------------------------------
void SCENES::code_exit( const std::string& s ){

	scene_end();

	scene = [this]{
		while ( !stack.empty() ){
			const auto&	t = stack.top();
			if ( smap.count( t.s ) ) {
				return t;
			}
			stack.pop();
		}
		return SCNI{ "", nullptr };
	}();

	scene_begin();
}

//========================================
// クライアント
//========================================
//----------------------------------------
// constructor & destructor
//----------------------------------------
SCENEC::SCENEC( const char* p ) :
	SCENE_CLIENT( p ),
	server( nullptr )
{}
SCENEC::~SCENEC(){}

//----------------------------------------
// 名前
//----------------------------------------
void SCENEC::SetServiceName( const char* p ){ SCENE_CLIENT::SetServiceName( p ? p : "" ); }
auto SCENEC::GetServiceName( void ) const->const char*{ return SCENE_CLIENT::GetServiceName(); }

//----------------------------------------
// サーバー
//----------------------------------------
void SCENEC::SetServer( SCENES* s ){ server = s;			}
SCENES* SCENEC::GetServer( void ) const { return server;	}

//----------------------------------------
// デストラクタ
//----------------------------------------
void SCENEC::SetDestroy( const std::string& s, std::function<void(SCENEC*)> f ){ destroy[s] = f;	}
auto SCENEC::GetDestroy( const std::string& s ) const->std::function<void(SCENEC*)>{ return destroy.count( s ) ? destroy.at( s ) : nullptr;	}
void SCENEC::SetDestroy( std::function<void(SCENEC*)> f ){ SetDestroy( "", f );	}
auto SCENEC::GetDestroy( void ) const->std::function<void(SCENEC*)>{ return GetDestroy( "" );	}
void SCENEC::SetDestroy( void ){ destroy.clear();	}

void SCENEC::Destroy( void ){

	std::stack<std::function<void(SCENEC*)>>	ds;

	for ( const auto& [s,d] : destroy ) {
		ds.push( d );
	}

	while ( !ds.empty() ) {
		ds.top()( this );
		ds.pop();
	}
}

//========================================
// シーンマネージャー
//========================================
//----------------------------------------
// 初期化
//----------------------------------------
void SCENE_MANAGER::Init( const char* p ){

	const auto s = p ? p : guid_name();

	step	= -1;
	server	= std::shared_ptr<SCENES>( SRVCS::Generate<SCENES>( s.c_str() ), []( SCENES* s ){ s->Destroy(); } );
	scene	= std::make_shared<CLIENT>( server );
}

//----------------------------------------
// 後始末
//----------------------------------------
void SCENE_MANAGER::Free( void ){

	if ( scene ) {
		scene->Free( this );
	}
	scene.reset();
	server	= nullptr;
	step	= -1;
}

//----------------------------------------
// 更新
//----------------------------------------
void SCENE_MANAGER::Exec( void ){

	if ( !server ) {
		return;
	}

	switch ( step ) {
	case 0: scene->Init( this );	step++;	return;
	case 1: if ( scene->SceneInit( this ) ) { step++;	}	return;
	case 2: if ( scene->SceneMain( this ) ) { step++;	}	return;
	case 3: if ( scene->SceneFree( this ) ) { step++;	}	return;
	case 4: scene->Free( this );	step++;	return;
	}

	if ( scene->next && scene->make.count( *scene->next ) ) {
		step = 0;
	}
}

//----------------------------------------
// デバッグ
//----------------------------------------
#if OPAL_DEBUG
void SCENE_MANAGER::Debug( void ) const {

	printd( "----------------------------------------\n" );
	printd( "- SCENE PLAYER[%s]\n", scene->now ? scene->now->c_str() : "-" );
	printd( "----------------------------------------\n" );
	const auto	c = scene->now ? *scene->now : "";

	for ( const auto& [s,f] : scene->make ) {
		printd( "%c%s\n", s == c ? '>' : ' ', s.c_str() );
	}
	printd( "----------------------------------------\n" );
}
#endif

//----------------------------------------
// ゲームワーク
//----------------------------------------
void SCENE_MANAGER::SetGame( void* p ){ game = p;	}
auto SCENE_MANAGER::GetGame( void ) const->void*{ return game;	}

//----------------------------------------
// 取得＆設定
//----------------------------------------
using FUNC = SCENE_MANAGER::FUNC<>;

template<typename CLIENT>void SetClient( std::shared_ptr<CLIENT> c, const std::string& s, FUNC f ){            c->make[s] = f;		}
template<typename CLIENT>void SetClient( std::shared_ptr<CLIENT> c, const char*        s, FUNC f ){ if ( s ) { c->make[s] = f;	}	}
template<typename CLIENT>void SetClient( std::shared_ptr<CLIENT> c, std::nullptr_t     s, FUNC f ){}
template<typename CLIENT>void SetClient( std::shared_ptr<CLIENT> c                               ){ c->make.clear(); }

template<typename CLIENT>auto GetClient( const std::shared_ptr<CLIENT> c, const std::string& s )->FUNC{ return     c->make.count( s ) ? c->make.at( s ) : nullptr;				}
template<typename CLIENT>auto GetClient( const std::shared_ptr<CLIENT> c, const char*        s )->FUNC{ return s ? c->make.count( s ) ? c->make.at( s ) : nullptr : nullptr;	}
template<typename CLIENT>auto GetClient( const std::shared_ptr<CLIENT> c, std::nullptr_t     s )->FUNC{ return nullptr;	}

void SCENE_MANAGER::SetScene( const std::string& s, FUNC<> f ){ return SetClient( scene, s,  f );	}
void SCENE_MANAGER::SetScene( const char*        s, FUNC<> f ){ return SetClient( scene, s,  f );	}
void SCENE_MANAGER::SetScene( std::nullptr_t     s, FUNC<> f ){ return SetClient( scene, s,  f );	}
void SCENE_MANAGER::SetScene(                       FUNC<> f ){ return SetClient( scene, "", f );	}
void SCENE_MANAGER::SetScene( void                           ){ return SetClient( scene );	        }
auto SCENE_MANAGER::GetScene( const std::string& s ) const->FUNC<>{ return GetClient( scene, s  );	}
auto SCENE_MANAGER::GetScene( const char*        s ) const->FUNC<>{ return GetClient( scene, s  );	}
auto SCENE_MANAGER::GetScene( std::nullptr_t     s ) const->FUNC<>{ return GetClient( scene, s  );	}
auto SCENE_MANAGER::GetScene( void                 ) const->FUNC<>{ return GetClient( scene, "" );	}

//----------------------------------------
// シーン実行
//----------------------------------------
template<typename CLIENT>void Client( std::shared_ptr<CLIENT> c, const std::string& s ){ c->next = std::make_shared<std::string>( s ); 			}
template<typename CLIENT>void Client( std::shared_ptr<CLIENT> c, const char*        s ){ c->next = std::make_shared<std::string>( s ? s : "" );	}
template<typename CLIENT>void Client( std::shared_ptr<CLIENT> c, std::nullptr_t     s ){ c->next = s; 											}

void SCENE_MANAGER::Scene( const std::string& s ){ return Client( scene, s  );	}
void SCENE_MANAGER::Scene( const char*        s ){ return Client( scene, s  );	}
void SCENE_MANAGER::Scene( std::nullptr_t     s ){ return Client( scene, s  );	}
void SCENE_MANAGER::Scene( void                 ){ return Client( scene, "" );	}

//----------------------------------------
// CLIENT
//----------------------------------------
auto SCENE_MANAGER::CLIENT::Make( void )->std::shared_ptr<SCENE_SECTION>{

	if ( next ) {
		if ( make.count( *next ) ) {
			if ( const auto& f = make.at( *next ) ) {
				if ( const auto p = f( *next ) ) {
					return p;
				}
			}
		}
	}
	return nullptr;
}

void SCENE_MANAGER::CLIENT::Init( SCENE_MANAGER* m ) {

	play = Make();
	play->SceneGenerate( m );
	now  = next;
	next = nullptr;
}

void SCENE_MANAGER::CLIENT::Free( SCENE_MANAGER* m ) {

	if ( now ) {
		server->DestroyScene( *now );
	}
	if ( play ) {
		play->SceneDestroy( m );
	}
	play = nullptr;
	now  = nullptr;
}

bool SCENE_MANAGER::CLIENT::SceneInit( SCENE_MANAGER* m ){ return !play->SceneInit( m ) ? true : false;	}
bool SCENE_MANAGER::CLIENT::SceneFree( SCENE_MANAGER* m ){ return !play->SceneFree( m ) ? true : false;	}
bool SCENE_MANAGER::CLIENT::SceneMain( SCENE_MANAGER* m ){

	const auto n = play->SceneMain( m );

	if ( !n ) {
		next = nullptr;
		return true;
	}

	if ( n != "" ) {
		next = std::make_shared<std::string>( n );
		return true;
	}

	if ( next ) {
		return true;
	}

	return false;
}

// End Of File
