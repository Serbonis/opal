//========================================
// OPAL Game Library
//========================================
#include "Draw/camera.hpp"

//========================================
// カメラタスク
//========================================
using namespace opal;

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
namespace opal::CAMERAS_C {
	extern const VECTOR3	pos;
}

void CAMERA::Init( const char* pName ){

	DRAWT::Init( pName );
	CAMERAX::Init();

	SetTrans( opal::CAMERAS_C::pos );
}

void CAMERA::Free( void ){

	CAMERAX::Free();
	DRAWT::Free();
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void CAMERA::DrawMain( void ){

	if ( ( active = GetDisp( 1 ) ) ) {
		Update( this, this );
	}
}

//----------------------------------------
// 状態更新
//----------------------------------------
CAMERAX* CAMERA::Update( CAMERAX* cx, const DRAWT3* d3 ){

	if ( cx && d3 ) {
		cx->Update( d3->WorldTrans(),
					d3->WorldAxisZ(),
					d3->WorldAxisY() );
	}
	return cx;
}

// End Of File
