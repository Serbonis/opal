//========================================
// OPAL Game Library
//========================================
#include "font_dx11.hpp"
#include "font_dx11_.hpp"

//========================================
// フォントタスク
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// Default
//----------------------------------------
#define DEFAULTPATH	"./Exe"
#define DEFAULTFONT	"ms_mincho_t.spritefont"

std::string FONT::defaultpath = DEFAULTPATH;
std::string FONT::defaultfont = DEFAULTFONT;

void FONT::SetDefaultPath( const char* p ){ defaultpath = p ? p : DEFAULTPATH;	}
void FONT::SetDefaultFont( const char* f ){ defaultfont = f ? f : DEFAULTFONT;	}

void FONT::SetDefaultPath( const std::string& p ){ defaultpath = p;	}
void FONT::SetDefaultFont( const std::string& f ){ defaultfont = f;	}

void FONT::SetDefaultPath( std::nullptr_t ){ defaultpath = DEFAULTPATH;	}
void FONT::SetDefaultFont( std::nullptr_t ){ defaultfont = DEFAULTFONT;	}

const char* FONT::GetDefaultPath( void ){ return defaultpath.c_str();	}
const char* FONT::GetDefaultFont( void ){ return defaultfont.c_str();	}

//----------------------------------------
// constructor & destructor
//----------------------------------------
FONT::FONT() :
	device( nullptr ),
	font( "" )
{}
FONT::~FONT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void FONT::Init( const char* p ){

	dx::FONT::Init( p );

	font = ( defaultfont == "" ) ? "" : defaultpath+"/"+defaultfont;
	FONT::InitDevice( 0 );
}

void FONT::Free( void ) {

	FONT::FreeDevice( 0 );
	font = "";

	dx::FONT::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int FONT::InitDevice( int n ){

	if ( !device ) {
		if ( n == 0 ) {
			DRAWX::DeviceLost( DeviceName( NAME( "%p:%s:%s", this, "(FONT)", font.c_str() ) ), this );
			device = FONT_MAP::CreateDevice( font, &font );
		}
	}

	return 0;
}

int FONT::FreeDevice( int n ){

	if ( device ) {
		if ( n == 0 ) {
			DRAWX::DeviceLost( DeviceName() );
			device = nullptr;
			FONT_MAP::DestroyDevice( font );
		}
	}

	return 0;
}

void FONT::DeviceData( const void* d ){}
void FONT::DeviceData( void ){}

//----------------------------------------
// 描画
//----------------------------------------
void FONT::DrawPrim( void ){

	if ( buff.size() && buff[0] ) {
		const auto	trans	= TransMatrix( World() );
		const auto	scale	= VECTOR2{ sv.x, sv.y } * 2.0f;
		const auto	rotate	= 0.0f;
		const auto	pivot	= VECTOR2( pv.x, pv.y );
		const auto	layer	= GetLayer();
		const auto	effects	= (GetRevV()<<1)|(GetRevU());

		device->DrawPrim( buff.data(), trans, scale, rotate, pivot, *this, effects, layer );
	}
}

//----------------------------------------
// サイズ
//----------------------------------------
void FONT::SetSize( float w, float h ){

	SIZE2::SetSize( w, h );
	device->Size(   w, h );
}

void FONT::SetSize( const dx::FONT* v ){

	if ( v ) {
		FONT::SetSize( v->FONT::GetSizeW(), v->FONT::GetSizeH() );
	}
}

//----------------------------------------
// フォント種別
//----------------------------------------
void FONT::SetFont( const char* f ){

	if ( f ) {
		SetFont( std::string{f} );
	} else {
		SetFont( nullptr );
	}
}

void FONT::SetFont( const std::string& f ){

	if ( const auto p = GetFont(); !p || std::string{p} != f ) {
		FONT::FreeDevice( 0 );
		font = absolute_path( f ) ? f : defaultpath + "/" + f;
		FONT::InitDevice( 0 );
	}
}

void FONT::SetFont( std::nullptr_t ){

	FONT::FreeDevice( 0 );
	font = defaultpath + "/" + defaultfont;
	FONT::InitDevice( 0 );
}

const char* FONT::GetFont( void ) const { return font.c_str();	}

// End Of File
