//========================================
// OPAL Game Library
//========================================
#include "shape_dx.hpp"

//========================================
// シェイプタスク
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SHAPE::SHAPE(){}
SHAPE::~SHAPE(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void SHAPE::Init( const char* p ){

	DRAWB::Init();
	DRAWT2::Init( p );

	COLOR::SetColor( 0xff,0xff,0xff );
}

void SHAPE::Free( void ) {

	DRAWT2::Free();
	DRAWB::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void SHAPE::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "SHAPE" );
	printd( "%-08s ", ID() );
	SHAPE::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void SHAPE::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void SHAPE::DrawMain( void ){ DrawPrim();	}

// End Of File
