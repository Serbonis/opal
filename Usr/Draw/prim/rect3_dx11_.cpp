//========================================
// OPAL Game Library
//========================================
#include "rect3_dx11.hpp"
#include "rect3_dx11_.hpp"

//========================================
// 矩形タスク(3D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<RECT3_>	device{"RECT3"};
}
IDEVICE_( RECT3_ );

void RECT3_::CreateDevice(  SDEVICE<RECT3_>* d ){ if ( d ) { *d = device.CreateDevice();	}	}
void RECT3_::DestroyDevice( SDEVICE<RECT3_>* d ){ if ( d ) { *d = nullptr; }					}

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT3_::RECT3_() {}
RECT3_::~RECT3_(){}

ID3D11Buffer*		RECT3_::vbuffer	= nullptr;
ID3D11InputLayout*	RECT3_::layout	= nullptr;
ID3D11VertexShader*	RECT3_::vshader	= nullptr;
ID3D11PixelShader*	RECT3_::pshader	= nullptr;
ID3D11Buffer*		RECT3_::cbuffer	= nullptr;

//----------------------------------------
// デバイス初期化
//----------------------------------------
namespace {
#include "xs_rect3.fxh"
#include "vs_rect3.fxh"
#include "ps_rect3.fxh"
#include "vs_rect3.cpp"
#include "ps_rect3.cpp"

	constexpr FVF_VIT	vertexlist[] = {
		{ { -0.5f, -0.5f, 0.0f }, 3UL, { 0.0f, 1.0f } },
		{ { -0.5f,  0.5f, 0.0f }, 0UL, { 0.0f, 0.0f } },
		{ {  0.5f, -0.5f, 0.0f }, 2UL, { 1.0f, 1.0f } },
		{ {  0.5f,  0.5f, 0.0f }, 1UL, { 1.0f, 0.0f } },
	};

	constexpr VS_ELEMENT_DESC	element[]= {
		VS_ELEMENT( VS_ELEMENT_S0, 0, VS_ELEMENT_F0 ),
		VS_ELEMENT( VS_ELEMENT_S1, 0, VS_ELEMENT_F1 ),
		VS_ELEMENT( VS_ELEMENT_S2, 0, VS_ELEMENT_F2 ),
	};

	constexpr auto	vs_p = VS_HLSL( rect3 );
	constexpr auto	ps_p = PS_HLSL( rect3 );
	constexpr auto	vs_s = sizeof( VS_HLSL( rect3 ) );
	constexpr auto	ps_s = sizeof( PS_HLSL( rect3 ) );
}

int RECT3_::InitDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device, &device );
	}

	RECTX_::InitDevice( n );

	using namespace DRAWX_DX11;

	REGISTER( vbuffer, []{ CreateVertexBuffer(   vbuffer, vertexlist, sizeof( FVF_VIT ) * std::size( vertexlist ) ); } );
	REGISTER( cbuffer, []{ CreateConstantBuffer( cbuffer, sizeof( CONST_BUFFER_RECT3 ) ); } );

	REGISTER( layout,  []{ CreateInputLayout(  layout,  vs_p, vs_s, element, std::size( element ) ); } );
	REGISTER( vshader, []{ CreateVertexShader( vshader, vs_p, vs_s ); } );
	REGISTER( pshader, []{ CreatePixelShader(  pshader, ps_p, ps_s ); } );

	ShaderReflection( vs_p, vs_s, "VERTEX SHADER" );
	ShaderReflection( ps_p, ps_s, "PIXEL  SHADER" );

	return 0;
}

//----------------------------------------
// コンテキスト切替
//----------------------------------------
RECTX_* RECT3_::Context( void ){

	RECTX_::vbuffer	= vbuffer;
	RECTX_::layout	= layout;
	RECTX_::vshader	= vshader;
	RECTX_::pshader	= pshader;
	RECTX_::cbuffer	= cbuffer;

	return this;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int RECT3_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device );
	}

	RELEASE( vbuffer );
	RELEASE( cbuffer );

	RELEASE( layout  );
	RELEASE( vshader );
	RELEASE( pshader );

	RECTX_::FreeDevice( n );

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void RECT3_::DestroyDevice( void ){ device.DestroyDevice();	}

// End Of File
