//========================================
// Pixel Shader
//========================================
#include "xs_rect3.fxh"
#include "ps_rect3.fxh"

float4 ps_rect3( PS_INPUT input ) : SV_Target
{
	return input.col * TEXTURE_2D_RECT3.Sample( SAMPLER_STATE_RECT3, input.tex );
}

// End Of File
