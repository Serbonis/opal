//========================================
// OPAL Game Library
//========================================
#include "rect3_dx11.hpp"
#include "rect3_dx11_.hpp"

//========================================
// 矩形タスク(3D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT3::RECT3() :
	device( nullptr )
{}
RECT3::~RECT3(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void RECT3::Init( const char* p ){

	dx::RECT3::Init( p );

	RECT3::InitDevice( 0 );
}

void RECT3::Free( void ){

	RECT3::FreeDevice( 0 );

	dx::RECT3::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int RECT3::InitDevice( int n ){

	if ( !device ) {
		RECT3_::CreateDevice( &device );
	}

	return 0;
}

int RECT3::FreeDevice( int n ){

	if ( device ) {
		RECT3_::DestroyDevice( &device );
	}

	return 0;
}

//----------------------------------------
// テクスチャーに合わせる
//----------------------------------------
void RECT3::SetSize( void ){

	if ( const auto t = GetTexture() ) {
		RECT3::SetSize( ( float )t->Width(),
						( float )t->Height() );
	}
}

//----------------------------------------
// 描画
//----------------------------------------
namespace {
#include "xs_rect3.fxh"
#include "vs_rect3.fxh"
#include "ps_rect3.fxh"
}

void RECT3::DrawPrim( void ){

	CONST_BUFFER_RECT3	cbuff{};

	{
		const auto	sz = GetSize();
		const auto	sv = VECTOR3{sz.w,sz.h,1};
		const auto	pv = GetPivot();
		const auto	rv = []( auto a ){
			switch ( a ) {
			case AXIS_PZ: return VECTOR3{RAD(   0 ), RAD( -180 ), 0};
			case AXIS_MZ: return VECTOR3{RAD(   0 ), RAD(    0 ), 0};
			case AXIS_PY: return VECTOR3{RAD( +90 ), RAD(    0 ), 0};
			case AXIS_MY: return VECTOR3{RAD( -90 ), RAD(    0 ), 0};
			case AXIS_PX: return VECTOR3{RAD(   0 ), RAD(  -90 ), 0};
			case AXIS_MX: return VECTOR3{RAD(   0 ), RAD(  +90 ), 0};
			};
			return VECTOR3{};
		}( axis );

		MATRIX		sm;
		MATRIX		rm;

		MatrixScale(  &sm, &sv );
		MatrixRotate( &rm, &rv );

		cbuff.World		= World();
		cbuff.Proj		= Proj();
		cbuff.View		= View();
		cbuff.Pivot		= pv;
		cbuff.Axis		= sm * rm;
	}

	{
		const auto	sm = hlsl::Sampler( GetRect() ) *  MatrixScale( GetRevU() ? -1 : +1, GetRevV() ? -1 : +1, 1 );

		cbuff.Sampler	= sm;
	}

	{
		const auto cl = RECT3::Color();

		for ( auto i = 0UL; i < 4; i++ ) {
			cbuff.Diffuse[i] = { cl[i].R(),
								 cl[i].G(),
								 cl[i].B(),
								 cl[i].A() };
		}
	}

	RECTX::DrawPrim( device->Context(), sizeof( FVF_VIT ), 0UL, &cbuff, GetTexture(), GetAlpha( 1 ) ? this : nullptr );
}

// End Of File
