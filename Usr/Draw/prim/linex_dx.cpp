//========================================
// OPAL Game Library
//========================================
#include "linex_dx.hpp"

//========================================
// 直線タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
LINEX::LINEX() :
	node( 0 ),
	bold( 0.0f ),
	loop( false )
{}
LINEX::~LINEX(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LINEX::Init( const char* p ){

	DRAWB::Init();
	COLOR::SetColor( 0xff,0xff,0xff );

	node	= 0;
	bold	= 2.0f;
	loop	= false;
}

void LINEX::Free( void ){

	DRAWB::Free();
}

//----------------------------------------
// 各種設定
//----------------------------------------
void LINEX::SetNode( UINT  n ){ node = n; }
void LINEX::SetBold( float b ){ bold = b; }
void LINEX::SetLoop( bool  f ){ loop = f; }

UINT  LINEX::GetNode( void ) const { return node; }
float LINEX::GetBold( void ) const { return bold; }
bool  LINEX::GetLoop( void ) const { return loop; }

//----------------------------------------
// タスクの情報
//----------------------------------------
void LINEX::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
#endif
}

// End Of File
