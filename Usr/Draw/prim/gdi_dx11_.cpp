//========================================
// OPAL Game Library
//========================================
#include "gdi_dx11.hpp"
#include "gdi_dx11_.hpp"
#include "surface_dx11_.hpp"

//========================================
// GDIタスク
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<GDI_>	device{"GDI"};
}
IDEVICE_( GDI_ );

template<> IDEVICE<GDI_MAP>::S_PTR	GDI_MAP::devmap = nullptr;
template<> IDEVICE<GDI_MAP>			GDI_MAP::device{"GDI_MAP"};
IDEVICE_( GDI_MAP );

namespace {
	constexpr auto	DEFAULT_CANVAS_ = 8UL;
}
UINT GDI_::DEFAULT_CANVAS( void ){ return DEFAULT_CANVAS_; 	}

//----------------------------------------
// constructor & destructor
//----------------------------------------
GDI_::GDI_( const void* d ) :
	data( d ),
	canvas{},
	target()
{}
GDI_::~GDI_(){}

//----------------------------------------
// デバイス初期化
//----------------------------------------
int GDI_::InitDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( DeviceName( NAME( "%p:GDI_", this ) ), this );
	}

	const auto	gd = reinterpret_cast<const SSIZEU*>( data );
	const auto	nw = ( gd->w ) ? gd->w : DEFAULT_CANVAS_;
	const auto	nh = ( gd->h ) ? gd->h : DEFAULT_CANVAS_;
	const auto	cw = std::pow( 2, nw );
	const auto	ch = std::pow( 2, nh );
	const auto	cs = SSIZEU( cw, ch );

	if ( canvas != cs ) {
		canvas = cs;
		target.Init( "GDI-TARGET", cw, ch );
	}
	return 0;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int GDI_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( DeviceName() );
	}

	target.Free();
	canvas = SSIZEU{};

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void GDI_::DestroyDevice( void ){ device.DestroyDevice();	}

//----------------------------------------
// リソース
//----------------------------------------
SSIZEU GDI_::Canvas( void ) const { return canvas;	}
const IDXGDI* GDI_::Target( void ) const { return &target; }

//----------------------------------------
// 描画
//----------------------------------------
void GDI_::DrawPrim( std::function<void(HDC,UINT,UINT)> gdidraw, COLOR col, COLOR key, const TEXTURE* texture ){

	if ( texture ){
		if ( const auto tx = texture->Texture() ) {
			if ( const auto rt = tx->Texture() ) {
				ComPtr<IDXGISurface2>	surface = nullptr;

				rt->QueryInterface( IID_IDXGISurface2, ( void** )&surface );
				{
					HDC	hdc{};

					surface->GetDC( false, &hdc );
					gdidraw( hdc, canvas.w, canvas.h );
					surface->ReleaseDC( nullptr );
				}
			}
		}
	}
}

// End Of File
