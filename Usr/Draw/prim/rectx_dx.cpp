//========================================
// OPAL Game Library
//========================================
#include "rectx_dx.hpp"

//========================================
// 矩形タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECTX::RECTX() {}
RECTX::~RECTX(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void RECTX::Init( const char* p ){

	DRAWB::Init();

	COLORS::SizeColor( 4 );
	COLORS::SetColor( WHITE );

	SIZE2::SetSize( 0.0f, 0.0f );

	RECTX::SetRect( 0.0f, 0.0f, 1.0f, 1.0f );
}

void RECTX::Free( void ) {

	DRAWB::Free();
}

//----------------------------------------
// 領域の削除
//----------------------------------------
void RECTX::SetRect( void ){

	RECTX::SetRect( 0.0f, 0.0f, 1.0f, 1.0f );
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void RECTX::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
#endif
}

// End Of File
