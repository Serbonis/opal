//========================================
// OPAL Game Library
//========================================
#include "gdi_dx11.hpp"
#include "gdi_dx11_.hpp"

//========================================
// GDIタスク
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
GDI::GDI() :
	device( nullptr ),
	data( nullptr )
{}
GDI::~GDI(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void GDI::Init( const char* p ){

	dx::GDI::Init( p );

	GDI::SetCanvas();
}

void GDI::Free( void ) {

	GDI::SetCanvas( 0 );

	dx::GDI::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
namespace {
	UINT mapkey( const void* d, UINT c ){

		if ( d ) {
			const auto	p = reinterpret_cast<const SSIZEU*>( d );
			const auto	w = ( p->w ) ? p->w : c;
			const auto	h = ( p->h ) ? p->h : c;

			return (w<<16)|h;
		}
		return 0;
	}
}

int GDI::InitDevice( int n ){

	if ( !device ) {
		if ( n == 0 ) {
			const auto	key = mapkey( data, device->DEFAULT_CANVAS() );

			DRAWX::DeviceLost( DeviceName( NAME( "%p:%s:%08x", this, "(GDI)", key ) ), this );
			device = GDI_MAP::CreateDevice( key, data );
		}
	}

	return 0;
}

int GDI::FreeDevice( int n ){

	if ( device ) {
		if ( n == 0 ) {
			DRAWX::DeviceLost( DeviceName() );
			device = nullptr;
			GDI_MAP::DestroyDevice( mapkey( data, device->DEFAULT_CANVAS() ) );
		}
	}

	return 0;
}

void GDI::DeviceData( const void* d[] ){

	if ( !data ) {
		data = NEWS<SSIZEU>();
	}

	if ( d ) {
		const auto gd = reinterpret_cast<SSIZEU*>( data );

		*gd = SSIZEU{*reinterpret_cast<const UINT*>( d[0] ),
					 *reinterpret_cast<const UINT*>( d[1] )};
	}
}

void GDI::DeviceData( void ){

	if ( data ) {
		DELETES<SSIZEU>( data );
	}
}

//----------------------------------------
// 生成＆破棄
//----------------------------------------
void GDI::SetCanvas( void ){

	const auto	n = device->DEFAULT_CANVAS();

	GDI::SetCanvas( n, n );
}

void GDI::SetCanvas( UINT n ){

	GDI::SetCanvas( n, n );
}

void GDI::SetCanvas( UINT w, UINT h ){

	if ( device ) {
		GDI::FreeDevice( 0 );
		GDI::DeviceData();
	}

	if ( w && h ) {
		const void*	d[] = { &w, &h, };

		GDI::DeviceData( d );
		GDI::InitDevice( 0 );
	}
}

opal::SSIZEU GDI::GetCanvas( void ) const { return device->Canvas(); }

//----------------------------------------
// 描画
//----------------------------------------
void GDI::DrawPrim( void ){

	RECT2::SetTexture( device->Target() );

	device->DrawPrim( [this](HDC hdc, UINT w, UINT h){ DrawGDI( hdc, w, h ); }, col, key, RECT2::GetTexture() );

	RECT2::DrawMain();
}

// End Of File
