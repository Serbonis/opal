//========================================
// Vertex Shader Sample
//========================================
#include "xs_line2.fxh"
#include "vs_line2.fxh"
#include "ps_line2.fxh"

VS_OUTPUT vs_line2( VS_INPUT input )
{
	VS_OUTPUT	output = ( VS_OUTPUT )0;

	output.pos	= mul( input.pos - Pivot, World );
	output.col	= Diffuse;

	return output;
}

// End Of File
