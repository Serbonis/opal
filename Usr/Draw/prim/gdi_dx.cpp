//========================================
// OPAL Game Library
//========================================
#include "gdi_dx.hpp"

//========================================
// GDIタスク
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
GDI::GDI() :
	col(),
	key(),
	brush()
{}
GDI::~GDI(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void GDI::Init( const char* p ){

	col		= COLOR();
	key 	= COLOR();
	brush	= nullptr;

	GDI::SetBrush( WHITE );

	RECT2::Init( p );
	RECT2::SetTexture( nullptr );
}

void GDI::Free( void ) {

	RECT2::SetTexture( nullptr );
	RECT2::Free();

	GDI::SetBrush();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void GDI::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "GDI" );
	printd( "%-08s ", ID() );
	GDI::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void GDI::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void GDI::DrawMain( void ){ DrawPrim();	}

//----------------------------------------
// ブラシ
//----------------------------------------
void GDI::SetBrush( COLOR c ){

	GDI::SetBrush();
	brush = ::CreateSolidBrush( (( DWORD )c) & 0x00ffffff );
}

void GDI::SetBrush( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){

	GDI::SetBrush( COLOR( r, g, b, a ) );
}

void GDI::SetBrush( void ){

	if ( brush ) {
		DeleteObject( brush );
		brush = nullptr;
	}
}

HBRUSH GDI::GetBrush( void ) const { return brush; }

//----------------------------------------
// カラー
//----------------------------------------
void  GDI::SetCol( COLOR c ){ col = c; }
void  GDI::SetKey( COLOR k ){ key = k; }
void  GDI::SetCol( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ col = COLOR_RGB( r,g,b,a ); }
void  GDI::SetKey( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ key = COLOR_RGB( r,g,b,a ); }
COLOR GDI::GetCol( void ) const { return col; }
COLOR GDI::GetKey( void ) const { return key; }

// End Of File
