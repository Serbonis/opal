//========================================
// Pixel Shader
//========================================
#include "xs_line2.fxh"
#include "ps_line2.fxh"

float4 ps_line2( PS_INPUT input ) : SV_Target
{
	PS_OUTPUT	output;

	output.col = input.col;

	return output.col;
}

// End Of File
