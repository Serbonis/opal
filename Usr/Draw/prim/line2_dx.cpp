//========================================
// OPAL Game Library
//========================================
#include "line2_dx.hpp"

//========================================
// 直線タスク(2D)
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
LINE2::LINE2(){}
LINE2::~LINE2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LINE2::Init( const char* p ){

	DRAWT2::Init( p );
	LINEX::Init(  p );

	lv.a.clear();
	lv.b.clear();
}

void LINE2::Free( void ){

	lv.a.clear();
	lv.b.clear();

	LINEX::Free();
	DRAWT2::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void LINE2::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "LINE2" );
	printd( "%-08s ", ID() );
	LINE2::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void LINE2::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void LINE2::DrawMain( void ){ DrawPrim();	}

//----------------------------------------
// ノード数
//----------------------------------------
void LINE2::SetNode( UINT n ){

	if ( LINEX::GetNode() != n ) {
		LINEX::SetNode( n );

		lv.a.resize( ( n+1 ) * 1 );
		lv.b.resize( ( n+1 ) * NODE_PACK );
	}
}

//----------------------------------------
// SET
//----------------------------------------
void LINE2::SetNode_( int id, float x, float y ){

	if ( const int n = LINEX::GetNode(); id >= 0 ) {
		if ( +id < n ) {
			lv.a[id+0].x = x;
			lv.a[id+0].y = y;
		}
	} else {
		if ( -id < n ) {
			lv.a[id+n].x = x;
			lv.a[id+n].y = y;
		}
	}
}

void LINE2::SetNode( int id, const VECTOR2* v ){ if ( v ) { SetNode_( id, v->x, v->y );	}	}
void LINE2::SetNode( int id, const VECTOR3* v ){ if ( v ) { SetNode_( id, v->x, v->y );	}	}
void LINE2::SetNode( int id, const VECTOR2& v ){ SetNode_( id, v.x, v.y );	}
void LINE2::SetNode( int id, const VECTOR3& v ){ SetNode_( id, v.x, v.y );	}
void LINE2::SetNode( int id, float x, float y ){ SetNode_( id, x, y );	}

//----------------------------------------
// ADD
//----------------------------------------
void LINE2::AddNode_( int id, float x, float y ){

	if ( const int n = LINEX::GetNode(); id >= 0 ) {
		if ( +id < n ) {
			lv.a[id+0].x += x;
			lv.a[id+0].y += y;
		}
	} else {
		if ( -id < n ) {
			lv.a[id+n].x += x;
			lv.a[id+n].y += y;
		}
	}
}

void LINE2::AddNode( int id, const VECTOR2* v ){ if ( v ) { AddNode_( id, v->x, v->y );	}	}
void LINE2::AddNode( int id, const VECTOR3* v ){ if ( v ) { AddNode_( id, v->x, v->y );	}	}
void LINE2::AddNode( int id, const VECTOR2& v ){ AddNode_( id, v.x, v.y );	}
void LINE2::AddNode( int id, const VECTOR3& v ){ AddNode_( id, v.x, v.y );	}
void LINE2::AddNode( int id, float x, float y ){ AddNode_( id, x, y );	}

//----------------------------------------
// GET
//----------------------------------------
void LINE2::GetNode_( int id, float* x, float *y ) const {

	if ( const int n = LINEX::GetNode(); id >= 0 ) {
		if ( +id < n ) {
			if ( x ) { *x = lv.a[id+0].x;	}
			if ( y ) { *y = lv.a[id+0].y;	}
		}
	} else {
		if ( -id < n ) {
			if ( x ) { *x = lv.a[id+n].x;	}
			if ( y ) { *y = lv.a[id+n].y;	}
		}
	}
}

void LINE2::GetNode( int id, float* x, float *y ) const { GetNode_( id, x, y );	}
auto LINE2::GetNode( int id ) const->VECTOR2{ VECTOR2 v; GetNode_( id, &v.x, &v.y ); return v;	}

//----------------------------------------
// MUL
//----------------------------------------
void LINE2::MulNode_( int id, float r ){

	if ( const int n = LINEX::GetNode(); id >= 0 ) {
		if ( +id < n ) { lv.a[id+0] *= r;	}
	} else {
		if ( -id < n ) { lv.a[id+n] *= r;	}
	}
}

void LINE2::MulNode( int id, float r ){ MulNode_( id, r );	}

void LINE2::MulNode( float r ){

	if ( const int n = LINE2::GetNode() ) {
		for ( auto i = 0; i < n+1; i++ ) {
			MulNode_( i, r );
		}
	}
}

//----------------------------------------
// ワールド座標
//----------------------------------------
VECTOR2 LINE2::Node( int id ) const { return World() * GetNode( id );	}
VECTOR2 LINE2::Node( void   ) const { return Node( 1 ) - Node( 0 );		}

//----------------------------------------
// 直線の場合
//----------------------------------------
void LINE2::SetLine_( const VECTOR2* sv, const VECTOR2* ev ){

	if ( sv ) { SetNode_( 0, sv->x, sv->y );	}
	if ( ev ) { SetNode_( 1, ev->x, ev->y );	}
}

void LINE2::GetLine_( VECTOR2* sv, VECTOR2* ev ) const {

	if ( sv ) { GetNode_( 0, &sv->x, &sv->y );	}
	if ( ev ) { GetNode_( 1, &ev->x, &ev->y );	}
}

void LINE2::SetLine( const VECTOR2* sv, const VECTOR2* ev ){ SetLine_(  sv,  ev );	}
void LINE2::SetLine( const VECTOR2* sv, const VECTOR2& ev ){ SetLine_(  sv, &ev );	}
void LINE2::SetLine( const VECTOR2& sv, const VECTOR2* ev ){ SetLine_( &sv,  ev );	}
void LINE2::SetLine( const VECTOR2& sv, const VECTOR2& ev ){ SetLine_( &sv, &ev );	}

void LINE2::SetLine( const VECTOR2* ev ){ SetLine_( nullptr,  ev );	}
void LINE2::SetLine( const VECTOR2& ev ){ SetLine_( nullptr, &ev );	}

void LINE2::SetLine( float sx, float sy, float ex, float ey ){

	const auto	sv = VECTOR2( sx, sy );
	const auto	ev = VECTOR2( ex, ey );

	SetLine_( &sv, &ev );
}

void LINE2::SetLine( float ex, float ey ){

	const auto	ev = VECTOR2( ex, ey );

	SetLine_( nullptr, &ev );
}

void LINE2::GetLine( VECTOR2* sv, VECTOR2* ev ) const { GetLine_( sv, ev );	}

void LINE2::GetLine( VECTOR2* pv, UINT n ) const {

	if ( !(n&1) ) {
		GetLine_( pv, nullptr );
	} else {
		GetLine_( nullptr, pv );
	}
}

auto LINE2::GetLine( UINT n ) const->VECTOR2{

	VECTOR2 pv;

	if ( !(n&1) ) {
		GetLine_( &pv, nullptr );
	} else {
		GetLine_( nullptr, &pv );
	}

	return pv;
}

void LINE2::GetLine( float* sx, float* sy, float* ex, float* ey ) const {

	VECTOR2	sv;
	VECTOR2	ev;

	GetLine_( &sv, &ev );

	if ( sx ) { *sx = sv.x;	}
	if ( sy ) { *sy = sv.y;	}
	if ( ex ) { *ex = ev.x;	}
	if ( ey ) { *ey = ev.y;	}
}

void LINE2::GetLine( float* ex, float* ey ) const {

	VECTOR2	ev;

	GetLine_( nullptr, &ev );

	if ( ex ) { *ex = ev.x;	}
	if ( ey ) { *ey = ev.y;	}
}

// End Of File
