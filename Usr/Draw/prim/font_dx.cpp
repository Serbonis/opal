//========================================
// OPAL Game Library
//========================================
#include "font_dx.hpp"

//========================================
// フォントタスク
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
FONT::FONT() :
	area{},
	cols{0},
	rows{0},
	leng{0},
	buff{}
{}
FONT::~FONT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void FONT::Init( const char* p ){

	DRAWT2::Init( p );

	COLOR::SetColor( WHITE );
	SIZE2::SetSize( 44, 42 );

	area.w	= 1600;
	area.h	= 1200;
	cols	= 0;
	rows	= 0;
	leng	= 0;

	buff.resize( 0x100 );

	FONT::Print( "hello world\n" );
}

void FONT::Free( void ) {

	buff.clear();

	DRAWT2::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void FONT::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "FONT" );
	printd( "%-08s ", ID() );
	FONT::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void FONT::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void FONT::DrawMain( void ){ DrawPrim();	}

//----------------------------------------
// オフセット/ピボット
//----------------------------------------
void FONT::SetOffset( void ){ FONT::SetOffset( FONT::Width()/2, FONT::Height()/2 );	}
void FONT::SetPivot(  void ){ FONT::SetPivot(  FONT::Width()/2, FONT::Height()/2 );	}

//----------------------------------------
// 表示エリア
//----------------------------------------
void FONT::SetArea( UINT w, UINT h ){

	area.w = w;
	area.h = h;
}

SSIZEU FONT::GetArea( void ) const { return area; }

//----------------------------------------
// センタリング
//----------------------------------------
void FONT::Center( float w ){

	FONT::SetTransX( ( w - FONT::Width() )/2.0f );
}

void FONT::Center( float w, float y ){

	FONT::Center( w );
	FONT::SetTransY( y );
}

//----------------------------------------
// 情報取得
//----------------------------------------
float FONT::Width(  void ) const { return ( float )SIZE2::W()/2 * cols; };
float FONT::Height( void ) const { return ( float )SIZE2::H()/1 * rows; };
UINT  FONT::Leng( void ) const { return leng; }
UINT  FONT::Cols( void ) const { return cols; }
UINT  FONT::Rows( void ) const { return rows; }

const char* FONT::Buff( void ) const { return buff.data(); };

char FONT::Buff( int vIndex ) const {

	const auto	n = ( int )leng;

	if ( +n <= vIndex || vIndex < -n ) {
		return '\0';
	}
	return ( vIndex >= 0 )
		? buff[vIndex+0]
		: buff[vIndex+n];
}

//----------------------------------------
// クリア
//----------------------------------------
void FONT::Clear( void ){

	leng	= 0;
	cols	= 0;
	rows	= 0;
	buff[0]	= '\0';
}

//----------------------------------------
// 表示
//----------------------------------------
void FONT::Print( const char* format, ... ){

	if ( format ) {
		va_list	list;

		va_start( list, format );
		if ( const auto l = strlenf_s( format, list ); buff.size() < l+1 ) {
			buff.resize( l+1 );
		}
		::vsprintf( buff.data(), format, list );
		va_end( list );
	} else {
		buff[0] = '\0';
	}

	leng = strlen_s( buff.data() );
	strcoline( buff.data(), &cols, &rows );
}

//----------------------------------------
// 書式をそのまま転送
//----------------------------------------
void FONT::String( const char* format ){

	if ( format ) {
		if ( const auto l = strlen_s( format ); buff.size() < l+1 ) {
			buff.resize( l+1 );
		}
		strcpy_s( buff.data(), format );
	} else {
		buff[0] = '\0';
	}

	leng = strlen_s( buff.data() );
	strcoline( buff.data(), &cols, &rows );
}

// End Of File
