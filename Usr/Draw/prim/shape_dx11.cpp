//========================================
// OPAL Game Library
//========================================
#include "shape_dx11.hpp"
#include "shape_dx11_.hpp"

//========================================
// シェイプタスク
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SHAPE::SHAPE() :
	device( nullptr )
{}
SHAPE::~SHAPE(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void SHAPE::Init( const char* p ){

	dx::SHAPE::Init( p );

	SHAPE::InitDevice( 0 );
}

void SHAPE::Free( void ){

	SHAPE::FreeDevice( 0 );

	dx::SHAPE::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int SHAPE::InitDevice( int n ){

	if ( !device ) {
		SHAPE_::CreateDevice( &device );
	}

	return 0;
}

int SHAPE::FreeDevice( int n ){

	if ( device ) {
		SHAPE_::DestroyDevice( &device );
	}

	return 0;
}

//----------------------------------------
// 描画
//----------------------------------------
#include "xs_com.hpp"

namespace {
#include "xs_shape.fxh"
#include "vs_shape.fxh"
#include "ps_shape.fxh"
}

void SHAPE::DrawPrim( void ){

	CONST_BUFFER_SHAPE	cbuff{};

	CONST_BUFFER_2D( this, cbuff );

	SHAPE::DrawPrim( device->Context(), sizeof( FVF_VIT ), 0UL, &cbuff, GetPolygon(), GetAlpha( 1 ) ? this : nullptr );
}

#include "polygon_dx11_.hpp"

void SHAPE::DrawPrim( SHAPE_* device, UINT size, UINT offset, const void* cbuff, const POLYGON* polygon, const DRAWB* drawb ){

	const auto	context = DRAWX_DX11::device( 0 );

	const auto	index = [&]( auto device ){
							context->IASetVertexBuffers( 0, 1, &device->Vbuffer(), &size, &offset );
							context->IASetIndexBuffer( device->Ibuffer(), DXGI_FORMAT_R16_UINT, 0 );
							context->IASetPrimitiveTopology( device->Topology() );
							return device->Index();
						}( polygon->Polygon() );

	context->IASetInputLayout( device->layout );

	context->VSSetShader( device->vshader, nullptr, 0 );
	context->PSSetShader( device->pshader, nullptr, 0 );

	context->UpdateSubresource( device->cbuffer, 0, nullptr, cbuff, 0, 0 );

	context->VSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->PSSetConstantBuffers( 0, 1, &device->cbuffer );

	if ( drawb && drawb->GetAlphaBlend() ) {
		context->OMSetBlendState( device->blend, nullptr, 0xffffffff );
	}

	context->OMSetDepthStencilState( device->depthstencil, 0x00000000 );

	context->DrawIndexed( index, 0, 0 );
}

// End Of File
