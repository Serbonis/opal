//========================================
// Pixel Shader
//========================================
#include "xs_rect2.fxh"
#include "ps_rect2.fxh"

float4 ps_rect2( PS_INPUT input ) : SV_Target
{
	return input.col * TEXTURE_2D_RECT2.Sample( SAMPLER_STATE_RECT2, input.tex );
}

// End Of File
