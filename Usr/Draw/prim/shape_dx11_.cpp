//========================================
// OPAL Game Library
//========================================
#include "shape_dx11.hpp"
#include "shape_dx11_.hpp"

//========================================
// シェイプタスク
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<SHAPE_>	device{"SHAPE"};
}
IDEVICE_( SHAPE_ );

void SHAPE_::CreateDevice(  SDEVICE<SHAPE_>* d ){ if ( d ) { *d = device.CreateDevice();	}	}
void SHAPE_::DestroyDevice( SDEVICE<SHAPE_>* d ){ if ( d ) { *d = nullptr; }					}

//----------------------------------------
// constructor & destructor
//----------------------------------------
SHAPE_::SHAPE_() {}
SHAPE_::~SHAPE_(){}

ID3D11InputLayout*			SHAPE_::layout		= nullptr;
ID3D11VertexShader*			SHAPE_::vshader		= nullptr;
ID3D11PixelShader*			SHAPE_::pshader		= nullptr;
ID3D11Buffer*				SHAPE_::cbuffer		= nullptr;
ID3D11BlendState*			SHAPE_::blend		= nullptr;
ID3D11DepthStencilState*	SHAPE_::depthstencil= nullptr;;

//----------------------------------------
// デバイス初期化
//----------------------------------------
namespace {
#include "xs_shape.fxh"
#include "vs_shape.fxh"
#include "ps_shape.fxh"
#include "vs_shape.cpp"
#include "ps_shape.cpp"

	constexpr VS_ELEMENT_DESC	element[]= {
		VS_ELEMENT( VS_ELEMENT_S0, 0, VS_ELEMENT_F0 ),
	};

	constexpr auto	vs_p = VS_HLSL( shape );
	constexpr auto	ps_p = PS_HLSL( shape );
	constexpr auto	vs_s = sizeof( VS_HLSL( shape ) );
	constexpr auto	ps_s = sizeof( PS_HLSL( shape ) );
}

int SHAPE_::InitDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device, &device );
	}

	using namespace DRAWX_DX11;

	REGISTER( layout,  []{ CreateInputLayout(  layout,  vs_p, vs_s, element, std::size( element ) ); } );
	REGISTER( vshader, []{ CreateVertexShader( vshader, vs_p, vs_s ); } );
	REGISTER( pshader, []{ CreatePixelShader(  pshader, ps_p, ps_s ); } );

	ShaderReflection( vs_p, vs_s, "VERTEX SHADER" );
	ShaderReflection( ps_p, ps_s, "PIXEL  SHADER" );

	REGISTER( cbuffer, []{ CreateConstantBuffer( cbuffer, sizeof( CONST_BUFFER_SHAPE ) ); } );

	REGISTER( blend,        []{ CreateBlendState( blend, BLEND_TYPE1  ); } );
	REGISTER( depthstencil,	[]{ CreateDepthStencilState( depthstencil ); } );

	return 0;
}

//----------------------------------------
// コンテキスト切替
//----------------------------------------
SHAPE_* SHAPE_::Context( void ){

	return this;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int SHAPE_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device );
	}

	RELEASE( layout  );
	RELEASE( vshader );
	RELEASE( pshader );

	RELEASE( cbuffer );

	RELEASE( blend,        nullptr );
	RELEASE( depthstencil, nullptr );

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void SHAPE_::DestroyDevice( void ){ device.DestroyDevice();	}

// End Of File
