//========================================
// OPAL Game Library
//========================================
#include "rectx_dx11.hpp"
#include "rectx_dx11_.hpp"
#include "texture_dx11_.hpp"

//========================================
// 矩形タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx11;

//========================================
// device interface
//========================================
RECTX_::RECTX_() {}
RECTX_::~RECTX_(){}

ID3D11Buffer*				RECTX_::ibuffer		= nullptr;
ID3D11PixelShader*			RECTX_::pshader_	= nullptr;

ID3D11Buffer*				RECTX_::vbuffer		= nullptr;
ID3D11InputLayout*			RECTX_::layout		= nullptr;
ID3D11VertexShader*			RECTX_::vshader		= nullptr;
ID3D11PixelShader*			RECTX_::pshader		= nullptr;
ID3D11Buffer*				RECTX_::cbuffer		= nullptr;
ID3D11BlendState*			RECTX_::blend		= nullptr;
ID3D11DepthStencilState*	RECTX_::depthstencil= nullptr;

//----------------------------------------
// データ
//----------------------------------------
namespace {
#include "ps_rectx.fxh"
#include "ps_rectx.cpp"

	constexpr WORD	indexlist[] = { 0, 1, 2, 3, };
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int RECTX_::InitDevice( int n ){

	using namespace DRAWX_DX11;

	REGISTER( ibuffer,      []{ CreateIndexBuffer( ibuffer,  indexlist,        sizeof( WORD ) * std::size( indexlist ) ); } );
	REGISTER( pshader_,     []{ CreatePixelShader( pshader_, PS_HLSL( rectx ), sizeof( PS_HLSL( rectx ) ) ); } );
	REGISTER( blend,        []{ CreateBlendState( blend, BLEND_TYPE1 ); } );
	REGISTER( depthstencil,	[]{ CreateDepthStencilState( depthstencil ); } );

	return 0;
}

int RECTX_::FreeDevice( int n ){

	RELEASE( ibuffer  );
	RELEASE( pshader_ );
	RELEASE( blend,        nullptr );
	RELEASE( depthstencil, nullptr );

	return 0;
}

// End Of File
