//========================================
// OPAL Game Library
//========================================
#include "marux.hpp"

//========================================
// 丸タスク
//========================================
using namespace opal;

namespace {
	constexpr auto	REGULAR = 36U;
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
MARUX::MARUX(){}
MARUX::~MARUX(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void MARUX::Init( void ){

	SIZE1::SetSize( 0.f );
	regular	= REGULAR;
}

void MARUX::Free( void ){}

//----------------------------------------
// 頂点数
//----------------------------------------
void MARUX::SetRegular( void   ){ regular = REGULAR;					}
void MARUX::SetRegular( UINT n ){ regular = n ? n * 4 : REGULAR * 10;	}
UINT MARUX::GetRegular( void ) const { return regular;	}

//----------------------------------------
// タスクの実行
//----------------------------------------
void MARUX::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
#endif
}

// End Of File
