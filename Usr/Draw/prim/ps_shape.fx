//========================================
// Pixel Shader
//========================================
#include "xs_shape.fxh"
#include "ps_shape.fxh"

float4 ps_shape( PS_INPUT input ) : SV_Target
{
	return input.col;
}

// End Of File
