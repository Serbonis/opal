//========================================
// OPAL Game Library
//========================================
#include "font_dx11.hpp"
#include "font_dx11_.hpp"

#if _MSC_VER
struct ID3D11VideoDevice {};
struct ID3D11VideoContext {};
struct ID3D11VideoDecoder {};
struct ID3D11CryptoSession {};
struct ID3D11VideoProcessor {};
struct ID3D11VideoProcessorEnumerator {};

using D3D11_VIDEO_DECODER_BUFFER_TYPE = int;
using D3D11_VIDEO_DECODER_DESC = int;
using D3D11_VIDEO_DECODER_CONFIG = int;
#endif

#include "SpriteFont.h"

//========================================
// フォントタスク
//========================================
using namespace opal;
using namespace opal::dx11;

using namespace DirectX;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<FONT_ >	device{"FONT"};
}
IDEVICE_( FONT_ );

template<> IDEVICE<FONT_MAP>::S_PTR	FONT_MAP::devmap = nullptr;
template<> IDEVICE<FONT_MAP>		FONT_MAP::device{"FONT_MAP"};
IDEVICE_( FONT_MAP );

//----------------------------------------
// constructor & destructor
//----------------------------------------
FONT_::FONT_( const void* d ) :
	data( d ),
	font( "" ),
	batch(  nullptr ),
	sprite( nullptr )
{}
FONT_::~FONT_(){}

//----------------------------------------
// デバイス初期化
//----------------------------------------
int FONT_::InitDevice( int n ){

	if ( n == 0 ) {
		const auto	fd = static_cast<const std::string*>( data );
		const auto	fs = std::ifstream( *fd );

		if ( fs.is_open() ) {
			if ( font != *fd ) {
				font = *fd;

				DRAWX::DeviceLost( DeviceName( NAME( "%p:FONT_", this ) ), this );

				const auto	context = DRAWX_DX11::device( 0 );
				const auto	device	= DRAWX_DX11::device();

				batch	= new SpriteBatch( context );
				sprite	= new SpriteFont( device, MTOW( font.c_str() ) );
			}
		}
	}

	return 0;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int FONT_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( DeviceName() );

		delete batch;
		delete sprite;
		batch  = nullptr;
		sprite = nullptr;
	}

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void FONT_::DestroyDevice( void ){ device.DestroyDevice();	}

//----------------------------------------
// 描画
//----------------------------------------
#include "draw_dx11.hpp"

void FONT_::DrawPrim( const char* buff, const VECTOR2& trans, const VECTOR2& scale, float rotate, const VECTOR2& pivot, COLOR color, UINT reverse, float layer ){

	if ( batch ) {
		batch->Begin();

		if ( sprite ) {
			const auto	string	= MTOW( buff );
			const auto	col_r	= color.R();
			const auto	col_g	= color.G();
			const auto	col_b	= color.B();
			const auto	col_a	= color.A();
			const auto	effects	= []( auto r ){
				switch ( r ) {
				case 1 : return SpriteEffects_FlipHorizontally;
				case 2 : return SpriteEffects_FlipVertically;
				case 3 : return SpriteEffects_FlipBoth;
				}
				return SpriteEffects_None;
			}( reverse );

			sprite->DrawString( batch, string,
								XMFLOAT2( trans.x, trans.y ),
								XMVECTORF32{ col_r, col_g, col_b, col_a },
								rotate,
								XMFLOAT2{ pivot.x, pivot.y },
								XMFLOAT2{ scale.x, scale.y },
								effects, layer );
		}
		batch->End();

		DRAWX_DX11::ResetState();
	}
}

//----------------------------------------
// サイズ
//----------------------------------------
SSIZEU FONT_::Size( void ) const { return SSIZEU{}; }
void FONT_::Size( float w, float h ){}

// End Of File
