//========================================
// OPAL Game Library
//========================================
#include "rect2_dx.hpp"

//========================================
// 矩形タスク
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT2::RECT2() {}
RECT2::~RECT2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void RECT2::Init( const char* p ){

	DRAWT2::Init( p );
	RECTX::Init(  p );

	SIZE2::SetSize( 100.0f, 100.0f );
	RECT2::SetTransZ( 0.0f );
}

void RECT2::Free( void ){

	RECTX::Free();
	DRAWT2::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void RECT2::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "RECT2" );
	printd( "%-08s ", ID() );
	RECT2::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void RECT2::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
	RECTX ::DrawInfo( m );
#endif
}

//----------------------------------------
// 描画
//----------------------------------------
void RECT2::DrawMain( void ){ DrawPrim();	}

//----------------------------------------
// サイズ
//----------------------------------------
void RECT2::SetSize(  const RECT2* p ){ if ( p ) { RECT2::SetSize( p->GetSizeW(),     p->GetSizeH()     ); } }
void RECT2::SetSizeW( const RECT2* p ){ if ( p ) { RECT2::SetSize( p->GetSizeW(),     RECT2::GetSizeH() ); } }
void RECT2::SetSizeH( const RECT2* p ){ if ( p ) { RECT2::SetSize( RECT2::GetSizeW(), p->GetSizeH()     ); } }

//----------------------------------------
// テクスチャ
//----------------------------------------
void RECT2::SetRect(   const RECT2* p ){ if ( p ) { RECT2::SetRect( p->GetRectX(),     p->GetRectY(),     p->GetRectW(),     p->GetRectH()     ); } }
void RECT2::SetRectX(  const RECT2* p ){ if ( p ) { RECT2::SetRect( p->GetRectX(),     RECT2::GetRectY(), RECT2::GetRectW(), RECT2::GetRectH() ); } }
void RECT2::SetRectY(  const RECT2* p ){ if ( p ) { RECT2::SetRect( RECT2::GetRectX(), p->GetRectY(),     RECT2::GetRectW(), RECT2::GetRectH() ); } }
void RECT2::SetRectW(  const RECT2* p ){ if ( p ) { RECT2::SetRect( RECT2::GetRectX(), RECT2::GetRectY(), p->GetRectW(),     RECT2::GetRectH() ); } }
void RECT2::SetRectH(  const RECT2* p ){ if ( p ) { RECT2::SetRect( RECT2::GetRectX(), RECT2::GetRectY(), RECT2::GetRectW(), p->GetRectH()     ); } }
void RECT2::SetRectXY( const RECT2* p ){ if ( p ) { RECT2::SetRect( p->GetRectX(),     p->GetRectY(),     RECT2::GetRectW(), RECT2::GetRectH() ); } }
void RECT2::SetRectWH( const RECT2* p ){ if ( p ) { RECT2::SetRect( RECT2::GetRectX(), RECT2::GetRectY(), p->GetRectW(),     p->GetRectH()     ); } }

//----------------------------------------
// ピボット
//----------------------------------------
void RECT2::SetPivot(   void ){ RECT2::SetPivot(   +RECT2::GetSizeW()/2, +RECT2::GetSizeH()/2 ); }
void RECT2::SetPivotX(  void ){ RECT2::SetPivotX(  +RECT2::GetSizeW()/2 ); }
void RECT2::SetPivotY(  void ){ RECT2::SetPivotY(  +RECT2::GetSizeH()/2 ); }

// End Of File
