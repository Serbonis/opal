//========================================
// OPAL Game Library
//========================================
#include "rectx_dx11.hpp"
#include "rectx_dx11_.hpp"
#include "texture_dx11_.hpp"

//========================================
// 矩形タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx11;

//========================================
// device interface
//========================================
RECTX::RECTX() {}
RECTX::~RECTX(){}

//----------------------------------------
// 情報
//----------------------------------------
void RECTX::DrawPrint( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// 描画
//----------------------------------------
void RECTX::DrawPrim( RECTX_* device, UINT size, UINT offset, const void* cbuff, const TEXTURE* texture, const DRAWB* drawb ){

	const auto	context = DRAWX_DX11::device( 0 );

	context->IASetVertexBuffers( 0, 1, &device->vbuffer, &size, &offset );
	context->IASetInputLayout( device->layout );
	context->IASetIndexBuffer( device->ibuffer, DXGI_FORMAT_R16_UINT, 0 );
	context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP );

	context->VSSetShader( device->vshader, nullptr, 0 );

	DrawPrimTexture( device, texture );

	context->UpdateSubresource( device->cbuffer, 0, nullptr, cbuff, 0, 0 );

	context->VSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->PSSetConstantBuffers( 0, 1, &device->cbuffer );

	DrawPrimBlend( device, drawb );

	context->OMSetDepthStencilState( device->depthstencil, 0x00000000 );

	context->DrawIndexed( 4, 0, 0 );
}

//----------------------------------------
// テクスチャ設定
//----------------------------------------
void RECTX::DrawPrimTexture( RECTX_* device, const TEXTURE* texture ){

	const auto	context = DRAWX_DX11::device( 0 );

	ID3D11ShaderResourceView*	rp[] = { nullptr,	};
	ID3D11SamplerState*			sp[] = { nullptr,	};

	if ( !texture ) {
		context->PSSetShader( device->pshader_, nullptr, 0 );
	} else {
		context->PSSetShader( device->pshader,  nullptr, 0 );

		if ( const auto tx = texture->Texture() ) {
			rp[0] = tx->Resource();
			sp[0] = tx->Sampler();
		}
	}

	context->PSSetShaderResources( 0, std::size( rp ), rp );
	context->PSSetSamplers(        0, std::size( sp ), sp );
}

//----------------------------------------
// ブレンドステート
//----------------------------------------
extern void DeviceBlend( ID3D11BlendState*&, const DRAWB* );

void RECTX::DrawPrimBlend( RECTX_* device, const DRAWB* drawb ){ DeviceBlend( device->blend, drawb );	}

void DeviceBlend( ID3D11BlendState*& devb, const DRAWB* drawb ){

	if ( !drawb || !drawb->GetAlphaBlend() ) {
		return;
	}

	ID3D11BlendState*	blend = nullptr;

	switch ( const auto t = drawb->GetAlphaBlendType() ) {
	default: return;
	case ALPHA_NONE		: [[fallthrough]];
	case ALPHA_TRANS	: [[fallthrough]];
	case ALPHA_ALPHA	: [[fallthrough]];
	case ALPHA_COLOR	: [[fallthrough]];
	case ALPHA_ADD		: [[fallthrough]];
	case ALPHA_SUB		: [[fallthrough]];
	case ALPHA_REV		: DRAWX_DX11::CreateBlendState( blend, t );	break;
	case ALPHA_BLEND 	:
		{
			const auto [co,cs,cd,ao,as,ad] = drawb->GetAlphaBlendDesc();
			const D3D11_BLEND_OP	op[] = {( D3D11_BLEND_OP )co,( D3D11_BLEND_OP )ao};
			const D3D11_BLEND		sr[] = {( D3D11_BLEND )cs,( D3D11_BLEND )as};
			const D3D11_BLEND		dt[] = {( D3D11_BLEND )cd,( D3D11_BLEND )ad};

			DRAWX_DX11::CreateBlendState( blend, sr, dt, op );
		}
		break;
	}

	if ( blend ) {
		if ( DRAWX_DX11::CheckBlendState( blend, nullptr, 0xffffffff ) ) {
			DRAWX_DX11::DestroyBlendState( blend );
		} else {
			const auto		context = DRAWX_DX11::device( 0 );

			DRAWX_DX11::DestroyBlendState( devb );

			context->OMSetBlendState( devb = blend, drawb->GetAlphaBlendFactor( false ), drawb->GetAlphaBlendMask() );
		}
	}
}

// End Of File
