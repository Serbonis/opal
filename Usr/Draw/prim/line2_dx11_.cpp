//========================================
// OPAL Game Library
//========================================
#include "line2_dx11.hpp"
#include "line2_dx11_.hpp"

//========================================
// 矩形タスク(2D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<LINE2_>	device{"LINE2"};
}
IDEVICE_( LINE2_ );

template<> IDEVICE<LINE2_MAP>::S_PTR	LINE2_MAP::devmap = nullptr;
template<> IDEVICE<LINE2_MAP>			LINE2_MAP::device{"LINE2_MAP"};
IDEVICE_( LINE2_MAP );

//----------------------------------------
// constructor & destructor
//----------------------------------------
LINE2_::LINE2_( const void* d ) :
	LINEX_( d ),
	vbuffer( nullptr )
{}
LINE2_::~LINE2_(){}

ID3D11InputLayout*	LINE2_::layout	= nullptr;
ID3D11VertexShader*	LINE2_::vshader	= nullptr;
ID3D11PixelShader*	LINE2_::pshader	= nullptr;
ID3D11Buffer*		LINE2_::cbuffer	= nullptr;

//----------------------------------------
// デバイス初期化
//----------------------------------------
namespace {
#include "xs_line2.fxh"
#include "vs_line2.fxh"
#include "ps_line2.fxh"
#include "vs_line2.cpp"
#include "ps_line2.cpp"

	constexpr VS_ELEMENT_DESC	elms[] = { VS_ELEMENT( VS_ELEMENT_S0, 0, VS_ELEMENT_F0 ), };

	constexpr auto	vs_p = VS_HLSL( line2 );
	constexpr auto	ps_p = PS_HLSL( line2 );
	constexpr auto	vs_s = sizeof( VS_HLSL( line2 ) );
	constexpr auto	ps_s = sizeof( PS_HLSL( line2 ) );
}

int LINE2_::InitDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( DeviceName( NAME( "%p:LINE2_", this ) ), this );
	}

	LINEX_::InitDevice( n );

	using namespace DRAWX_DX11;

	{
		const auto	vn = *reinterpret_cast<const UINT*>( data ) + 1;
		const auto	vp = new FVF_V[vn];

		CreateVertexBuffer( vbuffer, vp, sizeof( FVF_V ) * vn, true );

		delete [] vp;
	}

	REGISTER( cbuffer, []{ CreateConstantBuffer( cbuffer, sizeof( CONST_BUFFER_LINE2 ) ); } );
	REGISTER( layout,  []{ CreateInputLayout(  layout,  vs_p, vs_s, elms, std::size( elms ) ); } );
	REGISTER( vshader, []{ CreateVertexShader( vshader, vs_p, vs_s ); } );
	REGISTER( pshader, []{ CreatePixelShader(  pshader, ps_p, ps_s ); } );

	return 0;
}

//----------------------------------------
// コンテキスト切替
//----------------------------------------
LINEX_* LINE2_::Context( void ){

	LINEX_::vbuffer	= vbuffer;
	LINEX_::layout	= layout;
	LINEX_::vshader	= vshader;
	LINEX_::pshader	= pshader;
	LINEX_::cbuffer	= cbuffer;

	return this;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int LINE2_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( DeviceName() );
	}

	using namespace DRAWX_DX11;

	DestroyVertexBuffer( vbuffer );

	RELEASE( cbuffer );
	RELEASE( layout  );
	RELEASE( vshader );
	RELEASE( pshader );

	LINEX_::FreeDevice( n );

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void LINE2_::DestroyDevice( void ){

	device.DestroyDevice();
}

//----------------------------------------
// 頂点情報
//----------------------------------------
void LINE2_::VertexMap( const void* vp, UINT vn, UINT id ) const {

	const auto	context = DRAWX_DX11::device( 0 );

	D3D11_MAPPED_SUBRESOURCE	rp;

	context->Map( vbuffer, id, D3D11_MAP_WRITE_DISCARD, 0, &rp );
	{
		const auto	vd = ( FVF_V* )rp.pData;
		const auto	vs = ( const VECTOR2* )vp;

		for ( auto i = 0UL; i < vn; i++ ) {
			vd[i].pos.f[0] = vs[i].x;
			vd[i].pos.f[1] = vs[i].y;
			vd[i].pos.f[2] = 0.0f;
			vd[i].pos.f[3] = 1.0f;
		}
	}
	context->Unmap( vbuffer, id );
}

UINT LINE2_::VertexStride( void ) const { return sizeof( FVF_V );	}
UINT LINE2_::VertexOffset( void ) const { return 0;	}

// End Of File
