//========================================
// OPAL Game Library
//========================================
#include "rect2_dx11.hpp"
#include "rect2_dx11_.hpp"

//========================================
// 矩形タスク(2D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT2::RECT2() :
	device( nullptr )
{}
RECT2::~RECT2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void RECT2::Init( const char* p ){

	dx::RECT2::Init( p );

	RECT2::InitDevice( 0 );
}

void RECT2::Free( void ){

	RECT2::FreeDevice( 0 );

	dx::RECT2::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int RECT2::InitDevice( int n ){

	if ( !device ) {
		RECT2_::CreateDevice( &device );
	}

	return 0;
}

int RECT2::FreeDevice( int n ){

	if ( device ) {
		RECT2_::DestroyDevice( &device );
	}

	return 0;
}

//----------------------------------------
// テクスチャーに合わせる
//----------------------------------------
void RECT2::SetSize( void ){

	if ( const auto t = GetTexture() ) {
		RECT2::SetSize( ( float )t->Width(),
						( float )t->Height() );
	}
}

//----------------------------------------
// 描画
//----------------------------------------
namespace {
#include "xs_rect2.fxh"
#include "vs_rect2.fxh"
#include "ps_rect2.fxh"
}

void RECT2::DrawPrim( void ){

	CONST_BUFFER_RECT2	cbuff{};

	{
		const auto	wd = World();
		const auto	tv = TransMatrix( wd )+GetOffset();
		const auto	sz = GetSize();
		const auto	im = hlsl::Scale( wd, DRAWX::GetViewportSize(), sz );
		const auto	iv = hlsl::Trans( tv, DRAWX::VportInv() );
		const auto	mw = hlsl::World( im, VECTOR3{iv,GetLayer()} );
		const auto	vp = hlsl::Pivot( GetPivot(), sz );

		cbuff.World		= mw;
		cbuff.Pivot		= vp;
	}

	{
		const auto	sm = hlsl::Sampler( GetRect() ) *  MatrixScale( GetRevU() ? -1 : +1, GetRevV() ? -1 : +1, 1 );

		cbuff.Sampler	= sm;
	}

	{
		const auto cl = RECT2::Color();

		for ( auto i = 0UL; i < 4; i++ ) {
			cbuff.Diffuse[i].r =  cl[i].R();
			cbuff.Diffuse[i].g =  cl[i].G();
			cbuff.Diffuse[i].b =  cl[i].B();
			cbuff.Diffuse[i].a =  cl[i].A();
		}
	}

	RECTX::DrawPrim( device->Context(), sizeof( FVF_VIT ), 0UL, &cbuff, GetTexture(), GetAlpha( 1 ) ? this : nullptr );
}

// End Of File
