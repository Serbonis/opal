//========================================
// Vertex Shader Sample
//========================================
#include "xs_shape.fxh"
#include "vs_shape.fxh"
#include "ps_shape.fxh"

VS_OUTPUT vs_shape( VS_INPUT input )
{
	VS_OUTPUT	output = ( VS_OUTPUT )0;

	float4	pos = float4( input.pos, 1 ) - Pivot;

	output.pos	= mul( pos, World );
	output.col	= Diffuse;

	return output;
}

// End Of File
