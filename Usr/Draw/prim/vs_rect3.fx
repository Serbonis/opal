//========================================
// Vertex Shader Sample
//========================================
#include "xs_rect3.fxh"
#include "vs_rect3.fxh"
#include "ps_rect3.fxh"

VS_OUTPUT vs_rect3( VS_INPUT input )
{
	VS_OUTPUT	output = ( VS_OUTPUT )0;

	float4	pos = mul( float4( input.pos, 1 ), Axis ) + Pivot;
	float4	tex = mul( float4( input.tex, 0, 1 ), Sampler );

	output.pos	= mul( pos, World );
	output.pos	= mul( output.pos, View );
	output.pos	= mul( output.pos, Proj );
	output.col	= Diffuse[input.idx];
	output.tex	= tex.xy;

	return output;
}

// End Of File
