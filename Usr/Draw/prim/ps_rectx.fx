//========================================
// Pixel Shader(Default)
//========================================
#include "ps_rectx.fxh"

float4 ps_rectx( PS_INPUT input ) : SV_Target
{
	return input.col;
}

// End Of File
