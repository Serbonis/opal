//========================================
// Vertex Shader Sample
//========================================
#include "xs_rect2.fxh"
#include "vs_rect2.fxh"
#include "ps_rect2.fxh"

VS_OUTPUT vs_rect2( VS_INPUT input )
{
	VS_OUTPUT	output = ( VS_OUTPUT )0;

	float4	pos = float4( input.pos, 1 ) - Pivot;
	float4	tex = mul( float4( input.tex, 0, 1 ), Sampler );

	output.pos	= mul( pos, World );
	output.col	= Diffuse[input.idx];
	output.tex	= tex.xy;

	return output;
}

// End Of File
