//========================================
// OPAL Game Library
//========================================
#include "waku2.hpp"

//========================================
// 枠タスク(2D)
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
WAKU2::WAKU2(){}
WAKU2::~WAKU2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void WAKU2::Init( const char* p ){

	LINE2::Init( p );
	LINE2::SetNode( 4 );
	LINE2::SetLoop( true );

	WAKUX::Init();
	WAKUX::SetSize( 100.0f, 100.0f );
}

void WAKU2::Free( void ){

	LINE2::Free();
	WAKUX::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void WAKU2::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "WAKU2" );
	printd( "%-08s ", ID() );
	WAKU2::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void WAKU2::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
	WAKU2 ::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void WAKU2::DrawMain( void ){

	const auto		w = WAKU2::GetSizeW();
	const auto		h = WAKU2::GetSizeH();
	const VECTOR2	v[] = {
		{0,0},
		{w,0},
		{w,h},
		{0,h},
	};

	for  ( auto i = 0UL; i < 4; i++ ) {
		LINE2::SetNode( i, v[i] );
	}

	LINE2::DrawMain();
}

//----------------------------------------
// サイズ
//----------------------------------------
void WAKU2::SetSize( const WAKU2* p ){

	if ( p ) {
		WAKU2::SetSize( p->WAKU2::GetSizeW(),
						p->WAKU2::GetSizeH() );
	}
}

//----------------------------------------
// ピボット
//----------------------------------------
void WAKU2::SetPivot( void ){ WAKU2::SetPivot( WAKU2::GetSizeW()/2, WAKU2::GetSizeH()/2 ); }

// End Of File
