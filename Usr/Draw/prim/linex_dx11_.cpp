//========================================
// OPAL Game Library
//========================================
#include "linex_dx11.hpp"
#include "linex_dx11_.hpp"

//========================================
// 直線タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx11;

//========================================
// device interface
//========================================
LINEX_::LINEX_( const void* d ) :
	data( d )
{}
LINEX_::~LINEX_(){}

ID3D11Buffer*				LINEX_::vbuffer	= nullptr;
ID3D11InputLayout*			LINEX_::layout	= nullptr;
ID3D11VertexShader*			LINEX_::vshader	= nullptr;
ID3D11PixelShader*			LINEX_::pshader	= nullptr;
ID3D11Buffer*				LINEX_::cbuffer	= nullptr;

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int LINEX_::InitDevice( int n ){ return 0;	}
int LINEX_::FreeDevice( int n ){ return 0;	}

// End Of File
