//========================================
// OPAL Game Library
//========================================
#include "rect2_dx11.hpp"
#include "rect2_dx11_.hpp"

//========================================
// 矩形タスク(2D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace {
	IDEVICE<RECT2_>	device{"RECT2"};
}
IDEVICE_( RECT2_ );

void RECT2_::CreateDevice(  SDEVICE<RECT2_>* d ){ if ( d ) { *d = device.CreateDevice();	}	}
void RECT2_::DestroyDevice( SDEVICE<RECT2_>* d ){ if ( d ) { *d = nullptr; }					}

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT2_::RECT2_() {}
RECT2_::~RECT2_(){}

ID3D11Buffer*		RECT2_::vbuffer	= nullptr;
ID3D11InputLayout*	RECT2_::layout	= nullptr;
ID3D11VertexShader*	RECT2_::vshader	= nullptr;
ID3D11PixelShader*	RECT2_::pshader	= nullptr;
ID3D11Buffer*		RECT2_::cbuffer	= nullptr;

//----------------------------------------
// デバイス初期化
//----------------------------------------
namespace {
#include "xs_rect2.fxh"
#include "vs_rect2.fxh"
#include "ps_rect2.fxh"
#include "vs_rect2.cpp"
#include "ps_rect2.cpp"

	constexpr FVF_VIT	vertexlist[] = {
		{ { 0.0f, 1.0f, 0.0f }, 3UL, { 0.0f, 1.0f } },
		{ { 0.0f, 0.0f, 0.0f }, 0UL, { 0.0f, 0.0f } },
		{ { 1.0f, 1.0f, 0.0f }, 2UL, { 1.0f, 1.0f } },
		{ { 1.0f, 0.0f, 0.0f }, 1UL, { 1.0f, 0.0f } },
	};

	constexpr VS_ELEMENT_DESC	element[]= {
		VS_ELEMENT( VS_ELEMENT_S0, 0, VS_ELEMENT_F0 ),
		VS_ELEMENT( VS_ELEMENT_S1, 0, VS_ELEMENT_F1 ),
		VS_ELEMENT( VS_ELEMENT_S2, 0, VS_ELEMENT_F2 ),
	};

	constexpr auto	vs_p = VS_HLSL( rect2 );
	constexpr auto	ps_p = PS_HLSL( rect2 );
	constexpr auto	vs_s = sizeof( VS_HLSL( rect2 ) );
	constexpr auto	ps_s = sizeof( PS_HLSL( rect2 ) );
}

int RECT2_::InitDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device, &device );
	}

	RECTX_::InitDevice( n );

	using namespace DRAWX_DX11;

	REGISTER( vbuffer, []{ CreateVertexBuffer(   vbuffer, vertexlist, sizeof( FVF_VIT ) * std::size( vertexlist ) ); } );
	REGISTER( cbuffer, []{ CreateConstantBuffer( cbuffer, sizeof( CONST_BUFFER_RECT2 ) ); } );

	REGISTER( layout,  []{ CreateInputLayout(  layout,  vs_p, vs_s, element, std::size( element ) ); } );
	REGISTER( vshader, []{ CreateVertexShader( vshader, vs_p, vs_s ); } );
	REGISTER( pshader, []{ CreatePixelShader(  pshader, ps_p, ps_s ); } );

	ShaderReflection( vs_p, vs_s, "VERTEX SHADER" );
	ShaderReflection( ps_p, ps_s, "PIXEL  SHADER" );

	return 0;
}

//----------------------------------------
// コンテキスト切替
//----------------------------------------
RECTX_* RECT2_::Context( void ){

	RECTX_::vbuffer	= vbuffer;
	RECTX_::layout	= layout;
	RECTX_::vshader	= vshader;
	RECTX_::pshader	= pshader;
	RECTX_::cbuffer	= cbuffer;

	return this;
}

//----------------------------------------
// デバイス後始末
//----------------------------------------
int RECT2_::FreeDevice( int n ){

	if ( n == 0 ) {
		DRAWX::DeviceLost( device );
	}

	RELEASE( vbuffer );
	RELEASE( cbuffer );

	RELEASE( layout  );
	RELEASE( vshader );
	RELEASE( pshader );

	RECTX_::FreeDevice( n );

	return 0;
}

//----------------------------------------
// デバイス破棄
//----------------------------------------
void RECT2_::DestroyDevice( void ){ device.DestroyDevice();	}

// End Of File
