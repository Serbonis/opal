//========================================
// OPAL Game Library
//========================================
#include "line2_dx11.hpp"
#include "line2_dx11_.hpp"

//========================================
// 矩形タスク(2D)
//========================================
using namespace opal;
using namespace opal::dx11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
LINE2::LINE2() :
	device( nullptr ),
	data( nullptr )
{}
LINE2::~LINE2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LINE2::Init( const char* p ){

	dx::LINE2::Init( p );

	LINE2::SetNode();
	LINE2::SetLine( 0, 0, 100, 100 );
}

void LINE2::Free( void ){

	LINE2::SetNode( 0 );

	dx::LINE2::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
namespace {
	UINT mapkey( const void* d, UINT c ){

		if ( d ) {
			const auto	n = *reinterpret_cast<const UINT*>( d );

			return  n ? n : c;
		}
		return 0;
	}
}

int LINE2::InitDevice( int n ){

	if ( !device ) {
		if ( n == 0 ) {
			const auto	key = mapkey( data, 2 );

			DRAWX::DeviceLost( DeviceName( NAME( "%p:%s:%08x", this, "(LINE2)", key ) ), this );
			device = LINE2_MAP::CreateDevice( key, data );
		}
	}
	return 0;
}

int LINE2::FreeDevice( int n ){

	if ( device ) {
		if ( n == 0 ) {
			DRAWX::DeviceLost( DeviceName() );
			device = nullptr;
			LINE2_MAP::DestroyDevice( mapkey( data, 2 ) );
		}
	}
	return 0;
}

void LINE2::DeviceData( const void* d ){

	if ( !data ) {
		data = NEWS<UINT>();
	}

	if ( d ) {
		const auto gd = reinterpret_cast<UINT*>( data );

		*gd = *reinterpret_cast<const UINT*>( d );
	}
}

void LINE2::DeviceData( void ){

	if ( data ) {
		DELETES<UINT>( data );
	}
}

//----------------------------------------
// 生成＆破棄
//----------------------------------------
void LINE2::SetNode( void ){

	LINE2::SetNode( 2 );
}

void LINE2::SetNode( UINT n ){

	if ( device ) {
		dx::LINE2::SetNode( 0 );
		LINE2::FreeDevice( 0 );
		LINE2::DeviceData();
	}

	if ( n ) {
		LINE2::DeviceData( &n );
		LINE2::InitDevice( 0 );
		dx::LINE2::SetNode( n );
	}
}

//----------------------------------------
// 描画
//----------------------------------------
#include "xs_com.hpp"

namespace {
#include "xs_line2.fxh"
#include "vs_line2.fxh"
#include "ps_line2.fxh"
}

void LINE2::DrawPrim( void ){

	if ( node ) {
		const auto	ln = ( loop ) ? node+1 : node;

		lv.a[node] = lv.a[0];

		for ( auto i = 0UL; i < ln; i++ ) {
			lv.b[i] = lv.a[i];
		}

		CONST_BUFFER_LINE2	cbuff{};

		CONST_BUFFER_2D( this, cbuff );

		LINEX::DrawPrim( device->Context(), lv.b.data(), ln, &cbuff, bold );

		const auto	wd = World();
		const auto	pv = GetPivot();

		for ( auto i = 0UL; i < ln; i++ ) {
			lv.b[i] = wd * ( lv.a[i] - pv );
		}
	}
}

// End Of File
