//========================================
// OPAL Game Library
//========================================
#include "rect3_dx.hpp"

//========================================
// 矩形タスク(3D)
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
RECT3::RECT3() :
	axis( AXIS_MZ )
{}
RECT3::~RECT3(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void RECT3::Init( const char* p ) {

	DRAWT3::Init( p );
	RECTX::Init(  p );

	MATERIAL::SetDiffuse(  0, 0, 0 );
	MATERIAL::SetAmbient(  0, 0, 0 );
	MATERIAL::SetSpecular( 0, 0, 0 );
	MATERIAL::SetEmissive( 0, 0, 0 );
	MATERIAL::SetPower( 1.0f );

	axis	= AXIS_MZ;

	SIZE2::SetSize( 1.0f, 1.0f );
}

void RECT3::Free( void ) {

	RECTX::Free();
	DRAWT3::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void RECT3::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "RECT3" );
	printd( "%-08s ", ID() );
	RECT3::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void RECT3::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT3::DrawInfo( m );
	RECTX ::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void RECT3::DrawMain( void ){ DrawPrim(); }

//----------------------------------------
// サイズ
//----------------------------------------
void RECT3::SetSize(  const RECT3* p ){ if ( p ) { RECT3::SetSize( p->GetSizeW(),     p->GetSizeH()     ); } }
void RECT3::SetSizeW( const RECT3* p ){ if ( p ) { RECT3::SetSize( p->GetSizeW(),     RECT3::GetSizeH() ); } }
void RECT3::SetSizeH( const RECT3* p ){ if ( p ) { RECT3::SetSize( RECT3::GetSizeW(), p->GetSizeH()     ); } }

//----------------------------------------
// テクスチャ
//----------------------------------------
void RECT3::SetRect(   const RECT3* p ){ if ( p ) { RECT3::SetRect( p->GetRectX(),     p->GetRectY(),     p->GetRectW(),     p->GetRectH()     ); } }
void RECT3::SetRectX(  const RECT3* p ){ if ( p ) { RECT3::SetRect( p->GetRectX(),     RECT3::GetRectY(), RECT3::GetRectW(), RECT3::GetRectH() ); } }
void RECT3::SetRectY(  const RECT3* p ){ if ( p ) { RECT3::SetRect( RECT3::GetRectX(), p->GetRectY(),     RECT3::GetRectW(), RECT3::GetRectH() ); } }
void RECT3::SetRectW(  const RECT3* p ){ if ( p ) { RECT3::SetRect( RECT3::GetRectX(), RECT3::GetRectY(), p->GetRectW(),     RECT3::GetRectH() ); } }
void RECT3::SetRectH(  const RECT3* p ){ if ( p ) { RECT3::SetRect( RECT3::GetRectX(), RECT3::GetRectY(), RECT3::GetRectW(), p->GetRectH()     ); }	}
void RECT3::SetRectXY( const RECT3* p ){ if ( p ) { RECT3::SetRect( p->GetRectX(),     p->GetRectY(),     RECT3::GetRectW(), RECT3::GetRectH() ); }	}
void RECT3::SetRectWH( const RECT3* p ){ if ( p ) { RECT3::SetRect( RECT3::GetRectX(), RECT3::GetRectY(), p->GetRectW(),     p->GetRectH()     ); } }

//----------------------------------------
// 表示軸
//----------------------------------------
void RECT3::SetAxis( UINT v ) { axis = v; }
UINT RECT3::GetAxis( void ) const { return axis; }

// End Of File
