//========================================
// OPAL Game Library
//========================================
#include "linex_dx11.hpp"
#include "linex_dx11_.hpp"

//========================================
// 直線タスク(共通)
//========================================
using namespace opal;
using namespace opal::dx11;

//========================================
// device interface
//========================================
LINEX::LINEX() :
	topology{D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP}
{}
LINEX::~LINEX(){}

//----------------------------------------
// 情報
//----------------------------------------
void LINEX::DrawPrint( void ) const {
#ifdef OPAL_DEBUG
#endif
}

//----------------------------------------
// 描画
//----------------------------------------
void LINEX::DrawPrim( LINEX_* device, const VECTOR3* vp, UINT vn, const void* cbuff, UINT b ) const {

	const auto	context = DRAWX_DX11::device( 0 );

	device->VertexMap( vp, vn );

	const auto	st = device->VertexStride();
	const auto	of = device->VertexOffset();

	context->IASetVertexBuffers( 0, 1, &device->vbuffer, &st, &of );
	context->IASetInputLayout( device->layout );
	context->IASetPrimitiveTopology( topology );

	context->VSSetShader( device->vshader, nullptr, 0 );
	context->PSSetShader( device->pshader, nullptr, 0 );

	context->VSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->PSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->UpdateSubresource( device->cbuffer, 0, nullptr, cbuff, 0, 0 );

	context->Draw( vn, 0 );
}

void LINEX::DrawPrim( LINEX_* device, const VECTOR2* vp, UINT vn, const void* cbuff, UINT b ) const {

	const auto	context = DRAWX_DX11::device( 0 );

	device->VertexMap( vp, vn );

	const auto	st = device->VertexStride();
	const auto	of = device->VertexOffset();

	context->IASetVertexBuffers( 0, 1, &device->vbuffer, &st, &of );
	context->IASetInputLayout( device->layout );
	context->IASetPrimitiveTopology( topology );

	context->VSSetShader( device->vshader, nullptr, 0 );
	context->PSSetShader( device->pshader, nullptr, 0 );

	context->VSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->PSSetConstantBuffers( 0, 1, &device->cbuffer );
	context->UpdateSubresource( device->cbuffer, 0, nullptr, cbuff, 0, 0 );

	context->Draw( vn, 0 );
}

// End Of File
