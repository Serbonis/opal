//========================================
// OPAL Game Library
//========================================
#include "maru2.hpp"

//========================================
// 丸タスク(2D)
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
MARU2::MARU2(){}
MARU2::~MARU2(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void MARU2::Init( const char* p ){

	LINE2::Init( p );
	LINE2::SetLoop( true );

	MARUX::Init();
	MARUX::SetSize( 100.0f );

	MARU2::SetRegular();
}

void MARU2::Free( void ){

	LINE2::Free();
	MARUX::Free();
}

//----------------------------------------
// 頂点数
//----------------------------------------
void MARU2::SetRegular( void ){

	MARUX::SetRegular();
	LINE2::SetNode( MARUX::GetRegular() );
}

void MARU2::SetRegular( UINT n ){

	MARUX::SetRegular( n );
	LINE2::SetNode( MARUX::GetRegular() );
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void MARU2::DrawInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "MARU2" );
	printd( "%-08s ", ID() );
	MARU2::DrawInfo( DRAWX::Debug() );
	printd( "\n" );
#endif
}

void MARU2::DrawInfo( int m ) const {
#ifdef OPAL_DEBUG
	DRAWT ::DrawInfo( m );
	DRAWT2::DrawInfo( m );
	MARUX ::DrawInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void MARU2::DrawMain( void ){

	const auto	r = MARU2::GetSizeW();
	const auto	n = regular;
	const auto	d = 360/n;

	for ( auto i = 0UL; i < n; i++ ) {
		const auto	rd = RADIAN( i*d );
		const auto	px = std::sin( rd ) * r;
		const auto	py = std::cos( rd ) * r;

		LINE2::SetNode( i, px, py );
	}

	LINE2::DrawMain();
}

//----------------------------------------
// サイズ
//----------------------------------------
void MARU2::SetSize( const MARU2* p ){

	if ( p ) {
		MARU2::SetSize( p->MARU2::GetSize() );
	}
}

// End Of File
