//========================================
// OPAL Game Library
//========================================
#include "Draw/light.hpp"

//========================================
// ライトタスク
//========================================
using namespace opal;

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void LIGHT::Init( const char* pName ){

	DRAWT::Init( pName );
	LIGHTX::Init();
}

void LIGHT::Free( void ){

	LIGHTX::Free();
	DRAWT::Free();
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void LIGHT::DrawMain( void ){

	if ( ( active = GetDisp( 1 ) ) ) {
		pos = WorldTrans();
		dir = Fd();

		DRAWX::DrawLight( pos, dir, light );
	}
}

// End Of File
