//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/polygon_dx.hpp"

//========================================
// ポリゴンリソース
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
POLYGON::POLYGON() {}
POLYGON::~POLYGON(){}

//----------------------------------------
// 初期化
//----------------------------------------
void POLYGON::Init( const void* b, size_t s, const char* p, const void* d ){

	file.Info( FILEX::Path(), p, b, s );
}

//----------------------------------------
// 後始末
//----------------------------------------
void POLYGON::Free( void ){

	file.Info();
}

// End Of File
