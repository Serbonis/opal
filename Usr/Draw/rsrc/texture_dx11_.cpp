//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/texture_dx11.hpp"
#include "Draw/rsrc/texture_dx11_.hpp"
#include "Draw/rsrc/texture_tga.hpp"

#if _MSC_VER
struct ID3D11VideoDevice {};
struct ID3D11VideoContext {};
struct ID3D11VideoDecoder {};
struct ID3D11CryptoSession {};
struct ID3D11VideoProcessor {};
struct ID3D11VideoProcessorEnumerator {};

using D3D11_VIDEO_DECODER_BUFFER_TYPE = int;
using D3D11_VIDEO_DECODER_DESC = int;
using D3D11_VIDEO_DECODER_CONFIG = int;
#endif

#include "WICTextureLoader.h"

//========================================
// テクスチャーリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
IDEVICE_( TEXTURE_ );

//----------------------------------------
// constructor & destructor
//----------------------------------------
TEXTURE_::TEXTURE_( const CREATE_TEXTURE* d ) :
	data( d ),
	texture(  nullptr ),
	desc{},
	resource( nullptr ),
	sampler(  nullptr )
{}
TEXTURE_::~TEXTURE_(){}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int TEXTURE_::InitDevice( int n ){

	if ( n == 0 ) {
		using namespace DRAWX_DX11;

		const auto	td = reinterpret_cast<const CREATE_TEXTURE*>( data );

		if ( td->adrs && td->size ) {
			if ( FILEN::Kind( td->name, "tga" ) ) {
				TEXTURE_BUFFER	buffer;

				if ( !TGA::LoadImage( td->adrs, td->size, buffer ) ) {
					return 0;
				}

				REGISTER( texture,  [&]{ CreateTexture2D( texture, buffer.tw, buffer.th ); } );
				REGISTER( resource, [&]{ CreateShaderResourceView( resource, texture ); } );

				const auto	ss = D3D11CalcSubresource( buffer.ms,buffer.as,buffer.ml );
				const auto	bx = D3D11_BOX{0,0,0,buffer.tw,buffer.th,1};

				device( 0 )->UpdateSubresource( texture, ss, &bx, buffer(), buffer.rp * buffer.tw, buffer.dp );
			} else {
				DirectX::CreateWICTextureFromMemory( device(),
													 ( const uint8_t* )td->adrs,
													 td->size,
													 reinterpret_cast<ID3D11Resource**>( &texture ),
													 &resource,
													 0 );
			}
			texture->GetDesc( &desc );
		}

		CreateSamplerState( sampler );
	}

	return 0;
}

int TEXTURE_::FreeDevice( int n ){

	if ( n == 0 ) {
		using namespace DRAWX_DX11;

		DestroySamplerState( sampler );

		RELEASE( resource );
		RELEASE( texture  );
	}

	return 0;
}

//----------------------------------------
// テクスチャーサイズ
//----------------------------------------
UINT TEXTURE_::Width(  void ) const { return desc.Width;	}
UINT TEXTURE_::Height( void ) const { return desc.Height;	}

//----------------------------------------
// 取得
//----------------------------------------
ID3D11Texture2D*&          TEXTURE_::Texture(  void ){ return texture;	}
ID3D11ShaderResourceView*& TEXTURE_::Resource( void ){ return resource;	}
ID3D11SamplerState*&       TEXTURE_::Sampler(  void ){ return sampler;	}

// End Of File
