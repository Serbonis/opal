//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/polygon_dx11.hpp"
#include "Draw/rsrc/polygon_dx11_.hpp"

//========================================
// ポリゴンリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
POLYGON::POLYGON() :
	device( nullptr ),
	data( nullptr )
{}
POLYGON::~POLYGON(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void POLYGON::Init( const void* b, size_t s, const char* p, const void* d ){

	if ( !b || !s ) {
		RSRC::Init( p, d );
		return;
	}

	if ( !file.Name() ) {
		dx::POLYGON::Init( b, s, p, d );

		if ( file.Adrs() && file.Size() ) {
			data = NEWS<CREATE_POLYGON>( *static_cast<const CREATE_POLYGON*>( b ) );

			POLYGON::InitDevice( 0 );
			DRAWX::DeviceLost( DeviceName( NAME( "%p:POLYGON(%s)", this, p ) ), this );
		}
	}
}

void POLYGON::Free( void ){

	if ( file.Name() ) {
		if ( file.Adrs() && file.Size() ) {
			DRAWX::DeviceLost( DeviceName() );
			POLYGON::FreeDevice( 0 );

			DELETES( reinterpret_cast<CREATE_POLYGON*>( data ) );
		}

		dx::POLYGON::Free();
	}
}

void POLYGON::Init( const char* p,        const void* b ){ Init( b, sizeof( CREATE_POLYGON ), p,         nullptr );	}
void POLYGON::Init( const std::string& p, const void* b ){ Init( b, sizeof( CREATE_POLYGON ), p.c_str(), nullptr );	}
void POLYGON::Init( std::nullptr_t,       const void* b ){ Init( b, sizeof( CREATE_POLYGON ), nullptr,   nullptr );	}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int POLYGON::InitDevice( int n ){

	if ( !device ) {
		NEWS( device );
		device->CreateDevice( reinterpret_cast<CREATE_POLYGON*>( data ) );
	}

	return 0;
}

int POLYGON::FreeDevice( int n ){

	if ( device ) {
		device->DestroyDevice();
		DELETES( device );
	}

	return 0;
}

//----------------------------------------
// リソース
//----------------------------------------
POLYGON_* POLYGON::Polygon(  void ) const { return device ? (*device)() : nullptr; }

namespace opal::dx11 {
#include "polygon_dx.inl"
}

// End Of File
