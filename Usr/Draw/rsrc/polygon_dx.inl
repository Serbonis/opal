//========================================
// �ԍ������^
//========================================
POLYGONX::POLYGONX() {}
POLYGONX::~POLYGONX(){}

UINT POLYGONX::Polygons( void ) const { return Resource();	}

void POLYGONX::NowPolygon( UINT n ){ NowResource( n );				}
UINT POLYGONX::NowPolygon( void   ) const { return NowResource();	}

void POLYGONX::SetPolygon( const POLYGON*                  r, UINT n ){ SetResource( r,       n ); }
void POLYGONX::SetPolygon( const POLYGON*                  r         ){ SetResource( r          ); }
void POLYGONX::SetPolygon( const POLYGON&                  r, UINT n ){ SetResource( r,       n ); }
void POLYGONX::SetPolygon( const POLYGON&                  r         ){ SetResource( r          ); }
void POLYGONX::SetPolygon( const std::shared_ptr<POLYGON>& r, UINT n ){ SetResource( r.get(), n ); }
void POLYGONX::SetPolygon( const std::shared_ptr<POLYGON>& r         ){ SetResource( r.get()    ); }

const POLYGON* POLYGONX::GetPolygon( UINT n ) const { return GetResource( n );	}
const POLYGON* POLYGONX::GetPolygon( void   ) const { return GetResource(   );	}

POLYGON_* POLYGONX::Polygon( UINT n ) const { if ( const auto t = GetResource( n ) ) { return t->Polygon(); } return nullptr;	}
POLYGON_* POLYGONX::Polygon( void   ) const { if ( const auto t = GetResource(   ) ) { return t->Polygon(); } return nullptr;	}

//========================================
// ���O�����^
//========================================
POLYGONH::POLYGONH() {}
POLYGONH::~POLYGONH(){}

UINT POLYGONH::Polygons( void ) const { return Resource();	}

void POLYGONH::NowPolygon( const char* p        ){ NowResource( p         );	}
void POLYGONH::NowPolygon( const std::string& p ){ NowResource( p.c_str() );	}
void POLYGONH::NowPolygon( std::nullptr_t       ){ NowResource( nullptr   );	}
const char* POLYGONH::NowPolygon( void   ) const { return NowResource(    );	}

void POLYGONH::SetPolygon( const POLYGON*                  r, const char* p        ){ SetResource( r,       p         );	}
void POLYGONH::SetPolygon( const POLYGON*                  r, const std::string& p ){ SetResource( r,       p.c_str() );	}
void POLYGONH::SetPolygon( const POLYGON*                  r, std::nullptr_t       ){ SetResource( r,       nullptr   );	}
void POLYGONH::SetPolygon( const POLYGON*                  r                       ){ SetResource( r                  );	}
void POLYGONH::SetPolygon( const POLYGON&                  r, const char* p        ){ SetResource( r,       p         );	}
void POLYGONH::SetPolygon( const POLYGON&                  r, const std::string& p ){ SetResource( r,       p.c_str() );	}
void POLYGONH::SetPolygon( const POLYGON&                  r, std::nullptr_t       ){ SetResource( r,       nullptr   );	}
void POLYGONH::SetPolygon( const POLYGON&                  r                       ){ SetResource( r                  );	}
void POLYGONH::SetPolygon( const std::shared_ptr<POLYGON>& r, const char* p        ){ SetResource( r.get(), p         );	}
void POLYGONH::SetPolygon( const std::shared_ptr<POLYGON>& r, const std::string& p ){ SetResource( r.get(), p.c_str() );	}
void POLYGONH::SetPolygon( const std::shared_ptr<POLYGON>& r, std::nullptr_t       ){ SetResource( r.get(), nullptr   );	}
void POLYGONH::SetPolygon( const std::shared_ptr<POLYGON>& r                       ){ SetResource( r.get()            );	}

const POLYGON* POLYGONH::GetPolygon( const char* p        ) const { return GetResource( p         ); }
const POLYGON* POLYGONH::GetPolygon( const std::string& p ) const { return GetResource( p.c_str() ); }
const POLYGON* POLYGONH::GetPolygon( std::nullptr_t       ) const { return GetResource( nullptr   ); }
const POLYGON* POLYGONH::GetPolygon( void                 ) const { return GetResource(           ); }

POLYGON_* POLYGONH::Polygon( const char* p        ) const { if ( const auto t = GetResource( p         ) ) { return t->Polygon(); } return nullptr; }
POLYGON_* POLYGONH::Polygon( const std::string& p ) const { if ( const auto t = GetResource( p.c_str() ) ) { return t->Polygon(); } return nullptr; }
POLYGON_* POLYGONH::Polygon( std::nullptr_t       ) const { if ( const auto t = GetResource( nullptr   ) ) { return t->Polygon(); } return nullptr; }
POLYGON_* POLYGONH::Polygon( void                 ) const { if ( const auto t = GetResource(           ) ) { return t->Polygon(); } return nullptr; }

// End Of File
