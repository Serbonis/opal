//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/texture_buffer.hpp"

//========================================
// テクスチャー転送バッファ
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
TEXTURE_BUFFER::TEXTURE_BUFFER() :
	bp{nullptr},
	ms(0),as(0),ml(0),
	tw(0),th(0),
	rp(0),dp(0)
{}
TEXTURE_BUFFER::~TEXTURE_BUFFER()
{
	DELETEA( bp );
}

//----------------------------------------
// バッファ作成
//----------------------------------------
BYTE* TEXTURE_BUFFER::operator()( UINT w, UINT h, UINT r, UINT d ){

	DELETEA( bp );

	tw = w;
	th = h;
	rp = r;
	dp = d;

	return NEWA( bp, tw * th * rp * dp );
}

BYTE* TEXTURE_BUFFER::operator()( void ){ return bp;	}

// End OF File
