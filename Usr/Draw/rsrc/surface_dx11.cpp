//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/surface_dx11.hpp"
#include "Draw/rsrc/surface_dx11_.hpp"

//========================================
// サーフェイスリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SURFACE::SURFACE( UINT w, UINT h ) :
	device( nullptr ),
	data(   nullptr )
{}
SURFACE::SURFACE() :
	device( nullptr ),
	data(   nullptr )
{}
SURFACE::~SURFACE(){}

//----------------------------------------
// 初期化
//----------------------------------------
void SURFACE::Init( const void* b, size_t s, const char* p, const void* d ){

	dx::SURFACE::Init( b, s, p, d );

	file.Info( FILEX::Path(), p, b, s );

	SURFACE::InitDevice( 0 );
	DRAWX::DeviceLost( DeviceName( NAME( "%p:SURFACE(%s)", this, p ) ), this );
}

//----------------------------------------
// 後始末
//----------------------------------------
void SURFACE::Free( void ){

	DRAWX::DeviceLost( DeviceName() );
	SURFACE::FreeDevice( 0 );

	file.Info();

	dx::SURFACE::Free();
}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int SURFACE::InitDevice( int n ){

	if ( !device ) {
		NEWS( device );
		device->CreateDevice( reinterpret_cast<SURFACE_::CREATE_SURFACE*>( data ) );
	}
	return 0;
}

int SURFACE::FreeDevice( int n ){

	if ( device ) {
		device->DestroyDevice();
		DELETES( device );
	}
	return 0;
}

void SURFACE::DeviceData( const void* d ){

	if ( !data ) {
		data = NEWS<SURFACE_::CREATE_SURFACE>();
	}

	if ( d ) {
		const auto	sp = reinterpret_cast<const SURFACE_::CREATE_SURFACE*>( d    );
		const auto	dp = reinterpret_cast<      SURFACE_::CREATE_SURFACE*>( data );

		*dp = *sp;
	}
}

void SURFACE::DeviceData( void ){

	if ( data ) {
		DELETES<SURFACE_::CREATE_SURFACE>( data );
	}
}

//----------------------------------------
// テクスチャーサイズ
//----------------------------------------
UINT SURFACE::Width(  void ) const { return device ? (*device)()->Width()  : 0; }
UINT SURFACE::Height( void ) const { return device ? (*device)()->Height() : 0;	}

//----------------------------------------
// リソース
//----------------------------------------
TEXTURE_* SURFACE::Texture( void ) const { return device ? (*device)() : nullptr; }

//----------------------------------------
// 生成と破棄
//----------------------------------------
void SURFACE::Create( const char* p, UINT w, UINT h, DXGI_FORMAT f, const char* t ){

	using TYPE = SURFACE_::CREATE_SURFACE::TYPE;

	static const std::unordered_map<std::string,TYPE>	type = {
		{ "RENDER",TYPE::RENDER },
		{ "CANVAS",TYPE::CANVAS },
		{ "IDXGDI",TYPE::IDXGDI },
		{ "SHADER",TYPE::SHADER },
	};

	const SURFACE_::CREATE_SURFACE	dd( f,
										w ? w : 1024,
										h ? h : 1024,
										type.at( type.count( t ) ? t : "RENDER" ) );

	SURFACE::DeviceData( &dd );
	SURFACE::Init( nullptr, 0, p, nullptr );
}

void SURFACE::Create( const char* p, DXGI_FORMAT f, const char* t ){

	SURFACE::Create( p,  0, 0, f, t );
}

void SURFACE::Create( DXGI_FORMAT f, const char* t ){

	SURFACE::Create( nullptr, 0, 0, f, t );
}

void SURFACE::Destroy( void ){

	SURFACE::Free();
	SURFACE::DeviceData();
}

//----------------------------------------
// レンダリング
//----------------------------------------
void SURFACE::Begin( DWORD c, float z, DWORD s ){ (*device)()->Begin( c, z, s );	}
void SURFACE::Begin( void ){ (*device)()->Begin();	}
void SURFACE::End(   void ){ (*device)()->End();	}

void CANVAS::Begin( DWORD c, float, DWORD ){

	const auto	context = (*device)();

	context->Begin();
	context->Clear( 0, 0, Width(), Height(), c );
}

//----------------------------------------
// テクスチャークリア
//----------------------------------------
void SURFACE::Clear( UINT x, UINT y, UINT w, UINT h, DWORD c ){

	const auto	tw = Width();
	const auto	th = Height();
	const auto	sw = w ? w : tw;
	const auto	sh = w ? h : th;
	const auto	rw = ( x + sw < tw ) ? x + sw : tw;
	const auto	rh = ( y + sh < th ) ? y + sh : th;

	(*device)()->Clear( x, y, rw, rh, c );
}

void SURFACE::Clear( UINT w, UINT h, DWORD c ){ (*device)()->Clear( 0, 0, w, h, c );	}
void SURFACE::Clear( DWORD c ){ (*device)()->Clear( 0, 0, Width(), Height(), c ); 		}

//----------------------------------------
// Lock/UnLock
//----------------------------------------
bool SURFACE::Lock( LOCKED& r, UINT n ) const { return (*device)()->Lock( r, n );	}
auto SURFACE::Lock( UINT n ) const->std::shared_ptr<LOCKED>{

	LOCKED	r;

	if ( (*device)()->Lock( r, n ) ) {
		return std::make_shared<LOCKED>( r );
	}

	return nullptr;
}
void SURFACE::UnLock( UINT n ) const { (*device)()->UnLock( n );					}

//========================================
// SURFACE LOCK
//========================================
#define RC_DATA		rc.pData
#define RC_PITCH	rc.RowPitch
#define RC_DEPTH	rc.DepthPitch

SURFACE_LOCK::SURFACE_LOCK( const SURFACE* s, UINT n ) :
	sp{s},
	id{n},
	rc{}
{
	if ( sp ) {
		sp->Lock( rc, id );
	}
}
SURFACE_LOCK::SURFACE_LOCK( const SURFACE& s, UINT n ) : SURFACE_LOCK( &s, n ) {}
SURFACE_LOCK::SURFACE_LOCK( const std::shared_ptr<SURFACE> s, UINT n ) : SURFACE_LOCK( s.get(), n ) {}

SURFACE_LOCK::SURFACE_LOCK() :
	sp{nullptr},
	id{0},
	rc{}
{}
SURFACE_LOCK::~SURFACE_LOCK(){ 	if ( sp ) { sp->UnLock( id ); }	}

bool SURFACE_LOCK::Lock( const SURFACE& s, UINT n ){ return Lock( &s, n );	}
bool SURFACE_LOCK::Lock( const SURFACE* s, UINT n ){

	if ( s && s != sp ) {
		UnLock();
		sp = s;
		id = n;
		s->Lock( rc, id );
		return true;
	}
	return false;
}
void SURFACE_LOCK::UnLock( void ){

	if ( sp ) {
		sp->UnLock( id );
		sp = nullptr;
		id = 0;
		rc = {};
	}
}

SURFACE_LOCK::operator bool( void ) const { return RC_DATA ? true : false;	}

void* SURFACE_LOCK::Data(  void ) const { return RC_DATA;	};
UINT  SURFACE_LOCK::Pitch( void ) const { return RC_PITCH;	};
UINT  SURFACE_LOCK::Depth( void ) const { return RC_DEPTH;	};

UINT SURFACE_LOCK::Width(  void ) const { return sp->Width();	};
UINT SURFACE_LOCK::Height( void ) const { return sp->Height();	};

DWORD* SURFACE_LOCK::operator []( UINT h ) const {

	if ( const auto	pb = reinterpret_cast<BYTE*>( RC_DATA ) ) {
		return reinterpret_cast<DWORD*>( &pb[RC_PITCH*h] );
	}
	return nullptr;
}

DWORD* SURFACE_LOCK::operator ()( UINT h, UINT w ) const {

	if ( const auto pd = (*this)[h] ) {
		return &pd[w];
	}
	return nullptr;
}

void SURFACE_LOCK::SetPixel( UINT x, UINT y, UINT w, UINT h, DWORD c ){

	if ( const auto	pb = reinterpret_cast<BYTE*>( RC_DATA ) ) {
		const auto	tw = sp->Width();
		const auto	th = sp->Height();

		if ( x < tw && y < th ) {
			const auto	sw = ( x + w < tw ) ? x+w : tw;
			const auto	sh = ( y + h < th ) ? y+h : th;

			for ( auto i = y; i < sh; i++ ) {
				const auto	pd = reinterpret_cast<DWORD*>( &pb[RC_PITCH*i] );
				for ( auto j = x; j < sw; j++ ) {
					pd[j] = c;
				}
			}
		}
	}
}

void SURFACE_LOCK::SetPixel( UINT x, UINT y, UINT w, UINT h, const DWORD* c ){

	if ( const auto	pb = reinterpret_cast<BYTE*>( RC_DATA ) ) {
		const auto	tw = sp->Width();
		const auto	th = sp->Height();

		if ( x < tw && y < th ) {
			const auto	sw = ( x + w < tw ) ? x+w : tw;
			const auto	sh = ( y + h < th ) ? y+h : th;

			for ( auto i = y; i < sh; i++ ) {
				const auto	pd = reinterpret_cast<DWORD*>( &pb[RC_PITCH*i] );
				const auto	pc = &c[w*i];
				for ( auto j = x; j < sw; j++ ) {
					pd[j] = pc[j];
				}
			}
		}
	}
}

void SURFACE_LOCK::SetPixel( UINT x, UINT y, DWORD c ){

	if ( const auto	pb = reinterpret_cast<BYTE*>( RC_DATA ) ) {
		if ( x < sp->Width() && y < sp->Height() ) {
			const auto	pd = reinterpret_cast<DWORD*>( &pb[RC_PITCH*y] );

			pd[x] = c;
		}
	}
}

auto SURFACE_LOCK::GetPixel( UINT x, UINT y ) const->DWORD{

	if ( const auto	pb = reinterpret_cast<BYTE*>( RC_DATA ) ) {
		if ( x < sp->Width() && y < sp->Height() ) {
			const auto	pd = reinterpret_cast<DWORD*>( &pb[RC_PITCH*y] );

			return pd[x];
		}
	}
	return 0;
}

#undef RC_DATA
#undef RC_PITCH
#undef RC_DEPTH

//========================================
// RENDER
//========================================
void RENDER::Init( const std::string_view p, UINT w, UINT h ){

	SURFACE::Create( p.data(), w, h, DXGI_FORMAT_UNKNOWN, "RENDER" );
}
void RENDER::Init( const std::string_view p, UINT s ){ RENDER::Init( p,  s, s );	}
void RENDER::Init( const std::string_view p         ){ RENDER::Init( p,  0, 0 );	}
void RENDER::Init( UINT w, UINT h                   ){ RENDER::Init( {}, w, h );	}
void RENDER::Init( UINT s                           ){ RENDER::Init( {}, s, s );	}

void RENDER::Free( void ){ SURFACE::Destroy(); }

//========================================
// CANVAS
//========================================
void CANVAS::Init( const std::string_view p, UINT w, UINT h ){

	SURFACE::Create( p.data(), w, h, DXGI_FORMAT_UNKNOWN, "CANVAS" );
}
void CANVAS::Init( const std::string_view p, UINT s ){ CANVAS::Init( p,  s, s );	}
void CANVAS::Init( const std::string_view p         ){ CANVAS::Init( p,  0, 0 );	}
void CANVAS::Init( UINT w, UINT h                   ){ CANVAS::Init( {}, w, h );	}
void CANVAS::Init( UINT s                           ){ CANVAS::Init( {}, s, s );	}

void CANVAS::Free( void ){ SURFACE::Destroy(); }

//========================================
// IDXGDI
//========================================
void IDXGDI::Init( const std::string_view p, UINT w, UINT h ){

	SURFACE::Create( p.data(), w, h, DXGI_FORMAT_UNKNOWN, "IDXGDI" );
}
void IDXGDI::Init( const std::string_view p, UINT s ){ IDXGDI::Init( p,  s, s );	}
void IDXGDI::Init( const std::string_view p         ){ IDXGDI::Init( p,  0, 0 );	}
void IDXGDI::Init( UINT w, UINT h                   ){ IDXGDI::Init( {}, w, h );	}
void IDXGDI::Init( UINT s                           ){ IDXGDI::Init( {}, s, s );	}

void IDXGDI::Free( void ){ SURFACE::Destroy(); }

//========================================
// SHADER
//========================================
void SHADER::Init( const std::string_view p, UINT w, UINT h ){

	SURFACE::Create( p.data(), w, h, DXGI_FORMAT_UNKNOWN, "SHADER" );
}
void SHADER::Init( const std::string_view p, UINT s ){ SHADER::Init( p,  s, s );	}
void SHADER::Init( const std::string_view p         ){ SHADER::Init( p,  0, 0 );	}
void SHADER::Init( UINT w, UINT h                   ){ SHADER::Init( {}, w, h );	}
void SHADER::Init( UINT s                           ){ SHADER::Init( {}, s, s );	}

void SHADER::Free( void ){ SURFACE::Destroy(); }

// End Of File
