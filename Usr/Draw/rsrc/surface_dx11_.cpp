//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/surface_dx11.hpp"
#include "Draw/rsrc/surface_dx11_.hpp"

//========================================
// サーフェイスリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
IDEVICE_( SURFACE_ );

//----------------------------------------
// constructor & destructor
//----------------------------------------
SURFACE_::SURFACE_( const CREATE_SURFACE* d ) :
	data( d ),
	target{ nullptr,nullptr},
	stencil{nullptr,nullptr},
	buffer( nullptr )
{}
SURFACE_::~SURFACE_(){}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int SURFACE_::InitDevice( int n ){

	CreateSurface2D( texture, *data  );
	CreateBuffer2D(  buffer, texture );
	CreateShaderResourceView( resource,   texture );
	CreateRenderTargetView(   target[0],  texture );
	CreateDepthStencilView(   stencil[0], buffer );
	CreateSamplerState( sampler );

	texture->GetDesc( &desc );

	return 0;
}

int SURFACE_::FreeDevice( int n ){

	DestroySamplerState( sampler );
	DestroyDepthStencilView(  stencil[0] );
	DestroyRenderTargetView(  target[0]  );
	DestroyShaderResourceView( resource );
	DestroyTexture2D( buffer  );
	DestroyTexture2D( texture );

	return 0;
}

//----------------------------------------
// レンダリング
//----------------------------------------
void SURFACE_::Begin( COLOR c, float z, DWORD s ){

	const auto	context	= DRAWX_DX11::device( 0 );

	const auto&	tf = target[0];
	const auto&	sf = stencil[0];
	auto&		tb = target[1];
	auto&		sb = stencil[1];

	context->OMGetRenderTargets( 1, &tb, &sb );
	ResetResourceView();
	context->OMSetRenderTargets( 1, &tf,  sf );

	{
		const float	color[] = { c.R(), c.G(), c.B(), c.A(), };

		context->ClearRenderTargetView( tf, color );
		context->ClearDepthStencilView( sf, D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, z, s );
	}
}

void SURFACE_::Begin( void ){

	const auto	context	= DRAWX_DX11::device( 0 );

	const auto&	tf = target[0];
	const auto&	sf = stencil[0];
	auto&		tb = target[1];
	auto&		sb = stencil[1];

	context->OMGetRenderTargets( 1, &tb, &sb );
	ResetResourceView();
	context->OMSetRenderTargets( 1, &tf,  sf );
}

void SURFACE_::End( void ){

	const auto	context	= DRAWX_DX11::device( 0 );
	const auto&	tb = target[1];
	const auto&	sb = stencil[1];

	ResetResourceView();
	context->OMSetRenderTargets( 1, &tb, sb );
}

//----------------------------------------
// テクスチャークリア
//----------------------------------------
void SURFACE_::Clear( UINT x, UINT y, UINT w, UINT h, COLOR c ){

	if ( texture ) {
		const auto	context = DRAWX_DX11::device( 0 );

		D3D11_MAPPED_SUBRESOURCE	rc;
		const auto					id = 0;

		context->Map( texture, id, D3D11_MAP_WRITE_DISCARD, 0, &rc );

		if ( const auto pb = reinterpret_cast<BYTE*>( rc.pData ) ) {
			const auto	cl = ( DWORD )c;

			for ( auto i = y; i < h; i++ ) {
				const auto	pd = reinterpret_cast<DWORD*>( &pb[rc.RowPitch*i] );

				for ( auto j = x; j < w; j++ ) {
					pd[j] = cl;
				}
			}
		}
		context->Unmap( texture, id );
	}
}

//----------------------------------------
// Lock/UnLock
//----------------------------------------
bool SURFACE_::Lock( LOCKED& r, UINT n ){

	if ( texture ) {
		DRAWX_DX11::device( 0 )->Map( texture, n, D3D11_MAP_WRITE_DISCARD, 0, &r );
		return r.pData ? true : false;
	}
	return false;
}

void SURFACE_::UnLock( UINT n ){

	if ( texture ) {
		DRAWX_DX11::device( 0 )->Unmap( texture, n );
	}
}

// End Of File
