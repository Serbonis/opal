//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/polygon_dx11_.hpp"
#include "Draw/rsrc/polygon_dx11d.hpp"

//========================================
// �|���S�����\�[�X
//========================================
using namespace opal;
using namespace opal::dx11;

using CREATE_POLYGON = DRAWX_DX11::CREATE_POLYGON;

namespace {
#include "xs_shape.fxh"
#include "vs_shape.fxh"
#include "ps_shape.fxh"
}

namespace {
	constexpr auto	S = 100.f;
}

//----------------------------------------
// ��
//----------------------------------------
namespace {
	namespace TRIANGLE {
		constexpr auto	R = 1.7320508075689f;

		const WORD	indexlist[] = { 0, 1, 2, };

		const FVF_VIT	vertexlist[]
		= {
		   {  0, -S * R * 2 / 3, 0 },
		   { +S, +S * R * 1 / 3, 0 },
		   { -S, +S * R * 1 / 3, 0 },
		};

		const CREATE_POLYGON	polygon_data
			{
			 D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
			 indexlist,		std::size( indexlist  ),
			 vertexlist,	std::size( vertexlist ),
			};
	}
}

namespace opal::dx11 {
	void POLYGON_TRIANGLE::Init( const void*, size_t, const char* p, const void* ){

		POLYGON::Init( &TRIANGLE::polygon_data, sizeof( CREATE_POLYGON ), p, nullptr );
	}
}

//----------------------------------------
// ��
//----------------------------------------
namespace {
	namespace STAR {
		constexpr	auto	R = S;
#ifdef _MSC_VER
		const	 	auto	r = R * ( 3 - std::sqrt( 5 ) )/2;
#else
		constexpr 	auto	r = R * ( 3 - std::sqrt( 5 ) )/2;
#endif

		const WORD		indexlist[] = {
			0, 1, 2,
			0, 2, 3,
			0, 3, 4,
			0, 4, 5,
			0, 5, 6,
			0, 6, 7,
			0, 7, 8,
			0, 8, 9,
			0, 9, 10,
			0, 10,11,
		};

		const FVF_VIT	vertexlist[]
		= {
		   { 0,            0             },
		   { COS_M090 * R, SIN_M090 * R, },
		   { COS_M054 * r, SIN_M054 * r, },
		   { COS_M018 * R, SIN_M018 * R, },
		   { COS_P018 * r, SIN_P018 * r, },
		   { COS_P054 * R, SIN_P054 * R, },
		   { COS_P090 * r, SIN_P090 * r, },
		   { COS_P126 * R, SIN_P126 * R, },
		   { COS_P162 * r, SIN_P162 * r, },
		   { COS_P198 * R, SIN_P198 * R, },
		   { COS_P234 * r, SIN_P234 * r, },
		   { COS_P270 * R, SIN_P270 * R, },
		};

		const CREATE_POLYGON	polygon_data
			{
			 D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
			 indexlist,		std::size( indexlist  ),
			 vertexlist,	std::size( vertexlist ),
			};
	}
}

namespace opal::dx11 {
	void POLYGON_STAR::Init( const void*, size_t, const char* p, const void* ){

		POLYGON::Init( &STAR::polygon_data, sizeof( CREATE_POLYGON ), p, nullptr );
	}
}

//----------------------------------------
// ��
//----------------------------------------
namespace {
	namespace CIRCLE {
		const WORD	indexlist[]
		= {
		   0,    1,    2,	0,    2,    3,	0,    3,    4,
		   0,    4,    5,	0,    5,    6,	0,    6,    7,
		   0,    7,    8,	0,    8,    9,	0,    9,   10,
		   0,   10,   11,	0,   11,   12,	0,   12,   13,
		   0,   13,   14,	0,   14,   15,	0,   15,   16,
		   0,   16,   17,	0,   17,   18,	0,   18,   19,
		   0,   19,   20,	0,   20,   21,	0,   21,   22,
		   0,   22,   23,	0,   23,   24,	0,   24,   25,
		   0,   25,   26,	0,   26,   27,	0,   27,   28,
		   0,   28,   29,	0,   29,   30,	0,   30,   31,
		   0,   31,   32,	0,   32,   33,	0,   33,   34,
		   0,   34,   35,	0,   35,   36,	0,   36,   37,
		   0,   37,   38,	0,   38,   39,	0,   39,   40,
		   0,   40,   41,	0,   41,   42,	0,   42,   43,
		   0,   43,   44,	0,   44,   45,	0,   45,   46,
		   0,   46,   47,	0,   47,   48,	0,   48,   49,
		   0,   49,   50,	0,   50,   51,	0,   51,   52,
		   0,   52,   53,	0,   53,   54,	0,   54,   55,
		   0,   55,   56,	0,   56,   57,	0,   57,   58,
		   0,   58,   59,	0,   59,   60,	0,   60,   61,
		   0,   61,   62,	0,   62,   63,	0,   63,   64,
		   0,   64,   65,	0,   65,   66,	0,   66,   67,
		   0,   67,   68,	0,   68,   69,	0,   69,   70,
		   0,   70,   71,	0,   71,   72,	0,   72,   73,
		   0,   73,   74,	0,   74,   75,	0,   75,   76,
		   0,   76,   77,	0,   77,   78,	0,   78,   79,
		   0,   79,   80,	0,   80,   81,	0,   81,   82,
		   0,   82,   83,	0,   83,   84,	0,   84,   85,
		   0,   85,   86,	0,   86,   87,	0,   87,   88,
		   0,   88,   89,	0,   89,   90,	0,   90,   91,
		   0,   91,   92,	0,   92,   93,	0,   93,   94,
		   0,   94,   95,	0,   95,   96,	0,   96,   97,
		   0,   97,   98,	0,   98,   99,	0,   99,  100,
		   0,  100,  101,	0,  101,  102,	0,  102,  103,
		   0,  103,  104,	0,  104,  105,	0,  105,  106,
		   0,  106,  107,	0,  107,  108,	0,  108,  109,
		   0,  109,  110,	0,  110,  111,	0,  111,  112,
		   0,  112,  113,	0,  113,  114,	0,  114,  115,
		   0,  115,  116,	0,  116,  117,	0,  117,  118,
		   0,  118,  119,	0,  119,  120,	0,  120,  121,
		   0,  121,  122,	0,  122,  123,	0,  123,  124,
		   0,  124,  125,	0,  125,  126,	0,  126,  127,
		   0,  127,  128,	0,  128,  129,	0,  129,  130,
		   0,  130,  131,	0,  131,  132,	0,  132,  133,
		   0,  133,  134,	0,  134,  135,	0,  135,  136,
		   0,  136,  137,	0,  137,  138,	0,  138,  139,
		   0,  139,  140,	0,  140,  141,	0,  141,  142,
		   0,  142,  143,	0,  143,  144,	0,  144,  145,
		   0,  145,  146,	0,  146,  147,	0,  147,  148,
		   0,  148,  149,	0,  149,  150,	0,  150,  151,
		   0,  151,  152,	0,  152,  153,	0,  153,  154,
		   0,  154,  155,	0,  155,  156,	0,  156,  157,
		   0,  157,  158,	0,  158,  159,	0,  159,  160,
		   0,  160,  161,	0,  161,  162,	0,  162,  163,
		   0,  163,  164,	0,  164,  165,	0,  165,  166,
		   0,  166,  167,	0,  167,  168,	0,  168,  169,
		   0,  169,  170,	0,  170,  171,	0,  171,  172,
		   0,  172,  173,	0,  173,  174,	0,  174,  175,
		   0,  175,  176,	0,  176,  177,	0,  177,  178,
		   0,  178,  179,	0,  179,  180,	0,  180,  181,
		   0,  181,  182,	0,  182,  183,	0,  183,  184,
		   0,  184,  185,	0,  185,  186,	0,  186,  187,
		   0,  187,  188,	0,  188,  189,	0,  189,  190,
		   0,  190,  191,	0,  191,  192,	0,  192,  193,
		   0,  193,  194,	0,  194,  195,	0,  195,  196,
		   0,  196,  197,	0,  197,  198,	0,  198,  199,
		   0,  199,  200,	0,  200,  201,	0,  201,  202,
		   0,  202,  203,	0,  203,  204,	0,  204,  205,
		   0,  205,  206,	0,  206,  207,	0,  207,  208,
		   0,  208,  209,	0,  209,  210,	0,  210,  211,
		   0,  211,  212,	0,  212,  213,	0,  213,  214,
		   0,  214,  215,	0,  215,  216,	0,  216,  217,
		   0,  217,  218,	0,  218,  219,	0,  219,  220,
		   0,  220,  221,	0,  221,  222,	0,  222,  223,
		   0,  223,  224,	0,  224,  225,	0,  225,  226,
		   0,  226,  227,	0,  227,  228,	0,  228,  229,
		   0,  229,  230,	0,  230,  231,	0,  231,  232,
		   0,  232,  233,	0,  233,  234,	0,  234,  235,
		   0,  235,  236,	0,  236,  237,	0,  237,  238,
		   0,  238,  239,	0,  239,  240,	0,  240,  241,
		   0,  241,  242,	0,  242,  243,	0,  243,  244,
		   0,  244,  245,	0,  245,  246,	0,  246,  247,
		   0,  247,  248,	0,  248,  249,	0,  249,  250,
		   0,  250,  251,	0,  251,  252,	0,  252,  253,
		   0,  253,  254,	0,  254,  255,	0,  255,  256,
		   0,  256,  257,	0,  257,  258,	0,  258,  259,
		   0,  259,  260,	0,  260,  261,	0,  261,  262,
		   0,  262,  263,	0,  263,  264,	0,  264,  265,
		   0,  265,  266,	0,  266,  267,	0,  267,  268,
		   0,  268,  269,	0,  269,  270,	0,  270,  271,
		   0,  271,  272,	0,  272,  273,	0,  273,  274,
		   0,  274,  275,	0,  275,  276,	0,  276,  277,
		   0,  277,  278,	0,  278,  279,	0,  279,  280,
		   0,  280,  281,	0,  281,  282,	0,  282,  283,
		   0,  283,  284,	0,  284,  285,	0,  285,  286,
		   0,  286,  287,	0,  287,  288,	0,  288,  289,
		   0,  289,  290,	0,  290,  291,	0,  291,  292,
		   0,  292,  293,	0,  293,  294,	0,  294,  295,
		   0,  295,  296,	0,  296,  297,	0,  297,  298,
		   0,  298,  299,	0,  299,  300,	0,  300,  301,
		   0,  301,  302,	0,  302,  303,	0,  303,  304,
		   0,  304,  305,	0,  305,  306,	0,  306,  307,
		   0,  307,  308,	0,  308,  309,	0,  309,  310,
		   0,  310,  311,	0,  311,  312,	0,  312,  313,
		   0,  313,  314,	0,  314,  315,	0,  315,  316,
		   0,  316,  317,	0,  317,  318,	0,  318,  319,
		   0,  319,  320,	0,  320,  321,	0,  321,  322,
		   0,  322,  323,	0,  323,  324,	0,  324,  325,
		   0,  325,  326,	0,  326,  327,	0,  327,  328,
		   0,  328,  329,	0,  329,  330,	0,  330,  331,
		   0,  331,  332,	0,  332,  333,	0,  333,  334,
		   0,  334,  335,	0,  335,  336,	0,  336,  337,
		   0,  337,  338,	0,  338,  339,	0,  339,  340,
		   0,  340,  341,	0,  341,  342,	0,  342,  343,
		   0,  343,  344,	0,  344,  345,	0,  345,  346,
		   0,  346,  347,	0,  347,  348,	0,  348,  349,
		   0,  349,  350,	0,  350,  351,	0,  351,  352,
		   0,  352,  353,	0,  353,  354,	0,  354,  355,
		   0,  355,  356,	0,  356,  357,	0,  357,  358,
		   0,  358,  359,	0,  359,  360,	0,  360,  361,
		   0,  361,  362,
		};

		const FVF_VIT	vertexlist[]
		= {
		   { 0,            0            },
		   //------------------------------------------------------------------------------------------
		   { S * COS_M090, S * SIN_M090 },{ S * COS_M089, S * SIN_M089 },{ S * COS_M088, S * SIN_M088 },
		   { S * COS_M087, S * SIN_M087 },{ S * COS_M086, S * SIN_M086 },{ S * COS_M085, S * SIN_M085 },
		   { S * COS_M084, S * SIN_M084 },{ S * COS_M083, S * SIN_M083 },{ S * COS_M082, S * SIN_M082 },
		   { S * COS_M081, S * SIN_M081 },{ S * COS_M080, S * SIN_M080 },{ S * COS_M079, S * SIN_M079 },
		   { S * COS_M078, S * SIN_M078 },{ S * COS_M077, S * SIN_M077 },{ S * COS_M076, S * SIN_M076 },
		   { S * COS_M075, S * SIN_M075 },{ S * COS_M074, S * SIN_M074 },{ S * COS_M073, S * SIN_M073 },
		   { S * COS_M072, S * SIN_M072 },{ S * COS_M071, S * SIN_M071 },{ S * COS_M070, S * SIN_M070 },
		   { S * COS_M069, S * SIN_M069 },{ S * COS_M068, S * SIN_M068 },{ S * COS_M067, S * SIN_M067 },
		   { S * COS_M066, S * SIN_M066 },{ S * COS_M065, S * SIN_M065 },{ S * COS_M064, S * SIN_M064 },
		   { S * COS_M063, S * SIN_M063 },{ S * COS_M062, S * SIN_M062 },{ S * COS_M061, S * SIN_M061 },
		   { S * COS_M060, S * SIN_M060 },{ S * COS_M059, S * SIN_M059 },{ S * COS_M058, S * SIN_M058 },
		   { S * COS_M057, S * SIN_M057 },{ S * COS_M056, S * SIN_M056 },{ S * COS_M055, S * SIN_M055 },
		   { S * COS_M054, S * SIN_M054 },{ S * COS_M053, S * SIN_M053 },{ S * COS_M052, S * SIN_M052 },
		   { S * COS_M051, S * SIN_M051 },{ S * COS_M050, S * SIN_M050 },{ S * COS_M049, S * SIN_M049 },
		   { S * COS_M048, S * SIN_M048 },{ S * COS_M047, S * SIN_M047 },{ S * COS_M046, S * SIN_M046 },
		   { S * COS_M045, S * SIN_M045 },{ S * COS_M044, S * SIN_M044 },{ S * COS_M043, S * SIN_M043 },
		   { S * COS_M042, S * SIN_M042 },{ S * COS_M041, S * SIN_M041 },{ S * COS_M040, S * SIN_M040 },
		   { S * COS_M039, S * SIN_M039 },{ S * COS_M038, S * SIN_M038 },{ S * COS_M037, S * SIN_M037 },
		   { S * COS_M036, S * SIN_M036 },{ S * COS_M035, S * SIN_M035 },{ S * COS_M034, S * SIN_M034 },
		   { S * COS_M033, S * SIN_M033 },{ S * COS_M032, S * SIN_M032 },{ S * COS_M031, S * SIN_M031 },
		   { S * COS_M030, S * SIN_M030 },{ S * COS_M029, S * SIN_M029 },{ S * COS_M028, S * SIN_M028 },
		   { S * COS_M027, S * SIN_M027 },{ S * COS_M026, S * SIN_M026 },{ S * COS_M025, S * SIN_M025 },
		   { S * COS_M024, S * SIN_M024 },{ S * COS_M023, S * SIN_M023 },{ S * COS_M022, S * SIN_M022 },
		   { S * COS_M021, S * SIN_M021 },{ S * COS_M020, S * SIN_M020 },{ S * COS_M019, S * SIN_M019 },
		   { S * COS_M018, S * SIN_M018 },{ S * COS_M017, S * SIN_M017 },{ S * COS_M016, S * SIN_M016 },
		   { S * COS_M015, S * SIN_M015 },{ S * COS_M014, S * SIN_M014 },{ S * COS_M013, S * SIN_M013 },
		   { S * COS_M012, S * SIN_M012 },{ S * COS_M011, S * SIN_M011 },{ S * COS_M010, S * SIN_M010 },
		   { S * COS_M009, S * SIN_M009 },{ S * COS_M008, S * SIN_M008 },{ S * COS_M007, S * SIN_M007 },
		   { S * COS_M006, S * SIN_M006 },{ S * COS_M005, S * SIN_M005 },{ S * COS_M004, S * SIN_M004 },
		   { S * COS_M003, S * SIN_M003 },{ S * COS_M002, S * SIN_M002 },{ S * COS_M001, S * SIN_M001 },
		   //------------------------------------------------------------------------------------------
		   { S * COS_P000, S * SIN_P000 },{ S * COS_P001, S * SIN_P001 },{ S * COS_P002, S * SIN_P002 },
		   { S * COS_P003, S * SIN_P003 },{ S * COS_P004, S * SIN_P004 },{ S * COS_P005, S * SIN_P005 },
		   { S * COS_P006, S * SIN_P006 },{ S * COS_P007, S * SIN_P007 },{ S * COS_P008, S * SIN_P008 },
		   { S * COS_P009, S * SIN_P009 },{ S * COS_P010, S * SIN_P010 },{ S * COS_P011, S * SIN_P011 },
		   { S * COS_P012, S * SIN_P012 },{ S * COS_P013, S * SIN_P013 },{ S * COS_P014, S * SIN_P014 },
		   { S * COS_P015, S * SIN_P015 },{ S * COS_P016, S * SIN_P016 },{ S * COS_P017, S * SIN_P017 },
		   { S * COS_P018, S * SIN_P018 },{ S * COS_P019, S * SIN_P019 },{ S * COS_P020, S * SIN_P020 },
		   { S * COS_P021, S * SIN_P021 },{ S * COS_P022, S * SIN_P022 },{ S * COS_P023, S * SIN_P023 },
		   { S * COS_P024, S * SIN_P024 },{ S * COS_P025, S * SIN_P025 },{ S * COS_P026, S * SIN_P026 },
		   { S * COS_P027, S * SIN_P027 },{ S * COS_P028, S * SIN_P028 },{ S * COS_P029, S * SIN_P029 },
		   { S * COS_P030, S * SIN_P030 },{ S * COS_P031, S * SIN_P031 },{ S * COS_P032, S * SIN_P032 },
		   { S * COS_P033, S * SIN_P033 },{ S * COS_P034, S * SIN_P034 },{ S * COS_P035, S * SIN_P035 },
		   { S * COS_P036, S * SIN_P036 },{ S * COS_P037, S * SIN_P037 },{ S * COS_P038, S * SIN_P038 },
		   { S * COS_P039, S * SIN_P039 },{ S * COS_P040, S * SIN_P040 },{ S * COS_P041, S * SIN_P041 },
		   { S * COS_P042, S * SIN_P042 },{ S * COS_P043, S * SIN_P043 },{ S * COS_P044, S * SIN_P044 },
		   { S * COS_P045, S * SIN_P045 },{ S * COS_P046, S * SIN_P046 },{ S * COS_P047, S * SIN_P047 },
		   { S * COS_P048, S * SIN_P048 },{ S * COS_P049, S * SIN_P049 },{ S * COS_P050, S * SIN_P050 },
		   { S * COS_P051, S * SIN_P051 },{ S * COS_P052, S * SIN_P052 },{ S * COS_P053, S * SIN_P053 },
		   { S * COS_P054, S * SIN_P054 },{ S * COS_P055, S * SIN_P055 },{ S * COS_P056, S * SIN_P056 },
		   { S * COS_P057, S * SIN_P057 },{ S * COS_P058, S * SIN_P058 },{ S * COS_P059, S * SIN_P059 },
		   { S * COS_P060, S * SIN_P060 },{ S * COS_P061, S * SIN_P061 },{ S * COS_P062, S * SIN_P062 },
		   { S * COS_P063, S * SIN_P063 },{ S * COS_P064, S * SIN_P064 },{ S * COS_P065, S * SIN_P065 },
		   { S * COS_P066, S * SIN_P066 },{ S * COS_P067, S * SIN_P067 },{ S * COS_P068, S * SIN_P068 },
		   { S * COS_P069, S * SIN_P069 },{ S * COS_P070, S * SIN_P070 },{ S * COS_P071, S * SIN_P071 },
		   { S * COS_P072, S * SIN_P072 },{ S * COS_P073, S * SIN_P073 },{ S * COS_P074, S * SIN_P074 },
		   { S * COS_P075, S * SIN_P075 },{ S * COS_P076, S * SIN_P076 },{ S * COS_P077, S * SIN_P077 },
		   { S * COS_P078, S * SIN_P078 },{ S * COS_P079, S * SIN_P079 },{ S * COS_P080, S * SIN_P080 },
		   { S * COS_P081, S * SIN_P081 },{ S * COS_P082, S * SIN_P082 },{ S * COS_P083, S * SIN_P083 },
		   { S * COS_P084, S * SIN_P084 },{ S * COS_P085, S * SIN_P085 },{ S * COS_P086, S * SIN_P086 },
		   { S * COS_P087, S * SIN_P087 },{ S * COS_P088, S * SIN_P088 },{ S * COS_P089, S * SIN_P089 },
		   //------------------------------------------------------------------------------------------
		   { S * COS_P090, S * SIN_P090 },{ S * COS_P091, S * SIN_P091 },{ S * COS_P092, S * SIN_P092 },
		   { S * COS_P093, S * SIN_P093 },{ S * COS_P094, S * SIN_P094 },{ S * COS_P095, S * SIN_P095 },
		   { S * COS_P096, S * SIN_P096 },{ S * COS_P097, S * SIN_P097 },{ S * COS_P098, S * SIN_P098 },
		   { S * COS_P099, S * SIN_P099 },{ S * COS_P100, S * SIN_P100 },{ S * COS_P101, S * SIN_P101 },
		   { S * COS_P102, S * SIN_P102 },{ S * COS_P103, S * SIN_P103 },{ S * COS_P104, S * SIN_P104 },
		   { S * COS_P105, S * SIN_P105 },{ S * COS_P106, S * SIN_P106 },{ S * COS_P107, S * SIN_P107 },
		   { S * COS_P108, S * SIN_P108 },{ S * COS_P109, S * SIN_P109 },{ S * COS_P110, S * SIN_P110 },
		   { S * COS_P111, S * SIN_P111 },{ S * COS_P112, S * SIN_P112 },{ S * COS_P113, S * SIN_P113 },
		   { S * COS_P114, S * SIN_P114 },{ S * COS_P115, S * SIN_P115 },{ S * COS_P116, S * SIN_P116 },
		   { S * COS_P117, S * SIN_P117 },{ S * COS_P118, S * SIN_P118 },{ S * COS_P119, S * SIN_P119 },
		   { S * COS_P120, S * SIN_P120 },{ S * COS_P121, S * SIN_P121 },{ S * COS_P122, S * SIN_P122 },
		   { S * COS_P123, S * SIN_P123 },{ S * COS_P124, S * SIN_P124 },{ S * COS_P125, S * SIN_P125 },
		   { S * COS_P126, S * SIN_P126 },{ S * COS_P127, S * SIN_P127 },{ S * COS_P128, S * SIN_P128 },
		   { S * COS_P129, S * SIN_P129 },{ S * COS_P130, S * SIN_P130 },{ S * COS_P131, S * SIN_P131 },
		   { S * COS_P132, S * SIN_P132 },{ S * COS_P133, S * SIN_P133 },{ S * COS_P134, S * SIN_P134 },
		   { S * COS_P135, S * SIN_P135 },{ S * COS_P136, S * SIN_P136 },{ S * COS_P137, S * SIN_P137 },
		   { S * COS_P138, S * SIN_P138 },{ S * COS_P139, S * SIN_P139 },{ S * COS_P140, S * SIN_P140 },
		   { S * COS_P141, S * SIN_P141 },{ S * COS_P142, S * SIN_P142 },{ S * COS_P143, S * SIN_P143 },
		   { S * COS_P144, S * SIN_P144 },{ S * COS_P145, S * SIN_P145 },{ S * COS_P146, S * SIN_P146 },
		   { S * COS_P147, S * SIN_P147 },{ S * COS_P148, S * SIN_P148 },{ S * COS_P149, S * SIN_P149 },
		   { S * COS_P150, S * SIN_P150 },{ S * COS_P151, S * SIN_P151 },{ S * COS_P152, S * SIN_P152 },
		   { S * COS_P153, S * SIN_P153 },{ S * COS_P154, S * SIN_P154 },{ S * COS_P155, S * SIN_P155 },
		   { S * COS_P156, S * SIN_P156 },{ S * COS_P157, S * SIN_P157 },{ S * COS_P158, S * SIN_P158 },
		   { S * COS_P159, S * SIN_P159 },{ S * COS_P160, S * SIN_P160 },{ S * COS_P161, S * SIN_P161 },
		   { S * COS_P162, S * SIN_P162 },{ S * COS_P163, S * SIN_P163 },{ S * COS_P164, S * SIN_P164 },
		   { S * COS_P165, S * SIN_P165 },{ S * COS_P166, S * SIN_P166 },{ S * COS_P167, S * SIN_P167 },
		   { S * COS_P168, S * SIN_P168 },{ S * COS_P169, S * SIN_P169 },{ S * COS_P170, S * SIN_P170 },
		   { S * COS_P171, S * SIN_P171 },{ S * COS_P172, S * SIN_P172 },{ S * COS_P173, S * SIN_P173 },
		   { S * COS_P174, S * SIN_P174 },{ S * COS_P175, S * SIN_P175 },{ S * COS_P176, S * SIN_P176 },
		   { S * COS_P177, S * SIN_P177 },{ S * COS_P178, S * SIN_P178 },{ S * COS_P179, S * SIN_P179 },
		   //------------------------------------------------------------------------------------------
		   { S * COS_M180, S * SIN_M180 },{ S * COS_M179, S * SIN_M179 },{ S * COS_M178, S * SIN_M178 },
		   { S * COS_M177, S * SIN_M177 },{ S * COS_M176, S * SIN_M176 },{ S * COS_M175, S * SIN_M175 },
		   { S * COS_M174, S * SIN_M174 },{ S * COS_M173, S * SIN_M173 },{ S * COS_M172, S * SIN_M172 },
		   { S * COS_M171, S * SIN_M171 },{ S * COS_M170, S * SIN_M170 },{ S * COS_M169, S * SIN_M169 },
		   { S * COS_M168, S * SIN_M168 },{ S * COS_M167, S * SIN_M167 },{ S * COS_M166, S * SIN_M166 },
		   { S * COS_M165, S * SIN_M165 },{ S * COS_M164, S * SIN_M164 },{ S * COS_M163, S * SIN_M163 },
		   { S * COS_M162, S * SIN_M162 },{ S * COS_M161, S * SIN_M161 },{ S * COS_M160, S * SIN_M160 },
		   { S * COS_M159, S * SIN_M159 },{ S * COS_M158, S * SIN_M158 },{ S * COS_M157, S * SIN_M157 },
		   { S * COS_M156, S * SIN_M156 },{ S * COS_M155, S * SIN_M155 },{ S * COS_M154, S * SIN_M154 },
		   { S * COS_M153, S * SIN_M153 },{ S * COS_M152, S * SIN_M152 },{ S * COS_M151, S * SIN_M151 },
		   { S * COS_M150, S * SIN_M150 },{ S * COS_M149, S * SIN_M149 },{ S * COS_M148, S * SIN_M148 },
		   { S * COS_M147, S * SIN_M147 },{ S * COS_M146, S * SIN_M146 },{ S * COS_M145, S * SIN_M145 },
		   { S * COS_M144, S * SIN_M144 },{ S * COS_M143, S * SIN_M143 },{ S * COS_M142, S * SIN_M142 },
		   { S * COS_M141, S * SIN_M141 },{ S * COS_M140, S * SIN_M140 },{ S * COS_M139, S * SIN_M139 },
		   { S * COS_M138, S * SIN_M138 },{ S * COS_M137, S * SIN_M137 },{ S * COS_M136, S * SIN_M136 },
		   { S * COS_M135, S * SIN_M135 },{ S * COS_M134, S * SIN_M134 },{ S * COS_M133, S * SIN_M133 },
		   { S * COS_M132, S * SIN_M132 },{ S * COS_M131, S * SIN_M131 },{ S * COS_M130, S * SIN_M130 },
		   { S * COS_M129, S * SIN_M129 },{ S * COS_M128, S * SIN_M128 },{ S * COS_M127, S * SIN_M127 },
		   { S * COS_M126, S * SIN_M126 },{ S * COS_M125, S * SIN_M125 },{ S * COS_M124, S * SIN_M124 },
		   { S * COS_M123, S * SIN_M123 },{ S * COS_M122, S * SIN_M122 },{ S * COS_M121, S * SIN_M121 },
		   { S * COS_M120, S * SIN_M120 },{ S * COS_M119, S * SIN_M119 },{ S * COS_M118, S * SIN_M118 },
		   { S * COS_M117, S * SIN_M117 },{ S * COS_M116, S * SIN_M116 },{ S * COS_M115, S * SIN_M115 },
		   { S * COS_M114, S * SIN_M114 },{ S * COS_M113, S * SIN_M113 },{ S * COS_M112, S * SIN_M112 },
		   { S * COS_M111, S * SIN_M111 },{ S * COS_M110, S * SIN_M110 },{ S * COS_M109, S * SIN_M109 },
		   { S * COS_M108, S * SIN_M108 },{ S * COS_M107, S * SIN_M107 },{ S * COS_M106, S * SIN_M106 },
		   { S * COS_M105, S * SIN_M105 },{ S * COS_M104, S * SIN_M104 },{ S * COS_M103, S * SIN_M103 },
		   { S * COS_M102, S * SIN_M102 },{ S * COS_M101, S * SIN_M101 },{ S * COS_M100, S * SIN_M100 },
		   { S * COS_M099, S * SIN_M099 },{ S * COS_M098, S * SIN_M098 },{ S * COS_M097, S * SIN_M097 },
		   { S * COS_M096, S * SIN_M096 },{ S * COS_M095, S * SIN_M095 },{ S * COS_M094, S * SIN_M094 },
		   { S * COS_M093, S * SIN_M093 },{ S * COS_M092, S * SIN_M092 },{ S * COS_M091, S * SIN_M091 },
		   //------------------------------------------------------------------------------------------
		   { S * COS_M090, S * SIN_M090 },
		};

		const CREATE_POLYGON	polygon_data
			{
			 D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
			 indexlist,		std::size( indexlist  ),
			 vertexlist,	std::size( vertexlist ),
			};
	}
};

namespace opal::dx11 {
	void POLYGON_CIRCLE::Init( const void*, size_t, const char* p, const void* ){

		POLYGON::Init( &CIRCLE::polygon_data, sizeof( CREATE_POLYGON ), p, nullptr );
	}

	void POLYGON_CIRCLE::SetArch( UINT s ){ Polygon()->Index( s * 3 );	}
	auto POLYGON_CIRCLE::GetArch( void ) const->UINT{ return Polygon()->Index()/3;	}
}

// End Of File

