//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/texture_dx11.hpp"
#include "Draw/rsrc/texture_dx11_.hpp"

//========================================
// テクスチャーリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// constructor & destructor
//----------------------------------------
TEXTURE::TEXTURE() :
	device( nullptr ),
	data( nullptr )
{}
TEXTURE::~TEXTURE(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void TEXTURE::Init( const void* b, size_t s, const char* p, const void* d ){

	if ( !b || !s ) {
		RSRC::Init( p, d );
		return;
	}

	if ( !file.Name() ) {
		dx::TEXTURE::Init( b, s, p, d );

		if ( file.Adrs() && file.Size() ) {
			data = NEWS<CREATE_TEXTURE>( p, b, s, d );

			TEXTURE::InitDevice( 0 );
			DRAWX::DeviceLost( DeviceName( NAME( "%p:TEXTURE(%s)", this, p ) ), this );
		}
	}
}

void TEXTURE::Free( void ){

	if ( file.Name() ) {
		if ( file.Adrs() && file.Size() ) {
			DRAWX::DeviceLost( DeviceName() );
			TEXTURE::FreeDevice( 0 );

			DELETES( reinterpret_cast<CREATE_TEXTURE*>( data ) );
		}

		dx::TEXTURE::Free();
	}
}

void TEXTURE::Init( const std::string_view t, float x, float y, float w, float h ){ const CREATE_TEXTURE_RECT d{x,y,w,h}; Init( t.data(), {}, &d );	}
void TEXTURE::Init( const std::string_view t, float w, float h ){ Init( t.data(), 0, 0, w, h );	}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
int TEXTURE::InitDevice( int n ){

	if ( !device ) {
		NEWS( device );
		device->CreateDevice( reinterpret_cast<CREATE_TEXTURE*>( data ) );
	}

	return 0;
}

int TEXTURE::FreeDevice( int n ){

	if ( device ) {
		device->DestroyDevice();
		DELETES( device );
	}

	return 0;
}

//----------------------------------------
// テクスチャーサイズ
//----------------------------------------
UINT TEXTURE::Width(  void ) const { return device ? (*device)()->Width()  : 0;	}
UINT TEXTURE::Height( void ) const { return device ? (*device)()->Height() : 0;	}

//----------------------------------------
// リソース
//----------------------------------------
TEXTURE_* TEXTURE::Texture(  void ) const { return device ? (*device)() : nullptr; }

namespace opal::dx11 {
#include "texture_dx.inl"
}

// End Of File
