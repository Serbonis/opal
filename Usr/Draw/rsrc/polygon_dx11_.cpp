//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/polygon_dx11.hpp"
#include "Draw/rsrc/polygon_dx11_.hpp"

//========================================
// ポリゴンリソース
//========================================
using namespace opal;
using namespace opal::dx11;
using namespace opal::DRAWX_DX11;

//----------------------------------------
// デバイスワーク
//----------------------------------------
IDEVICE_( POLYGON_ );

//----------------------------------------
// constructor & destructor
//----------------------------------------
POLYGON_::POLYGON_( const CREATE_POLYGON* d ) :
	data( d ),
	ibuffer( nullptr ),
	vbuffer( nullptr )
{}
POLYGON_::~POLYGON_(){}

//----------------------------------------
// デバイス初期化＆後始末
//----------------------------------------
namespace {
#include "vs_shape.fxh"
}

int POLYGON_::InitDevice( int n ){

	if ( n == 0 ) {
		using namespace DRAWX_DX11;

		const auto	pd = reinterpret_cast<const CREATE_POLYGON*>( data );

		const auto	indexlist	= pd->indexlist;
		const auto	indexsize	= pd->indexsize;
		const auto	vertexlist	= pd->vertexlist;
		const auto	vertexsize	= pd->vertexsize;

		REGISTER( ibuffer, [&]{ CreateIndexBuffer(  ibuffer, indexlist,  sizeof( WORD    ) * indexsize  ); } );
		REGISTER( vbuffer, [&]{ CreateVertexBuffer( vbuffer, vertexlist, sizeof( FVF_VIT ) * vertexsize ); } );

		topology= pd->topology;
		index	= pd->indexsize;
	}

	return 0;
}

int POLYGON_::FreeDevice( int n ){

	if ( n == 0 ) {
		using namespace DRAWX_DX11;

		RELEASE( vbuffer );
		RELEASE( ibuffer );
	}

	return 0;
}

//----------------------------------------
// 取得
//----------------------------------------
ID3D11Buffer*& POLYGON_::Ibuffer( void ){ return ibuffer;	}
ID3D11Buffer*& POLYGON_::Vbuffer( void ){ return vbuffer;	}

auto POLYGON_::Topology( void ) const->D3D11_PRIMITIVE_TOPOLOGY{ return topology;	}
void POLYGON_::Topology( D3D11_PRIMITIVE_TOPOLOGY t ){ topology = t;	}
auto POLYGON_::Index( void ) const->size_t{ return index;	}
void POLYGON_::Index( size_t s ){ index = s;	}

// End Of File
