//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/texture_dx.hpp"

//========================================
// テクスチャーリソース
//========================================
using namespace opal;
using namespace opal::dx;

//----------------------------------------
// constructor & destructor
//----------------------------------------
TEXTURE::TEXTURE() {}
TEXTURE::~TEXTURE(){}

//----------------------------------------
// 初期化
//----------------------------------------
void TEXTURE::Init( const void* b, size_t s, const char* p, const void* d ){

	file.Info( FILEX::Path(), p, b, s );
}

//----------------------------------------
// 後始末
//----------------------------------------
void TEXTURE::Free( void ){

	file.Info();
}

// End Of File
