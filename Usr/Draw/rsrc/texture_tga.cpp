//========================================
// OPAL Game Library
//========================================
#include "Draw/rsrc/texture_tga.hpp"

#include "Std/fetch.hpp"

//========================================
// TGA(Targa)ファイル
//========================================
using namespace opal;

//----------------------------------------
// データ読み込み
//----------------------------------------
namespace TGA {
	UINT LoadImage( const void* sp, size_t sz, TEXTURE_BUFFER& buffer ){

		HEADER	header;

		const auto*	p = reinterpret_cast<FETCH_P>( sp );

		header.ImageIDLength	= FETCH::load<BYTE>( p );
		header.UseColorMap		= FETCH::load<BYTE>( p );
		header.ImageType		= FETCH::load<BYTE>( p );
		header.PaletteOrigin	= FETCH::load<WORD>( p );
		header.PaletteLength	= FETCH::load<WORD>( p );
		header.PaletteBitDepth	= FETCH::load<BYTE>( p );
		header.XOrigin			= FETCH::load<WORD>( p );
		header.YOrigin			= FETCH::load<WORD>( p );
		header.Width			= FETCH::load<WORD>( p );
		header.Height			= FETCH::load<WORD>( p );
		header.ColorBitDepth	= FETCH::load<BYTE>( p );
		header.Descriptor		= FETCH::load<BYTE>( p );

		if ( auto lp = reinterpret_cast<uint32_t*>( buffer( header.Width, header.Height ) ) ) {
			const auto	tw = header.Width;
			const auto	th = header.Height;
			const auto	hd = ( header.Descriptor & DESCRIPTOR_4 ) ? true : false;
			const auto	vd = ( header.Descriptor & DESCRIPTOR_5 ) ? false : true;
			const std::function<UINT(UINT,UINT)>	pn[] = {
				[=]( auto i, auto j ){ return tw*(0   +i)+ (0   +j);	},
				[=]( auto i, auto j ){ return tw*(0   +i)+ (tw-1-j);	},
				[=]( auto i, auto j ){ return tw*(th-1-i)+ (0   +j);	},
				[=]( auto i, auto j ){ return tw*(th-1-i)+ (tw-1-j);	},
			};
			const auto	pi = pn[(vd<<1)|hd];

			switch ( const auto pb = header.ColorBitDepth/8 ) {
			case COLORDEPTH_24/8:
				for ( auto i = 0UL; i < th; i++ ) {
					for ( auto j = 0UL; j < tw; j++ ) {
						lp[pi(i,j)] = (0xff000000)|(p[0]<<16)|(p[1]<<8)|(p[2]);
						p += pb;
					}
				}
				break;

			case COLORDEPTH_32/8:
				for ( auto i = 0UL; i < th; i++ ) {
					for ( auto j = 0UL; j < tw; j++ ) {
						lp[pi(i,j)] = (p[3]<<24)|(p[0]<<16)|(p[1]<<8)|(p[2]);
						p += pb;
					}
				}
				break;
			}
			return 1;
		}
		return 0;
	}
}

// End Of File
