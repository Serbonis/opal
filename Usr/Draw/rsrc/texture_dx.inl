//========================================
// �ԍ������^
//========================================
TEXTUREX::TEXTUREX() {}
TEXTUREX::~TEXTUREX(){}

UINT TEXTUREX::Textures( void ) const { return Resource();	}

void TEXTUREX::NowTexture( UINT n ){ NowResource( n );				}
UINT TEXTUREX::NowTexture( void   ) const { return NowResource();	}

void TEXTUREX::SetTexture( const TEXTURE*                  r, UINT n ){ SetResource( r,       n ); }
void TEXTUREX::SetTexture( const TEXTURE*                  r         ){ SetResource( r          ); }
void TEXTUREX::SetTexture( const TEXTURE&                  r, UINT n ){ SetResource( r,       n ); }
void TEXTUREX::SetTexture( const TEXTURE&                  r         ){ SetResource( r          ); }
void TEXTUREX::SetTexture( const std::shared_ptr<TEXTURE>& r, UINT n ){ SetResource( r.get(), n ); }
void TEXTUREX::SetTexture( const std::shared_ptr<TEXTURE>& r         ){ SetResource( r.get()    ); }

const TEXTURE* TEXTUREX::GetTexture( UINT n ) const { return GetResource( n );	}
const TEXTURE* TEXTUREX::GetTexture( void   ) const { return GetResource(   );	}

TEXTURE_* TEXTUREX::Texture( UINT n ) const { if ( const auto t = GetResource( n ) ) { return t->Texture(); } return nullptr;	}
TEXTURE_* TEXTUREX::Texture( void   ) const { if ( const auto t = GetResource(   ) ) { return t->Texture(); } return nullptr;	}

//========================================
// ���O�����^
//========================================
TEXTUREH::TEXTUREH() {}
TEXTUREH::~TEXTUREH(){}

UINT TEXTUREH::Textures( void ) const { return Resource();	}

void TEXTUREH::NowTexture( const char* p        ){ NowResource( p         );	}
void TEXTUREH::NowTexture( const std::string& p ){ NowResource( p.c_str() );	}
void TEXTUREH::NowTexture( std::nullptr_t       ){ NowResource( nullptr   );	}
const char* TEXTUREH::NowTexture( void   ) const { return NowResource(    );	}

void TEXTUREH::SetTexture( const TEXTURE*                  r, const char* p        ){ SetResource( r,       p         );	}
void TEXTUREH::SetTexture( const TEXTURE*                  r, const std::string& p ){ SetResource( r,       p.c_str() );	}
void TEXTUREH::SetTexture( const TEXTURE*                  r, std::nullptr_t       ){ SetResource( r,       nullptr   );	}
void TEXTUREH::SetTexture( const TEXTURE*                  r                       ){ SetResource( r                  );	}
void TEXTUREH::SetTexture( const TEXTURE&                  r, const char* p        ){ SetResource( r,       p         );	}
void TEXTUREH::SetTexture( const TEXTURE&                  r, const std::string& p ){ SetResource( r,       p.c_str() );	}
void TEXTUREH::SetTexture( const TEXTURE&                  r, std::nullptr_t       ){ SetResource( r,       nullptr   );	}
void TEXTUREH::SetTexture( const TEXTURE&                  r                       ){ SetResource( r                  );	}
void TEXTUREH::SetTexture( const std::shared_ptr<TEXTURE>& r, const char* p        ){ SetResource( r.get(), p         );	}
void TEXTUREH::SetTexture( const std::shared_ptr<TEXTURE>& r, const std::string& p ){ SetResource( r.get(), p.c_str() );	}
void TEXTUREH::SetTexture( const std::shared_ptr<TEXTURE>& r, std::nullptr_t       ){ SetResource( r.get(), nullptr   );	}
void TEXTUREH::SetTexture( const std::shared_ptr<TEXTURE>& r                       ){ SetResource( r.get()            );	}

const TEXTURE* TEXTUREH::GetTexture( const char* p        ) const { return GetResource( p         ); }
const TEXTURE* TEXTUREH::GetTexture( const std::string& p ) const { return GetResource( p.c_str() ); }
const TEXTURE* TEXTUREH::GetTexture( std::nullptr_t       ) const { return GetResource( nullptr   ); }
const TEXTURE* TEXTUREH::GetTexture( void                 ) const { return GetResource(           ); }

TEXTURE_* TEXTUREH::Texture( const char* p        ) const { if ( const auto t = GetResource( p         ) ) { return t->Texture(); } return nullptr; }
TEXTURE_* TEXTUREH::Texture( const std::string& p ) const { if ( const auto t = GetResource( p.c_str() ) ) { return t->Texture(); } return nullptr; }
TEXTURE_* TEXTUREH::Texture( std::nullptr_t       ) const { if ( const auto t = GetResource( nullptr   ) ) { return t->Texture(); } return nullptr; }
TEXTURE_* TEXTUREH::Texture( void                 ) const { if ( const auto t = GetResource(           ) ) { return t->Texture(); } return nullptr; }

// End Of File
