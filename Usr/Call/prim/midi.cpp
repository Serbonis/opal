//========================================
// OPAL Game Library
//========================================
#include "midi.hpp"

//========================================
// BGMシステム
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
MIDI::MIDI() :
	music( nullptr )
{}
MIDI::~MIDI(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void MIDI::Init( const char* pName ) {

	CALLT::Init( pName );

	music = NULL;
}

void MIDI::Free( void ) {

	CALLT::Free();
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void MIDI::CallInfo( opal::PRINT func ) const {
#ifdef OPAL_DEBUG
	const auto	print = func ? func : ( decltype( func ) )printd;

	print( "%-08s %p ", "MIDI", this );
	CALLT::CallPrint( print );
	MIDI::CallPrint(  print );
	print( "%p ", music );
#endif
}

void MIDI::CallPrint( opal::PRINT func ) const {
#ifdef OPAL_DEBUG
	const auto	print = func ? func : ( decltype( func ) )printd;
	const auto	m = GetMusic();

	print( "%p %d %s\n", m, ( m ) ? m->Length() : 0, ID() );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void MIDI::CallMain( void ){ /* 未実装 */ }

// End Of File

