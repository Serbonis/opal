//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"
#include "pcm.hpp"
#include "sound.hpp"

//========================================
// PCM
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PCM::PCM() :
	comp{nullptr},
	buffer{nullptr},
	volume{0.0f},
	tempo{0.0f},
	freq{0}
{}
PCM::~PCM(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void PCM::InitComp( void ){

	comp	= nullptr;
	buffer 	= nullptr;
	volume 	= 0.0f;
	tempo 	= 0.0f;
	freq	= 0;
}

void PCM::FreeComp( void ){

	Stop();

	RELEASE( buffer );
}

//----------------------------------------
// 楽曲設定
//----------------------------------------
void PCM::Component( void* s ){

	FreeComp();
	InitComp();

	if ( s ) {
		const auto			rs = reinterpret_cast<SOUND*>( comp = s );

		DSBUFFERDESC		dc = rs->Desc();
		IDirectSoundBuffer*	sb = nullptr;

		using namespace CALLX_DS;

		device()->CreateSoundBuffer( &dc, &sb, nullptr );
		sb->QueryInterface( IID_IDirectSoundBuffer8 , ( void** )&buffer );
		RELEASE( sb );

		void*		p;
		DWORD		l;

		buffer->Lock( 0, 0, &p, &l, nullptr, nullptr, DSBLOCK_ENTIREBUFFER );
		std::memcpy( p, rs->Buff(), l );
		buffer->Unlock( p, l, nullptr, 0 );
	}
}

//----------------------------------------
// 再生/停止
//----------------------------------------
void PCM::Play( void ){

	if ( buffer ) {
		buffer->SetCurrentPosition( 0 );
		buffer->Play( 0, 0, 0 );
	}
}

void PCM::Stop( void ){

	if ( buffer ) {
		buffer->Stop();
		buffer->SetCurrentPosition( 0 );
	}
}

//----------------------------------------
// 再生位置
//----------------------------------------
DWORD PCM::Seek( void ) const {

	if ( buffer ) {
		DWORD	seek = 0;

		buffer->GetCurrentPosition( &seek, nullptr );
		return seek;
	}

	return 0;
}

//----------------------------------------
// 再生ステータス
//----------------------------------------
bool PCM::Playing( void ) const {

	if ( buffer ) {
		DWORD	stat = 0;

		buffer->GetStatus( &stat );
		return stat & DSBSTATUS_PLAYING;
	}

	return false;
}

//----------------------------------------
// 音量
//----------------------------------------
void PCM::Volume( float v ){

	if ( buffer ) {
		buffer->SetVolume( []( auto v )->LONG{
			if ( v >= 1.0f ) { return DSBVOLUME_MAX; }
			if ( v <= 0.0f ) { return DSBVOLUME_MIN; }
			return log10( v ) * 33.2f * 100;
		}( volume = v ) );
	}
}

float PCM::Volume( void ) const {

	return volume;
}

//----------------------------------------
// 速度
//----------------------------------------
void PCM::Tempo( float t ){

	if ( buffer ) {
		buffer->SetFrequency( ( tempo = t ) > 0 ? ( DWORD )( freq * tempo + 0.5f ) : DSBFREQUENCY_ORIGINAL );
	}
}

float PCM::Tempo( void ) const {

	return tempo;
}

// End Of File
