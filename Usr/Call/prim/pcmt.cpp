//========================================
// OPAL Game Library
//========================================
#include "pcmt.hpp"

//========================================
// PCMタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
PCMT::PCMT() {}
PCMT::~PCMT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void PCMT::Init( const char* pName ){

	CALLT::Init( pName );

	PCM::InitComp();
}

void PCMT::Free( void ) {

	PCM::FreeComp();

	CALLT::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void PCMT::CallInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "PCMT" );
	printd( "%-08s ", ID() );
	PCMT::CallInfo( CALLX::Debug() );
	printd( "\n" );
#endif
}

void PCMT::CallInfo( int m ) const {
#ifdef OPAL_DEBUG
	CALLT::CallInfo( m );
	printd( "%08d ", prop.state.time );
#endif
}

//----------------------------------------
// コマンドの確認
//----------------------------------------
using MUSIC = PCM;

void PCMT::CallMain( void ){

	if ( const auto rs = Composition(); comp != rs ) {
		MUSIC::Component( comp = rs );
	}

	if ( comp ) {
		auto&	ps = prop.state;
		auto&	pc = prop.ctrl;

		if ( pc.play ) { pc.play = false; ps.mode = MODE_PLAY;	}
		if ( pc.stop ) { pc.stop = false; ps.mode = MODE_STOP;	}
		if ( pc.volm ) { pc.volm = false; MUSIC::Volume( GetMute() ? 0 : GetVolume() );	}
		if ( pc.tmpo ) { pc.tmpo = false; MUSIC::Tempo(  GetTempo() );	}

		switch ( ps.mode ) {
		case MODE_STOP    : mode_stop();	break;
		case MODE_PLAY    : mode_play();	break;
		case MODE_LOOP    : mode_loop();	break;
		case MODE_PLAYING : mode_playing();	break;
		}
	}
}

void PCMT::mode_stop( void ){

	auto&	ps = prop.state;

	ps = {};

	ps.play = false;

	MUSIC::Stop();
}

void PCMT::mode_play( void ){

	auto&	ps = prop.state;

	ps = {};

	ps.play = true;
	ps.mode = MODE_PLAYING;

	MUSIC::Play();
}

void PCMT::mode_loop( void ){

	auto&	ps = prop.state;

	if ( !GetLoop() ) {
		MUSIC::Play();
	}
	ps.mode = MODE_PLAYING;
}

void PCMT::mode_playing( void ){

	auto&	ps = prop.state;

	if ( MUSIC::Playing() ) {
		ps.time = MUSIC::Seek();
		return;
	}

	if ( GetLoop() && ( ++ps.loop < GetRepeat() ) ) {
		MUSIC::Play();
		return;
	}

	ps.mode = MODE_STOP;
}

// End Of File
