//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"
#include "ogg.hpp"
#include "vorbis.hpp"

//========================================
// OGG
//========================================
using namespace opal;

enum {
	MODE_PAUSE,
	MODE_PLAY,
	MODE_STOP,
	MODE_LOOP,
};

//----------------------------------------
// constructor & destructor
//----------------------------------------
OGG::OGG() :
	comp{nullptr},
	buffer{nullptr},
	mode{},
	seek{0}
{}
OGG::~OGG(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OGG::InitComp( void ){

	comp	= nullptr;
	buffer 	= nullptr;
	mode	= MODE_PAUSE;
	seek	= 0;
}

void OGG::FreeComp( void ){

	Stop();

	RELEASE( buffer );
}

//----------------------------------------
// 楽曲設定
//----------------------------------------
void OGG::Component( void* s ){

	FreeComp();
	InitComp();

	if ( s ) {
		const auto			rs = reinterpret_cast<VORBIS*>( comp = s );

		DSBUFFERDESC		dc = rs->Desc();
		IDirectSoundBuffer*	sb = nullptr;

		using namespace CALLX_DS;

		device()->CreateSoundBuffer( &dc, &sb, nullptr );
		sb->QueryInterface( IID_IDirectSoundBuffer8 , ( void** )&buffer );
		RELEASE( sb );
	}
}

//----------------------------------------
// ストリーム再生スレッド
//----------------------------------------
#include "thrdx.hpp"

void OGG::StopStream( void ) {

	mode = MODE_STOP;
	THRDX::Kill( this );
}

void OGG::PlayStream( void ) {

	const auto	rs = reinterpret_cast<VORBIS*>( comp );

	void* 	p0;
	void*	p1;
	DWORD 	l0;
	DWORD	l1;

	rs->Seek( 0.0 );
	buffer->SetCurrentPosition( 0 );

	buffer->Lock( 0, 0, &p0, &l0, &p1, &l1, DSBLOCK_ENTIREBUFFER );
	rs->Segment( ( char* )p0, l0 );
	buffer->Unlock( p0, l0, p1, l1 );

	THRDX::Throw( this, [&](auto)->void*{

		const auto		rs = reinterpret_cast<VORBIS*>( comp );
		const auto		sz = rs->Desc().dwBufferBytes/2;
		unsigned int	sp = 0;
		DWORD 			pt = 0;
		DWORD			op = 0;

		int	ns, ne;

		void* 	p0;
		void*	p1;
		DWORD 	l0;
		DWORD	l1;

		mode	= MODE_PLAY;
		seek	= 0;

		// 1841121
		do {
			switch ( mode ) {
			default: return nullptr;
			case MODE_PAUSE: 			break;
			case MODE_LOOP : sp = 0;	break;
			case MODE_PLAY :
				buffer->GetCurrentPosition( &pt, 0 );

				if ( op != pt ) {
					op = pt;

					if (      sp == 0 && pt >= sz ) { ns = 0; ne = 1; }
					else if ( sp == 1 && pt <  sz ) { ns = 1; ne = 2; }
					else { break; }

					buffer->Lock( sz * ns, sz * ne, &p0, &l0, &p1, &l1, 0 );
					if ( !rs->Segment( ( char* )p0, l0 ) ) {
						mode = MODE_STOP;
						printf( "----------------------------------------\n" );
					}
					buffer->Unlock( p0, l0, p1, l1 );
					sp = sp ? 0 : 1;
				}
				break;
			}
		} while ( 1 );

		return this;
	} );
}

//----------------------------------------
// 再生/停止
//----------------------------------------
void OGG::Play( void ){

	if ( buffer ) {
		PlayStream();

		buffer->SetCurrentPosition( 0 );
		buffer->Play( 0, 0, DSBPLAY_LOOPING );
	}
}

void OGG::Stop( void ){

	if ( buffer ) {
		buffer->Stop();
		buffer->SetCurrentPosition( 0 );

		StopStream();
	}
}

//----------------------------------------
// 再生位置
//----------------------------------------
DWORD OGG::Seek( void ) const {

	return seek;
}

//----------------------------------------
// 再生ステータス
//----------------------------------------
bool OGG::Playing( void ) const {

	return mode == MODE_PLAY;
}

//----------------------------------------
// 音量
//----------------------------------------
void OGG::Volume( float v ){}
float OGG::Volume( void ) const { return 0; }

//----------------------------------------
// 速度
//----------------------------------------
void OGG::Tempo( float t ){}
float OGG::Tempo( void ) const { return 0;	}

// End Of File
