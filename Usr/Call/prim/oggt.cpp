//========================================
// OPAL Game Library
//========================================
#include "oggt.hpp"

//========================================
// OGGタスク
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
OGGT::OGGT() {}
OGGT::~OGGT(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OGGT::Init( const char* pName ){

	CALLT::Init( pName );
	OGG::InitComp();
}

void OGGT::Free( void ) {

	OGG::FreeComp();
	CALLT::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void OGGT::CallInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "OGGT" );
	printd( "%-08s ", ID() );
	OGGT::CallInfo( CALLX::Debug() );
	printd( "\n" );
#endif
}

void OGGT::CallInfo( int m ) const {
#ifdef OPAL_DEBUG
	CALLT::CallInfo( m );
#endif
}

//----------------------------------------
// コマンドの確認
//----------------------------------------
using MUSIC = OGG;

void OGGT::CallMain( void ){

	if ( const auto rs = Composition(); comp != rs ) {
		MUSIC::Component( comp = rs );
	}

	if ( comp ) {
		auto&	ps = prop.state;
		auto&	pc = prop.ctrl;

		if ( pc.play ) { pc.play = false; ps.mode = MODE_PLAY;	}
		if ( pc.stop ) { pc.stop = false; ps.mode = MODE_STOP;	}
		if ( pc.volm ) { pc.volm = false; MUSIC::Volume( GetMute() ? 0 : GetVolume() );	}
		if ( pc.tmpo ) { pc.tmpo = false; MUSIC::Tempo(  GetTempo() );	}

		switch ( ps.mode ) {
		case MODE_STOP    : mode_stop();	break;
		case MODE_PLAY    : mode_play();	break;
		case MODE_LOOP    : mode_loop();	break;
		case MODE_PLAYING : mode_playing();	break;
		}
	}
}

void OGGT::mode_stop( void ){

	auto&	ps = prop.state;

	ps = {};

	ps.play = false;

	MUSIC::Stop();
}

void OGGT::mode_play( void ){

	auto&	ps = prop.state;

	ps = {};

	ps.play = true;
	ps.mode = MODE_PLAYING;

	MUSIC::Play();
}

void OGGT::mode_loop( void ){

	auto&	ps = prop.state;

	if ( !GetLoop() ) {
		MUSIC::Play();
	}
	ps.mode = MODE_PLAYING;
}

void OGGT::mode_playing( void ){

	auto&	ps = prop.state;

	if ( MUSIC::Playing() ) {
		ps.time = MUSIC::Seek();
		return;
	}
	return;

	if ( GetLoop() && ( ++ps.loop < GetRepeat() ) ) {
		MUSIC::Play();
		return;
	}

	ps.mode = MODE_STOP;
}

// End Of File
