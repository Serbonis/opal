//========================================
// OPAL Game Library
//========================================
#include "oggv.hpp"

//========================================
// Ogg/Vorbis
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
OGGV::OGGV() {}
OGGV::~OGGV(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void OGGV::Init( const char* pName ){

	OGGT::Init( pName );
}

void OGGV::Free( void ) {

	OGGT::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void OGGV::CallInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "OGGV" );
	printd( "%-08s ", ID() );
	OGGV::CallInfo( CALLX::Debug() );
	printd( "\n" );
#endif
}

void OGGV::CallInfo( int m ) const {
#ifdef OPAL_DEBUG
	OGGT::CallInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void OGGV::CallMain( void ){

	OGGT::CallMain();
}

//----------------------------------------
// リソース
//----------------------------------------
void* OGGV::Composition( void ) const {

	return const_cast<VORBIS*>( GetVorbis() );
}

// End Of File
