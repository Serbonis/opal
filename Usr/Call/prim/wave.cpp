//========================================
// OPAL Game Library
//========================================
#include "wave.hpp"

//========================================
// WAVE
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
WAVE::WAVE(){}
WAVE::~WAVE(){}

//----------------------------------------
// 初期化＆後始末
//----------------------------------------
void WAVE::Init( const char* pName ){

	PCMT::Init();
}

void WAVE::Free( void ) {

	PCMT::Free();
}

//----------------------------------------
// タスクの情報
//----------------------------------------
void WAVE::CallInfo( void ) const {
#ifdef OPAL_DEBUG
	printd( "%-08s ", "WAVE" );
	printd( "%-08s ", ID() );
	WAVE::CallInfo( CALLX::Debug() );
	printd( "\n" );
#endif
}

void WAVE::CallInfo( int m ) const {
#ifdef OPAL_DEBUG
	PCMT::CallInfo( m );
#endif
}

//----------------------------------------
// タスクの実行
//----------------------------------------
void WAVE::CallMain( void ){

	PCMT::CallMain();
}

//----------------------------------------
// リソース
//----------------------------------------
void* WAVE::Composition( void ) const {

	return const_cast<SOUND*>( GetSound() );
}

// End Of File
