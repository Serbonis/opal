//========================================
// OPAL Game Library
//========================================
#include "call_md.hpp"
#include "Call/rsrc/music.hpp"

//========================================
// MUSIC\[X
//========================================
using namespace opal;

//----------------------------------------
// ΔΆ/β~
//----------------------------------------
void MUSIC::Play( void ){

	if ( performance ) {
		segstate = NULL;
		performance->PlaySegmentEx( segment, NULL, NULL, 0, 0, &segstate, NULL, audiopath );
		segment->SetRepeats( 0 );
	}
}

void MUSIC::Stop( void ){

	if ( performance ) {
		performance->Stop( segment, segstate, 0, DMUS_SEGF_DEFAULT );
		segstate = NULL;
	}
}

//----------------------------------------
// ΉΚ
//----------------------------------------
void MUSIC::Volume( float vVolm ){

	if ( audiopath ) {
		volume = static_cast< LONG >( -9600 * ( 1.0f - vVolm ) );		// min ` max = -9600 ` 0
		audiopath->SetVolume( volume, 0 );
	} else {
		volume = static_cast< LONG >( vVolm );							// +1000 ` 0 ` -10000

		performance->SetGlobalParam( GUID_PerfMasterVolume, ( void* )&volume, sizeof( volume ) );
	}
}

float MUSIC::Volume( void ) const {

	if ( audiopath ) {
		;
	} else {
		performance->GetGlobalParam( GUID_PerfMasterVolume, ( void* )&volume, sizeof( volume ) );
	}

	return static_cast< float >( volume );
}

//----------------------------------------
// ΔΆΤ
//----------------------------------------
MUSIC_TIME MUSIC::Time( void ) const {

	if ( segstate ) {
		segstate->GetSeek( &time );
	}

	return ( time < length ) ? time : -1;
}

MUSIC_TIME MUSIC::Length( void ) const { return length; }

// End Of File

