//========================================
// OPAL Game Library
//========================================
#include "stream.hpp"

//========================================
// STREAM
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
STREAM::STREAM( DWORD s ) :
	buffer{nullptr},
	mmio{0},
	size{s/2},
	ofst{0},
	swap{false},
	loop{0},
	seek{0},
	play{false},
	buff( s ? s : 1.0f )
{}
STREAM::~STREAM(){

	if ( mmio ) {
		Close();
	}
}

//----------------------------------------
// Open
//----------------------------------------
void STREAM::Open( const char* name, WAVEFORMATEX& wfmt ){

	MMIOINFO	mi = {};

	mmio = ::mmioOpen( const_cast<char*>( name ), &mi, MMIO_READ );

	MMCKINFO	ri;
	MMCKINFO	fi;
	MMCKINFO	di;

	ri.fccType = mmioFOURCC( 'W','A','V','E' );
	::mmioDescend( mmio, &ri, nullptr, MMIO_FINDRIFF );

	fi.ckid = mmioFOURCC( 'f','m','t',' ' );
	::mmioDescend( mmio, &fi, &ri, MMIO_FINDCHUNK );

	::mmioRead( mmio, ( HPSTR )&wfmt, fi.cksize );
	::mmioAscend( mmio, &fi, 0 );

	di.ckid = mmioFOURCC( 'd','a','t','a' );
	::mmioDescend( mmio, &di, &ri, MMIO_FINDCHUNK );

	ofst = ::mmioSeek( mmio, 0, SEEK_CUR );
	::mmioRead( mmio, ( HPSTR )buff.data(), size * 2 );
}

//----------------------------------------
// Close
//----------------------------------------
void STREAM::Close( void ){

	::mmioClose( mmio, 0 );
	mmio = 0;
}

//----------------------------------------
//----------------------------------------
void STREAM::Buffer( IDirectSoundBuffer8* bp ){

	buffer = bp;
}

//----------------------------------------
//----------------------------------------
void STREAM::Play( void ){

	void*	p;
	DWORD	l;

	buffer->Lock( 0, 0, &p, &l, nullptr, nullptr, DSBLOCK_ENTIREBUFFER );
	std::memcpy( p, buff.data(), l );
	buffer->Unlock( p, l, nullptr, 0 );

	swap = false;
	loop = 0;
	seek = 0;
	play = true;
}

//----------------------------------------
//----------------------------------------
void STREAM::Stop( void ){

}

//----------------------------------------
// 再生位置
//----------------------------------------
DWORD STREAM::Seek( void ) const {

	return seek;
}

//----------------------------------------
// 再生ステータス
//----------------------------------------
bool STREAM::Playing( void ) const {

	return play;
}

//----------------------------------------
// ストリーム再生
//----------------------------------------
void* STREAM::operator()( void ){

	if ( [&]{
		DWORD	cpos;

		buffer->GetCurrentPosition( &cpos, 0 );

		return ( swap == 0 && cpos >= size  )
			|| ( swap == 1 && cpos <  size  );
	}() ) {
		void*	p1;
		void*	p2;
		DWORD	l1;
		DWORD	l2;

		if ( const DWORD read = ::mmioRead( mmio, ( HPSTR )buff.data(), size ); read <= 0 ) {
			::mmioSeek( mmio, ofst, SEEK_SET );
			::mmioRead( mmio, ( HPSTR )buff.data(), size );

			buffer->Lock( 0, 0, &p1, &l1, nullptr, nullptr, DSBLOCK_ENTIREBUFFER );
			std::memcpy( p1, buff.data(), l1 );
			buffer->Unlock( p1, l1, nullptr, 0 );
			buffer->SetCurrentPosition( 0 );
			swap = false;
			play = false;
			loop++;
		} else {
			if ( read < size ) {
				std::memset( &buff[read], 0, size-read );
			}
			buffer->Lock( size * swap, size, &p1, &l1, &p2, &l2, 0 );
			std::memcpy( p1, &buff[0 ], l1 );
			std::memcpy( p2, &buff[l1], l2 );
			buffer->Unlock( p1, l1, p2, l2 );
			swap = swap ? false : true;
			play = true;
		}
	}

	return this;
}

// End Of File
// 初期化(ストリーム再生)
void SOUND::Init( float vTime, const char* pName ){

	if ( FZERO( vTime ) ) {
		Init( pName );
		return;
	}

	file.Info( FILEX::Path(), pName, nullptr, 0 );

	HMMIO	mmio;

	mmio = mmioopen( file.Name() );
	leng = mmckinfo( mmio, wfmt );
	size = static_cast<UINT>( wfmt.nAvgBytesPerSec * vTime );
	buff = nullptr;
	::mmioClose( mmio, 0 );

	buffdesc( desc, size, wfmt );
}

static HMMIO mmioopen( const char* name ){

	MMIOINFO	mi = {};

	return ::mmioOpen( const_cast<char*>( name ), &mi, MMIO_READ );
}

