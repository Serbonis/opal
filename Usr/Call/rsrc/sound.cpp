//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"
#include "Call/rsrc/sound.hpp"

//========================================
// SOUNDリソース
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
SOUND::SOUND() :
	buff{},
	wfmt{},
	desc{},
	leng{0}
{}
SOUND::~SOUND(){}

//----------------------------------------
// 初期化
//----------------------------------------
static HMMIO mmioopen( const void*, LONG );
static DWORD mmckinfo( HMMIO&, WAVEFORMATEX& );
static void buffdesc( DSBUFFERDESC&, DWORD size, WAVEFORMATEX& );

void SOUND::Init( const void* pBuff, size_t vSize, const char* pName, const void* pDesc ){

	if ( !pBuff || !vSize ) {
		Init( pName );
		return;
	}

	file.Info( FILEX::Path(), pName, pBuff, vSize );

	HMMIO			mmio;

	mmio = mmioopen( file.Adrs(), file.Size() );
	leng = mmckinfo( mmio, wfmt );
	buff = std::make_shared<std::vector<char>>( leng );
	::mmioRead( mmio, ( HPSTR )buff->data(), leng );
	::mmioClose( mmio, 0 );

	buffdesc( desc, leng, wfmt );
}

static HMMIO mmioopen( const void* adrs, LONG size ){

	MMIOINFO	mi = {};

	mi.pchBuffer	= reinterpret_cast<char*>( const_cast<void*>( adrs ) );
	mi.fccIOProc	= FOURCC_MEM;
	mi.cchBuffer	= size;

	return ::mmioOpen( nullptr, &mi, MMIO_ALLOCBUF|MMIO_READ );
}

static DWORD mmckinfo( HMMIO& mmio, WAVEFORMATEX& wfmt ){

	MMCKINFO	ri;
	MMCKINFO	fi;
	MMCKINFO	di;

	ri.fccType = mmioFOURCC( 'W','A','V','E' );
	::mmioDescend( mmio, &ri, nullptr, MMIO_FINDRIFF );

	fi.ckid = mmioFOURCC( 'f','m','t',' ' );
	::mmioDescend( mmio, &fi, &ri, MMIO_FINDCHUNK );

	::mmioRead( mmio, ( HPSTR )&wfmt, fi.cksize );
	::mmioAscend( mmio, &fi, 0 );

	di.ckid = mmioFOURCC( 'd','a','t','a' );
	::mmioDescend( mmio, &di, &ri, MMIO_FINDCHUNK );

	return di.cksize;
}

static void buffdesc( DSBUFFERDESC& desc, DWORD size, WAVEFORMATEX& wfmt ){

	desc.dwSize				= sizeof( DSBUFFERDESC );
	desc.dwFlags			= (DSBCAPS_GLOBALFOCUS|DSBCAPS_GETCURRENTPOSITION2|DSBCAPS_CTRLPOSITIONNOTIFY|DSBCAPS_CTRLFREQUENCY|DSBCAPS_CTRLVOLUME|DSBCAPS_CTRLPAN);
	desc.dwBufferBytes		= size;
	desc.dwReserved			= 0;
	desc.lpwfxFormat		= &wfmt;
	desc.guid3DAlgorithm	= DS3DALG_DEFAULT;
}

//----------------------------------------
// 後始末
//----------------------------------------
void SOUND::Free( void ){

	buff = nullptr;
}

//----------------------------------------
// バッファディスクリプター
//----------------------------------------
auto SOUND::Desc( void ) const->DSBUFFERDESC{

	return desc;
}

//----------------------------------------
// バッファー
//----------------------------------------
auto SOUND::Buff( void ) const->const char*{

	return buff ? buff->data() : nullptr;
}

//----------------------------------------
// プレイ時間
//----------------------------------------
auto SOUND::Time( void ) const->DWORD{

	return ( DWORD )( 1000 * leng / wfmt.nAvgBytesPerSec );
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
SOUNDX::SOUNDX(){}
SOUNDX::~SOUNDX(){}

//----------------------------------------
// リソース
//----------------------------------------
UINT SOUNDX::Sounds( void ) const { return Resource();	}

void SOUNDX::NowSound( UINT n ){ NowResource( n );	}
UINT SOUNDX::NowSound( void   ) const { return NowResource();	}

void SOUNDX::SetSound( const SOUND* r, UINT n ){ SetResource( r, n );	}
void SOUNDX::SetSound( const SOUND* r         ){ SetResource( r    );	}
void SOUNDX::SetSound( const SOUND& r, UINT n ){ SetResource( r, n );	}
void SOUNDX::SetSound( const SOUND& r         ){ SetResource( r    );	}

const SOUND* SOUNDX::GetSound( UINT n ) const { return GetResource( n );	}
const SOUND* SOUNDX::GetSound( void   ) const { return GetResource(   );	}

// End Of File
