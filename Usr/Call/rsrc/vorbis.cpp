//========================================
// OPAL Game Library
//========================================
#include "call_ds.hpp"
#include "Call/rsrc/vorbis.hpp"

//========================================
// VORBISリソース
//========================================
using namespace opal;

#include "vorbis/vorbisfile.h"

//----------------------------------------
// constructor & destructor
//----------------------------------------
VORBIS::VORBIS() :
	ovf{nullptr},
	ovd{nullptr},
	channelNum{0},
	samplingRate{0},
	bitRate{0},
	playTime{0},
	wfmt{},
	desc{}
{}
VORBIS::~VORBIS(){}

//----------------------------------------
// デコーダー
//----------------------------------------
class OggVorbis_Decoder final {
public:
	OggVorbis_Decoder( const void* b, size_t s ) :
		buffer{static_cast<const char*>(b)},
		size{( int )s},
		cursor{0}
	{}

private:
	const char* const	buffer{nullptr};	// Oggファイルバッファ
	const int			size{0};			// バッファサイズ
	long  				cursor{0};			// 現在の位置

private:
	static auto decoder( void* b ){ return reinterpret_cast<OggVorbis_Decoder*>( b );	}

public:
	static size_t read(  void* p, size_t s, size_t m, void* b ){ return decoder( b )->Read( p, s, m );	}
	static int    seek(  void* b, ogg_int64_t o, int f        ){ return decoder( b )->Seek( o, f );	}
	static int    close( void* b                              ){ return decoder( b )->Close();	}
	static long   tell(  void* b                              ){ return decoder( b )->Tell();	}

private:
	size_t Read( void* p, size_t s, size_t m ){

		const auto	c = std::min( ( size - cursor )/s, m );
		const auto	r = s * c;

		std::memcpy( p, &buffer[cursor], r );
		cursor += r;

		return c;
	}

	int Seek( ogg_int64_t o, int f ){

		switch( f ) {
		case SEEK_CUR: cursor += o;			break;
		case SEEK_END: cursor = size + o;	break;
		case SEEK_SET: cursor = o;			break;
		default:							return -1;
		}
		if ( cursor > size ) { cursor = size;	return -1;	}
		if ( cursor < 0    ) { cursor = 0;		return -1;	}
		return 0;
	}

	int Close( void ){ return 0;	}

	long Tell( void ) const { return cursor;	}
};

//----------------------------------------
// 初期化
//----------------------------------------
void VORBIS::Init( const void* pBuff, size_t vSize, const char* pName, const void* pDesc ){

	if ( !pBuff || !vSize ) {
		Init( pName );
		return;
	}

	file.Info( FILEX::Path(), pName, pBuff, vSize );

	ovf = std::make_unique<OggVorbis_File>();
	ovd = std::make_unique<OggVorbis_Decoder>( pBuff, vSize );

	ov_open_callbacks( &*ovd, &*ovf , nullptr, 0, { ovd->read, ovd->seek, ovd->close, ovd->tell } );

	const vorbis_info* info = ov_info( &*ovf, -1 );

	channelNum		= info->channels;
	samplingRate	= info->rate;
	bitRate			= 16;	// 16/24
	playTime		= 1;	// sec

	wfmt.wFormatTag			= WAVE_FORMAT_PCM;
	wfmt.nChannels			= channelNum;
	wfmt.nSamplesPerSec		= samplingRate;
	wfmt.wBitsPerSample		= bitRate;
	wfmt.nBlockAlign		= channelNum * bitRate / 8;
	wfmt.nAvgBytesPerSec	= wfmt.nSamplesPerSec * wfmt.nBlockAlign;
	wfmt.cbSize				= 0;

	desc.dwSize				= sizeof( DSBUFFERDESC );
	desc.dwFlags			= DSBCAPS_CTRLPAN|DSBCAPS_CTRLVOLUME|DSBCAPS_CTRLFREQUENCY|DSBCAPS_GLOBALFOCUS|DSBCAPS_CTRLPOSITIONNOTIFY;
	desc.dwBufferBytes		= wfmt.nAvgBytesPerSec * playTime;
	desc.dwReserved			= 0;
	desc.lpwfxFormat		= &wfmt;
	desc.guid3DAlgorithm	= GUID_NULL;
}

//----------------------------------------
// 後始末
//----------------------------------------
void VORBIS::Free( void ){

	ov_clear( &*ovf );
	ovf = nullptr;
	ovd = nullptr;
}

//----------------------------------------
// バッファディスクリプター
//----------------------------------------
auto VORBIS::Desc( void ) const->DSBUFFERDESC{

	return desc;
}

//----------------------------------------
// セグメント読み込み
//----------------------------------------
void VORBIS::Seek( double msec ){

	ov_time_seek( &*ovf, 0.0 );
}

bool VORBIS::Segment( char* buff, DWORD size ) const {

	if ( buff && size ) {
		std::memset( buff, 0, size );

		constexpr auto	REQ_SIZE = 4096;

		UINT	reqs = ( REQ_SIZE < size ) ? REQ_SIZE : size;
		UINT	coms = 0;
		int 	bits;
		int		read;
		int		rest;

		while ( 1 ) {
			if ( read = ov_read( &*ovf, &buff[coms], reqs, 0, 2, 1, &bits ); !read ) {
				ov_time_seek( &*ovf, 0.0 );
				return false;
			} else if ( ( coms += read ) >= size ) {
				return true;
			} else if ( rest = size - coms; REQ_SIZE > rest ) {
				reqs = rest;
			}
		}
	}

	return false;
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
VORBISX::VORBISX(){}
VORBISX::~VORBISX(){}

//----------------------------------------
// リソース
//----------------------------------------
UINT VORBISX::Vorbiss( void ) const { return Resource();	}

void VORBISX::NowVorbis( UINT n ){ NowResource( n );	}
UINT VORBISX::NowVorbis( void   ) const { return NowResource();	}

void VORBISX::SetVorbis( const VORBIS* r, UINT n ){ SetResource( r, n );	}
void VORBISX::SetVorbis( const VORBIS* r         ){ SetResource( r    );	}
void VORBISX::SetVorbis( const VORBIS& r, UINT n ){ SetResource( r, n );	}
void VORBISX::SetVorbis( const VORBIS& r         ){ SetResource( r    );	}

const VORBIS* VORBISX::GetVorbis( UINT n ) const { return GetResource( n );	}
const VORBIS* VORBISX::GetVorbis( void   ) const { return GetResource(   );	}

// End Of File
