//========================================
// OPAL Game Library
//========================================
#include "call_md.hpp"
#include "Call/rsrc/music.hpp"

//========================================
// MUSICリソース
//========================================
using namespace opal;

//----------------------------------------
// constructor & destructor
//----------------------------------------
MUSIC::MUSIC() :
	performance( nullptr ),
	loader( nullptr ),
	segment( nullptr ),
	audiopath( nullptr ),
	segstate( nullptr ),
	volume( 0 ),
	time( 0 ),
	length( 0 )
{}
MUSIC::~MUSIC(){}

//----------------------------------------
// 初期化
//----------------------------------------
void MUSIC::Init( const void* pBuff, size_t vSize, const char* pName, const void* pDesc ){

	if ( !pBuff || !vSize ) {
		Init( pName );
		return;
	}

	file.Info( FILEX::Path(), pName, pBuff, vSize );

	::CoCreateInstance( CLSID_DirectMusicPerformance,
						nullptr,
						CLSCTX_INPROC,
						IID_IDirectMusicPerformance8,
						( void** )&performance );

	performance->InitAudio( nullptr,
							nullptr,
							WINDOWX::Handle(),
							DMUS_APATH_SHARED_STEREOPLUSREVERB,
							32,									// 同時再生数(wave=1,midi=16) 多めでもOK!
							DMUS_AUDIOF_ALL,
							nullptr );

	::CoCreateInstance( CLSID_DirectMusicLoader,
						nullptr,
						CLSCTX_INPROC,
						IID_IDirectMusicLoader8,
						( void** )&loader );

	DMUS_OBJECTDESC	desc = {};

    desc.dwSize			= sizeof( DMUS_OBJECTDESC );
	desc.dwValidData	= DMUS_OBJ_CLASS|DMUS_OBJ_MEMORY;
	//desc.guidObject	= ( GUID );
	desc.guidClass		= CLSID_DirectMusicSegment;
	//desc.ftDate		= ( FILETIME );
	//desc.vVersion		= ( DMUS_VERSION );
	//desc.wszName		= ( WCHAR )[DMUS_MAX_NAME];
	//desc.wszCategory	= ( WCHAR )[DMUS_MAX_CATEGORY];
	//desc.wszFileName	= ( WCHAR )[DMUS_MAX_FILENAME];
	desc.llMemLength	= file.Size();
	desc.pbMemData		= reinterpret_cast< BYTE* >( const_cast< void* >( file.Adrs() ) );
	//desc.pStream		= ( IStream* );

	loader->GetObject( &desc, IID_IDirectMusicSegment, ( void** )&segment );

	segment->SetParam( GUID_StandardMIDIFile, 0xffffffff, 0, 0, nullptr );
	segment->Download( performance );

	performance->CreateStandardAudioPath( DMUS_APATH_DYNAMIC_STEREO, 64, true, &audiopath );

	segstate	= nullptr;

	volume	= 0;
	time	= 0;
	segment->GetLength( &length );
}

//----------------------------------------
// 後始末
//----------------------------------------
void MUSIC::Free( void ){

	if ( segment ) {
		segment->Unload( performance );
		RELEASE( segment );
	}

	RELEASE( loader );

	if ( performance ) {
		performance->CloseDown();
		RELEASE( performance );
	}

	RELEASE( audiopath );
}

//----------------------------------------
// constructor & destructor
//----------------------------------------
MUSICX::MUSICX(){}
MUSICX::~MUSICX(){}

//----------------------------------------
// リソース
//----------------------------------------
UINT MUSICX::Musics( void ) const { return Resource();	}

void MUSICX::NowMusic( UINT n ){ NowResource( n );	}
UINT MUSICX::NowMusic( void   ) const { return NowResource();	}

void MUSICX::SetMusic( const MUSIC* r, UINT n ){ SetResource( r, n );	}
void MUSICX::SetMusic( const MUSIC* r         ){ SetResource( r    );	}
void MUSICX::SetMusic( const MUSIC& r, UINT n ){ SetResource( r, n );	}
void MUSICX::SetMusic( const MUSIC& r         ){ SetResource( r    );	}

const MUSIC* MUSICX::GetMusic( UINT n ) const { return GetResource( n );	}
const MUSIC* MUSICX::GetMusic( void   ) const { return GetResource(   );	}

// End Of File

