#================================================================================
# Makefile
#================================================================================
CC=C:/MinGW/bin/g++.exe
AR=C:/MinGW/bin/ar.exe
LD=C:/MinGW/bin/g++.exe
FX=C:/Program\ Files\ \(x86\)/Microsoft\ DirectX\ SDK\ \(June\ 2010\)/Utilities/bin/x64/fxc.exe
FP=C:/MinGW/bin/g++.exe

PAD = dinput8 xinput
DRAW = d3d11 d3dcompiler_47 D2d1 Dwrite Dwmapi #d3d9 d3dx9_43
CALL = dsound xaudio2_8
NOWARN = -Wno-float-equal -Wno-deprecated-declarations -Wno-sign-compare -Wno-unused-value -Wno-attributes
CHARSET = --input-charset=cp932 --exec-charset=cp932


CPPFLAGS := -Wall -Werror -std=c++2a -g -pg -DOPAL_DX11 -DOPAL_DEBUG -D_XM_NO_INTRINSICS_ -D_WIN32_WINNT=0xA00 -DOPAL_DX11 -DOPAL_HEAP -fno-exceptions -Wnoexcept -Wextra -Wformat=2 -Wcast-qual -Wcast-align -Wwrite-strings -Wfloat-equal -Wpointer-arith -Wno-missing-field-initializers -Wno-unused-parameter -Wno-multichar -Wno-unknown-pragmas -Wno-strict-aliasing -Wno-float-equal -Wno-attributes
LDFLAGS  := -pg
FXFLAGS  := /nologo /Gfp /Ges

INCPATH := -iquote -I-  -I../Opal/Inc -I../Opal/Inc/Etc -I../Opal/Inc/Lib -I../Opal/Inc/Sys -I../Opal/Inc/Usr -IInc -ILib -I../Opal/Inc/Usr/Draw/prim -I../Opal/Inc/Usr/Draw/rsrc -I../Opal/Inc/Usr/Draw/shader -I../Opal/Inc/Usr/Call/prim -I../Opal/Inc/Usr/Call/rsrc -IC:/MinGW/include/ -IC:/MinGW/include/DirectX -IC:/MinGW/include/DirectX/TK -IC:/MinGW/include/DirectX/Math -IC:/MinGW/include/ogg -IC:/MinGW/include/vorbis -I../Opal/Inc/../Lib/Game -I../Opal/Inc/../Lib/Game/drawprim -I.
LIBRARY := -luuid -lole32 -lwinmm -ldxguid $(foreach lib,$(PAD),-l$(lib)) $(foreach lib,$(DRAW),-l$(lib)) $(foreach lib,$(CALL),-l$(lib)) -lstrmiids -lquartz -lWSock32 -limm32 -luser32 -lgdi32 -lrpcrt4 -loleaut32 -lws2_32 -lshlwapi -lwindowscodecs 
DYNAMIC := 

EXES := Exe/main.exe
LIBS := Obj/libopal.a Obj/libcall.a Obj/libdraw_dx11.a Obj/libgame_dx11.a
RESS := 

#--------------------------------------------------------------------------------
# OPAL
#--------------------------------------------------------------------------------
OPAL=../Opal
OPAL_Exe=$(OPAL)/Exe
OPAL_OBJ=$(OPAL)/Obj
OPAL_LIB=$(OPAL)/Obj
OPAL_INC=$(OPAL)/Inc

#--------------------------------------------------------------------------------
# EXE
#--------------------------------------------------------------------------------
.PHONY:exe
exe:$(EXES)

#----------------------------------------
# Exe/main.exe
#----------------------------------------
EXE000_DEPEND := Obj/main.o

EXE000_OBJECT := ${filter %.o %.ro, $(EXE000_DEPEND)}
Exe/main.exe:$(EXE000_DEPEND) 
	@if [ ! -d Exe/ ]; then mkdir -p Exe/; fi
	$(LD) $(LDFLAGS) -mwindows -static-libgcc -static-libstdc++ -o $@  $(EXE000_OBJECT)  -L./Obj -L../Opal/Obj -lgame_dx11 -lgame -ldraw_dx11 -ldraw -ldxtk -lcall -lvorbis -logg -lopal $(LIBRARY)

Obj/main.o:main.cpp 
	@if [ ! -d Obj/ ]; then mkdir -p Obj/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 


#--------------------------------------------------------------------------------
# LIB
#--------------------------------------------------------------------------------
.PHONY:lib
lib:$(LIBS)

#----------------------------------------
# Obj/libopal.a
#----------------------------------------
LIB000_DEPEND := 

LIB000_OBJECT := ${filter %.o, $(LIB000_DEPEND)}
Obj/libopal.a:$(LIB000_DEPEND)
	$(AR) r $@ ${filter %.o %.ro, $?}
	$(AR) s $@


#----------------------------------------
# Obj/libcall.a
#----------------------------------------
LIB001_DEPEND := 

LIB001_OBJECT := ${filter %.o, $(LIB001_DEPEND)}
Obj/libcall.a:$(LIB001_DEPEND)
	$(AR) r $@ ${filter %.o %.ro, $?}
	$(AR) s $@


#----------------------------------------
# Obj/libdraw_dx11.a
#----------------------------------------
LIB002_DEPEND := Obj/dx11/Usr/Draw/rsrc/texture_dx11.o Obj/dx11/Usr/Draw/rsrc/texture_dx11_.o Obj/dx11/Usr/Draw/rsrc/surface_dx11.o Obj/dx11/Usr/Draw/rsrc/surface_dx11_.o Obj/dx11/Usr/Draw/rsrc/polygon_dx11.o Obj/dx11/Usr/Draw/rsrc/polygon_dx11_.o Obj/dx11/Usr/Draw/rsrc/polygon_dx11d.o Obj/dx11/Usr/Draw/prim/font_dx11.o Obj/dx11/Usr/Draw/prim/font_dx11_.o Usr/Draw/prim/vs_line2.cpp Usr/Draw/prim/ps_line2.cpp Obj/dx11/Usr/Draw/prim/linex_dx11.o Obj/dx11/Usr/Draw/prim/line2_dx11.o Obj/dx11/Usr/Draw/prim/linex_dx11_.o Obj/dx11/Usr/Draw/prim/line2_dx11_.o Usr/Draw/prim/vs_rect2.cpp Usr/Draw/prim/ps_rect2.cpp Usr/Draw/prim/vs_rect3.cpp Usr/Draw/prim/ps_rect3.cpp Usr/Draw/prim/ps_rectx.cpp Obj/dx11/Usr/Draw/prim/rectx_dx11.o Obj/dx11/Usr/Draw/prim/rect2_dx11.o Obj/dx11/Usr/Draw/prim/rect3_dx11.o Obj/dx11/Usr/Draw/prim/rectx_dx11_.o Obj/dx11/Usr/Draw/prim/rect2_dx11_.o Obj/dx11/Usr/Draw/prim/rect3_dx11_.o Obj/dx11/Usr/Draw/prim/shape_dx11.o Obj/dx11/Usr/Draw/prim/shape_dx11_.o Usr/Draw/prim/vs_shape.cpp Usr/Draw/prim/ps_shape.cpp Obj/dx11/Usr/Draw/prim/gdi_dx11.o Obj/dx11/Usr/Draw/prim/gdi_dx11_.o Obj/dx11/Usr/Draw/prim/waku2.o Obj/dx11/Usr/Draw/prim/maru2.o

LIB002_OBJECT := ${filter %.o, $(LIB002_DEPEND)}
Obj/libdraw_dx11.a:$(LIB002_DEPEND)
	$(AR) r $@ ${filter %.o %.ro, $?}
	$(AR) s $@

Obj/dx11/Usr/Draw/rsrc/texture_dx11.o:Usr/Draw/rsrc/texture_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/texture_dx11_.o:Usr/Draw/rsrc/texture_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/surface_dx11.o:Usr/Draw/rsrc/surface_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/surface_dx11_.o:Usr/Draw/rsrc/surface_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/polygon_dx11.o:Usr/Draw/rsrc/polygon_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/polygon_dx11_.o:Usr/Draw/rsrc/polygon_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/rsrc/polygon_dx11d.o:Usr/Draw/rsrc/polygon_dx11d.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_shape.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/rsrc/ ]; then mkdir -p Obj/dx11/Usr/Draw/rsrc/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/font_dx11.o:Usr/Draw/prim/font_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/font_dx11_.o:Usr/Draw/prim/font_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@   $(NOWARN) $(INCPATH) 

Obj/dx11/Usr/Draw/prim/linex_dx11.o:Usr/Draw/prim/linex_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/line2_dx11.o:Usr/Draw/prim/line2_dx11.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_line2.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/linex_dx11_.o:Usr/Draw/prim/linex_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/line2_dx11_.o:Usr/Draw/prim/line2_dx11_.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_line2.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rectx_dx11.o:Usr/Draw/prim/rectx_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rect2_dx11.o:Usr/Draw/prim/rect2_dx11.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_rect2.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rect3_dx11.o:Usr/Draw/prim/rect3_dx11.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_rect3.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rectx_dx11_.o:Usr/Draw/prim/rectx_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rect2_dx11_.o:Usr/Draw/prim/rect2_dx11_.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_rect2.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/rect3_dx11_.o:Usr/Draw/prim/rect3_dx11_.cpp ${foreach var,v p,${subst ?,$(var),Usr/Draw/prim/?s_rect3.hpp}}
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/shape_dx11.o:Usr/Draw/prim/shape_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/shape_dx11_.o:Usr/Draw/prim/shape_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/gdi_dx11.o:Usr/Draw/prim/gdi_dx11.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/gdi_dx11_.o:Usr/Draw/prim/gdi_dx11_.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/waku2.o:Usr/Draw/prim/waku2.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Usr/Draw/prim/maru2.o:Usr/Draw/prim/maru2.cpp 
	@if [ ! -d Obj/dx11/Usr/Draw/prim/ ]; then mkdir -p Obj/dx11/Usr/Draw/prim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Usr/Draw/prim/vs_line2.cpp:Usr/Draw/prim/vs_line2.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/vs_line2.fxi
	$(FX) Usr/Draw/prim/vs_line2.fxi $(FXFLAGS) /T  /E vs_line2 /Vn vs_line2 /Fh Usr/Draw/prim/vs_line2.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/vs_line2.hpp
	@echo -e "extern const unsigned char vs_line2[];"   > Usr/Draw/prim/vs_line2.tmp
	@echo -e "extern const unsigned int  vs_line2_size;" >> Usr/Draw/prim/vs_line2.tmp
	@cat Usr/Draw/prim/vs_line2.tmp >> Usr/Draw/prim/vs_line2.hpp
	@cat Usr/Draw/prim/vs_line2.cpp >> Usr/Draw/prim/vs_line2.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/vs_line2.tmp > Usr/Draw/prim/vs_line2.cpp
	@echo -e "const unsigned int vs_line2_size = sizeof( vs_line2 );" >> Usr/Draw/prim/vs_line2.cpp
	@rm --force Usr/Draw/prim/vs_line2.tmp
	@rm --force Usr/Draw/prim/vs_line2.fxi

Usr/Draw/prim/ps_line2.cpp:Usr/Draw/prim/ps_line2.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/ps_line2.fxi
	$(FX) Usr/Draw/prim/ps_line2.fxi $(FXFLAGS) /T  /E ps_line2 /Vn ps_line2 /Fh Usr/Draw/prim/ps_line2.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/ps_line2.hpp
	@echo -e "extern const unsigned char ps_line2[];"   > Usr/Draw/prim/ps_line2.tmp
	@echo -e "extern const unsigned int  ps_line2_size;" >> Usr/Draw/prim/ps_line2.tmp
	@cat Usr/Draw/prim/ps_line2.tmp >> Usr/Draw/prim/ps_line2.hpp
	@cat Usr/Draw/prim/ps_line2.cpp >> Usr/Draw/prim/ps_line2.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/ps_line2.tmp > Usr/Draw/prim/ps_line2.cpp
	@echo -e "const unsigned int ps_line2_size = sizeof( ps_line2 );" >> Usr/Draw/prim/ps_line2.cpp
	@rm --force Usr/Draw/prim/ps_line2.tmp
	@rm --force Usr/Draw/prim/ps_line2.fxi

Usr/Draw/prim/vs_rect2.cpp:Usr/Draw/prim/vs_rect2.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/vs_rect2.fxi
	$(FX) Usr/Draw/prim/vs_rect2.fxi $(FXFLAGS) /T  /E vs_rect2 /Vn vs_rect2 /Fh Usr/Draw/prim/vs_rect2.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/vs_rect2.hpp
	@echo -e "extern const unsigned char vs_rect2[];"   > Usr/Draw/prim/vs_rect2.tmp
	@echo -e "extern const unsigned int  vs_rect2_size;" >> Usr/Draw/prim/vs_rect2.tmp
	@cat Usr/Draw/prim/vs_rect2.tmp >> Usr/Draw/prim/vs_rect2.hpp
	@cat Usr/Draw/prim/vs_rect2.cpp >> Usr/Draw/prim/vs_rect2.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/vs_rect2.tmp > Usr/Draw/prim/vs_rect2.cpp
	@echo -e "const unsigned int vs_rect2_size = sizeof( vs_rect2 );" >> Usr/Draw/prim/vs_rect2.cpp
	@rm --force Usr/Draw/prim/vs_rect2.tmp
	@rm --force Usr/Draw/prim/vs_rect2.fxi

Usr/Draw/prim/ps_rect2.cpp:Usr/Draw/prim/ps_rect2.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/ps_rect2.fxi
	$(FX) Usr/Draw/prim/ps_rect2.fxi $(FXFLAGS) /T  /E ps_rect2 /Vn ps_rect2 /Fh Usr/Draw/prim/ps_rect2.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/ps_rect2.hpp
	@echo -e "extern const unsigned char ps_rect2[];"   > Usr/Draw/prim/ps_rect2.tmp
	@echo -e "extern const unsigned int  ps_rect2_size;" >> Usr/Draw/prim/ps_rect2.tmp
	@cat Usr/Draw/prim/ps_rect2.tmp >> Usr/Draw/prim/ps_rect2.hpp
	@cat Usr/Draw/prim/ps_rect2.cpp >> Usr/Draw/prim/ps_rect2.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/ps_rect2.tmp > Usr/Draw/prim/ps_rect2.cpp
	@echo -e "const unsigned int ps_rect2_size = sizeof( ps_rect2 );" >> Usr/Draw/prim/ps_rect2.cpp
	@rm --force Usr/Draw/prim/ps_rect2.tmp
	@rm --force Usr/Draw/prim/ps_rect2.fxi

Usr/Draw/prim/vs_rect3.cpp:Usr/Draw/prim/vs_rect3.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/vs_rect3.fxi
	$(FX) Usr/Draw/prim/vs_rect3.fxi $(FXFLAGS) /T  /E vs_rect3 /Vn vs_rect3 /Fh Usr/Draw/prim/vs_rect3.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/vs_rect3.hpp
	@echo -e "extern const unsigned char vs_rect3[];"   > Usr/Draw/prim/vs_rect3.tmp
	@echo -e "extern const unsigned int  vs_rect3_size;" >> Usr/Draw/prim/vs_rect3.tmp
	@cat Usr/Draw/prim/vs_rect3.tmp >> Usr/Draw/prim/vs_rect3.hpp
	@cat Usr/Draw/prim/vs_rect3.cpp >> Usr/Draw/prim/vs_rect3.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/vs_rect3.tmp > Usr/Draw/prim/vs_rect3.cpp
	@echo -e "const unsigned int vs_rect3_size = sizeof( vs_rect3 );" >> Usr/Draw/prim/vs_rect3.cpp
	@rm --force Usr/Draw/prim/vs_rect3.tmp
	@rm --force Usr/Draw/prim/vs_rect3.fxi

Usr/Draw/prim/ps_rect3.cpp:Usr/Draw/prim/ps_rect3.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/ps_rect3.fxi
	$(FX) Usr/Draw/prim/ps_rect3.fxi $(FXFLAGS) /T  /E ps_rect3 /Vn ps_rect3 /Fh Usr/Draw/prim/ps_rect3.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/ps_rect3.hpp
	@echo -e "extern const unsigned char ps_rect3[];"   > Usr/Draw/prim/ps_rect3.tmp
	@echo -e "extern const unsigned int  ps_rect3_size;" >> Usr/Draw/prim/ps_rect3.tmp
	@cat Usr/Draw/prim/ps_rect3.tmp >> Usr/Draw/prim/ps_rect3.hpp
	@cat Usr/Draw/prim/ps_rect3.cpp >> Usr/Draw/prim/ps_rect3.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/ps_rect3.tmp > Usr/Draw/prim/ps_rect3.cpp
	@echo -e "const unsigned int ps_rect3_size = sizeof( ps_rect3 );" >> Usr/Draw/prim/ps_rect3.cpp
	@rm --force Usr/Draw/prim/ps_rect3.tmp
	@rm --force Usr/Draw/prim/ps_rect3.fxi

Usr/Draw/prim/ps_rectx.cpp:Usr/Draw/prim/ps_rectx.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/ps_rectx.fxi
	$(FX) Usr/Draw/prim/ps_rectx.fxi $(FXFLAGS) /T  /E ps_rectx /Vn ps_rectx /Fh Usr/Draw/prim/ps_rectx.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/ps_rectx.hpp
	@echo -e "extern const unsigned char ps_rectx[];"   > Usr/Draw/prim/ps_rectx.tmp
	@echo -e "extern const unsigned int  ps_rectx_size;" >> Usr/Draw/prim/ps_rectx.tmp
	@cat Usr/Draw/prim/ps_rectx.tmp >> Usr/Draw/prim/ps_rectx.hpp
	@cat Usr/Draw/prim/ps_rectx.cpp >> Usr/Draw/prim/ps_rectx.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/ps_rectx.tmp > Usr/Draw/prim/ps_rectx.cpp
	@echo -e "const unsigned int ps_rectx_size = sizeof( ps_rectx );" >> Usr/Draw/prim/ps_rectx.cpp
	@rm --force Usr/Draw/prim/ps_rectx.tmp
	@rm --force Usr/Draw/prim/ps_rectx.fxi

Usr/Draw/prim/vs_shape.cpp:Usr/Draw/prim/vs_shape.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/vs_shape.fxi
	$(FX) Usr/Draw/prim/vs_shape.fxi $(FXFLAGS) /T  /E vs_shape /Vn vs_shape /Fh Usr/Draw/prim/vs_shape.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/vs_shape.hpp
	@echo -e "extern const unsigned char vs_shape[];"   > Usr/Draw/prim/vs_shape.tmp
	@echo -e "extern const unsigned int  vs_shape_size;" >> Usr/Draw/prim/vs_shape.tmp
	@cat Usr/Draw/prim/vs_shape.tmp >> Usr/Draw/prim/vs_shape.hpp
	@cat Usr/Draw/prim/vs_shape.cpp >> Usr/Draw/prim/vs_shape.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/vs_shape.tmp > Usr/Draw/prim/vs_shape.cpp
	@echo -e "const unsigned int vs_shape_size = sizeof( vs_shape );" >> Usr/Draw/prim/vs_shape.cpp
	@rm --force Usr/Draw/prim/vs_shape.tmp
	@rm --force Usr/Draw/prim/vs_shape.fxi

Usr/Draw/prim/ps_shape.cpp:Usr/Draw/prim/ps_shape.fx 
	@if [ ! -d Usr/Draw/prim/ ]; then mkdir -p Usr/Draw/prim/; fi
	@$(FP) -w -E    - < $< | sed -e 's/^#.*/\n/' > Usr/Draw/prim/ps_shape.fxi
	$(FX) Usr/Draw/prim/ps_shape.fxi $(FXFLAGS) /T  /E ps_shape /Vn ps_shape /Fh Usr/Draw/prim/ps_shape.cpp
	@echo -e "#pragma once" > Usr/Draw/prim/ps_shape.hpp
	@echo -e "extern const unsigned char ps_shape[];"   > Usr/Draw/prim/ps_shape.tmp
	@echo -e "extern const unsigned int  ps_shape_size;" >> Usr/Draw/prim/ps_shape.tmp
	@cat Usr/Draw/prim/ps_shape.tmp >> Usr/Draw/prim/ps_shape.hpp
	@cat Usr/Draw/prim/ps_shape.cpp >> Usr/Draw/prim/ps_shape.tmp
	@sed "s/BYTE/unsigned char/" Usr/Draw/prim/ps_shape.tmp > Usr/Draw/prim/ps_shape.cpp
	@echo -e "const unsigned int ps_shape_size = sizeof( ps_shape );" >> Usr/Draw/prim/ps_shape.cpp
	@rm --force Usr/Draw/prim/ps_shape.tmp
	@rm --force Usr/Draw/prim/ps_shape.fxi


#----------------------------------------
# Obj/libgame_dx11.a
#----------------------------------------
LIB003_DEPEND := Obj/dx11/Lib/Game/drawprim/drawprim_color.o Obj/dx11/Lib/Game/drawprim/drawprim_line.o Obj/dx11/Lib/Game/drawprim/drawprim_rect.o Obj/dx11/Lib/Game/drawprim/drawprim_circle.o Obj/dx11/Lib/Game/drawprim/drawprim_star.o Obj/dx11/Lib/Game/drawprim/drawprim_font.o Obj/dx11/Lib/Game/drawprim/drawprim_image.o Obj/dx11/Lib/Game/drawprim/drawprim_texture.o Obj/dx11/Lib/Game/drawprim/drawprim_vram.o

LIB003_OBJECT := ${filter %.o, $(LIB003_DEPEND)}
Obj/libgame_dx11.a:$(LIB003_DEPEND)
	$(AR) r $@ ${filter %.o %.ro, $?}
	$(AR) s $@

Obj/dx11/Lib/Game/drawprim/drawprim_color.o:Lib/Game/drawprim/drawprim_color.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_line.o:Lib/Game/drawprim/drawprim_line.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_rect.o:Lib/Game/drawprim/drawprim_rect.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_circle.o:Lib/Game/drawprim/drawprim_circle.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_star.o:Lib/Game/drawprim/drawprim_star.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_font.o:Lib/Game/drawprim/drawprim_font.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_image.o:Lib/Game/drawprim/drawprim_image.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_texture.o:Lib/Game/drawprim/drawprim_texture.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 

Obj/dx11/Lib/Game/drawprim/drawprim_vram.o:Lib/Game/drawprim/drawprim_vram.cpp 
	@if [ ! -d Obj/dx11/Lib/Game/drawprim/ ]; then mkdir -p Obj/dx11/Lib/Game/drawprim/; fi
	$(CC)  $< $(CPPFLAGS) -c -o $@    $(INCPATH) 


#--------------------------------------------------------------------------------
# RES
#--------------------------------------------------------------------------------
.PHONY:res
res:$(RESS)



#================================================================================
# EOF
#================================================================================
