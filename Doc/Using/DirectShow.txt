Microsoft Windows SDK for Windows 7 and .NET Framework 4 をインストールする。
⇒winsdk_web.exe

https://msdn.microsoft.com/ja-jp/library/cc371169.aspx

C:\Program Files\Microsoft SDKs\Windows\v6.1\Samples\Multimedia\DirectShow\BaseClasses

自前でビルドしてライブラリを作成する必要がある。。。

//----------------------------------------
// VisualStudio2015
//----------------------------------------
□strmbase.libのビルド
C:\Program Files\Microsoft SDKs\Windows\v6.1\Samples\Multimedia\DirectShow\BaseClasses\にある
baseclasses.sln(VS2010)を起動するとVS2015が自動的に変換してくれる。

設定が32bit(x86)なので64bit(x64)にする

デバッグ版だとstrmbas[d]になる。

□C:\Program Files\Microsoft SDKs\Windows\v6.0\Include\qedit.h
※:最新SDKではDirectShowの仕様変更対応の為、qedit.hに以下の編集を行わなければビルドできない。

498行目はライン全体のコメントアウト、その他はIDXEffect基底クラスから の派生を定義しない為のライン途中からのコメントアウト

qedit.h 498: //#include "dxtrans.h"
qedit.h 837: IDxtCompositor //: public IDXEffect
qedit.h 1151: IDxtAlphaSetter //: public IDXEffect
qedit.h 1345: IDxtJpeg //: public IDXEffect
qedit.h 1735: IDxtKey //: public IDXEffect

参考 http://social.msdn.microsoft.com/forums/en-US/windowssdk/thread/ed097d2c-3d68-4f48-8448-277eaaf68252/

#pragma message("....")がうざいのでコメントアウトする


□C:\Program files\Microsoft SDKs\Windows\v6.0\Include\uuids.h\
#pragma once を追加


□C:\Program Files\Microsoft SDKs\Windows\v6.0\Samples\Multimedia\DirectShow\BaseClasses\reftime.h
どこかで#defineで定義されている...
#ifndef NANOSECONDS
const LONGLONG NANOSECONDS = (1000000000);       // 10 ^ 9
#endif


□C:\Program Files\Microsoft SDKs\Windows\v6.0\Samples\Multimedia\DirectShow\BaseClasses\refclock.h
CAMSchedule が未定義？
http://blogs.yahoo.co.jp/gun534/49865539.html

#include <Schedule.h> → #include "Schedule.h"

□C:\Program Files\Microsoft SDKs\Windows\v6.0\Samples\Multimedia\DirectShow\BaseClasses\streams.h
cl.exeでビルドすると
uuids.h が InitGuid.hと被ってしまう。

とりあえず防止

#ifndef INITGUID
#include <uuids.h>
#endif

//----------------------------------------
// MinGW64
//----------------------------------------
↓未定義
C:\MinGW\include/strmif.h:892:2: エラー: 'IBaseFilter' does not name a type

↓C:\Program Files\Microsoft SDKs\Windows\v6.0\Include\strmif.hから移植
#ifndef __IBaseFilter_FWD_DEFINED__
#define __IBaseFilter_FWD_DEFINED__
typedef interface IBaseFilter IBaseFilter;
#endif 	/* __IBaseFilter_FWD_DEFINED__ */

↓未定義
C:\MinGW\include/strmif.h:991:2: エラー: 'LPDDPIXELFORMAT' does not name a type

↓dshow.hの前に置く
#include <ddraw.h>

↓多重定義
C:\MinGW\include\directx/ddraw.h:262:3: エラー: conflicting declaration 'typedef struct _DDCOLORKEY DDCOLORKEY'
C:\MinGW\include/strmif.h:737:3: エラー: 'DDCOLORKEY' has a previous declaration as 'typedef struct DDCOLORKEY DDCOLORKEY'

typedef struct _DDCOLORKEY
{
    DWORD       dwColorSpaceLowValue;   // low  boundary of color space that is to be treated as Color Key, inclusive
    DWORD       dwColorSpaceHighValue;  // high boundary of color space that is to be treated as Color Key, inclusive
} DDCOLORKEY;

↓strmif.hの方をコメントアウト
typedef struct {
	DWORD dw1;
	DWORD dw2
} DDCOLORKEY;


↓なんだこれ...文法ミス？
C:\MinGW\include/amvideo.h:56:2: エラー: メンバ宣言の最後には ';' が予期されます
C:\MinGW\include/amvideo.h:56:7: エラー: 'rcTarget' does not name a
type

↓, を ; に修正
typedef struct tagVIDEOINFO {
	RECT rcSource,
	RECT rcTarget,
	DWORD dwBitRate,
	DWORD dwBitErrorRate,

↓未定義
./Inc/Sys/Call/rsrc/show.hpp:26:2: エラー: 'IGraphBuilder' does not name a type
↓C:\Program Files\Microsoft Platform SDK\Include\strmif.hから移植
#ifndef __IGraphBuilder_FWD_DEFINED__
#define __IGraphBuilder_FWD_DEFINED__
typedef interface IGraphBuilder IGraphBuilder;
#endif 	/* __IGraphBuilder_FWD_DEFINED__ */

↓未定義
Sys/Call/rsrc/show.cpp:32:8: エラー: 'IID_IGraphBuilder' was not declared in this scope

↓自前定義
#ifndef _MSC_VER
const GUID IID_IGraphBuilder = { 0x56a868a9, 0x0ad4, 0x11ce, { 0xb0, 0x3a, 0x00, 0x20, 0xaf, 0x0b, 0xa7, 0x70 } };
const GUID CLSID_FilterGraph = { 0xe436ebb3, 0x524f, 0x11ce, { 0x9f, 0x53, 0x00, 0x20, 0xaf, 0x0b, 0xa7, 0x70 } };
#endif

↓C_ASSERTの無効化
C:\MinGW\include\directx\SDK\intsafe.h
#define SORTPP_PASS // 定義する
#ifndef SORTPP_PASS 
// compiletime asserts (failure results in error C2118: negative subscript)
#define C_ASSERT(e) typedef char __C_ASSERT__[(e)?1:-1]
#else
#define C_ASSERT(e)
#endif

