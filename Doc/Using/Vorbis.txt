■OGG/VORBIS
https://xiph.org/ の Downloadから libogg と libvorbis 安定最新版を持ってくる。

■対象ファイル
ogg
src/framing.c
src/bitwise.c

vorbis
lib/vorbisfile.c	
lib/info.c		
lib/synthesis.c	
lib/block.c		
lib/sharedbook.c	
lib/psy.c		
lib/codebook.c	
lib/mdct.c		
lib/smallft.c	
lib/registry.c	
lib/envelope.c	
lib/bitrate.c	
lib/lpc.c		
lib/window.c		
lib/floor0.c		
lib/floor1.c		
lib/res0.c		
lib/mapping0.c	
lib/lsp.c		

■mk.cnfの設定
対象ファイルをライブラリ作成で指定。

Obj/ogg:
	src/framing.c	$(NOWARN)
	src/bitwise.c	$(NOWARN)

Obj/vorbis:
	lib/vorbisfile.c	$(NOWARN)
		:

$(NOWARN) は警告がうるさいので抑制のため。
!ms $NOWARN=-Wno-float-equal -Wno-deprecated-declarations -Wno-sign-compare -Wno-unused-value -Wno-attributes
=ms $NOWARN=/D _CRT_SECURE_NO_WARNINGS

動作確認用には LIB に call と 上記で作成される ogg と vorbis を指定。

Exe/main.exe: LIB=call ogg vorbis opal
	main.cpp				$(NOWARN)

インクルードパスに Ogg/Vorbis 関連のヘッダを追記。

INCLUDE=\
@ @/Etc @/Lib @/Sys @/Usr @/Lib/Usr Inc/ Lib/	\
C:/MinGW/include/								\
C:/MinGW/include/ogg							\
C:/MinGW/include/vorbis							\


■ビルド
void* からのポインタの変換は、g++ は -fpermissive で回避出来るが、cl は明示的にキャストしないとダメ。

ogg
src/framing.c

cl 用に下記を追加
#ifdef _MSC_VER
#include <cstdint>
typedef std::uint64_t ogg_uint64_t;
#endif

vorbis
C++ の予約語が使われているので書き換え。

vorbis/backend.h	class
vorbis/codebook.c	this
vorbis/envelope.c	this
vorbis/floor1.c	    class
vorbis/lsp.c	    new
vorbis/mapping0.c	class
vorbis/res0.c		this

使用しない可能性があるので対処。
include/vorbis/vorbisfile.h
[[maybe_unused]]static ov_callbacks OV_CALLBACKS_DEFAULT
[[maybe_unused]]static ov_callbacks OV_CALLBACKS_NOCLOSE
[[maybe_unused]]static ov_callbacks OV_CALLBACKS_STREAMONLY
[[maybe_unused]]static ov_callbacks OV_CALLBACKS_STREAMONLY_NOCLOSE

C++だと const が内部リンケージになるので定義側にもexternが必要
vorbis/floor0.c:extern const vorbis_func_floor floor0_exportbundle;
vorbis/floor1.c:extern const vorbis_func_floor floor1_exportbundle;
vorbis/res0.c:extern const vorbis_func_residue residue0_exportbundle;
vorbis/res0.c:extern const vorbis_func_residue residue1_exportbundle;
vorbis/res0.c:extern const vorbis_func_residue residue2_exportbundle;
vorbis/mapping0.c:extern const vorbis_func_mapping mapping0_exportbundle;

■ヘッダー/ライブラリ
作成したライブラリを C:\cygwin\lib\ogg|vorbis にそれぞれコピー。

ogg
include/ogg/ogg.h
include/ogg/os_types.h

vorbis
include/vorbis/codec.h
include/vorbis/vorbisfile.h

g++	: libogg.a		libvorbis.a
cl	: libogg.lib	libvorbis.lib
