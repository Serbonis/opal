msc.hpp
	C4062を追加
---
Lib/Std/sincos.hpp
	define → constexpr に変更
----------------------------------------
inc/Opal.hpp
Sys/Opal.cpp
inc/sys/opal.hpp
	sysfunc を sysinit/sysexec/sysfree に別々にusing
	sysexec を float 引数で deltatime を渡すのも検討(とりあえずやらない)
----------------------------------------
OPAL::FUNCの引数でデフォルト設定する方法は
グローバル変数の初期化順番の微妙な問題をはらんでいるようなので廃止
----------------------------------------
Usr/Draw/prim/font_dx.cpp
デフォルトを hello,world から hello world に変更
----------------------------------------
Usr/Draw/rsrc/texture_dx11_.cpp
#include "WICTextureLoader.h"
↓
#include "WICTextureLoader11.h"
----------------------------------------
Inc/Usr/Draw/prim/font/font_dx.hpp
const std::string& に対応
----------------------------------------
Inc/Sys/drawx.hpp
DrawAnime( UINT )
↓
DrawAnime( int )

Sys/drawu.hpp
Anime off でも DrawAnime(-1)がコールされるように変更

Inc/Sys/drawa.hpp
anime_time へのアクセッサが無かったので追加
anime_time/anime_seek/anime_rate には Add を追加
anime_time のみ参照取得も追加
Lib/Usr/anime が独自に持っていた anime_time は廃止

よくわからん Animation メンバ関数を廃止

virtual bool Animation( void ){ return false; }

オーバロード前提なのだろうけどシステムでは呼んでいない...
意味が出たらまた復活させればいいや
----------------------------------------
mkの使用方法を mk.txt にまとめる
-h/--helpオプションを追加
----------------------------------------
リポジトリの復元？
git clone --mirror https://Serbonis@bitbucket.org/Serbonis/opal.git
cd opal.git
git push origin
----------------------------------------
Lib/Utl/degree にちょっと機能追加

DixRadian
DioRadian
DixDegree
DioDegree
NormalRadian
NormalDegree

constexpr 化をすすめる
----------------------------------------
Lib/Ext/coordinate

AngleVector を追加
----------------------------------------
inc/lib.hpp

#include <numbers> を追加...C++20だったのでダメ
----------------------------------------
inc/new.hpp

PlacementNew を追加
----------------------------------------
inc/sys/{obj,work,draw,call}.hpp
sys/{obj,work,draw,call}s.cpp

Linx( LINX* );
Linx( LINX& );
Linx( std::shared_ptr<LINX> );

で整備する
----------------------------------------
inc/sys/{work,draw,call}.hpp
sys/{work,draw,call}.cpp
sys/{work,draw,call}s.cpp
sys/{work,draw,call}t.cpp

Parent( ZOBJ* );
Parent( ZOBJ& );
Parent( std::shared_ptr<ZOBJ> );

を整備する
----------------------------------------
time.{cpp,hpp}
System.cpp
SysInit/SysExec/SysFreeを追加

SysExecで登録したカウンターを自動更新する。

SetFrame
GetFrame
----------------------------------------
Lib/Exc/coordinate.{cpp,hpp}
VectorAngle/AngleVector を2次元と3次元を名前で区別
----------------------------------------
Sys/time.{cpp,hpp}

SECOND/MINUTE を UINT から int へ変更
----------------------------------------
opal.{cpp,hpp}
system.cpp
pad.{cpp,hpp}
padx.cpp
padz.cpp

倍速判定のところに間違いあり
sysfunc の判定をTrigとPushができるように sysflagを追加
乱数種のところとあわせてキーコンフィグできるように修正
----------------------------------------
atari_s.cpp
当たりクライアントの候補を名前キーのmapで持っていた間違いを修正
単純にクライアントのポインターをvectorに突っ込む
----------------------------------------
time.{cpp,hpp}
Frame の進行を制御できるようにする
----------------------------------------
objt.cpp
UpdaterをSetで削除するとループ内の場合にこけるので
関数がすべてnullptrで遅延削除するように変更
----------------------------------------
font_dx.{cpp,hpp}
Clear() を追加
----------------------------------------
surface_dx11.{cpp.hpp}

SURAFCE_LOCK に関数追加
Data()
Pitch()
Depth()
Width()
Height()
SetPixel()
GetPixel()
----------------------------------------
Lib/Game/dframe.{cpp,hpp}

CFSURFACE に COLOR を継承させる
これはRECTの SetColor を無効化するための便法＆ Clear へ転送
Clear を直接呼べないようにした

RFSURFACEもそうだが、SetTexure もとりあえず無効化する

CFSURFACE に SetPixel を持たせて、SURFACE_LOCKに転送
いろいろあって const 化が波及した。
----------------------------------------
Sys/Attr/color.{cpp.hpp}
SetColor_/GetColor_ を private から public へ
----------------------------------------
Lib/Std/variable.{cpp,hpp}を非推奨に
----------------------------------------
Inc/Lib/Std/thing.hppを非推奨に
→Lib/Game/scene.hpp から利用クラスを削除
----------------------------------------
Lib/Std/glyph.{cpp,hpp}を新規作成
----------------------------------------
Inc/Sys/padr.hpp
PADR のコンストラクタから explicit を削除
----------------------------------------
objx.hpp
objt.cpp
Updaterの string を string& に修正
バグチェック用関数 UpdaterDebug を追加
----------------------------------------
drawx.hpp
DRAWL の DRAW2L/DRAW3L 拡張版も作成
----------------------------------------
Lib/Game/move/mover.{hpp,cpp}

メソッドを追加

SetPromoterPosition( TYPE, float, float );
SetPromoterPosition( float, float, TYPE );
----------------------------------------
Lib/Game/promoter/promoter.{hpp,cpp}

ポーズ機能を追加
Playメソッドを追加
----------------------------------------
カラー名のために Lib/ext/color.cpp を作成
----------------------------------------
Usr/Draw/rsrc/surface_dx11_.{hpp,cpp}

RnderTargetView と  DepthStencilView を Bgin-End でシステムのそれに戻していた...
スタック状態になよう「現在設定されている値」を退避して戻すように修正
dx9はそうなっていた...
----------------------------------------
Sys/drawx.{hpp,cpp}
Sys/drawu.cpp
(WORK|DRAW|CALL)_STATE_BOOT を廃止

最初の1ループをスルーするとワンタイムのオブジェ実施ができなくなるので...
----------------------------------------
Sys/Obj/zobj.{hpp,cpp}

Parent と対で Child を作成。
----------------------------------------
Sys/rsrc{x.hpp,t.cpp}
Init の string を string& に変更
あわせて nullptr_t 対応を追加
----------------------------------------
Lib/Std/str_s.{hpp,cpp}

itos をとりあえず作成...
----------------------------------------
Inc/Lib/Std/interface.hpp を廃止(残すけど)
Usr/Draw/rsrc/texture_dx.inl を修正
----------------------------------------
Inc/Sys/drawx.hpp
Sys/draw.cpp
Sys/drawt.cpp
関数型 Draw() への対応

public:
	using DTP = std::shared_ptr<DRAWT>;
	using DTF = std::function<void(DRAWT*)>;

	static void Draw( DTP, UINT, DTF = nullptr );
	static void Draw( DTP,       DTF = nullptr );

private:
	static void drawInit( void );
	static void drawExec( UINT );
	static void drawFree( void );
	static std::vector<std::pair<DTP,DTF>>	draw[2];

RECT2,FONT に static Draw を追加
----------------------------------------
FONT の char* buff を std::vector<char> に修正
----------------------------------------
CALLT も 関数型 Call() への対応

関連のコードを callo.cpp に集約。
DRAWT も drawo.cpp に集約。
----------------------------------------
{Draw,Call}/{prim,rsrc} をインクルードパスに入れて指定を省略することにした
ゲームコードには明示的につける方向
----------------------------------------
Lib/*/*.hpp は Lib は冗長なので */*.hpp に修正
----------------------------------------
Draw/prim/line系列を大改修
LINEで作るDOTを追加
そのうちサイズ変更できるようにしよう...
----------------------------------------
Sys/files.cpp
FILX::Path のカレントディレクトリのパス指定の末尾の / を自由にした
FILX::Path( p, f ) 形式を追加
----------------------------------------
Inc/Sys/opal.hpp
OPAL::FUNC のコンストラクタを public から　protected に修正
----------------------------------------
Sys/srvcs.cpp

UINT SRVCS::Destroy( const char* k );

k がオブジェクトの中から引っ張ている場合に
オブジェクト破棄(s->Clear())で無効になる場合がある...

s->End();
s->Clear();			// オブジェを破棄している(ことがある)
SRVCX::Clear( k );	// マップ削除の際に k が無効

↓

SRVCX::Clear( k );	// 先にマップから削除
s->End();
s->Clear();
----------------------------------------
Lib/Game/scene.{cpp,hpp}

大改造...マネージャーも作った
----------------------------------------
Lib/Game/object.{hpp.cpp} を廃止
あわせて scene も改造
----------------------------------------
rsrc と prim はシステム管理から外して機能の提供だけにする
----------------------------------------
Lib/Ext/color.{cpp,hpp}

名前→カラーコード関数を追加
----------------------------------------
LINE2,MARU2 に static Draw を追加
----------------------------------------
PRIMS::SharedPrim から PRIMS::MakeSharedPrim を拡張
----------------------------------------
ARGS&&... a と std::forward<ARGS>(a)... をとりあえず統一する
ユニバーサル参照むずかしい...
----------------------------------------
Lib/Std/guid.{cpp,hpp}

const char* から std::string に変更
----------------------------------------
↑この影響で Lib/Game/scene.cppを修正
連動して SRVCX::Search に nullガード追加
----------------------------------------
Lib/Usr を廃止して Lib/Game に統合
Lib/Game は全体的に見直し
----------------------------------------
Lib/Utl/collision
SHAPE から std::any でサイズ情報を返す
当たりタイプを UINT から enum に修正
連動して Lib/Game/atari を修正
----------------------------------------
Lib/Utl/collision を Lib/Game/atari に統合
----------------------------------------
Lib/Game/anime を廃止
----------------------------------------
Lib/Game/mover を Lib/Game/promoter に統合
----------------------------------------
Lib/Game/fsm
アクション実行中でもアクターが削除できるように対応
戻り値コードに ONECE と END を追加
----------------------------------------
Lib/Game/fsm
アクションのワークをアクターから取得できるように Work メソッドを追加
----------------------------------------
Lib/Game/behavior
ビヘイビアツリーを新規追加
----------------------------------------
OBJT::~OBJT(){} // TASK::UnSelf(); }

OBJTのデストラクタの処理を廃止
グローバルで確保された領域だといろいろコケる
SharedPrim系に影響がでるか様子見
----------------------------------------
DRAWT::Draw
proteced から public に変更
プリミティブが不要のオーバーロードを追加
----------------------------------------
Etc/util.hpp
moveFBLR を作成
makeshared2 を作成
----------------------------------------
Lib/Game/Scene
SCENE_PLAYER を SCENE_SECTION に変更
----------------------------------------
[work|draw|call]w.cpp
Parent と Adptor のアドレス表示が冗長なので ON/OFF 表示に変更
----------------------------------------
[draw2|draw3].cpp
SetDim をしていなかった...

XOBJ で Rotate 系のデフォルト軸をオーバーライド
----------------------------------------
using SOBJ::SetRotate;
using SOBJ::AddRotate;
using SOBJ::GetRotate;

XOBJで忘れ物
----------------------------------------
using OBJECT = ZOBJ;
opal::OBJECT を用意。
----------------------------------------
Lib/Game/fsm
コマンドを修正
BREAK->REDO
ONECE->BREAK
----------------------------------------
Lib/Ext/vector
y と z を入れ替えた3次元ベクトル DVECTOR を定義
Sys/Obj/{pobj,sobj}も対応
----------------------------------------
Lib/Game/Promoter
pos[2]　を　0=start/1=destination　に変更

GetPromoterPositionS/GetPromoterPositionD を新設
----------------------------------------
Sys/file
std::string 対応を徐々に進めていくか...

↓なんだったろう・・・とりあえず廃止
static void Path( const char*,        std::function<void(const char*)>  );
static void Path( const std::string&, std::function<void(const char*)>  );
static void Path( std::nullptr_t,     std::function<void(const char*)>  );
void FILEX::Path( const char* p, std::function<void(const char*)> f ){

	Path( p );
	if ( f ) { f( path ); }
	Path( nullptr );
}

void FILEX::Path( const std::string& p, std::function<void(const char*)> f ){  Path( p.c_str(), f );	}
void FILEX::Path( std::nullptr_t, std::function<void(const char*)> f ){ Path( nullptr, f );	}
----------------------------------------
Lib/Util/degree
Rad2Dix/Dix2Rad を新設
----------------------------------------
Lib/Ext/size
std::pair/tuple に対応
----------------------------------------
Sys/srvc
Lib/Game/scene

Set|GetName が干渉するので、Set|GetServiceName に変更。
----------------------------------------
Lib/Game/atari/collision

namespace を COLLISION_2D と COLLISION_3D に分ける
----------------------------------------
Lib?ext/coordinate

VectorRotate を追加
----------------------------------------
VIEW_PORT の扱いを全面変更。
かなり勘違いしていた...
Sizeはサイズをそのまま返すようにしたの(以前は半分)
hlsl::Scaleをそれにあわせて修正
----------------------------------------
SCISSOR を新設する...
immediate タイプ
----------------------------------------
Prio が FIFO/LIFO を選べるようにした
デフォルトは LIFO
----------------------------------------
FONT がデバイスステートをいろいろいじっていた。
それをリセットする機能をデバイスに新規追加。
三大ステート(Rasterrizer/Depthstencil/Blend)
理解がちょっと深まった。
----------------------------------------
FNOTZ(=!FZERO) がなかった
----------------------------------------
TASK::Name() はゲームに邪魔なので ID に変更
----------------------------------------
Inc/etc/util.hpp
map 系を修正...
----------------------------------------
Lib/Game/promoter
PromoterStop を追加
----------------------------------------
VIEW_PORT 間違っていた...修正
----------------------------------------
surface_dx11
SURFACE_LOCK のデフォルトコンストラクタを利用可能に
Lock/UnLock を追加
----------------------------------------
Utl/degree
DIC 四つ角を追加
DIM ３×３を追加
----------------------------------------
obj/oobj
VectorWorld/Trans を追加
----------------------------------------
Lib/Ext/vector
vector2->3->4 を継承に変更
ポインターの互換性が出来た
構造化束縛が効かなくなった
----------------------------------------
DirectX9 関連は最小限を残して排除
----------------------------------------
Usr/Draw/prim/
line2_dx
line3_dx
rect2_dx
rect3_dx

何もしていない InitDevice/FreeDevice を削除
----------------------------------------
Usr/Draw/rect
描画を LIST から STRIP　に変更

D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
↓
D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
----------------------------------------
Lib/Game/fsm
FSM_RETV に等値比較演算子を追加
----------------------------------------
Lib/Ext/hlsl
struct に alignas(16)を付記
----------------------------------------
Sys/obj
debug 用変数を用意
----------------------------------------
Lib/Game/fsm
ACTION::Exec の switch 分岐が BREAK対応していなかったので修正
----------------------------------------
Lib/Utl/degree
DIM_Offset の作成
----------------------------------------
Etc/float
範囲判定の追加(FRTT/FRTE/FRET/FREE)
----------------------------------------
Usr/Draw/rsrc/surface
速度改善のため COLOR を DWORD に変更
----------------------------------------
drawo
Draw() を Register と Parent に対応。
アニメはよくわからんので削除。
----------------------------------------
ARRAYSIZE を std::size に置き換え
----------------------------------------
Lib/Ext/color
COLOR_{V,C}2{R,G,B,A} を追加
----------------------------------------
Lib/Ext/Color
GetColor{RGBA}(UINT) を {RGBA}(void) に変更
----------------------------------------
Etc/util.hpp
moveFBLR を2D/3Dでわける
----------------------------------------
Lib/Ext/grid
廃止、汎用関数のみにして Lib/Game に移動
----------------------------------------
Sys/draw
DrawPrimitiveUP|DrawIndexedPrimitive の切り替えを廃止
----------------------------------------
Opal
デフォルトファイルパスの設定 FILEPATH
----------------------------------------
Usr/Draw/rect2
Draw を TEXTURE 対応
----------------------------------------
Draw()を廃止
----------------------------------------
PADX
キーボードの生配列を引っ張れるようにする
----------------------------------------
TASK
状態に Retire と Sleep を追加
----------------------------------------
Sys/draw
Draw()をDRAWXで再設計(DrawFunc)
----------------------------------------
Usr/Draw/rsrc/surface
デバイスの共有を廃止、すべて個別に
----------------------------------------
Sys/sig
なんか色々見直し...
----------------------------------------
Sys/draw_dx11z
Sys/drawb
Usr/Draw/prim/rectx

OMSetBlendState の blendfactor に対応
----------------------------------------
Lib/Game/drawprim
DRAWX::DrawFunc を汎用化
----------------------------------------
Sys/Opal
#define GMAIN
gmain の簡易記法を追加
----------------------------------------
Lib/Ext/vector
VECTORをメンバ変数で使用する場合、
コンストラクタを作成しないで、
メンバ初期化子にすると-Wnoxceptに引っ掛かるので
VECTORのコンストラクタに noexcept を明示的に追加
----------------------------------------
Sys/System
システムが初期化/実行/後始末を保証するワーク置き場を用意
----------------------------------------
Sys/prim[sx]
PRIMS::SharedSelfが登録中にリセットされると迷子になることが発覚

Lib/Std/task
selfを拡張してmapにする
KILL以外にREGISTER/RELEASE時の呼び出しを追加

やはりプリミティブを監視する機構が必要
PRIMXを新設する、あわせてRSRCXも作っておく
----------------------------------------
Lib/Game/Actor

ACTOR_MANAGER を機能とオブジェに分離
機能のみでも使用可能にする
----------------------------------------
Lib/Game/Listener

新規追加
----------------------------------------
Lib/Game/atari/atari_s

クライアントの監視を shared_ptr から weak_ptr に修正
----------------------------------------
Usr/Draw/prim/font

BUFF_MAX = 0x100 を撤廃して無制限に変更
----------------------------------------
surface/CANVAS
Clear() がサイズ指定がなかったバグを修正
CANVAS の引数付き Begin をオーバーライド
----------------------------------------
drawprim の表示優先をレイヤー指定に変更
----------------------------------------
即時描画系を修正
DrawFuncPrim -> DrawPrim　名前の変更/可変引数対応に拡張
DrawMainをprivateに変更
DrawMainの描画をSUPERXに変更
----------------------------------------
Etc/utility
SUPERクラスを追加
----------------------------------------
Lib/Game/Scene
使用方法を記載
シーン名の管理のstd::stringをshared_ptrにして
未定義状態を持てるように改造
objを切り離して独立させる
生成関数を作りやすくした
----------------------------------------
Sys/Time
FRAMEがよくわかんなくなったので廃止
やるならLib/Std/Tickの利用か？
Tickはどうせならコールバック対応にしよう
----------------------------------------
Lib/Ext/Vector
構造化束縛への対応
----------------------------------------
superx.hpp/oobj.cpp,sobj.cpp
SRTをInitで"初期化しない"フラグを追加
(コンストラクタでの設定を有効にする)
----------------------------------------
Lib/Game/atari
全体的にコンパクトに改修
----------------------------------------
Time
経過フレーム数のカウント
----------------------------------------
Lib/Game/crtn
まとめを兼ねて再整理
printfすると終了時にセグフォするなぁ
----------------------------------------
draw_dx11.cpp
DirectX11でDirect2Dを使用するために
D3D11CreateDeviceのフラグにD3D11_CREATE_DEVICE_BGRA_SUPPORTを追加
----------------------------------------
mk
L""でエラーがでるのでコンパイラオプションを追加

--input-charset=cp932
--exec-charset=cp932
----------------------------------------
DXTXは冗長だった...
DXTKに統合
----------------------------------------
directX 9関連を排除
Direct2Dでデバッグ表示を対応する
----------------------------------------
drawprimをimmidiate化する
→drawfuncを廃止
SysExec()を各オブジェの初期化後に実施するように修正
各オブジェのCoordinateをpublicに修正、引数もデフォルトnullptrにする
→drawoを廃止
rsrcにprimと同じ管理機能を追加
rsrcとprimのファイルを整理
----------------------------------------
dotを廃止
----------------------------------------
gmainを廃止
----------------------------------------
DeviceLost対策を廃止していこう...未着手
primからDeviceをはがしていく
lineは見直す、とりあえずline3は廃止
----------------------------------------
SYSTEMX::SysFuncを修正
----------------------------------------
const char*/const std::string を
std::string_viewに統合

tlinx.hpp + drawx/callx/workx
rsrcx + texture/surface
----------------------------------------
-std=c++1z -> -std=c++2a
----------------------------------------
drawprimを細分化
----------------------------------------
rsrc/Generate
確保済みリソースをGenerateすると読み込みしてしまう
FILED(それ回避用)のgenerateを追加

FILEDの必須引数が名前/アドレスだったが、名前だけに修正
→基底のMESSAGE_DATAを修正
→std::string_viewに修正
→NAMEまで波及...
----------------------------------------
Lib/Gameを見直す
crtn,fsm,behavior,atari,dframeをdeprecatedに
必要になったら再構築しよう...
----------------------------------------
thrd
Killの処理をフラグ立てにして、終了処理はタスクに任せる
----------------------------------------
call_xa
XAudio2 が使えるようになった

mk.cnf の _WIN32_WINNTのOS値を更新

0x601:Win7
↓
0xA00:Win10

XAudio2.hの下記をコメントアウト

__mingw_uuidof<XAudio2>
__mingw_uuidof<XAudio2Extension>

ソースファイルで明示的に実体化
ヘッダだと順番的にダメみたい(g++の問題？)
----------------------------------------
SwapChain
DXGI_SWAP_CHAIN_DESC
↓
DXGI_SWAP_CHAIN_DESC1

結構な大改造

draw_dx11
draw_dx2d
----------------------------------------
drawprim::imageにカラーセットしていないので
前段のrectの影響を受けていた
----------------------------------------
シェーダーの.fxhの場所を変えた際にmkの記述も変更していなかった
そしてmkの@${foreach}の処理にバグが見つかった
makeのマクロがスペースを使うから分割に\sが使えない...
----------------------------------------
超久しぶり...触る
いろいろ忘れていそう
----------------------------------------
makefile 内で $ (hoge)の空白がshでエラーになるようになった...
空白が入るのはmkのバグなので空白を入れないようにする
@$() 依存ファイル
$()　固有のオプション

分割が難しいので、依存ファイルを@[]に変更
----------------------------------------
