#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// new/delete
//========================================
//----------------------------------------
// new
//----------------------------------------
namespace opal {
	extern void* New( void*, const char*, size_t, size_t );

	template<typename T, typename... A>T* NS( A&&... a ) {

		return ( T* )New( new T ( std::forward<A>(a)... ), typeid( T ).name(), sizeof( T ), 1 );
	}

	template<typename T>T* NA( size_t n ) {

		return ( n ) ? ( T* )New( new T[n], typeid( T ).name(), sizeof( T ), n ) : nullptr;
	}
}

template<typename T               >inline T* NEWS( void            ){ return     opal::NS<T>();			}
template<typename T               >inline T* NEWS( T*& r           ){ return r = opal::NS<T>();			}
template<typename T, typename... A>inline T* NEWS(        A&&... a ){ return     opal::NS<T>( a... );	}
template<typename T, typename... A>inline T* NEWS( T*& r, A&&... a ){ return r = opal::NS<T>( a... );	}
template<typename T               >inline T* NEWA(        size_t s ){ return     opal::NA<T>( s );		}
template<typename T               >inline T* NEWA( T*& r, size_t s ){ return r = opal::NA<T>( s );		}

template<typename T, typename... A>inline T* NEWP( T* t, A&&... a ){ return new ( t ) T( std::forward<A>(a)... ); }

//----------------------------------------
// delete
//----------------------------------------
namespace opal {
	extern void Del( const void* );

	template<typename T>void DS( T* p ){ if ( p ) { Del( p ); delete    p;	}	}
	template<typename T>void DA( T* p ){ if ( p ) { Del( p ); delete [] p;	}	}
}

template<typename T>inline void DELETES( T*&         r ){ opal::DS(                  r   ); r = nullptr;	}
template<typename T>inline void DELETEA( T*&         r ){ opal::DA(                  r   ); r = nullptr;	}
template<typename T>inline void DELETES( const T*    r ){ opal::DS(                  r   ); 				}
template<typename T>inline void DELETEA( const T*    r ){ opal::DA(                  r   ); 				}
template<typename T>inline void DELETES( T*&&        r ){ opal::DS(                  r   ); 				}
template<typename T>inline void DELETEA( T*&&        r ){ opal::DA(                  r   ); 				}
template<typename T>inline void DELETES( void*&      r ){ opal::DS( static_cast<T*>( r ) ); r = nullptr;	}
template<typename T>inline void DELETEA( void*&      r ){ opal::DA( static_cast<T*>( r ) ); r = nullptr;	}
template<typename T>inline void DELETES( const void* r ){ opal::DS( static_cast<const T*>( r ) );			}
template<typename T>inline void DELETEA( const void* r ){ opal::DA( static_cast<const T*>( r ) );			}
template<typename T>inline void DELETES( void*&&     r ){ opal::DS( static_cast<T*>( r ) );					}
template<typename T>inline void DELETEA( void*&&     r ){ opal::DA( static_cast<T*>( r ) );					}

//----------------------------------------
// Shared Object
//----------------------------------------
namespace opal {
	template<typename T, typename... A>std::shared_ptr<T  > SharedObj( A&&... a ){ return { NEWS<T>( std::forward<A>( a )... ), [](auto p){ DELETES<T>( p ); } }; }
	template<typename T               >std::shared_ptr<T[]> SharedObj( UINT   n ){ return { NEWA<T>( n                       ), [](auto p){ DELETEA<T>( p ); } }; }
}

//----------------------------------------
// Placement New
//----------------------------------------
namespace opal {
	template<typename TYPE,typename THAT>TYPE* PlacementNew( THAT* that ){ return new ( ( TYPE* )that ) TYPE;	}
}

// End Of File
