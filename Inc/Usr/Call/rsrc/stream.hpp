#pragma once
//========================================
// OPAL Game Library
//========================================
#include "call.hpp"
#include "call_ds.hpp"

//========================================
// STREAM
//========================================
struct STREAM {
public:
	STREAM( DWORD = 0 );
	~STREAM();

public:
	IDirectSoundBuffer8*	buffer;
	HMMIO		mmio;
	const DWORD	size;
	LONG		ofst;
	bool		swap;
	DWORD		loop;
	LONG		seek;
	bool		play;

	std::vector<char>	buff;

public:
	void Open( const char*, WAVEFORMATEX& );
	void Close( void );

	void Buffer( IDirectSoundBuffer8* );

	void Play( void );
	void Stop( void );

	DWORD Seek( void ) const;
	bool Playing( void ) const;

	void* operator()( void );
};

// End Of File
