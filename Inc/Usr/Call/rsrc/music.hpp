#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "call.hpp"

#include "call_md.hpp"

//========================================
// MUSIC���\�[�X
//========================================
class MUSIC : public opal::RSRC {
	friend struct MUSICX;

	//----------------------------------------
	// constructor & destructor
	//----------------------------------------
public:
	explicit MUSIC();
	virtual ~MUSIC();

	//----------------------------------------
	// member variables
	//----------------------------------------
protected:
	IDirectMusicPerformance8*	performance;
	IDirectMusicLoader8*		loader;
	IDirectMusicSegment8*		segment;
	IDirectMusicAudioPath8*		audiopath;
	IDirectMusicSegmentState*	segstate;

protected:
	mutable LONG		volume;
	mutable MUSIC_TIME	time;
	MUSIC_TIME			length;

	//----------------------------------------
	// member functions
	//----------------------------------------
public:
	using RSRC::Init;

	virtual void Init( const void* pBuff, size_t vSize, const char* pName = nullptr, const void* pDesc = nullptr );
	virtual void Free( void );

public:
	virtual void Play( void );
	virtual void Stop( void );

public:
	virtual void  Volume( float vVolm );
	virtual float Volume( void ) const;

public:
	virtual MUSIC_TIME Time(   void ) const;
	virtual MUSIC_TIME Length( void ) const;
};

struct MUSICX : public opal::RSRCBV<MUSIC> {
	explicit MUSICX();
	virtual ~MUSICX();

	virtual UINT Musics( void ) const;

	virtual void NowMusic( UINT n );
	virtual UINT NowMusic( void   ) const;

	virtual void SetMusic( const MUSIC* r, UINT n );
	virtual void SetMusic( const MUSIC* r );
	virtual void SetMusic( const MUSIC& r, UINT n );
	virtual void SetMusic( const MUSIC& r );

	virtual const MUSIC* GetMusic( UINT n ) const;
	virtual const MUSIC* GetMusic( void   ) const;
};

// End Of File
