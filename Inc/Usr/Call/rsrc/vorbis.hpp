#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "call.hpp"

//========================================
// VORBISリソース
//========================================
class VORBIS : public opal::RSRC {
	friend struct VORBISX;

public:
	explicit VORBIS();
	virtual ~VORBIS();

protected:
	std::unique_ptr<struct OggVorbis_File>		ovf;
	std::unique_ptr<struct OggVorbis_Decoder>	ovd;

	UINT	channelNum;		// チャンネル数
	UINT	samplingRate;	// サンプリングレート
	UINT	bitRate;		// ビットレート
	UINT	playTime;		// 格納時間

	WAVEFORMATEX	wfmt;
	DSBUFFERDESC	desc;

public:
	using RSRC::Init;
	virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
	virtual void Free( void ) override;

public:
	virtual auto Desc( void ) const->DSBUFFERDESC;

public:
	virtual void Seek( double = 0.0 );
	virtual bool Segment( char*, DWORD ) const;
};

struct VORBISX : public opal::RSRCBV<VORBIS> {
	explicit VORBISX();
	virtual ~VORBISX();

	virtual UINT Vorbiss( void ) const;

	virtual void NowVorbis( UINT );
	virtual UINT NowVorbis( void ) const;

	virtual void SetVorbis( const VORBIS*, UINT );
	virtual void SetVorbis( const VORBIS* );
	virtual void SetVorbis( const VORBIS&, UINT );
	virtual void SetVorbis( const VORBIS& );

	virtual const VORBIS* GetVorbis( UINT ) const;
	virtual const VORBIS* GetVorbis( void ) const;
};

// End Of File
