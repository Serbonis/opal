#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "call.hpp"
#include "call_ds.hpp"

//========================================
// SOUND���\�[�X
//========================================
class SOUND : public opal::RSRC {
	friend struct SOUNDX;

public:
	explicit SOUND();
	virtual ~SOUND();

protected:
	std::shared_ptr<std::vector<char>>	buff;
	WAVEFORMATEX	wfmt;
	DSBUFFERDESC	desc;
	DWORD			leng;

public:
	using RSRC::Init;
	virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
	virtual void Free( void ) override;

public:
	virtual auto Desc( void ) const->DSBUFFERDESC;
	virtual auto Buff( void ) const->const char*;
	virtual auto Time( void ) const->DWORD;
};

struct SOUNDX : public opal::RSRCBV<SOUND> {
	explicit SOUNDX();
	virtual ~SOUNDX();

	virtual UINT Sounds( void ) const;

	virtual void NowSound( UINT );
	virtual UINT NowSound( void ) const;

	virtual void SetSound( const SOUND*, UINT );
	virtual void SetSound( const SOUND* );
	virtual void SetSound( const SOUND&, UINT );
	virtual void SetSound( const SOUND& );

	virtual const SOUND* GetSound( UINT ) const;
	virtual const SOUND* GetSound( void ) const;
};

// End Of File

