#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "call.hpp"

#include "call_ds.hpp"

//========================================
// OGG
//========================================
class OGG {
protected:
	explicit OGG();

public:
	virtual ~OGG();

protected:
	void*					comp;
	IDirectSoundBuffer8*	buffer;

protected:
	virtual void InitComp( void );
	virtual void FreeComp( void );
	virtual void Component( void* = nullptr );

protected:
	virtual void PlayStream( void );
	virtual void StopStream( void );

	UINT 	mode;
	LONG	seek;

protected:
	virtual void Play( void );
	virtual void Stop( void );

protected:
	virtual DWORD Seek( void ) const;
	virtual bool Playing( void ) const;

protected:
	virtual void  Volume( float );
	virtual float Volume( void ) const;

	virtual void  Tempo( float );
	virtual float Tempo( void ) const;
};

// End Of File
