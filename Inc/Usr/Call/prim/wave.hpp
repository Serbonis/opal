#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pcmt.hpp"
#include "sound.hpp"

//========================================
// WAVE�^�X�N
//========================================
class WAVE : public PCMT, public SOUNDX {
public:
	explicit WAVE();
	virtual ~WAVE();

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

public:
	using opal::SOBJ::operator =;
	virtual void CallInfo( void ) const override;
	virtual void CallInfo( int  ) const override;
	virtual void CallMain( void ) override;

private:
	virtual void* Composition( void ) const;
};

// End Of File
