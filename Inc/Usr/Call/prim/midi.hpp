#pragma once
//========================================
// OPAL Game Library
//========================================
#include "call.hpp"

#include "music.hpp"

//========================================
// BGM�^�X�N
//========================================
class MIDI : public opal::CALLT, public MUSICX {
	//----------------------------------------
	// constructor & destructor
	//----------------------------------------
public:
	explicit MIDI();
	virtual ~MIDI();

	//----------------------------------------
	// member variables
	//----------------------------------------
protected:
	MUSIC*	music;

	//----------------------------------------
	// member functions
	//----------------------------------------
public:
	virtual void Init( const char* pName = nullptr ) override;
	virtual void Free( void ) override;

public:
	using opal::SOBJ::operator =;
	virtual void CallPrint( opal::PRINT = nullptr ) const override;
	virtual void CallInfo(  opal::PRINT = nullptr ) const override;
	virtual void CallMain(  void ) override;
};

// End Of File
