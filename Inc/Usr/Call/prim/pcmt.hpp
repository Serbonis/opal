#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pcm.hpp"

//========================================
// PCM�^�X�N
//========================================
class PCMT : public opal::CALLT, public PCM {
protected:
	explicit PCMT();

public:
	virtual ~PCMT();

protected:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

protected:
	virtual void CallInfo( void ) const override;
	virtual void CallInfo( int  ) const override;
	virtual void CallMain( void ) override;

private:
	virtual void mode_stop( void ) final;
	virtual void mode_play( void ) final;
	virtual void mode_loop( void ) final;
	virtual void mode_playing( void ) final;

private:
	virtual void* Composition( void ) const = 0;
};

// End Of File
