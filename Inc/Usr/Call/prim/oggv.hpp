#pragma once
//========================================
// OPAL Game Library
//========================================
#include "oggt.hpp"
#include "vorbis.hpp"

//========================================
// Ogg/Vorbis
//========================================
class OGGV : public OGGT, public VORBISX {
public:
	explicit OGGV();
	virtual ~OGGV();

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

public:
	using opal::SOBJ::operator =;
	virtual void CallInfo( void ) const override;
	virtual void CallInfo( int  ) const override;
	virtual void CallMain( void ) override;

private:
	virtual void* Composition( void ) const;
};

// End Of File
