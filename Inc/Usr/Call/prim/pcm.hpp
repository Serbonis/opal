#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "call.hpp"

#include "call_ds.hpp"

//========================================
// PCM
//========================================
class PCM {
protected:
	explicit PCM();

public:
	virtual ~PCM();

protected:
	void*					comp;
	IDirectSoundBuffer8*	buffer;

	float	volume;
	float	tempo;
	DWORD	freq;

protected:
	virtual void InitComp( void );
	virtual void FreeComp( void );
	virtual void Component( void* = nullptr );

protected:
	virtual void Play( void );
	virtual void Stop( void );

protected:
	virtual DWORD Seek( void ) const;
	virtual bool Playing( void ) const;

protected:
	virtual void  Volume( float );
	virtual float Volume( void ) const;

	virtual void  Tempo( float );
	virtual float Tempo( void ) const;
};

// End Of File
