#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"
#include "surface_dx11.hpp"

//========================================
// GDI�^�X�N
//========================================
namespace opal::dx11 {
	class GDI_ : public XDEVICE {
	public:
		explicit GDI_( const void* = nullptr );
		virtual ~GDI_();

	protected:
		const void* const	data;

	protected:
		SSIZEU	canvas;
		IDXGDI	target;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void DestroyDevice( void ) override;

	public:
		virtual SSIZEU Canvas( void ) const;
		//virtual const IDXGDI& Target( void ) const;
		virtual const IDXGDI* Target( void ) const;

	public:
		virtual void DrawPrim( std::function<void(HDC,UINT,UINT)>, COLOR, COLOR, const TEXTURE* );

	public:
		static UINT DEFAULT_CANVAS( void );
	};
}

// End Of File
