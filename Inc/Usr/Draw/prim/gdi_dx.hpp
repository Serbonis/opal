#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "rect2.hpp"

//========================================
// GDI�^�X�N
//========================================
namespace opal::dx {
	class GDI :
		public opal::dx11::RECT2
	{
	protected:
		explicit GDI();

	public:
		virtual ~GDI();

	protected:
		COLOR	col;
		COLOR	key;
		HBRUSH	brush;

	public:
		virtual void Init( const char* = nullptr );
		virtual void Free( void );

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;

	public:
		virtual void   SetCanvas( UINT, UINT ) = 0;
		virtual SSIZEU GetCanvas( void ) const = 0;

	public:
		virtual void   SetBrush( COLOR );
		virtual void   SetBrush( UCHAR, UCHAR, UCHAR, UCHAR = 0xff );
		virtual void   SetBrush( void );
		virtual HBRUSH GetBrush( void ) const;

	public:
		virtual void  SetCol( COLOR = 0x00000000 );
		virtual void  SetKey( COLOR = 0x00000000 );
		virtual void  SetCol( UCHAR, UCHAR, UCHAR, UCHAR = 0x00 );
		virtual void  SetKey( UCHAR, UCHAR, UCHAR, UCHAR = 0x00 );
		virtual COLOR GetCol( void ) const;
		virtual COLOR GetKey( void ) const;

	private:
		virtual void DrawGDI( HDC, UINT, UINT ) = 0;
	};
}

// End Of File
