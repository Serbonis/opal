#pragma once
//========================================
// OPAL Game Library
//========================================
#include "linex_dx11_.hpp"

//========================================
// 直線タスク(2D)
//========================================
namespace opal::dx11 {
	class LINE2_ : public LINEX_ {
	public:
		explicit LINE2_( const void* = nullptr );
		virtual ~LINE2_();

	private:
		ID3D11Buffer*				vbuffer;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void DestroyDevice( void ) override;

	public:
		virtual LINEX_* Context( void ) override;

	public:
		virtual void VertexMap( const void*, UINT, UINT = 0 ) const override;
		virtual UINT VertexStride( void ) const override;
		virtual UINT VertexOffset( void ) const override;

	private:
		static ID3D11InputLayout*	layout;
		static ID3D11VertexShader*	vshader;
		static ID3D11PixelShader*	pshader;
		static ID3D11Buffer*		cbuffer;
	};
}

// End Of File
