#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx11_.hpp"

//========================================
// ��`�^�X�N(2D)
//========================================
namespace opal::dx11 {
	class RECT2_ : public RECTX_ {
	public:
		explicit RECT2_();
		virtual ~RECT2_();

	public:
		static void CreateDevice(  SDEVICE<RECT2_>* );
		static void DestroyDevice( SDEVICE<RECT2_>* );

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void DestroyDevice( void ) override;

	public:
		virtual RECTX_* Context( void ) override;

	private:
		static ID3D11Buffer*		vbuffer;
		static ID3D11InputLayout*	layout;
		static ID3D11VertexShader*	vshader;
		static ID3D11PixelShader*	pshader;
		static ID3D11Buffer*		cbuffer;
	};
}

// End Of File
