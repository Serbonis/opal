#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx.hpp"

//========================================
// ��`�^�X�N(2D)
//========================================
namespace opal::dx {
	class RECT2 :
		public DRAWT2,
		public RECTX,
		public XDEVICE
	{
	protected:
		explicit RECT2();

	public:
		virtual ~RECT2();

	protected:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;

	public:
		using POBJ::SetPivot;
		using POBJ::SetPivotX;
		using POBJ::SetPivotY;
		virtual void SetPivot(  void ) override;
		virtual void SetPivotX( void );
		virtual void SetPivotY( void );

	public:
		using RECTX::SetSize;
		using RECTX::SetSizeW;
		using RECTX::SetSizeH;
		virtual void SetSize(  const RECT2* );
		virtual void SetSizeW( const RECT2* );
		virtual void SetSizeH( const RECT2* );

	public:
		using RECTX::SetRect;
		using RECTX::SetRectX;
		using RECTX::SetRectY;
		using RECTX::SetRectW;
		using RECTX::SetRectH;
		using RECTX::SetRectXY;
		using RECTX::SetRectWH;
		virtual void SetRect(   const RECT2* );
		virtual void SetRectX(  const RECT2* );
		virtual void SetRectY(  const RECT2* );
		virtual void SetRectW(  const RECT2* );
		virtual void SetRectH(  const RECT2* );
		virtual void SetRectXY( const RECT2* );
		virtual void SetRectWH( const RECT2* );
	};
}

// End Of File
