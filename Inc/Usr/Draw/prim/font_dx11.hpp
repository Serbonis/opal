#pragma once
//========================================
// OPAL Game Library
//========================================
#include "font_dx.hpp"

//========================================
// フォントタスク
//========================================
namespace opal::dx11 {
	class FONT_;
	class FONT :
		public dx::FONT,
		public XDEVICE
	{
	public:
		explicit FONT();
		virtual ~FONT();

	protected:
		SDEVICE<FONT_>	device;
		std::string		font;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

		virtual void DeviceData( const void* ) override;
		virtual void DeviceData( void ) override;

	private:
		virtual void DrawPrim( void ) override;

	public:
		virtual void SetSize( float, float ) override;
		virtual void SetSize( const dx::FONT* ) override;

	public:
		virtual void SetFont( const char* );
		virtual void SetFont( const std::string& );
		virtual void SetFont( std::nullptr_t = nullptr );
		virtual const char* GetFont( void ) const;


	private:
		static std::string defaultfont;
		static std::string defaultpath;

	public:
		static void SetDefaultPath( const char* );
		static void SetDefaultFont( const char* );
		static void SetDefaultPath( const std::string& );
		static void SetDefaultFont( const std::string& );
		static void SetDefaultPath( std::nullptr_t = nullptr );
		static void SetDefaultFont( std::nullptr_t = nullptr );
		static const char* GetDefaultPath( void );
		static const char* GetDefaultFont( void );
	};

	using FONT_MAP = DEVICE_MAP<std::string,FONT_,DRAWX::DeviceLost,DRAWX::DeviceLost>;
}

// End Of File
