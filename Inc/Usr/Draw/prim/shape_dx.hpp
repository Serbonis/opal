#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "Attr/color.hpp"

//========================================
// シェイプタスク(共通)
//========================================
namespace opal::dx {
	class SHAPE :
		public DRAWT2,
		public DRAWB,
		public COLOR,
		public XDEVICE
	{
	protected:
		explicit SHAPE();

	public:
		virtual ~SHAPE();

	protected:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;
	};
}

// End Of File
