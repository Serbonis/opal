#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "Attr/color.hpp"
#include "Attr/size.hpp"

//========================================
// 矩形タスク(共通)
//========================================
namespace opal::dx {
	class RECTX :
		public DRAWB,
		public COLORS,
		public SIZE2,
		public CRECTF
	{
	protected:
		explicit RECTX();

	public:
		virtual ~RECTX();

	protected:
		virtual void Init( const char* = nullptr );
		virtual void Free( void );

	public:
		using SIZE2::SetSize;
		virtual void SetSize( void ) override = 0;

	public:
		using CRECTF::SetRect;
		virtual void SetRect( void );

	protected:
		virtual void DrawInfo( int ) const;
	};
}

// End Of File
