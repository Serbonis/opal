#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx11.hpp"
#include "rect2_dx.hpp"

//========================================
// ��`�^�X�N(2D)
//========================================
namespace opal::dx11 {
	class RECT2_;
	class RECT2 :
		public dx11::RECTX,
		public dx::RECT2
	{
	public:
		explicit RECT2();
		virtual ~RECT2();

	protected:
		SDEVICE<RECT2_>	device;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	private:
		virtual void DrawPrim( void ) override;

	public:
		using SIZE2::SetSize;
		virtual void SetSize( void ) override;
	};
}

// End Of File
