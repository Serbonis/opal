 #pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// シェイプタスク
//========================================
#if defined ( OPAL_DX11 )
#include "shape_dx11.hpp"
class SHAPE : public opal::dx11::SHAPE {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
