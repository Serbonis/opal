#pragma once
//========================================
// OPAL Game Library
//========================================
#include "marux.hpp"
#include "line2.hpp"

//========================================
// �ۃ^�X�N(2D)
//========================================
class MARU2 :
	public LINE2,
	public MARUX
{
public:
	explicit MARU2();
	virtual ~MARU2();

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

private:
	using LINE2::SetLoop;
	using LINE2::SetNode;
	using LINE2::GetNode;
	using LINE2::AddNode;
	using LINE2::MulNode;
	using LINE2::SetLine;
	using LINE2::GetLine;

public:
	virtual void SetRegular( void ) override;
	virtual void SetRegular( UINT ) override;

public:
	using MARUX::SetSize;
	virtual void SetSize( const MARU2* );

public:
	using SOBJ::operator =;
	virtual void DrawInfo( void ) const override;
	virtual void DrawInfo( int  ) const override;
	virtual void DrawMain( void ) override;
};

// End Of File
