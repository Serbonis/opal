#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

//========================================
// 矩形タスク(共通)
//========================================
namespace opal::dx11 {
	class RECTX_ : public XDEVICE {
		friend class RECTX;

	protected:
		explicit RECTX_();

	public:
		virtual ~RECTX_();

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual RECTX_* Context( void ) = 0;

	private:
		static ID3D11Buffer*				ibuffer;
		static ID3D11PixelShader*			pshader_;

	protected:
		static ID3D11Buffer*				vbuffer;
		static ID3D11InputLayout*			layout;
		static ID3D11VertexShader*			vshader;
		static ID3D11PixelShader*			pshader;
		static ID3D11Buffer*				cbuffer;
		static ID3D11BlendState*			blend;
		static ID3D11DepthStencilState*		depthstencil;
	};
}

// End Of File
