#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx.hpp"

//========================================
// ��`�^�X�N(3D)
//========================================
namespace opal::dx {
	class RECT3 :
		public DRAWT3,
		public MATERIAL,
		public RECTX,
		public XDEVICE
	{
	public:
		explicit RECT3();
		virtual ~RECT3();

	protected:
		UINT	axis;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;

	public:
		using RECTX::SetSize;
		using RECTX::SetSizeW;
		using RECTX::SetSizeH;
		virtual void SetSize(  const RECT3* );
		virtual void SetSizeW( const RECT3* );
		virtual void SetSizeH( const RECT3* );

	public:
		using RECTX::SetRect;
		using RECTX::SetRectX;
		using RECTX::SetRectY;
		using RECTX::SetRectW;
		using RECTX::SetRectH;
		using RECTX::SetRectXY;
		using RECTX::SetRectWH;
		virtual void SetRect(   const RECT3* );
		virtual void SetRectX(  const RECT3* );
		virtual void SetRectY(  const RECT3* );
		virtual void SetRectW(  const RECT3* );
		virtual void SetRectH(  const RECT3* );
		virtual void SetRectXY( const RECT3* );
		virtual void SetRectWH( const RECT3* );

	public:
		virtual void SetAxis( UINT );
		virtual UINT GetAxis( void ) const;
	};
}

// End Of File
