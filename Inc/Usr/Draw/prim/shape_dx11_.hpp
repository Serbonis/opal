#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

//========================================
// シェイプタスク
//========================================
namespace opal::dx11 {
	class SHAPE_ : public XDEVICE {
		friend class SHAPE;

	public:
		explicit SHAPE_();
		virtual ~SHAPE_();

	public:
		static void CreateDevice(  SDEVICE<SHAPE_>* );
		static void DestroyDevice( SDEVICE<SHAPE_>* );

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void DestroyDevice( void ) override;

	public:
		virtual SHAPE_* Context( void );

	protected:
		static ID3D11InputLayout*			layout;
		static ID3D11VertexShader*			vshader;
		static ID3D11PixelShader*			pshader;
		static ID3D11Buffer*				cbuffer;
		static ID3D11BlendState*			blend;
		static ID3D11DepthStencilState*		depthstencil;
	};
}

// End Of File
