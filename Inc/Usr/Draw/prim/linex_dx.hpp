#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

//========================================
// 直線タスク(共通)
//========================================
namespace opal::dx {
	class LINEX :
		public DRAWB,
		public COLOR
	{
	protected:
		explicit LINEX();

	public:
		virtual ~LINEX();

	protected:
		static constexpr auto	NODE_PACK = 6;

		UINT	node;
		float	bold;
		bool	loop;

	public:
		virtual void Init( const char* = nullptr );
		virtual void Free( void );

	public:
		virtual void SetNode( UINT );
		virtual UINT GetNode( void ) const;

	public:
		virtual void  SetBold( float );
		virtual float GetBold( void  ) const;

	public:
		virtual void SetLoop( bool );
		virtual bool GetLoop( void ) const;

	protected:
		virtual void DrawInfo( int ) const;
	};
}

// End Of File
