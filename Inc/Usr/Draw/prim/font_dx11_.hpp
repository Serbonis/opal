#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

//========================================
// フォントタスク
//========================================
namespace DirectX {
	class SpriteBatch;
	class SpriteFont;
}
namespace opal::dx11 {
	class FONT_ : public XDEVICE {
	public:
		explicit FONT_( const void* = nullptr );
		virtual ~FONT_();

	private:
		const void* const		data;
		std::string				font;
		DirectX::SpriteBatch*	batch;
		DirectX::SpriteFont*	sprite;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void DestroyDevice( void ) override;

	public:
		virtual void Size( float, float );
		virtual SSIZEU Size( void ) const;

	public:
		virtual void DrawPrim( const char*, const VECTOR2&, const VECTOR2&, float, const VECTOR2&, COLOR, UINT, float );
	};
}

// End Of File
