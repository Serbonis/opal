#pragma once
//========================================
// OPAL Game Library
//========================================
#include "wakux.hpp"
#include "line2.hpp"

//========================================
// �g�^�X�N(2D)
//========================================
class WAKU2 :
	public LINE2,
	public WAKUX
{
public:
	explicit WAKU2();
	virtual ~WAKU2();

public:
	virtual void Init( const char* = nullptr ) override;
	virtual void Free( void ) override;

private:
	using LINE2::SetLoop;
	using LINE2::SetNode;
	using LINE2::GetNode;
	using LINE2::AddNode;
	using LINE2::MulNode;
	using LINE2::SetLine;
	using LINE2::GetLine;

public:
	using POBJ::SetPivot;
	virtual void SetPivot(  void ) override;

public:
	using WAKUX::SetSize;
	virtual void SetSize( const WAKU2* );

public:
	using SOBJ::operator =;
	virtual void DrawInfo( void ) const override;
	virtual void DrawInfo( int  ) const override;
	virtual void DrawMain( void ) override;
};

// End Of File
