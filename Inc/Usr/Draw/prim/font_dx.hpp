#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "Attr/size.hpp"

//========================================
// フォントタスク(共通)
//========================================
namespace opal::dx {
	class FONT :
		public DRAWT2,
		public COLOR,
		public SIZE2
	{
	protected:
		explicit FONT();

	public:
		virtual ~FONT();

	protected:
		SSIZEU	area;
		UINT	cols;
		UINT	rows;
		UINT	leng;

	protected:
		std::vector<char>	buff;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;

	public:
		using SIZE2::SetSize;
		virtual void SetSize( float, float ) override = 0;
		virtual void SetSize( const FONT*  ) = 0;

	public:
		using POBJ::SetOffset;
		using POBJ::SetPivot;
		virtual void SetOffset( void ) override;
		virtual void SetPivot(  void ) override;

	public:
		virtual void SetArea( UINT, UINT );
		virtual auto GetArea( void ) const->SSIZEU;

	public:
		virtual void Center( float );
		virtual void Center( float, float );

	public:
		virtual auto Width(  void ) const->float;
		virtual auto Height( void ) const->float;

		virtual auto Leng( void ) const->UINT;
		virtual auto Cols( void ) const->UINT;
		virtual auto Rows( void ) const->UINT;

		virtual auto Buff( void ) const->const char*;
		virtual auto Buff( int  ) const->char;

	public:
		virtual void Clear( void );

	public:
		virtual void Print(  const char* = nullptr, ... );
		virtual void String( const char* = nullptr      );
	};
}

//========================================
// FONTクラスを継承ではなく包含する場合
//========================================
#ifdef _MSC_VER
#pragma warning( disable : 4995 )
#endif
#define FONT_Print( FONT, FORMAT, SIZE )	\
do {										\
	char	buff[SIZE];						\
	va_list	list;							\
\
	va_start( list, FORMAT );				\
	vsprintf( buff, FORMAT, list );			\
	va_end( list );							\
\
	FONT.Print( buff );						\
} while ( 0 )

//========================================
// Pitch
//========================================
// 定義				値		備考
//----------------------------------------
// DEFAULT_PITCH	0x00	デフォルト
// FIXED_PITCH		0x01	固定
// VARIABLE_PITCH	0x02	可変

//========================================
// Family
//========================================

// 定義				値		備考
//----------------------------------------
// FF_DONTCARE		0x00	日本語x
// FF_ROMAN			0x10	日本語o
// FF_SWISS			0x20	日本語x
// FF_MODERN		0x30	日本語o
// FF_SCRIPT		0x40	日本語o
// FF_DECORATIVE	0x50	日本語x

//========================================
// Face
//========================================
enum {
	FN_Arial,					// Arial					ANSI_CHARSET
	FN_Arial_Black,				// Arial Black				ANSI_CHARSET
	FN_Batang,					// Batang					ANSI_CHARSET
	FN_BatangChe,				// BatangChe				ANSI_CHARSET
	FN_Century,					// Century					ANSI_CHARSET
	FN_Comic_Sans_MS,			// Comic Sans MS			ANSI_CHARSET
	FN_Courier,					// Courier					ANSI_CHARSET
	FN_Courier_New,				// Courier New				ANSI_CHARSET
	FN_Dotum,					// Dotum					ANSI_CHARSET
	FN_DotumChe,				// DotumChe					ANSI_CHARSET
	FN_Estrangelo_Edessa,		// Estrangelo Edessa		DEFAULT_CHARSET
	FN_FixedSys,				// FixedSys					SHIFTJIS_CHARSET
	FN_Franklin_Gothic_Medium,	// Franklin Gothic Medium	ANSI_CHARSET
	FN_Gautami,					// Gautami					DEFAULT_CHARSET
	FN_Georgia,					// Georgia					ANSI_CHARSET
	FN_Gulim,					// Gulim					ANSI_CHARSET
	FN_GulimChe,				// GulimChe					ANSI_CHARSET
	FN_Gungsuh,					// Gungsuh					ANSI_CHARSET
	FN_GungsuhChe,				// GungsuhChe				ANSI_CHARSET
	FN_Impact,					// Impact					ANSI_CHARSET
	FN_Karita,					// Karita					ANSI_CHARSET
	FN_Latha,					// Latha					DEFAULT_CHARSET
	FN_Lucida_Console,			// Lucida Console			ANSI_CHARSET
	FN_Lucida_Sans_Unicode,		// Lucida Sans Unicode		ANSI_CHARSET
	FN_Mangal,					// Mangal					DEFAULT_CHARSET
	FN_Marlett,					// Marlett					SYMBOL_CHARSET
	FN_Microsoft_Sans_Serif,	// Microsoft Sans Serif		ANSI_CHARSET
	FN_MingLiU,					// MingLiU					ANSI_CHARSET
	FN_Modern,					// Modern					OEM_CHARSET
	FN_MS_PGothic,				// ＭＳ Ｐゴシック			SHIFTJIS_CHARSET
	FN_MS_PMincho,				// ＭＳ Ｐ明朝				SHIFTJIS_CHARSET
	FN_MS_Sans_Serif,			// MS Sans Serif			ANSI_CHARSET
	FN_MS_Serif,				// MS Serif					ANSI_CHARSET
	FN_MS_UI_Gothic,			// MS UI Gothic				ANSI_CHARSET
	FN_MS_Gothic,				// ＭＳ ゴシック			ANSI_CHARSET
	FN_MS_Mincho,				// ＭＳ 明朝				ANSI_CHARSET
	FN_MV_Boli,					// ＭＶ Boli				DEFAULT_CHARSET
	FN_NSimSun,					// NSimSun					ANSI_CHARSET
	FN_Platino_Linotype,		// Platino Linotype			ANSI_CHARSET
	FN_PMingLiU,				// PMingLiU					ANSI_CHARSET
	FN_Psprog,					// Psprog					ANSI_CHARSET
	FN_Raavi,					// Raavi					DEFAULT_CHARSET
	FN_Roman,					// Roman					OEM_CHARSET
	FN_Script,					// Script					OEM_CHARSET
	FN_Shruti,					// Shruti					DEFAULT_CHARSET
	FN_SimHei,					// SimHei					CHINESE_GB2312
	FN_SimSun,					// SimSun					CHINESE_GB2312
	FN_SimSun_PUA,				// SimSun-PUA				CHINESE_GB2312
	FN_Small_Fonts,				// Small Fonts				ANSI_CHARSET
	FN_Sylfaen,					// Sylfaen					ANSI_CHARSET
	FN_Symbol,					// Symbol					SYMBOL_CHARSET
	FN_System,					// System					SHIFTJIS_CHARSET
	FN_Tahoma,					// Tahoma					ANSI_CHARSET
	FN_Terminal,				// Terminal					SHIFTJIS_CHARSET
	FN_Times_New_Roman,			// Times New Roman			ANSI_CHARSET
	FN_Trebuchet_MS,			// Trebuchet MS				ANSI_CHARSET
	FN_Tunga,					// Tunga					DEFAULT_CHARSET
	FN_Verdana,					// Verdana					ANSI_CHARSET
	FN_Vrinda,					// Vrinda					ANSI_CHARSET
	FN_Webdings,				// Webdings					SYMBOL_CHARSET
	FN_Wingdings,				// Wingdings				SYMBOL_CHARSET
	FN_WST_Czec,				// WST_Czec					SYMBOL_CHARSET
	FN_WST_Engl,				// WST_Engl					SYMBOL_CHARSET
	FN_WST_Fren,				// WST_Fren					SYMBOL_CHARSET
	FN_WST_Germ,				// WST_Germ					SYMBOL_CHARSET
	FN_WST_Ital,				// WST_Ital					SYMBOL_CHARSET
	FN_WST_Span,				// WST_Span					SYMBOL_CHARSET
	FN_WST_Swed,				// WST_Swed					SYMBOL_CHARSET
	FN_Meiryo,					// メイリオ					SHIFTJIS_CHARSET
};

// End Of File
