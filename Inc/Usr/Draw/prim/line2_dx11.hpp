#pragma once
//========================================
// OPAL Game Library
//========================================
#include "linex_dx11.hpp"
#include "line2_dx.hpp"

//========================================
// 直線タスク(2D)
//========================================
namespace opal::dx11 {
	class LINE2_;
	class LINE2 :
		public dx11::LINEX,
		public dx::LINE2
	{
	public:
		explicit LINE2();
		virtual ~LINE2();

	protected:
		SDEVICE<LINE2_>	device;
		void*			data;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

		virtual void DeviceData( const void* ) override;
		virtual void DeviceData( void ) override;

	public:
		using dx::LINE2::SetNode;
		virtual void SetNode( UINT ) override;
		virtual void SetNode( void );

	private:
		virtual void DrawPrim( void ) override;
	};

	using LINE2_MAP = DEVICE_MAP<UINT,LINE2_,DRAWX::DeviceLost,DRAWX::DeviceLost>;
}

// End Of File
