#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// 矩形タスク(3D)
//========================================
#if defined ( OPAL_DX11 )
#include "rect3_dx11.hpp"
class RECT3 : public opal::dx11::RECT3 {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
