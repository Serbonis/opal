#pragma once
//========================================
// OPAL Game Library
//========================================
#include "linex_dx.hpp"

//========================================
// 直線タスク(2D)
//========================================
namespace opal::dx {
	class LINE2 :
		public DRAWT2,
		public LINEX,
		public XDEVICE
	{
	protected:
		explicit LINE2();

	public:
		virtual ~LINE2();

	protected:
		struct {
			std::vector<VECTOR2>	a;
			std::vector<VECTOR2>	b;
		}	lv;

	protected:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		using SOBJ::operator =;
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
		virtual void DrawMain( void ) override;

	private:
		virtual void DrawPrim( void ) = 0;

	public:
		virtual void SetNode( UINT ) override;

	protected:
		virtual void SetNode_( int, float,  float  );
		virtual void AddNode_( int, float,  float  );
		virtual void GetNode_( int, float*, float* ) const;
		virtual void MulNode_( int, float );

	public:
		virtual void SetNode( int, float, float );
		virtual void SetNode( int, const VECTOR2* );
		virtual void SetNode( int, const VECTOR3* );
		virtual void SetNode( int, const VECTOR2& );
		virtual void SetNode( int, const VECTOR3& );

		virtual void AddNode( int, float, float );
		virtual void AddNode( int, const VECTOR2* );
		virtual void AddNode( int, const VECTOR3* );
		virtual void AddNode( int, const VECTOR2& );
		virtual void AddNode( int, const VECTOR3& );

	public:
		using LINEX::GetNode;
		virtual void GetNode( int, float*, float* ) const;
		virtual auto GetNode( int ) const->VECTOR2;

	public:
		virtual void MulNode( int, float );
		virtual void MulNode( float );

	public:
		virtual VECTOR2 Node( int  ) const;
		virtual VECTOR2 Node( void ) const;

	protected:
		virtual void SetLine_( const VECTOR2*, const VECTOR2* );
		virtual void GetLine_( VECTOR2*, VECTOR2* ) const;

	public:
		virtual void SetLine( const VECTOR2*, const VECTOR2* );
		virtual void SetLine( const VECTOR2&, const VECTOR2* );
		virtual void SetLine( const VECTOR2*, const VECTOR2& );
		virtual void SetLine( const VECTOR2&, const VECTOR2& );

		virtual void SetLine( const VECTOR2* );
		virtual void SetLine( const VECTOR2& );

		virtual void SetLine( float, float, float, float );
		virtual void SetLine( float, float );

	public:
		virtual void GetLine( VECTOR2*, VECTOR2* ) const;
		virtual void GetLine( VECTOR2*, UINT = 0 ) const;
		virtual auto GetLine( UINT = 0 ) const->VECTOR2;
		virtual void GetLine( float*, float*, float*, float* ) const;
		virtual void GetLine( float*, float* ) const;
	};
}

// End Of File
