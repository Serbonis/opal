#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "Attr/size.hpp"

//========================================
// �ۃ^�X�N
//========================================
class MARUX :
	public SIZE1
{
protected:
	explicit MARUX();

public:
	virtual ~MARUX();

protected:
	UINT	regular;

public:
	virtual void Init( void );
	virtual void Free( void );

protected:
	virtual void SetRegular( void );
	virtual void SetRegular( UINT );

public:
	virtual UINT GetRegular( void ) const;

protected:
	virtual void DrawInfo( int ) const;
};

// End Of File
