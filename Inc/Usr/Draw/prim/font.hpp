 #pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// フォントタスク
//========================================
#if defined ( OPAL_DX11 )
#include "font_dx11.hpp"
class FONT : public opal::dx11::FONT {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
