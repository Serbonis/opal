#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx11.hpp"
#include "rect3_dx.hpp"

//========================================
// ��`�^�X�N(3D)
//========================================
namespace opal::dx11 {
	class RECT3_;
	class RECT3 :
		public dx11::RECTX,
		public dx::RECT3
	{
	public:
		explicit RECT3();
		virtual ~RECT3();

	protected:
		SDEVICE<RECT3_>	device;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	private:
		virtual void DrawPrim( void ) override;

	public:
		using SIZE2::SetSize;
		virtual void SetSize( void ) override;
	};
}

// End Of File
