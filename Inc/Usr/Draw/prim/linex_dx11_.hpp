#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"

//========================================
// 直線タスク(共通)
//========================================
namespace opal::dx11 {
	class LINEX_ : public XDEVICE {
		friend class LINEX;

	public:
		explicit LINEX_( const void* = nullptr );
		virtual ~LINEX_();

	protected:
		const void* const	data;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual LINEX_* Context( void ) = 0;

	public:
		virtual void VertexMap( const void*, UINT, UINT = 0 ) const = 0;
		virtual UINT VertexStride( void ) const = 0;
		virtual UINT VertexOffset( void ) const = 0;

	protected:
		static ID3D11Buffer*				vbuffer;
		static ID3D11InputLayout*			layout;
		static ID3D11VertexShader*			vshader;
		static ID3D11PixelShader*			pshader;
		static ID3D11Buffer*				cbuffer;
	};
}

// End Of File
