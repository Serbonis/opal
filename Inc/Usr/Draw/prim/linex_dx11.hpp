#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"
#include "linex_dx.hpp"

//========================================
// 直線タスク(共通)
//========================================
namespace opal::dx11 {
	class LINEX_;
	class LINEX
	{
	protected:
		explicit LINEX();

	public:
		virtual ~LINEX();

	protected:
		D3D11_PRIMITIVE_TOPOLOGY	topology;

	protected:
		virtual void DrawPrint( void ) const;

	protected:
		virtual void DrawPrim( LINEX_*, const VECTOR2*, UINT, const void*, UINT ) const;
		virtual void DrawPrim( LINEX_*, const VECTOR3*, UINT, const void*, UINT ) const;
	};
}

// End Of File
