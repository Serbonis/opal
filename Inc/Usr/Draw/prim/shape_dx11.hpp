#pragma once
//========================================
// OPAL Game Library
//========================================
#include "shape_dx.hpp"
#include "polygon_dx11.hpp"

//========================================
// シェイプタスク
//========================================
namespace opal::dx11 {
	class SHAPE_;
	class SHAPE :
		public POLYGONX,
		public dx::SHAPE
	{
	public:
		explicit SHAPE();
		virtual ~SHAPE();

	protected:
		SDEVICE<SHAPE_>	device;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	private:
		virtual void DrawPrim( void ) override;
		virtual void DrawPrim( SHAPE_*, UINT, UINT, const void*, const POLYGON*, const DRAWB* );
	};
}

// End Of File
