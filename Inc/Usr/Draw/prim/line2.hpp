#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// 直線タスク(2D)
//========================================
#if defined ( OPAL_DX11 )
#include "line2_dx11.hpp"
class LINE2 : public opal::dx11::LINE2 {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
