#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "draw.hpp"

#include "Attr/size.hpp"

//========================================
// �g�^�X�N
//========================================
class WAKUX :
	public SIZE2
{
protected:
	explicit WAKUX();

public:
	virtual ~WAKUX();

public:
	virtual void Init( void );
	virtual void Free( void );

protected:
	virtual void DrawInfo( int ) const;
};

// End Of File
