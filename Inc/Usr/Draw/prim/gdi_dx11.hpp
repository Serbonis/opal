#pragma once
//========================================
// OPAL Game Library
//========================================
#include "gdi_dx.hpp"

//========================================
// GDI�^�X�N
//========================================
namespace opal::dx11 {
	class GDI_ ;
	class GDI :
		public dx::GDI
	{
	public:
		explicit GDI();
		virtual ~GDI();

	protected:
		SDEVICE<GDI_>	device;
		void*			data;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

		virtual void DeviceData( const void*[] ) override;
		virtual void DeviceData( void ) override;

	private:
		virtual void DrawPrim( void ) override;

	public:
		virtual void   SetCanvas( UINT, UINT ) override;
		virtual void   SetCanvas( UINT );
		virtual void   SetCanvas( void );
		virtual SSIZEU GetCanvas( void ) const override;

	private:
		virtual void DrawGDI( HDC, UINT, UINT ) override = 0;
	};

	using GDI_MAP = DEVICE_MAP<UINT,GDI_,DRAWX::DeviceLost,DRAWX::DeviceLost>;
}

// End Of File
