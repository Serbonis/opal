#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// 矩形タスク(2D)
//========================================
#if defined ( OPAL_DX11 )
#include "rect2_dx11.hpp"
class RECT2 : public opal::dx11::RECT2 {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
