#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rectx_dx.hpp"
#include "texture_dx11.hpp"

//========================================
// 矩形タスク(共通)
//========================================
namespace opal::dx11 {
	class RECTX_;
	class RECTX :
		public TEXTUREX
	{
	protected:
		explicit RECTX();

	public:
		virtual ~RECTX();

	protected:
		virtual void DrawPrint( void ) const;

	protected:
		virtual void DrawPrim( RECTX_*, UINT, UINT, const void*, const TEXTURE*, const DRAWB* );

	private:
		virtual void DrawPrimTexture( RECTX_*, const TEXTURE* );
		virtual void DrawPrimBlend(   RECTX_*, const DRAWB*   );
	};
}

// End Of File
