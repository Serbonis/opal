 #pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// フォントタスク
//========================================
#if defined ( OPAL_DX11 )
#include "gdi_dx11.hpp"
class GDI : public opal::dx11::GDI {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
