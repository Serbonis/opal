#pragma once
//========================================
// OPAL Game Library
//========================================
#include "texture_dx.hpp"

//========================================
// テクスチャーリソース
//========================================
namespace opal::dx11 {
	class TEXTURE_;
	class TEXTURE :
		public dx::TEXTURE
	{
	public:
		explicit TEXTURE();
		virtual ~TEXTURE();

	protected:
		PDEVICE<TEXTURE_>	device;
		void*				data;

	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual void Init( const std::string_view, float, float, float, float );
		virtual void Init( const std::string_view, float, float );

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual UINT Width(  void ) const override;
		virtual UINT Height( void ) const override;

	public:
		virtual TEXTURE_* Texture(  void ) const;
	};
}

namespace opal::dx11 {
#include "texture_dx.inl"
}

// End Of File
