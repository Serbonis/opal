#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// ポリゴンリソース
//========================================
#if defined ( OPAL_DX11 )
#include "polygon_dx11.hpp"
class POLYGON	: public opal::dx11::POLYGON  {};
class POLYGONX	: public opal::dx11::POLYGONX {};
class POLYGONH	: public opal::dx11::POLYGONH {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
