#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "texture_buffer.hpp"

//========================================
// TGA(Targa)ファイル
//========================================

//----------------------------------------
// データ読み込み
//----------------------------------------
namespace TGA {
	UINT LoadImage( const void*, size_t, TEXTURE_BUFFER& );
}

//----------------------------------------
// HEADER
//----------------------------------------
namespace TGA {
	struct HEADER {
		BYTE	ImageIDLength;		// 00:IDフィールド長さ(byte)
		BYTE	UseColorMap;		// 01:カラーマップ有無
		BYTE	ImageType;			// 02:画像形式
		WORD	PaletteOrigin;		// 03:カラーパレット開始番号
		WORD	PaletteLength;		// 05:カラーパレット個数
		BYTE	PaletteBitDepth;	// 07:カラーパレットビット数(16:16ビット/24:24ビット/32:32ビット)
		WORD	XOrigin;			// 08:画像X座標
		WORD	YOrigin;			// 10:画像Y座標
		WORD	Width;				// 12:画像幅
		WORD	Height;				// 14:画像高
		BYTE	ColorBitDepth;		// 16:色深度(16/24/32bit)
		BYTE	Descriptor;			// 17:その他
	};

	enum IMAGETYPE {
		IMAGETYPE_00	= 0,	// 00:イメージ無し
		IMAGETYPE_01	= 1,	// 01:インデックスカラー(256色)	非圧縮
		IMAGETYPE_02	= 2,	// 02:フルカラー				非圧縮
		IMAGETYPE_03	= 3,	// 03:モノクロ					非圧縮
		IMAGETYPE_09	= 9,	// 09:インデックスカラー		RLE圧縮
		IMAGETYPE_10	= 10,	// 10:フルカラー				RLE圧縮
		IMAGETYPE_11	= 11,	// 11:モノクロ					RLE圧縮
	};

	enum DESCRIPTOR {
		DESCRIPTOR_0_3	= (1<<3)|(1<<2)|(1<<1)|(1<<0),	// bit 0-3	属性(16bit->0|1,24bit->0,32bit->8)
		DESCRIPTOR_4	= (1<<4),						// bit 4	格納方向(0:→|1:←)
		DESCRIPTOR_5	= (1<<5),						// bit 5	格納方向(0:↑|1:↓)
		DESCRIPTOR_6_7	= (1<<7)|(1<<6),				// bit 6-7  未使用
	};
	enum COLORDEPTH {
		COLORDEPTH_16	= 16,
		COLORDEPTH_24	= 24,
		COLORDEPTH_32	= 32,
	};
}

//----------------------------------------
// FOOTER
//----------------------------------------
namespace TGA {
	struct FOOTER {
		DWORD	ExtensionOffset;	// 拡張情報のアドレス
		DWORD	DeveloperOffset;	// 開発者領域のアドレス
		char	Signature[18];		// "TRUEVISION-XFILE"
	};

	struct EXTENSION {
		WORD 		Size;				// フッタ(495固定)
		BYTE 		Creator[41];		// 作者名(未使用時は 0 で埋める)
		BYTE 		Comment[4][81];		// 作者名や時刻、そのほかの情報
		WORD		CreationDateMonth;	// 作成日 月 1〜12
		WORD		CreationDateDay;	// 作成日 日 1〜31
		WORD		CreationDateYear;	// 作成日 年 4桁( 2004等 )
		WORD		CreationDateHour;	// 作成日 時 0〜23
		WORD		CreationDateMinute;	// 作成日 分 0〜59
		WORD		CreationDateSecond;	// 作成日 秒 0〜59
		BYTE 		JobName[41];		// 仕事名(未使用時は 0 で埋める)
		WORD		CreationTimeHours;	// 作成時刻 時 0-65535
		WORD		CreationTimeMinute;	// 作成時刻 分 0-59
		WORD		CreationTimeSecond;	// 作成時刻 秒 0-59
		BYTE		SoftID[41];			// ソフトウエアのID(未使用時は 0 で埋める)
		WORD		VersionNumber;		// バージョンナンバー(1.01 だったら 101)
		BYTE		VersionRot;			// ロット(アスキーコード)
		DWORD		ColorKey;			// カラーキー
		WORD		AcpectX;			// ドットのアスペクト比 X (0:無効)
		WORD		AcpectY;			// ドットのアスペクト比 Y (0:無効)
		WORD		GammaN;				// ガンマ値 0.0-10.0
		WORD		GammaD;				// ガンマ値 (0:ガンマ指定無し)
		DWORD		ColorTableOffset;	// カラーコレクションテーブルの位置 (0:省略)
		DWORD		StampTableOffset;	// スタンプテーブルの位置 (0:省略)
		DWORD		ScanTableOffset;	// スキャンテーブルの位置 (0:省略)
		BYTE		AttributeType;		// アトリビュートタイプ
	};

	enum ATTRIBUTE {
		ATTRIBUTE_0		= (1<<0),			// 0:αデータ無し
		ATTRIBUTE_1_2	= (1<<2)|(1<<1),	// 1:αデータ無効
		ATTRIBUTE_3		= (1<<3),			// 3:α有効
		ATTRIBUTE_4		= (1<<4),			// 4:pre-multiplied αデータ
	};
}

// End Of File
