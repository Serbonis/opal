#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// テクスチャーリソース
//========================================
#if defined ( OPAL_DX11 )
#include "texture_dx11.hpp"
class TEXTURE	: public opal::dx11::TEXTURE  {};
class TEXTUREX	: public opal::dx11::TEXTUREX {};
class TEXTUREH	: public opal::dx11::TEXTUREH {};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
