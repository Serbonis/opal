#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// テクスチャー転送バッファ
//========================================
struct TEXTURE_BUFFER {
	TEXTURE_BUFFER();
	~TEXTURE_BUFFER();

	BYTE*	bp;		// pData
	UINT	ms;		// MipSlice
	UINT	as;		// ArraySlice
	UINT	ml;		// MipLevels
	UINT	tw;		// Texture Width
	UINT	th;		// Texture Height
	UINT	rp;		// RowPitch
	UINT	dp;		// DepthPitch

public:
	BYTE* operator ()( UINT, UINT, UINT = sizeof( uint32_t ), UINT = 1 );
	BYTE* operator ()( void );
};

// End Of File
