#pragma once
//========================================
// OPAL Game Library
//========================================
#include "polygon_dx.hpp"

//========================================
// ポリゴンリソース
//========================================
namespace opal::dx11 {
	class POLYGON_;
	class POLYGON :
		public dx::POLYGON
	{
	public:
		explicit POLYGON();
		virtual ~POLYGON();

	protected:
		PDEVICE<POLYGON_>	device;
		void*				data;

	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual void Init( const char*,        const void* );
		virtual void Init( const std::string&, const void* );
		virtual void Init( std::nullptr_t,     const void* );

	protected:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual POLYGON_* Polygon(  void ) const;
	};
}

namespace opal::dx11 {
#include "polygon_dx.inl"
}

// End Of File
