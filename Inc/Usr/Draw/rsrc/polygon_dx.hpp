#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "draw.hpp"

//========================================
// ポリゴンリソース
//========================================
namespace opal::dx {
	class POLYGON :
		public RSRC,
		public XDEVICE
	{
	public:
		explicit POLYGON();
		virtual ~POLYGON();

	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
		virtual void Free( void ) override;
	};
}

// End Of File
