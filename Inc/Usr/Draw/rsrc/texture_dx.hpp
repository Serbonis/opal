#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "draw.hpp"

//========================================
// テクスチャーリソース
//========================================
namespace opal::dx {
	class TEXTURE :
		public RSRC,
		public XDEVICE
	{
	public:
		explicit TEXTURE();
		virtual ~TEXTURE();

	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual UINT Width(  void ) const = 0;
		virtual UINT Height( void ) const = 0;
	};
}

// End Of File
