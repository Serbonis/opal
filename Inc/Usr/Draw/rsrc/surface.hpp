#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// サーフェイスリソース
//========================================
#if defined ( OPAL_DX11 )
#include "surface_dx11.hpp"
using SURFACE_LOCK = opal::dx11::SURFACE_LOCK;
class SURFACE	: public opal::dx11::SURFACE {};
class RENDER	: public opal::dx11::RENDER { public: using opal::dx11::RENDER::RENDER;	};
class CANVAS	: public opal::dx11::CANVAS { public: using opal::dx11::CANVAS::CANVAS;	};
class IDXGDI	: public opal::dx11::IDXGDI { public: using opal::dx11::IDXGDI::IDXGDI;	};
class SHADER	: public opal::dx11::SHADER { public: using opal::dx11::SHADER::SHADER;	};
#else
#error "描画システムが定義されていません"
#endif

// End Of File
