#pragma once
//========================================
// OPAL Game Library
//========================================
#include "surface_dx.hpp"
#include "texture_dx11.hpp"

#include <d3d11.h>

//========================================
// サーフェイスリソース
//========================================
namespace opal::dx11 {
	struct LOCKED : D3D11_MAPPED_SUBRESOURCE {};
	class SURFACE_LOCK;

	class SURFACE_;
	class SURFACE :
		public dx::SURFACE,
		public TEXTURE
	{
	public:
		explicit SURFACE( UINT, UINT );
		explicit SURFACE();
		virtual ~SURFACE();

	protected:
		PDEVICE<SURFACE_>	device;
		void*				data;

	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

		virtual void DeviceData( const void* ) override;
		virtual void DeviceData( void ) override;

	public:
		virtual UINT Width(  void ) const override;
		virtual UINT Height( void ) const override;

	public:
		virtual TEXTURE_* Texture( void ) const override;

	public:
		virtual void Create( const char*, UINT, UINT, DXGI_FORMAT, const char* = nullptr );
		virtual void Create( const char*, DXGI_FORMAT, const char* = nullptr );
		virtual void Create( DXGI_FORMAT, const char* = nullptr );
		virtual void Destroy( void );

	public:
		virtual void Begin( DWORD, float = 1.0f, DWORD = 0 );
		virtual void Begin( void );
		virtual void End(   void );

	public:
		virtual void Clear( UINT, UINT, UINT, UINT, DWORD = BLANK );
		virtual void Clear( UINT, UINT, DWORD = BLANK );
		virtual void Clear( DWORD = BLANK );

	public:
		virtual bool Lock( LOCKED&, UINT = 0 ) const;
		virtual auto Lock( UINT = 0 ) const->std::shared_ptr<LOCKED>;
		virtual void UnLock( UINT = 0 ) const;
	};
}

//========================================
// SURFACE LOCK
//========================================
namespace opal::dx11 {
	class SURFACE_LOCK {
	public:
		explicit SURFACE_LOCK( const SURFACE*, UINT = 0 );
		explicit SURFACE_LOCK( const SURFACE&, UINT = 0 );
		explicit SURFACE_LOCK( const std::shared_ptr<SURFACE>, UINT = 0 );
		explicit SURFACE_LOCK();
		virtual ~SURFACE_LOCK();

	protected:
		const SURFACE* 	sp;
		UINT			id;
		LOCKED			rc;

	public:
		virtual bool Lock( const SURFACE*, UINT = 0 );
		virtual bool Lock( const SURFACE&, UINT = 0 );
		virtual void UnLock( void );

	public:
		virtual operator bool( void ) const;

	public:
		virtual void* Data(  void ) const;
		virtual UINT  Pitch( void ) const;
		virtual UINT  Depth( void ) const;

	public:
		virtual UINT Width(  void ) const;
		virtual UINT Height( void ) const;

	public:
		virtual DWORD* operator []( UINT ) const;
		virtual DWORD* operator ()( UINT = 0, UINT = 0 ) const;

	public:
		virtual void SetPixel( UINT, UINT, UINT, UINT, const DWORD* );
		virtual void SetPixel( UINT, UINT, UINT, UINT, DWORD );
		virtual void SetPixel( UINT, UINT, DWORD );
		virtual auto GetPixel( UINT, UINT ) const->DWORD;
	};
}

//========================================
// RENDER
//========================================
namespace opal::dx11 {
	class RENDER : public SURFACE {
	public:
		virtual void Init( const std::string_view, UINT, UINT );
		virtual void Init( const std::string_view, UINT );
		virtual void Init( const std::string_view = {} );
		virtual void Init( UINT, UINT );
		virtual void Init( UINT );

		virtual void Free( void ) override;
	};
}

//========================================
// CANVAS
//========================================
namespace opal::dx11 {
	class CANVAS : public SURFACE {
	public:
		using SURFACE::SURFACE;

	public:
		virtual void Init( const std::string_view, UINT, UINT );
		virtual void Init( const std::string_view, UINT );
		virtual void Init( const std::string_view = {} );

		virtual void Init( UINT, UINT );
		virtual void Init( UINT );

		virtual void Free( void ) override;

	public:
		using SURFACE::Begin;
		virtual void Begin( DWORD, float = 1.0f, DWORD = 0 ) override;
	};
}

//========================================
// IDXGDI
//========================================
namespace opal::dx11 {
	class IDXGDI : public SURFACE {
	public:
		using SURFACE::SURFACE;

	public:
		virtual void Init( const std::string_view, UINT, UINT );
		virtual void Init( const std::string_view, UINT );
		virtual void Init( const std::string_view = {} );
		virtual void Init( UINT, UINT );
		virtual void Init( UINT );

		virtual void Free( void ) override;
	};
}

//========================================
// SHADER
//========================================
namespace opal::dx11 {
	class SHADER : public SURFACE {
	public:
		using SURFACE::SURFACE;

	public:
		virtual void Init( const std::string_view, UINT, UINT );
		virtual void Init( const std::string_view, UINT );
		virtual void Init( const std::string_view = {} );
		virtual void Init( UINT, UINT );
		virtual void Init( UINT );

		virtual void Free( void ) override;
	};
}

// End Of File
