#pragma once
//========================================
// OPAL Game Library
//========================================
#include "surface_dx11.hpp"
#include "texture_dx11_.hpp"

//========================================
// サーフェイスリソース
//========================================
namespace opal::dx11 {
	class SURFACE_ : public TEXTURE_ {
	public:
		using CREATE_SURFACE = DRAWX_DX11::CREATE_SURFACE;

	public:
		explicit SURFACE_( const CREATE_SURFACE* = nullptr );
		virtual ~SURFACE_();

	protected:
		const CREATE_SURFACE*	data;

	protected:
		ID3D11RenderTargetView*	target[2];
		ID3D11DepthStencilView*	stencil[2];
		ID3D11Texture2D*		buffer;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual void Begin( COLOR, float = 1.0f, DWORD = 0 );
		virtual void Begin( void );
		virtual void End(   void );

	public:
		virtual void Clear( UINT, UINT, UINT, UINT, COLOR );

	public:
		virtual bool Lock( LOCKED&, UINT = 0 );
		virtual void UnLock( UINT = 0 );
	};
}

// End Of File
