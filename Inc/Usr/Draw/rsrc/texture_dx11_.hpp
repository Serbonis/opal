#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"
#include "texture_dx11.hpp"

//========================================
// テクスチャーリソース
//========================================
namespace opal::dx11 {
	class TEXTURE_ : public XDEVICE {
	public:
		using CREATE_TEXTURE = DRAWX_DX11::CREATE_TEXTURE;

	public:
		explicit TEXTURE_( const CREATE_TEXTURE* = nullptr );
		virtual ~TEXTURE_();

	protected:
		const CREATE_TEXTURE* const		data;

	protected:
		ID3D11Texture2D*			texture;
		D3D11_TEXTURE2D_DESC		desc;
		ID3D11ShaderResourceView*	resource;
		ID3D11SamplerState*			sampler;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual UINT Width(  void ) const;
		virtual UINT Height( void ) const;

	public:
		virtual ID3D11Texture2D*&          Texture(  void );
		virtual ID3D11ShaderResourceView*& Resource( void );
		virtual ID3D11SamplerState*&       Sampler(  void );
	};
}

// End Of File
