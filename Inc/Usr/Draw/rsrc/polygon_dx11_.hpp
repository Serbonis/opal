#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw_dx11.hpp"
#include "polygon_dx11.hpp"

//========================================
// ポリゴンリソース
//========================================
namespace opal::dx11 {
	class POLYGON_ : public XDEVICE {
	public:
		using CREATE_POLYGON = DRAWX_DX11::CREATE_POLYGON;

	public:
		explicit POLYGON_( const CREATE_POLYGON* = nullptr );
		virtual ~POLYGON_();

	protected:
		const CREATE_POLYGON* const		data;

	protected:
		ID3D11Buffer*	ibuffer;
		ID3D11Buffer*	vbuffer;

		D3D11_PRIMITIVE_TOPOLOGY	topology;
		size_t						index;

	public:
		virtual int InitDevice( int ) override;
		virtual int FreeDevice( int ) override;

	public:
		virtual ID3D11Buffer*& Ibuffer( void );
		virtual ID3D11Buffer*& Vbuffer( void );

	public:
		virtual auto Topology( void ) const->D3D11_PRIMITIVE_TOPOLOGY;
		virtual void Topology( D3D11_PRIMITIVE_TOPOLOGY );
		virtual auto Index( void ) const->size_t;
		virtual void Index( size_t );
	};
}

// End Of File
