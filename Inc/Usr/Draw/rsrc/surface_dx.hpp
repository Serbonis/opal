#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"
#include "rsrc.hpp"

//========================================
// サーフェイスリソース
//========================================
namespace opal::dx {
	class SURFACE {
	public:
		explicit SURFACE();
		virtual ~SURFACE();

	protected:
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) = 0;
		virtual void Free( void ) = 0;

	public:
		virtual UINT Width(  void ) const = 0;
		virtual UINT Height( void ) const = 0;
	};
}

// End Of File
