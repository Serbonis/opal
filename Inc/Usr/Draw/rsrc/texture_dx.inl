//========================================
// �ԍ������^
//========================================
class TEXTUREX : public opal::RSRCBV<TEXTURE> {
public:
	explicit TEXTUREX();
	virtual ~TEXTUREX();

public:
	virtual UINT Textures( void ) const;

	virtual void NowTexture( UINT );
	virtual UINT NowTexture( void ) const;

	virtual void SetTexture( const TEXTURE*, UINT );
	virtual void SetTexture( const TEXTURE* = nullptr );
	virtual void SetTexture( const TEXTURE&, UINT );
	virtual void SetTexture( const TEXTURE& );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>&, UINT );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>& );

	virtual const TEXTURE* GetTexture( UINT ) const;
	virtual const TEXTURE* GetTexture( void ) const;

	virtual TEXTURE_* Texture( UINT ) const;
	virtual TEXTURE_* Texture( void ) const;
};

//========================================
// ���O�����^
//========================================
class TEXTUREH : public opal::RSRCBM<TEXTURE> {
public:
	explicit TEXTUREH();
	virtual ~TEXTUREH();

public:
	virtual UINT Textures( void ) const;

	virtual void NowTexture( const char* );
	virtual void NowTexture( const std::string& );
	virtual void NowTexture( const std::nullptr_t );
	virtual const char* NowTexture( void ) const;

	virtual void SetTexture( const TEXTURE*, const char* );
	virtual void SetTexture( const TEXTURE*, const std::string& );
	virtual void SetTexture( const TEXTURE*, std::nullptr_t );
	virtual void SetTexture( const TEXTURE* = nullptr );
	virtual void SetTexture( const TEXTURE&, const char* );
	virtual void SetTexture( const TEXTURE&, const std::string& );
	virtual void SetTexture( const TEXTURE&, std::nullptr_t );
	virtual void SetTexture( const TEXTURE& );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>&, const char* );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>&, const std::string& );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>&, std::nullptr_t );
	virtual void SetTexture( const std::shared_ptr<TEXTURE>& );

	virtual const TEXTURE* GetTexture( const char* ) const;
	virtual const TEXTURE* GetTexture( const std::string& ) const;
	virtual const TEXTURE* GetTexture( std::nullptr_t ) const;
	virtual const TEXTURE* GetTexture( void ) const;

	virtual TEXTURE_* Texture( const char* ) const;
	virtual TEXTURE_* Texture( const std::string& ) const;
	virtual TEXTURE_* Texture( const std::nullptr_t ) const;
	virtual TEXTURE_* Texture( void ) const;
};
