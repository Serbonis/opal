#pragma once
//========================================
// OPAL Game Library
//========================================
#include "polygon_dx11.hpp"

//========================================
// �|���S�����\�[�X
//========================================
//----------------------------------------
// ��
//----------------------------------------
namespace opal::dx11 {
	class POLYGON_TRIANGLE : public dx11::POLYGON {
	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
	};
}

#if defined ( OPAL_DX11 )
class POLYGON_TRIANGLE : public opal::dx11::POLYGON_TRIANGLE{};
#endif

//----------------------------------------
// ��
//----------------------------------------
namespace opal::dx11 {
	class POLYGON_STAR : public dx11::POLYGON {
	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;
	};
}

#if defined ( OPAL_DX11 )
class POLYGON_STAR : public opal::dx11::POLYGON_STAR{};
#endif

//----------------------------------------
// ��
//----------------------------------------
namespace opal::dx11 {
	class POLYGON_CIRCLE : public dx11::POLYGON {
	public:
		using RSRC::Init;
		virtual void Init( const void*, size_t, const char* = nullptr, const void* = nullptr ) override;

	public:
		virtual void SetArch( UINT );
		virtual auto GetArch( void ) const->UINT;
	};
}

#if defined ( OPAL_DX11 )
class POLYGON_CIRCLE : public opal::dx11::POLYGON_CIRCLE{};
#endif

// End Of File
