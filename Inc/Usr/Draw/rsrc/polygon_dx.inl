//========================================
// �ԍ������^
//========================================
class POLYGONX : public opal::RSRCBV<POLYGON> {
public:
	explicit POLYGONX();
	virtual ~POLYGONX();

public:
	virtual UINT Polygons( void ) const;

	virtual void NowPolygon( UINT );
	virtual UINT NowPolygon( void ) const;

	virtual void SetPolygon( const POLYGON*, UINT );
	virtual void SetPolygon( const POLYGON* = nullptr );
	virtual void SetPolygon( const POLYGON&, UINT );
	virtual void SetPolygon( const POLYGON& );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>&, UINT );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>& );

	virtual const POLYGON* GetPolygon( UINT ) const;
	virtual const POLYGON* GetPolygon( void ) const;

	virtual POLYGON_* Polygon( UINT ) const;
	virtual POLYGON_* Polygon( void ) const;
};

//========================================
// ���O�����^
//========================================
class POLYGONH : public opal::RSRCBM<POLYGON> {
public:
	explicit POLYGONH();
	virtual ~POLYGONH();

public:
	virtual UINT Polygons( void ) const;

	virtual void NowPolygon( const char* );
	virtual void NowPolygon( const std::string& );
	virtual void NowPolygon( const std::nullptr_t );
	virtual const char* NowPolygon( void ) const;

	virtual void SetPolygon( const POLYGON*, const char* );
	virtual void SetPolygon( const POLYGON*, const std::string& );
	virtual void SetPolygon( const POLYGON*, std::nullptr_t );
	virtual void SetPolygon( const POLYGON* = nullptr );
	virtual void SetPolygon( const POLYGON&, const char* );
	virtual void SetPolygon( const POLYGON&, const std::string& );
	virtual void SetPolygon( const POLYGON&, std::nullptr_t );
	virtual void SetPolygon( const POLYGON& );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>&, const char* );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>&, const std::string& );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>&, std::nullptr_t );
	virtual void SetPolygon( const std::shared_ptr<POLYGON>& );

	virtual const POLYGON* GetPolygon( const char* ) const;
	virtual const POLYGON* GetPolygon( const std::string& ) const;
	virtual const POLYGON* GetPolygon( std::nullptr_t ) const;
	virtual const POLYGON* GetPolygon( void ) const;

	virtual POLYGON_* Polygon( const char* ) const;
	virtual POLYGON_* Polygon( const std::string& ) const;
	virtual POLYGON_* Polygon( const std::nullptr_t ) const;
	virtual POLYGON_* Polygon( void ) const;
};
