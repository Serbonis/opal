#pragma once
//========================================
// OPAL User Extension
//========================================
#include "draw.hpp"

//========================================
// カメラタスク
//========================================
class CAMERA : public opal::CAMERAX, public opal::DRAWT3 {
	//----------------------------------------
	// constructor & destructor
	//----------------------------------------
public:
	explicit CAMERA() = default;
	virtual ~CAMERA() = default;

	//----------------------------------------
	// member functions
	//----------------------------------------
public:
	virtual void Init( const char* pName = nullptr ) override;
	virtual void Free( void ) override;

protected:
	virtual void DrawMain( void ) override;

public:
	static CAMERAX* Update( CAMERAX*, const DRAWT3* );
};

// End Of File
