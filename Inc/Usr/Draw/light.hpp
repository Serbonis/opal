#pragma once
//========================================
// OPAL User Extension
//========================================
#include "draw.hpp"

//========================================
// ライトタスク
//========================================
class LIGHT : public opal::LIGHTX, public opal::DRAWT3 {
	//----------------------------------------
	// constructor & destructor
	//----------------------------------------
public:
	explicit LIGHT() = default;
	virtual ~LIGHT() = default;

	//----------------------------------------
	// member functions
	//----------------------------------------
public:
	virtual void Init( const char* pName = nullptr ) override;
	virtual void Free( void ) override;

protected:
	virtual void DrawMain( void ) override;
};

// End Of File
