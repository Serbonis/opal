#pragma once
//========================================
// 頂点シェーダー→ピクセルシェーダー
//========================================
#include "ps_com.fxh"

#if defined HLSL
struct PS_INPUT
{
	float4	pos : SV_POSITION;
	float4	col : COLOR;
};

struct PS_OUTPUT
{
	float4	col : COLOR;
};
#else
#endif

// End Of File
