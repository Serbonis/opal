#pragma once
//========================================
// メインプログラム→頂点シェーダー
//========================================
#include "vs_com.fxh"

#if defined HLSL
struct VS_INPUT {
	float3	pos : POSITION;
};

#define VS_OUTPUT	PS_INPUT
#else
struct FVF_VIT {
	float3	pos;
};

#define VS_ELEMENT_S0	"POSITION"
#define VS_ELEMENT_F0	DXGI_FORMAT_R32G32B32_FLOAT
#endif

// End Of File
