#pragma once
//========================================
// xs_com.hpp
//========================================
#include "Ext/hlsl.hpp"
#include "Ext/xmath.hpp"

#include "draw.hpp"

//----------------------------------------
// CONST BUFFER for 2D Object
//----------------------------------------
namespace opal {
	template<typename OBJECT, typename BUFFER>auto CONST_BUFFER_2D( const OBJECT* object, BUFFER& cbuff )->BUFFER*{

		const auto	wd = object->World();
		const auto	tv = TransMatrix( wd )+object->GetOffset();
		const auto	pv = object->GetPivot();
		const auto	im = hlsl::Scale( wd, DRAWX::GetViewportSize() );
		const auto	iv = hlsl::Trans( tv, DRAWX::VportInv() );
		const auto	mw = hlsl::World( im, VECTOR3{iv,object->GetLayer()} );
		const auto	vp = hlsl::Pivot( pv );

		cbuff.World 	= mw;
		cbuff.Pivot		= vp;
		cbuff.Diffuse	= { object->R(),object->G(),object->B(),object->A() };

		return &cbuff;
	}
}

//----------------------------------------
// CONST BUFFER for 3D Object
//----------------------------------------
namespace opal {
	template<typename OBJECT, typename BUFFER>auto CONST_BUFFER_3D( const OBJECT* object, BUFFER& cbuff )->BUFFER*{

		cbuff.World		= object->World();
		cbuff.Proj		= object->Proj();
		cbuff.View		= object->View();
		cbuff.Pivot		= object->GetPivot();
		cbuff.Diffuse	= { object->R(),object->G(),object->B(),object->A() };

		return &cbuff;
	}
}
// Edn OF File
