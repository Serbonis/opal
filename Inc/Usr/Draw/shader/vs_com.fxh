#pragma once
//========================================
// 頂点シェーダー共通
//========================================
#if defined HLSL
#else
#define VS_HLSL(s)	vs_##s

#define VS_ELEMENT_DESC			D3D11_INPUT_ELEMENT_DESC
#define VS_ELEMENT( s, n, f )	VS_ELEMENT_DESC{ s, n, f, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0	}
#endif

// End Of File
