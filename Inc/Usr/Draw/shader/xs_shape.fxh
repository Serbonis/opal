#pragma once
//========================================
// レジスター
//========================================
#include "xs_com.fxh"

CONST_BUFFER( SHAPE ) : CB_REGISTER( 0 )
{
	matrix	World;
	vector	Pivot;
	vector	Diffuse;
};

// End Of File
