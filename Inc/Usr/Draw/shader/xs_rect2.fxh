#pragma once
//========================================
// レジスター
//========================================
#include "xs_com.fxh"

CONST_BUFFER( RECT2 ) : CB_REGISTER( 0 )
{
	matrix	World;
	vector	Pivot;
	matrix	Sampler;
	vector	Diffuse[4];
};

TEXURE_2D(     RECT2 ) : TX_REGISTER( 0 );
SAMPLER_STATE( RECT2 ) : SS_REGISTER( 0 );

// End Of File
