#pragma once
//========================================
// メインプログラム→頂点シェーダー
//========================================
#include "vs_com.fxh"

#if defined HLSL
struct VS_INPUT {
	float3	pos : POSITION;
	uint	idx : INDEX;
	float2	tex	: TEXCOORD;
};

#define VS_OUTPUT	PS_INPUT
#else
struct FVF_VIT {
	float3	pos;
	uint	idx;
	float2	tex;
};

#define VS_ELEMENT_S0	"POSITION"
#define VS_ELEMENT_F0	DXGI_FORMAT_R32G32B32_FLOAT

#define VS_ELEMENT_S1	"INDEX"
#define VS_ELEMENT_F1	DXGI_FORMAT_R32_UINT

#define VS_ELEMENT_S2	"TEXCOORD"
#define VS_ELEMENT_F2	DXGI_FORMAT_R32G32_FLOAT
#endif

// End Of File
