#pragma once
//========================================
// レジスター
//========================================
#if defined HLSL
#define CONST_BUFFER( s )		cbuffer			CONST_BUFFER_##s
#define TEXURE_2D( s )			Texture2D		TEXTURE_2D_##s
#define SAMPLER_STATE( s )		SamplerState	SAMPLER_STATE_##s

#define CB_REGISTER( n )		register( b##n )
#define TX_REGISTER( n )		register( t##n )
#define SS_REGISTER( n )		register( s##n )
#else
#define CONST_BUFFER( s )		struct CONST_BUFFER_##s
#define TEXURE_2D( s )			struct TEXTURE_2D_##s
#define SAMPLER_STATE( s )		struct SAMPLER_STATE_##s

#define CB_REGISTER( n )		cb_register
#define TX_REGISTER( n )		tx_register {}
#define SS_REGISTER( n )		ss_register {}
#endif

// End Of File
