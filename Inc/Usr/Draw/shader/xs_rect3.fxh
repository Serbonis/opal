#pragma once
//========================================
// レジスター
//========================================
#include "xs_com.fxh"

CONST_BUFFER( RECT3 ) : CB_REGISTER( 0 )
{
	matrix	World;
	vector	Pivot;
	matrix	Sampler;
	vector	Diffuse[4];

	matrix	Proj;
	matrix	View;
	matrix	Axis;
};

TEXURE_2D(     RECT3 ) : TX_REGISTER( 0 );
SAMPLER_STATE( RECT3 ) : SS_REGISTER( 0 );

// End Of File
