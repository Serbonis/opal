#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// サービスシステム
//========================================
namespace opal {
	class SRVC;
	class SRVCX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class SRVC;
		friend class SRVCS;

	private:
		using SMAP = std::unordered_map<std::string,SRVC*>;
		using SKEY = std::vector<std::string>;
		using SVAL = std::vector<SRVC*>;

		static SMAP	smap;
		static SKEY	skey;
		static SVAL	sval;

	private:
		static bool SysInit( void );
		static bool SysExecBefore( void );
		static bool SysExecAfter(  void );
		static bool SysFree( void );

	private:
		static SRVC* Search( const char* );
		static SVAL* Search( void );
		static void  Clear( const char* = nullptr );
		static void  Sort( bool = false );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "srvcx.hpp"
#include "srvcs.hpp"

// End Of File
