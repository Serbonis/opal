#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xdraw.hpp"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <d3d11sdklayers.h>	// IID_D3D11Debug

//========================================
// グラフィックシステム
//========================================
//----------------------------------------
// パラメーター
//----------------------------------------
namespace opal::DRAWX_DX11 {
	class PARAMETERS : public DXGI_SWAP_CHAIN_DESC {
	public:
		explicit PARAMETERS();
		virtual ~PARAMETERS();

	public:
		virtual DXGI_SWAP_CHAIN_DESC& operator ()( HWND, UINT, UINT );
		virtual DXGI_SWAP_CHAIN_DESC& operator ()( HWND );
	};
}

//----------------------------------------
// デバイス
//----------------------------------------
namespace opal::DRAWX_DX11 {
	class DEVICE {
		friend void Device( void );

	public:
		explicit DEVICE();
		virtual ~DEVICE();

	private:
		ID3D11Device*				device;		// デバイス
		ID3D11Debug* 				debug;		// デバッグレポート
		ID3D11DeviceContext*		context;	// デバイスコンテキスト
		IDXGISwapChain*				swapchain;	// スワップチェーン
		ID3D11Texture2D*			texture;
		ID3D11RenderTargetView*		target;		// レンダーターゲット
		ID3D11Texture2D*			buffer;
		ID3D11DepthStencilView*		stencil;	// 深度バッファ
		D3D_FEATURE_LEVEL          	feature;	// 機能レベル
		HWND						window;
		PARAMETERS					param;
		D3D11_TEXTURE2D_DESC 		desc;
		DEVICE_LOST					lost;

	private:
		struct STATE final {
			ID3D11RasterizerState*						raster;
			ID3D11DepthStencilState*					stencil;
			std::unordered_map<UINT,ID3D11BlendState*>	blend;
		}	state;

	private:
		static const D3D_FEATURE_LEVEL	features[];
		static UINT Features( void );

	public:
		virtual ID3D11Device*&           operator ()( void );
		virtual ID3D11DeviceContext*&    operator ()( UINT );
		virtual ID3D11RenderTargetView*& Target(    void );
		virtual ID3D11DepthStencilView*& Stencil(   void );
		virtual IDXGISwapChain*&		 SwapChain( void );

	public:
		virtual void Init( HWND, UINT, UINT );
		virtual void Free( void );

	public:
		virtual void Begin(   void );
		virtual void End(     void );
		virtual void Present( void );

	public:
		virtual UINT Width(  void ) const;
		virtual UINT Height( void ) const;

	public:
		virtual void DeviceLost( const DEVICE_NAME, XDEVICE* );
		virtual void DeviceLost( const DEVICE_NAME );
		virtual UINT DeviceLost( void ) const;

	public:
		virtual void ResetState( void );
		virtual void ResetRasterizerState(   void );
		virtual void ResetDepthStencilState( void );
		virtual void ResetBlendState(        void );

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( void ) const;
		static const char* Features( D3D_FEATURE_LEVEL );
#endif
	};
}

//----------------------------------------
// ビューポート
//----------------------------------------
namespace opal::DRAWX_DX11 {
	struct VIEWPORT : D3D11_VIEWPORT, IVIEWPORT {
		using D3D11_VIEWPORT::D3D11_VIEWPORT;

		explicit VIEWPORT();
		explicit VIEWPORT( float, float );
		explicit VIEWPORT( float, float, float, float );

		VIEWPORT& operator ()( float, float );
		VIEWPORT& operator ()( float, float, float, float );

		MATRIX* Matrix(  MATRIX* ) const override;
		MATRIX* Inverse( MATRIX* ) const override;
	};

	class VIEW_PORT {
	public:
		explicit VIEW_PORT();
		virtual ~VIEW_PORT();

	private:
		VIEWPORT				port;
		VIEWPORT				save;
		std::vector<VIEWPORT>	back;

	private:
		MATRIX	matrix;
		MATRIX	inverse;

	public:
		virtual void Init( DEVICE& dv );
		virtual void Free( void );

	public:
		virtual void Begin( void );
		virtual void End(   void );
		virtual void Begin( DEVICE& dv );
		virtual void End(   DEVICE& dv );

	public:
		virtual const MATRIX* Matrix(  void ) const final;
		virtual const MATRIX* Inverse( void ) const final;

	public:
		virtual void Set( const SRECTF& );
		virtual void Set( float, float, float, float );
		virtual void Set( const VECTOR2&, float, float );
		virtual void Set( float, float, const SSIZE& );
		virtual void Set( const VECTOR2&, const SSIZE& );
		virtual void Set( void );
		virtual auto Get( void ) const->SRECTF;

		virtual void SetPosition( float, float );
		virtual void SetPosition( const VECTOR2& );
		virtual void SetPosition( void );
		virtual auto GetPosition( void ) const->VECTOR2;

		virtual void SetSize( float, float );
		virtual void SetSize( const SSIZE& );
		virtual void SetSize( void );
		virtual auto GetSize( void ) const->SSIZE;

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( void ) const;
#endif
	};
}

//----------------------------------------
// シザー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	struct SCISSOR : D3D11_RECT {
		using D3D11_RECT::D3D11_RECT;

		explicit SCISSOR();
		explicit SCISSOR( float, float );
		explicit SCISSOR( float, float, float, float );

		static void Clear( DEVICE& dv );

		static void Set( const SRECTF& );
		static void Set( float, float, float, float );
		static void Set( const VECTOR2&, float, float );
		static void Set( float, float, const SSIZE& );
		static void Set( const VECTOR2&, const SSIZE& );
		static void Set( void );
		static auto Get( void )->SRECTF;

		static void SetPosition( float, float );
		static void SetPosition( const VECTOR2& );
		static void SetPosition( void );
		static auto GetPosition( void )->VECTOR2;

		static void SetSize( float, float );
		static void SetSize( const SSIZE& );
		static void SetSize( void );
		static auto GetSize( void )->SSIZE;
	};
}

//----------------------------------------
// バッファクリア
//----------------------------------------
namespace opal::DRAWX_DX11 {
	class CLEAR_BUFFER {
	public:
		explicit CLEAR_BUFFER();
		virtual ~CLEAR_BUFFER();

	public:
		float	color[4];

	public:
		virtual void Init( DEVICE& dv );
		virtual void Free( void );

	public:
		virtual void Clear( DEVICE&, ID3D11RenderTargetView*&, ID3D11DepthStencilView*& );
		virtual void Clear( DEVICE&, ID3D11DepthStencilView*& );
		virtual void Clear( DEVICE&, ID3D11RenderTargetView*& );
		virtual void Clear( DEVICE& );

	public:
		virtual COLOR Color( void ) const;
		virtual void  Color( COLOR );
		virtual void  Color( UCHAR, UCHAR, UCHAR );

	public:
		virtual operator float*( void );
		virtual float& operator []( UINT );

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( void ) const;
#endif
	};
}

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::DRAWX_DX11 {
	extern DEVICE			device;
	extern VIEW_PORT		viewport;
	extern CLEAR_BUFFER		clearbuffer;
}

//----------------------------------------
// インデックスバッファ/頂点バッファ/定数バッファ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	extern auto CreateIndexBuffer(     ID3D11Buffer*&, const void*, size_t, bool = false )->D3D11_BUFFER_DESC;
	extern auto CreateVertexBuffer(    ID3D11Buffer*&, const void*, size_t, bool = false )->D3D11_BUFFER_DESC;
	extern auto CreateConstantBuffer(  ID3D11Buffer*&, size_t )->D3D11_BUFFER_DESC;

	extern void DestroyIndexBuffer(    ID3D11Buffer*& );
	extern void DestroyVertexBuffer(   ID3D11Buffer*& );
	extern void DestroyConstantBuffer( ID3D11Buffer*& );
}

//----------------------------------------
// 頂点レイアウト/頂点シェーダー/ピクセルシェーダー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	extern void CreateInputLayout(  ID3D11InputLayout*&,  const void*, size_t, const D3D11_INPUT_ELEMENT_DESC*, size_t );
	extern void CreateVertexShader( ID3D11VertexShader*&, const void*, size_t, ID3D11ClassLinkage* = nullptr );
	extern void CreatePixelShader(  ID3D11PixelShader*&,  const void*, size_t, ID3D11ClassLinkage* = nullptr );

	extern void DestroyInputLayout(  ID3D11InputLayout*&  );
	extern void DestroyVertexShader( ID3D11VertexShader*& );
	extern void DestroyPixelShader(  ID3D11PixelShader*&  );
}

//----------------------------------------
// レンダリングステート/深度バッファ/ブレンド
//----------------------------------------
namespace opal::DRAWX_DX11 {
	enum BLEND_TYPE {
		BLEND_TYPE0,
		BLEND_TYPE1,
		BLEND_TYPE2,
		BLEND_TYPE3,
		BLEND_TYPE4,
		BLEND_TYPE5,
		BLEND_TYPE6,
		BLEND_TYPE7,
		BLEND_TYPE_MAX,
	};

	extern auto CreateRasterizerState(   ID3D11RasterizerState*&, bool = false )->D3D11_RASTERIZER_DESC;
	extern auto CreateDepthStencilState( ID3D11DepthStencilState*& )->D3D11_DEPTH_STENCIL_DESC;
	extern auto CreateBlendState(        ID3D11BlendState*&, UINT )->D3D11_BLEND_DESC;
	extern auto CreateBlendState(        ID3D11BlendState*& )->D3D11_BLEND_DESC;
	extern auto CreateBlendState(        ID3D11BlendState*&, D3D11_BLEND, D3D11_BLEND, D3D11_BLEND_OP )->D3D11_BLEND_DESC;
	extern auto CreateBlendState(        ID3D11BlendState*&, const D3D11_BLEND*, const D3D11_BLEND*, const D3D11_BLEND_OP* )->D3D11_BLEND_DESC;
	extern auto CreateBlendState(        ID3D11BlendState*&, const D3D11_RENDER_TARGET_BLEND_DESC& )->D3D11_BLEND_DESC;

	extern void DestroyRasterizerState(   ID3D11RasterizerState*&   );
	extern void DestroyDepthStencilState( ID3D11DepthStencilState*& );
	extern void DestroyBlendState(        ID3D11BlendState*&        );

	extern bool CheckRasterizerState(   ID3D11RasterizerState*& );
	extern bool CheckDepthStencilState( ID3D11DepthStencilState*&, UINT );
	extern bool CheckBlendState(        ID3D11BlendState*&, const float*, UINT );

	extern void ResetState( void );
	extern void ResetRasterizerState(   void );
	extern void ResetDepthStencilState( void );
	extern void ResetBlendState(        void );
}

namespace opal::DRAWX_DX11 {
	class KEEP_DepthStencilState {
	private:
		ID3D11DeviceContext*		context;
		ID3D11DepthStencilState*	state;
		UINT 						ref;

	public:
		explicit KEEP_DepthStencilState();
		virtual ~KEEP_DepthStencilState();
	};
}

//----------------------------------------
// テクスチャー/バッファー/サーフェス
//----------------------------------------
namespace opal::DRAWX_DX11 {
	struct CREATE_TEXTURE_ {
		explicit CREATE_TEXTURE_( DXGI_FORMAT, UINT, UINT );
		explicit CREATE_TEXTURE_( DXGI_FORMAT );
		explicit CREATE_TEXTURE_( UINT, UINT );
		explicit CREATE_TEXTURE_();

		DXGI_FORMAT	format;
		UINT		width;
		UINT		height;
	};
}

namespace opal::DRAWX_DX11 {
	struct CREATE_TEXTURE_RECT{
		explicit CREATE_TEXTURE_RECT( float, float, float, float );
		explicit CREATE_TEXTURE_RECT( float, float );
		explicit CREATE_TEXTURE_RECT( const CREATE_TEXTURE_RECT& );
		explicit CREATE_TEXTURE_RECT( const CREATE_TEXTURE_RECT* );
		explicit CREATE_TEXTURE_RECT();

		float	x;
		float	y;
		float	w;
		float	h;
	};
}

namespace opal::DRAWX_DX11 {
	struct CREATE_TEXTURE : CREATE_TEXTURE_ {
		using CREATE_TEXTURE_::CREATE_TEXTURE_;
		explicit CREATE_TEXTURE( const char*, const void*, size_t, DXGI_FORMAT, const void* = nullptr );
		explicit CREATE_TEXTURE( const char*, const void*, size_t, const void* = nullptr );
		explicit CREATE_TEXTURE( DXGI_FORMAT, const void* = nullptr );
		explicit CREATE_TEXTURE( const void* = nullptr );

		const char*			name;
		const void*			adrs;
		size_t				size;
		CREATE_TEXTURE_RECT	rect;
	};

	extern auto CreateTexture2D( ID3D11Texture2D*&, const CREATE_TEXTURE&   )->D3D11_TEXTURE2D_DESC;
	extern auto CreateTexture2D( ID3D11Texture2D*&, DXGI_FORMAT, UINT, UINT )->D3D11_TEXTURE2D_DESC;
	extern auto CreateTexture2D( ID3D11Texture2D*&,              UINT, UINT )->D3D11_TEXTURE2D_DESC;

	extern void DestroyTexture2D( ID3D11Texture2D*& );
}

namespace opal::DRAWX_DX11 {
	struct CREATE_BUFFER : CREATE_TEXTURE_ {
		explicit CREATE_BUFFER( DXGI_FORMAT, UINT, UINT );
		explicit CREATE_BUFFER( DXGI_FORMAT );
		explicit CREATE_BUFFER( UINT, UINT );
		explicit CREATE_BUFFER();
	};

	struct BUFFER_DESC : D3D11_TEXTURE2D_DESC {
		explicit BUFFER_DESC( UINT, UINT, DXGI_FORMAT );
		explicit BUFFER_DESC( CREATE_BUFFER );
	};

	extern auto CreateBuffer2D(  ID3D11Texture2D*&, const CREATE_BUFFER& )->D3D11_TEXTURE2D_DESC;
	extern auto CreateBuffer2D(  ID3D11Texture2D*&, ID3D11Texture2D*     )->D3D11_TEXTURE2D_DESC;
}

namespace opal::DRAWX_DX11 {
	struct CREATE_SURFACE : CREATE_TEXTURE_ {
		enum TYPE {
			RENDER,
			CANVAS,
			IDXGDI,
			SHADER,
		};
		explicit CREATE_SURFACE( DXGI_FORMAT, UINT, UINT, TYPE = RENDER );
		explicit CREATE_SURFACE( DXGI_FORMAT, TYPE = RENDER );
		explicit CREATE_SURFACE( UINT, UINT,  TYPE = RENDER );
		explicit CREATE_SURFACE( TYPE = RENDER );

		TYPE	type;
	};

	struct RENDER_DESC : D3D11_TEXTURE2D_DESC {
		explicit RENDER_DESC( UINT, UINT, DXGI_FORMAT );
		explicit RENDER_DESC( CREATE_SURFACE );
	};

	struct CANVAS_DESC : D3D11_TEXTURE2D_DESC {
		explicit CANVAS_DESC( UINT, UINT );
		explicit CANVAS_DESC( CREATE_SURFACE );
	};

	struct IDXGDI_DESC : D3D11_TEXTURE2D_DESC {
		explicit IDXGDI_DESC( UINT, UINT );
		explicit IDXGDI_DESC( CREATE_SURFACE );
	};

	struct SHADER_DESC : D3D11_TEXTURE2D_DESC {
		explicit SHADER_DESC( UINT, UINT );
		explicit SHADER_DESC( CREATE_SURFACE );
	};

	extern auto CreateSurface2D( ID3D11Texture2D*&, const CREATE_SURFACE& )->D3D11_TEXTURE2D_DESC;
	extern auto CreateRender2D(  ID3D11Texture2D*&, const CREATE_SURFACE& )->D3D11_TEXTURE2D_DESC;
	extern auto CreateCanvas2D(  ID3D11Texture2D*&, const CREATE_SURFACE& )->D3D11_TEXTURE2D_DESC;
	extern auto CreateIDXGDI2D(  ID3D11Texture2D*&, const CREATE_SURFACE& )->D3D11_TEXTURE2D_DESC;
	extern auto CreateShader2D(  ID3D11Texture2D*&, const CREATE_SURFACE& )->D3D11_TEXTURE2D_DESC;
}

//----------------------------------------
// ポリゴンデータ
//----------------------------------------
namespace opal::DRAWX_DX11 {
	struct CREATE_POLYGON {
		explicit CREATE_POLYGON( D3D11_PRIMITIVE_TOPOLOGY, const WORD*, size_t, const void*, size_t );
		explicit CREATE_POLYGON( const WORD*, size_t, const void*, size_t );
		explicit CREATE_POLYGON( const CREATE_POLYGON& );
		explicit CREATE_POLYGON();

		D3D11_PRIMITIVE_TOPOLOGY	topology;
		const WORD*					indexlist;
		size_t						indexsize;;
		const void*					vertexlist;
		size_t						vertexsize;
	};
}

//----------------------------------------
// ビュー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	extern auto CreateShaderResourceView(  ID3D11ShaderResourceView*&, ID3D11Texture2D* )->D3D11_SHADER_RESOURCE_VIEW_DESC;
	extern auto CreateRenderTargetView(    ID3D11RenderTargetView*&,   ID3D11Texture2D* )->D3D11_RENDER_TARGET_VIEW_DESC;
	extern auto CreateDepthStencilView(    ID3D11DepthStencilView*&,   ID3D11Texture2D* )->D3D11_DEPTH_STENCIL_VIEW_DESC;

	extern void DestroyShaderResourceView( ID3D11ShaderResourceView*& );
	extern void DestroyRenderTargetView(   ID3D11RenderTargetView*&   );
	extern void DestroyDepthStencilView(   ID3D11DepthStencilView*&   );
}

//----------------------------------------
// サンプラー
//----------------------------------------
namespace opal::DRAWX_DX11 {
	extern auto CreateSamplerState(  ID3D11SamplerState*& )->D3D11_SAMPLER_DESC;

	extern void DestroySamplerState( ID3D11SamplerState*& );
}

//----------------------------------------
// リソースビューのスロットのリセット
//----------------------------------------
namespace opal::DRAWX_DX11 {
	void ResetResourceView( UINT = 0, UINT = 1 );
}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal::DRAWX_DX11 {
	extern void ShaderReflection( const void*, size_t, const char* = nullptr );
}
#endif

// End Of File
