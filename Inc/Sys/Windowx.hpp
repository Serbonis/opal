#pragma once
//========================================
// OPAL Game Library
//========================================
#include "window.hpp"

//========================================
// ウインドウクラス
//========================================
namespace opal {
	class WINDOW : public SINGS<WINDOW> {
		//----------------------------------------
		// friends
		//----------------------------------------
		friend class SYSTEM;
		friend class DEBUGX;
		friend class SINGS<WINDOW>;

		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	private:
		explicit WINDOW();

	public:
		virtual ~WINDOW() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		HWND		handle;		// ウインドウハンドル
		UINT		message;	// ウィンドウメッセージ
		DWORD		style;		// ウインドウスタイル

	private:
		bool		active;		// ウインドウがアクティブ
		bool		focus;		// ウインドウがフォーカスされている
		bool		moving;		// ウインドウが移動している
		int			sizing;		// ウインドウがサイズ変更

	private:
		bool		full;		// フルスクリーン
		HMENU		menu;		// メニューハンドル

	private:
		struct {
			SRECTU	std;
			SRECTU	now;
		}			window;		// ウインドウ情報

	private:
		struct {
			int		w;
			int		h;
		}			screen;		// スクリーン情報

	private:
		struct {
			int		w;
			int		h;
		}			display;	// 描画領域情報

	private:
		struct {
			bool	show;
		}			cursor;		// カーソル

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		HWND Handle(  void ) const;
		UINT Message( void ) const;

	public:
		void Open(  void );
		void Close( void );

	public:
		enum {
			PROCESS_MESSAGE,
			PROCESS_GAME,
			PROCESS_QUIT,
			PROCESS_ERROR,
		};
		int Process( void );

	public:
		void Active( int vFlag );
		bool Active( void ) const;

	public:
		void Focus( int vFlag );
		bool Focus( void ) const;

	public:
		void Move( int X, int Y, int W, int H );
		bool Moving( void ) const;
		void Reset(  void );

	public:
		void Sizing( int S );
		int  Sizing( void  ) const;

	private:
		void WindowX( int X );
		void WindowY( int Y );
		void WindowW( int W );
		void WindowH( int H );

	public:
		int WindowX(  void ) const;
		int WindowY(  void ) const;
		int WindowW(  void ) const;
		int WindowH(  void ) const;

	public:
		int ScreenW(  void ) const;
		int ScreenH(  void ) const;
		int DisplayW( void ) const;
		int DisplayH( void ) const;

	public:
		void Screen( int vFlag );
		bool Screen( void ) const;

	public:
		void Cursor( int vFlag );
		bool Cursor( void ) const;

		//----------------------------------------
		// static member functions
		//----------------------------------------
	private:
		static LRESULT CALLBACK Event( HWND hWnd, UINT msg, WPARAM wp, LPARAM lp );
	};
}

// End Of File
