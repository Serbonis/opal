#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"
#include "new.hpp"

//========================================
// HEAP管理
//========================================
//----------------------------------------
// タスク
//----------------------------------------
namespace opal {
	struct HEAP_NODE;
	class HEAP_HASH final : private NOCOPY {
	public:
		explicit HEAP_HASH();
		~HEAP_HASH();

	private:
		size_t		size;		// ノードの数
		UINT		keys;		// キーの数
		HEAP_NODE**	node;		// 各ノードへのポインターのリスト

	public:
		void Init( void );
		void Free( void );

	public:
		size_t Size( void ) const;
		UINT   Keys( void ) const;

	public:
		HEAP_NODE* Node( const void* ) const;

	public:
		HEAP_NODE* New( const void*, const char*, size_t, size_t );
		void Del( const void* );

#if OPAL_DEBUG
	public:
		void Debug( bool = false ) const;
#endif
	};
}

//----------------------------------------
// データ
//----------------------------------------
namespace opal {
	struct HEAP_DATA final : private NOCOPY {
		explicit HEAP_DATA();
		explicit HEAP_DATA( const void*, const char*, size_t, size_t );
		~HEAP_DATA();

		const void*	adrs;
		char*		type;
		size_t		size;
		size_t		unit;
	};
}

//----------------------------------------
// ノード
//----------------------------------------
namespace opal {
	using HEAP_KEY = UINT64;

	struct HEAP_NODE final : private NOCOPY {
		explicit HEAP_NODE();
		~HEAP_NODE();

		HEAP_KEY	key;		// ハッシュキー
		HEAP_DATA	val;		// 保持する値
		HEAP_NODE*	next;		// 同じハッシュキーの次のノード

		void Init( HEAP_KEY, const void* p, const char* t, size_t s, size_t u, HEAP_NODE* );
		void Free( void );
	};
}

// End Of File
