#pragma once
//========================================
// padr.hpp
//========================================
#include "pad.hpp"

//----------------------------------------
// PAD REPEAT
//----------------------------------------
namespace opal {
	class PADR {
	public:
		enum REPEAT {
			REPEAT_NULL,	// 入力されていない
			REPEAT_TRIG,	// 最初の入力
			REPEAT_PUSH,	// 以降の連続入力
		};

	public:
		struct TIME final {
			TIME( UINT, UINT );
			TIME( UINT );
			TIME();

			UINT	d;		// 最初の入力から次の入力までの遅延時間
			UINT	i;		// 以降の連続入力の取得間隔
		};

		using KEY	= UINT;
		using PAD	= UINT;
		using FUNC	= std::function<UINT(UINT)>;

	public:
		PADR( TIME, KEY, PAD = PAD_NULL );
		PADR( TIME, FUNC );
		PADR();
		virtual ~PADR() = default;

	protected:
		mutable	UINT	step;
		mutable TIME	time[2];
		KEY				key;
		PAD				pad;
		FUNC			func[3];

	public:
		virtual void Reset( void );

	public:
		virtual void SetTime( TIME );
		virtual void SetTime( UINT, UINT );
		virtual void SetTime( UINT );
		virtual TIME GetTime( void ) const;

	public:
		virtual void SetKey(  KEY  );
		virtual void SetPad(  PAD  );
		virtual void SetFunc( FUNC );
		virtual auto GetKey(  void ) const->KEY;
		virtual auto GetPad(  void ) const->PAD;
		virtual auto GetFunc( void ) const->FUNC;

	public:
		virtual REPEAT operator ()( UINT = JOY1 ) const;
	};
}

// End Of File
