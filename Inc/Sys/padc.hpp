#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力カウント
//========================================
namespace opal {
	class PADC : private NOCOPY {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit PADC();
		virtual ~PADC() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		using KEYS	= UINT[KEY_MAX];
		using MOSS	= UINT[MOUSE_MAX];
		using JOYS	= UINT[PAD_MAX];

		KEYS	keyboard;
		MOSS	mouse;
		UINT	joymax;
		JOYS*	joypad;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		void Init( UINT );
		void Free( void );

	public:
		void SetCountKeyboard(       UINT, UINT = 0 );
		void SetCountMouse(          UINT, UINT = 0 );
		void SetCountJoypad(   UINT, UINT, UINT = 0 );

	public:
		void AddCountKeyboard(       UINT, UINT = 1 );
		void AddCountMouse(          UINT, UINT = 1 );
		void AddCountJoypad(   UINT, UINT, UINT = 1 );

	public:
		UINT GetCountKeyboard(       UINT ) const;
		UINT GetCountMouse(          UINT ) const;
		UINT GetCountJoypad(   UINT, UINT ) const;

		//----------------------------------------
		// 判定関数
		//----------------------------------------
	public:
		UINT KeyCount(       UINT ) const;
		UINT MouseCount(     UINT ) const;
		UINT JoyCount( UINT, UINT ) const;
		UINT Joy1Count(      UINT ) const;
		UINT Joy2Count(      UINT ) const;

		UINT JKCount(  UINT, UINT, UINT ) const;
		UINT J1KCount(       UINT, UINT ) const;
		UINT J2KCount(       UINT, UINT ) const;

		UINT JKCount(  UINT, const PADA* ) const;
		UINT J1KCount(       const PADA* ) const;
		UINT J2KCount(       const PADA* ) const;
	};
}

// End Of File
