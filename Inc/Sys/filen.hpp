#pragma once
//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイル名データ
//========================================
namespace opal {
	class FILEN {
	public:
		FILEN( const char* = nullptr );
		FILEN( const char*, const char* );

	public:
		NAME operator ()( void ) const;

	public:
		NAME Path( const char* = nullptr ) const;
		NAME File( const char* = nullptr ) const;

	public:
		void SetPath( const char* );
		void SetFile( const char* );
		const char* GetPath( void ) const;
		const char* GetFile( void ) const;

	private:
		NAME	path;
		NAME	file;

	public:
		static bool Kind( const char*, const char* );
	};

	class FILEN_S : public FILEN, public CSIZEU {
	public:
		FILEN_S( const char*, UINT, UINT );
		FILEN_S( const char*, const CSIZEU& );
		FILEN_S( const char*, const SSIZEU& );
		FILEN_S( const char* = nullptr );
		FILEN_S( UINT, UINT );
		FILEN_S( const CSIZEU& );
		FILEN_S( const SSIZEU& );
	};

	class FILEN_R : public FILEN, public CRECTU {
	public:
		FILEN_R( const char*, UINT, UINT, UINT, UINT );
		FILEN_R( const char*, const CRECTU& );
		FILEN_R( const char*, const SRECTU& );
		FILEN_R( const char* = nullptr );
		FILEN_R( UINT, UINT, UINT, UINT );
		FILEN_R( const CRECTU& );
		FILEN_R( const SRECTU& );

		FILEN_R( const char*, UINT, UINT );
		FILEN_R( const char*, const CSIZEU& );
		FILEN_R( const char*, const SSIZEU& );
		FILEN_R( UINT, UINT );
		FILEN_R( const CSIZEU& );
		FILEN_R( const SSIZEU& );

	public:
		float U( void ) const;
		float V( void ) const;

	public:
		SSIZE Unit( void ) const;
	};
}

// End Of File
