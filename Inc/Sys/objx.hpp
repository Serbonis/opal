#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"
#include "obj.hpp"

//========================================
// オブジェクトシステム
//========================================
namespace opal {
	class OBJ : public SIGL {
		friend class OBJT;

	public:
		explicit OBJ();
		virtual ~OBJ();

	protected:
		enum {
			OBJ_STATE_NULL		= 0L,
			OBJ_STATE_PAUSE		= 1 << 0,
			OBJ_STATE_PAUSE_T	= 1 << 1,
			OBJ_STATE_FUNC		= 1 << 2,
			OBJ_STATE_MAIN 		= 1 << 3,
		};
		UINT			state;

	protected:
		struct FLAG final {
			FLAG();

			template<typename TYPE>struct ELEMENT final {
				ELEMENT();

				TYPE	func;		// 関数実行
			};
			ELEMENT<int>	s;		// 制御スタイル
			ELEMENT<bool>	v[2];	// ローカル/ワールド値
			UINT			t;		// 遅延開始
		}	flag;

	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual void SetFunc( int = FLAG_FLIP, UINT = 0 );
		virtual bool GetFunc( void ) const;
		virtual bool GetFunc( UINT ) const;

	public:
		virtual void SetWait( UINT = 0 );
		virtual UINT GetWait( void ) const;

	public:
		virtual void SetSuperFunc( int  );
		virtual void SetSuperFunc( void );
		virtual int  GetSuperFunc( void ) const;

	public:
		virtual void Coordinate( const OBJ* = nullptr );
	};
}

//========================================
// オブジェクトタスク
//========================================
namespace opal {
	class OLINX;

	class OBJT :
		public TASK,
		public OBJ
	{
		friend class OBJX;

	public:
		explicit OBJT();
		virtual ~OBJT();

	protected:
		int		step;
		int		time;
		bool	debug;

	public:
		class UPDATER {
			friend class OBJT;

			using FUNC0 = std::function<void(void )>;
			using FUNC1 = std::function<void(OBJT*)>;
			using FUNC2 = std::function<bool(void )>;
			using FUNC3 = std::function<bool(OBJT*)>;

		private:
			FUNC0	func0{nullptr};
			FUNC1	func1{nullptr};
			FUNC2	func2{nullptr};
			FUNC3	func3{nullptr};

		public:
			explicit operator bool( void ) const { return ( func0 && func1 && func2 && func3 ) ? true : false; }
		};
		using UPDATERS = std::pair<UPDATER,UPDATER>;

	protected:
		struct {
			std::map<std::string,UPDATER>	begin;
			std::map<std::string,UPDATER>	end;
		}			updater;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual void Register( OLINX* );
		virtual void Release(  OLINX* );
		virtual void Register( std::shared_ptr<OLINX> );
		virtual void Release(  std::shared_ptr<OLINX> );
		virtual void Register( void );
		virtual void Release(  void );

	public:
		virtual void SetSelf( SELF );
		virtual auto GetSelf( void ) const->SELF;

		template<typename TYPE>void SetSelf( const TYPE* t ){ Self( t ); }

	public:
		virtual void SetDebug( int = FLAG_FLIP );
		virtual bool GetDebug( void ) const;

	protected:
		virtual void SetID_( const char* );

	public:
		virtual void SetID( const char*, ... );
		virtual void SetID( const std::string& );
		virtual void SetID( std::nullptr_t = nullptr );
		virtual auto GetID( void ) const->const char*;

		virtual void SetInfo( UINT );
		virtual UINT GetInfo( void ) const;

		virtual void SetPrio( UINT, bool = true );
		virtual UINT GetPrio( void ) const;

	public:
		virtual void SetPrune( bool );
		virtual bool GetPrune( void ) const;

	public:
		virtual void SetUpdater(      void );
		virtual void SetUpdaterBegin( void );
		virtual void SetUpdaterEnd(   void );
		virtual UINT GetUpdater(      void ) const;
		virtual UINT GetUpdaterBegin( void ) const;
		virtual UINT GetUpdaterEnd(   void ) const;

		virtual void SetUpdater(      const std::string&, std::nullptr_t = nullptr, std::nullptr_t = nullptr );
		virtual void SetUpdaterBegin( const std::string&, std::nullptr_t = nullptr );
		virtual void SetUpdaterEnd(   const std::string&, std::nullptr_t = nullptr );
		virtual auto GetUpdater(      const std::string& ) const->UPDATERS;
		virtual auto GetUpdaterBegin( const std::string& ) const->UPDATER;
		virtual auto GetUpdaterEnd(   const std::string& ) const->UPDATER;

	public:
		template<typename TYPE>auto SetUpdaterBegin( const std::string& s, TYPE f )->decltype( f(nullptr), void() ){ SetUpdaterBegin( s ); if constexpr ( !std::is_void_v<decltype(f(nullptr))> ) { updater.begin[s].func3 = f; } else { updater.begin[s].func1 = f; } }
		template<typename TYPE>auto SetUpdaterBegin( const std::string& s, TYPE f )->decltype( f(),        void() ){ SetUpdaterBegin( s ); if constexpr ( !std::is_void_v<decltype(f()       )> ) { updater.begin[s].func2 = f; } else { updater.begin[s].func0 = f; } }
		template<typename TYPE>auto SetUpdaterEnd(   const std::string& s, TYPE f )->decltype( f(nullptr), void() ){ SetUpdaterEnd(   s ); if constexpr ( !std::is_void_v<decltype(f(nullptr))> ) { updater.end[s].func3   = f; } else { updater.end[s].func1   = f; } }
		template<typename TYPE>auto SetUpdaterEnd(   const std::string& s, TYPE f )->decltype( f(),        void() ){ SetUpdaterEnd(   s ); if constexpr ( !std::is_void_v<decltype(f()       )> ) { updater.end[s].func2   = f; } else { updater.end[s].func0   = f; } }

		template<typename TYPE>auto SetUpdater( const std::string& s, TYPE fb )->decltype( void() ){ SetUpdaterBegin( s, fb ); }

		template<typename TYPE>auto SetUpdater( const std::string& s, TYPE f, std::nullptr_t )->decltype( f(nullptr), void() ){ SetUpdaterBegin( s, f ); SetUpdaterEnd( s, nullptr ); }
		template<typename TYPE>auto SetUpdater( const std::string& s, TYPE f, std::nullptr_t )->decltype( f(),        void() ){ SetUpdaterBegin( s, f ); SetUpdaterEnd( s, nullptr ); }
		template<typename TYPE>auto SetUpdater( const std::string& s, std::nullptr_t, TYPE f )->decltype( f(nullptr), void() ){ SetUpdaterBegin( s, nullptr ); SetUpdaterEnd( s, f ); }
		template<typename TYPE>auto SetUpdater( const std::string& s, std::nullptr_t, TYPE f )->decltype( f(),        void() ){ SetUpdaterBegin( s, nullptr ); SetUpdaterEnd( s, f ); }

		template<typename T_FB,typename T_FE>auto SetUpdater( const std::string& s, T_FB fb, T_FE fe )->decltype( fb(nullptr), fe(nullptr), void() ){ SetUpdaterBegin( s, fb ); SetUpdaterEnd( s, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( const std::string& s, T_FB fb, T_FE fe )->decltype( fb(nullptr), fe(),        void() ){ SetUpdaterBegin( s, fb ); SetUpdaterEnd( s, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( const std::string& s, T_FB fb, T_FE fe )->decltype( fb(),        fe(nullptr), void() ){ SetUpdaterBegin( s, fb ); SetUpdaterEnd( s, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( const std::string& s, T_FB fb, T_FE fe )->decltype( fb(),        fe(),        void() ){ SetUpdaterBegin( s, fb ); SetUpdaterEnd( s, fe ); }

	public:
		template<typename TYPE>auto SetUpdaterBegin( TYPE f )->decltype( f(nullptr), void() ){ SetUpdaterBegin( "", f );	}
		template<typename TYPE>auto SetUpdaterBegin( TYPE f )->decltype( f(),        void() ){ SetUpdaterBegin( "", f );	}
		template<typename TYPE>auto SetUpdaterEnd(   TYPE f )->decltype( f(nullptr), void() ){ SetUpdaterEnd(   "", f );	}
		template<typename TYPE>auto SetUpdaterEnd(   TYPE f )->decltype( f(),        void() ){ SetUpdaterEnd(   "", f );	}

		template<typename TYPE>auto SetUpdater( TYPE fb )->decltype( fb(), void() ){ SetUpdater( "", fb ); }

		template<typename TYPE>auto SetUpdater( TYPE f, std::nullptr_t )->decltype( f(nullptr), void() ){ SetUpdater( "", f, nullptr ); }
		template<typename TYPE>auto SetUpdater( TYPE f, std::nullptr_t )->decltype( f(),        void() ){ SetUpdater( "", f, nullptr ); }
		template<typename TYPE>auto SetUpdater( std::nullptr_t, TYPE f )->decltype( f(nullptr), void() ){ SetUpdater( "", nullptr, f ); }
		template<typename TYPE>auto SetUpdater( std::nullptr_t, TYPE f )->decltype( f(),        void() ){ SetUpdater( "", nullptr, f ); }

		template<typename T_FB,typename T_FE>auto SetUpdater( T_FB fb, T_FE fe )->decltype( fb(nullptr), fe(nullptr), void() ){ SetUpdater( "", fb, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( T_FB fb, T_FE fe )->decltype( fb(nullptr), fe(),        void() ){ SetUpdater( "", fb, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( T_FB fb, T_FE fe )->decltype( fb(),        fe(nullptr), void() ){ SetUpdater( "", fb, fe ); }
		template<typename T_FB,typename T_FE>auto SetUpdater( T_FB fb, T_FE fe )->decltype( fb(),        fe(),        void() ){ SetUpdater( "", fb, fe ); }

	protected:
		virtual void Updater( void );

#if OPAL_DEBUG
	public:
		virtual void UpdaterDebug( void );
#endif

	protected:
		virtual void Coordinate( void ) final;

	public:
		virtual void ObjInfo( void ) const;
		virtual void ObjInfo( int  ) const;

	protected:
		virtual void ObjFunc(  void );
		virtual void ObjBegin( void );
		virtual void ObjMain(  void );
		virtual void ObjEnd(   void );
		virtual void ObjKill(  void );

	protected:
		virtual void TaskInfo(  void ) const override;
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	private:
		virtual void Register( LINX* ) override;
		virtual void Release(  LINX* ) override;

	public:
		static OLINX* TLinx( const OBJT* );
		static LINX*  TLinx( void );
	};
}

namespace opal {
	class OBJXT : public OBJT {
	public:
		explicit OBJXT() = default;
		virtual ~OBJXT() = default;
	};
}

//========================================
// オブジェクトタスク専用リンク
//========================================
#include "tlinx.hpp"

namespace opal {
	class OLINX : public TLINX<OBJT>{};
	class OBJL  : public TASKL<OBJT,OLINX>{};
	template<typename TYPE> class OBJTL : public TASKL<TYPE,OLINX>{};
}

// End Of File
