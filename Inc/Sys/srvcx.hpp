#pragma once
//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスタスク
//========================================
namespace opal {
	class SRVC : private NOCOPY {
		friend class SRVCX;
		friend class SRVCS;

	public:
		explicit SRVC( const char* = nullptr );
		virtual ~SRVC();

		using CFUNC = std::function<void(void)>;

	protected:
		NAME	name;
		UINT	prio;
		CFUNC	clear;

		//----------------------------------------
		// member functions
		//----------------------------------------
	private:
		virtual void Begin(  void ) = 0;
		virtual void End(    void ) = 0;
		virtual void Before( void ) = 0;
		virtual void After(  void ) = 0;

	public:
		virtual void SetServiceName( const char* );
		virtual const char* GetServiceName( void ) const;

	public:
		virtual void SetServicePrio( UINT );
		virtual UINT GetServicePrio( void ) const;

	public:
		virtual void  SetServiceClear( CFUNC );
		virtual CFUNC GetServiceClear( void ) const;

	protected:
		virtual void Clear( void );

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( const char* = nullptr ) const;
#endif
	};
}

//========================================
// サービスタスク:サーバー
//========================================
namespace opal {
	template<typename SERVER, typename CLIENT>class SRVC_S : public SRVC {
	protected:
		explicit SRVC_S( const char* p = nullptr ) :
			SRVC( p ),
			client{}
		{}
	public:
		virtual ~SRVC_S(){ Release(); }

	public:
		using CMAP = std::map<CLIENT*,std::string>;
		using XMAP = std::map<const std::string,CMAP>;

	protected:
		XMAP	client;

	public:
		virtual void Begin(  void ) override {}
		virtual void End(    void ) override {}
		virtual void Before( void ) override {}
		virtual void After(  void ) override {}

	public:
		virtual void Register( CLIENT* c ){ Register( c, "" );	}
		virtual void Register( CLIENT* c, const char* p ){

			if ( c && p ) {
				client[p][c] = p;
			}
		};

	public:
		virtual void Release( void ){ client.clear(); }
		virtual void Release( const char* p ){

			if ( p ) {
				if ( client.count( p ) ) {
					client[p].clear();
				}
			} else {
				for ( auto cm : client ) {
					cm.second.clear();
				}
			}
		}
		virtual void Release( CLIENT* c ){ Release(  c, "" );	}
		virtual void Release( CLIENT* c, const char* p ){

			if ( c && p && client.count( p ) ) {
				client[p].erase( c );
			}
		};

	public:
		virtual UINT Size( void ) const { return client.size();	}
		virtual UINT Size( const char* p ) const { return ( p && client.count( p ) ) ? client.at( p ).size() : 0 ;	}

		virtual const XMAP& Client( void ) const { return client; }
		virtual const CMAP* Client( const char* p ) const { return ( p && client.count( p ) ) ? &client.at( p ) : nullptr; }
	};
}

//========================================
// サービスタスク:クライアント
//========================================
namespace opal {
	template<typename SERVER, typename CLIENT>class SRVC_C {
	protected:
		explicit SRVC_C( const char* p = nullptr ) :
			name( p ),
			server{}
		{}

	public:
		virtual ~SRVC_C() { Server( nullptr, nullptr ); }

	public:
		using SMAP = std::unordered_map<std::string,SERVER*>;
		using XMAP = std::unordered_map<SERVER*,SMAP>;

	protected:
		NAME	name;
		XMAP	server;

	public:
		virtual void SetServiceName( const char* p            ){ name = p;			}
		virtual void SetServiceName( const std::string& s     ){ name = s.c_str();	}
		virtual void SetServiceName( std::nullptr_t = nullptr ){ name = nullptr;	}
		virtual auto GetServiceName( void ) const->const char*{ return name;	}

	public:
		virtual const XMAP& Server( void ) const { return server; }
		virtual SERVER* Server( const char* p ){

			if ( p ) {
				for ( auto sm : server ) {
					if ( sm.second.count( p ) ) {
						return sm.first;
					}
				}
			}
			return nullptr;
		}

		virtual void Server( SERVER* s ){ Server( s, "" ); }
		virtual void Server( SERVER* s, const char* p ){

			static const auto	clear = []( SERVER* s, CLIENT* c, const char* p = nullptr ){

				std::vector<std::pair<CLIENT*,const std::string>>	cmap;

				const auto xm = s->Client();

				for ( auto cm : xm ) {
					for ( auto cp : cm.second ) {
						if ( cp.first == c && ( !p || cp.second == p ) ) {
							cmap.emplace_back( cp );
						}
					}
				}

				for ( auto cm : cmap ) {
					s->Release( cm.first, cm.second.c_str() );
				}
			};

			const auto c = dynamic_cast<CLIENT*>( this );

			if ( s ) {
				if ( p ) {
					server[s][p] = s;
					s->Register( c, p );
				} else if ( server.count( s ) ) {
					clear( s, c );
				}
			} else if ( p ) {
				for ( auto xm : server ) {
					clear( xm.first, c, p );
				}
			} else {
				for ( auto xm : server ) {
					clear( xm.first, c );
				}
			}
		}
	};
}

// End Of File
