#pragma once
//========================================
// OPAL Game Library
//========================================
#include "memory.hpp"

//========================================
// メモリータスク
//========================================
namespace opal {
	class MEMT : public TASK {
		friend class MEMORY;

	public:
		explicit MEMT();
		virtual ~MEMT() = default;

	private:
		MEMORY::ADRS	st;	// 開始アドレス
		MEMORY::ADRS	ed;	// 終了アドレス

	private:
		virtual void TaskMain( void ) override;
	};
}

// End Of File
