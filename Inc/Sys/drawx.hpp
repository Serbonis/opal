#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

#include "draw.hpp"
#include "drawa.hpp"
#include "drawb.hpp"

#include "tlinx.hpp"

#include "Obj/pobj.hpp"
#include "Obj/zobj.hpp"

//========================================
// グラフィックワーク
//========================================
namespace opal {
	class DRAWS {
		friend class DRAWT;

	public:
		explicit DRAWS();
		virtual ~DRAWS();

	public:
		enum {
			NORMAL,			// 通常表示
			PARTICLE,		// パーティクル(常にカメラに相対)
			BILLBOARD,		// ビルボード(Y軸固定パーティクル)

			PART	= PARTICLE,
			BILL	= BILLBOARD,
		};

	protected:
		enum {
			DRAW_STATE_NULL		= 0L,
			DRAW_STATE_PAUSE	= 1 <<  0,
			DRAW_STATE_PAUSE_T	= 1 <<  1,
			DRAW_STATE_FUNC		= 1 <<  2,
			DRAW_STATE_ANIME	= 1 <<  3,
			DRAW_STATE_ANIME_T	= 1 <<  4,
			DRAW_STATE_MAIN 	= 1 <<  5,

			DRAW_STATE_DEFAULT	= DRAW_STATE_NULL,
		};
		UINT	state;

	protected:
		struct FLAG final {
			FLAG();

			FLAG_SUPER<bool>	pers;	// パース
			FLAG_SUPER<bool>	disp;	// 表示
			FLAG_SUPER<bool>	anime;	// アニメ
			FLAG_SUPER<bool>	alpha;	// αフラグ
			FLAG_SUPER<bool>	render;	// 自前レンダリング
			FLAG_SUPER<bool>	revu;	// U反転
			FLAG_SUPER<bool>	revv;	// V反転
		}	flag;

	protected:
		struct PROP final {
			PROP();

			UINT		part;	// 表示方式
		}	prop;

	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual void SetPart( UINT = NORMAL );
		virtual UINT GetPart( void ) const;

	public:
		virtual void SetPers(    int = FLAG_FLIP );
		virtual void SetDisp(    int = FLAG_FLIP );
		virtual void SetAnime(   int = FLAG_FLIP );
		virtual void SetAlpha(   int = FLAG_FLIP );
		virtual void SetRender(  int = FLAG_FLIP );
		virtual void SetReverse( int = FLAG_FLIP );
		virtual void SetRevU(    int = FLAG_FLIP );
		virtual void SetRevV(    int = FLAG_FLIP );

		virtual bool GetPers(    void ) const;
		virtual bool GetDisp(    void ) const;
		virtual bool GetAnime(   void ) const;
		virtual bool GetAlpha(   void ) const;
		virtual bool GetRender(  void ) const;
		virtual bool GetReverse( void ) const;
		virtual bool GetRevU(    void ) const;
		virtual bool GetRevV(    void ) const;

		virtual bool GetPers(    UINT ) const;
		virtual bool GetDisp(    UINT ) const;
		virtual bool GetAnime(   UINT ) const;
		virtual bool GetAlpha(   UINT ) const;
		virtual bool GetRender(  UINT ) const;
		virtual bool GetRevU(    UINT ) const;
		virtual bool GetRevV(    UINT ) const;

	public:
		virtual void SetSuperPers(   int );
		virtual void SetSuperDisp(   int );
		virtual void SetSuperAnime(  int );
		virtual void SetSuperAlpha(  int );
		virtual void SetSuperRender( int );
		virtual void SetSuperRevU(   int );
		virtual void SetSuperRevV(   int );

		virtual void SetSuperPers(   void );
		virtual void SetSuperDisp(   void );
		virtual void SetSuperAnime(  void );
		virtual void SetSuperAlpha(  void );
		virtual void SetSuperRender( void );
		virtual void SetSuperRevU(   void );
		virtual void SetSuperRevV(   void );

		virtual int  GetSuperPers(   void ) const;
		virtual int  GetSuperDisp(   void ) const;
		virtual int  GetSuperAnime(  void ) const;
		virtual int  GetSuperAlpha(  void ) const;
		virtual int  GetSuperRender( void ) const;
		virtual int  GetSuperRevU(   void ) const;
		virtual int  GetSuperRevV(   void ) const;

	public:
		virtual bool Culling( void ) const;
		virtual void Reverse( VECTOR2[4], float, float, float, float ) const;

	public:
		virtual void Coordinate( const DRAWS* = nullptr );
		virtual void Viewport(   const DRAWS* = nullptr );
	};
}

//========================================
// 描画タスク
//========================================
namespace opal {
	class DLINX;

	class DRAWT :
		public ZOBJ,
		public DRAWS,
		public POBJ,
		public ILINX<DLINX>
	{
		friend class DRAWX;

	public:
		explicit DRAWT();
		virtual ~DRAWT();

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	private:
		virtual void Open_( const std::string_view, DLINX* ) override;
		virtual void Close_(                        DLINX* ) override;

	public:
		using ILINX::Open;
		using ILINX::Close;
		virtual void Open( const std::string_view, const char* );

	private:
		virtual void Register(   LINX* ) override;
		virtual void Release(    LINX* ) override;
		virtual void Register_( DLINX* ) override;
		virtual void Release_(  DLINX* ) override;

	public:
		using ILINX::Register;
		using ILINX::Release;
		virtual void Register( const char* );

	public:
		virtual auto GetBranch( void ) const->DLINX*;
		virtual void SetBranch( DLINX* );

	public:
		virtual const MATRIX* View( void ) const;
		virtual const MATRIX* Vinv( void ) const;
		virtual const MATRIX* Bill( void ) const;
		virtual const MATRIX* Proj( void ) const;

	public:
		virtual const VECTOR3* LightPosition(  UINT ) const;
		virtual const VECTOR3* LightDirection( UINT ) const;
		virtual const COLOR Ambient( void ) const;

	public:
		virtual void DrawInfo( void ) const;
		virtual void DrawInfo( int  ) const;

	protected:
		virtual void DrawInit(  void );
		virtual void DrawEnter( void );
		virtual void DrawFunc(  void );
		virtual void DrawBegin( void );
		virtual void DrawMain(  void );
		virtual void DrawEnd(   void );
		virtual void DrawExit(  void );
		virtual void DrawFree(  void );

	protected:
		virtual void DrawAnime( void );
		virtual void DrawAnime( int  );

	protected:
		virtual void TaskInfo(  void ) const override final;
		virtual void TaskInit(  void ) override final;
		virtual void TaskEnter( void ) override final;
		virtual void TaskFunc(  void ) override final;
		virtual void TaskBegin( void ) override final;
		virtual void TaskMain(  void ) override final;
		virtual void TaskMain(  UINT ) override final;
		virtual void TaskEnd(   void ) override final;
		virtual void TaskExit(  void ) override final;
		virtual void TaskFree(  void ) override final;

	private:
		virtual void particle( void ) final;

	public:
		virtual void Coordinate(   const SUPERT* = nullptr ) override;
		virtual void ReCoordinate( const SUPERT* = nullptr ) override;

	public:
		virtual DLINX* DLinx( void ) const;
		static  DLINX* TLinx( const DRAWT* );
		static  LINX*  TLinx( void );
	};
}

//========================================
// サブシステム
//========================================
#include "draw2.hpp"
#include "draw3.hpp"

//========================================
// グラフィックタスク専用リンク
//========================================
namespace opal {
	class DLINX : public TLINX<DRAWT>{};
	class DRAWL : public TASKL<DRAWT,DLINX>{};
	template<typename TYPE> class DRAWTL : public TASKL<TYPE,DLINX>{};

	class DRAW2L : public TASKL<DRAWT2,DLINX>{};
	class DRAW3L : public TASKL<DRAWT3,DLINX>{};
}

// End Of File
