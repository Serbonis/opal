#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// JOY PAD
//========================================
namespace opal {
	enum {
		JOY1 				= 0UL,
		JOY2,
		JOY3,
		JOY4,

		PAD_NULL			= 0UL,
		PAD_NONE			= PAD_NULL,

		PAD_A				= ( 1 <<  0 ),		// ×ボタン
		PAD_B				= ( 1 <<  1 ),		// ○ボタン
		PAD_X				= ( 1 <<  2 ),		// □ボタン
		PAD_Y				= ( 1 <<  3 ),		// △ボタン

		PAD_L1				= ( 1 <<  4 ),		// L1トリガ
		PAD_R1				= ( 1 <<  5 ),		// R1トリガ
		PAD_L2				= ( 1 <<  6 ),		// L2トリガ
		PAD_R2				= ( 1 <<  7 ),		// R2トリガ

		PAD_SL				= ( 1 <<  8 ),		// SELECTボタン
		PAD_ST				= ( 1 <<  9 ),		// STARTボタン
		PAD_L3				= ( 1 << 10 ),		// L3トリガ
		PAD_R3				= ( 1 << 11 ),		// R3トリガ

		PAD_U				= ( 1 << 12 ),		// 方向キー上
		PAD_R				= ( 1 << 13 ),		// 方向キー右
		PAD_D				= ( 1 << 14 ),		// 方向キー下
		PAD_L				= ( 1 << 15 ),		// 方向キー左

		PAD_UR				= PAD_U|PAD_R,
		PAD_DR				= PAD_D|PAD_R,
		PAD_UL				= PAD_U|PAD_L,
		PAD_DL				= PAD_D|PAD_L,

		PAD_UP				= PAD_U,
		PAD_DOWN			= PAD_D,
		PAD_LEFT			= PAD_L,
		PAD_RIGHT			= PAD_R,
		PAD_START			= PAD_ST,
		PAD_SELECT			= PAD_SL,
		PAD_BU				= PAD_A,
		PAD_BL				= PAD_B,
		PAD_BR				= PAD_X,
		PAD_BD				= PAD_Y,

		PAD_CROSS			= PAD_U|PAD_D|PAD_L|PAD_R,
		PAD_BUTTON			= PAD_A|PAD_B|PAD_X|PAD_Y,
		PAD_TRIG1			= PAD_L1|PAD_R1,
		PAD_TRIG2			= PAD_L2|PAD_R2,
		PAD_TRIGGER			= PAD_TRIG1|PAD_TRIG2,

		// Stick
		STICK_LX			= ( 1 <<  0 ),
		STICK_LY			= ( 1 <<  1 ),
		STICK_RX			= ( 1 <<  2 ),
		STICK_RY			= ( 1 <<  3 ),
	};
}

//========================================
// MOUSE
//========================================
namespace opal {
	enum {
		MOUSE_NULL			= 0UL,
		MOUSE_NONE			= MOUSE_NULL,

		MOUSE_C0			= ( 1 <<  0 ),
		MOUSE_C1			= ( 1 <<  1 ),
		MOUSE_C2			= ( 1 <<  2 ),
		MOUSE_C3			= ( 1 <<  3 ),
		MOUSE_C4			= ( 1 <<  4 ),
		MOUSE_C5			= ( 1 <<  5 ),
		MOUSE_C6			= ( 1 <<  6 ),
		MOUSE_C7			= ( 1 <<  7 ),

		MOUSE_LC			= MOUSE_C0,
		MOUSE_RC			= MOUSE_C1,
		MOUSE_CC			= MOUSE_C2,
	};
}

//========================================
// KEY BOARD
//========================================
namespace opal {
	enum {
		KEY_NULL			,	// 0x00
		KEY_NONE = KEY_NULL	,

		KEY_ESCAPE			,	// 0x01
		KEY_1				,	// 0x02
		KEY_2				,	// 0x03
		KEY_3				,	// 0x04
		KEY_4				,	// 0x05
		KEY_5				,	// 0x06
		KEY_6				,	// 0x07
		KEY_7				,	// 0x08
		KEY_8				,	// 0x09
		KEY_9				,	// 0x0A
		KEY_0				,	// 0x0B
		KEY_MINUS			,	// 0x0C - on main keyboard
		KEY_EQUALS			,	// 0x0D
		KEY_BACK			,	// 0x0E backspace
		KEY_TAB				,	// 0x0F

		KEY_Q				,	// 0x10
		KEY_W				,	// 0x11
		KEY_E				,	// 0x12
		KEY_R				,	// 0x13
		KEY_T				,	// 0x14
		KEY_Y				,	// 0x15
		KEY_U				,	// 0x16
		KEY_I				,	// 0x17
		KEY_O				,	// 0x18
		KEY_P				,	// 0x19
		KEY_LBRACKET		,	// 0x1A
		KEY_RBRACKET		,	// 0x1B
		KEY_RETURN			,	// 0x1C Enter on main keyboard
		KEY_LCONTROL		,	// 0x1D
		KEY_A				,	// 0x1E
		KEY_S				,	// 0x1F

		KEY_D				,	// 0x20
		KEY_F				,	// 0x21
		KEY_G				,	// 0x22
		KEY_H				,	// 0x23
		KEY_J				,	// 0x24
		KEY_K				,	// 0x25
		KEY_L				,	// 0x26
		KEY_SEMICOLON		,	// 0x27
		KEY_APOSTROPHE		,	// 0x28
		KEY_GRAVE			,	// 0x29 accent grave
		KEY_LSHIFT			,	// 0x2A
		KEY_BACKSLASH		,	// 0x2B
		KEY_Z				,	// 0x2C
		KEY_X				,	// 0x2D
		KEY_C				,	// 0x2E
		KEY_V				,	// 0x2F

		KEY_B				,	// 0x30
		KEY_N				,	// 0x31
		KEY_M				,	// 0x32
		KEY_COMMA			,	// 0x33
		KEY_PERIOD			,	// 0x34 . on main keyboard
		KEY_SLASH			,	// 0x35 / on main keyboard
		KEY_RSHIFT			,	// 0x36
		KEY_MULTIPLY		,	// 0x37 * on numeric keypad
		KEY_LMENU			,	// 0x38 left Alt
		KEY_SPACE			,	// 0x39
		KEY_CAPITAL			,	// 0x3A
		KEY_F1				,	// 0x3B
		KEY_F2				,	// 0x3C
		KEY_F3				,	// 0x3D
		KEY_F4				,	// 0x3E
		KEY_F5				,	// 0x3F

		KEY_F6				,	// 0x40
		KEY_F7				,	// 0x41
		KEY_F8				,	// 0x42
		KEY_F9				,	// 0x43
		KEY_F10				,	// 0x44
		KEY_NUMLOCK			,	// 0x45
		KEY_SCROLL			,	// 0x46 Scroll Lock
		KEY_NUMPAD7			,	// 0x47
		KEY_NUMPAD8			,	// 0x48
		KEY_NUMPAD9			,	// 0x49
		KEY_SUBTRACT		,	// 0x4A - on numeric keypad
		KEY_NUMPAD4			,	// 0x4B
		KEY_NUMPAD5			,	// 0x4C
		KEY_NUMPAD6			,	// 0x4D
		KEY_ADD				,	// 0x4E + on numeric keypad
		KEY_NUMPAD1			,	// 0x4F

		KEY_NUMPAD2			,	// 0x50
		KEY_NUMPAD3			,	// 0x51
		KEY_NUMPAD0			,	// 0x52
		KEY_DECIMAL			,	// 0x53 . on numeric keypad
		KEY_DUMMY_0x54		,	// 0x54
		KEY_DUMMY_0x55		,	// 0x55
		KEY_OEM_102			,	// 0x56 <> or \| on RT 102-key keyboard (Non-U.S.)
		KEY_F11				,	// 0x57
		KEY_F12				,	// 0x58
		KEY_DUMMY_0x59		,	// 0x59
		KEY_DUMMY_0x5A		,	// 0x5A
		KEY_DUMMY_0x5B		,	// 0x5B
		KEY_DUMMY_0x5C		,	// 0x5C
		KEY_DUMMY_0x5D		,	// 0x5D
		KEY_DUMMY_0x5E		,	// 0x5E
		KEY_DUMMY_0x5F		,	// 0x5F

		KEY_DUMMY_0x60		,	// 0x60
		KEY_DUMMY_0x61		,	// 0x61
		KEY_DUMMY_0x62		,	// 0x62
		KEY_DUMMY_0x63		,	// 0x63
		KEY_F13				,	// 0x64 (NEC PC98)
		KEY_F14				,	// 0x65 (NEC PC98)
		KEY_F15				,	// 0x66 (NEC PC98)
		KEY_DUMMY_0x67		,	// 0x67
		KEY_DUMMY_0x68		,	// 0x68
		KEY_DUMMY_0x69		,	// 0x69
		KEY_DUMMY_0x6A		,	// 0x6A
		KEY_DUMMY_0x6B		,	// 0x6B
		KEY_DUMMY_0x6C		,	// 0x6C
		KEY_DUMMY_0x6D		,	// 0x6D
		KEY_DUMMY_0x6E		,	// 0x6E
		KEY_DUMMY_0x6F		,	// 0x6F

		KEY_KANA			,	// 0x70 (Japanese keyboard)
		KEY_DUMMY_0x70		,	// 0x71
		KEY_DUMMY_0x71		,	// 0x72
		KEY_ABNT_C1			,	// 0x73 /? on Brazilian keyboard
		KEY_DUMMY_0x74		,	// 0x74
		KEY_DUMMY_0x75		,	// 0x75
		KEY_DUMMY_0x76		,	// 0x76
		KEY_DUMMY_0x77		,	// 0x77
		KEY_DUMMY_0x78		,	// 0x78
		KEY_CONVERT			,	// 0x79 (Japanese keyboard)
		KEY_DUMMY_0x7A		,	// 0x7A
		KEY_NOCONVERT		,	// 0x7B (Japanese keyboard)
		KEY_DUMMY_0x7C		,	// 0x7C
		KEY_YEN				,	// 0x7D (Japanese keyboard)
		KEY_ABNT_C2			,	// 0x7E Numpad . on Brazilian keyboard
		KEY_DUMMY_0x7F		,	// 0x7F

		KEY_DUMMY_0x80		,	// 0x80
		KEY_DUMMY_0x81		,	// 0x81
		KEY_DUMMY_0x82		,	// 0x82
		KEY_DUMMY_0x83		,	// 0x83
		KEY_DUMMY_0x84		,	// 0x84
		KEY_DUMMY_0x85		,	// 0x85
		KEY_DUMMY_0x86		,	// 0x86
		KEY_DUMMY_0x87		,	// 0x87
		KEY_DUMMY_0x88		,	// 0x88
		KEY_DUMMY_0x89		,	// 0x89
		KEY_DUMMY_0x8A		,	// 0x8A
		KEY_DUMMY_0x8B		,	// 0x8B
		KEY_DUMMY_0x8C		,	// 0x8C
		KEY_NUMPADEQUALS	,	// 0x8D		= on numeric keypad (NEC PC98)
		KEY_DUMMY_0x8E		,	// 0x8E
		KEY_DUMMY_0x8F		,	// 0x8F

		KEY_CIRCUMFLEX		,	// 0x90 Previous Track (DIK_PREVTRACK on English keyboard)
		KEY_AT				,	// 0x91 (NEC PC98)
		KEY_COLON			,	// 0x92 (NEC PC98)
		KEY_UNDERLINE		,	// 0x93 (NEC PC98)
		KEY_KANJI			,	// 0x94 (Japanese keyboard)
		KEY_STOP			,	// 0x95 (NEC PC98)
		KEY_AX				,	// 0x96 (Japan AX)
		KEY_UNLABELED		,	// 0x97 (J3100)
		KEY_DUMMY_0x98		,	// 0x98
		KEY_NEXTTRACK		,	// 0x99 Next Track
		KEY_DUMMY_0x9A		,	// 0x9A
		KEY_DUMMY_0x9B		,	// 0x9B
		KEY_NUMPADENTER		,	// 0x9C Enter on numeric keypad
		KEY_RCONTROL		,	// 0x9D
		KEY_DUMMY_0x9E		,	// 0x9E
		KEY_DUMMY_0x9F		,	// 0x9F

		KEY_MUTE			,	// 0xA0 Mute
		KEY_CALCULATOR		,	// 0xA1 Calculator
		KEY_PLAYPAUSE		,	// 0xA2 Play / Pause
		KEY_DUMMY_0xA3		,	// 0xA3
		KEY_MEDIASTOP		,	// 0xA4 Media Stop
		KEY_DUMMY_0xA5		,	// 0xA5
		KEY_DUMMY_0xA6		,	// 0xA6
		KEY_DUMMY_0xA7		,	// 0xA7
		KEY_DUMMY_0xA8		,	// 0xA8
		KEY_DUMMY_0xA9		,	// 0xA9
		KEY_DUMMY_0xAA		,	// 0xAA
		KEY_DUMMY_0xAB		,	// 0xAB
		KEY_DUMMY_0xAC		,	// 0xAC
		KEY_DUMMY_0xAD		,	// 0xAD
		KEY_VOLUMEDOWN		,	// 0xAE Volume -
		KEY_DUMMY_0xAF		,	// 0xAF

		KEY_VOLUMEUP		,	// 0xB0 Volume +
		KEY_DUMMY_0xB1		,	// 0xB1
		KEY_WEBHOME			,	// 0xB2 Web home
		KEY_NUMPADCOMMA		,	// 0xB3, on numeric keypad (NEC PC98)
		KEY_DUMMY_0xB4		,	// 0xB4
		KEY_DIVIDE			,	// 0xB5 / on numeric keypad
		KEY_DUMMY_0xB6		,	// 0xB6
		KEY_SYSRQ			,	// 0xB7 PrintScreen|SysRq
		KEY_RMENU			,	// 0xB8 right Alt
		KEY_DUMMY_0xB9		,	// 0xB9
		KEY_DUMMY_0xBA		,	// 0xBA
		KEY_DUMMY_0xBB		,	// 0xBB
		KEY_DUMMY_0xBC		,	// 0xBC
		KEY_DUMMY_0xBD		,	// 0xBD
		KEY_DUMMY_0xBE		,	// 0xBE
		KEY_DUMMY_0xBF		,	// 0xBF

		KEY_DUMMY_0xC0		,	// 0xC0
		KEY_DUMMY_0xC1		,	// 0xC1
		KEY_DUMMY_0xC2		,	// 0xC2
		KEY_DUMMY_0xC3		,	// 0xC3
		KEY_DUMMY_0xC4		,	// 0xC4
		KEY_PAUSE			,	// 0xC5 Pause|Break
		KEY_DUMMY_0xC6		,	// 0xC6
		KEY_HOME			,	// 0xC7 Home on arrow keypad
		KEY_UP				,	// 0xC8 UpArrow on arrow keypad
		KEY_PRIOR			,	// 0xC9 PgUp on arrow keypad
		KEY_DUMMY_0xCA		,	// 0xCA
		KEY_LEFT			,	// 0xCB LeftArrow on arrow keypad
		KEY_DUMMY_0xCC		,	// 0xCC
		KEY_RIGHT			,	// 0xCD RightArrow on arrow keypad
		KEY_DUMMY_0xCE		,	// 0xCE
		KEY_END				,	// 0xCF End on arrow keypad

		KEY_DOWN			,	// 0xD0 DownArrow on arrow keypad
		KEY_NEXT			,	// 0xD1 PgDn on arrow keypad
		KEY_INSERT			,	// 0xD2 Insert on arrow keypad
		KEY_DELETE			,	// 0xD3 Delete on arrow keypad
		KEY_DUMMY_0xD4		,	// 0xD4
		KEY_DUMMY_0xD5		,	// 0xD5
		KEY_DUMMY_0xD6		,	// 0xD6
		KEY_DUMMY_0xD7		,	// 0xD7
		KEY_DUMMY_0xD8		,	// 0xD8
		KEY_DUMMY_0xD9		,	// 0xD9
		KEY_DUMMY_0xDA		,	// 0xDA
		KEY_LWIN			,	// 0xDB Left Windows key
		KEY_RWIN			,	// 0xDC Right Windows key
		KEY_APPS			,	// 0xDD AppMenu key
		KEY_POWER			,	// 0xDE System Power
		KEY_SLEEP			,	// 0xDF System Sleep

		KEY_DUMMY_0xE0		,	// 0xE0
		KEY_DUMMY_0xE1		,	// 0xE1
		KEY_DUMMY_0xE2		,	// 0xE2
		KEY_WAKE			,	// 0xE3 System Wake
		KEY_DUMMY_0xE4		,	// 0xE4
		KEY_WEBSEARCH		,	// 0xE5 Web Search
		KEY_WEBFAVORITES	,	// 0xE6 Web Favorites
		KEY_WEBREFRESH		,	// 0xE7 Web Refresh
		KEY_WEBSTOP			,	// 0xE8 Web Stop
		KEY_WEBFORWARD		,	// 0xE9 Web Forward
		KEY_WEBBACK			,	// 0xEA Web Back
		KEY_MYCOMPUTER		,	// 0xEB My Computer
		KEY_MAIL			,	// 0xEC Mail
		KEY_MEDIASELECT		,	// 0xED Media Select
		KEY_DUMMY_0xEE		,	// 0xEE
		KEY_DUMMY_0xEF		,	// 0xEF

		KEY_FUNCTION		,	// 0xF0
		KEY_ALPHABET		,	// 0xF1
		KEY_NUMBER			,	// 0xF2
		KEY_SHIFT			,	// 0xF3
		KEY_CONTROL			,	// 0xF4
		KEY_ALT				,	// 0xF5
		KEY_ARROW			,	// 0xF6
		KEY_NUMPAD			,	// 0xF7
		KEY_SUPER			,	// 0xF8
		KEY_DUMMY_0xF9		,	// 0xF9
		KEY_DUMMY_0xFA		,	// 0xFA
		KEY_DUMMY_0xFB		,	// 0xFB
		KEY_DUMMY_0xFC		,	// 0xFC
		KEY_DUMMY_0xFD		,	// 0xFD
		KEY_DUMMY_0xFE		,	// 0xFE
		KEY_ANY				,	// 0xFF
	};
}

//========================================
// Alternate names for keys,
// to facilitate transition from DOS.
//========================================
namespace opal {
	enum {
		KEY_BACKSPACE		= KEY_BACK,			// backspace
		KEY_NUMPADSTAR		= KEY_MULTIPLY,		// * on numeric keypad
		KEY_LALT			= KEY_LMENU,		// left Alt
		KEY_CAPSLOCK		= KEY_CAPITAL,		// CapsLock
		KEY_NUMPADMINUS		= KEY_SUBTRACT,		// - on numeric keypad
		KEY_NUMPADPLUS		= KEY_ADD,			// + on numeric keypad
		KEY_NUMPADPERIOD	= KEY_DECIMAL,		// . on numeric keypad
		KEY_NUMPADSLASH		= KEY_DIVIDE,		// / on numeric keypad
		KEY_RALT			= KEY_RMENU,		// right Alt
		KEY_UPARROW			= KEY_UP,			// UpArrow on arrow keypad
		KEY_PGUP			= KEY_PRIOR,		// PgUp on arrow keypad
		KEY_LEFTARROW		= KEY_LEFT,			// LeftArrow on arrow keypad
		KEY_RIGHTARROW		= KEY_RIGHT,		// RightArrow on arrow keypad
		KEY_DOWNARROW		= KEY_DOWN,			// DownArrow on arrow keypad
		KEY_PGDN			= KEY_NEXT,			// PgDn on arrow keypad
		KEY_PRINTSCREEN		= KEY_SYSRQ,		// PrintScreen
	};
}

//========================================
// Custom alias
//========================================
namespace opal {
	enum {
		KEY_PAGEUP			= KEY_PRIOR,
		KEY_PAGEDOWN		= KEY_NEXT,
		KEY_BREAK			= KEY_PAUSE,
		KEY_ENTER			= KEY_RETURN,
		KEY_CTRL			= KEY_CONTROL,
		KEY_LCTRL			= KEY_LCONTROL,
		KEY_RCTRL			= KEY_RCONTROL,
	};
}

// End Of File
