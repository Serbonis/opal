#pragma once
//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイル情報
//========================================
namespace opal {
	class FILED : public MESSAGE_DATA, public MESSAGE_TASK {
		using MESSAGE_DATA::MESSAGE_DATA;
		using MESSAGE_TASK::MESSAGE_TASK;
	};

	class FILEI : public FILED {
		friend class FILEX;
		friend class FILET;

	public:
		using FILED::FILED;

		explicit FILEI() = default;
		virtual ~FILEI() = default;

	protected:
		enum {
			TASK_NULL = -1,
			TASK_READ,
			TASK_WRITE,
		};

		enum {
			ERROR_NULL,
			ERROR_TASK,
			ERROR_STEP,
			ERROR_MODE,
			ERROR_INIT,
			ERROR_OPEN,
			ERROR_EXEC,
		};

#ifdef OPAL_DEBUG
	public:
		void Debug( void ) const;
		void Error( const FILET* ) const;
#endif
	};
}

// End Of File
