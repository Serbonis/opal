#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

#include "work.hpp"

#include "tlinx.hpp"

#include "Obj/zobj.hpp"

//========================================
// コードワーク
//========================================
namespace opal {
	class WORKS {
		friend class WORKT;

	public:
		explicit WORKS();
		virtual ~WORKS();

	protected:
		enum {
			WORK_STATE_NULL		= 0L,
			WORK_STATE_PAUSE	= 1 <<  0,
			WORK_STATE_PAUSE_T	= 1 <<  1,
			WORK_STATE_FUNC		= 1 <<  2,
			WORK_STATE_MAIN 	= 1 <<  3,

			WORK_STATE_DEFAULT	= WORK_STATE_NULL,
		};
		UINT	state;

	protected:
		struct FLAG final {
			FLAG();
		}	flag;

	protected:

		struct PROP final {
			PROP();
		}	prop;

	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual void Coordinate( const WORKS* = nullptr );
	};
}

//========================================
// コードタスク
//========================================
namespace opal {
	class WLINX;

	class WORKT :
		public ZOBJ,
		public WORKS,
		public ILINX<WLINX>
	{
		friend class WORKX;

	public:
		explicit WORKT();
		virtual ~WORKT();

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	private:
		virtual void Open_( const std::string_view, WLINX* ) override;
		virtual void Close_(                        WLINX* ) override;

	public:
		using ILINX::Open;
		using ILINX::Close;
		virtual void Open( const std::string_view, const char* );

	private:
		virtual void Register(   LINX* ) override;
		virtual void Release(    LINX* ) override;
		virtual void Register_( WLINX* ) override;
		virtual void Release_(  WLINX* ) override;

	public:
		using ILINX::Register;
		using ILINX::Release;
		virtual void Register( const char* );

	public:
		virtual auto GetBranch( void ) const->WLINX*;
		virtual void SetBranch( WLINX* );

	public:
		virtual void WorkInfo( void ) const;
		virtual void WorkInfo( int  ) const;

	protected:
		virtual void WorkInit(  void );
		virtual void WorkEnter( void );
		virtual void WorkFunc(  void );
		virtual void WorkBegin( void );
		virtual void WorkMain(  void );
		virtual void WorkEnd(   void );
		virtual void WorkExit(  void );
		virtual void WorkFree(  void );

	protected:
		virtual void TaskInfo(  void ) const override final;
		virtual void TaskInit(  void ) override final;
		virtual void TaskEnter( void ) override final;
		virtual void TaskFunc(  void ) override final;
		virtual void TaskBegin( void ) override final;
		virtual void TaskMain(  void ) override final;
		virtual void TaskEnd(   void ) override final;
		virtual void TaskExit(  void ) override final;
		virtual void TaskFree(  void ) override final;

	public:
		virtual void Coordinate(   const SUPERT* = nullptr ) override;
		virtual void ReCoordinate( const SUPERT* = nullptr ) override;

	public:
		virtual WLINX* WLinx( void ) const;
		static  WLINX* TLinx( const WORKT* );
		static  LINX*  TLinx( void );
	};
}

//========================================
// コードタスク専用リンク
//========================================
namespace opal {
	class WLINX : public TLINX<WORKT>{};
	class WORKL : public TASKL<WORKT,WLINX>{};
	template<typename TYPE> class WAORKTL : public TASKL<TYPE,WLINX>{};
}

// End Of File
