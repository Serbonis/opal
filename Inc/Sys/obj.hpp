#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// オブジェクトシステム
//========================================
namespace opal {
	class OBJT;

	class OBJX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class OBJT;

	private:
		static LINX*	index;
		static LINX*	linx;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static void  Linx( LINX* );
		static void  Linx( LINX& );
		static void  Linx( std::shared_ptr<LINX> );
		static LINX* Linx( void  );

#ifdef OPAL_DEBUG
	private:
		static int	debug;

	public:
		static void Debug( int  );
		static int  Debug( void );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "objx.hpp"

// End Of File
