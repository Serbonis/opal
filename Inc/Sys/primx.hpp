#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"

//========================================
// プリミティブジェネレーター
//========================================
namespace opal {
	template<typename TYPE, typename PRIM, typename... ARGS>class PRIMT : public PRIM {
	protected:
		explicit PRIMT( ARGS... args ) : PRIM( std::forward<ARGS>( args )... ) {}

	public:
		virtual ~PRIMT() = default;

	protected:
		std::shared_ptr<TYPE>	self;

	public:
		using TASK::Self;
		auto Self( std::shared_ptr<TYPE> p ){

			PRIMX::Register( p );
			Self( []( auto p ){ DELETES( dynamic_cast<TYPE*>( p ) ); } );

			return self = p;
		}
	};
}

// End O File
