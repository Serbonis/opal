#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// グラフィックシステムオブジェクト
//========================================
#include "draw/camerax.hpp"
#include "draw/lightx.hpp"

// End Of File
