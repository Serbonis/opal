#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// エンジンコアシステム
//========================================
extern int Opal( void );

namespace opal {
	class SYSTEM final : private NOINSTANCE {
		friend int ::Opal( void );
		friend class DEBUGX;

		//----------------------------------------
		// static member variables
		//----------------------------------------
	private:
		static int	result;
		static UINT	reset;
		static bool flag;

		//----------------------------------------
		// static member functions
		//----------------------------------------
	private:
		static void Main( void );
		static UINT CALLBACK process( void* );

	private:
		static bool Active(  void );
		static bool Process( void );
		static bool Suspend( void );

	private:
		static int SysInit( void );
		static int SysExec( void );
		static int SysFree( void );

	public:
		static int  Result( void );
		static void Result( int );
		static UINT Reset( void );
	};
}

//========================================
// システムワーク
//========================================
namespace opal {
	class SYSTEMX {
	protected:
		explicit SYSTEMX() = default;
	public:
		virtual ~SYSTEMX() = default;

	private:
		virtual bool SysInit( void ) = 0;
		virtual bool SysExec( void ) = 0;
		virtual bool SysFree( void ) = 0;

	private:
		virtual void SysDebug( UINT = 0 ) = 0;

	public:
		using syswork = std::shared_ptr<void>;
		using sysinit = std::function<void(void*)>;
		using sysexec = std::function<void(void*)>;
		using sysfree = std::function<void(void*)>;

		static void SysFunc( const std::string&, sysinit, sysexec, sysfree, syswork = nullptr );
		static void SysFunc( const std::string& );

		static void SysFuncInit( const std::string&, sysinit );
		static void SysFuncExec( const std::string&, sysexec );
		static void SysFuncFree( const std::string&, sysfree );

		static void SysFuncWork( const std::string&, syswork );
		static auto SysFuncWork( const std::string& )->void*;
	};
}

// End Of File
