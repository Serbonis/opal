#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xcall.hpp"

#ifdef _MSC_VER
// GUID_NULL 定義
#define INITGUID
#include <ks.h>
#endif

#include <dsound.h>

//========================================
// サウンドシステム
//========================================
//----------------------------------------
// デバイス
//----------------------------------------
namespace opal::CALLX_DS {
	class DEVICE {
	public:
		explicit DEVICE();
		virtual ~DEVICE();

	private:
		IDirectSound8*	device;

	public:
		virtual IDirectSound8* operator ()( void );
		virtual operator IDirectSound8*(    void );

	public:
		virtual void Init( void );
		virtual void Free( void );

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( void ) const;
#endif
	};
}

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::CALLX_DS {
	extern DEVICE	device;
}

// End Of File

