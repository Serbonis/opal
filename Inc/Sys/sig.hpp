#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// シグナルシステム
//========================================
namespace opal {
	class SIGT;
	class SIGX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	public:
		enum SIGNAL {
			NORMAL,
			SUSPEND,	// システムサスペンド
			DESTROY,	// ウィンドウクローズ
			RESET,		// ゲームリセット
			PAUSE,		// ゲームポーズ
			MAX,
		};

	public:
		enum PAUSE {
			PAUSE_LEVEL	= 2,
		};

	private:
		static SIGT	signal[MAX];
		static int	resume;
		static int	count;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		using FUNC = std::function<int(void*)>;
		using ARGV = void*;

		static void Signal( SIGNAL, const char* = nullptr, FUNC = nullptr, ARGV = nullptr );
		static void Kill(   SIGNAL, const char* = nullptr );

	public:
		static void Suspend( const char*, FUNC = nullptr, ARGV = nullptr );
		static void Destroy( const char*, FUNC = nullptr, ARGV = nullptr );
		static void Reset(   const char*, FUNC = nullptr, ARGV = nullptr );
		static void Pause(   const char*, FUNC = nullptr, ARGV = nullptr );
		static void Exit(    const char*, FUNC = nullptr, ARGV = nullptr );

		static void Suspend( FUNC, ARGV = nullptr );
		static void Destroy( FUNC, ARGV = nullptr );
		static void Reset(   FUNC, ARGV = nullptr );
		static void Pause(   FUNC, ARGV = nullptr );
		static void Exit(    FUNC, ARGV = nullptr );

	public:
		static void Level( SIGNAL, int );
		static int  Level( SIGNAL );

		static void Stat(  SIGNAL, int );
		static bool Stat(  SIGNAL );

	public:
		static void Destroy( int );
		static bool Destroy( void );

	public:
		static void ResetLevel( UINT );
		static void PauseLevel( UINT );

		static UINT ResetLevel( void );
		static UINT PauseLevel( void );

		static void Reset( int );
		static void Pause( int );

		static bool Reset( void );
		static bool Pause( void );

		static bool PauseCheck( UINT );

		static int  Resume( void );
		static int  Resume( int  );

	public:
		static void Break( const char* = nullptr );

	private:
		static bool SysPause(   void );
		static bool SysReset(   void );
		static bool SysDestroy( void );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "sigx.hpp"
#include "sigl.hpp"

// End Of File
