#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// デバッグシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal {
	namespace DEBUGX_ {
		extern void SysInit( void );
		extern void SysExec( void );
		extern void SysFree( void );
	}
}

// End Of File
