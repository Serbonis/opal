#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xcall.hpp"

#include <XAudio2.h>

//========================================
// サウンドシステム
//========================================
//----------------------------------------
// デバイス
//----------------------------------------
namespace opal::CALLX_XA {
	class DEVICE {
	public:
		explicit DEVICE();
		virtual ~DEVICE();

	private:
		IXAudio2*				device;
		IXAudio2MasteringVoice*	voice;

	public:
		virtual IXAudio2* operator ()( void );
		virtual operator IXAudio2*(    void );

	public:
		virtual void Init( void );
		virtual void Free( void );

#ifdef OPAL_DEBUG
	public:
		virtual void Debug( void ) const;
#endif
	};
}

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::CALLX_XA {
	extern DEVICE	device;
}

// End Of File

