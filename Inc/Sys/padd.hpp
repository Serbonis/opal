#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力データ
//========================================
namespace opal {
	class PADD {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit PADD();
		virtual ~PADD();

		//----------------------------------------
		// inner class
		//----------------------------------------
	public:

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		KEYBOARD	keyboard;
		MOUSE		mouse;
		JOYPAD		joypad[JOY_MAX];

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		void Clear( void );

	public:
		KEYBOARD* Keyboard( void );
		MOUSE*    Mouse(    void );
		JOYPAD*   Joypad(   void );
		JOYPAD*   Joypad(   UINT );
	};
}

//========================================
// パッドタスク
//========================================
namespace opal {
	class PADT : public TASK, public PADD {
	public:
		explicit PADT();
		virtual ~PADT();
	};
}

// End Of File
