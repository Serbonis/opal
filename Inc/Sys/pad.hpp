#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xpad.hpp"

#include "padx.hpp"
#include "padd.hpp"
#include "pada.hpp"
#include "padw.hpp"
#include "padc.hpp"
#include "pads.hpp"
#include "padr.hpp"

//========================================
// 入力システム
//========================================
namespace opal {
	class PADX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	public:
		using SYSFUNC = std::function<int(void)>;

	private:
		static UINT					joymax;		// 接続パッド数
		static PADD*				buff;		// 入力データ受け場所
		static PADW					work;		// 入力データワーク
		static PADC					count;		// 連続入力カウンタ
		static PADS					data[2];	// 処理先入力データ
		static UINT					sysfkey[];	// システム制御キー
		static bool					sysflag[];	// システム制御タイプ
		static SYSFUNC				sysfunc[];	// システム制御判定

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static void  SetData( const PADS* );
		static void  SetData( void );
		static PADS* GetData( bool = false );

	public:
		static UINT JoyMax( void );

	public:
		static const char* KeyName(   UINT );
		static const char* JoyName(   UINT );
		static const char* MouseName( UINT );

		//----------------------------------------
		// システムキー設定
		//----------------------------------------
	private:
		enum {
			SYSFUNC_HOME,
			SYSFUNC_EXIT,
			SYSFUNC_FULL,
			SYSFUNC_RESET,
			SYSFUNC_PAUSE,
			SYSFUNC_BREAK,
			SYSFUNC_CAPTURE,
			SYSFUNC_FRAME,
			SYSFUNC_TIMES,
			SYSFUNC_TIMEC,
			SYSFUNC_RANDOM,
			SYSFUNC_MAX,
		};
		static int  SysFunc( UINT );
		static void SysFunc( UINT, SYSFUNC );

		static UINT SysFkey( UINT );
		static void SysFkey( UINT, UINT, bool = false );

	public:
		static int  SysHome(    void );
		static int  SysExit(    void );
		static int  SysFull(    void );
		static int  SysReset(   void );
		static int  SysPause(   void );
		static int  SysBreak(   void );
		static int  SysCapture( void );
		static int  SysFrame(   void );
		static int  SysTimes(   void );
		static int  SysTimec(   void );
		static int  SysRandom(  void );

		static int  SysTimes(   int  );

		static void SysHome(    SYSFUNC );
		static void SysExit(    SYSFUNC );
		static void SysFull(    SYSFUNC );
		static void SysReset(   SYSFUNC );
		static void SysPause(   SYSFUNC );
		static void SysBreak(   SYSFUNC );
		static void SysCapture( SYSFUNC );
		static void SysFrame(   SYSFUNC );
		static void SysTimes(   SYSFUNC );
		static void SysTimec(   SYSFUNC );
		static void SysRandom(  SYSFUNC );

		static void SetSysHome(    void );
		static void SetSysExit(    void );
		static void SetSysFull(    void );
		static void SetSysReset(   void );
		static void SetSysPause(   void );
		static void SetSysBreak(   void );
		static void SetSysCapture( void );
		static void SetSysFrame(   void );
		static void SetSysTimes(   void );
		static void SetSysTimec(   void );
		static void SetSysRandom(  void );

		static void SetSysHome(    UINT );
		static void SetSysExit(    UINT );
		static void SetSysFull(    UINT );
		static void SetSysReset(   UINT );
		static void SetSysPause(   UINT );
		static void SetSysBreak(   UINT );
		static void SetSysCapture( UINT );
		static void SetSysFrame(   UINT );
		static void SetSysTimes(   UINT );
		static void SetSysTimec(   UINT );
		static void SetSysRandom(  UINT );

		static int  GetSysHome(    void );
		static int  GetSysExit(    void );
		static int  GetSysFull(    void );
		static int  GetSysReset(   void );
		static int  GetSysPause(   void );
		static int  GetSysBreak(   void );
		static int  GetSysCapture( void );
		static int  GetSysFrame(   void );
		static int  GetSysTimes(   void );
		static int  GetSysTimec(   void );
		static int  GetSysRandom(  void );

		//----------------------------------------
		// キー配列
		//----------------------------------------
	public:
		static UCHAR* Keyboard( void );

		//----------------------------------------
		// 判定関数
		//----------------------------------------
	public:
		static int KeyCount(    UINT );
		static int MouseCount(  UINT );
		static int JoyCount(    UINT, UINT );
		static int Joy1Count(         UINT );
		static int Joy2Count(         UINT );

		static int JKCount(  UINT, UINT, UINT );
		static int J1KCount(       UINT, UINT );
		static int J2KCount(       UINT, UINT );

		static int JKCount(  UINT, const PADA* );
		static int J1KCount(       const PADA* );
		static int J2KCount(       const PADA* );

	public:
		enum KEY_TYPE {
			TYPE_NONE,
			TYPE_PUSH,
			TYPE_TRIG,
			TYPE_RELS,
		};
		static int KeyType( UINT, UINT = KEY_ANY, UINT = 0, UINT = 1 );
		static int KeyNone(       UINT = KEY_ANY, UINT = 0, UINT = 1 );
		static int KeyPush(       UINT = KEY_ANY, UINT = 0, UINT = 1 );
		static int KeyTrig(       UINT = KEY_ANY, UINT = 0, UINT = 1 );
		static int KeyRels(       UINT = KEY_ANY, UINT = 0, UINT = 1 );

	public:
		static int JoyxPushO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JoyxTrigO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JoyxRelsO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JoyxPushA( UINT, UINT, UINT = 0, UINT = 1 );
		static int JoyxTrigA( UINT, UINT, UINT = 0, UINT = 1 );
		static int JoyxRelsA( UINT, UINT, UINT = 0, UINT = 1 );

		static int Joy1PushO( UINT, UINT = 0, UINT = 1 );
		static int Joy2PushO( UINT, UINT = 0, UINT = 1 );
		static int Joy1TrigO( UINT, UINT = 0, UINT = 1 );
		static int Joy2TrigO( UINT, UINT = 0, UINT = 1 );
		static int Joy1RelsO( UINT, UINT = 0, UINT = 1 );
		static int Joy2RelsO( UINT, UINT = 0, UINT = 1 );
		static int Joy1PushA( UINT, UINT = 0, UINT = 1 );
		static int Joy2PushA( UINT, UINT = 0, UINT = 1 );
		static int Joy1TrigA( UINT, UINT = 0, UINT = 1 );
		static int Joy2TrigA( UINT, UINT = 0, UINT = 1 );
		static int Joy1RelsA( UINT, UINT = 0, UINT = 1 );
		static int Joy2RelsA( UINT, UINT = 0, UINT = 1 );

		static float JoyxStick( UINT, UINT, UINT = 0 );
		static float Joy1Stick(       UINT, UINT = 0 );
		static float Joy2Stick(       UINT, UINT = 0 );

	public:
		static int MousePush( UINT, UINT = 0, UINT = 1 );
		static int MouseTrig( UINT, UINT = 0, UINT = 1 );
		static int MouseRels( UINT, UINT = 0, UINT = 1 );

		static float Mouse(  UINT, UINT = 0 );
		static float MouseX(       UINT = 0 );
		static float MouseY(       UINT = 0 );
		static float Scroll(       UINT = 0 );
		static auto  Mouse(        UINT = 0 )->std::pair<float,float>;

		static float Cursor(  UINT, UINT = 0 );
		static float CursorX(       UINT = 0 );
		static float CursorY(       UINT = 0 );
		static auto  Cursor(        UINT = 0 )->std::pair<float,float>;

	public:
		static int JxKPushO( UINT, UINT, UINT, UINT = 0, UINT = 1 );
		static int JxKTrigO( UINT, UINT, UINT, UINT = 0, UINT = 1 );
		static int JxKRelsO( UINT, UINT, UINT, UINT = 0, UINT = 1 );
		static int JxKPushA( UINT, UINT, UINT, UINT = 0, UINT = 1 );
		static int JxKTrigA( UINT, UINT, UINT, UINT = 0, UINT = 1 );
		static int JxKRelsA( UINT, UINT, UINT, UINT = 0, UINT = 1 );

		static int J1KPushO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KPushO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J1KTrigO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KTrigO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J1KRelsO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KRelsO( UINT, UINT, UINT = 0, UINT = 1 );
		static int J1KPushA( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KPushA( UINT, UINT, UINT = 0, UINT = 1 );
		static int J1KTrigA( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KTrigA( UINT, UINT, UINT = 0, UINT = 1 );
		static int J1KRelsA( UINT, UINT, UINT = 0, UINT = 1 );
		static int J2KRelsA( UINT, UINT, UINT = 0, UINT = 1 );

		static int JxKPushO( UINT, const PADA*, UINT = 0, UINT = 1 );
		static int JxKTrigO( UINT, const PADA*, UINT = 0, UINT = 1 );
		static int JxKRelsO( UINT, const PADA*, UINT = 0, UINT = 1 );
		static int JxKPushA( UINT, const PADA*, UINT = 0, UINT = 1 );
		static int JxKTrigA( UINT, const PADA*, UINT = 0, UINT = 1 );
		static int JxKRelsA( UINT, const PADA*, UINT = 0, UINT = 1 );

		static int J1KPushO( const PADA*, UINT = 0, UINT = 1 );
		static int J2KPushO( const PADA*, UINT = 0, UINT = 1 );
		static int J1KTrigO( const PADA*, UINT = 0, UINT = 1 );
		static int J2KTrigO( const PADA*, UINT = 0, UINT = 1 );
		static int J1KRelsO( const PADA*, UINT = 0, UINT = 1 );
		static int J2KRelsO( const PADA*, UINT = 0, UINT = 1 );
		static int J1KPushA( const PADA*, UINT = 0, UINT = 1 );
		static int J2KPushA( const PADA*, UINT = 0, UINT = 1 );
		static int J1KTrigA( const PADA*, UINT = 0, UINT = 1 );
		static int J2KTrigA( const PADA*, UINT = 0, UINT = 1 );
		static int J1KRelsA( const PADA*, UINT = 0, UINT = 1 );
		static int J2KRelsA( const PADA*, UINT = 0, UINT = 1 );

	public:
		static int JoyPushO( UINT, UINT = 0, UINT = 1 );
		static int JoyTrigO( UINT, UINT = 0, UINT = 1 );
		static int JoyRelsO( UINT, UINT = 0, UINT = 1 );
		static int JoyPushA( UINT, UINT = 0, UINT = 1 );
		static int JoyTrigA( UINT, UINT = 0, UINT = 1 );
		static int JoyRelsA( UINT, UINT = 0, UINT = 1 );

		static float JoyStick( UINT, UINT = 0 );

	public:
		static int JKPushO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JKTrigO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JKRelsO( UINT, UINT, UINT = 0, UINT = 1 );
		static int JKPushA( UINT, UINT, UINT = 0, UINT = 1 );
		static int JKTrigA( UINT, UINT, UINT = 0, UINT = 1 );
		static int JKRelsA( UINT, UINT, UINT = 0, UINT = 1 );

		static int JKPushO( const PADA*, UINT = 0, UINT = 1 );
		static int JKTrigO( const PADA*, UINT = 0, UINT = 1 );
		static int JKRelsO( const PADA*, UINT = 0, UINT = 1 );
		static int JKPushA( const PADA*, UINT = 0, UINT = 1 );
		static int JKTrigA( const PADA*, UINT = 0, UINT = 1 );
		static int JKRelsA( const PADA*, UINT = 0, UINT = 1 );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};

	inline int nullkey(){ return 0; }

	template<UINT KEY>int keytrig( void ){ return PADX::KeyTrig( KEY );	}
	template<UINT KEY>int keypush( void ){ return PADX::KeyPush( KEY );	}
	template<UINT KEY>int keyrels( void ){ return PADX::KeyRels( KEY );	}
}

//========================================
// デフォルトはパッド1
//========================================
#define JoyPush		Joy1PushO
#define JoyTrig		Joy1TrigO
#define JoyRels		Joy1RelsO
#define JoyxPush	JoyxPushO
#define JoyxTrig	JoyxTrigO
#define JoyxRels	JoyxRelsO
#define Joy1Push	Joy1PushO
#define Joy2Push	Joy2PushO
#define Joy1Trig	Joy1TrigO
#define Joy2Trig	Joy2TrigO
#define Joy1Rels	Joy1RelsO
#define Joy2Rels	Joy2RelsO
#define JKPush		JKPushO
#define JKTrig		JKTrigO
#define JKRels		JKRelsO
#define J1KPush		J1KPushO
#define J2KPush		J2KPushO
#define J1KTrig		J1KTrigO
#define J2KTrig		J2KTrigO
#define J1KRels		J1KRelsO
#define J2KRels		J2KRelsO

// End Of File
