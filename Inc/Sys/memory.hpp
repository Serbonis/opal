#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// メモリ管理システム
//========================================
namespace opal {
	class MEMT;
	class MEMORY final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	private:
		static LINX	lock;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		using ADRS = UINT64;

		static const char* Locked( ADRS, ADRS );
		static MEMT* Lock( ADRS, ADRS, const char* );
		static void  UnLock( MEMT* );
	};
}

//========================================
// サブシステム
//========================================
#include "memoryx.hpp"

// End Of File
