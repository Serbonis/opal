#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// サウンドシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal {
	namespace CALLX_ {
		extern void SysInitDevice( void );
		extern void SysFreeDevice( void );
	}
}

// End Of File
