#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// カメラマネージャー
//========================================
namespace opal {
	class CAMERAX {
		friend class DRAWX;
		friend class CAMERAS;
		friend class DEBUGX;

		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit CAMERAX() = default;
		virtual ~CAMERAX() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	protected:
		bool			active;

	protected:
		float			fov;
		CLIPF			clip;
		CSIZEF			screen;
		D3DVIEWPORT9	port;

	protected:
		MATRIX  		view;
		MATRIX  		pers;
		MATRIX  		orth;
		MATRIX  		vinv;
		MATRIX  		bill;

	protected:
		MATRIX  		vpers;
		MATRIX  		vorth;

	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual const MATRIX* View( void ) const;
		virtual const MATRIX* Proj( bool = true ) const;
		virtual const MATRIX* ViewProj( bool = true ) const;

	public:
		virtual float ZValue( const VECTOR3* ) const;

	public:
		virtual void SetViewPort( DWORD, DWORD, DWORD, DWORD );
		virtual const D3DVIEWPORT9* GetViewPort( void ) const;

	public:
		virtual void  SetFov( void );
		virtual void  SetFov( float );
		virtual void  AddFov( float );
		virtual float GetFov( void ) const;

	public:
		virtual void  SetClip( void );
		virtual void  SetClip( float, float );
		virtual void  SetClip( CLIPF );
		virtual CLIPF GetClip( void ) const;

		virtual void  SetNear( void );
		virtual void  SetFar(  void );
		virtual void  SetNear( float );
		virtual void  SetFar(  float );
		virtual float GetNear( void ) const;
		virtual float GetFar(  void ) const;

	public:
		virtual void Update( const VECTOR3&,
							 const VECTOR3&,
							 const VECTOR3& );

	};
}

// End Of File
