#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// システムカメラ
//========================================
namespace opal {
	class CAMERAX;

	namespace CAMERAS_C {
		extern const VECTOR3	pos;
		extern const float 		fov;
		extern const CLIPF 		clip;
	}

	class CAMERAS final {
		friend class DRAWX;
		friend class CAMERAX;
		friend class DEBUGX;

	public:
		explicit CAMERAS();
		virtual ~CAMERAS();

	private:
		bool							system;
		const CAMERAX*					active;

		VECTOR3							pos;
		float							fov;
		CLIPF							clip;

		const MATRIX*  					view;
		const MATRIX*  					pers;
		const MATRIX*  					vpers;
		const MATRIX*  					orth;
		const MATRIX*  					vorth;
		const MATRIX*  					vinv;
		const MATRIX*  					bill;

		MATRIX  						view_s;
		MATRIX  						pers_s;
		MATRIX  						vpers_s;
		MATRIX  						orth_s;
		MATRIX  						vorth_s;
		MATRIX  						vinv_s;
		MATRIX  						bill_s;

	public:
		void SysCamera( bool );
		bool SysCamera( void ) const;

		const CAMERAX* Camera( const CAMERAX* );
		const CAMERAX* Camera( void ) const;

		void  CameraAngle( float );
		float CameraAngle( void ) const;

		void    CameraPos( float, float, float );
		VECTOR3 CameraPos( void ) const;

		void  CameraPosX( float );
		void  CameraPosY( float );
		void  CameraPosZ( float );
		float CameraPosX( void  ) const;
		float CameraPosY( void  ) const;
		float CameraPosZ( void  ) const;

		void  CameraClip(  float, float );
		void  CameraClipN( float );
		void  CameraClipF( float );

		CLIPF CameraClip(  void ) const;
		float CameraClipN( void ) const;
		float CameraClipF( void ) const;

		const MATRIX* View( void ) const;
		const MATRIX* Pers( void ) const;
		const MATRIX* Orth( void ) const;
		const MATRIX* Vinv( void ) const;
		const MATRIX* Bill( void ) const;
		const MATRIX* Proj( bool = true ) const;
		const MATRIX* ViewProj( bool = true ) const;

		float ZValue( const VECTOR3*, const MATRIX* ) const;
		float ZValue( const VECTOR3* ) const;

	public:
		static void DrawCamera( const VECTOR3&,
								const VECTOR3&,
								const VECTOR3&,
								float, float, float, const CLIPF&,
								MATRIX*,
								MATRIX*,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr );

		static MATRIX* CameraView( MATRIX&, const VECTOR3&, const VECTOR3&, const VECTOR3& );
		static MATRIX* CameraVinv( MATRIX&, const MATRIX& );
		static MATRIX* CameraPers( MATRIX&, float, float, float, float, float );
		static MATRIX* CameraOrth( MATRIX&, float, float, float, float );
		static MATRIX* CameraBill( MATRIX&, const VECTOR3& );

	public:
		static void InitCamera( CAMERAS* );
		static void ExecCamera( CAMERAS* );
		static void FreeCamera( CAMERAS* );
	};
}

// End Of File
