#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// ライトマネージャー
//========================================
namespace opal {
	class LIGHTX {
		friend class DRAWX;
		friend class LIGHTS;
		friend class DEBUGX;

		//----------------------------------------
		// constructor & destructor
		//----------------------------------------

	public:
		explicit LIGHTX() = default;
		virtual ~LIGHTX() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	protected:
		bool	active;
		UINT	index;

	protected:
		DLIGHT	light;
		VECTOR3	pos;
		VECTOR3	dir;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual const VECTOR3* Position(  void ) const;
		virtual const VECTOR3* Direction( void ) const;

	public:
		virtual operator DLIGHT*( void );
	};
}

// End Of File
