#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// システムライト
//========================================
namespace opal {
	class LIGHTX;

	namespace LIGHTS_C {
		extern const DLIGHT 	property;
	}

	class LIGHTS final {
		friend class DRAWX;
		friend class LIGHTX;
		friend class DEBUGX;

	public:
		explicit LIGHTS();
		virtual ~LIGHTS();

	private:
		bool					system;
		COLOR					ambient;

	private:
		struct INSTANCE {
			const LIGHTX*		active;
			const bool*			flag;
			const DLIGHT*		prop;
			const VECTOR3*		pos;
			const VECTOR3*		dir;
			bool				flag_s;
			DLIGHT				prop_s;
			VECTOR3				pos_s;
			VECTOR3				dir_s;
		};

		std::vector<INSTANCE>	instance;

	public:
		void SysLight( bool vFlag );
		bool SysLight( void ) const;
		UINT SysLights( void ) const;

		const LIGHTX* Light( const LIGHTX* pLight, UINT vID = 0 );
		const LIGHTX* Light( UINT vID ) const;

		void Lighting( UINT vID, bool vFlag );
		bool Lighting( UINT vID ) const;

		COLOR Ambient( COLOR vCol );
		COLOR Ambient( UCHAR vR, UCHAR vG, UCHAR vB, UCHAR vA = 0xff );
		COLOR Ambient( void ) const;

		void LightPosition(  UINT vID, const VECTOR3* pPos );
		void LightDirection( UINT vID, const VECTOR3* pDir );
		void LightPosition(  const VECTOR3* pPos );
		void LightDirection( const VECTOR3* pDir );
		void LightPosition(  UINT vID, float vX, float vY, float vZ );
		void LightDirection( UINT vID, float vX, float vY, float vZ );
		void LightPosition(  float vX, float vY, float vZ );
		void LightDirection( float vX, float vY, float vZ );

	public:
		const VECTOR3* LightPosition(  UINT vID ) const;
		const VECTOR3* LightDirection( UINT vID ) const;

		static VECTOR3 LightPosition(  void );
		static VECTOR3 LightDirection( void );
		static COLOR   LightDiffuse( void );

	public:
		static void DrawLight( const VECTOR3& pos, const VECTOR3& dir, DLIGHT& prop );

	public:
		static void InitLight( LIGHTS* light );
		static void ExecLight( LIGHTS* light );
		static void FreeLight( LIGHTS* light );

	};
}

// End Of File

