#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

#include "heaps.hpp"
#include "heapx.hpp"

//========================================
// HEAP管理システム
//========================================
namespace opal {
	class HEAP final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

		friend void* New( void*, const char*, size_t, size_t );
		friend void  Del( const void* );

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	private:
		static HEAP_CORE* Core( void );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};

	static HEAP_SYSTEM	heap_system;
}

// End Of File

