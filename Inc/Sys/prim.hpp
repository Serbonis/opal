#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// プリミティブシステム
//========================================
namespace opal {
	class PRIMX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	private:
		static std::unordered_map<TASK*,std::weak_ptr<TASK>>	prim;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static void Register( std::shared_ptr<TASK> );
		static void Release( TASK* );
		static void Release( void );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "primx.hpp"
#include "prims.hpp"

// End Of File
