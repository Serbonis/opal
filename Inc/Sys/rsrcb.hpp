#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "file.hpp"

//========================================
// リソースバンク:vector型
//========================================
namespace opal {
	template<typename TYPE>struct RSRCBV : public PBANKV<TYPE> {
		using BANK = PBANKV<TYPE>;

		explicit RSRCBV() = default;
		virtual ~RSRCBV() = default;

		virtual void NowResource( UINT n ){ BANK::Index( n );				}
		virtual UINT NowResource( void   ) const { return BANK::Index();	}

		virtual void SetResource( const TYPE* r, UINT n ){ BANK::Set( n, const_cast<TYPE*>(  r ) );	}
		virtual void SetResource( const TYPE* r         ){ BANK::Set(    const_cast<TYPE*>(  r ) );	}
		virtual void SetResource( const TYPE& r, UINT n ){ BANK::Set( n, const_cast<TYPE*>( &r ) );	}
		virtual void SetResource( const TYPE& r         ){ BANK::Set(    const_cast<TYPE*>( &r ) );	}

		virtual const TYPE* GetResource( UINT n ) const { return BANK::Get( n );	}
		virtual const TYPE* GetResource( void   ) const { return BANK::Get(   );	}

		virtual UINT Resource( void ) const { return BANK::Size();	}
	};
}

//========================================
// リソースバンク:map型
//========================================
namespace opal {
	template<typename TYPE>struct RSRCBM : public PBANKM<TYPE> {
		using BANK = PBANKM<TYPE>;

		explicit RSRCBM() = default;
		virtual ~RSRCBM() = default;

		virtual void NowResource( const char* p ){ BANK::Index( p );			}
		virtual const char* NowResource( void   ) const { return BANK::Index();	}

		virtual void SetResource( const TYPE* r, const char* p ){ BANK::Set( p, const_cast<TYPE*>(  r ) );	}
		virtual void SetResource( const TYPE* r                ){ BANK::Set(    const_cast<TYPE*>(  r ) );	}
		virtual void SetResource( const TYPE& r, const char* p ){ BANK::Set( p, const_cast<TYPE*>( &r ) );	}
		virtual void SetResource( const TYPE& r                ){ BANK::Set(    const_cast<TYPE*>( &r ) );	}

		virtual const TYPE* GetResource( const char* p ) const { return BANK::Get( p ); }
		virtual const TYPE* GetResource( void          ) const { return BANK::Get(   ); }

		virtual UINT Resource( void ) const { return BANK::Size();	}

	protected:
		using BANK::Set;

		virtual void Set( const char* p, TYPE* v = nullptr ){

			if ( p ) {
				BANK::Set( p, v );
			} else if ( const auto f = v->File() ) {
				BANK::Set( GetFileName( f ), v );
			}
		}
	};
}

// End Of File
