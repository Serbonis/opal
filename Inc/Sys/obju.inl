//========================================
// OPAL Game Library
//========================================
#ifdef USING_OBJ
using USING_OBJ::Move;
using USING_OBJ::MoveX;
using USING_OBJ::MoveY;
using USING_OBJ::MoveZ;
using USING_OBJ::MoveXY;
using USING_OBJ::MoveYX;
using USING_OBJ::MoveYZ;
using USING_OBJ::MoveZY;
using USING_OBJ::MoveZX;
using USING_OBJ::MoveXZ;
using USING_OBJ::MoveXYZ;
using USING_OBJ::MoveXZY;
using USING_OBJ::MoveYXZ;
using USING_OBJ::MoveYZX;
using USING_OBJ::MoveZXY;
using USING_OBJ::MoveZYX;
using USING_OBJ::Turn;
using USING_OBJ::TurnX;
using USING_OBJ::TurnY;
using USING_OBJ::TurnZ;
using USING_OBJ::TurnXY;
using USING_OBJ::TurnYX;
using USING_OBJ::TurnYZ;
using USING_OBJ::TurnZY;
using USING_OBJ::TurnZX;
using USING_OBJ::TurnXZ;
using USING_OBJ::TurnXYZ;
using USING_OBJ::TurnXZY;
using USING_OBJ::TurnYXZ;
using USING_OBJ::TurnYZX;
using USING_OBJ::TurnZXY;
using USING_OBJ::TurnZYX;
using USING_OBJ::AddRotate;
using USING_OBJ::AddRotateX;
using USING_OBJ::AddRotateXY;
using USING_OBJ::AddRotateXYZ;
using USING_OBJ::AddRotateXZ;
using USING_OBJ::AddRotateXZY;
using USING_OBJ::AddRotateY;
using USING_OBJ::AddRotateYX;
using USING_OBJ::AddRotateYXZ;
using USING_OBJ::AddRotateYZ;
using USING_OBJ::AddRotateYZX;
using USING_OBJ::AddRotateZ;
using USING_OBJ::AddRotateZX;
using USING_OBJ::AddRotateZXY;
using USING_OBJ::AddRotateZY;
using USING_OBJ::AddRotateZYX;
using USING_OBJ::AddScale;
using USING_OBJ::AddScaleX;
using USING_OBJ::AddScaleXY;
using USING_OBJ::AddScaleXYZ;
using USING_OBJ::AddScaleXZ;
using USING_OBJ::AddScaleXZY;
using USING_OBJ::AddScaleY;
using USING_OBJ::AddScaleYX;
using USING_OBJ::AddScaleYXZ;
using USING_OBJ::AddScaleYZ;
using USING_OBJ::AddScaleYZX;
using USING_OBJ::AddScaleZ;
using USING_OBJ::AddScaleZX;
using USING_OBJ::AddScaleZXY;
using USING_OBJ::AddScaleZY;
using USING_OBJ::AddScaleZYX;
using USING_OBJ::AddTrans;
using USING_OBJ::AddTransX;
using USING_OBJ::AddTransXY;
using USING_OBJ::AddTransXYZ;
using USING_OBJ::AddTransXZ;
using USING_OBJ::AddTransXZY;
using USING_OBJ::AddTransY;
using USING_OBJ::AddTransYX;
using USING_OBJ::AddTransYXZ;
using USING_OBJ::AddTransYZ;
using USING_OBJ::AddTransYZX;
using USING_OBJ::AddTransZ;
using USING_OBJ::AddTransZX;
using USING_OBJ::AddTransZXY;
using USING_OBJ::AddTransZY;
using USING_OBJ::AddTransZYX;
using USING_OBJ::GetRotate;
using USING_OBJ::GetRotateX;
using USING_OBJ::GetRotateXY;
using USING_OBJ::GetRotateXYZ;
using USING_OBJ::GetRotateXZ;
using USING_OBJ::GetRotateXZY;
using USING_OBJ::GetRotateY;
using USING_OBJ::GetRotateYX;
using USING_OBJ::GetRotateYXZ;
using USING_OBJ::GetRotateYZ;
using USING_OBJ::GetRotateYZX;
using USING_OBJ::GetRotateZ;
using USING_OBJ::GetRotateZX;
using USING_OBJ::GetRotateZXY;
using USING_OBJ::GetRotateZY;
using USING_OBJ::GetRotateZYX;
using USING_OBJ::GetScale;
using USING_OBJ::GetScaleX;
using USING_OBJ::GetScaleXY;
using USING_OBJ::GetScaleXYZ;
using USING_OBJ::GetScaleXZ;
using USING_OBJ::GetScaleXZY;
using USING_OBJ::GetScaleY;
using USING_OBJ::GetScaleYX;
using USING_OBJ::GetScaleYXZ;
using USING_OBJ::GetScaleYZ;
using USING_OBJ::GetScaleYZX;
using USING_OBJ::GetScaleZ;
using USING_OBJ::GetScaleZX;
using USING_OBJ::GetScaleZXY;
using USING_OBJ::GetScaleZY;
using USING_OBJ::GetScaleZYX;
using USING_OBJ::GetTrans;
using USING_OBJ::GetTransX;
using USING_OBJ::GetTransXY;
using USING_OBJ::GetTransXYZ;
using USING_OBJ::GetTransXZ;
using USING_OBJ::GetTransXZY;
using USING_OBJ::GetTransY;
using USING_OBJ::GetTransYX;
using USING_OBJ::GetTransYXZ;
using USING_OBJ::GetTransYZ;
using USING_OBJ::GetTransYZX;
using USING_OBJ::GetTransZ;
using USING_OBJ::GetTransZX;
using USING_OBJ::GetTransZXY;
using USING_OBJ::GetTransZY;
using USING_OBJ::GetTransZYX;
using USING_OBJ::SetRotate;
using USING_OBJ::SetRotateX;
using USING_OBJ::SetRotateXY;
using USING_OBJ::SetRotateXYZ;
using USING_OBJ::SetRotateXZ;
using USING_OBJ::SetRotateXZY;
using USING_OBJ::SetRotateY;
using USING_OBJ::SetRotateYX;
using USING_OBJ::SetRotateYXZ;
using USING_OBJ::SetRotateYZ;
using USING_OBJ::SetRotateYZX;
using USING_OBJ::SetRotateZ;
using USING_OBJ::SetRotateZX;
using USING_OBJ::SetRotateZXY;
using USING_OBJ::SetRotateZY;
using USING_OBJ::SetRotateZYX;
using USING_OBJ::SetScale;
using USING_OBJ::SetScaleX;
using USING_OBJ::SetScaleXY;
using USING_OBJ::SetScaleXYZ;
using USING_OBJ::SetScaleXZ;
using USING_OBJ::SetScaleXZY;
using USING_OBJ::SetScaleY;
using USING_OBJ::SetScaleYX;
using USING_OBJ::SetScaleYXZ;
using USING_OBJ::SetScaleYZ;
using USING_OBJ::SetScaleYZX;
using USING_OBJ::SetScaleZ;
using USING_OBJ::SetScaleZX;
using USING_OBJ::SetScaleZXY;
using USING_OBJ::SetScaleZY;
using USING_OBJ::SetScaleZYX;
using USING_OBJ::SetTrans;
using USING_OBJ::SetTransX;
using USING_OBJ::SetTransXY;
using USING_OBJ::SetTransXYZ;
using USING_OBJ::SetTransXZ;
using USING_OBJ::SetTransXZY;
using USING_OBJ::SetTransY;
using USING_OBJ::SetTransYX;
using USING_OBJ::SetTransYXZ;
using USING_OBJ::SetTransYZ;
using USING_OBJ::SetTransYZX;
using USING_OBJ::SetTransZ;
using USING_OBJ::SetTransZX;
using USING_OBJ::SetTransZXY;
using USING_OBJ::SetTransZY;
using USING_OBJ::SetTransZYX;
using USING_OBJ::ZadRotate;
using USING_OBJ::ZadRotateX;
using USING_OBJ::ZadRotateY;
using USING_OBJ::ZadRotateZ;
using USING_OBJ::ZadScale;
using USING_OBJ::ZadScaleX;
using USING_OBJ::ZadScaleY;
using USING_OBJ::ZadScaleZ;
using USING_OBJ::ZadTransX;
using USING_OBJ::ZadTransY;
using USING_OBJ::ZadTransZ;
using USING_OBJ::ZetRotate;
using USING_OBJ::ZetRotateX;
using USING_OBJ::ZetRotateY;
using USING_OBJ::ZetRotateZ;
using USING_OBJ::ZetScale;
using USING_OBJ::ZetScaleX;
using USING_OBJ::ZetScaleY;
using USING_OBJ::ZetScaleZ;
using USING_OBJ::ZetTransX;
using USING_OBJ::ZetTransY;
using USING_OBJ::ZetTransZ;
#endif
