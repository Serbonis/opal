#pragma once
//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"
#include "Obj/oobj.hpp"

//========================================
// スカラーオブジェクト
//========================================
//----------------------------------------
// TRANS
//----------------------------------------
namespace opal {
	class SOBJ;

	struct SOBJ_T_D {
		explicit SOBJ_T_D();

		VECTOR3	tv;
		MATRIX	tm;
	};

	class SOBJ_T_I {
	public:
		virtual const VECTOR3* Trans(  void ) const = 0;
		virtual void Trans( const VECTOR3* ) = 0;
		virtual void Trans( const VECTOR3& ) = 0;

	protected:
		virtual void SetTrans_( float,  float,  float  ) = 0;
		virtual void AddTrans_( float,  float,  float  ) = 0;
		virtual void GetTrans_( float*, float*, float* ) const = 0;

	public:
		virtual void SetTrans( float,  float, float   );
		virtual void AddTrans( float,  float, float   );
		virtual void GetTrans( float*, float*, float* ) const;

		virtual void SetTrans( float,  float  );
		virtual void AddTrans( float,  float  );
		virtual void GetTrans( float*, float* ) const;

		virtual void SetTrans( void );
		virtual auto GetTrans( void ) const->VECTOR3;

	public:
		virtual void SetTrans( const std::tuple<float,float,float>& );
		virtual void AddTrans( const std::tuple<float,float,float>& );
		virtual void SetTrans( const std::pair<float,float>& );
		virtual void AddTrans( const std::pair<float,float>& );

	public:
		virtual void SetTrans( const SOBJ* );
		virtual void SetTrans( const SOBJ& );
		virtual void GetTrans( SOBJ* ) const;
		virtual void GetTrans( SOBJ& ) const;

	public:
		virtual void SetTrans( const VECTOR3* );
		virtual void AddTrans( const VECTOR3* );
		virtual void SetTrans( const VECTOR3& );
		virtual void AddTrans( const VECTOR3& );
		virtual void GetTrans( VECTOR3* ) const;
		virtual void GetTrans( VECTOR3& ) const;

		virtual void SetTrans( const VECTOR2* );
		virtual void AddTrans( const VECTOR2* );
		virtual void SetTrans( const VECTOR2& );
		virtual void AddTrans( const VECTOR2& );
		virtual void GetTrans( VECTOR2* ) const;
		virtual void GetTrans( VECTOR2& ) const;

		virtual void SetTrans( const DVECTOR* );
		virtual void AddTrans( const DVECTOR* );
		virtual void SetTrans( const DVECTOR& );
		virtual void AddTrans( const DVECTOR& );
		virtual void GetTrans( DVECTOR* ) const;
		virtual void GetTrans( DVECTOR& ) const;

	public:
		virtual void SetTransX( float = 0.0f );
		virtual void SetTransY( float = 0.0f );
		virtual void SetTransZ( float = 0.0f );
		virtual void AddTransX( float  );
		virtual void AddTransY( float  );
		virtual void AddTransZ( float  );
		virtual void GetTransX( float* ) const;
		virtual void GetTransY( float* ) const;
		virtual void GetTransZ( float* ) const;
		virtual auto GetTransX( void   ) const->float;
		virtual auto GetTransY( void   ) const->float;
		virtual auto GetTransZ( void   ) const->float;
	};

	class SOBJ_T :
		public SOBJ_T_D,
		public SOBJ_T_I
	{
	protected:
		UINT	df;

	public:
		virtual const VECTOR3* Trans(  void ) const override;
		virtual void Trans( const VECTOR3* ) override;
		virtual void Trans( const VECTOR3& ) override;

	protected:
		virtual void SetTrans_( float,  float,  float  ) override;
		virtual void AddTrans_( float,  float,  float  ) override;
		virtual void GetTrans_( float*, float*, float* ) const override;
	};
}

//----------------------------------------
// ROTATE
//----------------------------------------
namespace opal {
	class SOBJ;

	struct SOBJ_R_D {
		explicit SOBJ_R_D();

		VECTOR3	rv;
		MATRIX	rm;
	};

	class SOBJ_R_I {
	public:
		virtual const VECTOR3* Rotate( void ) const = 0;
		virtual void Rotate( const VECTOR3* ) = 0;
		virtual void Rotate( const VECTOR3& ) = 0;

	protected:
		virtual void SetRotate_( float,  float,  float  ) = 0;
		virtual void AddRotate_( float,  float,  float  ) = 0;
		virtual void GetRotate_( float*, float*, float* ) const = 0;

	public:
		virtual void SetRotate( float,  float,  float  );
		virtual void AddRotate( float,  float,  float  );
		virtual void GetRotate( float*, float*, float* ) const;

		virtual void SetRotate( float  );
		virtual void AddRotate( float  );
		virtual void GetRotate( float* ) const;

		virtual void SetRotate( void );
		virtual auto GetRotate( void ) const->float;

	public:
		virtual void SetRotate( const std::tuple<float,float,float>& );
		virtual void AddRotate( const std::tuple<float,float,float>& );
		virtual void SetRotate( const std::pair<float,float>& );
		virtual void AddRotate( const std::pair<float,float>& );

	public:
		virtual void SetRotate( const SOBJ* );
		virtual void SetRotate( const SOBJ& );
		virtual void GetRotate( SOBJ* ) const;
		virtual void GetRotate( SOBJ& ) const;

	public:
		virtual void SetRotate( const VECTOR3* );
		virtual void AddRotate( const VECTOR3* );
		virtual void SetRotate( const VECTOR3& );
		virtual void AddRotate( const VECTOR3& );
		virtual void GetRotate( VECTOR3* ) const;
		virtual void GetRotate( VECTOR3& ) const;

		virtual void SetRotate( const VECTOR2* );
		virtual void AddRotate( const VECTOR2* );
		virtual void SetRotate( const VECTOR2& );
		virtual void AddRotate( const VECTOR2& );
		virtual void GetRotate( VECTOR2* ) const;
		virtual void GetRotate( VECTOR2& ) const;

		virtual void SetRotate( const DVECTOR* );
		virtual void AddRotate( const DVECTOR* );
		virtual void SetRotate( const DVECTOR& );
		virtual void AddRotate( const DVECTOR& );
		virtual void GetRotate( DVECTOR* ) const;
		virtual void GetRotate( DVECTOR& ) const;

	public:
		virtual void SetRotateX( float = 0.0f );
		virtual void SetRotateY( float = 0.0f );
		virtual void SetRotateZ( float = 0.0f );
		virtual void AddRotateX( float  );
		virtual void AddRotateY( float  );
		virtual void AddRotateZ( float  );
		virtual void GetRotateX( float* ) const;
		virtual void GetRotateY( float* ) const;
		virtual void GetRotateZ( float* ) const;
		virtual auto GetRotateX( void   ) const->float;
		virtual auto GetRotateY( void   ) const->float;
		virtual auto GetRotateZ( void   ) const->float;
	};

	class SOBJ_R :
		public SOBJ_R_D,
		public SOBJ_R_I
	{
	protected:
		UINT	df;

	public:
		virtual const VECTOR3* Rotate( void ) const override;
		virtual void Rotate( const VECTOR3* ) override;
		virtual void Rotate( const VECTOR3& ) override;

	protected:
		virtual void SetRotate_( float,  float,  float  ) override;
		virtual void AddRotate_( float,  float,  float  ) override;
		virtual void GetRotate_( float*, float*, float* ) const override;
	};
}

//----------------------------------------
// SCALE
//----------------------------------------
namespace opal {
	class SOBJ;

	struct SOBJ_S_D {
		explicit SOBJ_S_D();

		VECTOR3	sv;
		MATRIX	sm;
	};

	class SOBJ_S_I {
	public:
		virtual const VECTOR3* Scale(  void ) const = 0;
		virtual void Scale( const VECTOR3* ) = 0;
		virtual void Scale( const VECTOR3& ) = 0;

	protected:
		virtual void SetScale_( float,  float,  float  ) = 0;
		virtual void AddScale_( float,  float,  float  ) = 0;
		virtual void GetScale_( float*, float*, float* ) const = 0;

	public:
		virtual void SetScale( float,  float,  float  );
		virtual void AddScale( float,  float,  float  );
		virtual void GetScale( float*, float*, float* ) const;

		virtual void SetScale( float,  float  );
		virtual void AddScale( float,  float  );
		virtual void GetScale( float*, float* ) const;

		virtual void SetScale( float  );
		virtual void AddScale( float  );
		virtual void GetScale( float* ) const;

		virtual void SetScale( void );
		virtual auto GetScale( void ) const->float;

	public:
		virtual void SetScale( const std::tuple<float,float,float>& );
		virtual void AddScale( const std::tuple<float,float,float>& );
		virtual void SetScale( const std::pair<float,float>& );
		virtual void AddScale( const std::pair<float,float>& );

	public:
		virtual void SetScale( const SOBJ* );
		virtual void SetScale( const SOBJ& );
		virtual void GetScale( SOBJ* ) const;
		virtual void GetScale( SOBJ& ) const;

	public:
		virtual void SetScale( const VECTOR3* );
		virtual void AddScale( const VECTOR3* );
		virtual void SetScale( const VECTOR3& );
		virtual void AddScale( const VECTOR3& );
		virtual void GetScale( VECTOR3* ) const;
		virtual void GetScale( VECTOR3& ) const;

		virtual void SetScale( const VECTOR2* );
		virtual void AddScale( const VECTOR2* );
		virtual void SetScale( const VECTOR2& );
		virtual void AddScale( const VECTOR2& );
		virtual void GetScale( VECTOR2* ) const;
		virtual void GetScale( VECTOR2& ) const;

		virtual void SetScale( const DVECTOR* );
		virtual void AddScale( const DVECTOR* );
		virtual void SetScale( const DVECTOR& );
		virtual void AddScale( const DVECTOR& );
		virtual void GetScale( DVECTOR* ) const;
		virtual void GetScale( DVECTOR& ) const;

	public:
		virtual void SetScaleX( float = 1.0f );
		virtual void SetScaleY( float = 1.0f );
		virtual void SetScaleZ( float = 1.0f );
		virtual void AddScaleX( float  );
		virtual void AddScaleY( float  );
		virtual void AddScaleZ( float  );
		virtual void GetScaleX( float* ) const;
		virtual void GetScaleY( float* ) const;
		virtual void GetScaleZ( float* ) const;
		virtual auto GetScaleX( void   ) const->float;
		virtual auto GetScaleY( void   ) const->float;
		virtual auto GetScaleZ( void   ) const->float;
	};

	class SOBJ_S :
		public SOBJ_S_D,
		public SOBJ_S_I
	{
	protected:
		UINT	df;

	public:
		virtual const VECTOR3* Scale(  void ) const override;
		virtual void Scale( const VECTOR3* ) override;
		virtual void Scale( const VECTOR3& ) override;

	protected:
		virtual void SetScale_( float,  float,  float  ) override;
		virtual void AddScale_( float,  float,  float  ) override;
		virtual void GetScale_( float*, float*, float* ) const override;
	};
}

//----------------------------------------
// スカラーオブジェクト
//----------------------------------------
namespace opal {
	class SOBJ :
		public OOBJ,
		public SOBJ_S,
		public SOBJ_R,
		public SOBJ_T
	{
	protected:
		explicit SOBJ();

	public:
		virtual ~SOBJ();

	protected:
		UINT	df;		// ダーティーフラグ
		MATRIX	sr;		// sm * rm キャッシュ

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	protected:
		virtual void ObjInfo( void ) const override;

	protected:
		virtual void Coordinate(   const SUPERT* ) override;
		virtual void ReCoordinate( const SUPERT* ) override;

	private:
		virtual void ReCoordinate( void );

	protected:
		virtual void Dirty( UINT = 0 ) final;
		virtual void CalcurateN( void );
		virtual void CalcurateP( const MATRIX* vinv );
		virtual void CalcurateB( const MATRIX* bill );

	protected:
		virtual UINT SRT(    void ) const;
		virtual bool SRT(    UINT ) const;
		virtual bool SRT_S(  void ) const;
		virtual bool SRT_R(  void ) const;
		virtual bool SRT_T(  void ) const;
		virtual bool SRT_SX( void ) const;
		virtual bool SRT_SY( void ) const;
		virtual bool SRT_SZ( void ) const;
		virtual bool SRT_RX( void ) const;
		virtual bool SRT_RY( void ) const;
		virtual bool SRT_RZ( void ) const;
		virtual bool SRT_TX( void ) const;
		virtual bool SRT_TY( void ) const;
		virtual bool SRT_TZ( void ) const;
	};
}

// End Of File
