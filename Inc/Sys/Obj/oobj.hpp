#pragma once
//========================================
// OPAL Game Library
//========================================
#include "super.hpp"
#include "superx.hpp"

//========================================
// オリジナルオブジェクト
//========================================
namespace opal {
	class OOBJ : public SUPERT {
	protected:
		explicit OOBJ();

	public:
		virtual ~OOBJ();

	protected:
		UINT	flag;		// コーディネイトフラグ

	protected:
		const MATRIX*	attach;		// 連結先行列
		const OOBJ*		parent;		// 連結先OBJ
		const OOBJ*		adopter;	// 連結先OBJ

	private:
		struct POSE {
			POSE();
			void operator ()( void );

			MATRIX		matrix;		// マトリックス
			VECTOR3		scale;		// 座標
			VECTOR3		rotate;		// 座標
			VECTOR3		trans;		// 座標
			VECTOR3		axisx;		// Ｘ軸
			VECTOR3		axisy;		// Ｙ軸
			VECTOR3		axisz;		// Ｚ軸
		};

	protected:
		POSE	local;
		POSE	world;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	protected:
		virtual void ObjInfo( void ) const override;

	protected:
		virtual void Coordinate(   const SUPERT* ) override;
		virtual void ReCoordinate( const SUPERT* ) override;

	private:
		virtual void ReCoordinate( void );

	public:
		static UINT Coordinate( const LINX*, UINT = 0 );

	public:
		virtual void SetFlag( void );
		virtual void SetFlag( UINT );
		virtual void AddFlag( UINT );
		virtual void SubFlag( UINT );
		virtual UINT GetFlag( void ) const;
		virtual UINT GetFlag( UINT ) const;
		virtual bool AndFlag( UINT ) const;
		virtual bool OrFlag(  UINT ) const;
		virtual bool EquFlag( UINT ) const;

	public:
		virtual const MATRIX* Attach( void ) const;
		virtual const MATRIX* Attach( const MATRIX* );

	public:
		virtual const OOBJ* Parent(  const OOBJ*, bool = true );
		virtual const OOBJ* Adopter( const OOBJ*, bool = true );
		virtual const OOBJ* Parent(  std::shared_ptr<const OOBJ>, bool = true );
		virtual const OOBJ* Adopter( std::shared_ptr<const OOBJ>, bool = true );
		virtual const OOBJ* Parent(  void ) const;
		virtual const OOBJ* Adopter( void ) const;

	public:
		virtual MATRIX World( void ) const;
		virtual MATRIX Local( void ) const;

		virtual const MATRIX*  MatrixWorld( void ) const;
		virtual const MATRIX*  MatrixLocal( void ) const;
		virtual const VECTOR3* VectorWorld( void ) const;
		virtual const VECTOR3* VectorLocal( void ) const;

		virtual void    WorldMatrix( const MATRIX& );
		virtual void    LocalMatrix( const MATRIX& );
		virtual MATRIX  WorldMatrix( void ) const;
		virtual MATRIX  LocalMatrix( void ) const;
		virtual void    WorldVector( const VECTOR3& );
		virtual void    LocalVector( const VECTOR3& );
		virtual void    WorldVector( const VECTOR2& );
		virtual void    LocalVector( const VECTOR2& );
		virtual VECTOR3 WorldVector( void ) const;
		virtual VECTOR3 LocalVector( void ) const;

		virtual operator       MATRIX(   void ) const;
		virtual operator const MATRIX*(  void ) const;
		virtual operator       VECTOR3(  void ) const;
		virtual operator       VECTOR2(  void ) const;
		virtual operator const VECTOR3*( void ) const;
		virtual operator const VECTOR2*( void ) const;

	public:
		virtual VECTOR3 WorldAxisX(  void ) const;
		virtual VECTOR3 WorldAxisY(  void ) const;
		virtual VECTOR3 WorldAxisZ(  void ) const;
		virtual VECTOR3 LocalAxisX(  void ) const;
		virtual VECTOR3 LocalAxisY(  void ) const;
		virtual VECTOR3 LocalAxisZ(  void ) const;

	public:
		virtual VECTOR3 WorldScale(  void ) const;
		virtual VECTOR3 LocalScale(  void ) const;
		virtual VECTOR3 WorldRotate( void ) const;
		virtual VECTOR3 LocalRotate( void ) const;
		virtual VECTOR3 WorldTrans(  void ) const;
		virtual VECTOR3 LocalTrans(  void ) const;

		virtual float WorldScaleX(  void ) const;
		virtual float WorldScaleY(  void ) const;
		virtual float WorldScaleZ(  void ) const;
		virtual float LocalScaleX(  void ) const;
		virtual float LocalScaleY(  void ) const;
		virtual float LocalScaleZ(  void ) const;
		virtual float WorldRotateX( void ) const;
		virtual float WorldRotateY( void ) const;
		virtual float WorldRotateZ( void ) const;
		virtual float LocalRotateX( void ) const;
		virtual float LocalRotateY( void ) const;
		virtual float LocalRotateZ( void ) const;
		virtual float WorldTransX(  void ) const;
		virtual float WorldTransY(  void ) const;
		virtual float WorldTransZ(  void ) const;
		virtual float LocalTransX(  void ) const;
		virtual float LocalTransY(  void ) const;
		virtual float LocalTransZ(  void ) const;

	public:
		virtual OOBJ* Search( OOBJ* ) const;
	};
}

// End Of File
