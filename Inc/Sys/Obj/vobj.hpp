#pragma once
//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"
#include "Obj/sobj.hpp"

//========================================
// ベクターオブジェクト
//========================================
namespace opal {
	class VOBJ : public SOBJ {
	protected:
		explicit VOBJ();

	public:
		virtual ~VOBJ();

	protected:
		VECTOR3		ud;		// 上方
		VECTOR3		fd;		// 前方
		VECTOR3		rd;		// 右方

	protected:
		MATRIX		vw;		// 視線行列

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	protected:
		virtual void ObjInfo( void ) const override;

	protected:
		virtual void Coordinate( const SUPERT* ) override;

	public:
		virtual const VECTOR3& Ud( void ) const;
		virtual const VECTOR3& Fd( void ) const;
		virtual const VECTOR3& Rd( void ) const;
		virtual const VECTOR3& Ad( UINT ) const;

	public:
		virtual const MATRIX* Look( void ) const;
	};
}

// End Of File
