#pragma once
//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"
#include "Obj/vobj.hpp"

//========================================
// ���_�����W�n
//========================================
namespace opal {
	class XOBJ : public VOBJ {
	protected:
		explicit XOBJ();

	public:
		virtual ~XOBJ();

	protected:
		const XOBJ*	origin;
		bool		dim3;
		bool		axis[2];
		VECTOR3		orig[2];

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	protected:
		virtual void ObjInfo( void ) const override;

	protected:
		virtual void Coordinate( const SUPERT* ) override;

	public:
		virtual void SetDim2( bool = true );
		virtual void SetDim3( bool = true );
		virtual bool GetDim2( void ) const;
		virtual bool GetDim3( void ) const;

		virtual void SetCoordinate( bool = false );
		virtual bool GetCoordinate( void ) const;

	public:
		virtual void SetOrigin_( float,  float,  float  = 0.f     );
		virtual void GetOrigin_( float*, float*, float* = nullptr ) const;

	public:
		virtual void SetOrigin( float,   float,  float  = 0.f );
		virtual void SetOrigin( const VECTOR3& );
		virtual void SetOrigin( const VECTOR2& );
		virtual void SetOrigin( const VECTOR3* );
		virtual void SetOrigin( const VECTOR2* );
		virtual void SetOrigin( void );

		virtual void GetOrigin( float*,  float*, float* = nullptr ) const;
		virtual void GetOrigin( VECTOR3& ) const;
		virtual void GetOrigin( VECTOR2& ) const;
		virtual void GetOrigin( VECTOR3* ) const;
		virtual void GetOrigin( VECTOR2* ) const;
		virtual auto GetOrigin( void ) const->VECTOR3;

	public:
		virtual const XOBJ* Origin( const XOBJ* );
		virtual const XOBJ* Origin( std::shared_ptr<const XOBJ> );
		virtual const XOBJ* Origin( void ) const;

	public:
		using SOBJ::SetRotate;
		using SOBJ::AddRotate;
		using SOBJ::GetRotate;
		void SetRotate( float  ) override;
		void AddRotate( float  ) override;
		void GetRotate( float* ) const override;
		auto GetRotate( void ) const->float override;

	public:
		virtual auto World( void ) const->MATRIX override;
	};
}

// End Of File
