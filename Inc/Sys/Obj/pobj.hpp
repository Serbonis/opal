#pragma once
//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"
#include "Obj/oobj.hpp"

//========================================
// ピボット＆オフセット
//========================================

//----------------------------------------
// PIVOT
//----------------------------------------
namespace opal {
	class POBJ;

	struct POBJ_P_D {
		explicit POBJ_P_D();

		VECTOR3	pv;
		MATRIX	pm;
	};

	class POBJ_P_I {
	public:
		virtual void Pivot( const VECTOR3* ) = 0;
		virtual const VECTOR3* Pivot( void ) const = 0;

	protected:
		virtual void SetPivot_( float,  float,  float  ) = 0;
		virtual void AddPivot_( float,  float,  float  ) = 0;
		virtual void GetPivot_( float*, float*, float* ) const = 0;

	public:
		virtual void SetPivot( float,  float,  float  );
		virtual void AddPivot( float,  float,  float  );
		virtual void GetPivot( float*, float*, float* ) const;

		virtual void SetPivot( float,  float  );
		virtual void AddPivot( float,  float  );
		virtual void GetPivot( float*, float* ) const;

		virtual void SetPivot( float );
		virtual void AddPivot( float );

		virtual void SetPivot( void );
		virtual auto GetPivot( void ) const->VECTOR3;

	public:
		virtual void SetPivot( const POBJ* );
		virtual void SetPivot( const POBJ& );
		virtual void GetPivot( POBJ* ) const;
		virtual void GetPivot( POBJ& ) const;

	public:
		virtual void SetPivot( const VECTOR3* );
		virtual void AddPivot( const VECTOR3* );
		virtual void SetPivot( const VECTOR2* );
		virtual void AddPivot( const VECTOR2* );
		virtual void SetPivot( const DVECTOR* );
		virtual void AddPivot( const DVECTOR* );
		virtual void GetPivot( VECTOR3* ) const;
		virtual void GetPivot( VECTOR2* ) const;
		virtual void GetPivot( DVECTOR* ) const;

		virtual void SetPivot( const VECTOR3& );
		virtual void AddPivot( const VECTOR3& );
		virtual void SetPivot( const VECTOR2& );
		virtual void AddPivot( const VECTOR2& );
		virtual void SetPivot( const DVECTOR& );
		virtual void AddPivot( const DVECTOR& );
		virtual void GetPivot( VECTOR3& ) const;
		virtual void GetPivot( VECTOR2& ) const;
		virtual void GetPivot( DVECTOR& ) const;

	public:
		virtual void SetPivotX( float  );
		virtual void SetPivotY( float  );
		virtual void SetPivotZ( float  );
		virtual void AddPivotX( float  );
		virtual void AddPivotY( float  );
		virtual void AddPivotZ( float  );
		virtual void GetPivotX( float* ) const;
		virtual void GetPivotY( float* ) const;
		virtual void GetPivotZ( float* ) const;
		virtual auto GetPivotX( void   ) const->float;
		virtual auto GetPivotY( void   ) const->float;
		virtual auto GetPivotZ( void   ) const->float;
	};

	class POBJ_P :
		public POBJ_P_D,
		public POBJ_P_I
	{
	public:
		virtual void Pivot( const VECTOR3* ) override;
		virtual const VECTOR3* Pivot( void ) const override;

	protected:
		virtual void SetPivot_( float,  float,  float  ) override;
		virtual void AddPivot_( float,  float,  float  ) override;
		virtual void GetPivot_( float*, float*, float* ) const override;
	};
}

//----------------------------------------
// OFFSET
//----------------------------------------
namespace opal {
	class POBJ;

	struct POBJ_O_D {
		explicit POBJ_O_D();

		VECTOR3	ov;
		MATRIX	om;
	};

	class POBJ_O_I {
	public:
		virtual void Offset( const VECTOR3* ) = 0;
		virtual const VECTOR3* Offset( void ) const = 0;

	protected:
		virtual void SetOffset_( float,  float,  float  ) = 0;
		virtual void AddOffset_( float,  float,  float  ) = 0;
		virtual void GetOffset_( float*, float*, float* ) const = 0;

	public:
		virtual void SetOffset( float,  float,  float  );
		virtual void AddOffset( float,  float,  float  );
		virtual void GetOffset( float*, float*, float* ) const;

		virtual void SetOffset( float,  float  );
		virtual void AddOffset( float,  float  );
		virtual void GetOffset( float*, float* ) const;

		virtual void SetOffset( float );
		virtual void AddOffset( float );

		virtual void SetOffset( void );
		virtual auto GetOffset( void ) const->VECTOR3;

	public:
		virtual void SetOffset( const POBJ* );
		virtual void SetOffset( const POBJ& );
		virtual void GetOffset( POBJ* ) const;
		virtual void GetOffset( POBJ& ) const;

	public:
		virtual void SetOffset( const VECTOR3* );
		virtual void AddOffset( const VECTOR3* );
		virtual void SetOffset( const VECTOR2* );
		virtual void AddOffset( const VECTOR2* );
		virtual void SetOffset( const DVECTOR* );
		virtual void AddOffset( const DVECTOR* );
		virtual void GetOffset( VECTOR3* ) const;
		virtual void GetOffset( VECTOR2* ) const;
		virtual void GetOffset( DVECTOR* ) const;

		virtual void SetOffset( const VECTOR3& );
		virtual void AddOffset( const VECTOR3& );
		virtual void SetOffset( const VECTOR2& );
		virtual void AddOffset( const VECTOR2& );
		virtual void SetOffset( const DVECTOR& );
		virtual void AddOffset( const DVECTOR& );
		virtual void GetOffset( VECTOR3& ) const;
		virtual void GetOffset( VECTOR2& ) const;
		virtual void GetOffset( DVECTOR& ) const;

	public:
		virtual void SetOffsetX( float  );
		virtual void SetOffsetY( float  );
		virtual void SetOffsetZ( float  );
		virtual void AddOffsetX( float  );
		virtual void AddOffsetY( float  );
		virtual void AddOffsetZ( float  );
		virtual void GetOffsetX( float* ) const;
		virtual void GetOffsetY( float* ) const;
		virtual void GetOffsetZ( float* ) const;
		virtual auto GetOffsetX( void   ) const->float;
		virtual auto GetOffsetY( void   ) const->float;
		virtual auto GetOffsetZ( void   ) const->float;
	};

	class POBJ_O :
		public POBJ_O_D,
		public POBJ_O_I
	{
	public:
		virtual void Offset( const VECTOR3* ) override;
		virtual const VECTOR3* Offset( void ) const override;

	protected:
		virtual void SetOffset_( float,  float,  float  ) override;
		virtual void AddOffset_( float,  float,  float  ) override;
		virtual void GetOffset_( float*, float*, float* ) const override;
	};
}

//----------------------------------------
// ピボット＆オフセット
//----------------------------------------
namespace opal {
	class POBJ :
		public POBJ_P,
		public POBJ_O
	{
	public:
		explicit POBJ();
		virtual ~POBJ();

	protected:
		virtual void Coordinate( const SUPERT* );
	};
}

// End Of File
