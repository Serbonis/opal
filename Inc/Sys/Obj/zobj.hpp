#pragma once
//========================================
// OPAL Game Library
//========================================
#include "obj.hpp"
#include "Obj/xobj.hpp"

//========================================
// 最終ゲームオブジェクト
//========================================
namespace opal {
	class ZOBJ : public XOBJ {
	protected:
		explicit ZOBJ();

	public:
		virtual ~ZOBJ();

	protected:
		using TRACK = std::deque<VECTOR3>;
		UINT	size;
		TRACK	track;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	protected:
		virtual void ObjInfo( void ) const override;

	protected:
		virtual void Coordinate( const SUPERT* ) override;

	public:
		using FUNC = SUPERX::FUNC;

		virtual const ZOBJ* Parent(  const ZOBJ*, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Adopter( const ZOBJ*, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Parent(  const ZOBJ*, bool );
		virtual const ZOBJ* Adopter( const ZOBJ*, bool );
		virtual const ZOBJ* Parent(  std::shared_ptr<const ZOBJ>, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Adopter( std::shared_ptr<const ZOBJ>, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Parent(  std::shared_ptr<const ZOBJ>, bool );
		virtual const ZOBJ* Adopter( std::shared_ptr<const ZOBJ>, bool );
		virtual const ZOBJ* Parent(  void ) const;
		virtual const ZOBJ* Adopter( void ) const;

		virtual const ZOBJ* Child(   ZOBJ*, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Adopted( ZOBJ*, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Child(   ZOBJ*, bool );
		virtual const ZOBJ* Adopted( ZOBJ*, bool );
		virtual const ZOBJ* Child(   std::shared_ptr<ZOBJ>, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Adopted( std::shared_ptr<ZOBJ>, FUNC = nullptr, bool = true );
		virtual const ZOBJ* Child(   std::shared_ptr<ZOBJ>, bool );
		virtual const ZOBJ* Adopted( std::shared_ptr<ZOBJ>, bool );

	public:
		virtual void SetTrack( UINT );
		virtual UINT GetTrack( void ) const;
		virtual VECTOR3 GetTrack( UINT ) const;
	};
}

// End Of File
