#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xdraw.hpp"

//========================================
// グラフィックシステム
//========================================
namespace opal {
	class CAMERAS;
	class LIGHTS;
	class FOGS;

	class CAMERAX;
	class LIGHTX;
	class FOGX;

	class ZOBJ;
	class DRAWT;

	class DRAWX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

		friend class CAMERAX;
		friend class LIGHTX;
		friend class FOGX;

		friend class DRAWT;

	public:
		enum { PORT_MAX = 4 };
		enum {
			EXEC_DRAW,
			EXEC_ANIME,
			EXEC_EFFECT,
			EXEC_MAX,
		};

	private:
		static LINX*	index;

	private:
		static struct SYSDEF final {
			explicit SYSDEF();

			LINX*	linx;
			ZOBJ*	parent;
			bool	disp;
			bool	anime;
			bool	alpha;
			UINT	prio;
		}	sysdef;

	private:
		static bool SysInit(  void );
		static bool SysFree(  void );

		static bool SysTaskInit(  void );
		static bool SysTaskEnter( void );
		static bool SysTaskMain(  void );
		static bool SysTaskExit(  void );
		static bool SysTaskFree(  void );

	private:
		static void Present( void );

	public:
		static void Capture( const char* = nullptr );

	public:
		static UINT TextureStage( void );

	public:
		static void Proj(  const MATRIX* );
		static void View(  const MATRIX* );
		static void World( const MATRIX* );
		static void FVF( DWORD );
		static void Culling( DWORD );
		static void Material( const MATERIAL* );
		static void Texture( DWORD, const TEXTURES* );

	public:
		static void DeviceLost( const DEVICE_NAME, XDEVICE* );
		static void DeviceLost( const DEVICE_NAME );
		static void DeviceLost( const XDEVICE*, XDEVICE* );
		static void DeviceLost( const XDEVICE* );
		static UINT DeviceLost( void );

	public:
		static const MATRIX* Viewport( void );
		static const MATRIX* VportInv( void );

		static void SetViewport( const SRECTF& );
		static void SetViewport( float, float, float, float );
		static void SetViewport( const VECTOR2&, float, float );
		static void SetViewport( float, float, const SSIZE& );
		static void SetViewport( void );
		static auto GetViewport( void )->SRECTF;

		static void SetViewportPosition( float, float );
		static void SetViewportPosition( const VECTOR2& );
		static void SetViewportPosition( void );
		static auto GetViewportPosition( void )->VECTOR2;

		static void SetViewportSize( float, float );
		static void SetViewportSize( const SSIZE& );
		static void SetViewportSize( void );
		static auto GetViewportSize( void )->SSIZE;

		static void ViewportBegin( void );
		static void ViewportBegin( const SRECTF& );
		static void ViewportBegin( float, float, float, float );
		static void ViewportBegin( const VECTOR2&, float, float );
		static void ViewportBegin( float, float, const SSIZE& );
		static void ViewportBegin( const VECTOR2&, const SSIZE& );
		static void ViewportEnd( void );

	public:
		static void SetScissor( const SRECTF& );
		static void SetScissor( float, float, float, float );
		static void SetScissor( const VECTOR2&, float, float );
		static void SetScissor( float, float, const SSIZE& );
		static void SetScissor( const VECTOR2&, const SSIZE& );
		static void SetScissor( void );
		static auto GetScissor( void )->SRECTF;

	public:
		static COLOR Color( void );
		static void  Color( COLOR );
		static void  Color( UCHAR, UCHAR, UCHAR );

	public:
		static void SaveTexture( DWORD );
		static void LoadTexture( DWORD );

	public:
		static void  Linx( LINX* );
		static void  Linx( LINX& );
		static void  Linx( std::shared_ptr<LINX> );
		static LINX* Linx( void );

		static void  Parent( ZOBJ* );
		static void  Parent( ZOBJ& );
		static void  Parent( std::shared_ptr<ZOBJ> );
		static ZOBJ* Parent( void );

		static void  Disp(  int   );
		static bool  Disp(  void  );
		static void  Anime( int   );
		static bool  Anime( void  );
		static void  Alpha( int   );
		static bool  Alpha( void  );
		static void  Prio(  int   );
		static UINT  Prio(  void  );

	public:
		static VECTOR3* Screen2World( VECTOR3*, const VECTOR3*, const MATRIX*, const MATRIX*, const MATRIX* = nullptr );
		static MATRIX*  Screen2World( MATRIX*,  const MATRIX*,  const MATRIX*, const MATRIX*, const MATRIX* = nullptr );

		//----------------------------------------
		// system camera
		//----------------------------------------
	private:
		static CAMERAS*	camera;

		static void InitCamera( void );
		static void ExecCamera( void );
		static void FreeCamera( void );

	public:
		static void SysCamera( bool );
		static bool SysCamera( void );

		static const CAMERAX* Camera( const CAMERAX* );
		static const CAMERAX* Camera( void );

		static void  CameraAngle( float );
		static float CameraAngle( void  );

		static void    CameraPos( float, float, float );
		static VECTOR3 CameraPos( void );
		static void  CameraPosX( float );
		static float CameraPosX( void  );
		static void  CameraPosY( float );
		static float CameraPosY( void  );
		static void  CameraPosZ( float );
		static float CameraPosZ( void  );

		static void  CameraClip(  float, float );
		static void  CameraClipN( float );
		static void  CameraClipF( float );

		static CLIPF CameraClip(  void );
		static float CameraClipN( void );
		static float CameraClipF( void );

		static const MATRIX* View( void );
		static const MATRIX* Pers( void );
		static const MATRIX* Orth( void );
		static const MATRIX* Vinv( void );
		static const MATRIX* Bill( void );
		static const MATRIX* Proj( bool = true );
		static const MATRIX* ViewProj( bool = true );

		static float ZValue( const VECTOR3*, const MATRIX* );
		static float ZValue( const VECTOR3* );

		static void DrawCamera( void );
		static void DrawCamera( const VECTOR3&,
								const VECTOR3&,
								const VECTOR3&,
								float, float, float, const CLIPF&,
								MATRIX*,
								MATRIX*,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr,
								MATRIX* = nullptr );

		static MATRIX* CameraView( MATRIX&, const VECTOR3&, const VECTOR3&, const VECTOR3& );
		static MATRIX* CameraVinv( MATRIX&, const MATRIX& );
		static MATRIX* CameraPers( MATRIX&, float, float, float, float, float );
		static MATRIX* CameraOrth( MATRIX&, float, float, float, float );
		static MATRIX* CameraBill( MATRIX&, const VECTOR3& );

		//----------------------------------------
		// system light
		//----------------------------------------
	private:
		static LIGHTS*	light;

		static void InitLight( void );
		static void ExecLight( void );
		static void FreeLight( void );

	public:
		static void SysLight(  bool );
		static bool SysLight(  void );
		static UINT SysLights( void );

		static const LIGHTX* Light( const LIGHTX*, UINT = 0 );
		static const LIGHTX* Light(                UINT );
		static void Lighting( UINT, bool );
		static bool Lighting( UINT );

		static COLOR Ambient( COLOR );
		static COLOR Ambient( UCHAR, UCHAR, UCHAR, UCHAR = 0xff );
		static COLOR Ambient( void );

		static void LightPosition(  UINT, const VECTOR3* );
		static void LightDirection( UINT, const VECTOR3* );
		static void LightPosition(        const VECTOR3* );
		static void LightDirection(       const VECTOR3* );
		static void LightPosition(  UINT, float, float, float );
		static void LightDirection( UINT, float, float, float );
		static void LightPosition(        float, float, float );
		static void LightDirection(       float, float, float );
		static const VECTOR3* LightPosition(  UINT );
		static const VECTOR3* LightDirection( UINT );

		static VECTOR3 LightPosition(  void );
		static VECTOR3 LightDirection( void );
		static COLOR   LightDiffuse(   void );

		static void DrawLight( void );
		static void DrawLight( const VECTOR3&, const VECTOR3&, DLIGHT& );

		//----------------------------------------
		// debug
		//----------------------------------------
#ifdef OPAL_DEBUG
	private:
		static int	debug;

	public:
		static void Debug(  int  );
		static int  Debug(  void );
		static void Device( void );
		static void Spec(   void );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "drawx.hpp"
#include "draws.hpp"
#include "drawf.hpp"

// End Of File
