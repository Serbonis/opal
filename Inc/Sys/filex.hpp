#pragma once
//========================================
// OPAL Game Library
//========================================
#include "memory.hpp"
#include "file.hpp"

//========================================
// ファイルタスク
//========================================
namespace opal {
	class FILEI;

	class FILET : public TASK, public MESSAGE_WORK<FILEI> {
		friend class FILEX;
		friend class FILEI;

	protected:
		enum {
			STEP_NULL,
			STEP_INIT,
			STEP_OPEN,
			STEP_ADRS,
			STEP_LOCK,
			STEP_EXEC,
			STEP_WAIT,
			STEP_FUNC,
			STEP_EXIT,
		};

		enum {
			RETRY_MAX = 10,
		};

	public:
		using MESSAGE_WORK::MESSAGE_WORK;

		explicit FILET();
		virtual ~FILET();

	protected:
		HANDLE		hand;	// ファイルハンドル
		OVERLAPPED	ovlp;	// 非同期読み書き用
		MEMT*		lock;	// 多重読み書き防止ロック
		void*		buff;	// 読み書き領域確保フラグ
		FILEF		func;	// 終了時コールバック関数

	protected:
		virtual void kill( void );

	private:
		virtual void TaskMain( void ) override;
	};
}

// End Of File
