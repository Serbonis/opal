#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"

//========================================
// リソースインスタンス
//========================================
namespace opal {
	class FILED;
	class RSRC {
	public:
		explicit RSRC();
		virtual ~RSRC();

	protected:
		FILEINFO	file;	// ファイル情報

	public:
		virtual void Init( const void*, size_t, const char* = nullptr, const void*  = nullptr ) = 0;
		virtual void Free( void ) = 0;

	public:
		virtual void Init( const std::string_view, const std::string_view, const void* = nullptr );
		virtual void Init( const std::string_view,                         const void* = nullptr );
		virtual void Init( const FILED*,                                   const void* = nullptr );
		virtual void Init( void );

	public:
		virtual const char* File( void ) const;
		virtual const void* Adrs( void ) const;
		virtual size_t      Size( void ) const;
	};
}

// End Of File
