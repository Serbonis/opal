#pragma once
//========================================
// shader.hpp
//========================================
#include "Ext/hlsl.hpp"

//----------------------------------------
// DirectX11
//----------------------------------------
#include "draw_dx11.hpp"

using namespace opal::DRAWX_DX11;

using VS_ELEMENT_DESC = D3D11_INPUT_ELEMENT_DESC;
using VS_ELEMENT_FORM = DXGI_FORMAT;
constexpr auto VS_ELEMENT_ALIG = D3D11_APPEND_ALIGNED_ELEMENT;
constexpr auto VS_ELEMENT_DATA = D3D11_INPUT_PER_VERTEX_DATA;

//----------------------------------------
// シェーダーデータ
//----------------------------------------
#define VS_HLSL_DATA(s)	vs_##s
#define PS_HLSL_DATA(s)	ps_##s
#define VS_HLSL_SIZE(s)	vs_##s##_size
#define PS_HLSL_SIZE(s)	ps_##s##_size

//----------------------------------------
// 頂点シェーダーデータ配置
//----------------------------------------
enum {
	VS_ELEMENT_N0,
	VS_ELEMENT_N1,
	VS_ELEMENT_N2,
	VS_ELEMENT_N3,
	VS_ELEMENT_N4,
	VS_ELEMENT_N5,
	VS_ELEMENT_N6,
	VS_ELEMENT_N7,
	VS_ELEMENT_N8,
	VS_ELEMENT_N9,
	VS_ELEMENT_N10,
	VS_ELEMENT_N11,
	VS_ELEMENT_N12,
	VS_ELEMENT_N13,
	VS_ELEMENT_N14,
};

enum {
	VS_ELEMENT_S0,
	VS_ELEMENT_S1,
	VS_ELEMENT_S2,
	VS_ELEMENT_S3,
	VS_ELEMENT_S4,
	VS_ELEMENT_S5,
	VS_ELEMENT_S6,
	VS_ELEMENT_S7,
	VS_ELEMENT_S8,
	VS_ELEMENT_S9,
	VS_ELEMENT_S10,
	VS_ELEMENT_S11,
	VS_ELEMENT_S12,
	VS_ELEMENT_S13,
	VS_ELEMENT_S14,
};

constexpr VS_ELEMENT_DESC vs_element_desc( const char* p, UINT n, VS_ELEMENT_FORM f, UINT s = VS_ELEMENT_S0 ){

	return { p,n,f,s,VS_ELEMENT_ALIG,VS_ELEMENT_DATA,0 };
}

constexpr VS_ELEMENT_DESC vs_element_desc( const char* p, VS_ELEMENT_FORM f, UINT s = VS_ELEMENT_S0 ){

	return { p,VS_ELEMENT_N0,f,s,VS_ELEMENT_ALIG,VS_ELEMENT_DATA,0 };
}

// End Of File
