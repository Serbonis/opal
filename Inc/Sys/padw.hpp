#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力ワーク
//========================================
namespace opal {
	class PADW : public LINX {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit PADW();
		virtual ~PADW();

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		size_t	size;
		size_t	joymax;
		PADT*	state;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		void Init( size_t, UINT = 0 );
		void Free( void );

	public:
		size_t Size( void ) const;
		UINT JoyMax( void ) const;

	public:
		void SetData( const PADD* = nullptr );

		void SetDataKeyboard(     const KEYBOARD* = nullptr );
		void SetDataMouse(        const MOUSE*    = nullptr );
		void SetDataJoypad( UINT, const JOYPAD*   = nullptr );

	public:
		PADD* GetData( UINT = 0 ) const;

		KEYBOARD* GetDataKeyboard(     UINT = 0 ) const;
		MOUSE*    GetDataMouse(        UINT = 0 ) const;
		JOYPAD*   GetDataJoypad( UINT, UINT = 0 ) const;

	private:
		PADT* Data( UINT = 0, UINT = 0 ) const;

		//----------------------------------------
		// 判定関数
		//----------------------------------------
	public:
		UINT KeyPush( UINT, UINT = 0, UINT = 1 ) const;
		UINT KeyTrig( UINT, UINT = 0, UINT = 1 ) const;
		UINT KeyRels( UINT, UINT = 0, UINT = 1 ) const;

	public:
		UINT JoyPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JoyTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JoyRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JoyPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JoyTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JoyRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;

		UINT Joy1PushO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2PushO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy1TrigO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2TrigO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy1RelsO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2RelsO( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy1PushA( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2PushA( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy1TrigA( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2TrigA( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy1RelsA( UINT, UINT = 0, UINT = 1 ) const;
		UINT Joy2RelsA( UINT, UINT = 0, UINT = 1 ) const;

		float JoyStick(  UINT, UINT, UINT = 0 ) const;
		float Joy1Stick(       UINT, UINT = 0 ) const;
		float Joy2Stick(       UINT, UINT = 0 ) const;

	public:
		UINT MousePush( UINT, UINT = 0, UINT = 1 ) const;
		UINT MouseTrig( UINT, UINT = 0, UINT = 1 ) const;
		UINT MouseRels( UINT, UINT = 0, UINT = 1 ) const;

		LONG Mouse(  UINT, UINT = 0 ) const;
		LONG MouseX(       UINT = 0 ) const;
		LONG MouseY(       UINT = 0 ) const;
		LONG Scroll(       UINT = 0 ) const;

		LONG Cursor(  UINT, UINT = 0 ) const;
		LONG CursorX(       UINT = 0 ) const;
		LONG CursorY(       UINT = 0 ) const;

	public:
		UINT JKPushO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JKTrigO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JKRelsO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JKPushA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JKTrigA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT JKRelsA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;

		UINT J1KPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J1KTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J1KRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J1KPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J1KTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J1KRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		UINT J2KRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;

		UINT JKPushO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		UINT JKTrigO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		UINT JKRelsO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		UINT JKPushA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		UINT JKTrigA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		UINT JKRelsA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;

		UINT J1KPushO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KPushO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J1KTrigO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KTrigO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J1KRelsO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KRelsO( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J1KPushA( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KPushA( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J1KTrigA( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KTrigA( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J1KRelsA( const PADA*, UINT = 0, UINT = 1 ) const;
		UINT J2KRelsA( const PADA*, UINT = 0, UINT = 1 ) const;
	};
}

// End Of File
