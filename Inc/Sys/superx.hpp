#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"
#include "super.hpp"

#include "obj.hpp"

//========================================
// スーパータスク
//========================================
namespace opal {
	class SUPERT : public OBJT {
		friend class SUPERX;

	protected:
		explicit SUPERT() = default;

	public:
		virtual ~SUPERT() = default;

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual void Register( const char* );
		virtual void Release(  const char* );
		virtual void Register( LINX* = nullptr ) override;
		virtual void Release(  LINX* = nullptr ) override;

	protected:
		virtual void TaskInit(  void ) override;
		virtual void TaskEnter( void ) override;
		virtual void TaskFunc(  void ) override;
		virtual void TaskBegin( void ) override;
		virtual void TaskMain(  void ) override;
		virtual void TaskEnd(   void ) override;
		virtual void TaskExit(  void ) override;
		virtual void TaskFree(  void ) override;

	public:
		virtual void Exec( void );

	private:
		virtual void Coordinate(   const SUPERT* ) = 0;
		virtual void ReCoordinate( const SUPERT* ) = 0;
	};

	enum OC {
		FLAG_N		= 0L,		// 何もしない

		FLAG_W		= 1 << 0,	// World は計算済み
		FLAG_L		= 1 << 1,	// Local は計算済み
		FLAG_RW		= 1 << 2,	// World 逆計算
		FLAG_RL		= 1 << 3,	// Local 逆計算

		FLAG_CS		= 1 << 4,	// Scale  を初期化しない
		FLAG_CR		= 1 << 5,	// Rotate を初期化しない
		FLAG_CT		= 1 << 6,	// Trans  を初期化しない

		FLAG_SX		= 1 << 16,	// Scale  X を計算する
		FLAG_SY		= 1 << 17,	// Scale  Y を計算する
		FLAG_SZ		= 1 << 18,	// Scale  Z を計算する
		FLAG_SS		= 1 << 19,	// Scale 速度
		FLAG_RX		= 1 << 20,	// Rotate X を計算する
		FLAG_RY		= 1 << 21,	// Rotate Y を計算する
		FLAG_RZ		= 1 << 22,	// Rotate Z を計算する
		FLAG_RS		= 1 << 23,	// Rotate 速度
		FLAG_TX		= 1 << 24,	// Trans  X を計算する
		FLAG_TY		= 1 << 25,	// Trans  Y を計算する
		FLAG_TZ		= 1 << 26,	// Trans  Z を計算する
		FLAG_TS		= 1 << 27,	// Trans 速度

		FLAG_B		= 1 << 30,	// 初回
		FLAG_P		= 1 << 31,	// ポーズ

		FLAG_RC		= (FLAG_RW|FLAG_RL),

		FLAG_S		= (FLAG_SX|FLAG_SY|FLAG_SZ),
		FLAG_R		= (FLAG_RX|FLAG_RY|FLAG_RZ),
		FLAG_T		= (FLAG_TX|FLAG_TY|FLAG_TZ),

		FLAG_SR		= (FLAG_S|FLAG_R),
		FLAG_RT		= (FLAG_R|FLAG_T),
		FLAG_ST		= (FLAG_S|FLAG_T),
		FLAG_SRT	= (FLAG_S|FLAG_R|FLAG_T),
		FLAG_SRY	= (FLAG_S|FLAG_RY),
		FLAG_VS		= (FLAG_SS|FLAG_RS|FLAG_TS),

		FLAG_BOOT	= FLAG_B,
		FLAG_PAUSE	= FLAG_P,

		FLAG_CA		= (FLAG_CS|FLAG_CR|FLAG_CT),

		FLAG_DEFAULT1	= (FLAG_CA),					// デフォルトマスク
		FLAG_DEFAULT2	= (FLAG_B|FLAG_SRT|FLAG_VS),	// デフォルト設定
	};
}

// End Of File
