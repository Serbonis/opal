#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力データ
//========================================
namespace opal {
	class PADS {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit PADS( const PADS* = nullptr );
		virtual ~PADS() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		PADW*	work;
		PADC*	count;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		PADW* Work(  void ) const;
		PADC* Count( void ) const;
		void  Work(  PADW* );
		void  Count( PADC* );

		//----------------------------------------
		// 判定関数:PADC
		//----------------------------------------
	public:
		int KeyCount(       UINT ) const;
		int MouseCount(     UINT ) const;
		int JoyCount( UINT, UINT ) const;
		int Joy1Count(      UINT ) const;
		int Joy2Count(      UINT ) const;

		int JKCount(  UINT, UINT, UINT ) const;
		int J1KCount(       UINT, UINT ) const;
		int J2KCount(       UINT, UINT ) const;

		int JKCount(  UINT, const PADA* ) const;
		int J1KCount(       const PADA* ) const;
		int J2KCount(       const PADA* ) const;

		//----------------------------------------
		// 判定関数:PADW
		//----------------------------------------
	public:
		int KeyPush( UINT, UINT = 0, UINT = 1 ) const;
		int KeyTrig( UINT, UINT = 0, UINT = 1 ) const;
		int KeyRels( UINT, UINT = 0, UINT = 1 ) const;

	public:
		int JoyPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JoyTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JoyRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JoyPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JoyTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JoyRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;

		int Joy1PushO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2PushO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy1TrigO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2TrigO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy1RelsO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2RelsO( UINT, UINT = 0, UINT = 1 ) const;
		int Joy1PushA( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2PushA( UINT, UINT = 0, UINT = 1 ) const;
		int Joy1TrigA( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2TrigA( UINT, UINT = 0, UINT = 1 ) const;
		int Joy1RelsA( UINT, UINT = 0, UINT = 1 ) const;
		int Joy2RelsA( UINT, UINT = 0, UINT = 1 ) const;

		float JoyStick(  UINT, UINT, UINT = 0 ) const;
		float Joy1Stick(       UINT, UINT = 0 ) const;
		float Joy2Stick(       UINT, UINT = 0 ) const;

	public:
		int MousePush( UINT, UINT = 0, UINT = 1 ) const;
		int MouseTrig( UINT, UINT = 0, UINT = 1 ) const;
		int MouseRels( UINT, UINT = 0, UINT = 1 ) const;

		float Mouse(  UINT, UINT = 0 ) const;
		float MouseX(       UINT = 0 ) const;
		float MouseY(       UINT = 0 ) const;
		float Scroll(       UINT = 0 ) const;

		float Cursor( UINT, UINT = 0 ) const;
		float CursorX(      UINT = 0 ) const;
		float CursorY(      UINT = 0 ) const;

	public:
		int JKPushO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JKTrigO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JKRelsO( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JKPushA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JKTrigA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;
		int JKRelsA( UINT, UINT, UINT, UINT = 0, UINT = 1 ) const;

		int J1KPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KPushO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J1KTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KTrigO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J1KRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KRelsO( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J1KPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KPushA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J1KTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KTrigA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J1KRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;
		int J2KRelsA( UINT, UINT, UINT = 0, UINT = 1 ) const;

		int JKPushO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		int JKTrigO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		int JKRelsO( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		int JKPushA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		int JKTrigA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;
		int JKRelsA( UINT, const PADA*, UINT = 0, UINT = 1 ) const;

		int J1KPushO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J2KPushO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J1KTrigO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J2KTrigO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J1KRelsO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J2KRelsO( const PADA*, UINT = 0, UINT = 1 ) const;
		int J1KPushA( const PADA*, UINT = 0, UINT = 1 ) const;
		int J2KPushA( const PADA*, UINT = 0, UINT = 1 ) const;
		int J1KTrigA( const PADA*, UINT = 0, UINT = 1 ) const;
		int J2KTrigA( const PADA*, UINT = 0, UINT = 1 ) const;
		int J1KRelsA( const PADA*, UINT = 0, UINT = 1 ) const;
	};
}

// End Of File
