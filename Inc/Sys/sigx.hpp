#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

//========================================
// シグナルタスク
//========================================
namespace opal {
	class SIGT {
		friend class SIGX;
		friend class DEBUGX;

	public:
		explicit SIGT();
		virtual ~SIGT() = default;

	private:
		std::unordered_map<std::string,SIGX::FUNC>	func;
		std::unordered_map<std::string,SIGX::ARGV>	argv;
		bool stat;
		int  level;

	public:
		virtual void Reset( void );
		virtual void Clear( void );

	public:
		virtual void Set( const char*, SIGX::FUNC = nullptr, SIGX::ARGV = nullptr );
		virtual void Del( const char* );

	public:
		virtual void Kill( const char* );
	};
}

// End Of File
