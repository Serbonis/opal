#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

#include "call.hpp"

#include "tlinx.hpp"

#include "Obj/zobj.hpp"

//========================================
// サウンドワーク
//========================================
namespace opal {
	class CALLS {
		friend class CALLT;

	public:
		explicit CALLS();
		virtual ~CALLS();

	protected:
		enum {
			MODE_NULL,
			MODE_STOP,
			MODE_PLAY,
			MODE_LOOP,
			MODE_PLAYING,
		};

	protected:
		enum {
			CALL_STATE_NULL		= 0L,
			CALL_STATE_PAUSE	= 1 <<  0,
			CALL_STATE_PAUSE_T	= 1 <<  1,
			CALL_STATE_FUNC		= 1 <<  2,
			CALL_STATE_MAIN 	= 1 <<  3,

			CALL_STATE_DEFAULT	= CALL_STATE_NULL,
		};
		UINT	state;

	protected:
		struct FLAG final {
			FLAG();

			FLAG_SUPER<bool>	mute;	// 消音
			FLAG_SUPER<bool>	loop;	// ループ
		}	flag;

	protected:
		struct PROP final {
			PROP();

			float	volume;		// 音量
			float	tempo;		// 速度
			UINT	repeat;		// 回数
			UINT	fade;		// フェード

			struct CTRL {
				CTRL();

				bool	play;	// 演奏開始
				bool	stop;	// 演奏停止
				bool	volm;	// 音量変更
				bool	tmpo;	// 速度変更
			}	ctrl;

			struct STATE {
				STATE();

				UINT	mode;	// 演奏制御
				bool	play;	// 演奏フラグ
				UINT	time;	// 演奏時間
				UINT	leng;	// 曲の長さ
				UINT	loop;	// 演奏回数
			}	state;
		}	prop;

	public:
		virtual void Init( void );
		virtual void Free( void );

	public:
		virtual void SetMute( int = FLAG_FLIP );
		virtual void SetLoop( int = FLAG_FLIP );
		virtual bool GetMute( void ) const;
		virtual bool GetLoop( void ) const;
		virtual bool GetMute( UINT ) const;
		virtual bool GetLoop( UINT ) const;

	public:
		virtual void SetSuperMute( int  );
		virtual void SetSuperLoop( int  );
		virtual void SetSuperMute( void );
		virtual void SetSuperLoop( void );
		virtual int  GetSuperMute( void ) const;
		virtual int  GetSuperLoop( void ) const;

	public:
		virtual void  SetVolume( float = 1.0f );
		virtual void  SetTempo(  float = 1.0f );
		virtual void  SetRepeat( UINT  = 0 );
		virtual void  SetFade(   UINT  = 0 );

		virtual float GetVolume( void ) const;
		virtual float GetTempo(  void ) const;
		virtual UINT  GetRepeat( void ) const;
		virtual UINT  GetFade(   void ) const;

	public:
		virtual void SetPlay( UINT = 0 );
		virtual void SetStop( UINT = 0 );

		virtual bool GetPlay( void ) const;
		virtual bool GetStop( void ) const;

	public:
		virtual UINT PlayMode( void ) const;
		virtual UINT PlayTime( void ) const;
		virtual UINT PlayLeng( void ) const;
		virtual UINT PlayLoop( void ) const;

	public:
		virtual void Coordinate( const CALLS* = nullptr );
	};
}

//========================================
// サウンドタスク
//========================================
namespace opal {
	class CLINX;

	class CALLT :
		public ZOBJ,
		public CALLS,
		public ILINX<CLINX>
	{
		friend class CALLX;

	public:
		explicit CALLT();
		virtual ~CALLT();

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	private:
		virtual void Open_( const std::string_view, CLINX* ) override;
		virtual void Close_(                        CLINX* ) override;

	public:
		using ILINX::Open;
		using ILINX::Close;
		virtual void Open( const std::string_view, const char* );

	private:
		virtual void Register(   LINX* ) override;
		virtual void Release(    LINX* ) override;
		virtual void Register_( CLINX* ) override;
		virtual void Release_(  CLINX* ) override;

	public:
		using ILINX::Register;
		using ILINX::Release;
		virtual void Register( const char* );

	public:
		virtual auto GetBranch( void ) const->CLINX*;
		virtual void SetBranch( CLINX* );

	public:
		virtual void CallInfo( void ) const;
		virtual void CallInfo( int  ) const;

	protected:
		virtual void CallInit(  void );
		virtual void CallEnter( void );
		virtual void CallFunc(  void );
		virtual void CallBegin( void );
		virtual void CallMain(  void );
		virtual void CallEnd(   void );
		virtual void CallExit(  void );
		virtual void CallFree(  void );

	protected:
		virtual void TaskInfo(  void ) const override final;
		virtual void TaskInit(  void ) override final;
		virtual void TaskEnter( void ) override final;
		virtual void TaskFunc(  void ) override final;
		virtual void TaskBegin( void ) override final;
		virtual void TaskMain(  void ) override final;
		virtual void TaskEnd(   void ) override final;
		virtual void TaskExit(  void ) override final;
		virtual void TaskFree(  void ) override final;

	public:
		virtual void Coordinate(   const SUPERT* = nullptr ) override;
		virtual void ReCoordinate( const SUPERT* = nullptr ) override;

	public:
		virtual CLINX* CLinx( void ) const;
		static  CLINX* TLinx( const CALLT* );
		static  LINX*  TLinx( void );

	protected:
		using DTP = std::shared_ptr<CALLT>;
		using DTS = std::string;
		using DTR = std::shared_ptr<class RSRC>;
		using DTI = std::function<DTR(CALLT*,const char*,DTR)>;
		using DTF = std::function<void(CALLT*)>;

		static void Call( DTP, const char*, DTI, DTF = nullptr );

	private:
		static void callSysInit( void );
		static void callSysFree( void );

		static void callInit( void );
		static void callExec( void );
		static void callFree( void );

		struct CALL_LIST {
			DTP		task;
			DTS		name;
			DTI		init;
			DTF 	func;
			UINT	loop;
		};

		static std::vector<CALL_LIST>				call_list;
		static std::unordered_map<CALLT*,DTP>		call_task;
		static std::unordered_map<std::string,DTR>	call_file;
	};
}

//========================================
// サウンドタスク専用リンク
//========================================
namespace opal {
	class CLINX : public TLINX<CALLT>{};
	class CALLL : public TASKL<CALLT,CLINX>{};
	template<typename TYPE> class CALLTL : public TASKL<TYPE,CLINX>{};
}

// End Of File
