#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// OPAL
//========================================
namespace opal {}

#ifndef OPAL_GAME
#define OPAL_GAME	opal_game
#endif

class GAME;
class OPAL final : private opal::NOINSTANCE {
	//----------------------------------------
	// コンフィグ
	//----------------------------------------
public:
	class CONFIG {
	public:
		explicit CONFIG();
		explicit CONFIG( const char* );
		explicit CONFIG( const std::string& );
		explicit CONFIG( std::nullptr_t );
		virtual ~CONFIG();

	protected:
		const char*	project{nullptr};
	};

	//----------------------------------------
	// ゲーム関数
	//----------------------------------------
public:
	class FUNC : public CONFIG {
		using CONFIG::CONFIG;

	protected:
		explicit FUNC();

	public:
		virtual ~FUNC();

	public:
		operator FUNC*();

	public:
		virtual void SysInit( void );
		virtual void SysExec( void );
		virtual void SysFree( void );

	private:
		friend int Opal( void );
		virtual void SysConf( void ) final;
	};

	using sysinit = std::function<void(void)>;
	using sysexec = std::function<void(void)>;
	using sysfree = std::function<void(void)>;

public:
	static void SYSFUNC( sysinit fi, sysinit fe, sysfree ff = nullptr );
	static void SYSFUNC( sysexec fe );
	static void SYSFUNC( FUNC* fc );
	static void SYSFUNC( FUNC& fc );

public:
	static FUNC* SYSFUNC( void );
	template<typename TYPE=class GAME>static TYPE* GAME( void ){ return dynamic_cast<TYPE*>( SYSFUNC() ); }

public:
	static void SYSINIT( sysinit fi );
	static void SYSEXEC( sysexec fe );
	static void SYSFREE( sysfree ff );

public:
	static sysinit SYSINIT( void );
	static sysexec SYSEXEC( void );
	static sysfree SYSFREE( void );

	//----------------------------------------
	// スプラッシュ
	//----------------------------------------
public:
	static void SPLASH( UINT );
	static UINT SPLASH( void );

	//----------------------------------------
	// バージョン
	//----------------------------------------
public:
	static float VERSION( void );

	//----------------------------------------
	// 引数
	//----------------------------------------
public:
	static UINT ARGC( void );
	static const char* ARGV( void );
	static const char* ARGV( UINT );

	//----------------------------------------
	// ファイル名など
	//----------------------------------------
public:
	static const char* GAME_PATH( void );
	static const char* GAME_FILE( void );
	static const char* GAME_CURD( void );
	static const char* GAME_RELP( void );

	//----------------------------------------
	// ファイルパス
	//----------------------------------------
public:
	static const char* FILEPATH( void );
	static void FILEPATH( const char* );

	//----------------------------------------
	// デバッグモニター2
	//----------------------------------------
public:
	static void CONSOLE( bool );
	static bool CONSOLE( void );

	//----------------------------------------
	// デバッグモニター1
	//----------------------------------------
public:
	static void DEBUG_MODE( UINT );
	static UINT DEBUG_MODE( void );

	//----------------------------------------
	// 多重起動の許可
	//----------------------------------------
public:
	static void MUTEX( UINT );
	static UINT MUTEX( void );

	//----------------------------------------
	// ウインドウのタイトル
	//----------------------------------------
public:
	static void PROJECT( const char* );
	static void PROJECT( const std::string& );
	static void PROJECT( std::nullptr_t );
	static const char* PROJECT( void );

	//----------------------------------------
	// ウィンドウアイコン
	//----------------------------------------
public:
	static void ICON( const char* );
	static void ICON( const std::string& );
	static void ICON( std::nullptr_t );
	static const char* ICON( void );

	//----------------------------------------
	// ハンドル
	//----------------------------------------
public:
	static void HANDLE( bool );
	static bool HANDLE( void );

	//----------------------------------------
	// 動作モード
	//----------------------------------------
public:
	enum ACTIVE {
		ACTIVE_DEFAULT		= 0L,
		ACTIVE_KILLFOCUS	= 1 << 0,
		ACTIVE_MINIMIZE		= 1 << 1,
		ACTIVE_SIZEMOVE		= 1 << 2,
	};
	static void ACTIVE( UINT );
	static UINT ACTIVE( void );

	//----------------------------------------
	// ウィンドウスタイル
	//----------------------------------------
public:
	static void WINSTYLE( UINT );
	static UINT WINSTYLE( void );

	//----------------------------------------
	// スクリーンサイズ
	//----------------------------------------
public:
	static void SCREEN( UINT w, UINT h );
	static void SCREEN_W( UINT );
	static void SCREEN_H( UINT );
	static UINT SCREEN_W( void );
	static UINT SCREEN_H( void );

	//----------------------------------------
	// 描画領域サイズ
	//----------------------------------------
public:
	static void DISPLAY( UINT w, UINT h );
	static void DISPLAY_W( UINT );
	static void DISPLAY_H( UINT );
	static UINT DISPLAY_W( void );
	static UINT DISPLAY_H( void );

	//----------------------------------------
	// カーソル
	//----------------------------------------
public:
	static void CURSOR( bool );
	static bool CURSOR( void );

	//----------------------------------------
	// 背景色
	//----------------------------------------
public:
	static void  BGCOLOR( UCHAR, UCHAR, UCHAR );
	static void  BGCOLOR( UINT );
	static UCHAR BGCOLOR_R( void );
	static UCHAR BGCOLOR_G( void );
	static UCHAR BGCOLOR_B( void );
	static UINT  BGCOLOR( void );

	//----------------------------------------
	// フレームレート
	//----------------------------------------
public:
	enum FRAME_RATE {
		VARIABLE,
		FIXED60,
		FIXED30,
		FIXED20,
		FIXED10,
	};
	static void FRAME( FRAME_RATE );
	static FRAME_RATE FRAME( void );

	//----------------------------------------
	// メインメモリ
	//----------------------------------------
public:
	static void   MAINMEMORY_SIZE( size_t );
	static size_t MAINMEMORY_SIZE( void );

	static void* MAINMEMORY_ADRS( void );
	template<typename TYPE> static TYPE* MAINMEMORY_ADRS( void ){ return reinterpret_cast<TYPE*>( OPAL::MAINMEMORY_ADRS() ); }

	//----------------------------------------
	// パッドバッファ
	//----------------------------------------
public:
	static void PAD_WORK( UINT );
	static UINT PAD_WORK( void );
	static void PAD_HOME(    UINT );
	static void PAD_EXIT(    UINT );
	static void PAD_FULL(    UINT );
	static void PAD_RESET(   UINT );
	static void PAD_PAUSE(   UINT );
	static void PAD_BREAK(   UINT );
	static void PAD_CAPTURE( UINT );
	static void PAD_FRAME(   UINT );
	static void PAD_TIMES(   UINT );
	static void PAD_TIMEC(   UINT );
	static void PAD_RANDOM(  UINT );
	static UINT PAD_HOME(    void );
	static UINT PAD_EXIT(    void );
	static UINT PAD_FULL(    void );
	static UINT PAD_RESET(   void );
	static UINT PAD_PAUSE(   void );
	static UINT PAD_BREAK(   void );
	static UINT PAD_CAPTURE( void );
	static UINT PAD_FRAME(   void );
	static UINT PAD_TIMES(   void );
	static UINT PAD_TIMEC(   void );
	static UINT PAD_RANDOM(  void );

	//----------------------------------------
	// クリティカルセクション
	//----------------------------------------
public:
	struct CRITICAL {
		CRITICAL();
		~CRITICAL();
	};
	static CRITICAL_SECTION* Lock( void );

	//----------------------------------------
	// システムメッセージ
	//----------------------------------------
public:
	static void MESSAGE( int(*)( const char*, ... ) = nullptr );
};

// End Of File
