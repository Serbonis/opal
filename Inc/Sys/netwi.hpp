#pragma once
//========================================
// OPAL Game Library
//========================================
#include "netw.hpp"

//========================================
//  ネットワーク情報
//========================================
namespace opal {
	class NETWD : public MESSAGE_DATA, public MESSAGE_TASK {
		using MESSAGE_DATA::MESSAGE_DATA;
		using MESSAGE_TASK::MESSAGE_TASK;
	};

	class NETWI : public NETWD {
		friend class NETWX;
		friend class NETWT;
		friend class NETWT_S;
		friend class NETWT_C;

	public:
		using NETWD::NETWD;

		explicit NETWI() = default;
		virtual ~NETWI() = default;

	protected:
		enum {
			TASK_NULL = -1,
			TASK_SERVER,
			TASK_CLIENT,
		};

		enum {
			ERROR_NULL,
			ERROR_SOCKET,
			ERROR_BIND,
			ERROR_LISTEN,
		};

#ifdef OPAL_DEBUG
	public:
		void Debug( void ) const;
		void Error( const NETWT* ) const;
#endif
	};
}

// End Of FILE
