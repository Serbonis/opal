#pragma once
//========================================
// OPAL Game Library
//========================================
#include "prim.hpp"
#include "primx.hpp"

//========================================
// プリミティブジェネレーター
//========================================
namespace opal {
	class PRIMS final : private NOINSTANCE {
	private:
		using CCP = const char*;
		using CSR = const std::string&;

		template<typename TT>using SP = std::shared_ptr<TT>;

		//----------------------------------------
		// ユーザーワークタイプ
		//----------------------------------------
	public:
		template<typename TT, typename... AT>static auto SharedPrim(            AT&&... a )->SP<TT>{ return { NewOpen<    TT>(    std::forward<AT>( a )... ), []( TT* p ){ CloseDelete( p ); } };	}
		template<typename TT, typename... AT>static auto SharedPrimName( CCP k, AT&&... a )->SP<TT>{ return { NewOpenName<TT>( k, std::forward<AT>( a )... ), []( TT* p ){ CloseDelete( p ); } };	}
		template<typename TT, typename... AT>static auto SharedPrimName( CSR k, AT&&... a )->SP<TT>{ return { NewOpenName<TT>( k, std::forward<AT>( a )... ), []( TT* p ){ CloseDelete( p ); } };	}

		template<typename ST, typename... AT>static auto MakeSharedPrim(     ST& p,        AT&&... a )->ST{ return p = SharedPrim<    typename ST::element_type>(    std::forward<AT>( a )... );	}
		template<typename ST, typename... AT>static auto MakeSharedPrimName( ST& p, CCP k, AT&&... a )->ST{ return p = SharedPrimName<typename ST::element_type>( k, std::forward<AT>( a )... );	}
		template<typename ST, typename... AT>static auto MakeSharedPrimName( ST& p, CSR k, AT&&... a )->ST{ return p = SharedPrimName<typename ST::element_type>( k, std::forward<AT>( a )... );	}

		//----------------------------------------
		// 自己参照タイプ(PRIMT継承)
		//----------------------------------------
	public:
		template<typename TT, typename... AT>static auto SharedSelf(            AT&&... a )->SP<TT>{ if ( auto p = SP<TT>{ NewOpen<    TT>(    std::forward<AT>( a )... ), [](auto){} } ) { return p->Self( p ); } return nullptr; }
		template<typename TT, typename... AT>static auto SharedSelfName( CCP k, AT&&... a )->SP<TT>{ if ( auto p = SP<TT>{ NewOpenName<TT>( k, std::forward<AT>( a )... ), [](auto){} } ) { return p->Self( p ); } return nullptr; }
		template<typename TT, typename... AT>static auto SharedSelfName( CSR k, AT&&... a )->SP<TT>{ if ( auto p = SP<TT>{ NewOpenName<TT>( k, std::forward<AT>( a )... ), [](auto){} } ) { return p->Self( p ); } return nullptr; }

		//----------------------------------------
		// New/Open - Close/Delete
		//----------------------------------------
	private:
		template<typename TT, typename... AT>static TT* NewOpen(            AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Open(   ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewOpenName( CCP k, AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Open( k ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewOpenName( CSR k, AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Open( k ); return p; } return nullptr; }

		template<typename TT>static void CloseDelete( TT* p ){

			if ( p ) {
				p->Close();
				DELETES( p );
			}
		}
	};
}

// End Of File
