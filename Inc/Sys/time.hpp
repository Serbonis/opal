#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// タイマーシステム
//========================================
namespace opal {
	class TIME final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	private:
		static DWORD			fps;
		static LARGE_INTEGER	seed;
		static LARGE_INTEGER 	freq;
		static LARGE_INTEGER	now;
		static LARGE_INTEGER	old;
		static double			dlt;
		static DWORD			time;
		static DWORD			frame;

	private:
		static void Init( void );
		static void Free( void );

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	private:
		static void Sleep( UINT = 1 );

	public:
		static DWORD Seed( void );
		static DWORD Time( void );

	public:
		static float Fps( void );
		static float FPS( void );

	public:
		static float Delta( void  );
		static float Delta( float );

	public:
		static float Second( float );
		static float Minute( float );
		static int   Second( void  );
		static int   Minute( void  );

	public:
		static float Frame( float );
		static DWORD Frame( void  );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};

	constexpr int SECOND( float s ){ return static_cast<int>( s * 60 *  1 );	}
	constexpr int MINUTE( float m ){ return static_cast<int>( m * 60 * 60 );	}

#define SEC( s )	SECOND( s )
#define MIN( m )	MINUTE( m )
}

// End Of File
