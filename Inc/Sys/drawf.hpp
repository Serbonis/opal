#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// 描画フォーマット
//========================================
namespace opal {
	using FVF_INDEX16	= WORD;
	using FVF_INDEX32	= DWORD;
}

//========================================
// 汎用型フォーマット
//========================================
namespace opal {
	struct FVF_XYZ {
		constexpr explicit FVF_XYZ() : x(0.0f),y(0.0f),z(0.0f) {}
		constexpr explicit FVF_XYZ( float x, float y, float z ) : x(x),y(y),z(z) {}

		float		x;
		float		y;
		float		z;
	};
}

namespace opal {
	struct FVF_XYZW {
		constexpr explicit FVF_XYZW() : x(0.0f),y(0.0f),z(0.0f),w(1.0f) {}
		constexpr explicit FVF_XYZW( float x, float y, float z, float w = 1.0f ) : x(x),y(y),z(z),w(w) {}

		float		x;
		float		y;
		float		z;
		float		w;
	};
}

namespace opal {
	struct FVF_UV {
		constexpr explicit FVF_UV() : u(0.0f),v(0.0f) {}
		constexpr explicit FVF_UV( float u, float v ) : u(u),v(v) {}

		float		u;
		float		v;
	};
}

//========================================
// 目的別フォーマット
//========================================
namespace opal {
	struct FVF_VERTEX : public FVF_XYZ {
		constexpr explicit FVF_VERTEX() : FVF_XYZ() {}
		constexpr explicit FVF_VERTEX( float x, float y, float z ) : FVF_XYZ( x, y, z ) {}
	};
}

namespace opal {
	struct FVF_VERTEXW : public FVF_XYZW {
		constexpr explicit FVF_VERTEXW() : FVF_XYZW() {}
		constexpr explicit FVF_VERTEXW( float x, float y, float z, float w = 1.0f ) : FVF_XYZW( x, y, z, w ) {}
	};
}

namespace opal {
	struct FVF_NORMAL : public FVF_XYZ {
		explicit FVF_NORMAL() : FVF_XYZ() {}
		explicit FVF_NORMAL( float x, float y, float z ) : FVF_XYZ( x, y, z ) {}
	};
}

namespace opal {
	struct FVF_TEXTURE : public FVF_UV {
		constexpr explicit FVF_TEXTURE() : FVF_UV() {}
		constexpr explicit FVF_TEXTURE( float u, float v ) : FVF_UV( u, v ) {}
	};
}

namespace opal {
	struct FVF_TEXTUREW : public FVF_XYZW {
		constexpr explicit FVF_TEXTUREW() : FVF_XYZW() {}
		constexpr explicit FVF_TEXTUREW( float x, float y, float z, float w = 1.0f ) : FVF_XYZW( x, y, z, w ) {}
	};
}

namespace opal {
	struct FVF_COLOR {
		explicit FVF_COLOR( COLOR c = 0xffffffff ) : c(c) {}
		explicit FVF_COLOR( COLOR c, UCHAR a ) : c( COLOR_C( c, a ) ) {}
		explicit FVF_COLOR( UCHAR r, UCHAR g, UCHAR b, UCHAR a = 0xff ) : c( COLOR_V( a, r, g, b ) ) {}

		DWORD	c;	// D3DCOLOR
	};
}

namespace opal {
	template <UINT N>struct FVF_BLENDW {
		explicit FVF_BLENDW() {
			for ( auto i = 0UL; i < N; i++ ) {
				w[i] = 0.0f;
			}
		}
		float		w[N];
	};
}

namespace opal {
	struct FVF_BLENDI {
		explicit FVF_BLENDI() : d(0L ) {}
		union {
			DWORD			d;
			unsigned char	i[4];
		};
	};
}

//========================================
// 固定機能パイプライン用
//========================================
namespace opal {
	struct FVF_VC2 {
		enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE };

		explicit FVF_VC2() : vertex(),color() {}
		explicit FVF_VC2( float x, float y, float z, COLOR c = 0xffffffff ) :
			vertex( x, y, z ),
			color( c )
		{}
		explicit FVF_VC2( float x, float y, COLOR c = 0xffffffff ) :
			vertex( x, y, 0.0f ),
			color( c )
		{}
		explicit FVF_VC2( COLOR c ) :
			vertex(),
			color( c )
		{}

		FVF_VERTEX	vertex;
		FVF_COLOR	color;
	};

	struct FVF_VC2W {
		enum { FVF = D3DFVF_XYZRHW|D3DFVF_DIFFUSE };

		explicit FVF_VC2W() : vertex(),color() {}
		explicit FVF_VC2W( float x, float y, float z, float w, COLOR c = 0xffffffff ) :
			vertex( x, y, z, w ),
			color( c )
		{}
		explicit FVF_VC2W( float x, float y, COLOR c = 0xffffffff ) :
			vertex( x, y, 0.0f, 1.0f ),
			color( c )
		{}
		explicit FVF_VC2W( COLOR c ) :
			vertex(),
			color( c )
		{}

		FVF_VERTEXW	vertex;
		FVF_COLOR	color;
	};
}

namespace opal {
	struct FVF_VCT2 {
		enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1 };

		explicit FVF_VCT2() : vertex(),color(),texture() {}
		explicit FVF_VCT2( float x, float y, float z, COLOR c, float u, float v ) :
			vertex( x, y, z ),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT2( float x, float y, float z = 0.0f, COLOR c = 0xffffffff ) :
			vertex( x, y, z ),
			color( c ),
			texture()
		{}
		explicit FVF_VCT2( COLOR c, float u, float v ) :
			vertex(),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT2( COLOR c ) :
			vertex(),
			color( c ),
			texture()
		{}

		FVF_VERTEX	vertex;
		FVF_COLOR	color;
		FVF_TEXTURE	texture;
	};

	struct FVF_VCT2W {
		enum { FVF = D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1 };

		explicit FVF_VCT2W() : vertex(),color(),texture() {}
		explicit FVF_VCT2W( float x, float y, float z, float w, COLOR c, float u, float v ) :
			vertex( x, y, z, w ),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT2W( float x, float y, float z = 0.0f, float w = 1.0f, COLOR c = 0xffffffff ) :
			vertex( x, y, z, w ),
			color( c ),
			texture()
		{}
		explicit FVF_VCT2W( COLOR c, float u, float v ) :
			vertex(),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT2W( COLOR c ) :
			vertex(),
			color( c ),
			texture()
		{}

		FVF_VERTEXW	vertex;
		FVF_COLOR	color;
		FVF_TEXTURE	texture;
	};
}

namespace opal {
	struct FVF_VC3 {
	enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE };

		explicit FVF_VC3() : vertex(),color() {}
		explicit FVF_VC3( float x, float y, float z, COLOR c = 0xffffffff ) :
			vertex( x, y, z ),
			color( c )
		{}
		explicit FVF_VC3( COLOR c ) :
			vertex(),
			color( c )
		{}

		FVF_VERTEX	vertex;
		FVF_COLOR	color;
	};
}

namespace opal {
	struct FVF_VCT3 {
		enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1 };

		explicit FVF_VCT3() : vertex(),color(),texture() {}
		explicit FVF_VCT3( float x, float y, float z, COLOR c, float u, float v ) :
			vertex( x, y, z ),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT3( float x, float y, float z, COLOR c = 0xffffffff ) :
			vertex( x, y, z ),
			color( c ),
			texture()
		{}
		explicit FVF_VCT3( COLOR c, float u, float v ) :
			vertex(),
			color( c ),
			texture( u, v )
		{}
		explicit FVF_VCT3( COLOR c ) :
			vertex(),
			color( c ),
			texture()
		{}

		FVF_VERTEX	vertex;
		FVF_COLOR	color;
		FVF_TEXTURE	texture;
	};
}

namespace opal {
	struct FVF_VT {
		enum { FVF = D3DFVF_XYZ|D3DFVF_TEX1 };

		constexpr explicit FVF_VT() : vertex(),texture() {}
		constexpr explicit FVF_VT( float x, float y, float z, float u, float v ) :
			vertex( x, y, z ),
			texture( u, v )
		{}
		constexpr explicit FVF_VT( float x, float y, float z ) :
			vertex( x, y, z ),
			texture()
		{}
		constexpr explicit FVF_VT( float u, float v ) :
			vertex(),
			texture( u, v )
		{}

		FVF_VERTEX	vertex;
		FVF_TEXTURE	texture;

		static constexpr auto	INDEX0 = 0UL;
		static constexpr auto	INDEX1 = INDEX0 + sizeof( FVF_VERTEX );
	};
}

namespace opal {
	struct FVF_VNT {
		enum { FVF = D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1 };

		explicit FVF_VNT() : vertex(),normal(),texture() {}
		explicit FVF_VNT( float x, float y, float z, float _a, float _b, float c, float u, float v ) :
			vertex( x, y, z ),
			normal( _a, _b, c ),
			texture( u, v )
		{}
		explicit FVF_VNT( float x, float y, float z, float u, float v ) :
			vertex( x, y, z ),
			normal(),
			texture( u, v )
		{}
		explicit FVF_VNT( float x, float y, float z ) :
			vertex( x, y, z ),
			normal(),
			texture()
		{}
		explicit FVF_VNT( float u, float v ) :
			vertex(),
			normal(),
			texture( u, v )
		{}

		FVF_VERTEX	vertex;
		FVF_NORMAL	normal;
		FVF_TEXTURE	texture;
	};
}

#define FVF_VCT2(N)																						\
namespace opal {																						\
	struct FVF_VCT2_##N {																				\
		enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX##N };											\
\
		explicit FVF_VCT2_##N() : vertex(),color(),texture() {}											\
		explicit FVF_VCT2_##N( float x, float y, float z, COLOR c = 0xffffffff ) :						\
			vertex( x, y, z ),																			\
			color( c ),																					\
			texture()																					\
		{}																								\
		explicit FVF_VCT2_##N( COLOR c ) :																\
			vertex(),																					\
			color( c ),																					\
			texture()																					\
		{}																								\
\
		FVF_VERTEX	vertex;																				\
		FVF_COLOR	color;																				\
		FVF_TEXTURE	texture[N];																			\
\
		void SetTexture( UINT n, float u, float v ){ texture[n] = FVF_TEXTURE( u, v );	};				\
	};																									\
}

FVF_VCT2(1)
FVF_VCT2(2)
FVF_VCT2(3)
FVF_VCT2(4)
FVF_VCT2(5)
FVF_VCT2(6)
FVF_VCT2(7)
FVF_VCT2(8)

#define FVF_VCT2W(N)																					\
namespace opal {																						\
	struct FVF_VCT2W_##N {																				\
		enum { FVF = D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX##N };										\
\
		explicit FVF_VCT2W_##N() : vertex(),color(),texture() {}										\
		explicit FVF_VCT2W_##N( float x, float y, float z, float w = 1.0f, COLOR c = 0xffffffff ) :		\
			vertex( x, y, z, w ),																		\
			color( c ),																					\
			texture()																					\
		{}																								\
		explicit FVF_VCT2W_##N( COLOR c ) :																\
			vertex(),																					\
			color( c ),																					\
			texture()																					\
		{}																								\
\
		FVF_VERTEXW	vertex;																				\
		FVF_COLOR	color;																				\
		FVF_TEXTURE	texture[N];																			\
\
		void SetTexture( UINT n, float u, float v ){ texture[n] = FVF_TEXTURE( u, v );	};				\
	};																									\
}

FVF_VCT2W(1)
FVF_VCT2W(2)
FVF_VCT2W(3)
FVF_VCT2W(4)
FVF_VCT2W(5)
FVF_VCT2W(6)
FVF_VCT2W(7)
FVF_VCT2W(8)

#define FVF_VCT3(N)																						\
namespace opal {																						\
	struct FVF_VCT3_##N {																				\
		enum { FVF = D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX##N };											\
\
		explicit FVF_VCT3_##N() : vertex(),color(),texture() {}											\
		explicit FVF_VCT3_##N( float x, float y, float z, COLOR c = 0xffffffff ) :						\
			vertex( x, y, z ),																			\
			color( c ),																					\
			texture()																					\
		{}																								\
		explicit FVF_VCT3_##N( COLOR c ) :																\
			vertex(),																					\
			color( c ),																					\
			texture()																					\
		{}																								\
\
		FVF_VERTEX	vertex;																				\
		FVF_COLOR	color;																				\
		FVF_TEXTURE	texture[N];																			\
\
		void SetTexture( UINT n, float u, float v ){ texture[n] = FVF_TEXTURE( u, v );	};				\
	};																									\
}

FVF_VCT3(1)
FVF_VCT3(2)
FVF_VCT3(3)
FVF_VCT3(4)
FVF_VCT3(5)
FVF_VCT3(6)
FVF_VCT3(7)
FVF_VCT3(8)

//========================================
// セット関数
//========================================
namespace opal {
	namespace FVF {
		template<typename TYPE>void Color( TYPE* fvf, const COLOR* color, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].color = FVF_COLOR( color[i] );
			}
		}

		template<typename TYPE>void Texture( TYPE* fvf, const VECTOR2* texture, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].texture.u = texture[i].x;
				fvf[i].texture.v = texture[i].y;
			}
		}

		template<typename TYPE>void Texture( TYPE* fvf, const VECTOR2* texture, UINT stage, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].texture[stage].u = texture[i].x;
				fvf[i].texture[stage].v = texture[i].y;
			}
		}

		template<typename TYPE>void Vertex2( TYPE* fvf, const VECTOR4* vertex, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].vertex = FVF_VERTEX( vertex[i].x,
											vertex[i].y,
											vertex[i].z );
			}
		}

		template<typename TYPE>void Vertex2w( TYPE* fvf, const VECTOR4* vertex, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].vertex = FVF_VERTEXW( vertex[i].x,
											 vertex[i].y,
											 vertex[i].z,
											 vertex[i].w );
			}
		}

		template<typename TYPE>void Vertex3( TYPE* fvf, const VECTOR4* vertex, UINT size = 1 ){

			for ( auto i = 0UL; i < size; i++ ) {
				fvf[i].vertex = FVF_VERTEX( vertex[i].x,
											vertex[i].y,
											vertex[i].z );
			}
		}
	}
}

// End Of File
