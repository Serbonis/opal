#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// ウィンドウシステム
//========================================
namespace opal {
	class WINDOW;

	class WINDOWX final : private NOINSTANCE {
		friend class DEBUGX;

	public:
		static void Open(  void );
		static void Close( void );

		static void Open(  HWND&, UINT, UINT, const char* = nullptr, WNDPROC = nullptr );
		static void Close( HWND& );

	public:
		static const WINDOW* Window( void );
		static HWND Handle(  void );
		static UINT Message( void );

	public:
		static int Process( void );

	public:
		static void Active( int  );
		static bool Active( void );

	public:
		static void Focus( int  );
		static bool Focus( void );

	public:
		static void Move( int, int, int, int );
		static bool Moving( void );
		static void Reset( void );

	public:
		static void Sizing( int  );
		static int  Sizing( void );

	public:
		static int WindowX( void );
		static int WindowY( void );
		static int WindowW( void );
		static int WindowH( void );

	public:
		static int FrameX( void );
		static int FrameY( void );
		static int FrameW( void );
		static int FrameH( void );

	public:
		static int ScreenW(  void );
		static int ScreenH(  void );
		static int DisplayW( void );
		static int DisplayH( void );
		static int ScreenW(  float );
		static int ScreenH(  float );
		static int DisplayW( float );
		static int DisplayH( float );
		static int ScreenW(  float, float );
		static int ScreenH(  float, float );
		static int DisplayW( float, float );
		static int DisplayH( float, float );

	public:
		static float ScreenR(  void );
		static float DisplayR( void );

	public:
		static void Screen( int  );
		static bool Screen( void );

	public:
		static void Cursor( int  );
		static bool Cursor( void );

		static void SetCursor(  LONG, LONG );
		static void SetCursorX( LONG );
		static void SetCursorY( LONG );

		static void AddCursor(  LONG, LONG );
		static void AddCursorX( LONG );
		static void AddCursorY( LONG );

		static void GetCursor(  LONG*, LONG* );
		static LONG GetCursorX( void );
		static LONG GetCursorY( void );

	public:
		static const char* Message( UINT );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "windowx.hpp"

// End Of File

