#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// 入力システム
//========================================
namespace opal {
	constexpr UINT	JOY_MAX		=  4;
	constexpr UINT	PAD_MAX		= 16;
	constexpr UINT	MOUSE_MAX	=  8;
	constexpr UINT	KEY_MAX		= 0x100;

	struct KEYBOARD {
		explicit KEYBOARD();
		virtual ~KEYBOARD();

		UCHAR	stroke[KEY_MAX];

		void clear( void );
		bool Extension( UINT vStk ) const;
	};

	struct MOUSE {
		explicit MOUSE();
		virtual ~MOUSE();

		::POINT		point;
		UINT		click;
		signed long	mx,my,mz;

		void clear( void );
	};

	struct JOYPAD {
		explicit JOYPAD();
		virtual ~JOYPAD();

		static constexpr UINT	POV_MAX = 4;

		UINT		button;
		float		lx,ly;
		float		rx,ry;
		DWORD		pov[POV_MAX];

		void clear( void );

		static constexpr UINT POV( UINT n ){ return 45*100*n;	}
	};

	namespace PADX_ {
		extern void SysInit( void );
		extern void SysExec( void );
		extern void SysFree( void );

		extern void SysInitKeyboard( void );
		extern bool SysExecKeyboard( KEYBOARD& );
		extern void SysFreeKeyboard( void );

		extern void SysInitMouse( void );
		extern bool SysExecMouse( MOUSE& );
		extern void SysFreeMouse( void );

		extern void SysInitJoypad( void );
		extern bool SysExecJoypad( JOYPAD&, UINT );
		extern void SysFreeJoypad( void );
		extern UINT SysJoypadMax(  void );
	}
}

// End Of File
