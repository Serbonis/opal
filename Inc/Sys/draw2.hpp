#pragma once
//========================================
// OPAL Game Library
//========================================
#include "drawx.hpp"

//========================================
// グラフィックタスク(2D)
//========================================
namespace opal {
	class DRAWT2 : public DRAWT {
		friend class DRAWX;
		friend class DRAWT;

	public:
		explicit DRAWT2();
		virtual ~DRAWT2();

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	protected:
		float	layer;

	public:
		virtual void  SetLayer( float = 1.0f );
		virtual float GetLayer( void ) const;

	public:
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
	};
}

// End Of File
