#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// ブレンド
//========================================
namespace opal {
	enum {
		ALPHA_NONE,		// なにもしない
		ALPHA_BLEND,	// 自由設定
		ALPHA_TRANS,	// 透明
		ALPHA_ALPHA,	// 半透明アルファ
		ALPHA_COLOR,	// 半透明カラー
		ALPHA_ADD,		// 加算合成(s+d)
		ALPHA_SUB,		// 減算合成(s-d)
		ALPHA_REV,		// 減算合成(d-s)
	};

	class DRAWB {
	public:
		explicit DRAWB();
		virtual ~DRAWB();

	public:
		virtual void Init( void );
		virtual void Free( void );

#if 0	// DX9に合わせていたダミー
	public:
		virtual void Alpha( bool ) const;
#endif

	protected:
		struct TEST final {
			TEST();

			UINT	flag;
			UCHAR	ref;	// α基準値
			DWORD	func;
		}		test;

	public:
		virtual void  SetAlphaTest( int );
		virtual void  SetAlphaTestRef(  UCHAR );
		virtual void  SetAlphaTestFunc( DWORD );
		virtual bool  GetAlphaTest(     void ) const;
		virtual UCHAR GetAlphaTestRef(  void ) const;
		virtual DWORD GetAlphaTestFunc( void ) const;
		virtual void  SetAlphaTest( UCHAR, DWORD );

	protected:
		struct BLEND final {
			BLEND();

			UINT	flag;
			UINT	type;
			DWORD	c[3];
			DWORD	a[3];
			float	factor[4];
			UINT	mask;
		}		blend;

	public:
		virtual void SetAlphaBlend( int  );
		virtual bool GetAlphaBlend( void ) const;

	public:
		virtual void SetAlphaBlendType( UINT );
		virtual UINT GetAlphaBlendType( void ) const;

	public:
		virtual void SetAlphaBlendDesc( DWORD, DWORD, DWORD, UINT = 0 );
		virtual void SetAlphaBlendDesc( DWORD, DWORD, DWORD, DWORD, DWORD, DWORD );
		virtual auto GetAlphaBlendDesc( UINT ) const->std::tuple<DWORD,DWORD,DWORD>;
		virtual auto GetAlphaBlendDesc( void ) const->std::tuple<DWORD,DWORD,DWORD,DWORD,DWORD,DWORD>;

	public:
		virtual void SetAlphaBlendFactor( float, float, float, float );
		virtual void SetAlphaBlendFactor( float, float, float );
		virtual void SetAlphaBlendFactor( float );
		virtual auto GetAlphaBlendFactor( void ) const->std::array<float,4>;
		virtual auto GetAlphaBlendFactor( bool ) const->const float*;

		virtual void SetAlphaBlendMask( UINT );
		virtual UINT GetAlphaBlendMask( void ) const;
	};
}

// End Of File
