#pragma once
//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ネットワークタスク
//========================================
namespace opal {
	class NETWI;

	class NETWT : public TASK, public MESSAGE_WORK<NETWI> {
		friend class NETWX;
		friend class NETWI;

	protected:
		using socklen_t	= int;

		struct HOST {
			HOST();
			~HOST();

			void operator ()( USHORT, ULONG );

			SOCKET			sock;
			SOCKADDR_IN		adrs;
			ADDRINFO*		list;
		};

	protected:
		enum {
			STEP_NULL,
			STEP_INIT,
			STEP_EXEC,
			STEP_FREE,
		};

	protected:
		explicit NETWT();

	public:
		using MESSAGE_WORK::MESSAGE_WORK;

		virtual ~NETWT();

	protected:
		HOST		server;	// サーバー情報
		UINT		fork;	// ソケット数
		UINT		size;	// 最大数

	protected:
		virtual void init( void ) final;
		virtual void free( void );
		virtual void kill( void );

	protected:
		virtual bool Socket( void );
		virtual bool Attach( void ) = 0;

	private:
		virtual void TaskMain( void ) override;
	};
}

//========================================
// NETWT for SERVER
//========================================
namespace opal {
	class NETWT_S : public NETWT {
		friend class NETWX;
		friend class NETWT;

	public:
		using NETWT::NETWT;

		explicit NETWT_S();
		virtual ~NETWT_S();

	protected:
		std::vector<HOST*>*	client;

	protected:
		virtual void init( USHORT, UINT = 0 );
		virtual void free( void ) override;
		virtual void kill( void ) override;

	protected:
		virtual bool Socket( void ) override;
		virtual bool Attach( void ) override;

	public:
		virtual void TaskInfo( void ) const;
	};
}

//========================================
// NETWT for CLIENT
//========================================
namespace opal {
	class NETWT_C : public NETWT {
		friend class NETWX;
		friend class NETWT;

	public:
		using NETWT::NETWT;

		explicit NETWT_C();
		virtual ~NETWT_C();

	protected:
		virtual void init( USHORT, UINT = 0 );
		virtual void free( void ) override;
		virtual void kill( void ) override;

	protected:
		virtual bool Socket( void ) override;
		virtual bool Attach( void ) override;

	public:
		virtual void TaskInfo( void ) const;
	};
}

// End Of Netw
