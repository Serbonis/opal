#pragma once
//========================================
// OPAL Game Library
//========================================
#ifdef OPAL_DEBUG
#include "xdebug.hpp"

//========================================
// デバッグマネージャー
//========================================
namespace opal {
	class DEBUGX : private NOINSTANCE {
	public:
		static void SysInit( void );
		static void SysExec( void );
		static void SysFree( void );

	private:
		static void Debug( UINT = 0 );

	public:
		static void Window( void );

	public:
		static void Begin(  void );
		static void Render( void );
		static void End(    void );

	private:
		static UINT CALLBACK process( void* );

	public:
		static void Monitor( void );

	private:
		static void Build( void );
		static void Rand(  void );
		static void Fps(   void );
		static void Gauge( void );
		static void Heap(  void );

	private:
		static void Sigx(   void );
		static void Padx(   void );
		static void Thrdx(  void );
		static void Filex(  void );
		static void Netwx(  void );

	private:
		static void Srvcx(  void );
		static void Rsrcx(  void );
		static void Primx(  void );

	private:
		static void Timer( void );

	private:
		static void Device( void );
		static void Spec(   void );

	private:
		static void Objx(  bool );
		static void Workx( bool );
		static void Drawx( bool );
		static void Callx( bool );

	private:
		static void Camera( void );

	private:
		template<typename TYPE>static void debugobj( const char* obj, bool f );
	};
}
#endif

// End Of File
