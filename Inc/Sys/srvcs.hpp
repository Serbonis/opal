#pragma once
//========================================
// OPAL Game Library
//========================================
#include "srvc.hpp"

//========================================
// サービスジェネレーター
//========================================
namespace opal {
	class SRVCS : private NOINSTANCE {
	public:
		template<typename TYPE>static TYPE* Generate( const char* k, UINT n = 0 ){

			if ( k ) {
				if ( !SRVCX::Search( k ) ) {
					if ( const auto s = NEWS<TYPE>() ) {
						s->SetServiceName( k );
						s->SetServicePrio( n );
						s->SetServiceClear( [=]{ DELETES( s ); } );
						s->Begin();
						return s;
					}
				}
			}
			return nullptr;
		}

		template<typename TYPE>static TYPE* Generate( const char* k, TYPE* s, UINT n = 0 ){

			if ( k ) {
				if ( !SRVCX::Search( k ) ) {
					s->SetServiceName( k );
					s->SetServicePrio( n );
					s->Begin();
					return s;
				}
			}
			return nullptr;
		}

		template<typename TYPE>static TYPE* Generate( const std::string& k,          UINT n = 0 ){ return Generate<TYPE>( k.c_str(),    n );	}
		template<typename TYPE>static TYPE* Generate( const std::string& k, TYPE* s, UINT n = 0 ){ return Generate<TYPE>( k.c_str(), s, n );	}

	public:
		template<typename TYPE>static TYPE* Search( const char* k        ){ return dynamic_cast<TYPE*>( SRVCX::Search( k ) );	}
		template<typename TYPE>static TYPE* Search( const std::string& k ){ return Search<TYPE>( k.c_str() );					}

		static SRVC* Search( const char* );

	public:
		static UINT Destroy( const char* );
		static UINT Destroy( const std::string& );
		static UINT Destroy( void );

	public:
		template<typename TYPE>static void Debug( const char* k ){ if ( const auto s = Search<TYPE>( k ) ) { s->Debug(); }	}
		template<typename TYPE>static void Debug( const std::string& k ){ Debug<TYPE>( k.c_str() );	}
	};
}

// End Of File
