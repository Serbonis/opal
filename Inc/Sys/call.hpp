#pragma once
//========================================
// OPAL Game Library
//========================================
#include "xcall.hpp"

//========================================
// サウンドシステム
//========================================
namespace opal {
	class ZOBJ;
	class CALLT;

	class CALLX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class CALLT;

	private:
		static LINX*	index;
		static LINX*	linx;
		static ZOBJ*	parent;

	private:
		static bool SysInit(  void );
		static bool SysFree(  void );

		static bool SysTaskInit(  void );
		static bool SysTaskEnter( void );
		static bool SysTaskMain(  void );
		static bool SysTaskExit(  void );
		static bool SysTaskFree(  void );

	public:
		static void  Linx( LINX* );
		static void  Linx( LINX& );
		static void  Linx( std::shared_ptr<LINX> );
		static LINX* Linx( void  );

		static void  Parent( ZOBJ* );
		static void  Parent( ZOBJ& );
		static void  Parent( std::shared_ptr<ZOBJ> );
		static ZOBJ* Parent( void );

#ifdef OPAL_DEBUG
	private:
		static int		debug;

	public:
		static void Debug( int  );
		static int  Debug( void );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "callx.hpp"

// End Of File
