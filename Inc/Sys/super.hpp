#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// スーパーシステム
//========================================
namespace opal {
	class ZOBJ;
	class SUPERT;

	class SUPERX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class SUPERT;

	public:
		using LMAP = std::unordered_map<std::string,LINX*>;

	private:
		static LMAP		linx;
		static LINX*	index;

	private:
		static bool SysInit( void );
		static bool SysFree( void );

	public:
		static LMAP* Linx( void );
		static LINX* Linx( const char* );

	public:
		static bool SysTaskInit(  void );
		static bool SysTaskEnter( void );
		static bool SysTaskMain(  void );
		static bool SysTaskExit(  void );
		static bool SysTaskFree(  void );

		static bool TaskInit(  LINX*, UINT = 0 );
		static bool TaskEnter( LINX*, UINT = 0 );
		static bool TaskMain(  LINX*, UINT = 0 );
		static bool TaskExit(  LINX*, UINT = 0 );
		static bool TaskFree(  LINX*, UINT = 0 );

	public:
		using POBJ = ZOBJ;
		using COBJ = ZOBJ;
		using FUNC = std::function<void(COBJ*)>;
		using FMAP = std::unordered_map<COBJ*,FUNC>;

	private:
		using PMAP = std::unordered_map<const POBJ*, std::shared_ptr<FMAP>>;
		using CMAP = std::unordered_map<const COBJ*, const POBJ*>;

		static PMAP	pmap;
		static CMAP	cmap;

	public:
		static auto Register( const POBJ* )->const FMAP*;

		static void Register( COBJ*, const POBJ*, FUNC = nullptr );
		static void Release(  ZOBJ* );

	public:
		static UINT Parent(   void );
		static UINT Children( void );

		static bool Parent(   const POBJ* );
		static bool Children( const COBJ* );

#if OPAL_DEBUG
	private:
		static void Debug( UINT );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "superx.hpp"

// End Of File
