#pragma once
//========================================
// OPAL Game Library
//========================================
#include "thrd.hpp"

//========================================
// スレッドタスク
//========================================
namespace opal {
	class THRDT : public TASK {
		friend class THRDX;

	public:
		explicit THRDT();
		virtual ~THRDT();

	protected:
		HANDLE							hand;	// スレッドハンドル
		UINT							step;	// 内部ステップ
		void*							argv;	// 拡張データ
		size_t							size;	// データサイズ
		bool							sect;	// クリティカルセクション
		UINT							thid;	// スレッドID
		DWORD							code;	// スレッドの終了コード
		std::function<void*(THRDT*)>	func;	// スレッドで実行する関数用ワーク
		UINT							level;	// 実行レベル

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual HANDLE Handle( void ) const;
		virtual void*  Argv(   void ) const;

	public:
		virtual void Level( UINT );
		virtual UINT Level( void ) const;

	protected:
		virtual void async( void );
		virtual void kill(  void );

	private:
		virtual void TaskMain( void ) override;

	private:
		static UINT CALLBACK process( void* );
	};
}

// End Of File
