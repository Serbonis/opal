#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

#ifdef _MSC_VER
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma warning( disable:4996 )
#endif

//========================================
// ネットワークシステム
//========================================
namespace opal {
	class NETWI;
	class NETWT;
	class NETWT_S;
	class NETWT_C;
	using NETWM_S = MTASK<NETWI,NETWT_S>;
	using NETWM_C = MTASK<NETWI,NETWT_C>;

	class NETWX final : private NOINSTANCE {
		friend class NETWZ;

		friend class SYSTEM;
		friend class DEBUGX;

		friend class NETWT;
		friend class NETWT_S;
		friend class NETWT_C;

	private:
		enum TYPE { SERVER,CLIENT,MAX, };
		enum {
			WINSOCK_MAJOR_VER	= 2,
			WINSOCK_MINOR_VER	= 2,
		};
		enum {
			HOSTNAME_SIZE	= 0x100,
			HOSTADRS_SIZE	= 0x100,
		};

	private:
		static NETWM_S			info_s;
		static NETWM_C			info_c;
		static WORD				version;
		static WSADATA			wsadata;
		static char				hostname[];
		static char				hostadrs[];
		static HOSTENT*			hostinfo;
		static IN_ADDR			ipadrs;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static NETWI* Server( const char*, USHORT, int = 0 );
		static NETWI* Client( const char*, USHORT );

	private:
		static UINT Size(  UINT );
		static UINT SizeS( void );
		static UINT SizeC( void );

	private:
		static NETWT*   Task( UINT, const char* );
		static NETWT_S* TaskS(      const char* );
		static NETWT_C* TaskC(      const char* );

	private:
		static int Kill( UINT, const char* = nullptr );
		static int KillS(      const char* = nullptr );
		static int KillC(      const char* = nullptr );

	public:
		static int Sync( UINT, const char* = nullptr );
		static int SyncS(      const char* = nullptr );
		static int SyncC(      const char* = nullptr );

	private:
		static const char* Error( int );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "netwx.hpp"
#include "netwi.hpp"

// End Of File
