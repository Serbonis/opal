#pragma once
//========================================
// OPAL Game Library
//========================================
#include "file.hpp"

//========================================
// ファイルハンドル
//========================================
namespace opal {
	class FILEH {
	public:
		explicit FILEH( const char* = nullptr );
		virtual ~FILEH();

	protected:
		NAME	name;
		bool	flag;
		char*	buff;
		size_t	size;
		size_t	head;
		size_t	foot;
		UINT	seek;

	public:
		void Name( const char* );
		const char* Name( void ) const;

		void   Buff( void*, size_t = 0, size_t = 0, size_t = 0 );
		void*  Buff( void ) const;
		size_t Size( void ) const;
		size_t Head( void ) const;
		size_t Foot( void ) const;

		void Seek( UINT );
		UINT Seek( void ) const;

	public:
		int  print( void );
		int  print( const char*, ... );
		void flush( void );
	};
}

// End Of File
