#pragma once
//========================================
// OPAL Game Library
//========================================
#include "rsrc.hpp"
#include "rsrcx.hpp"

#include "file.hpp"

//========================================
// リソースジェネレーター
//========================================
namespace opal {
	class RSRCS final : private NOINSTANCE {
		//----------------------------------------
		// ユーザーワークタイプ
		//----------------------------------------
	public:
		template<typename TT, typename... AT>static auto SharedRsrc( const std::string_view n, const std::string_view t, const void* p, AT&&... a )->std::shared_ptr<TT>{ return { NewInit<TT>( n, t, p, std::forward<AT>( a )... ), [](TT* p){ FreeDelete(p); } }; }
		template<typename TT, typename... AT>static auto SharedRsrc( const std::string_view n,                           const void* p, AT&&... a )->std::shared_ptr<TT>{ return { NewInit<TT>( n,    p, std::forward<AT>( a )... ), [](TT* p){ FreeDelete(p); } }; }
		template<typename TT, typename... AT>static auto SharedRsrc( const FILED* d,                                     const void* p, AT&&... a )->std::shared_ptr<TT>{ return { NewInit<TT>( d,    p, std::forward<AT>( a )... ), [](TT* p){ FreeDelete(p); } }; }
		template<typename TT, typename... AT>static auto SharedRsrc(                                                                    AT&&... a )->std::shared_ptr<TT>{ return { NewInit<TT>(          std::forward<AT>( a )... ), [](TT* p){ FreeDelete(p); } }; }

		//----------------------------------------
		// New/Init - Free/Delete
		//----------------------------------------
	private:
		template<typename TT, typename... AT>static TT* NewInit( const std::string_view n, const std::string_view t, const void* p, AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( n, t, p ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit( const std::string_view n, const std::string_view t,                AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( n, t    ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit( const std::string_view n,                           const void* p, AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( n,    p ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit( const std::string_view n,                                          AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( n       ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit( const FILED* d,                                     const void* p, AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( d,    p ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit( const FILED* d,                                                    AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init( d       ); return p; } return nullptr; }
		template<typename TT, typename... AT>static TT* NewInit(                                                                    AT&&... a ){ if ( const auto p = NEWS<TT>( std::forward<AT>( a )... ) ) { p->Init(         ); return p; } return nullptr; }

		template<typename TT>static void FreeDelete( TT* p ){ if ( p ) { p->Free(); DELETES( p ); } }

		//----------------------------------------
		// ジェネレーター
		//----------------------------------------
	public:
		template<typename TT, typename... AT>static auto Generate( const std::string_view n, AT&&... a )->std::shared_ptr<TT>{

			if ( const auto r = SharedRsrc<TT>( n, std::forward<AT>( a )... ) ) {
				RSRCX::Register( std::string( n ), r );
				return r;
			}
			return nullptr;
		}

		template<typename TT, typename... AT>static auto Generate( const FILED* d, AT&&... a )->std::shared_ptr<TT>{

			if ( d ) {
				if ( const auto r = SharedRsrc<TT>( d, std::forward<AT>( a )... ) ) {
					RSRCX::Register( std::string( d->Name() ), r );
					return r;
				}
			}
			return nullptr;
		}
		template<typename TT, typename... AT>static auto Generate( const FILED& d, AT&&... a )->std::shared_ptr<TT>{

			return Generate<TT>( &d, std::forward<AT>( a )... );
		}

		template<typename TT>static auto Search( const std::string_view n )->std::shared_ptr<TT>{

			return std::dynamic_pointer_cast<TT>( RSRCX::Search( std::string( n ) ) );
		}
	};
}

// End Of File
