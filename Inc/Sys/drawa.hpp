#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// アニメ制御
//========================================
namespace opal {
	class DRAWA {
	protected:
		explicit DRAWA();
		virtual ~DRAWA();

	protected:
		enum {
			STAT_NULL 	= 0L,
			STAT_PLAY	= 1 << 0,
			STAT_BEGIN	= 1 << 1,
			STAT_SHIFT	= 1 << 2,
			STAT_END	= 1 << 3,
		};

	protected:
		UINT		anime_id;		// アニメ番号
		UINT		anime_step;		// 遷移状態ステップ
		UINT		anime_stat;		// アニメ状態
		bool		anime_play;		// アニメ進行
		float		anime_loop;		// ループ(0=無限)
		float		anime_time;		// ループ更新用
		float		anime_seek;		// アニメ位置
		float		anime_rate;		// 速度倍率

	public:
		virtual void InitAnime( void );
		virtual void FreeAnime( void );

	public:
		virtual void  SetAnimeID( UINT );
		virtual void  SetAnimePlay(  void );
		virtual void  SetAnimeStop(  void );
		virtual void  SetAnimePause( void );
		virtual void  SetAnimeLoop(  float = 0.0f );
		virtual void  SetAnimeTime(  float = 0.0f );
		virtual void  SetAnimeSeek(  float = 0.0f );
		virtual void  SetAnimeRate(  float = 1.0f );

	public:
		virtual void  AddAnimeTime(  float = 1.0f );
		virtual void  AddAnimeSeek(  float = 1.0f );
		virtual void  AddAnimeRate(  float = 1.0f );

	public:
		virtual UINT  GetAnimeID(    void ) const;
		virtual bool  GetAnimePlay(  void ) const;
		virtual bool  GetAnimeStop(  void ) const;
		virtual bool  GetAnimePause( void ) const;
		virtual float GetAnimeLoop(  void ) const;
		virtual float GetAnimeTime(  void ) const;
		virtual float GetAnimeSeek(  void ) const;
		virtual float GetAnimeRate(  void ) const;

	public:
		virtual bool AnimeStatPlay(  void ) const;
		virtual bool AnimeStatStop(  void ) const;
		virtual bool AnimeStatBegin( void ) const;
		virtual bool AnimeStatShift( void ) const;
		virtual bool AnimeStatEnd(   void ) const;

	public:
		virtual float& AnimeTime( void );
	};
}

// End Of File
