#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// ��������
//========================================
namespace opal {
	extern long long Seed( void );
	extern void  Srand( UINT = 0 );
	extern UINT  Urand( void );
	extern int   Irand( void );
	extern float Frand( void );
	extern UINT  Urand( UINT,  UINT  =  0 );
	extern int   Irand( int,   int   =  0 );
	extern float Frand( float, float = .0 );

	template<UINT N>float Arand( void ){

		auto	r = 0.0f;

		for ( auto i = 0UL; i < N; i++ ) {
			r += Frand();
		}

		return ( float )r/N;
	}

	template<UINT N>float Arand( float a, float b = .0 ){

		auto	r = 0.0f;

		for ( auto i = 0UL; i < N; i++ ) {
			r += Frand( a, b );
		}

		return ( float )r/N;
	}

	extern void Vrand( std::vector<int>& );
	extern void Vrand( std::vector<int>&, int );
	extern auto Vrand( int )->std::vector<int>;

	extern UINT Drand( const std::vector<double>& );
}

// End Of File
