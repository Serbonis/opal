#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// ファイルシステム
//========================================
namespace opal {
	class FILEI;
	class FILEH;
	class FILET;
	using FILEF = std::function<void*(FILEI*)>;
	using FILEM = MTASK<FILEI,FILET>;

	class FILEX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class FILET;

	private:
		enum {
			READ,
			WRITE,
			MAX,
		};

	public:
		enum {
			NORMAL,	// アドレスをそのまま使う
			MANAGE,	// 領域は他で管理されている
			SYSTEM,	// 領域はシステムを最初から使う
			APPEND,	// 領域はシステムを続きから使う
			BUFFER,	// 領域は内部で一時的に用意する
			SYNCRW,	// 読み書きを待機する
			CALLBK,	// コールバックのみ
		};

	private:
		static FILEM				info[MAX];	// ファイル情報
		static UINT					mode;		// デフォルトモード
		static NAME					path;		// デフォルトパス
		static std::stack<NAME*>*	list;		// パスリスト
		static INT64*				base;		// デフォルトアドレス
		static INT64*				adrs;		// 読み込みアドレス
		static UINT					size;		// 読み込んだサイズ
		static FILEI*				file;		// 最後に読み込んだファイル情報

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static void Mode( UINT );
		static UINT Mode( void );

	public:
		static void Path( const char* );
		static void Path( const std::string& );
		static void Path( std::nullptr_t );
		static const char* Path( void );

	public:
		static void Push( const char* = nullptr );
		static void Pop( void );
		static void Flush( void );

	public:
		static void  Base( void* );
		static void* Base( void  );

	public:
		static const FILEI* Read(  const char*,        FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Load(  const char*,        FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Read(  const char*, void*, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Load(  const char*, void*, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Read(  const char*, UINT,  FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Load(  const char*, UINT,  FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Read(  const FILEH*,       FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Load(  const FILEH*,       FILEF = nullptr, UINT = 0, UINT = 0 );

		static const FILEI* Read(  const char*, const char*, const char* = nullptr );
		static const FILEI* Load(  const char*, const char*, const char* = nullptr );

	public:
		static const FILEI* Write( const char*,              size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Store( const char*,              size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Write( const char*, const void*, size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Store( const char*, const void*, size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Write( const char*, UINT,        size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Store( const char*, UINT,        size_t, FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Write( const FILEH*,                     FILEF = nullptr, UINT = 0, UINT = 0 );
		static const FILEI* Store( const FILEH*,                     FILEF = nullptr, UINT = 0, UINT = 0 );

	private:
		static const FILEI* TaskSync(  UINT, const char* );
		static const FILEI* TaskASync( UINT, const char*, UINT, void*, size_t, FILEF, UINT, UINT );

	private:
		static UINT Size(  UINT );
		static UINT SizeR( void );
		static UINT SizeW( void );

	private:
		static FILET* Task( UINT, const char* );
		static FILET* TaskR(      const char* );
		static FILET* TaskW(      const char* );

	private:
		static int Kill( UINT, const char* = nullptr );
		static int KillR(      const char* = nullptr );
		static int KillW(      const char* = nullptr );

	public:
		static int Sync( UINT, const char* = nullptr );
		static int SyncR(      const char* = nullptr );
		static int SyncW(      const char* = nullptr );

	public:
		static int Time( UINT, const char* = nullptr );
		static int TimeR(      const char* = nullptr );
		static int TimeW(      const char* = nullptr );

	public:
		static const FILEI* File( const char* = nullptr );
		static void*        Adrs( const char* = nullptr );
		static UINT         Size( const char* = nullptr );

	public:
		static void Clear( const char*, UINT = READ );
		static void Clear( UINT );
		static void Clear( void );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "filex.hpp"
#include "filei.hpp"
#include "fileh.hpp"
#include "filen.hpp"

// End Of File
