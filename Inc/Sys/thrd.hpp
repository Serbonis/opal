#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// スレッドシステム
//========================================
namespace opal {
	class THRDT;

	class THRDX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;
		friend class THRDT;

	private:
		static LINX										index;
		static std::unordered_map<std::string,THRDT*>	tmap;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static THRDT* Throw( const char*, std::function<void*(THRDT*)>, void* = nullptr, size_t = 0 );
		static THRDT* Throw( const void*, std::function<void*(THRDT*)>, void* = nullptr, size_t = 0 );
		static THRDT* Throw( UINT,        std::function<void*(THRDT*)>, void* = nullptr, size_t = 0 );

	public:
		static std::size_t Task( void );

		static THRDT* Task( const char* );
		static THRDT* Task( const void* );
		static THRDT* Task( UINT        );

	public:
		static bool Kill( const char*, UINT = 0 );
		static bool Kill( const void*, UINT = 0 );
		static bool Kill( UINT,        UINT = 0 );

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "thrdx.hpp"

// End Of File
