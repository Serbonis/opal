#pragma once
//========================================
// OPAL Game Library
//========================================
#include "drawx.hpp"

//========================================
// グラフィックタスク(3D)
//========================================
namespace opal {
	class DRAWT3 : public DRAWT {
		friend class DRAWX;
		friend class DRAWT;

	public:
		explicit DRAWT3();
		virtual ~DRAWT3();

	public:
		virtual void Init( const char* = nullptr ) override;
		virtual void Free( void ) override;

	public:
		virtual void DrawInfo( void ) const override;
		virtual void DrawInfo( int  ) const override;
	};
}

// End Of File
