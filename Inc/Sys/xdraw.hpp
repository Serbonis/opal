#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// グラフィックシステム
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal {
	namespace DRAWX_ {
		extern void SysInit( void );
		extern void SysExec( void );
		extern void SysFree( void );

		extern void SysInitAdapter( void );
		extern void SysFreeAdapter( void );

		extern void SysInitDevice( HWND, UINT, UINT );
		extern void SysFreeDevice( void );

		extern void SysInitMonitor( void );
		extern void SysFreeMonitor( void );

		extern void SysInitViewport( void );
		extern void SysFreeViewport( void );

		extern void SysInitClearbuffer( void );
		extern void SysFreeClearbuffer( void );

		extern void SysBegin( void );
		extern void SysEnd(   void );
		extern void Present(  void );

		extern void Capture( const char* );
	}
}

//----------------------------------------
// システムインスタンス
//----------------------------------------
namespace opal {
	class CAMERAS;
	class LIGHTS;
	class FOGS;

	namespace DRAWX_ {
		extern void InitCamera( CAMERAS* );
		extern void ExecCamera( CAMERAS* );
		extern void FreeCamera( CAMERAS* );

		extern void InitLight( LIGHTS* );
		extern void ExecLight( LIGHTS* );
		extern void FreeLight( LIGHTS* );

		extern void InitFog( FOGS* );
		extern void ExecFog( FOGS* );
		extern void FreeFog( FOGS* );
	}
}

// End Of File
