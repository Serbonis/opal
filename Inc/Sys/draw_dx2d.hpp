#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

#include <d2d1.h>
#include <dwrite.h>

//========================================
// グラフィックシステム
//========================================
//----------------------------------------
// Direct2D & DirectWrite
//----------------------------------------
namespace opal::DRAWX_DX2D {
	class DEVICE {
	public:
		explicit DEVICE();
		virtual ~DEVICE();

	private:
		IDXGISurface* 		surface;
		ID2D1RenderTarget*	render;

	private:
		IDWriteFactory*		write;

	public:
		virtual void Init( IDXGISwapChain*&	);
		virtual void Free( void );

	public:
		virtual void Begin( void );
		virtual void End(   void );

	public:
		virtual ID2D1RenderTarget*&	Render( void );
		virtual IDWriteFactory*&	Write(  void );
	};
}

//----------------------------------------
// ワーク
//----------------------------------------
namespace opal::DRAWX_DX2D {
	extern DEVICE	device;
}

// End Of File
