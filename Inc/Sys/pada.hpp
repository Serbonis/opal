#pragma once
//========================================
// OPAL Game Library
//========================================
#include "pad.hpp"

//========================================
// 入力アサイン
//========================================
namespace opal {
	class PADA {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		explicit PADA();
		explicit PADA( UINT, UINT );
		virtual ~PADA() = default;

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		UINT	keyboard;
		UINT	joypad;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		void SetKeyboard( UINT );
		void SetJoypad(   UINT );
		void SetJoyKey(   UINT, UINT );

	public:
		UINT GetKeyboard( void ) const;
		UINT GetJoypad(   void ) const;
	};
}

// End Of File
