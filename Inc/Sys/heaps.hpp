#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"
#include "new.hpp"

//========================================
// HEAP管理
//========================================
//----------------------------------------
// システム
//----------------------------------------
namespace opal {
	struct HEAP_SYSTEM final {
		HEAP_SYSTEM();
		~HEAP_SYSTEM();

		static UINT Counter( void );
	};
}

//----------------------------------------
// コア
//----------------------------------------
#include "heapx.hpp"

namespace opal {
	class HEAP_CORE final {
		friend class HEAP;

		friend void* New( void*, const char*, size_t, size_t );
		friend void  Del( const void* );

		HEAP_CORE();
		~HEAP_CORE();

		HEAP_HASH	hash;	// 履歴保存
		size_t		size;	// 合計サイズ
		UINT		count;	// カウンタ

		void Init( void );
		void Free( void );

		void New( const void*, const char*, size_t, size_t );
		void Del( const void* );
	};
}

// End Of File
