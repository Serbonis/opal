#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"

//========================================
// リソースシステム
//========================================
namespace opal {
	class RSRC;
	class RSRCX final : private NOINSTANCE {
		friend class SYSTEM;
		friend class DEBUGX;

	private:
		static std::unordered_map<std::string,std::shared_ptr<RSRC>>	rsrc;

	private:
		static bool SysInit( void );
		static bool SysExec( void );
		static bool SysFree( void );

	public:
		static void Register( const std::string&, std::shared_ptr<RSRC> );
		static void Release( const std::string& );
		static void Release( void );
		static auto Search( const std::string& )->std::shared_ptr<RSRC>;

#ifdef OPAL_DEBUG
	private:
		static void Debug( UINT = 0 );
#endif
	};
}

//========================================
// サブシステム
//========================================
#include "rsrcx.hpp"
#include "rsrcs.hpp"
#include "rsrcb.hpp"

// End Of File
