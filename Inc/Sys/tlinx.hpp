#pragma once
//========================================
// OPAL Game Library
//========================================
#include "Std/linx.hpp"

//========================================
// 専用リンク
//========================================
namespace opal {
	template<typename TYPE>class TLINX : public LINX {
	protected:
		bool	pause;

	private:
		virtual void LinxFuncBegin( void ) override final {

			const auto	t = Task<TYPE>();
			const auto	l = t ? t->GetLevel() : 0;

			LinxFuncBegin( pause = SIGX::PauseCheck( l ) ? false : true );
		}
		virtual void LinxFuncEnd(   void ) override final { LinxFuncEnd(   pause );	}
		virtual void LinxMainBegin( void ) override final { LinxMainBegin( pause );	}
		virtual void LinxMainEnd(   void ) override final { LinxMainEnd(   pause );	}

	public:
		virtual void LinxFuncBegin( UINT ){}
		virtual void LinxFuncEnd(   UINT ){}
		virtual void LinxMainBegin( UINT ){}
		virtual void LinxMainEnd(   UINT ){}

	public:
		virtual void SetLinxInfo( UINT v = 1 ){ LINX::Info( v );		}
		virtual UINT GetLinxInfo( void ) const { return LINX::Info();	}

	public:
		virtual void SetLinxName( const char* p = nullptr ){ LINX::Name( p );	}
		virtual const char* GetLinxName( void ) const{ return LINX::Name();		}

	public:
		static TYPE TObj( void );
	};
}

//========================================
// 専用リンク付きタスク
//========================================
namespace opal {
	template<typename T_TYPE, typename L_TYPE>class TASKL : public T_TYPE, public L_TYPE {
	public:
		virtual void Init( const char* p = nullptr ) override { T_TYPE::Init( p ); 	T_TYPE::Branch( this );	}
		virtual void Free( void                    ) override { L_TYPE::TaskKill();	T_TYPE::Free();			}

	public:
		virtual void Register( L_TYPE* l = nullptr       ){ T_TYPE::Register( l );	}
		virtual void Release(  L_TYPE* l = nullptr       ){ T_TYPE::Release(  l );	}
		virtual void Register( std::shared_ptr<L_TYPE> l ){ T_TYPE::Register( l );	}
		virtual void Release(  std::shared_ptr<L_TYPE> l ){ T_TYPE::Release(  l );	}
	};
}

//========================================
// リンクのタスク
//========================================
namespace opal {
	template<typename TYPE>TYPE* LinxTask( TYPE* p ){

		if ( const LINX* l = p->Linx() ) {
			return l->Task<TYPE>();
		}
		return nullptr;
	}
}

//========================================
// Open/Closeインターフェース
//========================================
namespace opal {
	template<typename TYPE>class ILINX {
	private:
		virtual void Open_( const std::string_view, TYPE* ) = 0;
		virtual void Close_(                        TYPE* ) = 0;

	public:
		virtual void Open( const std::string_view p                          ){ Open_( p,  nullptr );	}
		virtual void Open( const std::string_view p,                 TYPE* l ){ Open_( p,  l       );	}
		virtual void Open( const std::string_view p, std::shared_ptr<TYPE> l ){ Open_( p,  l.get() );	}
		virtual void Open( const std::string_view p, std::nullptr_t        l ){ Open_( p,  l       );	}
		virtual void Open(                                           TYPE* l ){ Open_( {}, l       );	}
		virtual void Open(                           std::shared_ptr<TYPE> l ){ Open_( {}, l.get() );	}
		virtual void Open(                                           void    ){ Open_( {}, nullptr );	}

		virtual void Close(                 TYPE* l ){ Close_( l       );	}
		virtual void Close( std::shared_ptr<TYPE> l ){ Close_( l.get() );	}
		virtual void Close(                 void    ){ Close_( nullptr );	}

	private:
		virtual void Register_( TYPE* ) = 0;
		virtual void Release_(  TYPE* ) = 0;

	public:
		virtual void Register( TYPE* l ){ Register_( l ); }
		virtual void Release(  TYPE* l ){ Release_(  l ); }
		virtual void Register( std::shared_ptr<TYPE> l ){ Register_( l.get() ); }
		virtual void Release(  std::shared_ptr<TYPE> l ){ Release_(  l.get() ); }
		virtual void Register( void ){ Register_( nullptr ); }
		virtual void Release(  void ){ Release_(  nullptr ); }
	};
}

// End Of File
