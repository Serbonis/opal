#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// サイズリソース
//========================================
//----------------------------------------
// SIZE1
//----------------------------------------
class SIZE1 : public opal::CSIZE1F {
public:
	using opal::CSIZE1F::CSIZE1F;
	explicit SIZE1( float );
	explicit SIZE1();
	virtual ~SIZE1();

protected:
	virtual void MulSize_( float );
	virtual void AddSize_( float );

public:
	virtual void MulSize(  float );
	virtual void MulSizeW( float );

public:
	virtual void MulSize(  const SIZE1* );
	virtual void MulSize(  const SIZE1& );
	virtual void MulSizeW( const SIZE1* );
	virtual void MulSizeW( const SIZE1& );

public:
	virtual void AddSize(  float );
	virtual void AddSizeW( float );

public:
	virtual void AddSize(  const SIZE1* );
	virtual void AddSize(  const SIZE1& );
	virtual void AddSizeW( const SIZE1* );
	virtual void AddSizeW( const SIZE1& );
};

//----------------------------------------
// SIZE2
//----------------------------------------
class SIZE2 : public opal::CSIZE2F {
public:
	using opal::CSIZE2F::CSIZE2F;
	explicit SIZE2( float, float );
	explicit SIZE2( float );
	explicit SIZE2();
	virtual ~SIZE2();

protected:
	virtual void MulSize_( float, float );
	virtual void AddSize_( float, float );

public:
	virtual void MulSize(  float, float );
	virtual void MulSize(  float );
	virtual void MulSizeW( float );
	virtual void MulSizeH( float );

public:
	virtual void MulSize(  const SIZE2* );
	virtual void MulSize(  const SIZE2& );
	virtual void MulSizeW( const SIZE2* );
	virtual void MulSizeW( const SIZE2& );
	virtual void MulSizeH( const SIZE2* );
	virtual void MulSizeH( const SIZE2& );

public:
	virtual void AddSize(  float, float );
	virtual void AddSize(  float );
	virtual void AddSizeW( float );
	virtual void AddSizeH( float );

public:
	virtual void AddSize(  const SIZE2* );
	virtual void AddSize(  const SIZE2& );
	virtual void AddSizeW( const SIZE2* );
	virtual void AddSizeW( const SIZE2& );
	virtual void AddSizeH( const SIZE2* );
	virtual void AddSizeH( const SIZE2& );
};

//----------------------------------------
// SIZE3
//----------------------------------------
class SIZE3 : public opal::CSIZE3F {
public:
	using opal::CSIZE3F::CSIZE3F;
	explicit SIZE3( float, float, float );
	explicit SIZE3( float );
	explicit SIZE3();
	virtual ~SIZE3();

protected:
	virtual void MulSize_( float, float, float );
	virtual void AddSize_( float, float, float );

public:
	virtual void MulSize(  float, float, float );
	virtual void MulSize(  float );
	virtual void MulSizeW( float );
	virtual void MulSizeH( float );
	virtual void MulSizeD( float );

public:
	virtual void MulSize(  const SIZE3* );
	virtual void MulSize(  const SIZE3& );
	virtual void MulSizeW( const SIZE3* );
	virtual void MulSizeW( const SIZE3& );
	virtual void MulSizeH( const SIZE3* );
	virtual void MulSizeH( const SIZE3& );
	virtual void MulSizeD( const SIZE3* );
	virtual void MulSizeD( const SIZE3& );

public:
	virtual void AddSize(  float, float, float );
	virtual void AddSize(  float );
	virtual void AddSizeW( float );
	virtual void AddSizeH( float );
	virtual void AddSizeD( float );

public:
	virtual void AddSize(  const SIZE3* );
	virtual void AddSize(  const SIZE3& );
	virtual void AddSizeW( const SIZE3* );
	virtual void AddSizeW( const SIZE3& );
	virtual void AddSizeH( const SIZE3* );
	virtual void AddSizeH( const SIZE3& );
	virtual void AddSizeD( const SIZE3* );
	virtual void AddSizeD( const SIZE3& );
};

// End Of File
