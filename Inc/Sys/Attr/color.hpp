#pragma once
//========================================
// OPAL Game Library
//========================================
#include "draw.hpp"

//========================================
// カラーリソース
//========================================
class COLORS : public opal::VBANKA<opal::COLOR> {
public:
	using COLOR	= opal::COLOR;
	using RGB	= opal::RGB;
	using HSV	= opal::HSV;

public:
	explicit COLORS();
	virtual ~COLORS();

public:
	virtual COLOR* Color( void ) const;
	virtual COLOR* Color( UINT ) const;

public:
	virtual void NowColor( UINT );
	virtual UINT NowColor( void ) const;

public:
	virtual void SizeColor( UINT );
	virtual UINT SizeColor( void ) const;

public:
	virtual void  SetColor_( COLOR );
	virtual COLOR GetColor_( void  ) const;

	virtual void  SetColor_( UINT, COLOR );
	virtual COLOR GetColor_( UINT ) const;

public:
	virtual void  SetColor( COLOR );
	virtual void  SetColor( UCHAR, UCHAR, UCHAR, UCHAR );
	virtual void  SetColor( UCHAR, UCHAR, UCHAR );
	virtual void  SetColor( RGB  );
	virtual void  SetColor( HSV  );

	virtual void  SetColor( UINT, COLOR );
	virtual void  SetColor( UINT, UCHAR, UCHAR, UCHAR, UCHAR );
	virtual void  SetColor( UINT, UCHAR, UCHAR, UCHAR, void* );
	virtual void  SetColor( UINT, RGB   );
	virtual void  SetColor( UINT, HSV   );

	virtual void  SetColor( void );

public:
	virtual COLOR GetColor( void ) const;
	virtual COLOR GetColor( UINT ) const;
	virtual operator COLOR( void ) const;

public:
	virtual void  SetColorR( UCHAR );
	virtual void  SetColorG( UCHAR );
	virtual void  SetColorB( UCHAR );
	virtual void  SetColorA( UCHAR );
	virtual void  AddColorR( short );
	virtual void  AddColorG( short );
	virtual void  AddColorB( short );
	virtual void  AddColorA( short );
	virtual UCHAR GetColorR( void ) const;
	virtual UCHAR GetColorG( void ) const;
	virtual UCHAR GetColorB( void ) const;
	virtual UCHAR GetColorA( void ) const;

	virtual void  SetColorR( UINT, UCHAR );
	virtual void  SetColorG( UINT, UCHAR );
	virtual void  SetColorB( UINT, UCHAR );
	virtual void  SetColorA( UINT, UCHAR );
	virtual void  AddColorR( UINT, short );
	virtual void  AddColorG( UINT, short );
	virtual void  AddColorB( UINT, short );
	virtual void  AddColorA( UINT, short );
	virtual UCHAR GetColorR( UINT ) const;
	virtual UCHAR GetColorG( UINT ) const;
	virtual UCHAR GetColorB( UINT ) const;
	virtual UCHAR GetColorA( UINT ) const;
};

// End Of File
