#pragma once
//========================================
// OPAL Game Library
//========================================
#include "sig.hpp"

//========================================
// シグナルレベル
//========================================
namespace opal {
	class SIGL {
		friend class SIGX;
		friend class DEBUGX;

	protected:
		explicit SIGL();
	public:
		virtual ~SIGL() = default;

	public:
		enum LEVEL {
			PRIMAL,
			FINAL,
			FORCE,
		};
		enum {
			LEVEL_INFINITE = ( UINT )-1,
		};

	private:
		UINT	level[3];

	public:
		virtual void SetLevel( UINT, LEVEL = PRIMAL );
		virtual void SetLevel( void );
		virtual UINT GetLevel( LEVEL) const;
		virtual UINT GetLevel( void ) const;

	public:
		template<typename TYPE>void Coordinate( const TYPE* p ) {

			const auto	cv = GetLevel( PRIMAL );
			const auto	pv = p ? p->GetLevel( FINAL ) : 0;

			SetLevel( ( pv > cv ) ? pv : cv, FINAL );
		}
	};
}

