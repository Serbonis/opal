#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// MSC
//========================================
#ifdef _MSC_VER
#if _MSC_VER < 1920					// yvals_core.h対策
#undef  _MSC_VER
#define _MSC_VER 1920
#endif
#endif

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS		// <strsafe.h> に警告が出るのを回避
#define _WIN32_DCOM					// CoInitializeEx用に <windows.h>の前に置く

// STL激遅い対策...
#undef	_ITERATOR_DEBUG_LEVEL
#define _ITERATOR_DEBUG_LEVEL	0
#endif

//----------------------------------------
// ヘッダー
//----------------------------------------
#ifdef _MSC_VER
#include <InitGuid.h>
#endif

//----------------------------------------
// ライブラリ
//----------------------------------------
#ifdef _MSC_VER
#pragma comment( lib, "winmm.lib"		)
#pragma comment( lib, "dxguid.lib"		)
#pragma comment( lib, "dinput8.lib"		)
#pragma comment( lib, "xinput.lib"		)
#pragma comment( lib, "d3d9.lib"		)
#ifdef OPAL_DEBUG
#pragma comment( lib, "d3dx9d.lib"		)
#else
#pragma comment( lib, "d3dx9.lib"		)
#endif
#pragma comment( lib, "d3d11.lib"		)
#pragma comment( lib, "d2d1.lib"		)
#pragma comment( lib, "dwrite.lib"		)
#pragma comment( lib, "dsound.lib"		)
#pragma comment( lib, "strmiids.lib"	)
#pragma comment( lib, "DxErr.lib"		)
#pragma comment( lib, "quartz.lib"		)
#pragma comment( lib, "WSock32.lib"		)
#pragma comment( lib, "imm32.lib"		)
#pragma comment( lib, "rpcrt4.lib"		)
#pragma comment( lib, "shlwapi.lib"     )
#pragma comment( lib, "ws2_32.lib"      )
#pragma comment( lib, "d3dcompiler.lib" )
#endif

//----------------------------------------
// DirectShowを使う場合は自前ビルドして用意する
//----------------------------------------
#ifdef _MSC_VER
#ifdef OPAL_DSHOW
#ifdef OPAL_DEBUG
#pragma comment( lib, "strmbasd.lib"    )
#else
#pragma comment( lib, "strmbase.lib"    )
#endif
#endif
#endif

//----------------------------------------
// 警告抑制
//----------------------------------------
#ifdef _MSC_VER
#pragma warning( disable:4005 )	// マクロが再定義されました
#pragma warning( disable:4060 ) // switch ステートメントに ' case ' または ' default ' ラベルが含まれていません
#pragma warning( disable:4061 )	// switch ステートメント内には、この列挙子のハンドラーがありません
#pragma warning( disable:4062 )	// 列挙子 '識別子'列挙型のスイッチは' in列挙' はハンドルされません
#pragma warning( disable:4065 )	// switch ステートメントに ' default ' が含まれていますが、' case ' ラベルがありません
#pragma warning( disable:4099 ) // クラスとして宣言されたオブジェクトが構造体として定義されています
#pragma warning( disable:4100 )	// 仮パラメーターが関数の本体で参照されていません。未参照のパラメーターは無視されます
#pragma warning( disable:4127 ) // 件式が定数です
#pragma warning( disable:4129 )	// 文字列中の '\'
#pragma warning( disable:4146 )	// 符号付きの値を代入する変数は、符号付き型にキャストしなければなりません。
#pragma warning( disable:4191 )	// 関数ポインターを使用するいくつかの操作は安全でないと見なされます
#pragma warning( disable:4201 )	// 非標準の拡張機能: 無名の構造体または共用体
#pragma warning( disable:4242 ) // A型からB型に変換しました。データが失われている可能性があります
#pragma warning( disable:4244 )	// 整数型が、より小さい整数の型に変換されています
#pragma warning( disable:4245 )	// signed/unsigned が一致しません
#pragma warning( disable:4250 )	// 2つ以上のメンバーが同じ名前を持っています。
#pragma warning( disable:4263 )	// クラスの関数定義の名前が基本クラスの仮想関数と同じ名前ですが、引数の数または型が異なっています
#pragma warning( disable:4264 )	// 仮想メンバー関数用に基本クラス 'class' から使用可能なオーバーライドはありません
#pragma warning( disable:4265 )	// クラスは仮想関数を含んでいますが、デストラクターは仮想デストラクターではありません
#pragma warning( disable:4266 )	// 派生クラスは、仮想関数のオーバーロードはすべてオーバーライドしませんでした
#pragma warning( disable:4267 )	// size_t からサイズの小さな型への変換を検出しました
#pragma warning( disable:4312 )	// より大きいサイズへ変換します(符号拡張が発生する場合)
#pragma warning( disable:4355 ) // 'this' : ベース メンバー初期化リストで使用されました
#pragma warning( disable:4324 ) // アラインメント指定子のために構造体がパッドされました
#pragma warning( disable:4365 )	// signed/unsigned が一致しません
#pragma warning( disable:4371 ) // std::pair パッキングの改善のために、前バージョンのコンパイラからクラスのレイアウト変更された可能性
#pragma warning( disable:4435 ) // class1' :/Vd2 下のオブジェクトのレイアウトは仮想ベース 'class2' により変更されます。
#pragma warning( disable:4456 ) // '*' を宣言すると、以前のローカル宣言が隠蔽されます
#pragma warning( disable:4457 )	// 宣言すると、関数パラメーターが隠蔽されます
#pragma warning( disable:4458 )	// 宣言すると、クラス メンバーが隠蔽されます
#pragma warning( disable:4459 )	// 宣言すると、グローバル宣言が隠蔽されます
#pragma warning( disable:4464 ) // 相対インクルード パスに '..' が含まれています
#pragma warning( disable:4464 ) // 相対インクルード パスに '..' が含まれています
#pragma warning( disable:4514 )	// 参照されていないインライン関数は削除されました
#pragma warning( disable:4571 ) // catch(...) の意味が Visual C++ 7.1 から変更されています。構造化例外 (SEH) はキャッチされません。
#pragma warning( disable:4574 ) // 'identifier' が '0' として定義されています。'#if identifier' を使用しますか?
#pragma warning( disable:4574 )	// 'identifier' が '0' として定義されています。'#if identifier' を使用しますか?
#pragma warning( disable:4576 ) // 初期化子が続くかっこで囲まれた型は、標準でない明示的な型の変換構文です
#pragma warning( disable:4619 )	// 無効化を試みた警告は存在しません
#pragma warning( disable:4623 )	// 既定のコンストラクターは暗黙的に削除済みとして定義されました
#pragma warning( disable:4625 )	// コピー コンストラクターは暗黙的に削除済みとして定義されました
#pragma warning( disable:4626 )	// 代入演算子は暗黙的に削除済みとして定義されました
#pragma warning( disable:4624 )	// デストラクターを暗黙的に削除済みとして定義しました
#pragma warning( disable:4661 )	// 明示的なテンプレートのインスタンス生成要求に対して適切な定義がありません
#pragma warning( disable:4668 )	// プリプロセッサ 定義されていないシンボルが使用されています。false として評価されます
#pragma warning( disable:4723 )	// 除算演算の2番目のオペランドは、コンパイル時に0と評価され、未定義の結果を提供します。
#pragma warning( disable:4702 ) // 制御が渡らないコードです
#pragma warning( disable:4706 )	// 条件式の比較値は、代入の結果になっています
#pragma warning( disable:4710 )	// 指定された関数がインライン展開の対象として選択されましたが、実際にはインライン展開されませんでした
#pragma warning( disable:4711 ) // 関数 'function' インライン展開を選択しました
#pragma warning( disable:4800 )	// bool値にintを突っ込む
#pragma warning( disable:4804 ) // 'bool' 操作で型の安全でない使用
#pragma warning( disable:4805 ) // 演算中のA型とB型の混用は安全ではありません
#pragma warning( disable:4820 )	// 要素の型と順序を基にして、コンパイラは構造体の終わりに文字を埋め込みました
#pragma warning( disable:4826 ) // A型からB型への変換は符号拡張されています。これは、予期しない実行時の動作を発生させる可能性があります
#pragma warning( disable:4838 ) // 変換が縮小変換が必要です
#pragma warning( disable:4917 )	// GUID はクラス、インターフェイスまたは名前空間のみに関連付けることができます
#pragma warning( disable:4927 )	// 変換が正しくありません。複数のユーザー定義の変換が暗黙的に適用されています
#pragma warning( disable:4946 )	// 関連する型どうしのキャストするために reinterpret_cast を使用しないでください
#pragma warning( disable:5026 )	// 移動コンストラクターが暗黙的に削除済みとして定義されました
#pragma warning( disable:5027 )	// 移動代入演算子が暗黙的に削除済みとして定義されました
#pragma warning( disable:5029 )	// 非標準の拡張機能: C++ のアラインメント属性は変数、データ メンバー、タグの種類にのみ適用されます
#endif

// End Of File
