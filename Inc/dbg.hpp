#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// デバッグプリント
//========================================
#ifdef OPAL_DEBUG
extern int printd( const char*, ... );

namespace opal {
	struct VECTOR2;
	struct VECTOR3;
	struct VECTOR4;
	struct MATRIX;
}
extern int  printd( const opal::VECTOR2* );
extern int  printd( const opal::VECTOR2& );
extern int  printd( const opal::VECTOR3* );
extern int  printd( const opal::VECTOR3& );
extern int  printd( const opal::VECTOR4* );
extern int  printd( const opal::VECTOR4& );
extern int  printd( const opal::MATRIX*  );
extern int  printd( const opal::MATRIX&  );
extern void printf( const opal::VECTOR2* );
extern void printf( const opal::VECTOR2& );
extern void printf( const opal::VECTOR3* );
extern void printf( const opal::VECTOR3& );
extern void printf( const opal::VECTOR4* );
extern void printf( const opal::VECTOR4& );
extern void printf( const opal::MATRIX*  );
extern void printf( const opal::MATRIX&  );

#define printm( m )	MessageBox( nullptr, m, _T( "DEBUG" ), MB_OK )

#else
#define Printf( ... )	( void )0
#define Printd( ... )	( void )0
#define Printm( m   )	( void )m
#endif

//========================================
// GetLastError
//========================================
#ifdef OPAL_DEBUG
namespace opal {
	inline const char* FMES( DWORD c, char* p, DWORD s ) {

		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
					   nullptr,
					   c ? c : GetLastError(),
					   MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
					   p, s,
					   nullptr );
		return p;
	}
}

namespace opal {
	inline const char* FMES( DWORD c ) {

		static TCHAR	buff[1024] = {};

		return FMES( c, buff, std::size( buff ) );
	}
}
#else
namespace opal {
	inline const char* FMES( DWORD c = 0L, char* p = nullptr, DWORD s = 0UL ) { return nullptr; }
}
#endif

//========================================
// HRESULT
//========================================
#ifdef OPAL_DEBUG
#ifdef _MSC_VER
#define HRES( r )	DXGetErrorString( r )	// DxErr.h
#else
#define HRES( r )	DXGetErrorString9( r )	// dxerr9.h
#endif
#else
#define HRES( r )	opal::FMES( r )
#endif

// End Of File
