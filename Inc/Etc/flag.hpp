#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// フラグ操作
//========================================
namespace opal {
	enum {
		FLAG_NULL 	= -2,	// 何もしない
		FLAG_FLIP	= -1,	// 反転する
		FLAG_FALSE	=  0,	// false にする
		FLAG_TRUE	= +1,	// true  にする
	};

	enum {
		FLAG_SUPER_NULL,	// 親に従う
		FLAG_SUPER_FREE,	// 親を無視する
		FLAG_SUPER_AND,		// 両方とも
		FLAG_SUPER_OR,		// どちらか
		FLAG_SUPER_XOR,		// 排他的
		FLAG_SUPER_C_TRUE,	// true の時は親を無視、falseの時は親に従う
		FLAG_SUPER_C_FALSE,	// flaseの時は親を無視、true の時は親に従う
		FLAG_SUPER_P_TRUE,	// 親がtrue なら従う、falseの時は無視する
		FLAG_SUPER_P_FALSE,	// 親がfalseなら従う、true の時は無視する

		FLAG_SUPER_PARENT	= FLAG_SUPER_NULL,
		FLAG_SUPER_CHILD	= FLAG_SUPER_FREE,
	};

	template<typename TYPE>TYPE FLAG_CTRL( TYPE& f, int c ){

		if ( c > FLAG_NULL ) {
			switch ( c ) {
			case FLAG_FLIP:	f = !f;		break;
			case FLAG_FALSE:f = false;	break;
			case FLAG_TRUE:	f = true;	break;
			default:		f = c;		break;
			}
		}
		return f;
	}

	template<typename TYPE>void FLAG_CLR( TYPE& f         ) { f  =  0;	}
	template<typename TYPE>void FLAG_SUB( TYPE& f, UINT b ) { f &= ~b;	}
	template<typename TYPE>void FLAG_ADD( TYPE& f, UINT b ) { f |=  b;	}
	template<typename TYPE>bool FLAG_GET( TYPE& f, UINT b ) { return ( f&b ) ? true : false;	}
	template<typename TYPE>void FLAG_SET( TYPE& f, UINT b, bool c = true ) { ( c ) ? FLAG_ADD( f, b ) : FLAG_SUB( f, b );	}
	template<typename TYPE>TYPE FLAG_AND( TYPE& f, UINT b ) { return f&b;	}
	template<typename TYPE>bool FLAG_EQU( TYPE& f, UINT b ) { return ( FLAG_AND( f, b ) == b ) ? true : false;	}

	inline bool FLAG_COORDINATE( int s, bool c, bool p ){

		switch ( s ) {
		case FLAG_SUPER_NULL:		return p;				// 親に従う
		case FLAG_SUPER_FREE:		return c;				// 親を無視する
		case FLAG_SUPER_AND:		return ( c && p );		// 両方とも
		case FLAG_SUPER_OR:			return ( c || p );		// どちらか
		case FLAG_SUPER_XOR:		return ( c ^  p );		// 排他的
		case FLAG_SUPER_C_TRUE:		return (  c ) ? c : p;	// true の時は親を無視、falseの時は親に従う
		case FLAG_SUPER_C_FALSE:	return ( !c ) ? c : p;	// flaseの時は親を無視、true の時は親に従う
		case FLAG_SUPER_P_TRUE:		return (  p ) ? p : c;	// 親がtrue なら従う、falseの時は無視する
		case FLAG_SUPER_P_FALSE:	return ( !p ) ? p : c;	// 親がfalseなら従う、true の時は無視する
		}
		return false;
	}

	template<typename TYPE>TYPE* POINTER_COORDINATE( int s, TYPE* c, TYPE* p ){

		switch ( s ) {
		case FLAG_SUPER_NULL:		return p;				// 親に従う
		case FLAG_SUPER_FREE:		return c;				// 親を無視する
		case FLAG_SUPER_C_TRUE:		return (  c ) ? c : p;	// true の時は親を無視、falseの時は親に従う
		case FLAG_SUPER_C_FALSE:	return ( !c ) ? c : p;	// flaseの時は親を無視、true の時は親に従う
		case FLAG_SUPER_P_TRUE:		return (  p ) ? p : c;	// 親がtrue なら従う、falseの時は無視する
		case FLAG_SUPER_P_FALSE:	return ( !p ) ? p : c;	// 親がfalseなら従う、true の時は無視する
		}
		return nullptr;
	}

	template<typename TYPE>struct FLAG_SUPER {
		FLAG_SUPER(int s, TYPE v) : s{s              },v{v,v} {}
		FLAG_SUPER(       TYPE v) : s{FLAG_SUPER_NULL},v{v,v} {}
		FLAG_SUPER(             ) : s{FLAG_SUPER_NULL},v{   } {}

		int		s;
		TYPE	v[2];
	};
}

// End Of File
