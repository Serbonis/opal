#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// ���������_ EPSILON
//========================================
#ifdef _MSC_VER
#define __FLT_EPSILON__	FLT_EPSILON
#define __DBL_EPSILON__	DBL_EPSILON
#endif

namespace opal {
	constexpr float FZERO( void ){ return __FLT_EPSILON__;	}
}

namespace opal {
#ifdef _MSC_VER
	constexpr bool FZERO(  float a          ){ return ( a > 0 ) ? ( +a < __FLT_EPSILON__ ) : ( -a < __FLT_EPSILON__ );	}
	constexpr bool FNOTZ(  float a          ){ return !FZERO( a );	}
	constexpr bool FEQUAL( float a, float b ){ return  FZERO( a - b );	}
	constexpr bool FNOTEQ( float a, float b ){ return !FZERO( a - b );	}
#else
	constexpr bool FZERO(  float a          ){ return std::fabs( a ) < __FLT_EPSILON__;	}
	constexpr bool FNOTZ(  float a          ){ return !FZERO( a );	}
	constexpr bool FEQUAL( float a, float b ){ return std::fabs(a-b) < __FLT_EPSILON__;	}
	constexpr bool FNOTEQ( float a, float b ){ return std::fabs(a-b) > __FLT_EPSILON__;	}
#endif
	constexpr bool FGREAT( float a, float b = 0 ){ return ( a > b ) || FEQUAL( a, b );	}
	constexpr bool FLESS(  float a, float b = 0 ){ return ( a < b ) || FEQUAL( a, b );	}

	constexpr bool FEQU( float a, float b     ){ return FEQUAL( a,b );	}
	constexpr bool FNEQ( float a, float b     ){ return FNOTEQ( a,b );	}
	constexpr bool FGRT( float a, float b = 0 ){ return FGREAT( a,b );	}
	constexpr bool FLSS( float a, float b = 0 ){ return FLESS(  a,b );	}

	constexpr bool FRTT( float a, float n, float m ){ return n < a        && a < m;			}
	constexpr bool FRTE( float a, float n, float m ){ return n < a        && FLSS( a, m );	}
	constexpr bool FRET( float a, float n, float m ){ return FGRT( a, n ) && a < m;			}
	constexpr bool FREE( float a, float n, float m ){ return FGRT( a, n ) && FLSS( a, m );	}
}

// End Of File
