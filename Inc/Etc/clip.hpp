#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// �͈̓N���b�v
//========================================
namespace opal {
	template<typename TYPE>class CLIP {
	public:
		explicit CLIP( TYPE _n, TYPE _f ) : n( _n ),f( _f ) {}
		explicit CLIP(){}
		virtual ~CLIP(){}

	private:
		TYPE	n;
		TYPE	f;

	public:
		void Near( TYPE _n ){ n = _n; }
		void Far(  TYPE _f ){ f = _f; }
		TYPE Near( void ) const { return n; }
		TYPE Far(  void ) const { return f; }

		TYPE length( void ) { return f - n; }

	public:
		void N( TYPE _n ){ n = _n; }
		void F( TYPE _f ){ f = _f; }
		TYPE N( void ) const { return n; }
		TYPE F( void ) const { return f; }

		operator TYPE() const { return f - n; }
	};

	using CLIPF	= CLIP<float>;
	using CLIPI	= CLIP<int>;
	using CLIPU	= CLIP<UINT>;
}

// End Of File
