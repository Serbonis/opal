#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// COM
//========================================
namespace opal {
	template<typename TYPE> inline UINT REFCOUNT( TYPE*& p ){

		if ( p ) {
			const auto n = p->AddRef();

			p->Release();

			return n-1;
		}
		return 0;
	}

	template<typename TYPE> inline UINT REGISTER( TYPE*& p ){

		if ( p ) {
			return p->AddRef();
		}
		return 0;
	}

	template<typename TYPE> inline UINT REGISTER( TYPE*& p, std::function<void(void)> f ){

		if ( p ) {
			return p->AddRef();
		} else if ( f ) {
			f();
			return 1;
		}
		return 0;
	}

	template<typename TYPE> inline UINT REGISTER( TYPE*& p, std::function<void(TYPE*&)> f ){

		if ( p ) {
			return p->AddRef();
		} else if ( f ) {
			f( p );
			return 1;
		}
		return 0;
	}

	template<typename TYPE> inline UINT RELEASE( TYPE*& p ){

		if ( p ) {
			if ( const auto n = p->Release() ) {
				return n;
			}
			p = nullptr;
		}
		return 0;
	}

	template<typename TYPE> inline UINT RELEASE( TYPE*& p, void* ){

		if ( p ) {
			const auto n = p->Release();
			p = nullptr;
			return n;
		}
		return 0;
	}

	template<typename TYPE> inline UINT RELEASE( TYPE*& p, std::function<void(void)> f ){

		if ( p ) {
			if ( const auto n = p->Release() ) {
				return n;
			} else if ( f ) {
				f();
			}
			p = nullptr;
		}
		return 0;
	}
}

// End Of File
