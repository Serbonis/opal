#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// �r�b�g���Z
//========================================
namespace opal {
	inline UINT BitMask(  UINT a, UINT b ){ return a & b; }

	inline void BitClear( UINT& a ){ a = 0;  }
	inline void BitClear( UINT& a, UINT b ){ a &= ~b; }
	inline void BitSet(   UINT& a, UINT b ){ a |=  b; }
	inline bool BitGet(   UINT  a, UINT b ){ return BitMask( a, b ) ? true : false; }
	inline bool BitEqual( UINT  a, UINT b ){ return BitMask( a, b ) == b; }
	inline bool BitZero(  UINT  a ){ return !a;  }

	inline UINT IndexToBit( UINT i ) { return 1 << i; }
	inline UINT BitToIndex( UINT b ) {

		if ( b ) {
			for ( auto i = 0UL; i < 32; i++ ) {
				if ( b&1 ) {
					return i;
				}
				b >>= 1;
			}
		}

		return static_cast<UINT>( -1 );
	}
}

// End Of File
