#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// キャスト
//========================================
//----------------------------------------
// FLOAT ⇔ DWORD
//----------------------------------------
namespace opal {
	inline DWORD FtoDW( FLOAT f ) { return *( ( DWORD* )&f ); }
	inline FLOAT DWtoF( DWORD d ) { return *( ( FLOAT* )&d ); }
}

//----------------------------------------
// BYTE -> int 抜き出し
//----------------------------------------
namespace opal {
	inline int B4INT( const BYTE* p ){ return p[3]<<24|p[2]<<16|p[1]<<8|p[0]; }
	inline int B2INT( const BYTE* p ){ return p[1]<< 8|p[0]; }
}

//----------------------------------------
// ポインター型同士でreinterpretを回避
//----------------------------------------
namespace opal {
	template<typename TYPE> inline TYPE pointer_cast(       void* p ){ return static_cast< TYPE >( p ); }
	template<typename TYPE> inline TYPE pointer_cast( const void* p ){ return static_cast< TYPE >( const_cast< void* >( p ) ); }
}

// End Of File
