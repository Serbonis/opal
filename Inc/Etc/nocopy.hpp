#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// �R�s�[�h�~
//========================================
namespace opal {
	//----------------------------------------
	// private �Ōp������
	//----------------------------------------
	class NOCOPY {
	protected:
		explicit NOCOPY() = default;
		virtual ~NOCOPY() = default;

	private:
		NOCOPY( const NOCOPY&  ) = delete;
		NOCOPY(       NOCOPY&& ) = delete;
		NOCOPY& operator =( const NOCOPY&  ) = delete;
		NOCOPY& operator =(       NOCOPY&& ) = delete;
	};
}

// End Of File
