#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// インスタンスを作らせない
//========================================
namespace opal {
	//----------------------------------------
	// private で継承する
	//----------------------------------------
	class NOINSTANCE {
		NOINSTANCE() = delete;
		NOINSTANCE( const NOINSTANCE&  ) = delete;
		NOINSTANCE(       NOINSTANCE&& ) = delete;
		NOINSTANCE& operator =( const NOINSTANCE&  ) = delete;
		NOINSTANCE& operator =(       NOINSTANCE&& ) = delete;
	};
}

// End Of File
