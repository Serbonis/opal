#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// 配列関連
//========================================
//----------------------------------------
// 配列の要素数と最大インデックス
//----------------------------------------
#ifndef ARRAYSIZE
#define ARRAYSIZE( a )	( ( size_t )( sizeof( a )/sizeof( a[0] ) ) )
#endif

//----------------------------------------
// 循環インデックス
//----------------------------------------
inline UINT INDEX( int n, size_t s ){

	if ( s ) {
		return (( n > 0 ) ? 0+n : s-n )%s;
	} else {
		return 0;
	}
}

//----------------------------------------
// 丸め込み
//----------------------------------------
template<typename TYPE>constexpr size_t ARRAYROUND( TYPE n, size_t s ){

	if ( n <  0 ) { return 0;	}
	if ( s <= n ) { return s-1;	}

	return n;
}

//----------------------------------------
// 文字列サイズ
//----------------------------------------
template<size_t N>constexpr size_t STRINGSIZE( const char (&s)[N] ) { return N-1;	}

// End Of File
