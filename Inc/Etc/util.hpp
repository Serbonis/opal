#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// いろいろ
//========================================
//----------------------------------------
// いろいろ
//----------------------------------------
#define DECLVAL( a )		decltype( a ) a

#define if_constexpr		if constexpr

#define FORSE( i, s, e )	for ( auto i = s; i < e; i++ )
#define FORE(  i,    e )	FORSE( i, 0UL, e )
#define FOR(         e )	FORE(  i,      e )

//----------------------------------------
// SUPERクラス
//----------------------------------------
namespace opal {
	template<typename T>struct SUPER_CLASS : T { using SUPER = T; };
}

//----------------------------------------
// 最大値＆最小値保存
//----------------------------------------
namespace opal {
	template<typename TYPE>void maxval( TYPE& m, TYPE v ){ if ( m < v ) { m = v; } }
	template<typename TYPE>void minval( TYPE& m, TYPE v ){ if ( m > v ) { m = v; } }
}

//----------------------------------------
// 増減
//----------------------------------------
namespace opal {
	template<typename TYPE>void incvalr( TYPE& v, TYPE m, TYPE n = 0 ){ if ( v < m ) { v += 1; } else { v = n; }	}
	template<typename TYPE>void decvalr( TYPE& v, TYPE m, TYPE n = 0 ){ if ( v > n ) { v -= 1; } else { v = m; }	}
	template<typename TYPE>void incvals( TYPE& v, TYPE m ){ if ( v < m ) { v += 1; } 	}
	template<typename TYPE>void decvals( TYPE& v, TYPE n ){ if ( v > n ) { v -= 1; } 	}
}

//----------------------------------------
// ローカルタイム取得
//----------------------------------------
namespace opal {
	inline void timelocaltime( struct tm* l ){

		time_t	t = time( nullptr );

		localtime_s( l, &t );
	}
}

//----------------------------------------
// 絶対パスか？
//----------------------------------------
namespace opal {
	inline bool absolute_path( const std::string& p ){

		if ( p.size() < 3 ) { return false; }

		if ( p[1] == ':' && p[2] == '/' ) { return true; }
		if ( p[0] == '.' && p[1] == '/' ) { return true; }
		if ( p[0] == '.' && p[1] == '.' && p[2] == '/' ) { return true; }

		return false;
	}

	inline bool absolute_path( const char* p ){ return p ? absolute_path( std::string{p} ) : false;	}
	constexpr bool absolute_path( std::nullptr_t ){ return false; }
}

//----------------------------------------
// shared map
//----------------------------------------
namespace opal {
	template<typename TYPE>using SHARED_MAP = std::unordered_map<TYPE*,std::shared_ptr<TYPE>>;
}

//----------------------------------------
// map
//----------------------------------------
namespace opal {
	template<typename M, typename K>auto mapfind( const M& m, const K& k )->bool{

		return m.find( k ) != m.end();
	}

	template<typename M, typename K>auto maperase( M& m, const K& k )->bool{

		if ( mapfind( m, k ) ) {
			m.erase( k );
			return true;
		}
		return false;
	}

	template<typename M, typename F>auto maperase_if( M& m, F f ){

		auto	n = 0UL;
		auto	i = m.begin();

		while ( i != m.end() ) {
			if ( f( *i ) ) {
				m.erase( i++ );
			} else {
				++i;
				++n;
			}
		}
		return n;
	}

	template<typename M, typename K>auto mapped( const M& m, const K& k )->typename M::mapped_type{

		if ( mapfind( m, k ) ) {
			return m.at( k );
		}
		return {};
	}

	template<typename M, typename K>auto mapcount( const M& m, const K& k )->std::size_t{

		return m.count( 0 );
	}

	template<typename M>auto mapat( const M& m, typename M::key_type k )->typename M::mapped_type{

		return m.at( k );
	}

	template<typename M>auto mapsize( const M& m )->typename M::size_type{

		return m.size();
	}

	template<typename M>void mapclear( M& m ){

		m.clear();
	}

	template<typename M>auto mapval( const M& m, typename M::key_type k )->typename M::value_type{

		if ( mapfind( m, k ) ) {
			return {k,m.at(k)};
		}
		return {};
	}
}

//----------------------------------------
// stack
//----------------------------------------
namespace opal {
	template<typename S>void stackclear( S& s ){ S().swap(s); }
}

//----------------------------------------
// shared ptr
//----------------------------------------
namespace opal {
	template<typename TYPE>using SP_ = std::shared_ptr<TYPE>;
}

//----------------------------------------
// make shared
//----------------------------------------
namespace opal {
	template<typename TYPE,typename... ARGS> auto makeshared( TYPE& s, ARGS&&... args )->auto{

		return s = std::make_shared<typename TYPE::element_type>( std::forward<ARGS>( args )... );
	}

	template<typename TYPE,typename... ARGS> auto makeshared( std::shared_ptr<void>& s, ARGS&&... args )->auto{

		return static_cast<TYPE*>( ( s = std::make_shared<TYPE>( std::forward<ARGS>( args )... ) ).get() );
	}

	template<typename TYPE,typename... ARGS> auto makeshared2( TYPE& s, std::function<void(typename TYPE::element_type*)> f, ARGS&&... args )->auto{

		return s = std::shared_ptr<typename TYPE::element_type>( new typename TYPE::element_type( std::forward<ARGS>( args )... ), f );
	}

	template<typename TYPE,typename... ARGS> auto makeshared2( std::shared_ptr<void>& s, std::function<void(typename TYPE::element_type*)> f, ARGS&&... args )->auto{

		return static_cast<TYPE*>( ( s = std::shared_ptr<typename TYPE::element_type>( new typename TYPE::element_type( std::forward<ARGS>( args )... ), f ) ).get() );
	}

}

//----------------------------------------
// weak pointer のset/map
//----------------------------------------
namespace opal {
	template<typename TYPE             >using WSET = std::set<std::weak_ptr<TYPE>,    std::owner_less<std::weak_ptr<TYPE>>>;
	template<typename TYPE,typename VAL>using WMAP = std::map<std::weak_ptr<TYPE>,VAL,std::owner_less<std::weak_ptr<TYPE>>>;
}

//----------------------------------------
// weak_ptr から shared_ptr を取得
//----------------------------------------
namespace opal {
	template<typename TYPE>auto weak_lock( const std::weak_ptr<TYPE>& w )->std::shared_ptr<TYPE>{

		if ( !w.expired() ) {
			return w.lock();
		}
		return nullptr;
	}
}

//----------------------------------------
// weak_ptr と shared_ptr の比較
//----------------------------------------
namespace opal {
	template<typename TYPE>bool operator== ( const std::weak_ptr<  TYPE>& w, const std::shared_ptr<TYPE>& s ){ return !w.expired() && w.lock() == s;	}
	template<typename TYPE>bool operator== ( const std::shared_ptr<TYPE>& s, const std::weak_ptr<  TYPE>& w ){ return !w.expired() && w.lock() == s;	}
	template<typename TYPE>bool operator!= ( const std::weak_ptr<  TYPE>& w, const std::shared_ptr<TYPE>& s ){ return  w.expired() || w.lock() != s;	}
	template<typename TYPE>bool operator!= ( const std::shared_ptr<TYPE>& s, const std::weak_ptr<  TYPE>& w ){ return  w.expired() || w.lock() != s;	}
}

//----------------------------------------
// 型判定
//----------------------------------------
namespace opal {
	template<template<typename...>typename Template,typename    TYPE>struct is_template                             : std::false_type{};
	template<template<typename...>typename Template,typename... ARGS>struct is_template<Template,Template<ARGS...>> : std::true_type {};

	template<typename TYPE>using is_vector	= is_template<std::vector,			TYPE>;
	template<typename TYPE>using is_map		= is_template<std::map,				TYPE>;
	template<typename TYPE>using is_umap	= is_template<std::unordered_map,	TYPE>;
	template<typename TYPE>using is_shared	= is_template<std::shared_ptr,		TYPE>;
}

//----------------------------------------
// 格子模様
//----------------------------------------
namespace opal {
	inline bool lattice( int i, int j ){ return !((i&1) ^ (j&1)); }
}

//----------------------------------------
// 移動
//----------------------------------------
namespace opal {
	inline auto moveF3( float r, float s = 1.0f )->std::pair<float,float>{ return {+s * std::sin( r ),+s * std::cos( r )}; }
	inline auto moveB3( float r, float s = 1.0f )->std::pair<float,float>{ return {-s * std::sin( r ),-s * std::cos( r )}; }
	inline auto moveL3( float r, float s = 1.0f )->std::pair<float,float>{ return {-s * std::cos( r ),+s * std::sin( r )}; }
	inline auto moveR3( float r, float s = 1.0f )->std::pair<float,float>{ return {+s * std::cos( r ),-s * std::sin( r )}; }

	inline auto moveF2( float r, float s = 1.0f )->std::pair<float,float>{ return {-s * std::sin( r ),+s * std::cos( r )}; }
	inline auto moveB2( float r, float s = 1.0f )->std::pair<float,float>{ return {+s * std::sin( r ),-s * std::cos( r )}; }
	inline auto moveL2( float r, float s = 1.0f )->std::pair<float,float>{ return {+s * std::cos( r ),+s * std::sin( r )}; }
	inline auto moveR2( float r, float s = 1.0f )->std::pair<float,float>{ return {-s * std::cos( r ),-s * std::sin( r )}; }
}

// End Of File
