#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// wÖA
//========================================
//----------------------------------------
// ÍÍ`FbN
//----------------------------------------
namespace opal {
	template<typename VAL, typename MIN, typename MAX> inline bool RangeEE( VAL v, MIN a, MAX b ){ return ( a <= v ) && ( v <= b ); }
	template<typename VAL, typename MIN, typename MAX> inline bool RangeET( VAL v, MIN a, MAX b ){ return ( a <= v ) && ( v <  b ); }
	template<typename VAL, typename MIN, typename MAX> inline bool RangeTE( VAL v, MIN a, MAX b ){ return ( a <  v ) && ( v <= b ); }
	template<typename VAL, typename MIN, typename MAX> inline bool RangeTT( VAL v, MIN a, MAX b ){ return ( a <  v ) && ( v <  b ); }
}

//----------------------------------------
// £vZ
//----------------------------------------
namespace opal {
	template<typename T> inline double GetLength( T p1x, T p1y, T p2x, T p2y ){

		return std::sqrt( ( p2x-p1x )*( p2x-p1x )+( p2y-p1y )*( p2y-p1y ) );
	}
	template<typename V> inline double GetLengthXY( V& p1, V& p2 ){

		return std::sqrt( ( p2.x-p1.x )*( p2.x-p1.x )+( p2.y-p1.y )*( p2.y-p1.y ) );
	}
	template<typename V> inline double GetLengthYZ( V& p1, V& p2 ){

		return std::sqrt( ( p2.y-p1.y )*( p2.y-p1.y )+( p2.z-p1.z )*( p2.z-p1.z ) );
	}
	template<typename V> inline double GetLengthZX( V& p1, V& p2 ){

		return std::sqrt( ( p2.z-p1.z )*( p2.z-p1.z )+( p2.x-p1.x )*( p2.x-p1.x ) );
	}
	template<typename V> inline double GetLength( V& p1, V& p2 ){

		return GetLengthXY( p1, p2 );
	}
}

//----------------------------------------
// CN/fN
//----------------------------------------
namespace opal {
	template<typename TYPE> inline void LimitInc( TYPE& term, size_t max ){ if ( term < max-1 ) { ++term;	} }
	template<typename TYPE> inline void LimitDec( TYPE& term, size_t max ){ if ( term > 0     ) { --term;	} }
	template<typename TYPE> inline void LoopInc(  TYPE& term, size_t max ){ term = ( term == max-1 ) ? 0     : term+1; }
	template<typename TYPE> inline void LoopDec(  TYPE& term, size_t max ){ term = ( term == 0     ) ? max-1 : term-1; }
}

//----------------------------------------
// ÍÍÛßÝ
//----------------------------------------
namespace opal {
	template<typename R>inline int LimitRange( R& val, R min, R max ){

		if ( val < min ) { val += ( max - min ); return +1; }
		if ( max < val ) { val -= ( max - min ); return -1; }

		return 0;
	}
}

//----------------------------------------
// tMOD
//----------------------------------------
namespace opal {
	constexpr float modfz( float a ){

		float n = 0;

		if ( a > 0 ) { std::modf( +a, &n ); return n;   }
		if ( a < 0 ) { std::modf( -a, &n ); return n-1; }

		return 0;
	}

	constexpr UINT rmod( int n, UINT m ){

		if ( m ) {
			while ( n < 0 ) {
				n += m;
			}
			return n%m;
		}
		return 0;
	}
}

//----------------------------------------
// æ¾
//----------------------------------------
namespace opal {
	template<typename TYPE> inline int Sign( TYPE s ){ return ( s ) ? ( ( s > 0 ) ? +1 : -1 ) : 0; }
	template<> inline int Sign( float s ){ return !FZERO( s ) ? ( ( s > 0 ) ? +1 : -1 ) : 0; }
}

//----------------------------------------
// Næ
//----------------------------------------
namespace opal {
	template<typename TYPE>TYPE PowN( TYPE t, unsigned int n ){

		TYPE	s = 1;

		for ( auto i = 0UL; i < n; i++ ) {
			s *= t;
		}

		return s;
	}
	inline unsigned Pow2( unsigned int n ){ return PowN( ( unsigned int )2, n );	}
}

//----------------------------------------
// 2Ì×«æÉß
//----------------------------------------
namespace opal {
	template<typename TYPE>TYPE Pow2Min( TYPE  s ) { TYPE n = 1; while ( n <= s ) { n <<= 1; } return n >> 1;	}
	template<typename TYPE>TYPE Pow2Max( TYPE  s ) { TYPE n = 1; while ( n <  s ) { n <<= 1; } return n; 		}
	template<typename TYPE>TYPE Pow2Min( TYPE* s ) { return *s = Pow2Max( *s ); }
	template<typename TYPE>TYPE Pow2Max( TYPE* s ) { return *s = Pow2Max( *s ); }
}

//----------------------------------------
// Êq»
//----------------------------------------
namespace opal {
	constexpr int Quantize( float v, float u ) {
#ifndef _MSC_VER
		const auto	n = std::abs( u );
#else
		const auto	n = ( u < 0 ) ? -u : +u;
#endif

		if ( v > 0 ) { return static_cast<int>( +std::floor( +v/n ) ); }
		if ( v < 0 ) { return static_cast<int>( -std::ceil(  -v/n ) ); }

		return 0;
	}
}

//----------------------------------------
// è]
//----------------------------------------
namespace opal {
	constexpr UINT Surplus( int n, size_t m ){

		if ( m == 0 ) { return 0;		}
		if ( n >  0 ) { return 0+n%m;	}
		if ( n <  0 ) { return m+n%m;	}

		return 0;
	}
}

//----------------------------------------
// 1¶ itoa
//----------------------------------------
namespace opal {
	constexpr int ntoa( unsigned int n ) { return ( n <= 9 ) ? n+'0' : 0; }
}

// End Of File
