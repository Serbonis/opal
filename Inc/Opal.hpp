#pragma once
//========================================
// OPAL Game Library
//========================================
#include "com.hpp"
#include "sys.hpp"

//========================================
// main
//========================================
#define main( ... )	APIENTRY _tWinMain( HINSTANCE, HINSTANCE, LPSTR, int )

//========================================
// Opal
//========================================
extern int Opal( void );

extern int Opal( OPAL::sysinit, OPAL::sysexec, OPAL::sysfree );
extern int Opal( OPAL::sysinit, OPAL::sysexec                );
extern int Opal(                OPAL::sysexec                );

extern int Opal( OPAL::FUNC* );
extern int Opal( OPAL::FUNC& );

extern int Opal( const char*        );
extern int Opal( const std::string& );
extern int Opal( std::nullptr_t     );

extern int Opal( const char*,        OPAL::sysinit, OPAL::sysexec, OPAL::sysfree );
extern int Opal( const char*,        OPAL::sysinit, OPAL::sysexec                );
extern int Opal( const char*,                       OPAL::sysexec                );
extern int Opal( const std::string&, OPAL::sysinit, OPAL::sysexec, OPAL::sysfree );
extern int Opal( const std::string&, OPAL::sysinit, OPAL::sysexec                );
extern int Opal( const std::string&,                OPAL::sysexec                );
extern int Opal( std::nullptr_t,     OPAL::sysinit, OPAL::sysexec, OPAL::sysfree );
extern int Opal( std::nullptr_t,     OPAL::sysinit, OPAL::sysexec                );
extern int Opal( std::nullptr_t,                    OPAL::sysexec                );

extern int Opal( const char*,        OPAL::FUNC* );
extern int Opal( const char*,        OPAL::FUNC& );
extern int Opal( const std::string&, OPAL::FUNC* );
extern int Opal( const std::string&, OPAL::FUNC& );
extern int Opal( std::nullptr_t,     OPAL::FUNC* );
extern int Opal( std::nullptr_t,     OPAL::FUNC& );

template <typename TYPE>int Opal( void                 ){ return Opal(    new TYPE ); }
template <typename TYPE>int Opal( const char*        p ){ return Opal( p, new TYPE ); }
template <typename TYPE>int Opal( const std::string& p ){ return Opal( p, new TYPE ); }
template <typename TYPE>int Opal( std::nullptr_t     p ){ return Opal( p, new TYPE ); }

// End Of File
