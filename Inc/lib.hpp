#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// MSC
//========================================
#ifndef NOMINMAX
#define NOMINMAX
#endif

#ifndef STRICT
#define STRICT
#endif

#ifdef _MSC_VER
#include "msc.hpp"
#endif

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <windowsx.h>
#include <shlwapi.h>

// GUID & ComPtr用
#include <dxgi1_2.h>
#include <wrl/client.h>

using namespace Microsoft::WRL;

//========================================
// 標準ライブラリ
//========================================
//----------------------------------------
// C
//----------------------------------------
#include <tchar.h>
#include <wchar.h>
#include <process.h>
#include <stdint.h>
#include <stdlib.h>
#include <signal.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif

//----------------------------------------
// C++
//----------------------------------------
#include <stdexcept>
#include <cfloat>

#include <cstddef>
#include <cstdio>
#include <cstdarg>
#include <cassert>

#include <cstring>
#include <cmath>
#include <ctime>
//#include <numbers>

#include <new>
#include <typeinfo>

#include <array>
#include <list>
#include <map>
#include <unordered_map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <deque>

#include <string>
#include <limits>
#include <algorithm>
#include <functional>
#include <type_traits>

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <random>
#include <memory>

#include <any>
#include <optional>

//========================================
// その他
//========================================
#ifdef _MSC_VER
#include <DxErr.h>
#else
#include <dxerr9.h>
#endif

// End Of File
