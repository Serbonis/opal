#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// �g��
//========================================
#include "Ext/point.hpp"
#include "Ext/result.hpp"
#include "Ext/treeway.hpp"
#include "Ext/rectangle.hpp"
#include "Ext/size.hpp"
#include "Ext/minmax.hpp"
#include "Ext/restrict.hpp"
#include "Ext/color.hpp"
#include "Ext/texture.hpp"
#include "Ext/material.hpp"
#include "Ext/light.hpp"
#include "Ext/vector.hpp"
#include "Ext/polar.hpp"
#include "Ext/cylind.hpp"
#include "Ext/matrix.hpp"
#include "Ext/quaternion.hpp"
#include "Ext/xmath.hpp"
#include "Ext/viewport.hpp"
#include "Ext/world.hpp"
#include "Ext/hlsl.hpp"
#include "Ext/generic.hpp"

// End Of File
