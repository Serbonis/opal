#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// �e�p�[�g
//========================================
#include "heap.hpp"
#include "memory.hpp"
#include "sig.hpp"

#include "pad.hpp"
#include "thrd.hpp"
#include "file.hpp"
#include "netw.hpp"

#include "srvc.hpp"
#include "rsrc.hpp"
#include "prim.hpp"

#include "obj.hpp"
#include "super.hpp"
#include "work.hpp"
#include "draw.hpp"
#include "call.hpp"

#include "debug.hpp"

// End Of File
