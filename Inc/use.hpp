#pragma once
//========================================
// OPAL Game Library
//========================================
namespace opal {}
#ifdef OPAL_NAMESPACE
using namespace opal;
#endif

//========================================
// ライブラリのライブラリ
//========================================
#ifndef _MSC_VER
#include "mingw.hpp"	// MinGW 関連
#else
#include "msc.hpp"		// MSC 関連
#endif

#include "lib.hpp"		// 標準ヘッダー
#include "dbg.hpp"		// デバッグ

#include "etc.hpp"		// Etc/のヘッダー
#include "std.hpp"		// Lib/Std/のヘッダー
#include "ext.hpp"		// Lib/Ext/のヘッダー
#include "utl.hpp"		// Lib/Utl/のヘッダー

#include "Time.hpp"		// 計測関連
#include "Rand.hpp"		// 乱数関連

// End Of File
