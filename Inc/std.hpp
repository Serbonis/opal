#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// �W��
//========================================
#include "Std/const.hpp"
#include "Std/sincos.hpp"
#include "Std/task.hpp"
#include "Std/linx.hpp"
#include "Std/str_s.hpp"
#include "Std/mem_s.hpp"
#include "Std/times.hpp"
#include "Std/name.hpp"
#include "Std/tick.hpp"
#include "Std/finfo.hpp"
#include "Std/guid.hpp"
#include "Std/glyph.hpp"
#include "Std/message.hpp"
#include "Std/node.hpp"

#include "Std/single.hpp"
#include "Std/fetch.hpp"
#include "Std/bank.hpp"
#include "Std/device.hpp"
#include "Std/interface.hpp"

// End Of File
