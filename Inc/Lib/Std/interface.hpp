#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//----------------------------------------
// INTERFACE
//----------------------------------------
namespace opal {
	template<typename TYPE_D, typename TYPE_I=std::in_place_t>class INTRF : public TYPE_I {
	public:
		constexpr explicit INTRF() :
			self( nullptr ),
			that( nullptr )
		{}
		constexpr explicit INTRF( std::shared_ptr<TYPE_D> p ) :
			self( p ),
			that( self.get() )
		{}
		constexpr explicit INTRF( TYPE_D* t ) :
			self( nullptr ),
			that( t )
		{}

		virtual ~INTRF(){
			self = nullptr;
			that = nullptr;
		}

	protected:
		std::shared_ptr<TYPE_D>	self;
		TYPE_D*					that;

	public:
		virtual std::shared_ptr<TYPE_D> GetInterface( void ) const { return self;	}

		virtual void SetInterface( const INTRF<TYPE_D,TYPE_I>& t ){

			self = t.self;
			that = t.that;
		}

		virtual void SetInterface( const INTRF<TYPE_D,TYPE_I>* t ){

			if ( t ) {
				SetInterface( *t );
			}
		}

	public:
		virtual TYPE_D* GetThat( void ) const { return that;	}

		virtual void SetThat( TYPE_D& t ){

			self = nullptr;
			that = &t;
		}

		virtual void SetThat( TYPE_D* t ){

			if ( t ) {
				SetThat( *t );
			}
		}
	};
}

#if 0
//----------------------------------------
// sample:メンバー部
//----------------------------------------
namespace opal::sample {
	class SAMPLE_I;										// メソッド部を前方宣言
	class SAMPLE_D {
		friend class SAMPLE_I;							// メソッド部はフレンド

		// メンバー変数を記載
		int	x;
		int	y;
	};
}

//----------------------------------------
// sample:メソッド部
//----------------------------------------
namespace opal::sample {
	class SAMPLE_I : public opal::INTRF<SAMPLE_D> {		// メンバー部を持つインターフェースを継承
		using INTRF::INTRF;								// コンストラクタを取り入れる

	public:
		// メンバー関数を記載
		virtual int Sum( void ) const { return that->x + that->y; }
	};
}

//----------------------------------------
// sample
//----------------------------------------
namespace opal::sample {
	class SAMPLE : public SAMPLE_I {
		using SAMPLE_I::SAMPLE_I;

	public:
		explicit SAMPLE() :
			SAMPLE_I( std::make_shared<SAMPLE_D> )
		{}

		virtual ~SAMPLE()
		{
			if ( !self ) {
				DELETES( that );					// NEWSしている場合
			}
		}

	public:
		SAMPLE& GetMySelf( void );					// 自身を返す関数は SAMPLE_I に書けない
	};
#endif

// End Of File
