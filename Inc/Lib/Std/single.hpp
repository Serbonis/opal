#pragma once
//----------------------------------------
// OPAL Game Library
//----------------------------------------
#include "Etc/nocopy.hpp"

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// シングルトン
//----------------------------------------
// 1.public で継承する
// 2.継承したクラスはシングルトンのfrinedにする
// 3.インスタンスを生成できなくする
// 4.ちゃんと破棄できるようにする
//
// class TYPE : public SINGS<TYPE> {	…1
//		friend class SINGS<TYPE>;		…2
//	private: explicit TYPE() = default;	…3
//	public:  virtual ~TYPE() = default;	…4
// };
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
namespace opal {
	template<typename TYPE> struct SINGS : private NOCOPY {
		static TYPE* Instance( void ){ static TYPE instance; return &instance;	}
		static TYPE& INSTANCE( void ){ return *Instance();	}
	};
}

// End Of File
