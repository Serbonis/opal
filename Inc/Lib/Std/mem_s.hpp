#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// NULL対応のメモリ操作関数
//========================================
namespace opal {
	extern void* memcpy_s( void* dest, const void* src, size_t n );
	extern void* memset_s( void* buff, int ch, size_t n );
}

// End Of File
