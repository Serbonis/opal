#pragma once
//========================================
// OPAL Game Library
//========================================
#include "msc.hpp"

#include <rpc.h>
#include <stdio.h>
#include <string>

//========================================
// 匿名IDを生成する
//========================================
namespace opal {
	extern auto guid_code( void )->UUID;
	extern auto guid_name( void )->std::string;
}

// End Of File
