#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

//========================================
// デバイス
//========================================
//----------------------------------------
// インターフェース
//----------------------------------------
namespace opal {
	using DEVICE_NAME = std::string;

	class XDEVICE {
	public:
		explicit XDEVICE( const DEVICE_NAME );
		explicit XDEVICE();
		virtual ~XDEVICE();

	protected:
		DEVICE_NAME	name;

	public:
		virtual operator DEVICE_NAME(   void ) const;
		virtual DEVICE_NAME DeviceName( void ) const;
		virtual DEVICE_NAME DeviceName( const DEVICE_NAME );
		virtual DEVICE_NAME DeviceName( const char* );

	public:
		virtual int InitDevice( int );
		virtual int FreeDevice( int );

	public:
		virtual void DeviceData( const void*[] );
		virtual void DeviceData( const void*   );
		virtual void DeviceData( void );

	public:
		virtual void DestroyDevice( void );

	public:
		virtual bool DeviceReady( void ) const;
	};
}

//----------------------------------------
// デバイスロスト
//----------------------------------------
namespace opal {
	class DEVICE_LOST {
	public:
		explicit DEVICE_LOST();
		virtual ~DEVICE_LOST();

	private:
		UINT										count;
		std::unordered_map<DEVICE_NAME,XDEVICE*>	device;

	public:
		virtual void Clear( void );

	public:
		virtual void DeviceLost( const DEVICE_NAME, XDEVICE* );
		virtual void DeviceLost( const DEVICE_NAME );
		virtual UINT DeviceLost( void ) const;

	public:
		virtual const decltype( device )& operator ()( void ) const;

	public:
		virtual operator UINT( void ) const;
		virtual UINT operator  =( UINT );
		virtual UINT operator ++( void );
		virtual UINT operator --( void );
		virtual UINT operator ++( int  );
		virtual UINT operator --( int  );

	public:
		virtual int  InitDevice( int );
		virtual int  FreeDevice( int );
		virtual void DestroyDevice( void );
	};
}

//----------------------------------------
// 共通デバイス
//----------------------------------------
namespace opal {
	template<typename TYPE>class IDEVICE final : public XDEVICE {
	public:
		explicit IDEVICE( DEVICE_NAME n ) : XDEVICE( n ), sp( nullptr ) {}
		explicit IDEVICE() : sp( nullptr ) {}
		virtual ~IDEVICE(){}

	public:
		using S_PTR = std::shared_ptr<TYPE>;

	private:
		std::unique_ptr<S_PTR>	sp;

	public:
#ifdef _MSC_VER
		virtual int InitDevice( int n ) override;
		virtual int FreeDevice( int n ) override;
#define IDEVICE_( TYPE )	\
		template<> int IDEVICE<TYPE>::InitDevice( int n ){ return sp ? (*sp)->InitDevice( n ) : 0; } \
		template<> int IDEVICE<TYPE>::FreeDevice( int n ){ return sp ? (*sp)->FreeDevice( n ) : 0; }
#else
		virtual int InitDevice( int n ) override { return sp ? (*sp)->InitDevice( n ) : 0; }
		virtual int FreeDevice( int n ) override { return sp ? (*sp)->FreeDevice( n ) : 0; }
#define IDEVICE_( TYPE )
#endif

	public:
		virtual TYPE* operator ()( void ) const {

			if ( sp ) {
				if ( const auto tp = sp.get() ) {
					return tp->get();
				}
			}
			return nullptr;
		}

	public:
		template<typename... ARGS>S_PTR CreateDevice( ARGS&&... args ){

			if ( !sp ) {
				sp  = std::make_unique<S_PTR>();
				*sp = std::shared_ptr<TYPE>( NEWS<TYPE>( std::forward<ARGS>( args )... ),
											 []( TYPE* p ){
												 p->FreeDevice( 0 );
												 DELETES( p ); } );

				(*sp)->InitDevice( 0 );
			}
			return *sp;
		}

		virtual void DestroyDevice( void ){

			if ( sp ) {
				sp->reset();
				sp = nullptr;
			}
		}
	public:
		virtual UINT DeviceCount( void ){ return sp ? (*sp).use_count() : 0; }
	};
}

//----------------------------------------
// デバイスポインター
//----------------------------------------
namespace opal {
	template<typename TYPE>using PDEVICE = IDEVICE<TYPE>*;
	template<typename TYPE>using SDEVICE = typename IDEVICE<TYPE>::S_PTR;
}

//----------------------------------------
// デバイスワーク
//----------------------------------------
namespace opal {
	template<typename VAL>class DEVICE_VAL : public XDEVICE {
	public:
		explicit DEVICE_VAL(){}
		virtual ~DEVICE_VAL(){}

	public:
		static IDEVICE<VAL>	device;

	public:
		static void CreateDevice(  SDEVICE<VAL>* d ){ if ( d ) { *d = device.CreateDevice( d ); }	}
		static void DestroyDevice( SDEVICE<VAL>* d ){ if ( d ) { *d = nullptr;	} }
	};
}

//----------------------------------------
// デバイスマップ
//----------------------------------------
namespace opal {
	template<typename KEY,
			 typename VAL,
			 void (*REG)(const DEVICE_NAME, XDEVICE*) = nullptr,
			 void (*REL)(const DEVICE_NAME          ) = nullptr>
	class DEVICE_MAP : public XDEVICE {
	public:
		explicit DEVICE_MAP() : map{} {}
		virtual ~DEVICE_MAP(){}

	public:
		static			IDEVICE<DEVICE_MAP<KEY,VAL,REG,REL>>		device;
		static typename	IDEVICE<DEVICE_MAP<KEY,VAL,REG,REL>>::S_PTR	devmap;

	protected:
		std::unordered_map<KEY,IDEVICE<VAL>*>	map;

	public:
		virtual int InitDevice( int n ) override {

			if ( n == 0 ) {
//!				if constexpr ( REG ) {
					REG( device, &device );
//!				}
			}
			map.clear();
			return 0;
		}

		virtual int FreeDevice( int n ) override {

			if ( n == 0 ) {
//!				if constexpr ( REL ) {
					REL( device );
//!				}
			}
			map.clear();
			return 0;
		}

	public:
		template<typename TYPE>static SDEVICE<VAL> CreateDevice( const KEY k, const TYPE* d = nullptr ){

			if ( !devmap ) {
				devmap = device.CreateDevice();
			}
			return devmap->create( k, d );
		}

		static void DestroyDevice( const KEY k ){

			if ( devmap ) {
				devmap->destroy( k );
				if ( !devmap->map.size() ) {
					devmap = nullptr;
				}
			}
		}

	private:
		template<typename TYPE>SDEVICE<VAL> create( const KEY k, const TYPE* d = nullptr ){

			if ( !map.count( k ) ) {
				NEWS( map[k], std::string{NAME( 0, "%s:%p", typeid( VAL ).name(), this )} );
			}
			return map.at( k )->CreateDevice( d );
		}

		void destroy( const KEY k ){

			if ( !map.count( k ) ) {
				return;
			}

			if ( map.at( k )->DeviceCount() == 1 ) {
				DELETES( map.at( k ) );
				map.erase( k );
			}
		}
	};
}

// End Of File
