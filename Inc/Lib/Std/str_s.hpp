#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 文字列操作
//========================================

//========================================
// NULL対応の文字列操作関数
//========================================
namespace opal {
	extern UINT strlen_s(  const char* );
	extern UINT strlenf_s( const char*, ... );
	extern UINT strlenf_s( const char*, va_list& );

	extern char* strcpy_s(  char* dest, const char* );
	extern char* strncpy_s( char* dest, const char*, UINT );

	extern int strcmp_s(  const char*, const char* );
	extern int strncmp_s( const char*, const char*, UINT );

	extern const char* strstr_s( const char*, const char* );
	extern const char* strcnt_s( const char*, const char*, UINT );

	extern const char* strptr_s( const char*, int n );

	extern UINT strline_s( const char* );
	extern int  strcoline( const char*, UINT*, UINT* );
}

//========================================
// NULL対応の文字列操作関数( WCHAR )
//========================================
namespace opal {
	extern wchar_t* wcsncpy_s ( wchar_t*, const wchar_t*, size_t );
}

//========================================
// WCHAR* ⇔ char* 変換
//========================================
namespace opal {
	extern size_t wtomlen( const wchar_t* );
	extern size_t mtowlen( const char*    );

	extern const char*    wtomcpy( const wchar_t*, char*,    size_t );
	extern const wchar_t* mtowcpy( const char*,    wchar_t*, size_t );

	class MTOW {
		friend MTOW operator +( const MTOW&,    const MTOW&    );
		friend MTOW operator +( const MTOW&,    const char*    );
		friend MTOW operator +( const char*,    const MTOW&    );
		friend MTOW operator +( const MTOW&,    const wchar_t* );
		friend MTOW operator +( const wchar_t*, const MTOW&    );

	public:
		MTOW();
		MTOW( const MTOW& );
		MTOW( size_t );
		MTOW( const char* );
		MTOW( const wchar_t* );
		MTOW( const MTOW&,    const MTOW&    );
		MTOW( const MTOW&,    const char*    );
		MTOW( const char*,    const MTOW&    );
		MTOW( const char*,    const char*    );
		MTOW( const MTOW&,    const wchar_t* );
		MTOW( const wchar_t*, const MTOW&    );
		MTOW( const wchar_t*, const wchar_t* );
		MTOW( const wchar_t*, const char*    );
		MTOW( const char*,    const wchar_t* );
		virtual ~MTOW();

	public:
		virtual wchar_t* operator ()( size_t );
		virtual wchar_t* operator []( size_t );
		virtual wchar_t* operator ()(    void ) const;
		virtual operator const wchar_t*( void ) const;

	public:
		virtual operator size_t( void ) const;

	protected:
		size_t		size{0};
		wchar_t*	buff{nullptr};
	};

	extern MTOW operator + ( const MTOW&,    const MTOW&    );
	extern MTOW operator + ( const MTOW&,    const char*    );
	extern MTOW operator + ( const char*,    const MTOW&    );
	extern MTOW operator + ( const MTOW&,    const wchar_t* );
	extern MTOW operator + ( const wchar_t*, const MTOW&    );
}

//========================================
// sjisかどうか
//========================================
namespace opal {
	extern bool sjischeck( unsigned char c );
}

//========================================
// 分割
//========================================
namespace opal {
	std::vector<std::string> split( const std::string&, const std::string& );
}

//========================================
// 数値→文字列
//========================================
namespace opal {
	std::string itos( int );
}

// End Of File
