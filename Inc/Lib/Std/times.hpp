#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

//========================================
// 速度調整クラス
//========================================
namespace opal {
	class TIMES final : private NOCOPY {
	public:
		explicit TIMES();
		~TIMES() = default;

	private:
		int	skip;
		int	loop;
		int	rate;

	public:
		bool operator ()( int  );
		int  operator ()( void );

	public:
		operator int( void ) const;

	public:
		int operator =( int );
	};
}

// End Of File
