#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

//========================================
// タイムインターバル
//========================================
namespace opal {
	class TICK {
	public:
		explicit TICK( int, int );
		explicit TICK( int );
		explicit TICK();
		virtual ~TICK() = default;

	private:
		int	tick;
		int	wait;

	public:
		virtual void SetTick( int );
		virtual int GetTick( void ) const;
		virtual int operator =( int );

	public:
		virtual int operator ()( int );
		virtual operator  int() const;

	public:
		virtual bool operator ()( void );
		virtual bool Tick( void );

	public:
		template<typename TYPE>static bool rels( TYPE& c         ){ return c > 0 && --c <= 0;	}
		template<typename TYPE>static bool push( TYPE& c         ){ return c > 0 && --c >= 0;	}
		template<typename TYPE>static bool rels( TYPE& c, TYPE t ){ return c < t && --c >= t;	}
		template<typename TYPE>static bool push( TYPE& c, TYPE t ){ return c < t && ++c <= t;	}

	public:
		template<typename TYPE>static bool trig( TYPE& c         ){ return c == 0 || --c < 0;	}
		template<typename TYPE>static bool trig( TYPE& c, TYPE t ){ return c == t || ++c > t;	}
	};
	template<>inline bool TICK::trig( float& c          ){ return c <= 0 || --c < 0;	}
	template<>inline bool TICK::trig( float& c, float t ){ return c >= t || ++c > t;	}
}

// End Of File
