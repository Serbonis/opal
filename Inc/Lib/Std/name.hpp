#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

#include "Std/str_s.hpp"

//========================================
// �ėp���O�Ǘ�
//========================================
namespace opal {
	class NAME {
		//----------------------------------------
		// constructor & destructor
		//----------------------------------------
	public:
		NAME();
		virtual ~NAME();

	public:
		NAME( std::nullptr_t );
		NAME( const std::string_view );
		NAME( const NAME& );
		NAME( const char*, ... );

		//----------------------------------------
		// member variables
		//----------------------------------------
	private:
		std::unique_ptr<std::string>	name;

		//----------------------------------------
		// member functions
		//----------------------------------------
	public:
		virtual const char* operator ()( void ) const;
		virtual operator const char*( void ) const;

	public:
		virtual operator bool( void ) const;
		virtual bool operator !( void ) const;

	public:
		virtual NAME& operator  =( const NAME& );
		virtual NAME& operator +=( const NAME& );

	public:
		virtual void Name( const NAME& );
		virtual auto Name( void ) const->const char*;
		virtual auto Leng( void ) const->std::size_t;

	public:
		virtual void wide(  const wchar_t*, size_t = 0 );
		virtual void utf16( const char*,    size_t = 0 );
		virtual void utf8(  const char*,    size_t = 0 );
	};

	extern bool operator ==( const NAME&, const NAME& );
	extern bool operator ==( const NAME&, const char* );
	extern bool operator ==( const char*, const NAME& );
	extern bool operator !=( const NAME&, const NAME& );
	extern bool operator !=( const NAME&, const char* );
	extern bool operator !=( const char*, const NAME& );
	extern NAME operator + ( const NAME&, const NAME& );
	extern NAME operator + ( const NAME&, const char* );
	extern NAME operator + ( const char*, const NAME& );
}

// End Of File
