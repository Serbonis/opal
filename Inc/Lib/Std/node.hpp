#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

//========================================
// �m�[�h
//========================================
namespace opal {
	class NODE {
		using CHILD	= std::map<std::string,std::shared_ptr<NODE>>;

	public:
		explicit NODE();
		explicit NODE( const std::string&, NODE* = nullptr );
		virtual ~NODE();

	private:
		std::type_info const * 	type;
		std::string				name;
		std::shared_ptr<void>	data;
		std::shared_ptr<CHILD>	child;
		NODE*					parent;

	public:
		virtual const char* Type( void ) const;
		virtual const char* Name( void ) const;
		virtual std::string Path( void ) const;

	public:
		static void* Search( const NODE&, const std::string& );

		template<typename TYPE>static TYPE* Search( const NODE& n, const std::string& s ){

			return reinterpret_cast<TYPE*>( Search( n, s ) );
		}

	public:
		static void Destroy( NODE&, const std::string& );
		static void Destroy( NODE&, const std::type_info& );
		static void Destroy( NODE& );

	public:
		template<typename TYPE,typename... ARGS>static TYPE* Generate( NODE& n, const std::string& s, ARGS&&... a ){

			auto	p = &n;

			for ( auto& i : split( s, "/" ) ) {
				if ( !p->child ) {
					p->child = std::make_shared<CHILD>();
				}
				if ( !p->child->count( i ) ) {
					(*p->child)[i] = std::make_shared<NODE>( i, p );
				}
				p = p->child->at( i ).get();
			}

			p->type = &typeid( TYPE );
			p->data = std::shared_ptr<void>( NEWS<TYPE>( std::forward<ARGS>( a )... ), [](auto p){ DELETES<TYPE>( p ); } );

			return reinterpret_cast<TYPE*>( p->data.get() );
		}
#ifdef OPAL_DEBUG
	public:
		static void Debug( NODE& );
#endif
	};
}

// End Of File
