#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// データフェッチ:データ抽出
//========================================
namespace opal {
	using FETCH_P = const BYTE*;
}

namespace opal {
	namespace FETCH {
		template<typename TYPE>TYPE data( FETCH_P p ){

			return *reinterpret_cast<const TYPE*>( p );
		}

		template<typename TYPE>TYPE data( FETCH_P p, int s ){

			switch ( s ) {
			case -1: return *reinterpret_cast<const   signed char* >( p );
			case -2: return *reinterpret_cast<const   signed short*>( p );
			case -4: return *reinterpret_cast<const   signed int*  >( p );
			case +1: return *reinterpret_cast<const unsigned char* >( p );
			case +2: return *reinterpret_cast<const unsigned short*>( p );
			case +4: return *reinterpret_cast<const unsigned int*  >( p );
			}
			return 0;
		}

		template<typename TYPE>void data( FETCH_P p, TYPE* b, int s ){

			for ( auto i = 0; i < s; i++ ) {
				b[i] = data<TYPE>( p );
			}
		}
	}
}

//========================================
// データフェッチ:ポインター進める
//========================================
namespace opal {
	namespace FETCH {
		template<typename TYPE>TYPE load( FETCH_P& p ){

			auto	v = data<TYPE>( p );

			p += sizeof( TYPE );

			return v;
		}

		template<typename TYPE>TYPE load( FETCH_P& p, int s ){

			auto	v = data<TYPE>( p, s );

			p += abs( s );

			return v;
		}

		template<typename TYPE>void load( FETCH_P& p, TYPE* b, int s ){

			data<TYPE>( p, b, s );

			p += abs( s );
		}

		template<typename TYPE>const TYPE* pntr( FETCH_P& p, int s ){

			const auto	q = reinterpret_cast<const TYPE*>( p );

			p += abs( s );

			return q;
		}
	}
}

// End Of File
