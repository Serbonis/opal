#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// メッセ―ジ
//========================================
//----------------------------------------
// メッセージデータ
//----------------------------------------
#include "Std/name.hpp"

namespace opal {
	class MESSAGE_DATA {
	public:
		explicit MESSAGE_DATA();
		explicit MESSAGE_DATA( const std::string_view, void* = nullptr, size_t = 0, UINT = 0 );
		virtual ~MESSAGE_DATA() = default;

	protected:
		NAME		name;	// 名前
		void* 		adrs;	// アドレス
		size_t		size;	// サイズ
		UINT		argv;	// 拡張データ

	public:
		const char* Name( void ) const;
		void*       Adrs( void ) const;
		size_t		Size( void ) const;
		UINT        Argv( void ) const;

	public:
		void Name( const char* );
		void Adrs( void*  );
		void Size( size_t );
		void Argv( UINT   );
	};
}

//----------------------------------------
// メッセージタスク
//----------------------------------------
namespace opal {
	class MESSAGE_TASK {
	public:
		explicit MESSAGE_TASK();
		virtual ~MESSAGE_TASK() = default;

	protected:
		int		kind;	// 種別
		UINT	mode;	// モード
		DWORD	time;	// 実行時間
		UINT	errr;	// エラー
		DWORD	code;	// ラストエラー

	public:
		virtual void Task( int, UINT, DWORD = 0 );
	};
}

//----------------------------------------
// メッセージワーク
//----------------------------------------
namespace opal {
	constexpr UINT	MESSAGE_WORK_RETRY_MAX = 16;

	template<typename TYPE>struct MESSAGE_WORK {
		explicit MESSAGE_WORK() :
			info( nullptr ),
			step( 0 ),
			time( 0 ),
			rmax( 0 ),
			flag( 0 )
		{}
		explicit MESSAGE_WORK( TYPE* i, UINT s = 0, UINT r = 0 ) :
			info( i ),
			step( s ),
			time( 0 ),
			rmax( r ? r : MESSAGE_WORK_RETRY_MAX ),
			flag( 0 )
		{}
		virtual ~MESSAGE_WORK() = default;

		TYPE*	info;	// メッセージ情報
		UINT	step;	// 処理ステップ
		int		time;	// タイムアウト
		UINT	rmax;	// リトライカウント
		int		flag;	// 実行中フラグ
	};
}

//----------------------------------------
// メッセージ
//----------------------------------------
namespace opal {
	template<typename TYPE>class MESSAGE : private NOCOPY {
	public:
		using DATA = std::unordered_map<std::string,TYPE*>;

	public:
		explicit MESSAGE() = default;
		virtual ~MESSAGE() = default;

	protected:
		DATA	data;

	public:
		virtual DATA& Data( void ){ return data; }
		virtual TYPE* Data( const char* p ) const { return ( p && data.count( p ) ) ? data.at( p ) : nullptr; }

	public:
		virtual TYPE* Information( const char* p, void* a, size_t s = 0, UINT v = 0 ){

			if ( data.count( p ) ) {
				if ( const auto fp = data[p] ) {
					fp->Adrs( a );
					fp->Size( s );
					fp->Argv( v );
					return fp;
				}
			}
			return NEWS( data[p], p, a, s, v );
		}
		virtual TYPE* Information( const MESSAGE_DATA* d ){ return d ? Information( d->Name(), d->Adrs(), d->Size(), d->Argv() ) : nullptr;	}
		virtual TYPE* Information( const MESSAGE_DATA& d ){ return     Information(  d.Name(),  d.Adrs(),  d.Size(),  d.Argv() );			}

	public:
		virtual void Clear( const char* p = nullptr ){

			if ( p ) {
				DELETES( data[p] );
				data.erase( p );
			} else {
				for ( auto&& [s,d] : data ) {
					( void )s;
					DELETES( d );
				}
				data.clear();
			}
		}
	};
}

//----------------------------------------
// メッセージタスク
//----------------------------------------
#include "Std/task.hpp"
#include "Std/linx.hpp"

namespace opal {
	template<typename TYPE,typename TASK>class MTASK : public MESSAGE<TYPE> {
	public:
		using TMAP = std::unordered_map<std::string,TASK*>;

	public:
		explicit MTASK() = default;
		virtual ~MTASK() = default;

	protected:
		LINX	indx;
		TMAP	task;

	public:
		virtual void Init( const char* p = nullptr ){

			MESSAGE<TYPE>::Clear();

			indx.Init( p );
			task.clear();
		}

		virtual void Free( void ){

			task.clear();
			indx.TaskDestroy();

			MESSAGE<TYPE>::Clear();
		}

	public:
		virtual void Exec( void ){ indx.Exec();	}

	public:
		virtual void Info( UINT v ){ indx.Info( v );	}
		virtual UINT Info( void ) const { return indx.Info(); }

	public:
		virtual LINX& operator ()( void ){ return indx;	}

	public:
		virtual TASK* Head( void    ) const { return indx.Head<TASK>();		}
		virtual bool  Head( TASK* f ) const { return Head() == f;			}

	public:
		virtual UINT Size( void ) const { return task.size();	}

	public:
		virtual TMAP& Task( void ){ return task; }
		virtual TASK* Task( const char* p ) const { return ( p && task.count( p ) ) ? task.at( p ) : nullptr; }

	public:
		virtual TASK* Message( const char* p, TYPE* wi = nullptr, UINT ws = 0, UINT wr = 0 ){

			if ( const auto d = MESSAGE<TYPE>::Data( p ) ) {
				if ( const auto t = NEWS( task[p] ) ) {
					t->Init( p );
					t->Self( []( auto t ){ DELETES( t ); } );

					t->info = wi;
					t->step	= ws;
					t->time	= 0;
					t->rmax	= wr ? wr : MESSAGE_WORK_RETRY_MAX;
					t->flag	= true;

					indx.Register( t );
					return t;
				}
			}
			return nullptr;
		}
		virtual TASK* Message( const char* p, const MESSAGE_WORK<TYPE>* w ){ return w ? Message( p, w->info, w->step, w->rmax ) : nullptr;	}
		virtual TASK* Message( const char* p, const MESSAGE_WORK<TYPE>& w ){ return     Message( p,  w.info,  w.step,  w.rmax );			}

	public:
		virtual void Clear( const char* p = nullptr ){

			if ( p ) {
				task[p] = nullptr;
			} else {
				for ( auto&& [s,d] : task ) {
					( void )s;
					d = nullptr;
				}
			}
		}
	};
}

// End Of File
