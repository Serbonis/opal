#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

#include "Std/str_s.hpp"
#include "Std/mem_s.hpp"
#include "Std/name.hpp"

//========================================
// タスクシステム
//========================================
namespace opal {
	class TASQ;
	class TASK;
	class LINX;
}

//----------------------------------------
// タスククラス
//----------------------------------------
namespace opal {
	class TASK : private NOCOPY {
		friend class LINX;

	public:
		using SELF = std::function<void(TASK*)>;

	public:
		enum {
			PRIO_MIN = 0x00000000,
			PRIO_MAX = 0xffffffff,
		};

	protected:
		explicit TASK();

	public:
		virtual ~TASK();

	private:
		std::shared_ptr<TASQ>	tasq;

	private:
		struct FAT {
			bool	flag{false};
			UINT	time{0};
		};
		struct TASK_ {
			TASK_();

			NAME	name;	// 識別名
			UINT	info;	// 情報表示
			FAT		live;	// 生存フラグ
			FAT		kill;	// 死亡フラグ
			FAT		retire;	// 引退フラグ
			FAT		sleep;	// 休眠フラグ
		}	task_;

		std::unordered_map<std::string,SELF>	self;

	public:
		virtual void Init( const char* = nullptr );
		virtual void Free( void );

	public:
		virtual void Register( LINX* = nullptr );
		virtual void Release(  LINX* = nullptr );

	public:
		virtual void Open(  const char*, LINX* );
		virtual void Open(  const char* );
		virtual void Open(  LINX* );
		virtual void Close( LINX* );
		virtual void Open(  void );
		virtual void Close( void );

	public:
		virtual void Self( SELF );
		virtual auto Self( void ) const->SELF;

		virtual void Self( const std::string&, SELF );
		virtual auto Self( const std::string& ) const->SELF;

		virtual void UnSelf( void ) final;

	public:
		virtual auto Task( void ) const->TASQ*;

	public:
		virtual auto Next( void ) const->TASK*;
		virtual auto Last( void ) const->TASK*;

		template<typename TYPE>TYPE* Next( void ) const { return dynamic_cast<TYPE*>( Next() );	}
		template<typename TYPE>TYPE* Last( void ) const { return dynamic_cast<TYPE*>( Last() );	}

	public:
		virtual auto Linx( void  ) const->LINX*;
		virtual void Linx( LINX* );

	public:
		virtual auto Root( void ) const->TASK*;
		virtual auto Root( int  ) const->TASK*;

	public:
		virtual auto Branch( void  ) const->LINX*;
		virtual void Branch( LINX* );
		virtual auto Prune(  void  ) const->bool;
		virtual void Prune(  bool  );

	public:
		virtual bool Live(   void ) const;
		virtual bool Kill(   void ) const;
		virtual bool Retire( void ) const;
		virtual bool Sleep(  void ) const;

		virtual void Kill(   int );
		virtual void Retire( int );
		virtual void Sleep(  int );

		virtual UINT LiveTime(   void ) const;
		virtual UINT KillTime(   void ) const;
		virtual UINT RetireTime( void ) const;
		virtual UINT SleepTime(  void ) const;

		virtual bool LiveTime(   bool );
		virtual bool KillTime(   bool );
		virtual bool RetireTime( bool );
		virtual bool SleepTime(  bool );

		virtual bool Die( int = 0 );

	public:
		virtual UINT Info( void ) const;
		virtual void Info( UINT );

	public:
		virtual auto ID( void ) const->const char*;
		virtual void ID( const char* );

	public:
		virtual UINT Prio( void ) const;
		virtual void Prio( UINT, bool = true );

	public:
		virtual void TaskInfo(  void ) const;
		virtual void TaskInit(  void );
		virtual void TaskEnter( void );
		virtual void TaskFunc(  void );
		virtual void TaskBegin( void );
		virtual void TaskMain(  void );
		virtual void TaskMain(  UINT );
		virtual void TaskEnd(   void );
		virtual void TaskExit(  void );
		virtual void TaskFree(  void );
	};
}

//----------------------------------------
// タスク用コンテナ
//----------------------------------------
namespace opal {
	class TASQ final {
	public:
		explicit TASQ( TASK* = nullptr );

	private:
		TASK*	task;	// 管理TASK
		TASQ*	next;	// 次のTASQ
		TASQ*	last;	// 前のTASQ
		LINX*	linx;	// 所属するLINX
		LINX*	branch;	// LINX分岐
		bool	prune;	// LINX剪定
		UINT	prio;	// 優先順位

	public:
		void Register( LINX* = nullptr );
		void Release(  LINX* = nullptr );

	public:
		auto Task( void ) const->TASK*;
		void Task( TASK* );

		template<typename TYPE>TYPE* Task( void ) const { return dynamic_cast<TYPE*>( task );	}

	public:
		auto Next( void ) const->TASQ*;
		auto Last( void ) const->TASQ*;

		void Next( TASQ* );
		void Last( TASQ* );

		template<typename TYPE>TYPE* Last( void ) const { return last ? dynamic_cast<TYPE*>( last->task ) : nullptr;	}
		template<typename TYPE>TYPE* Next( void ) const { return next ? dynamic_cast<TYPE*>( next->task ) : nullptr;	}

	public:
		auto Linx( void  ) const->LINX*;
		void Linx( LINX* );

	public:
		auto Root( void ) const->TASK*;
		auto Root( int  ) const->TASK*;

	public:
		auto Branch( void  ) const->LINX*;
		void Branch( LINX* );
		auto Prune(  void  ) const->bool;
		void Prune(  bool  );

	public:
		UINT Prio( void ) const;
		void Prio( UINT, bool = true );
	};
}

// End Of File
