#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Std/str_s.hpp"
#include "Std/name.hpp"

//========================================
// 汎用ファイル管理
//========================================
namespace opal {
	class FILEINFO {
	public:
		FILEINFO();
		virtual ~FILEINFO() = default;

	public:
		FILEINFO( FILEINFO& );
		FILEINFO& operator =( FILEINFO& );

	private:
		NAME			path;
		NAME			name;
		const void*		adrs;
		size_t			size;

	public:
		virtual void Path( const char* );
		virtual void Name( const char* );
		virtual void Adrs( const void* );
		virtual void Size(      size_t );

	public:
		virtual const char* Path( void ) const;
		virtual const char* Name( void ) const;
		virtual const void* Adrs( void ) const;
		virtual size_t      Size( void ) const;

	public:
		virtual void Info( const char*, const char*, const void*, size_t );
		virtual void Info( void );
	};
}

//========================================
// 汎用ファイル操作関数
//========================================
namespace opal {
	extern int         GetFilePath( const char* path );
	extern const char* GetFileName( const char* path );
	extern const char* GetFileKind( const char* path );
	extern NAME PathFile( const char* path, const char* file );
}

// End Of File
