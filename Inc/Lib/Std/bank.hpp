#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// データバンク:vector型
//========================================
namespace opal {
	template<typename TYPE> class VBANKV {
	public:
		explicit VBANKV() = default;
		virtual ~VBANKV() = default;

	protected:
		UINT						indx{0};
		mutable std::vector<TYPE>	bank;

	public:
		virtual void Index( UINT n ) { if ( const auto s = bank.size() ) { indx = n % s; } }
		virtual UINT Index( void   ) const { return indx; }

	public:
		virtual void Size( UINT n ) { bank.resize( n );	}
		virtual UINT Size( void ) const { return bank.size(); }

	protected:
		virtual void  Set_( UINT n, TYPE v ){ bank.resize( n+1 ); bank[n] = v;	}
		virtual TYPE* Get_( UINT n ) const { return ( n < bank.size() ) ? &bank[n] : nullptr;	}

	public:
		virtual void  Set( UINT n, TYPE  v = TYPE() ){ Set_( n,    v  );	}
		virtual void  Set(         TYPE  v = TYPE() ){ Set_( indx, v  );	}

		virtual TYPE* Get( UINT n ) const { return Get_( n    );	}
		virtual TYPE* Get( void   ) const { return Get_( indx );	}
	};
}

namespace opal {
	template<typename TYPE>class VBANKA : public VBANKV<TYPE> {
	public:
		using VBANKV<TYPE>::Set;

	protected:
		virtual void Set( TYPE v = TYPE() ) override {

			const auto	s = VBANKV<TYPE>::Size();

			for ( auto i = 0UL; i < s; i++ ) {
				VBANKV<TYPE>::Set( i, v );
			}
		}
	};
}

//========================================
// データバンク:map型
//========================================
namespace opal {
	template<typename TYPE> class VBANKM {
	public:
		explicit VBANKM() = default;
		virtual ~VBANKM() = default;

	protected:
		std::string										indx{""};
		mutable std::unordered_map<std::string,TYPE>	bank;

	public:
		virtual void Index( const char* p ) { if ( !p ) { p = ""; } if ( bank.count( p ) ) { indx = p; } }
		virtual const char* Index( void   ) const { return indx.c_str(); }

	public:
		virtual size_t Size( void ) const { return bank.size();	}

	private:
		virtual void  Set_( const char* p, TYPE v ){ if ( !p ) { p = ""; } bank[p] = v;	}
		virtual TYPE* Get_( const char* p  ) const { if ( !p ) { p = ""; } return bank.count( p ) ? &bank.at( p ) : nullptr;	}

	public:
		virtual void  Set( const char* p, TYPE  v = TYPE() ){ Set_( p,            v  );	}
		virtual void  Set(                TYPE  v = TYPE() ){ Set_( indx.c_str(), v  );	}

		virtual TYPE* Get( const char* p ) const { return Get_( p );				}
		virtual TYPE* Get( void          ) const { return Get_( indx.c_str() );	}
	};
}

//========================================
// ポインターバンク:vector型
//========================================
namespace opal {
	template<typename TYPE> class PBANKV {
	public:
		explicit PBANKV() = default;
		virtual ~PBANKV() = default;

	protected:
		UINT				indx{0};
		std::vector<TYPE*>	bank;

	public:
		virtual void Index( UINT n ) { if ( const auto s = bank.size() ) { indx = n % s; } }
		virtual UINT Index( void   ) const { return indx; }

	public:
		virtual void Size( UINT n ) { bank.resize( n );	}
		virtual UINT Size( void ) const { return bank.size(); }

	protected:
		virtual void  Set_( UINT n, TYPE* v ){ bank.resize( n+1 ); bank[n] = v;	}
		virtual TYPE* Get_( UINT n ) const { return ( n < bank.size() ) ? bank[n] : nullptr;	}

	public:
		virtual void  Set( UINT n, TYPE& v           ){ Set_( n,    &v );	}
		virtual void  Set(         TYPE& v           ){ Set_( indx, &v );	}
		virtual void  Set( UINT n, TYPE* v = nullptr ){ Set_( n,     v );	}
		virtual void  Set(         TYPE* v = nullptr ){ Set_( indx,  v );	}

		virtual TYPE* Get( UINT n ) const { return Get_( n    );	}
		virtual TYPE* Get( void   ) const { return Get_( indx );	}
	};
}

//========================================
// ポインターバンク:map型
//========================================
namespace opal {
	template<typename TYPE> class PBANKM {
	public:
		explicit PBANKM() = default;
		virtual ~PBANKM() = default;

	protected:
		std::string								indx{""};
		std::unordered_map<std::string,TYPE*>	bank;

	public:
		virtual void Index( const char* p ) { if ( !p ) { p = ""; } if ( bank.count( p ) ) { indx = p; } }
		virtual const char* Index( void   ) const { return indx.c_str(); }

	public:
		virtual size_t Size( void ) const { return bank.size();	}

	private:
		virtual void  Set_( const char* p, TYPE* v ){ if ( !p ) { p = ""; } bank[p] = v;	}
		virtual TYPE* Get_( const char* p   ) const { if ( !p ) { p = ""; } return bank.count( p ) ? bank.at( p ) : nullptr;	}

	public:
		virtual void  Set( const char* p, TYPE& v           ){ Set_( p,            &v );	}
		virtual void  Set(                TYPE& v           ){ Set_( indx.c_str(), &v ); 	}
		virtual void  Set( const char* p, TYPE* v = nullptr ){ Set_( p,             v );	}
		virtual void  Set(                TYPE* v = nullptr ){ Set_( indx.c_str(),  v ); 	}

		virtual TYPE* Get( const char* p ) const { return Get_( p );				}
		virtual TYPE* Get( void          ) const { return Get_( indx.c_str() );		}
	};
}

// End Of File
