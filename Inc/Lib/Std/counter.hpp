#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"

//========================================
// COUNTER
//========================================
namespace opal {
	class COUNTER {
	public:
		explicit COUNTER();
		virtual ~COUNTER();

	private:
		UINT	c;
		UINT	t;

	public:
		virtual void SetC( UINT n = 0 );
		virtual void SetT( UINT n = 0 );
		virtual UINT GetC( void ) const;
		virtual UINT GetT( void ) const;

	public:
		virtual UINT operator =( UINT n );
		virtual UINT operator ()( UINT n );

	public:
		virtual operator bool( void ) const;
		virtual COUNTER& operator ()( void );

	public:
		virtual UINT operator ++( void );
		virtual UINT operator --( void );
		virtual UINT operator ++( int  );
		virtual UINT operator --( int  );
	};
}

// End Of File
