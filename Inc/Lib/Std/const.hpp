#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/vector.hpp"
#include "Ext/matrix.hpp"
#include "Ext/quaternion.hpp"

//========================================
// 数学定数
//========================================
namespace opal {
	constexpr auto	OPAL_PI		= float{3.141592654f};
	constexpr auto	OPAL_SQ2	= float{1.41421356237309504880f};
	constexpr auto	OPAL_RSQ2	= 1.f/OPAL_SQ2;

	constexpr auto	OPAL_ORIGIN	= VECTOR3{ 0, 0, 0 };

	constexpr auto	OPAL_UNIT_X	= VECTOR3{ 1, 0, 0 };
	constexpr auto	OPAL_UNIT_Y	= VECTOR3{ 0, 1, 0 };
	constexpr auto	OPAL_UNIT_Z	= VECTOR3{ 0, 0, 1 };
	constexpr auto	OPAL_ZERO	= VECTOR3{ 0, 0, 0 };

	constexpr auto	OPAL_IDV	= VECTOR3{ 1, 1, 1 };
	constexpr auto	OPAL_IDM	= MATRIX{ 1, 0, 0, 0, 0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1 };
	constexpr auto	OPAL_IDQ	= QUATERNION{ 0, 0, 0, 1 };
}

//========================================
// 自然定数
//========================================
namespace opal {
	constexpr auto		OPAL_GRAVITY	= float{9.80665f};
}

// End Of File

