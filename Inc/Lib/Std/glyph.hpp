#pragma once
//========================================
// OPAL Game Library
//========================================
#include "msc.hpp"
#include <windows.h>
#include <tchar.h>
#include <vector>
#include <cstring>
#include <string>

//----------------------------------------
// GLYPH
//----------------------------------------
namespace opal {
	class GLYPH final {
	public:
		explicit GLYPH( LONG, int = GGO_GRAY8_BITMAP, const char* = nullptr );
		~GLYPH();

	private:
		LOGFONT		logfont;
		int			format;
		int			grad;

	private:
		HFONT		nft;
		HDC			hdc;
		HFONT		oft;

	private:
		TEXTMETRIC						tmetrix;
		std::vector<GLYPHMETRICS>		gmetrix;
		std::vector<std::vector<BYTE>>	buffer;
		std::vector<bool>				dbcs;

	public:
		void operator()( const char*,        int, BYTE*, DWORD, DWORD, DWORD );
		void operator()( const std::string&, int, BYTE*, DWORD, DWORD, DWORD );
		void operator()( std::nullptr_t,     int, BYTE*, DWORD, DWORD, DWORD );
		void operator()(                     int, BYTE*, DWORD, DWORD, DWORD );

		void operator()( const char*,        int, BYTE*, DWORD, DWORD );
		void operator()( const std::string&, int, BYTE*, DWORD, DWORD );
		void operator()( std::nullptr_t,     int, BYTE*, DWORD, DWORD );
		void operator()(                     int, BYTE*, DWORD, DWORD );

		void operator()( const char*        );
		void operator()( const std::string& );
		void operator()( std::nullptr_t     );

	public:
		void Register( void );
		void Release(  void );

	public:
		void back( int, BYTE*, DWORD, DWORD );
		void font( int, BYTE*, DWORD, DWORD );

	private:
		LOGFONT Logfont( LONG, const char* = nullptr );
		int Grad( int );
	};
}

// End Of File
