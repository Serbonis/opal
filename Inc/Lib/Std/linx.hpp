#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

#include "Std/name.hpp"

//========================================
// タスクリスト
//========================================
namespace opal {
	class TASK;
	class TASQ;

	class LINX : private NOCOPY {
		friend class TASQ;

	public:
		explicit LINX();
		virtual ~LINX();

	private:
		NAME	name;	// 名前
		TASQ*	index;	// 開始位置
		TASQ*	task;	// 接続位置
		UINT	elms;	// 登録数
		UINT	info;	// 情報表示

	public:
		virtual void Init( const char* = nullptr );
		virtual void Exec( void );
		virtual void Free( void );

	public:
		virtual auto Name( void ) const->const char*;
		virtual void Name( const char* );

	public:
		virtual UINT Info( void ) const;
		virtual void Info( UINT );

	public:
		virtual UINT Elements( void ) const;

	private:
		virtual void Task( TASQ* );
		virtual auto Task( void  ) const->TASQ*;

	public:
		template<typename TYPE>TYPE* Task( void ) const { return task ? dynamic_cast<TYPE*>( task->Task() ) : nullptr;	}

	public:
		virtual auto Head( void ) const->TASQ*;
		virtual auto Tail( void ) const->TASQ*;

		template<typename TYPE>TYPE* Head( void ) const { if ( const auto t = Head() ) { return dynamic_cast<TYPE*>( t->Task() ); } return nullptr;	}
		template<typename TYPE>TYPE* Tail( void ) const { if ( const auto t = Tail() ) { return dynamic_cast<TYPE*>( t->Task() ); } return nullptr;	}

	public:
		virtual auto Next( void )->LINX&;
		virtual auto Last( void )->LINX&;

	public:
		virtual void RegisterS( TASK* );
		virtual void RegisterQ( TASK* );
		virtual void Register(  TASK*, std::function<bool(UINT,UINT)> = nullptr );
		virtual void Release(   TASK* = nullptr );

	public:
		virtual void LinxMainBegin( void );
		virtual void LinxMainEnd(   void );
		virtual void LinxFuncBegin( void );
		virtual void LinxFuncEnd(   void );

	public:
		virtual void TaskInit(  void );
		virtual void TaskEnter( void );
		virtual void TaskFunc(  void );
		virtual void TaskMain(  void );
		virtual void TaskMain(  UINT );
		virtual void TaskInfo(  void );
		virtual void TaskExit(  void );
		virtual void TaskFree(  void );
		virtual void TaskKill(  void );

	public:
		virtual UINT TaskCount( void ) const;

	public:
		using QTASK = std::deque<TASK*>;
		virtual void TaskDestroy( void );
		virtual void TaskDestroy( const QTASK& );
		virtual void TaskRelease( void );

	public:
		using VTASK = std::vector<TASK*>;
		using SFUNC = std::function<bool(const TASK*)>;
		virtual UINT TaskSearch( VTASK& task, const SFUNC = nullptr );

	public:
		using EFUNC = std::function<void(TASK*)>;
		virtual void TaskEval( const EFUNC );
	};
}

// End Of File
