#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// サイズ:メンバー用
//========================================
//----------------------------------------
// SIZE1
//----------------------------------------
namespace opal {
	template<typename TYPE=float>struct SSIZE1 {
		constexpr          SSIZE1( const SSIZE1* s ) : w{ s ? s->w : 0 }{}
		constexpr explicit SSIZE1( TYPE w ) : w{w} {}
		constexpr          SSIZE1() : w{0} {}

		TYPE	w{0};

		constexpr explicit operator TYPE( void ) const { return w; }

		constexpr SSIZE1 operator +( void ) const { return SSIZE1{+w}; };
		constexpr SSIZE1 operator -( void ) const { return SSIZE1{-w}; };

		constexpr bool operator ()( TYPE x ) const { return ( 0 <= x && x < w ); }

		constexpr SSIZE1 operator +=( const SSIZE1& s ){ w += s.w; return *this;	}
		constexpr SSIZE1 operator -=( const SSIZE1& s ){ w -= s.w; return *this;	}
		constexpr SSIZE1 operator *=( float r ){ w *= r; return *this;	}
		constexpr SSIZE1 operator /=( float r ){ w /= r; return *this;	}

		constexpr TYPE Range( void   ) const { return static_cast<TYPE>( *this );	}
		constexpr bool Range( TYPE x ) const { return (*this)( x );	}
	};
#ifdef _MSC_VER
#else
	template<typename TYPE>using SSIZE1 = SSIZE1<TYPE>;
#endif

	using SSIZE1F = SSIZE1<float>;
	using SSIZE1I = SSIZE1<int>;
	using SSIZE1U = SSIZE1<UINT>;

	template<typename TYPE>  bool operator == ( const SSIZE1<TYPE >& a, const SSIZE1<TYPE >& b ){ return a.w == b.w;	}
	template<typename TYPE>  bool operator != ( const SSIZE1<TYPE >& a, const SSIZE1<TYPE >& b ){ return a.w != b.w;	}
	template<typename>inline bool operator == ( const SSIZE1<float>& a, const SSIZE1<float>& b ){ return FEQU( a.w, b.w );	}
	template<typename>inline bool operator != ( const SSIZE1<float>& a, const SSIZE1<float>& b ){ return FNEQ( a.w, b.w );	}

	template<typename TYPE>SSIZE1<TYPE> operator + ( const SSIZE1<TYPE>& a, const SSIZE1<TYPE>& b ){ return SSIZE1<TYPE>( a.w + b.w );	}
	template<typename TYPE>SSIZE1<TYPE> operator - ( const SSIZE1<TYPE>& a, const SSIZE1<TYPE>& b ){ return SSIZE1<TYPE>( a.w - b.w );	}
	template<typename TYPE>SSIZE1<TYPE> operator * ( const SSIZE1<TYPE>& s, float v ){ return SSIZE1<TYPE>( s.w * v );	}
	template<typename TYPE>SSIZE1<TYPE> operator * ( float v, const SSIZE1<TYPE>& s ){ return SSIZE1<TYPE>( s.w * v );	}
	template<typename TYPE>SSIZE1<TYPE> operator / ( const SSIZE1<TYPE>& s, float v ){ return SSIZE1<TYPE>( s.w / v );	}
}

//----------------------------------------
// SIZE2
//----------------------------------------
namespace opal {
	template<typename TYPE=float>struct SSIZE2 {
		constexpr          SSIZE2( const SSIZE2* s ) : w{ s ? s->w : 0 },h{ s ? s->h : 0 }{}
		constexpr          SSIZE2( const std::pair<TYPE,TYPE>& s ) : w{s.first},h{s.second}{}
		constexpr          SSIZE2( TYPE w, TYPE h ) : w{w},h{h} {}
		constexpr explicit SSIZE2( TYPE x ) : w{x},h{x} {}
		constexpr          SSIZE2() : w{0},h{0} {}

		TYPE	w{0};
		TYPE	h{0};

		constexpr explicit operator TYPE( void ) const { return w * h; }

		constexpr SSIZE2 operator +( void ) const { return SSIZE2{+w,+h}; };
		constexpr SSIZE2 operator -( void ) const { return SSIZE2{-w,-h}; };

		constexpr bool operator ()( TYPE x, TYPE y ) const { return ( ( 0 <= x && x < w ) && ( 0 <= y && y < h ) ); }

		constexpr SSIZE2 operator +=( const SSIZE2& s ){ w += s.w; h += s.h; return *this;	}
		constexpr SSIZE2 operator -=( const SSIZE2& s ){ w -= s.w; h -= s.h; return *this;	}
		constexpr SSIZE2 operator *=( float r ){ w *= r; h *= r; return *this;	}
		constexpr SSIZE2 operator /=( float r ){ w /= r; h /= r; return *this;	}

		constexpr TYPE Range( void ) const { return static_cast<TYPE>( *this );	}
		constexpr bool Range( TYPE x, TYPE y ) const { return (*this)( x, y );	}
	};

#ifdef _MSC_VER
#else
	template<typename TYPE>using SSIZE2 = SSIZE2<TYPE>;
#endif

	using SSIZE2F = SSIZE2<float>;
	using SSIZE2I = SSIZE2<int>;
	using SSIZE2U = SSIZE2<UINT>;

	template<typename TYPE>  bool operator == ( const SSIZE2<TYPE >& a, const SSIZE2<TYPE >& b ){ return ( a.w == b.w ) && ( a.h == b.h );	}
	template<typename TYPE>  bool operator != ( const SSIZE2<TYPE >& a, const SSIZE2<TYPE >& b ){ return ( a.w != b.w ) || ( a.h != b.h );	}
	template<typename>inline bool operator == ( const SSIZE2<float>& a, const SSIZE2<float>& b ){ return FEQU( a.w, b.w ) && FEQU( a.h, b.h );	}
	template<typename>inline bool operator != ( const SSIZE2<float>& a, const SSIZE2<float>& b ){ return FNEQ( a.w, b.w ) || FNEQ( a.h, b.h );	}

	template<typename TYPE>SSIZE2<TYPE> operator + ( const SSIZE2<TYPE>& a, const SSIZE2<TYPE>& b ){ return SSIZE2<TYPE>( a.w + b.w, a.h + b.h );	}
	template<typename TYPE>SSIZE2<TYPE> operator - ( const SSIZE2<TYPE>& a, const SSIZE2<TYPE>& b ){ return SSIZE2<TYPE>( a.w - b.w, a.h - b.h );	}
	template<typename TYPE>SSIZE2<TYPE> operator * ( const SSIZE2<TYPE>& s, float v ){ return SSIZE2<TYPE>( s.w * v, s.h * v );	}
	template<typename TYPE>SSIZE2<TYPE> operator * ( float v, const SSIZE2<TYPE>& s ){ return SSIZE2<TYPE>( s.w * v, s.h * v );	}
	template<typename TYPE>SSIZE2<TYPE> operator / ( const SSIZE2<TYPE>& s, float v ){ return SSIZE2<TYPE>( s.w / v, s.h / v );	}
}

//----------------------------------------
// SIZE3
//----------------------------------------
namespace opal {
	template<typename TYPE=float>struct SSIZE3 {
		constexpr          SSIZE3( const SSIZE3* s ) : w{ s ? s->w : 0 },h{ s ? s->h : 0 },d{ s ? s->d : 0 }{}
		constexpr          SSIZE3( const std::tuple<TYPE,TYPE,TYPE>& s ) : w{std::get<0>(s)},h{std::get<1>(s)},d{std::get<2>(s)}{}
		constexpr          SSIZE3( TYPE w, TYPE h, TYPE d ) : w{w},h{h},d{d} {}
		constexpr explicit SSIZE3( TYPE x                 ) : w{x},h{x},d{x} {}
		constexpr          SSIZE3() : w{0},h{0},d{0} {}

		TYPE	w{0};
		TYPE	h{0};
		TYPE	d{0};

		constexpr explicit operator TYPE( void ) const { return w * h * d; }

		constexpr SSIZE3 operator +( void ) const { return SSIZE3{+w,+h,+d}; };
		constexpr SSIZE3 operator -( void ) const { return SSIZE3{-w,-h,-d}; };

		constexpr bool operator ()( TYPE x, TYPE y, TYPE z ) const { return ( ( 0 <= x && x < w ) &&
																			  ( 0 <= y && y < h ) &&
																			  ( 0 <= z && z < d ) ); }

		constexpr SSIZE3 operator +=( const SSIZE3& s ){ w += s.w; h += s.h; d += s.d; return *this;	}
		constexpr SSIZE3 operator -=( const SSIZE3& s ){ w -= s.w; h -= s.h; d -= s.d; return *this;	}
		constexpr SSIZE3 operator *=( float r ){ w *= r; h *= r; d *= r; return *this;	}
		constexpr SSIZE3 operator /=( float r ){ w /= r; h /= r; d /= r; return *this;	}

		constexpr TYPE Range( void ) const { return static_cast<TYPE>( *this );	}
		constexpr bool Range( TYPE x, TYPE y, TYPE z ) const { return (*this)( x, y, z );	}
	};

#ifdef _MSC_VER
#else
	template<typename TYPE>using SSIZE3 = SSIZE3<TYPE>;
#endif

	using SSIZE3F = SSIZE3<float>;
	using SSIZE3I = SSIZE3<int>;
	using SSIZE3U = SSIZE3<UINT>;

	template<typename TYPE>  bool operator == ( const SSIZE3<TYPE >& a, const SSIZE3<TYPE >& b ){ return ( a.w == b.w ) && ( a.h == b.h ) && ( a.d == b.d );	}
	template<typename TYPE>  bool operator != ( const SSIZE3<TYPE >& a, const SSIZE3<TYPE >& b ){ return ( a.w != b.w ) || ( a.h != b.h ) || ( a.d != b.d );	}
	template<typename>inline bool operator == ( const SSIZE3<float>& a, const SSIZE3<float>& b ){ return FEQU( a.w, b.w ) && FEQU( a.h, b.h ) && FEQU( a.d, b.d );	}
	template<typename>inline bool operator != ( const SSIZE3<float>& a, const SSIZE3<float>& b ){ return FNEQ( a.w, b.w ) || FNEQ( a.h, b.h ) || FNEQ( a.d, b.d );	}

	template<typename TYPE>SSIZE3<TYPE> operator + ( const SSIZE3<TYPE>& a, const SSIZE3<TYPE>& b ){ return SSIZE3<TYPE>( a.w + b.w, a.h + b.h );	}
	template<typename TYPE>SSIZE3<TYPE> operator - ( const SSIZE3<TYPE>& a, const SSIZE3<TYPE>& b ){ return SSIZE3<TYPE>( a.w - b.w, a.h - b.h );	}
	template<typename TYPE>SSIZE3<TYPE> operator * ( const SSIZE3<TYPE>& s, float v ){ return SSIZE3<TYPE>( s.w * v, s.h * v );	}
	template<typename TYPE>SSIZE3<TYPE> operator * ( float v, const SSIZE3<TYPE>& s ){ return SSIZE3<TYPE>( s.w * v, s.h * v );	}
	template<typename TYPE>SSIZE3<TYPE> operator / ( const SSIZE3<TYPE>& s, float v ){ return SSIZE3<TYPE>( s.w / v, s.h / v );	}
}

//========================================
// サイズ:継承用
//========================================
//----------------------------------------
// SIZE1
//----------------------------------------
namespace opal {
	template<typename TYPE=float>class CSIZE1 : public SSIZE1<TYPE> {
	public:
		using SSIZE1<TYPE>::SSIZE1;
		explicit CSIZE1(){}
		virtual ~CSIZE1(){}

	public:
		virtual void W( TYPE w ){ this->w = w; }
		virtual TYPE W( void ) const { return this->w; }

	public:
		virtual void SetSize_( TYPE  w ){ this->w = w; }
		virtual void GetSize_( TYPE* w ) const { if ( w ) { *w = this->w; } }

	public:
		virtual void SetSize(  TYPE w ){ SetSize_( w );	}
		virtual void SetSizeW( TYPE w ){ SetSize_( w );	}
		virtual void SetSize(  void   ){ SetSize_( TYPE{0} );	}

		virtual auto GetSize(  void    ) const->SSIZE1<TYPE>{ SSIZE1<TYPE> v; GetSize_( &v.w ); return v;	}
		virtual void GetSize(  TYPE* w ) const { GetSize_(  w );	}
		virtual void GetSize(  TYPE& w ) const { GetSize_( &w );	}
		virtual void GetSizeW( TYPE* w ) const { GetSize_(  w );	}
		virtual void GetSizeW( TYPE& w ) const { GetSize_( &w );	}
		virtual TYPE GetSizeW( void    ) const { TYPE w; GetSize_( &w ); return w; }

	public:
		virtual void SetSize(  const CSIZE1*       v ){   if ( v ) { SetSize_( v->w ); }	}
		virtual void SetSize(  const SSIZE1<TYPE>* v ){   if ( v ) { SetSize_( v->w ); }	}
		virtual void SetSizeW( const CSIZE1*       v ){   if ( v ) { SetSize_( v->w ); }	}
		virtual void SetSizeW( const SSIZE1<TYPE>* v ){   if ( v ) { SetSize_( v->w ); }	}
		virtual void SetSize(  const CSIZE1&       v ){              SetSize_( v.w  );		}
		virtual void SetSize(  const SSIZE1<TYPE>& v ){              SetSize_( v.w  );		}
		virtual void SetSizeW( const CSIZE1&       v ){              SetSize_( v.w  );		}
		virtual void SetSizeW( const SSIZE1<TYPE>& v ){              SetSize_( v.w  );		}

		virtual void GetSize(  CSIZE1*        v ) const { if ( v ) { GetSize_( &v->w ); }	}
		virtual void GetSize(  SSIZE1<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w ); }	}
		virtual void GetSizeW( CSIZE1*        v ) const { if ( v ) { GetSize_( &v->w ); }	}
		virtual void GetSizeW( SSIZE1<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w ); }	}
		virtual void GetSize(  CSIZE1&        v ) const {            GetSize_( &v.w  );		}
		virtual void GetSize(  SSIZE1<TYPE>&  v ) const {            GetSize_( &v.w  );		}
		virtual void GetSizeW( CSIZE1&        v ) const {            GetSize_( &v.w  );		}
		virtual void GetSizeW( SSIZE1<TYPE>&  v ) const {            GetSize_( &v.w  );		}
	};

#ifdef _MSC_VER
#else
	template<typename TYPE>using CSIZE1 = CSIZE1<TYPE>;
#endif

	using CSIZE1F = CSIZE1<float>;
	using CSIZE1I = CSIZE1<int>;
	using CSIZE1U = CSIZE1<UINT>;
}

//----------------------------------------
// SIZE2
//----------------------------------------
namespace opal {
	template<typename TYPE=float>class CSIZE2 : public SSIZE2<TYPE> {
	public:
		using SSIZE2<TYPE>::SSIZE2;
		explicit CSIZE2(){}
		virtual ~CSIZE2(){}

	public:
		virtual void W( TYPE w ){ this->w = w; }
		virtual void H( TYPE h ){ this->h = h; }
		virtual TYPE W( void ) const { return this->w; }
		virtual TYPE H( void ) const { return this->h; }

	public:
		virtual void SetSize_( TYPE w, TYPE h ){

			this->w = w;
			this->h = h;
		}

		virtual void GetSize_( TYPE* w, TYPE* h ) const {

			if ( w ) { *w = this->w; }
			if ( h ) { *h = this->h; }
		}

	public:
		virtual void SetSize(  TYPE w, TYPE h ){ SetSize_( w, h );	}
		virtual void SetSize(  TYPE s         ){ SetSize_( s, s );	}
		virtual void SetSizeW( TYPE w         ){ SetSize_( w, this->h );	}
		virtual void SetSizeH( TYPE h         ){ SetSize_( this->w, h );	}
		virtual void SetSize(  void ){ SetSize_( TYPE{0}, TYPE{0} );	}

		virtual auto GetSize(  void    ) const->SSIZE2<TYPE>{ SSIZE2<TYPE> v; GetSize_( &v.w, &v.h ); return v;	}
		virtual void GetSize(  TYPE* w, TYPE* h ) const { GetSize_(  w,  h ); 		}
		virtual void GetSize(  TYPE& w, TYPE& h ) const { GetSize_( &w, &h ); 		}
		virtual void GetSizeW( TYPE* w ) const {         GetSize_(  w, nullptr );	}
		virtual void GetSizeW( TYPE& w ) const {         GetSize_( &w, nullptr );	}
		virtual void GetSizeH( TYPE* h ) const {         GetSize_( nullptr,  h );	}
		virtual void GetSizeH( TYPE& h ) const {         GetSize_( nullptr, &h );	}
		virtual TYPE GetSizeW( void    ) const { TYPE w; GetSize_( &w, nullptr ); return w; }
		virtual TYPE GetSizeH( void    ) const { TYPE h; GetSize_( nullptr, &h ); return h; }

	public:
		virtual void SetSize(  const CSIZE2*       v ){   if ( v ) { SetSize_( v->w,    v->h    ); }	}
		virtual void SetSize(  const SSIZE2<TYPE>* v ){   if ( v ) { SetSize_( v->w,    v->h    ); }	}
		virtual void SetSizeW( const CSIZE2*       v ){   if ( v ) { SetSize_( v->w,    this->h ); }	}
		virtual void SetSizeW( const SSIZE2<TYPE>* v ){   if ( v ) { SetSize_( v->w,    this->h ); }	}
		virtual void SetSizeH( const CSIZE2*       v ){   if ( v ) { SetSize_( this->w, v->h    ); }	}
		virtual void SetSizeH( const SSIZE2<TYPE>* v ){   if ( v ) { SetSize_( this->w, v->h    ); }	}
		virtual void SetSize(  const CSIZE2&       v ){              SetSize_( v.w,     v.h     ); 		}
		virtual void SetSize(  const SSIZE2<TYPE>& v ){              SetSize_( v.w,     v.h     ); 		}
		virtual void SetSizeW( const CSIZE2&       v ){              SetSize_( v.w,     this->h ); 		}
		virtual void SetSizeW( const SSIZE2<TYPE>& v ){              SetSize_( v.w,     this->h ); 		}
		virtual void SetSizeH( const CSIZE2&       v ){              SetSize_( this->w, v.h     ); 		}
		virtual void SetSizeH( const SSIZE2<TYPE>& v ){              SetSize_( this->w, v.h     ); 		}

		virtual void GetSize(  CSIZE2*        v ) const { if ( v ) { GetSize_( &v->w,   &v->h   ); }	}
		virtual void GetSize(  SSIZE2<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w,   &v->h   ); }	}
		virtual void GetSizeW( CSIZE2*        v ) const { if ( v ) { GetSize_( &v->w,   nullptr ); }	}
		virtual void GetSizeW( SSIZE2<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w,   nullptr ); }	}
		virtual void GetSizeH( CSIZE2*        v ) const { if ( v ) { GetSize_( nullptr, &v->h   ); }	}
		virtual void GetSizeH( SSIZE2<TYPE>*  v ) const { if ( v ) { GetSize_( nullptr, &v->h   ); }	}
		virtual void GetSize(  CSIZE2&        v ) const {            GetSize_( &v.w,    &v.h    );		}
		virtual void GetSize(  SSIZE2<TYPE>&  v ) const {            GetSize_( &v.w,    &v.h    );		}
		virtual void GetSizeW( CSIZE2&        v ) const {            GetSize_( &v.w,    nullptr );		}
		virtual void GetSizeW( SSIZE2<TYPE>&  v ) const {            GetSize_( &v.w,    nullptr );		}
		virtual void GetSizeH( CSIZE2&        v ) const {            GetSize_( nullptr, &v.h    );		}
		virtual void GetSizeH( SSIZE2<TYPE>&  v ) const {            GetSize_( nullptr, &v.h    );		}
	};

#ifdef _MSC_VER
#else
	template<typename TYPE>using CSIZE2 = CSIZE2<TYPE>;
#endif

	using CSIZE2F = CSIZE2<float>;
	using CSIZE2I = CSIZE2<int>;
	using CSIZE2U = CSIZE2<UINT>;
}

//----------------------------------------
// SIZE3
//----------------------------------------
namespace opal {
	template<typename TYPE=float>class CSIZE3 : public SSIZE3<TYPE> {
	public:
		using SSIZE3<TYPE>::SSIZE3;
		explicit CSIZE3(){}
		virtual ~CSIZE3(){}

	public:
		virtual void W( TYPE w ){ this->w = w; }
		virtual void H( TYPE h ){ this->h = h; }
		virtual void D( TYPE d ){ this->d = d; }
		virtual TYPE W( void ) const { return this->w; }
		virtual TYPE H( void ) const { return this->h; }
		virtual TYPE D( void ) const { return this->d; }

	public:
		virtual void SetSize_( TYPE w, TYPE h, TYPE d ){

			this->w = w;
			this->h = h;
			this->d = d;
		}

		virtual void GetSize_( TYPE* w, TYPE* h, TYPE* d ) const {

			if ( w ) { *w = this->w; }
			if ( h ) { *h = this->h; }
			if ( d ) { *d = this->d; }
		}

	public:
		virtual void SetSize(  TYPE w, TYPE h, TYPE d ){ SetSize_( w, h, d ); }
		virtual void SetSize(  TYPE s                 ){ SetSize_( s, s, s ); }
		virtual void SetSizeW( TYPE w                 ){ SetSize_( w, this->h, this->d ); }
		virtual void SetSizeH( TYPE h                 ){ SetSize_( this->w, h, this->d ); }
		virtual void SetSizeD( TYPE d                 ){ SetSize_( this->w, this->h, d ); }
		virtual void SetSize(  void ){ SetSize_( TYPE{0}, TYPE{0}, TYPE{0} );	}

		virtual auto GetSize(  void    ) const->SSIZE3<TYPE>{ SSIZE3<TYPE> v; GetSize_( &v.w, &v.h, &v.d ); return v; }
		virtual void GetSize(  TYPE* w, TYPE* h, TYPE* d ) const { GetSize_(  w,  h,  d ); }
		virtual void GetSize(  TYPE& w, TYPE& h, TYPE& d ) const { GetSize_( &w, &h, &d ); }
		virtual void GetSizeW( TYPE* w ) const {         GetSize_(  w, nullptr, nullptr ); }
		virtual void GetSizeW( TYPE& w ) const {         GetSize_( &w, nullptr, nullptr ); }
		virtual void GetSizeH( TYPE* h ) const {         GetSize_( nullptr,  h, nullptr ); }
		virtual void GetSizeH( TYPE& h ) const {         GetSize_( nullptr, &h, nullptr ); }
		virtual void GetSizeD( TYPE* d ) const {         GetSize_( nullptr, nullptr,  d ); }
		virtual void GetSizeD( TYPE& d ) const {         GetSize_( nullptr, nullptr, &d ); }
		virtual TYPE GetSizeW( void    ) const { TYPE w; GetSize_( &w, nullptr, nullptr ); return w; }
		virtual TYPE GetSizeH( void    ) const { TYPE h; GetSize_( nullptr, &h, nullptr ); return h; }
		virtual TYPE GetSizeD( void    ) const { TYPE d; GetSize_( nullptr, nullptr, &d ); return d; }

	public:
		virtual void SetSize(  const CSIZE3*       v ){   if ( v ) { SetSize_( v->w,    v->h,    v->d    ); }	}
		virtual void SetSize(  const SSIZE3<TYPE>* v ){   if ( v ) { SetSize_( v->w,    v->h,    v->d    ); }	}
		virtual void SetSizeW( const CSIZE3*       v ){   if ( v ) { SetSize_( v->w,    this->h, this->d ); }	}
		virtual void SetSizeW( const SSIZE3<TYPE>* v ){   if ( v ) { SetSize_( v->w,    this->h, this->d ); }	}
		virtual void SetSizeH( const CSIZE3*       v ){   if ( v ) { SetSize_( this->w, v->h,    this->d ); }	}
		virtual void SetSizeH( const SSIZE3<TYPE>* v ){   if ( v ) { SetSize_( this->w, v->h,    this->d ); }	}
		virtual void SetSizeD( const CSIZE3*       v ){   if ( v ) { SetSize_( this->w, this->h, v->d    ); }	}
		virtual void SetSizeD( const SSIZE3<TYPE>* v ){   if ( v ) { SetSize_( this->w, this->h, v->d    ); }	}
		virtual void SetSize(  const CSIZE3&       v ){              SetSize_( v.w,     v.h,     v.d     );		}
		virtual void SetSize(  const SSIZE3<TYPE>& v ){              SetSize_( v.w,     v.h,     v.d     );		}
		virtual void SetSizeW( const CSIZE3&       v ){              SetSize_( v.w,     this->h, this->d );		}
		virtual void SetSizeW( const SSIZE3<TYPE>& v ){              SetSize_( v.w,     this->h, this->d );		}
		virtual void SetSizeH( const CSIZE3&       v ){              SetSize_( this->w, v.h,     this->d );		}
		virtual void SetSizeH( const SSIZE3<TYPE>& v ){              SetSize_( this->w, v.h,     this->d );		}
		virtual void SetSizeD( const CSIZE3&       v ){              SetSize_( this->w, this->h, v.d     );		}
		virtual void SetSizeD( const SSIZE3<TYPE>& v ){              SetSize_( this->w, this->h, v.d     );		}

		virtual void GetSize(  CSIZE3*        v ) const { if ( v ) { GetSize_( &v->w,   &v->h,   &v->d   ); }	}
		virtual void GetSize(  SSIZE3<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w,   &v->h,   &v->d   ); }	}
		virtual void GetSizeW( CSIZE3*        v ) const { if ( v ) { GetSize_( &v->w,   nullptr, nullptr ); }	}
		virtual void GetSizeW( SSIZE3<TYPE>*  v ) const { if ( v ) { GetSize_( &v->w,   nullptr, nullptr ); }	}
		virtual void GetSizeH( CSIZE3*        v ) const { if ( v ) { GetSize_( nullptr, &v->h,   nullptr ); }	}
		virtual void GetSizeH( SSIZE3<TYPE>*  v ) const { if ( v ) { GetSize_( nullptr, &v->h,   nullptr ); }	}
		virtual void GetSizeD( CSIZE3*        v ) const { if ( v ) { GetSize_( nullptr, nullptr, &v->d   ); }	}
		virtual void GetSizeD( SSIZE3<TYPE>*  v ) const { if ( v ) { GetSize_( nullptr, nullptr, &v->d   ); } 	}
		virtual void GetSize(  CSIZE3&        v ) const {            GetSize_( &v.w,    &v.h,    &v.d    );		}
		virtual void GetSize(  SSIZE3<TYPE>&  v ) const {            GetSize_( &v.w,    &v.h,    &v.d    );		}
		virtual void GetSizeW( CSIZE3&        v ) const {            GetSize_( &v.w,    nullptr, nullptr );		}
		virtual void GetSizeW( SSIZE3<TYPE>&  v ) const {            GetSize_( &v.w,    nullptr, nullptr );		}
		virtual void GetSizeH( CSIZE3&        v ) const {            GetSize_( nullptr, &v.h,    nullptr );		}
		virtual void GetSizeH( SSIZE3<TYPE>&  v ) const {            GetSize_( nullptr, &v.h,    nullptr );		}
		virtual void GetSizeD( CSIZE3&        v ) const {            GetSize_( nullptr, nullptr, &v.d    );		}
		virtual void GetSizeD( SSIZE3<TYPE>&  v ) const {            GetSize_( nullptr, nullptr, &v.d    );		}
	};

#ifdef _MSC_VER
#else
	template<typename TYPE>using CSIZE3 = CSIZE3<TYPE>;
#endif

	using CSIZE3F = CSIZE3<float>;
	using CSIZE3I = CSIZE3<int>;
	using CSIZE3U = CSIZE3<UINT>;
}

//========================================
// デフォルト
//========================================
namespace opal {
	using SSIZE		= SSIZE2F;
	using SSIZEF	= SSIZE2F;
	using SSIZEU	= SSIZE2U;
	using SSIZEI	= SSIZE2I;

	using CSIZE		= CSIZE2F;
	using CSIZEF	= CSIZE2F;
	using CSIZEU	= CSIZE2U;
	using CSIZEI	= CSIZE2I;
}

// End Of File
