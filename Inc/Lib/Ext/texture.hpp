#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include <d3d9.h>

//========================================
// TEXTURE
//========================================
namespace opal {
	using TEXTUREB = IDirect3DBaseTexture9;
	using TEXTURES = IDirect3DTexture9;
}

// End Of File
