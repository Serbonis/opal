#pragma once
//========================================
// OPAL Game Library
//========================================
#include "vector.hpp"
#include "polar.hpp"
#include "cylind.hpp"
#include "matrix.hpp"
#include "quaternion.hpp"
#include "size.hpp"

//========================================
// 座標系
//========================================
//----------------------------------------
// 変換関数
//----------------------------------------
namespace opal {
	extern float POLAR2_X( const POLAR2& );
	extern float POLAR2_Y( const POLAR2& );

	extern VECTOR2 POLAR3_VECTOR2( const POLAR3& );
	extern VECTOR2 POLAR3_VECTOR2( const POLAR3* );
	extern VECTOR3 POLAR3_VECTOR3( const POLAR3& );
	extern VECTOR3 POLAR3_VECTOR3( const POLAR3* );
}

namespace opal {
	extern float POLAR3_X( const POLAR3& );
	extern float POLAR3_Y( const POLAR3& );
	extern float POLAR3_Z( const POLAR3& );

	extern VECTOR2 POLAR2_VECTOR2( const POLAR2& );
	extern VECTOR2 POLAR2_VECTOR2( const POLAR2* );
	extern VECTOR3 POLAR2_VECTOR3( const POLAR2&, float = 0.f );
	extern VECTOR3 POLAR2_VECTOR3( const POLAR2*, float = 0.f );
}

namespace opal {
	extern POLAR2 VECTOR2_POLAR2( const VECTOR2& );
	extern POLAR2 VECTOR2_POLAR2( const VECTOR2* );
	extern POLAR3 VECTOR2_POLAR3( const VECTOR2&, float = 0.f );
	extern POLAR3 VECTOR2_POLAR3( const VECTOR2*, float = 0.f );
}

namespace opal {
	extern POLAR2 VECTOR3_POLAR2( const VECTOR3& );
	extern POLAR2 VECTOR3_POLAR2( const VECTOR3* );
	extern POLAR3 VECTOR3_POLAR3( const VECTOR3& );
	extern POLAR3 VECTOR3_POLAR3( const VECTOR3* );
}

namespace opal {
	extern float CYLIND_X( const CYLIND );
	extern float CYLIND_Y( const CYLIND );
	extern float CYLIND_Z( const CYLIND );

	extern VECTOR2 CYLIND_VECTOR2( const CYLIND* );
	extern VECTOR2 CYLIND_VECTOR2( const CYLIND& );
	extern VECTOR3 CYLIND_VECTOR3( const CYLIND* );
	extern VECTOR3 CYLIND_VECTOR3( const CYLIND& );
}

namespace opal {
	extern CYLIND  VECTOR2_CYLIND( const VECTOR2*, float = 0.f );
	extern CYLIND  VECTOR2_CYLIND( const VECTOR2&, float = 0.f );
	extern CYLIND  VECTOR3_CYLIND( const VECTOR3* );
	extern CYLIND  VECTOR3_CYLIND( const VECTOR3& );
}

//----------------------------------------
// 座標変換
//----------------------------------------
namespace opal {
	extern VECTOR2 operator *( const MATRIX&,  const VECTOR2& );
	extern VECTOR2 operator *( const VECTOR2&, const MATRIX&  );
	extern VECTOR3 operator *( const MATRIX&,  const VECTOR3& );
	extern VECTOR3 operator *( const VECTOR3&, const MATRIX&  );
	extern VECTOR4 operator *( const MATRIX&,  const VECTOR4& );
	extern VECTOR4 operator *( const VECTOR4&, const MATRIX&  );
}

//----------------------------------------
// 正規化
//----------------------------------------
namespace opal {
	extern VECTOR2 Normalize( const VECTOR2& );
	extern VECTOR3 Normalize( const VECTOR3& );
	extern VECTOR4 Normalize( const VECTOR4& );

	extern VECTOR2 Normal( float );
}

//----------------------------------------
// 長さ
//----------------------------------------
namespace opal {
	extern float VectorLength(   const VECTOR2& );
	extern float VectorLength(   const VECTOR3& );
	extern float VectorLength(   const VECTOR4& );
	extern float VectorLengthSq( const VECTOR2& );
	extern float VectorLengthSq( const VECTOR3& );
	extern float VectorLengthSq( const VECTOR4& );

	extern float VectorLength(   const VECTOR2&, const VECTOR2& );
	extern float VectorLength(   const VECTOR3&, const VECTOR3& );
	extern float VectorLength(   const VECTOR2&, const VECTOR3& );
	extern float VectorLength(   const VECTOR3&, const VECTOR2& );
	extern float VectorLengthSq( const VECTOR2&, const VECTOR2& );
	extern float VectorLengthSq( const VECTOR3&, const VECTOR3& );
	extern float VectorLengthSq( const VECTOR2&, const VECTOR3& );
	extern float VectorLengthSq( const VECTOR3&, const VECTOR2& );
}

//----------------------------------------
// 内積＆外積
//----------------------------------------
namespace opal {
	extern float   VectorDot(   const VECTOR2&, const VECTOR2& );
	extern float   VectorDot(   const VECTOR3&, const VECTOR3& );
	extern float   VectorCross( const VECTOR2&, const VECTOR2& );
	extern VECTOR3 VectorCross( const VECTOR3&, const VECTOR3& );

	extern float VectorAdjacentSide( const VECTOR2&, const VECTOR2& );
	extern float VectorOppositeSide( const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// ベクトルの回転
//----------------------------------------
namespace opal {
	extern VECTOR3 VectorRotate( const VECTOR2&, float );
	extern VECTOR3 VectorRotate( const VECTOR3&, float );
}

//----------------------------------------
// ベクトルの角度
//----------------------------------------
namespace opal {
	extern float VectorAngle3( const VECTOR3&, const VECTOR3& );
	extern float VectorAngle3( const VECTOR3&, float, float, float );
	extern float VectorAngle3( float, float, float, const VECTOR3& );
	extern float VectorAngle3( float, float, float, float, float, float );
	extern float VectorAngle3( const VECTOR3& );
	extern float VectorAngle3( float, float, float );

	extern float VectorAngle2( const VECTOR2&, const VECTOR2& );
	extern float VectorAngle2( const VECTOR2&, float, float );
	extern float VectorAngle2( float, float, const VECTOR2& );
	extern float VectorAngle2( float, float, float, float );
	extern float VectorAngle2( const VECTOR2& );
	extern float VectorAngle2( float, float );

	extern VECTOR2 AngleVector2( float );
	extern VECTOR3 AngleVector3( float, float = 0.f );
}

//----------------------------------------
// ベクトルとサイズ
//----------------------------------------
namespace opal {
	template<typename TYPE>VECTOR2 operator +( const VECTOR2& v, const SSIZE2<TYPE>& s ){ return { v.x+s.w, v.y+s.h          }; }
	template<typename TYPE>VECTOR2 operator -( const VECTOR2& v, const SSIZE2<TYPE>& s ){ return { v.x-s.w, v.y-s.h          }; }
	template<typename TYPE>VECTOR2 operator *( const VECTOR2& v, const SSIZE2<TYPE>& s ){ return { v.x*s.w, v.y*s.h          }; }
	template<typename TYPE>VECTOR2 operator /( const VECTOR2& v, const SSIZE2<TYPE>& s ){ return { v.x/s.w, v.y/s.h          }; }
	template<typename TYPE>VECTOR2 operator +( const VECTOR2& v, const SSIZE3<TYPE>& s ){ return { v.x+s.w, v.y+s.h          }; }
	template<typename TYPE>VECTOR2 operator -( const VECTOR2& v, const SSIZE3<TYPE>& s ){ return { v.x-s.w, v.y-s.h          }; }
	template<typename TYPE>VECTOR2 operator *( const VECTOR2& v, const SSIZE3<TYPE>& s ){ return { v.x*s.w, v.y*s.h          }; }
	template<typename TYPE>VECTOR2 operator /( const VECTOR2& v, const SSIZE3<TYPE>& s ){ return { v.x/s.w, v.y/s.h          }; }

	template<typename TYPE>VECTOR3 operator +( const VECTOR3& v, const SSIZE2<TYPE>& s ){ return { v.x+s.w, v.y+s.h, v.z     }; }
	template<typename TYPE>VECTOR3 operator -( const VECTOR3& v, const SSIZE2<TYPE>& s ){ return { v.x-s.w, v.y-s.h, v.z     }; }
	template<typename TYPE>VECTOR3 operator *( const VECTOR3& v, const SSIZE2<TYPE>& s ){ return { v.x*s.w, v.y*s.h, v.z     }; }
	template<typename TYPE>VECTOR3 operator /( const VECTOR3& v, const SSIZE2<TYPE>& s ){ return { v.x/s.w, v.y/s.h, v.z     }; }
	template<typename TYPE>VECTOR3 operator +( const VECTOR3& v, const SSIZE3<TYPE>& s ){ return { v.x+s.w, v.y+s.h, v.z+s.d }; }
	template<typename TYPE>VECTOR3 operator -( const VECTOR3& v, const SSIZE3<TYPE>& s ){ return { v.x-s.w, v.y-s.h, v.z-s.d }; }
	template<typename TYPE>VECTOR3 operator *( const VECTOR3& v, const SSIZE3<TYPE>& s ){ return { v.x*s.w, v.y*s.h, v.z*s.d }; }
	template<typename TYPE>VECTOR3 operator /( const VECTOR3& v, const SSIZE3<TYPE>& s ){ return { v.x/s.w, v.y/s.h, v.z/s.d }; }

	template<typename TYPE>VECTOR2 operator +( const SSIZE2<TYPE>& s, const VECTOR2& v ){ return { v.x+s.w, v.y+s.h          }; }
	template<typename TYPE>VECTOR2 operator *( const SSIZE2<TYPE>& s, const VECTOR2& v ){ return { v.x*s.w, v.y*s.h          }; }
	template<typename TYPE>VECTOR3 operator +( const SSIZE2<TYPE>& s, const VECTOR3& v ){ return { v.x+s.w, v.y+s.h, v.z     }; }
	template<typename TYPE>VECTOR3 operator *( const SSIZE2<TYPE>& s, const VECTOR3& v ){ return { v.x*s.w, v.y*s.h, v.z     }; }
	template<typename TYPE>VECTOR2 operator +( const SSIZE3<TYPE>& s, const VECTOR2& v ){ return { v.x+s.w, v.y+s.h          }; }
	template<typename TYPE>VECTOR2 operator *( const SSIZE3<TYPE>& s, const VECTOR2& v ){ return { v.x*s.w, v.y*s.h          }; }
	template<typename TYPE>VECTOR3 operator +( const SSIZE3<TYPE>& s, const VECTOR3& v ){ return { v.x+s.w, v.y+s.h, v.z+s.d }; }
	template<typename TYPE>VECTOR3 operator *( const SSIZE3<TYPE>& s, const VECTOR3& v ){ return { v.x*s.w, v.y*s.h, v.z*s.d }; }

	inline VECTOR2 VectorSize( float w, float h          ){ return VECTOR2{w,h  }; }
	inline VECTOR3 VectorSize( float w, float h, float d ){ return VECTOR3{w,h,d}; }

	template<typename TYPE>TYPE VectorSize( const TYPE& v ){ return v; }
	template<typename TYPE>TYPE VectorSize( const TYPE* v ){ return v ? *v : TYPE{}; }

	template<typename TYPE>VECTOR2 VectorSize( const SSIZE2<TYPE>& s ){ return VECTOR3(s.w,s.h    ); }
	template<typename TYPE>VECTOR3 VectorSize( const SSIZE3<TYPE>& s ){ return VECTOR3(s.w,s.h,s.d); }

	template<typename TYPE>VECTOR2 VectorSize( const SSIZE2<TYPE>* s ){ return s ? VECTOR2(s->w,s->h     ) : VECTOR2{}; }
	template<typename TYPE>VECTOR3 VectorSize( const SSIZE3<TYPE>* s ){ return s ? VECTOR3(s->w,s->h,s->d) : VECTOR3{}; }

	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2& v, const SSIZE2<TYPE>& s ){ return v+s; }
	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2& v, const SSIZE3<TYPE>& s ){ return v+s; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3& v, const SSIZE3<TYPE>& s ){ return v+s; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3& v, const SSIZE2<TYPE>& s ){ return v+s; }

	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2* v, const SSIZE2<TYPE>& s ){ return v ? *v+s : VECTOR2{}; }
	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2* v, const SSIZE3<TYPE>& s ){ return v ? *v+s : VECTOR2{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3* v, const SSIZE3<TYPE>& s ){ return v ? *v+s : VECTOR3{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3* v, const SSIZE2<TYPE>& s ){ return v ? *v+s : VECTOR3{}; }

	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2& v, const SSIZE2<TYPE>* s ){ return s ? v+*s : VECTOR2{}; }
	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2& v, const SSIZE3<TYPE>* s ){ return s ? v+*s : VECTOR2{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3& v, const SSIZE3<TYPE>* s ){ return s ? v+*s : VECTOR3{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3& v, const SSIZE2<TYPE>* s ){ return s ? v+*s : VECTOR3{}; }

	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2* v, const SSIZE2<TYPE>* s ){ return ( v && s ) ? *v+*s : VECTOR2{}; }
	template<typename TYPE>VECTOR2 VectorSize( const VECTOR2* v, const SSIZE3<TYPE>* s ){ return ( v && s ) ? *v+*s : VECTOR2{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3* v, const SSIZE3<TYPE>* s ){ return ( v && s ) ? *v+*s : VECTOR3{}; }
	template<typename TYPE>VECTOR3 VectorSize( const VECTOR3* v, const SSIZE2<TYPE>* s ){ return ( v && s ) ? *v+*s : VECTOR3{}; }

	template<typename TYPE>float   CenterSize( const SSIZE1<TYPE>& s ){ return {s.w/2.f                };	}
	template<typename TYPE>VECTOR2 CenterSize( const SSIZE2<TYPE>& s ){ return {s.w/2.f,s.h/2.f        };	}
	template<typename TYPE>VECTOR3 CenterSize( const SSIZE3<TYPE>& s ){ return {s.w/2.f,s.h/2.f,s.d/2.f};	}
	template<typename TYPE>float   CenterSize( const SSIZE1<TYPE>* s ){ return s ? float  {s->w/2.f                  } : float{};	}
	template<typename TYPE>VECTOR2 CenterSize( const SSIZE2<TYPE>* s ){ return s ? VECTOR2{s->w/2.f,s->h/2.f         } : VECTOR2{};	}
	template<typename TYPE>VECTOR3 CenterSize( const SSIZE3<TYPE>* s ){ return s ? VECTOR3{s->w/2.f,s->h/2.f,s->d/2.f} : VECTOR3{};	}
}

//----------------------------------------
// 線形補間
//----------------------------------------
namespace opal {
	extern VECTOR3 Lerp( const VECTOR3&, const VECTOR3&, float );
}

//----------------------------------------
// 任意軸の移動
//----------------------------------------
namespace opal {
	extern VECTOR3  AxisTrans(           const VECTOR3&, const VECTOR3&, const VECTOR3&, float, float, float );
	extern VECTOR3* AxisTrans( VECTOR3*, const VECTOR3&, const VECTOR3&, const VECTOR3&, float, float, float );
}

//----------------------------------------
// 任意軸の回転
//----------------------------------------
namespace opal {
	extern VECTOR3  AxisRotate(           const VECTOR3&, const VECTOR3&, const VECTOR3&, float, float, float );
	extern VECTOR3* AxisRotate( VECTOR3*, const VECTOR3&, const VECTOR3&, const VECTOR3&, float, float, float );

	extern VECTOR3  AxisRotateVector(           const MATRIX& );
	extern VECTOR3* AxisRotateVector( VECTOR3*, const MATRIX& );

	extern MATRIX   AxisRotateMatrix(          const VECTOR3&, float );
	extern MATRIX*  AxisRotateMatrix( MATRIX*, const VECTOR3&, float );
}

//----------------------------------------
// 変換行列
//----------------------------------------
namespace opal {
	extern MATRIX* MatrixScale(  MATRIX*, float, float, float );
	extern MATRIX* MatrixRotate( MATRIX*, float, float, float );
	extern MATRIX* MatrixTrans(  MATRIX*, float, float, float );

	extern MATRIX* MatrixScale(  MATRIX*, const VECTOR3& );
	extern MATRIX* MatrixRotate( MATRIX*, const VECTOR3& );
	extern MATRIX* MatrixTrans(  MATRIX*, const VECTOR3& );

	extern MATRIX MatrixScale(  float, float, float );
	extern MATRIX MatrixRotate( float, float, float );
	extern MATRIX MatrixTrans(  float, float, float );

	extern MATRIX MatrixScale(  const VECTOR3& );
	extern MATRIX MatrixRotate( const VECTOR3& );
	extern MATRIX MatrixTrans(  const VECTOR3& );

	extern MATRIX* MatrixScaleX(  MATRIX*, float );
	extern MATRIX* MatrixScaleY(  MATRIX*, float );
	extern MATRIX* MatrixScaleZ(  MATRIX*, float );
	extern MATRIX* MatrixRotateX( MATRIX*, float );
	extern MATRIX* MatrixRotateY( MATRIX*, float );
	extern MATRIX* MatrixRotateZ( MATRIX*, float );
	extern MATRIX* MatrixTransX(  MATRIX*, float );
	extern MATRIX* MatrixTransY(  MATRIX*, float );
	extern MATRIX* MatrixTransZ(  MATRIX*, float );

	extern MATRIX MatrixScaleX(  float );
	extern MATRIX MatrixScaleY(  float );
	extern MATRIX MatrixScaleZ(  float );
	extern MATRIX MatrixRotateX( float );
	extern MATRIX MatrixRotateY( float );
	extern MATRIX MatrixRotateZ( float );
	extern MATRIX MatrixTransX(  float );
	extern MATRIX MatrixTransY(  float );
	extern MATRIX MatrixTransZ(  float );
}

namespace opal {
	extern MATRIX MatrixScale(  float, float );
	extern MATRIX MatrixRotate( float        );
	extern MATRIX MatrixTrans(  float, float );

	extern MATRIX MatrixScale(  const VECTOR2& );
	extern MATRIX MatrixTrans(  const VECTOR2& );
}

//----------------------------------------
// 回転行列→オイラー角
//----------------------------------------
namespace opal {
	extern float   MatrixAngleX( const MATRIX& );
	extern float   MatrixAngleY( const MATRIX& );
	extern float   MatrixAngleZ( const MATRIX& );
	extern VECTOR3 MatrixAngle(  const MATRIX& );
}

//----------------------------------------
// 位置部分だけ代入/取得
//----------------------------------------
namespace opal {
	extern MATRIX*  TransMatrix( MATRIX*, const VECTOR3* );
	extern MATRIX*  TransMatrix( MATRIX*, const VECTOR2* );
	extern MATRIX*  TransMatrix( MATRIX*, const VECTOR3& );
	extern MATRIX*  TransMatrix( MATRIX*, const VECTOR2& );
	extern MATRIX*  TransMatrix( MATRIX*, float, float, float );
	extern MATRIX*  TransMatrix( MATRIX*, float, float );
	extern MATRIX*  TransMatrix( MATRIX*, float );

	extern VECTOR3* TransMatrix( VECTOR3*, const MATRIX& );
	extern VECTOR3* TransMatrix( VECTOR3&, const MATRIX& );
	extern VECTOR3  TransMatrix( const MATRIX* );
	extern VECTOR3  TransMatrix( const MATRIX& );
	extern VECTOR3  TransMatrix( const MATRIX*, float );
	extern VECTOR3  TransMatrix( const MATRIX&, float );
}

//----------------------------------------
// 初期姿勢
//----------------------------------------
namespace opal {
	extern MATRIX* InitMatrix( MATRIX* );
	extern MATRIX* InitMatrix( MATRIX*, const VECTOR3& );
	extern MATRIX* InitMatrix( MATRIX*, const float*   );
	extern MATRIX* InitMatrix( MATRIX*, float, float, float );
}

//----------------------------------------
// Extension
//----------------------------------------
namespace opal {
	extern VECTOR2* Vec2TransformCoord( VECTOR2*, const VECTOR2*, const MATRIX* );
	extern VECTOR3* Vec3TransformCoord( VECTOR3*, const VECTOR3*, const MATRIX* );

	extern VECTOR3* Vec3Normalize( VECTOR3*, const VECTOR3* );

	extern float    Vec2Length(   const VECTOR2* );
	extern float    Vec3Length(   const VECTOR3* );
	extern float    Vec2LengthSq( const VECTOR2* );
	extern float    Vec3LengthSq( const VECTOR3* );

	extern float    Vec2Dot( const VECTOR2*, const VECTOR2* );
	extern float    Vec3Dot( const VECTOR3*, const VECTOR3* );

	extern VECTOR3* Vec3Cross( VECTOR3*, const VECTOR3*, const VECTOR3* );

	extern VECTOR4* Vec2Transform( VECTOR4*, const VECTOR2*, const MATRIX* );
	extern VECTOR4* Vec3Transform( VECTOR4*, const VECTOR3*, const MATRIX* );
	extern VECTOR4* Vec4Transform( VECTOR4*, const VECTOR4*, const MATRIX* );
}

namespace opal {
	extern MATRIX* MatrixRotationQuaternion( MATRIX*, const QUATERNION* );
	extern MATRIX* MatrixRotationQuaternion( MATRIX*, const QUATERNION& );
	extern MATRIX* MatrixRotation( MATRIX* );
	extern MATRIX* MatrixRotationX( MATRIX*, float );
	extern MATRIX* MatrixRotationY( MATRIX*, float );
	extern MATRIX* MatrixRotationZ( MATRIX*, float );
	extern MATRIX* MatrixRotationAxis( MATRIX*, const VECTOR3*, float );
	extern MATRIX* MatrixRotationYawPitchRoll( MATRIX*, float, float, float );

	extern MATRIX* MatrixTranslation( MATRIX*, float, float, float );
	extern MATRIX* MatrixScaling( MATRIX*, float, float, float );

	extern MATRIX* MatrixIdentity( MATRIX* );

	extern MATRIX* MatrixMultiply( MATRIX*, const MATRIX*, const MATRIX* );
	extern MATRIX* MatrixMultiply( MATRIX*, const MATRIX* );

	extern MATRIX* MatrixLookAtLH( MATRIX*, const VECTOR3*, const VECTOR3*, const VECTOR3* );
	extern MATRIX* MatrixInverse( MATRIX*, float*, const MATRIX* );
	extern MATRIX* MatrixPerspectiveFovLH( MATRIX*, float, float, float, float );
	extern MATRIX* MatrixOrthoLH( MATRIX*, float, float, float, float );
	extern UINT    MatrixDecompose( VECTOR3*, QUATERNION*, VECTOR3*, const MATRIX* );
	extern MATRIX* MatrixTranspose( MATRIX*, const MATRIX* );
}

namespace opal {
	extern QUATERNION* QuaternionRotationAxis( QUATERNION*, const VECTOR3*, float );
	extern QUATERNION* QuaternionSlerp( QUATERNION*, const QUATERNION*, const QUATERNION*, float );
}

// End Of File

