#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/size.hpp"
#include "Ext/rectangle.hpp"

#include "Ext/color.hpp"
#include "Ext/material.hpp"

#include "Ext/vector.hpp"
#include "Ext/matrix.hpp"
#include "Ext/quaternion.hpp"

//========================================
// HLSL用型定義
//========================================
namespace opal {
	struct cb_register {};
	struct tx_register {};
	struct ss_register {};
}

namespace opal {
	struct uint  {  unsigned int	u;		};
	struct uint1 {  unsigned int	u[1];	};
	struct uint2 {	unsigned int	u[2];	};
	struct uint3 {	unsigned int	u[3];	};
	struct uint4 {	unsigned int	u[4];	};
}

namespace opal {
	struct float2 {	float	f[2];	};
	struct float3 {	float	f[3];	};
	struct float4 {	float	f[4];	};
}

namespace opal {
	struct vector {
		vector();
		vector( float, float, float = 0.0f, float = 1.0f );
		vector( const vector* );
		vector( const vector& );
		vector( const VECTOR2* );
		vector( const VECTOR2& );
		vector( const VECTOR3* );
		vector( const VECTOR3& );
		vector( const VECTOR4* );
		vector( const VECTOR4& );

		operator float*( void );

		union {
			struct { float	x,y,z,w;	};
			struct { float	r,g,b,a;	};
			float	v[4];
		};
	};

	struct matrix {
		matrix();
		matrix( float, float, float, float,
				float, float, float, float,
				float, float, float, float,
				float, float, float, float );
		matrix( const float*  );
		matrix( const matrix* );
		matrix( const matrix& );
		matrix( const MATRIX* );
		matrix( const MATRIX& );

		operator float*( void );

		union {
			struct {
				float	_11,_21,_31,_41;
				float	_12,_22,_32,_42;
				float	_13,_23,_33,_43;
				float	_14,_24,_34,_44;
			};
			float	m[16];
		};
	};

	struct float4x3	{
		float4x3();
		float4x3( float, float, float, float,
				  float, float, float, float,
				  float, float, float, float );
		float4x3( const float*  );
		float4x3( const matrix* );
		float4x3( const matrix& );
		float4x3( const MATRIX* );
		float4x3( const MATRIX& );

		operator float*( void );

		union {
			struct {
				float	_11,_21,_31,_41;
				float	_12,_22,_32,_42;
				float	_13,_23,_33,_43;
			};
			float	m[12];
		};
	};
}

//========================================
// ユーティリティ
//========================================
namespace opal::hlsl {
	extern MATRIX Scale( const MATRIX&, const SSIZEF&, const SSIZEF& );
	extern MATRIX Scale( const MATRIX&, const SSIZEF&, const SSIZEF* );
	extern MATRIX Scale( const MATRIX&, const SSIZEF&, float, float  );
	extern MATRIX Scale( const MATRIX&, const SSIZEF*, const SSIZEF& );
	extern MATRIX Scale( const MATRIX&, const SSIZEF*, const SSIZEF* );
	extern MATRIX Scale( const MATRIX&, const SSIZEF*, float, float  );
	extern MATRIX Scale( const MATRIX*, const SSIZEF&, const SSIZEF& );
	extern MATRIX Scale( const MATRIX*, const SSIZEF&, const SSIZEF* );
	extern MATRIX Scale( const MATRIX*, const SSIZEF&, float, float  );
	extern MATRIX Scale( const MATRIX*, const SSIZEF*, const SSIZEF& );
	extern MATRIX Scale( const MATRIX*, const SSIZEF*, const SSIZEF* );
	extern MATRIX Scale( const MATRIX*, const SSIZEF*, float, float  );
	extern MATRIX Scale( const MATRIX&, float, float,  const SSIZEF& );
	extern MATRIX Scale( const MATRIX&, float, float,  const SSIZEF* );
	extern MATRIX Scale( const MATRIX&, float, float,  float, float  );
	extern MATRIX Scale( const MATRIX*, float, float,  const SSIZEF& );
	extern MATRIX Scale( const MATRIX*, float, float,  const SSIZEF* );
	extern MATRIX Scale( const MATRIX*, float, float,  float, float  );
	extern MATRIX Scale( const MATRIX&, const SSIZEF& );
	extern MATRIX Scale( const MATRIX&, const SSIZEF* );
	extern MATRIX Scale( const MATRIX&, float, float  );
	extern MATRIX Scale( const MATRIX*, const SSIZEF& );
	extern MATRIX Scale( const MATRIX*, const SSIZEF* );
	extern MATRIX Scale( const MATRIX*, float, float  );

	extern VECTOR2 Trans( const VECTOR2*, const MATRIX* );
	extern VECTOR2 Trans( const VECTOR2*, const MATRIX& );
	extern VECTOR2 Trans( const VECTOR2&, const MATRIX* );
	extern VECTOR2 Trans( const VECTOR2&, const MATRIX& );
	extern VECTOR2 Trans( float, float,   const MATRIX* );
	extern VECTOR2 Trans( float, float,   const MATRIX& );

	extern MATRIX World( const MATRIX*, const VECTOR3* );
	extern MATRIX World( const MATRIX*, const VECTOR3& );
	extern MATRIX World( const MATRIX&, const VECTOR3* );
	extern MATRIX World( const MATRIX&, const VECTOR3& );
	extern MATRIX World( const MATRIX*, const VECTOR2* );
	extern MATRIX World( const MATRIX*, const VECTOR2& );
	extern MATRIX World( const MATRIX&, const VECTOR2* );
	extern MATRIX World( const MATRIX&, const VECTOR2& );
	extern MATRIX World( const MATRIX*, float, float, float = 0.f );
	extern MATRIX World( const MATRIX&, float, float, float = 0.f );
	extern MATRIX World( const MATRIX* );
	extern MATRIX World( const MATRIX& );

	extern VECTOR2 Pivot( const VECTOR2*, const SSIZEF& );
	extern VECTOR2 Pivot( const VECTOR2&, const SSIZEF& );
	extern VECTOR2 Pivot( const VECTOR2*, float, float  );
	extern VECTOR2 Pivot( const VECTOR2&, float, float  );
	extern VECTOR2 Pivot( const VECTOR2* );
	extern VECTOR2 Pivot( const VECTOR2& );

	extern MATRIX Sampler( const VECTOR2*, const SSIZEF* );
	extern MATRIX Sampler( const VECTOR2*, const SSIZEF& );
	extern MATRIX Sampler( const VECTOR2&, const SSIZEF* );
	extern MATRIX Sampler( const VECTOR2&, const SSIZEF& );
	extern MATRIX Sampler( const VECTOR2*, float, float  );
	extern MATRIX Sampler( const VECTOR2&, float, float  );
	extern MATRIX Sampler( float, float, const SSIZEF&   );
	extern MATRIX Sampler( float, float, const SSIZEF*   );
	extern MATRIX Sampler( float, float, float, float    );
	extern MATRIX Sampler( const SRECTF* );
	extern MATRIX Sampler( const SRECTF& );
}

//========================================
// DEBUG
//========================================
namespace opal::hlsl {
#ifdef OPAL_DEBUG
	extern void MatrixPrintd( const matrix* );
	extern void MatrixPrintd( const matrix& );
	extern void MatrixPrintf( const matrix* );
	extern void MatrixPrintf( const matrix& );

	extern void VectorPrintd( const vector* );
	extern void VectorPrintd( const vector& );
	extern void VectorPrintf( const vector* );
	extern void VectorPrintf( const vector& );
#endif
}

// End Of File
