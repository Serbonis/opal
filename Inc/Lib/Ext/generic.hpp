#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

#include "Std/str_s.hpp"
#include "Std/node.hpp"

//========================================
// ジェネリック
//========================================
namespace opal {
	template<NODE& node>class GENERIC : private NOINSTANCE {
	public:
		template<typename TT>static TT* Search( const char*        k ){ return search<TT>( k         );	}
		template<typename TT>static TT* Search( const std::string& k ){ return search<TT>( k.c_str() );	}

	private:
		template<typename TT>static TT* search( const char* k ){

			if ( k ) {
				if ( const auto p = NODE::Search<std::shared_ptr<TT>>( node, k ) ) {
					return reinterpret_cast<TT*>( p->get() );
				}
			}
			return nullptr;
		}

	public:
		static void Destroy( const char*        k ){ return destroy( k         );	}
		static void Destroy( const std::string& k ){ return destroy( k.c_str() );	}

		static void destroy( const char* k ){

			if ( k ) {
				NODE::Destroy( node, k );
			}
		}

		template<typename TT>static void Destroy( void ){ NODE::Destroy( node, typeid( decltype( std::shared_ptr<TT>() ) ) ); }

		static void Destroy( void ){ return NODE::Destroy( node );	}
	};
}

// End Of File
