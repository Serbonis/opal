#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// クォータニアン
//========================================
namespace opal {
	struct QUATERNION {
		constexpr QUATERNION();
		constexpr QUATERNION( float, float, float, float = 1.0f );
		constexpr QUATERNION( const QUATERNION* );
		constexpr QUATERNION( const QUATERNION& );
		constexpr QUATERNION( const float* );

		float	x,y,z,w;

		operator float*();
		operator const float*() const;

		QUATERNION operator +() const;
		QUATERNION operator -() const;

		QUATERNION operator +( const QUATERNION& ) const;
		QUATERNION operator -( const QUATERNION& ) const;
		QUATERNION operator *( const QUATERNION& ) const;
		QUATERNION operator *( float ) const;
		QUATERNION operator /( float ) const;

		friend QUATERNION operator * ( float, const QUATERNION& );

#define EQ(a,b)	  FEQUAL(a,b)
#define NE(a,b)	 !FEQUAL(a,b)
		bool operator ==( const QUATERNION& q ) const { return EQ( x, q.x ) && EQ( y, q.y ) && EQ( z, q.z ) && EQ( w, q.w );	}
		bool operator !=( const QUATERNION& q ) const { return NE( x, q.x ) || NE( y, q.y ) || NE( z, q.z ) || NE( w, q.w );	}
#undef EQ
#undef NE
	};
}

//----------------------------------------
// constructor
//----------------------------------------
namespace opal {
	constexpr QUATERNION::QUATERNION() : x(0),y(0),z(0),w(1) {}
	constexpr QUATERNION::QUATERNION( float x, float y, float z, float w ) : x(x),y(y),z(z),w(w) {}
	constexpr QUATERNION::QUATERNION( const QUATERNION* q ) : QUATERNION( q ? *q : QUATERNION{} ) {}
	constexpr QUATERNION::QUATERNION( const QUATERNION& q ) : QUATERNION( q.x,q.y,q.z,q.w ) {}
	constexpr QUATERNION::QUATERNION( const float* q ) : QUATERNION( q ? QUATERNION{q[0],q[1],q[2],q[3]} : QUATERNION{} ) {}
}

// End Of File
