#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 最小値/最大値
//========================================
namespace opal {
	template<typename TYPE>class MIN_MAX {
	public:
		constexpr MIN_MAX( TYPE n, TYPE m ) : min{n},max{m} {}
		constexpr MIN_MAX(         TYPE m ) : min{0},max{m} {}
		constexpr MIN_MAX(){}

	protected:
		TYPE	min{0};
		TYPE	max{(TYPE)INT_MAX};

	public:
		virtual void SetMin( TYPE n ){ min  = n;	}
		virtual void SetMax( TYPE m ){ max  = m;	}
		virtual void AddMin( TYPE n ){ min += n;	}
		virtual void AddMax( TYPE m ){ max += m;	}
		virtual TYPE GetMin( void ) const { return min;		}
		virtual TYPE GetMax( void ) const { return max;		}

		virtual void SetMinMax( TYPE n, TYPE m ){ min  = n; max  = m;	}
		virtual void SetMinMax(         TYPE m ){           max  = m;	}
		virtual void AddMinMax( TYPE n, TYPE m ){ min += n; max += m;	}
		virtual void AddMinMax(         TYPE m ){           max += m;	}
		virtual std::pair<TYPE,TYPE> GetMinMax( void ) const { return {min,max};	}
	};
}

// End Of File
