#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// ベクター
//========================================
namespace opal {
	struct VECTOR2;
	struct VECTOR3;
	struct VECTOR4;
	struct DVECTOR;
}

//----------------------------------------
// ベクター2
//----------------------------------------
namespace opal {
	struct VECTOR2 {
		constexpr VECTOR2() noexcept;
		constexpr VECTOR2( float, float ) noexcept;
		constexpr VECTOR2( const VECTOR2& ) noexcept;
		constexpr VECTOR2( const VECTOR2* ) noexcept;
		constexpr VECTOR2( const VECTOR3& ) noexcept;
		constexpr VECTOR2( const VECTOR3* ) noexcept;
		constexpr VECTOR2( const VECTOR4& ) noexcept;
		constexpr VECTOR2( const VECTOR4* ) noexcept;
		constexpr VECTOR2( const DVECTOR& ) noexcept;
		constexpr VECTOR2( const DVECTOR* ) noexcept;
		constexpr VECTOR2( const float*   ) noexcept;
		constexpr VECTOR2( const std::tuple<float,float,float>& ) noexcept;
		constexpr VECTOR2( const std::tuple<float,float>& ) noexcept;
		constexpr VECTOR2( const std::pair< float,float>& ) noexcept;

		float	x,y;

		constexpr VECTOR2  operator +( void ) const;
		constexpr VECTOR2  operator -( void ) const;

		constexpr VECTOR2& operator =( const VECTOR2& );
		constexpr VECTOR2& operator =( const VECTOR3& );
		constexpr VECTOR2& operator =( const VECTOR4& );
		constexpr VECTOR2& operator =( const DVECTOR& );

		constexpr explicit operator       float*( void );
		constexpr explicit operator const float*( void ) const;

		constexpr explicit operator bool(  void ) const;
		constexpr explicit operator float( void ) const;

		constexpr VECTOR2 operator +( const VECTOR2& ) const;
		constexpr VECTOR2 operator -( const VECTOR2& ) const;
		constexpr VECTOR2 operator +( const VECTOR3& ) const;
		constexpr VECTOR2 operator -( const VECTOR3& ) const;
		constexpr VECTOR2 operator +( const VECTOR4& ) const;
		constexpr VECTOR2 operator -( const VECTOR4& ) const;
		constexpr VECTOR2 operator +( const DVECTOR& ) const;
		constexpr VECTOR2 operator -( const DVECTOR& ) const;
		constexpr VECTOR2 operator *( float ) const;
		constexpr VECTOR2 operator /( float ) const;
		constexpr VECTOR2 operator *( int   ) const;
		constexpr VECTOR2 operator /( int   ) const;
		constexpr VECTOR2 operator *( UINT  ) const;
		constexpr VECTOR2 operator /( UINT  ) const;

		constexpr VECTOR2& operator +=( const VECTOR2& );
		constexpr VECTOR2& operator -=( const VECTOR2& );
		constexpr VECTOR2& operator +=( const VECTOR3& );
		constexpr VECTOR2& operator -=( const VECTOR3& );
		constexpr VECTOR2& operator +=( const VECTOR4& );
		constexpr VECTOR2& operator -=( const VECTOR4& );
		constexpr VECTOR2& operator +=( const DVECTOR& );
		constexpr VECTOR2& operator -=( const DVECTOR& );
		constexpr VECTOR2& operator *=( float );
		constexpr VECTOR2& operator /=( float );
		constexpr VECTOR2& operator *=( int   );
		constexpr VECTOR2& operator /=( int   );
		constexpr VECTOR2& operator *=( UINT  );
		constexpr VECTOR2& operator /=( UINT  );

		friend VECTOR2 operator *( float, const VECTOR2& );
		friend VECTOR2 operator *( int,   const VECTOR2& );
		friend VECTOR2 operator *( UINT,  const VECTOR2& );

		constexpr bool operator ==( const VECTOR2& ) const;
		constexpr bool operator !=( const VECTOR2& ) const;
		constexpr bool operator ==( const VECTOR3& ) const;
		constexpr bool operator !=( const VECTOR3& ) const;
		constexpr bool operator ==( const VECTOR4& ) const;
		constexpr bool operator !=( const VECTOR4& ) const;
		constexpr bool operator ==( const DVECTOR& ) const;
		constexpr bool operator !=( const DVECTOR& ) const;

		constexpr float Length(   void ) const;
#ifndef _MSC_VER
		constexpr float LengthSq( void ) const;
		constexpr VECTOR2& Normalize( void );
#else
		float LengthSq( void ) const;
		VECTOR2& Normalize( void );
#endif
	};
}

//----------------------------------------
// ベクター3
//----------------------------------------
namespace opal {
	struct VECTOR3 : VECTOR2 {
		constexpr VECTOR3() noexcept;
		constexpr VECTOR3( float, float,   float = 0.f ) noexcept;
		constexpr VECTOR3( const VECTOR2&, float = 0.f ) noexcept;
		constexpr VECTOR3( const VECTOR2*, float = 0.f ) noexcept;
		constexpr VECTOR3( const VECTOR3& ) noexcept;
		constexpr VECTOR3( const VECTOR3* ) noexcept;
		constexpr VECTOR3( const VECTOR4& ) noexcept;
		constexpr VECTOR3( const VECTOR4* ) noexcept;
		constexpr VECTOR3( const DVECTOR& ) noexcept;
		constexpr VECTOR3( const DVECTOR* ) noexcept;
		constexpr VECTOR3( const float*   ) noexcept;
		constexpr VECTOR3( const std::tuple<float,float,float>& ) noexcept;
		constexpr VECTOR3( const std::tuple<float,float>&, float = 0.f ) noexcept;
		constexpr VECTOR3( const std::pair< float,float>&, float = 0.f ) noexcept;

		float	z;

		constexpr operator VECTOR2( void ) const;

		constexpr VECTOR3  operator +( void ) const;
		constexpr VECTOR3  operator -( void ) const;

		constexpr VECTOR3& operator =( const VECTOR2& );
		constexpr VECTOR3& operator =( const VECTOR3& );
		constexpr VECTOR3& operator =( const VECTOR4& );
		constexpr VECTOR3& operator =( const DVECTOR& );

		constexpr explicit operator       float*( void );
		constexpr explicit operator const float*( void ) const;

		constexpr explicit operator bool(  void ) const;
		constexpr explicit operator float( void ) const;

		constexpr VECTOR3 operator +( const VECTOR2& ) const;
		constexpr VECTOR3 operator -( const VECTOR2& ) const;
		constexpr VECTOR3 operator +( const VECTOR3& ) const;
		constexpr VECTOR3 operator -( const VECTOR3& ) const;
		constexpr VECTOR3 operator +( const VECTOR4& ) const;
		constexpr VECTOR3 operator -( const VECTOR4& ) const;
		constexpr VECTOR3 operator +( const DVECTOR& ) const;
		constexpr VECTOR3 operator -( const DVECTOR& ) const;
		constexpr VECTOR3 operator *( float ) const;
		constexpr VECTOR3 operator /( float ) const;
		constexpr VECTOR3 operator *( int   ) const;
		constexpr VECTOR3 operator /( int   ) const;
		constexpr VECTOR3 operator *( UINT  ) const;
		constexpr VECTOR3 operator /( UINT  ) const;

		constexpr VECTOR3& operator +=( const VECTOR2& );
		constexpr VECTOR3& operator -=( const VECTOR2& );
		constexpr VECTOR3& operator +=( const VECTOR3& );
		constexpr VECTOR3& operator -=( const VECTOR3& );
		constexpr VECTOR3& operator +=( const VECTOR4& );
		constexpr VECTOR3& operator -=( const VECTOR4& );
		constexpr VECTOR3& operator +=( const DVECTOR& );
		constexpr VECTOR3& operator -=( const DVECTOR& );
		constexpr VECTOR3& operator *=( float );
		constexpr VECTOR3& operator /=( float );
		constexpr VECTOR3& operator *=( int   );
		constexpr VECTOR3& operator /=( int   );
		constexpr VECTOR3& operator *=( UINT  );
		constexpr VECTOR3& operator /=( UINT  );

		friend VECTOR3 operator *( float, const VECTOR3& );
		friend VECTOR3 operator *( int,   const VECTOR3& );
		friend VECTOR3 operator *( UINT,  const VECTOR3& );

		constexpr bool operator ==( const VECTOR2& v ) const;
		constexpr bool operator !=( const VECTOR2& v ) const;
		constexpr bool operator ==( const VECTOR3& v ) const;
		constexpr bool operator !=( const VECTOR3& v ) const;
		constexpr bool operator ==( const VECTOR4& v ) const;
		constexpr bool operator !=( const VECTOR4& v ) const;
		constexpr bool operator ==( const DVECTOR& v ) const;
		constexpr bool operator !=( const DVECTOR& v ) const;

		constexpr float Length(   void ) const;
#ifndef _MSC_VER
		constexpr float LengthSq( void ) const;
		constexpr VECTOR3& Normalize( void );
#else
		float LengthSq( void ) const;
		VECTOR3& Normalize( void );
#endif
	};
}

//----------------------------------------
// ベクター4
//----------------------------------------
namespace opal {
	struct VECTOR4 : VECTOR3 {
		constexpr VECTOR4() noexcept;
		constexpr VECTOR4( float, float,   float = 0.f, float = 0.f ) noexcept;
		constexpr VECTOR4( const VECTOR2*, float = 0.f ) noexcept;
		constexpr VECTOR4( const VECTOR2&, float = 0.f ) noexcept;
		constexpr VECTOR4( const VECTOR3*, float = 0.f ) noexcept;
		constexpr VECTOR4( const VECTOR3&, float = 0.f ) noexcept;
		constexpr VECTOR4( const VECTOR4* ) noexcept;
		constexpr VECTOR4( const VECTOR4& ) noexcept;
		constexpr VECTOR4( const float*   ) noexcept;
		constexpr VECTOR4( const std::tuple<float,float,float,float>& ) noexcept;
		constexpr VECTOR4( const std::tuple<float,float,float>&, float = 0.f ) noexcept;
		constexpr VECTOR4( const std::tuple<float,float>&, float = 0.f, float = 0.f ) noexcept;
		constexpr VECTOR4( const std::pair< float,float>&, float = 0.f, float = 0.f ) noexcept;

		float	w;

		constexpr operator VECTOR2( void ) const;
		constexpr operator VECTOR3( void ) const;

		constexpr VECTOR4  operator +( void ) const;
		constexpr VECTOR4  operator -( void ) const;

		constexpr VECTOR4& operator =( const VECTOR2& );
		constexpr VECTOR4& operator =( const VECTOR3& );
		constexpr VECTOR4& operator =( const VECTOR4& );

		constexpr explicit operator       float*( void );
		constexpr explicit operator const float*( void ) const;

		constexpr explicit operator bool(  void ) const;
		constexpr explicit operator float( void ) const;

		constexpr VECTOR4 operator +( const VECTOR2& ) const;
		constexpr VECTOR4 operator -( const VECTOR2& ) const;
		constexpr VECTOR4 operator +( const VECTOR3& ) const;
		constexpr VECTOR4 operator -( const VECTOR3& ) const;
		constexpr VECTOR4 operator +( const VECTOR4& ) const;
		constexpr VECTOR4 operator -( const VECTOR4& ) const;
		constexpr VECTOR4 operator *( float ) const;
		constexpr VECTOR4 operator /( float ) const;
		constexpr VECTOR4 operator *( int   ) const;
		constexpr VECTOR4 operator /( int   ) const;
		constexpr VECTOR4 operator *( UINT  ) const;
		constexpr VECTOR4 operator /( UINT  ) const;

		constexpr VECTOR4& operator +=( const VECTOR2& );
		constexpr VECTOR4& operator -=( const VECTOR2& );
		constexpr VECTOR4& operator +=( const VECTOR3& );
		constexpr VECTOR4& operator -=( const VECTOR3& );
		constexpr VECTOR4& operator +=( const VECTOR4& );
		constexpr VECTOR4& operator -=( const VECTOR4& );
		constexpr VECTOR4& operator *=( float );
		constexpr VECTOR4& operator /=( float );
		constexpr VECTOR4& operator *=( int   );
		constexpr VECTOR4& operator /=( int   );
		constexpr VECTOR4& operator *=( UINT  );
		constexpr VECTOR4& operator /=( UINT  );

		friend VECTOR4 operator *( float, const VECTOR4& );
		friend VECTOR4 operator *( int,   const VECTOR4& );
		friend VECTOR4 operator *( UINT,  const VECTOR4& );

		constexpr bool operator ==( const VECTOR2& v ) const;
		constexpr bool operator !=( const VECTOR2& v ) const;
		constexpr bool operator ==( const VECTOR3& v ) const;
		constexpr bool operator !=( const VECTOR3& v ) const;
		constexpr bool operator ==( const VECTOR4& v ) const;
		constexpr bool operator !=( const VECTOR4& v ) const;

		constexpr float Length(   void ) const;
#ifndef _MSC_VER
		constexpr float LengthSq( void ) const;
		constexpr VECTOR4& Normalize( void );
#else
		float LengthSq( void ) const;
		VECTOR4& Normalize( void );
#endif
	};
}

//----------------------------------------
// Dベクター
//----------------------------------------
namespace opal {
	struct DVECTOR : VECTOR2 {
		constexpr DVECTOR() noexcept;
		constexpr DVECTOR( float, float,   float = 0.f ) noexcept;
		constexpr DVECTOR( const VECTOR2&, float = 0.f ) noexcept;
		constexpr DVECTOR( const VECTOR2*, float = 0.f ) noexcept;
		constexpr DVECTOR( const VECTOR3& ) noexcept;
		constexpr DVECTOR( const VECTOR3* ) noexcept;
		constexpr DVECTOR( const DVECTOR& ) noexcept;
		constexpr DVECTOR( const DVECTOR* ) noexcept;
		constexpr DVECTOR( const float*   ) noexcept;
		constexpr DVECTOR( const std::tuple<float,float,float>& ) noexcept;
		constexpr DVECTOR( const std::tuple<float,float>&, float = 0.f ) noexcept;
		constexpr DVECTOR( const std::pair< float,float>&, float = 0.f ) noexcept;

		float	h;

		constexpr operator VECTOR2( void ) const;
		constexpr operator VECTOR3( void ) const;

		constexpr DVECTOR  operator +( void ) const;
		constexpr DVECTOR  operator -( void ) const;

		constexpr DVECTOR& operator =( const VECTOR2& );
		constexpr DVECTOR& operator =( const VECTOR3& );
		constexpr DVECTOR& operator =( const DVECTOR& );

		constexpr explicit operator       float*( void );
		constexpr explicit operator const float*( void ) const;

		constexpr explicit operator bool(  void ) const;
		constexpr explicit operator float( void ) const;

		constexpr DVECTOR operator +( const VECTOR2& ) const;
		constexpr DVECTOR operator -( const VECTOR2& ) const;
		constexpr DVECTOR operator +( const VECTOR3& ) const;
		constexpr DVECTOR operator -( const VECTOR3& ) const;
		constexpr DVECTOR operator +( const DVECTOR& ) const;
		constexpr DVECTOR operator -( const DVECTOR& ) const;
		constexpr DVECTOR operator *( float ) const;
		constexpr DVECTOR operator /( float ) const;
		constexpr DVECTOR operator *( int   ) const;
		constexpr DVECTOR operator /( int   ) const;
		constexpr DVECTOR operator *( UINT  ) const;
		constexpr DVECTOR operator /( UINT  ) const;

		constexpr DVECTOR& operator +=( const VECTOR2& );
		constexpr DVECTOR& operator -=( const VECTOR2& );
		constexpr DVECTOR& operator +=( const VECTOR3& );
		constexpr DVECTOR& operator -=( const VECTOR3& );
		constexpr DVECTOR& operator +=( const DVECTOR& );
		constexpr DVECTOR& operator -=( const DVECTOR& );
		constexpr DVECTOR& operator *=( float );
		constexpr DVECTOR& operator /=( float );
		constexpr DVECTOR& operator *=( int   );
		constexpr DVECTOR& operator /=( int   );
		constexpr DVECTOR& operator *=( UINT  );
		constexpr DVECTOR& operator /=( UINT  );

		friend DVECTOR operator *( float, const DVECTOR& );
		friend DVECTOR operator *( int,   const DVECTOR& );
		friend DVECTOR operator *( UINT,  const DVECTOR& );

		constexpr bool operator ==( const VECTOR2& v ) const;
		constexpr bool operator !=( const VECTOR2& v ) const;
		constexpr bool operator ==( const VECTOR3& v ) const;
		constexpr bool operator !=( const VECTOR3& v ) const;
		constexpr bool operator ==( const DVECTOR& v ) const;
		constexpr bool operator !=( const DVECTOR& v ) const;

		constexpr float Length(   void ) const;
#ifndef _MSC_VER
		constexpr float LengthSq( void ) const;
		constexpr DVECTOR& Normalize( void );
#else
		float LengthSq( void ) const;
		DVECTOR& Normalize( void );
#endif
	};
}

//----------------------------------------
// 定義
//----------------------------------------
namespace opal {
	constexpr VECTOR2::VECTOR2() noexcept : x(0),y(0) {}
	constexpr VECTOR2::VECTOR2( float x, float y ) noexcept : x(x),y(y) {}
	constexpr VECTOR2::VECTOR2( const VECTOR2& v ) noexcept : VECTOR2( v.x,v.y ) {}
	constexpr VECTOR2::VECTOR2( const VECTOR3& v ) noexcept : VECTOR2( v.x,v.y ) {}
	constexpr VECTOR2::VECTOR2( const VECTOR4& v ) noexcept : VECTOR2( v.x,v.y ) {}
	constexpr VECTOR2::VECTOR2( const DVECTOR& v ) noexcept : VECTOR2( v.x,v.y ) {}
	constexpr VECTOR2::VECTOR2( const VECTOR2* v ) noexcept : VECTOR2( v ? VECTOR2{v->x,v->y} : VECTOR2{} ) {}
	constexpr VECTOR2::VECTOR2( const VECTOR3* v ) noexcept : VECTOR2( v ? VECTOR2{v->x,v->y} : VECTOR2{} ) {}
	constexpr VECTOR2::VECTOR2( const VECTOR4* v ) noexcept : VECTOR2( v ? VECTOR2{v->x,v->y} : VECTOR2{} ) {}
	constexpr VECTOR2::VECTOR2( const DVECTOR* v ) noexcept : VECTOR2( v ? VECTOR2{v->x,v->y} : VECTOR2{} ) {}
	constexpr VECTOR2::VECTOR2( const float*   v ) noexcept : VECTOR2( v ? VECTOR2{v[0],v[1]} : VECTOR2{} ) {}
	constexpr VECTOR2::VECTOR2( const std::tuple<float,float,float>& v ) noexcept : VECTOR2( std::get<0>(v),std::get<1>(v) ) {}
	constexpr VECTOR2::VECTOR2( const std::tuple<float,float      >& v ) noexcept : VECTOR2( std::get<0>(v),std::get<1>(v) ) {}
	constexpr VECTOR2::VECTOR2( const std::pair< float,float      >& v ) noexcept : VECTOR2( v.first,v.second ){}

	constexpr VECTOR3::VECTOR3() noexcept : VECTOR2{0,0},z(0) {}
	constexpr VECTOR3::VECTOR3( float x, float y, float z ) noexcept : VECTOR2{x,y},z(z) {}
	constexpr VECTOR3::VECTOR3( const VECTOR2& v, float z ) noexcept : VECTOR3( v.x,v.y,  z ) {}
	constexpr VECTOR3::VECTOR3( const VECTOR3& v          ) noexcept : VECTOR3( v.x,v.y,v.z ) {}
	constexpr VECTOR3::VECTOR3( const VECTOR4& v          ) noexcept : VECTOR3( v.x,v.y,v.z ) {}
	constexpr VECTOR3::VECTOR3( const DVECTOR& v          ) noexcept : VECTOR3( v.x,v.h,v.y ) {}
	constexpr VECTOR3::VECTOR3( const VECTOR2* v, float z ) noexcept : VECTOR3( v ? VECTOR3{v->x,v->y,   z} : VECTOR3{} ) {}
	constexpr VECTOR3::VECTOR3( const VECTOR3* v          ) noexcept : VECTOR3( v ? VECTOR3{v->x,v->y,v->z} : VECTOR3{} ) {}
	constexpr VECTOR3::VECTOR3( const VECTOR4* v          ) noexcept : VECTOR3( v ? VECTOR3{v->x,v->y,v->z} : VECTOR3{} ) {}
	constexpr VECTOR3::VECTOR3( const DVECTOR* v          ) noexcept : VECTOR3( v ? VECTOR3{v->x,v->h,v->y} : VECTOR3{} ) {}
	constexpr VECTOR3::VECTOR3( const float*   v          ) noexcept : VECTOR3( v ? VECTOR3{v[0],v[1],v[2]} : VECTOR3{} ) {}
	constexpr VECTOR3::VECTOR3( const std::tuple<float,float,float>& v    ) noexcept : VECTOR3( std::get<0>(v),std::get<1>(v),std::get<2>(v) ){}
	constexpr VECTOR3::VECTOR3( const std::tuple<float,float>& v, float z ) noexcept : VECTOR3( std::get<0>(v),std::get<1>(v),z ){}
	constexpr VECTOR3::VECTOR3( const std::pair< float,float>& v, float z ) noexcept : VECTOR3( v.first,v.second,z ){}

	constexpr VECTOR4::VECTOR4() noexcept : VECTOR3{0,0,0},w(0) {}
	constexpr VECTOR4::VECTOR4( float x, float y, float z, float w ) noexcept : VECTOR3{x,y,z},w(w) {}
	constexpr VECTOR4::VECTOR4( const VECTOR2& v, float w ) noexcept : VECTOR4( v.x,v.y,  0,  w  ) {}
	constexpr VECTOR4::VECTOR4( const VECTOR3& v, float w ) noexcept : VECTOR4( v.x,v.y,v.z,  w  ) {}
	constexpr VECTOR4::VECTOR4( const VECTOR4& v          ) noexcept : VECTOR4( v.x,v.y,v.z,v.w  ) {}
	constexpr VECTOR4::VECTOR4( const VECTOR2* v, float w ) noexcept : VECTOR4( v ? VECTOR4{v->x,v->y,   0,   w} : VECTOR4{} ) {}
	constexpr VECTOR4::VECTOR4( const VECTOR3* v, float w ) noexcept : VECTOR4( v ? VECTOR4{v->x,v->y,v->z,   w} : VECTOR4{} ) {}
	constexpr VECTOR4::VECTOR4( const VECTOR4* v          ) noexcept : VECTOR4( v ? VECTOR4{v->x,v->y,v->z,v->w} : VECTOR4{} ) {}
	constexpr VECTOR4::VECTOR4( const float*   v          ) noexcept : VECTOR4( v ? VECTOR4{v[0],v[1],v[2],v[3]} : VECTOR4{} ) {}
	constexpr VECTOR4::VECTOR4( const std::tuple<float,float,float,float>& v       ) noexcept : VECTOR4( std::get<0>(v),std::get<1>(v),std::get<2>(v),std::get<3>(v) ){}
	constexpr VECTOR4::VECTOR4( const std::tuple<float,float,float>& v,    float w ) noexcept : VECTOR4( std::get<0>(v),std::get<1>(v),std::get<2>(v),w ){}
	constexpr VECTOR4::VECTOR4( const std::tuple<float,float>& v, float z, float w ) noexcept : VECTOR4( std::get<0>(v),std::get<1>(v),z,w ){}
	constexpr VECTOR4::VECTOR4( const std::pair< float,float>& v, float z, float w ) noexcept : VECTOR4( v.first,v.second,z,w ){}

	constexpr DVECTOR::DVECTOR() noexcept : VECTOR2{0,0},h(0) {}
	constexpr DVECTOR::DVECTOR( float x, float y, float h ) noexcept : VECTOR2{x,y},h(h) {}
	constexpr DVECTOR::DVECTOR( const VECTOR2& v, float h ) noexcept : DVECTOR( v.x,v.y,  h ) {}
	constexpr DVECTOR::DVECTOR( const VECTOR3& v          ) noexcept : DVECTOR( v.x,v.z,v.y ) {}
	constexpr DVECTOR::DVECTOR( const DVECTOR& v          ) noexcept : DVECTOR( v.x,v.y,v.h ) {}
	constexpr DVECTOR::DVECTOR( const VECTOR2* v, float h ) noexcept : DVECTOR( v ? DVECTOR{v->x,v->y,   h} : DVECTOR{} ) {}
	constexpr DVECTOR::DVECTOR( const VECTOR3* v          ) noexcept : DVECTOR( v ? DVECTOR{v->x,v->z,v->y} : DVECTOR{} ) {}
	constexpr DVECTOR::DVECTOR( const DVECTOR* v          ) noexcept : DVECTOR( v ? DVECTOR{v->x,v->y,v->h} : DVECTOR{} ) {}
	constexpr DVECTOR::DVECTOR( const float*   v          ) noexcept : DVECTOR( v ? DVECTOR{v[0],v[1],v[2]} : DVECTOR{} ) {}
	constexpr DVECTOR::DVECTOR( const std::tuple<float,float,float>& v    ) noexcept : DVECTOR( std::get<0>(v),std::get<1>(v),std::get<2>(v) ){}
	constexpr DVECTOR::DVECTOR( const std::tuple<float,float>& v, float h ) noexcept : DVECTOR( std::get<0>(v),std::get<1>(v),h ){}
	constexpr DVECTOR::DVECTOR( const std::pair< float,float>& v, float h ) noexcept : DVECTOR( v.first,v.second,h ){}
}

namespace opal {
	constexpr VECTOR3::operator VECTOR2( void ) const { return {x,y}; 	}
	constexpr VECTOR4::operator VECTOR2( void ) const { return {x,y}; 	}
	constexpr VECTOR4::operator VECTOR3( void ) const { return {x,y,z};	}
	constexpr DVECTOR::operator VECTOR2( void ) const { return {x,y}; 	}
	constexpr DVECTOR::operator VECTOR3( void ) const { return {x,h,y}; }
}

namespace opal {
	constexpr VECTOR2  VECTOR2::operator +( void ) const { return VECTOR2{ +x, +y			}; }
	constexpr VECTOR3  VECTOR3::operator +( void ) const { return VECTOR3{ +x, +y, +z		}; }
	constexpr VECTOR4  VECTOR4::operator +( void ) const { return VECTOR4{ +x, +y, +z, w	}; }
	constexpr DVECTOR  DVECTOR::operator +( void ) const { return DVECTOR{ +x, +y, +h		}; }

	constexpr VECTOR2  VECTOR2::operator -( void ) const { return VECTOR2{ -x, -y			}; }
	constexpr VECTOR3  VECTOR3::operator -( void ) const { return VECTOR3{ -x, -y, -z		}; }
	constexpr VECTOR4  VECTOR4::operator -( void ) const { return VECTOR4{ -x, -y, -z, w	}; }
	constexpr DVECTOR  DVECTOR::operator -( void ) const { return DVECTOR{ -x, -y, -h		}; }
}

namespace opal {
	constexpr VECTOR2& VECTOR2::operator =( const VECTOR2& v ){ x = v.x; y = v.y;						return *this;	}
	constexpr VECTOR2& VECTOR2::operator =( const VECTOR3& v ){ x = v.x; y = v.y;						return *this;	}
	constexpr VECTOR2& VECTOR2::operator =( const VECTOR4& v ){ x = v.x; y = v.y;						return *this;	}
	constexpr VECTOR2& VECTOR2::operator =( const DVECTOR& v ){ x = v.x; y = v.y;						return *this;	}

	constexpr VECTOR3& VECTOR3::operator =( const VECTOR2& v ){ x = v.x; y = v.y;						return *this;	}
	constexpr VECTOR3& VECTOR3::operator =( const VECTOR3& v ){ x = v.x; y = v.y; z = v.z;				return *this;	}
	constexpr VECTOR3& VECTOR3::operator =( const VECTOR4& v ){ x = v.x; y = v.y; z = v.z;				return *this;	}
	constexpr VECTOR3& VECTOR3::operator =( const DVECTOR& v ){ x = v.x; y = v.h; z = v.y;				return *this;	}

	constexpr VECTOR4& VECTOR4::operator =( const VECTOR2& v ){ x = v.x; y = v.y; z = 0.f;				return *this;	}
	constexpr VECTOR4& VECTOR4::operator =( const VECTOR3& v ){ x = v.x; y = v.y; z = v.z;				return *this;	}
	constexpr VECTOR4& VECTOR4::operator =( const VECTOR4& v ){ x = v.x; y = v.y; z = v.z; w = v.w;		return *this;	}

	constexpr DVECTOR& DVECTOR::operator =( const VECTOR2& v ){ x = v.x; y = v.y;						return *this;	}
	constexpr DVECTOR& DVECTOR::operator =( const VECTOR3& v ){ x = v.x; y = v.z; h = v.y;				return *this;	}
	constexpr DVECTOR& DVECTOR::operator =( const DVECTOR& v ){ x = v.x; y = v.y; h = v.h;				return *this;	}
}

namespace opal {
	constexpr VECTOR2::operator       float*( void )       { return (       float* )this; }
	constexpr VECTOR3::operator       float*( void )       { return (       float* )this; }
	constexpr VECTOR4::operator       float*( void )       { return (       float* )this; }
	constexpr DVECTOR::operator       float*( void )       { return (       float* )this; }

	constexpr VECTOR2::operator const float*( void ) const { return ( const float* )this; }
	constexpr VECTOR3::operator const float*( void ) const { return ( const float* )this; }
	constexpr VECTOR4::operator const float*( void ) const { return ( const float* )this; }
	constexpr DVECTOR::operator const float*( void ) const { return ( const float* )this; }

	constexpr VECTOR2::operator bool(  void ) const { return !FZERO( x ) || !FZERO( y );								}
	constexpr VECTOR3::operator bool(  void ) const { return !FZERO( x ) || !FZERO( y ) || !FZERO( z );					}
	constexpr VECTOR4::operator bool(  void ) const { return !FZERO( x ) || !FZERO( y ) || !FZERO( z ) || !FZERO( w );	}
	constexpr DVECTOR::operator bool(  void ) const { return !FZERO( x ) || !FZERO( y ) || !FZERO( h );					}

	constexpr VECTOR2::operator float( void ) const { return x * x + y * y;												}
	constexpr VECTOR3::operator float( void ) const { return x * x + y * y + z * z;										}
	constexpr VECTOR4::operator float( void ) const { return x * x + y * y + z * z + w * w;								}
	constexpr DVECTOR::operator float( void ) const { return x * x + y * y + h * h;										}
}

namespace opal {
	constexpr VECTOR2 VECTOR2::operator +( const VECTOR2& v ) const { return VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2 VECTOR2::operator -( const VECTOR2& v ) const { return VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2 VECTOR2::operator +( const VECTOR3& v ) const { return VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2 VECTOR2::operator -( const VECTOR3& v ) const { return VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2 VECTOR2::operator +( const VECTOR4& v ) const { return VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2 VECTOR2::operator -( const VECTOR4& v ) const { return VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2 VECTOR2::operator +( const DVECTOR& v ) const { return VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2 VECTOR2::operator -( const DVECTOR& v ) const { return VECTOR2{ x-v.x, y-v.y					}; }

	constexpr VECTOR3 VECTOR3::operator +( const VECTOR2& v ) const { return VECTOR3{ x+v.x, y+v.y, z    			}; }
	constexpr VECTOR3 VECTOR3::operator -( const VECTOR2& v ) const { return VECTOR3{ x-v.x, y-v.y, z    			}; }
	constexpr VECTOR3 VECTOR3::operator +( const VECTOR3& v ) const { return VECTOR3{ x+v.x, y+v.y, z+v.z			}; }
	constexpr VECTOR3 VECTOR3::operator -( const VECTOR3& v ) const { return VECTOR3{ x-v.x, y-v.y, z-v.z			}; }
	constexpr VECTOR3 VECTOR3::operator +( const VECTOR4& v ) const { return VECTOR3{ x+v.x, y+v.y, z+v.z			}; }
	constexpr VECTOR3 VECTOR3::operator -( const VECTOR4& v ) const { return VECTOR3{ x-v.x, y-v.y, z-v.z			}; }
	constexpr VECTOR3 VECTOR3::operator +( const DVECTOR& v ) const { return VECTOR3{ x+v.x, y+v.h, z+v.y			}; }
	constexpr VECTOR3 VECTOR3::operator -( const DVECTOR& v ) const { return VECTOR3{ x-v.x, y-v.h, z-v.y			}; }

	constexpr VECTOR4 VECTOR4::operator +( const VECTOR2& v ) const { return VECTOR4{ x+v.x, y+v.y, z,     w    	}; }
	constexpr VECTOR4 VECTOR4::operator -( const VECTOR2& v ) const { return VECTOR4{ x-v.x, y-v.y, z,     w    	}; }
	constexpr VECTOR4 VECTOR4::operator +( const VECTOR3& v ) const { return VECTOR4{ x+v.x, y+v.y, z+v.z, w    	}; }
	constexpr VECTOR4 VECTOR4::operator -( const VECTOR3& v ) const { return VECTOR4{ x-v.x, y-v.y, z-v.z, w    	}; }
	constexpr VECTOR4 VECTOR4::operator +( const VECTOR4& v ) const { return VECTOR4{ x+v.x, y+v.y, z+v.z, w+v.w	}; }
	constexpr VECTOR4 VECTOR4::operator -( const VECTOR4& v ) const { return VECTOR4{ x-v.x, y-v.y, z-v.z, w-v.w	}; }

	constexpr DVECTOR DVECTOR::operator +( const VECTOR2& v ) const { return DVECTOR{ x+v.x, y+v.y, h    			}; }
	constexpr DVECTOR DVECTOR::operator -( const VECTOR2& v ) const { return DVECTOR{ x-v.x, y-v.y, h    			}; }
	constexpr DVECTOR DVECTOR::operator +( const VECTOR3& v ) const { return DVECTOR{ x+v.x, y+v.z, h+v.y			}; }
	constexpr DVECTOR DVECTOR::operator -( const VECTOR3& v ) const { return DVECTOR{ x-v.x, y-v.z, h-v.y			}; }
	constexpr DVECTOR DVECTOR::operator +( const DVECTOR& v ) const { return DVECTOR{ x+v.x, y+v.y, h+v.h			}; }
	constexpr DVECTOR DVECTOR::operator -( const DVECTOR& v ) const { return DVECTOR{ x-v.x, y-v.y, h-v.h			}; }

	constexpr VECTOR2 VECTOR2::operator *( float r ) const { return VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2 VECTOR2::operator /( float r ) const { return VECTOR2{ x/(float)r, y/(float)r							}; }
	constexpr VECTOR2 VECTOR2::operator *( int   r ) const { return VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2 VECTOR2::operator /( int   r ) const { return VECTOR2{ x/(float)r, y/(float)r							}; }
	constexpr VECTOR2 VECTOR2::operator *( UINT  r ) const { return VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2 VECTOR2::operator /( UINT  r ) const { return VECTOR2{ x/(float)r, y/(float)r							}; }

	constexpr VECTOR3 VECTOR3::operator *( float r ) const { return VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3 VECTOR3::operator /( float r ) const { return VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }
	constexpr VECTOR3 VECTOR3::operator *( int   r ) const { return VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3 VECTOR3::operator /( int   r ) const { return VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }
	constexpr VECTOR3 VECTOR3::operator *( UINT  r ) const { return VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3 VECTOR3::operator /( UINT  r ) const { return VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }

	constexpr VECTOR4 VECTOR4::operator *( float r ) const { return VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w*(float)r	}; }
	constexpr VECTOR4 VECTOR4::operator /( float r ) const { return VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }
	constexpr VECTOR4 VECTOR4::operator *( int   r ) const { return VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w*(float)r	}; }
	constexpr VECTOR4 VECTOR4::operator /( int   r ) const { return VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }
	constexpr VECTOR4 VECTOR4::operator *( UINT  r ) const { return VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w*(float)r	}; }
	constexpr VECTOR4 VECTOR4::operator /( UINT  r ) const { return VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }

	constexpr DVECTOR DVECTOR::operator *( float r ) const { return DVECTOR{ x*(float)r, y*(float)r, h*(float)r				}; }
	constexpr DVECTOR DVECTOR::operator /( float r ) const { return DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
	constexpr DVECTOR DVECTOR::operator *( int   r ) const { return DVECTOR{ x*(float)r, y*(float)r, h*(float)r				}; }
	constexpr DVECTOR DVECTOR::operator /( int   r ) const { return DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
	constexpr DVECTOR DVECTOR::operator *( UINT  r ) const { return DVECTOR{ x*(float)r, y*(float)r, h*(float)r				}; }
	constexpr DVECTOR DVECTOR::operator /( UINT  r ) const { return DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
}

namespace opal {
	constexpr VECTOR2& VECTOR2::operator +=( const VECTOR2& v ){ return *this = VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2& VECTOR2::operator -=( const VECTOR2& v ){ return *this = VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2& VECTOR2::operator +=( const VECTOR3& v ){ return *this = VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2& VECTOR2::operator -=( const VECTOR3& v ){ return *this = VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2& VECTOR2::operator +=( const VECTOR4& v ){ return *this = VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2& VECTOR2::operator -=( const VECTOR4& v ){ return *this = VECTOR2{ x-v.x, y-v.y					}; }
	constexpr VECTOR2& VECTOR2::operator +=( const DVECTOR& v ){ return *this = VECTOR2{ x+v.x, y+v.y					}; }
	constexpr VECTOR2& VECTOR2::operator -=( const DVECTOR& v ){ return *this = VECTOR2{ x-v.x, y-v.y					}; }

	constexpr VECTOR3& VECTOR3::operator +=( const VECTOR2& v ){ return *this = VECTOR3{ x+v.x, y+v.y, z    			}; }
	constexpr VECTOR3& VECTOR3::operator -=( const VECTOR2& v ){ return *this = VECTOR3{ x-v.x, y-v.y, z    			}; }
	constexpr VECTOR3& VECTOR3::operator +=( const VECTOR3& v ){ return *this = VECTOR3{ x+v.x, y+v.y, z+v.z			}; }
	constexpr VECTOR3& VECTOR3::operator -=( const VECTOR3& v ){ return *this = VECTOR3{ x-v.x, y-v.y, z-v.z			}; }
	constexpr VECTOR3& VECTOR3::operator +=( const VECTOR4& v ){ return *this = VECTOR3{ x+v.x, y+v.y, z+v.z			}; }
	constexpr VECTOR3& VECTOR3::operator -=( const VECTOR4& v ){ return *this = VECTOR3{ x-v.x, y-v.y, z-v.z			}; }
	constexpr VECTOR3& VECTOR3::operator +=( const DVECTOR& v ){ return *this = VECTOR3{ x+v.x, y+v.h, z+v.y			}; }
	constexpr VECTOR3& VECTOR3::operator -=( const DVECTOR& v ){ return *this = VECTOR3{ x-v.x, y-v.h, z-v.y			}; }

	constexpr VECTOR4& VECTOR4::operator +=( const VECTOR2& v ){ return *this = VECTOR4{ x+v.x, y+v.y, z,     w    		}; }
	constexpr VECTOR4& VECTOR4::operator -=( const VECTOR2& v ){ return *this = VECTOR4{ x-v.x, y-v.y, z,     w    		}; }
	constexpr VECTOR4& VECTOR4::operator +=( const VECTOR3& v ){ return *this = VECTOR4{ x+v.x, y+v.y, z+v.z, w    		}; }
	constexpr VECTOR4& VECTOR4::operator -=( const VECTOR3& v ){ return *this = VECTOR4{ x-v.x, y-v.y, z-v.z, w    		}; }
	constexpr VECTOR4& VECTOR4::operator +=( const VECTOR4& v ){ return *this = VECTOR4{ x+v.x, y+v.y, z+v.z, w+v.w		}; }
	constexpr VECTOR4& VECTOR4::operator -=( const VECTOR4& v ){ return *this = VECTOR4{ x-v.x, y-v.y, z-v.z, w-v.w		}; }

	constexpr DVECTOR& DVECTOR::operator +=( const VECTOR2& v ){ return *this = DVECTOR{ x+v.x, y+v.y, h    			}; }
	constexpr DVECTOR& DVECTOR::operator -=( const VECTOR2& v ){ return *this = DVECTOR{ x-v.x, y-v.y, h    			}; }
	constexpr DVECTOR& DVECTOR::operator +=( const VECTOR3& v ){ return *this = DVECTOR{ x+v.x, y+v.z, h+v.y			}; }
	constexpr DVECTOR& DVECTOR::operator -=( const VECTOR3& v ){ return *this = DVECTOR{ x-v.x, y-v.z, h-v.y			}; }
	constexpr DVECTOR& DVECTOR::operator +=( const DVECTOR& v ){ return *this = DVECTOR{ x+v.x, y+v.y, h+v.h			}; }
	constexpr DVECTOR& DVECTOR::operator -=( const DVECTOR& v ){ return *this = DVECTOR{ x-v.x, y-v.y, h-v.h			}; }

	constexpr VECTOR2& VECTOR2::operator *=( float r ){ return *this = VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2& VECTOR2::operator /=( float r ){ return *this = VECTOR2{ x/(float)r, y/(float)r							}; }
	constexpr VECTOR2& VECTOR2::operator *=( int   r ){ return *this = VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2& VECTOR2::operator /=( int   r ){ return *this = VECTOR2{ x/(float)r, y/(float)r							}; }
	constexpr VECTOR2& VECTOR2::operator *=( UINT  r ){ return *this = VECTOR2{ x*(float)r, y*(float)r							}; }
	constexpr VECTOR2& VECTOR2::operator /=( UINT  r ){ return *this = VECTOR2{ x/(float)r, y/(float)r							}; }

	constexpr VECTOR3& VECTOR3::operator *=( float r ){ return *this = VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3& VECTOR3::operator /=( float r ){ return *this = VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }
	constexpr VECTOR3& VECTOR3::operator *=( int   r ){ return *this = VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3& VECTOR3::operator /=( int   r ){ return *this = VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }
	constexpr VECTOR3& VECTOR3::operator *=( UINT  r ){ return *this = VECTOR3{ x*(float)r, y*(float)r, z*(float)r				}; }
	constexpr VECTOR3& VECTOR3::operator /=( UINT  r ){ return *this = VECTOR3{ x/(float)r, y/(float)r, z/(float)r				}; }

	constexpr VECTOR4& VECTOR4::operator *=( float r ){ return *this = VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w/(float)r	}; }
	constexpr VECTOR4& VECTOR4::operator /=( float r ){ return *this = VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }
	constexpr VECTOR4& VECTOR4::operator *=( int   r ){ return *this = VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w*(float)r	}; }
	constexpr VECTOR4& VECTOR4::operator /=( int   r ){ return *this = VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }
	constexpr VECTOR4& VECTOR4::operator *=( UINT  r ){ return *this = VECTOR4{ x*(float)r, y*(float)r, z*(float)r, w*(float)r	}; }
	constexpr VECTOR4& VECTOR4::operator /=( UINT  r ){ return *this = VECTOR4{ x/(float)r, y/(float)r, z/(float)r, w/(float)r	}; }

	constexpr DVECTOR& DVECTOR::operator /=( float r ){ return *this = DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
	constexpr DVECTOR& DVECTOR::operator *=( int   r ){ return *this = DVECTOR{ x*(float)r, y*(float)r, h*(float)r				}; }
	constexpr DVECTOR& DVECTOR::operator /=( int   r ){ return *this = DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
	constexpr DVECTOR& DVECTOR::operator *=( UINT  r ){ return *this = DVECTOR{ x*(float)r, y*(float)r, h*(float)r				}; }
	constexpr DVECTOR& DVECTOR::operator /=( UINT  r ){ return *this = DVECTOR{ x/(float)r, y/(float)r, h/(float)r				}; }
}

namespace opal {
	inline VECTOR2 operator *( float r, const VECTOR2& v ){ return VECTOR2{ (float)r*v.x, (float)r*v.y								}; }
	inline VECTOR2 operator *( int   r, const VECTOR2& v ){ return VECTOR2{ (float)r*v.x, (float)r*v.y								}; }
	inline VECTOR2 operator *( UINT  r, const VECTOR2& v ){ return VECTOR2{ (float)r*v.x, (float)r*v.y								}; }

	inline VECTOR3 operator *( float r, const VECTOR3& v ){ return VECTOR3{ (float)r*v.x, (float)r*v.y, (float)r*v.z				}; }
	inline VECTOR3 operator *( int   r, const VECTOR3& v ){ return VECTOR3{ (float)r*v.x, (float)r*v.y, (float)r*v.z				}; }
	inline VECTOR3 operator *( UINT  r, const VECTOR3& v ){ return VECTOR3{ (float)r*v.x, (float)r*v.y, (float)r*v.z				}; }

	inline VECTOR4 operator *( float r, const VECTOR4& v ){ return VECTOR4{ (float)r*v.x, (float)r*v.y, (float)r*v.z, (float)r*v.w	}; }
	inline VECTOR4 operator *( int   r, const VECTOR4& v ){ return VECTOR4{ (float)r*v.x, (float)r*v.y, (float)r*v.z, (float)r*v.w	}; }
	inline VECTOR4 operator *( UINT  r, const VECTOR4& v ){ return VECTOR4{ (float)r*v.x, (float)r*v.y, (float)r*v.z, (float)r*v.w	}; }

	inline DVECTOR operator *( float r, const DVECTOR& v ){ return DVECTOR{ (float)r*v.x, (float)r*v.y, (float)r*v.h				}; }
	inline DVECTOR operator *( int   r, const DVECTOR& v ){ return DVECTOR{ (float)r*v.x, (float)r*v.y, (float)r*v.h				}; }
	inline DVECTOR operator *( UINT  r, const DVECTOR& v ){ return DVECTOR{ (float)r*v.x, (float)r*v.y, (float)r*v.h				}; }
}

namespace opal {
#define EQ( a, b )	 FEQUAL( a, b )
#define NE( a, b )	!FEQUAL( a, b )
	constexpr bool VECTOR2::operator ==( const VECTOR2& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR2::operator !=( const VECTOR2& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool VECTOR2::operator ==( const VECTOR3& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR2::operator !=( const VECTOR3& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool VECTOR2::operator ==( const VECTOR4& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR2::operator !=( const VECTOR4& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool VECTOR2::operator ==( const DVECTOR& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR2::operator !=( const DVECTOR& v ) const { return NE( x, v.x ) || NE( y, v.y ); }

	constexpr bool VECTOR3::operator ==( const VECTOR2& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR3::operator !=( const VECTOR2& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool VECTOR3::operator ==( const VECTOR3& v ) const { return EQ( x, v.x ) && EQ( y, v.y ) && EQ( z, v.z ); }
	constexpr bool VECTOR3::operator !=( const VECTOR3& v ) const { return NE( x, v.x ) || NE( y, v.y ) || NE( z, v.z ); }
	constexpr bool VECTOR3::operator ==( const VECTOR4& v ) const { return EQ( x, v.x ) && EQ( y, v.y ) && EQ( z, v.z ); }
	constexpr bool VECTOR3::operator !=( const VECTOR4& v ) const { return NE( x, v.x ) || NE( y, v.y ) || NE( z, v.z ); }
	constexpr bool VECTOR3::operator ==( const DVECTOR& v ) const { return EQ( x, v.x ) && EQ( y, v.h ) && EQ( z, v.y ); }
	constexpr bool VECTOR3::operator !=( const DVECTOR& v ) const { return NE( x, v.x ) || NE( y, v.h ) || NE( z, v.y ); }

	constexpr bool VECTOR4::operator ==( const VECTOR2& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool VECTOR4::operator !=( const VECTOR2& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool VECTOR4::operator ==( const VECTOR3& v ) const { return EQ( x, v.x ) && EQ( y, v.y ) && EQ( z, v.z ); }
	constexpr bool VECTOR4::operator !=( const VECTOR3& v ) const { return NE( x, v.x ) || NE( y, v.y ) || NE( z, v.z ); }
	constexpr bool VECTOR4::operator ==( const VECTOR4& v ) const { return EQ( x, v.x ) && EQ( y, v.y ) && EQ( z, v.z ) && EQ( w, v.w ); }
	constexpr bool VECTOR4::operator !=( const VECTOR4& v ) const { return NE( x, v.x ) || NE( y, v.y ) || NE( z, v.z ) || NE( w, v.w ); }

	constexpr bool DVECTOR::operator ==( const VECTOR2& v ) const { return EQ( x, v.x ) && EQ( y, v.y ); }
	constexpr bool DVECTOR::operator !=( const VECTOR2& v ) const { return NE( x, v.x ) || NE( y, v.y ); }
	constexpr bool DVECTOR::operator ==( const VECTOR3& v ) const { return EQ( x, v.x ) && EQ( y, v.z ) && EQ( h, v.y ); }
	constexpr bool DVECTOR::operator !=( const VECTOR3& v ) const { return NE( x, v.x ) || NE( y, v.z ) || NE( h, v.y ); }
	constexpr bool DVECTOR::operator ==( const DVECTOR& v ) const { return EQ( x, v.x ) && EQ( y, v.y ) && EQ( h, v.h ); }
	constexpr bool DVECTOR::operator !=( const DVECTOR& v ) const { return NE( x, v.x ) || NE( y, v.y ) || NE( h, v.h ); }
#undef EQ
#undef NE
}

namespace opal {
	constexpr float VECTOR2::Length(   void ) const { return x*x + y*y;				}
	constexpr float VECTOR3::Length(   void ) const { return x*x + y*y + z*z;		}
	constexpr float VECTOR4::Length(   void ) const { return x*x + y*y + z*z + w*w; }
	constexpr float DVECTOR::Length(   void ) const { return x*x + y*y + h*h;		}
#ifndef _MSC_VER
	constexpr float VECTOR2::LengthSq( void ) const { return std::sqrt( Length() ); }
	constexpr float VECTOR3::LengthSq( void ) const { return std::sqrt( Length() ); }
	constexpr float VECTOR4::LengthSq( void ) const { return std::sqrt( Length() ); }
	constexpr float DVECTOR::LengthSq( void ) const { return std::sqrt( Length() ); }

	constexpr VECTOR2& VECTOR2::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	constexpr VECTOR3& VECTOR3::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	constexpr VECTOR4& VECTOR4::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	constexpr DVECTOR& DVECTOR::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
#else
	inline float VECTOR2::LengthSq( void ) const { return std::sqrt( Length() ); }
	inline float VECTOR3::LengthSq( void ) const { return std::sqrt( Length() ); }
	inline float VECTOR4::LengthSq( void ) const { return std::sqrt( Length() ); }
	inline float DVECTOR::LengthSq( void ) const { return std::sqrt( Length() ); }

	inline VECTOR2& VECTOR2::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	inline VECTOR3& VECTOR3::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	inline VECTOR4& VECTOR4::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
	inline DVECTOR& DVECTOR::Normalize( void ){ if ( const auto l = LengthSq(); !FZERO( l ) ) { *this /= l; } return *this; }
#endif
}

//----------------------------------------
// 構造化束縛への対応
//----------------------------------------
namespace std {
    template<> struct tuple_size<opal::VECTOR2> : integral_constant<size_t, 2> {};
    template<> struct tuple_size<opal::VECTOR3> : integral_constant<size_t, 3> {};
    template<> struct tuple_size<opal::VECTOR4> : integral_constant<size_t, 4> {};
    template<> struct tuple_size<opal::DVECTOR> : integral_constant<size_t, 3> {};
}

namespace std {
    template <size_t N>struct tuple_element<N,opal::VECTOR2> { using type = float;	};
    template <>struct tuple_element<0,opal::VECTOR2> { using type = float;	};
    template <>struct tuple_element<1,opal::VECTOR2> { using type = float;	};

    template <size_t N>struct tuple_element<N,opal::VECTOR3> { using type = float;	};
    template <>struct tuple_element<0,opal::VECTOR3> { using type = float;	};
    template <>struct tuple_element<1,opal::VECTOR3> { using type = float;	};
    template <>struct tuple_element<2,opal::VECTOR3> { using type = float;	};

    template <size_t N>struct tuple_element<N,opal::VECTOR4> { using type = float;	};
    template <>struct tuple_element<0,opal::VECTOR4> { using type = float;	};
    template <>struct tuple_element<1,opal::VECTOR4> { using type = float;	};
    template <>struct tuple_element<2,opal::VECTOR4> { using type = float;	};
    template <>struct tuple_element<3,opal::VECTOR4> { using type = float;	};

    template <size_t N>struct tuple_element<N,opal::DVECTOR> { using type = float;	};
    template <>struct tuple_element<0,opal::DVECTOR> { using type = float;	};
    template <>struct tuple_element<1,opal::DVECTOR> { using type = float;	};
    template <>struct tuple_element<2,opal::DVECTOR> { using type = float;	};
}

namespace opal {
	template <std::size_t N>float get( const VECTOR2& v ) {
		if_constexpr ( N == 0 ) { return v.x; }
		if_constexpr ( N == 1 ) { return v.y; }
	}

	template <std::size_t N>float get( const VECTOR3& v ) {
		if_constexpr ( N == 0 ) { return v.x; }
		if_constexpr ( N == 1 ) { return v.y; }
		if_constexpr ( N == 2 ) { return v.z; }
	}

	template <std::size_t N>float get( const VECTOR4& v ) {
		if_constexpr ( N == 0 ) { return v.x; }
		if_constexpr ( N == 1 ) { return v.y; }
		if_constexpr ( N == 2 ) { return v.z; }
		if_constexpr ( N == 3 ) { return v.w; }
	}

	template <std::size_t N>float get( const DVECTOR& v ) {
		if_constexpr ( N == 0 ) { return v.x; }
		if_constexpr ( N == 1 ) { return v.y; }
		if_constexpr ( N == 2 ) { return v.h; }
	}
}

//----------------------------------------
// デバッグ
//----------------------------------------
#ifdef OPAL_DEBUG
namespace opal {
	extern void VectorPrintd( const VECTOR2* );
	extern void VectorPrintd( const VECTOR2& );
	extern void VectorPrintf( const VECTOR2* );
	extern void VectorPrintf( const VECTOR2& );

	extern void VectorPrintd( const VECTOR3* );
	extern void VectorPrintd( const VECTOR3& );
	extern void VectorPrintf( const VECTOR3& );

	extern void VectorPrintd( const VECTOR4* );
	extern void VectorPrintd( const VECTOR4& );
	extern void VectorPrintf( const VECTOR4* );
	extern void VectorPrintf( const VECTOR4& );

	extern void VectorPrintd( const DVECTOR* );
	extern void VectorPrintd( const DVECTOR& );
	extern void VectorPrintf( const DVECTOR* );
	extern void VectorPrintf( const DVECTOR& );
}
#endif

// End Of File
