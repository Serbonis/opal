#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/vector.hpp"
#include "Ext/color.hpp"
#include "Utl/degree.hpp"

//========================================
// ���C�g
//========================================
namespace opal {
	enum LIGHTTYPE {
		LIGHT_POINT        = 1,
		LIGHT_SPOT         = 2,
		LIGHT_DIRECTIONAL  = 3,
		LIGHT_FORCE_DWORD  = 0x7fffffff
	};

	struct DLIGHT {
		constexpr DLIGHT();
		constexpr DLIGHT( LIGHTTYPE,			// Type
						  VCOLOR,				// Diffuse
						  VCOLOR,  				// Specular
						  VCOLOR, 				// Ambient
						  VECTOR3, 				// Position
						  VECTOR3, 				// Direction
						  float, float,			// Range,Falloff
						  float, float, float,	// Attenuation
						  float, float );		// Theta,Phi
		constexpr DLIGHT( LIGHTTYPE,
						  float, float, float, float,
						  float, float, float, float,
						  float, float, float, float,
						  float, float, float,
						  float, float, float,
						  float, float,
						  float, float, float,
						  float, float );
		constexpr DLIGHT( const DLIGHT& );

		LIGHTTYPE	Type;
		VCOLOR		Diffuse;
		VCOLOR		Specular;
		VCOLOR		Ambient;
		VECTOR3		Position;
		VECTOR3		Direction;
		float		Range;
		float		Falloff;
		float		Attenuation0;
		float		Attenuation1;
		float		Attenuation2;
		float		Theta;
		float		Phi;
	};
}

//----------------------------------------
// constructor
//----------------------------------------
namespace opal {
	constexpr DLIGHT::DLIGHT() :
		Type( LIGHT_DIRECTIONAL ),
		Diffuse(  1.0f, 1.0f, 1.0f, 1.0f ),
		Specular( 0.0f, 0.0f, 0.0f, 0.0f ),
		Ambient(  1.0f, 1.0f, 1.0f, 1.0f ),
		Position(  0.0f, 0.0f, 0.0f ),
		Direction( 0.0f, 0.0f, 0.0f ),
		Range( 1000 ), 						// �L���͈� ::sqrt( FLT_MAX ) ),
		Falloff( 1.0f ),					// �t�H�[���I�t
		Attenuation0( 1.0f ),				// ��팸���W��
		Attenuation1( 0.0f ),				// ���`�����W��
		Attenuation2( 0.0f ),				// ���������W��
		Theta		( RADIAN( 90.0f/2 ) ),	// �����̉~���̊p�x
		Phi			( RADIAN( 90.0f   ) )	// �O���̉~���̊p�x
	{}

	constexpr DLIGHT::DLIGHT( LIGHTTYPE Type,
					VCOLOR Diffuse,
					VCOLOR Specular,
					VCOLOR Ambient,
					VECTOR3 Position,
					VECTOR3 Direction,
					float Range, float Falloff,
					float Attenuation0, float Attenuation1, float Attenuation2,
					float Theta, float Phi ) :
		Type( Type ),
		Diffuse(  Diffuse ),
		Specular( Specular ),
		Ambient(  Ambient ),
		Position(  Position ),
		Direction( Direction ),
		Range( Range ),
		Falloff( Falloff ),
		Attenuation0( Attenuation0 ),
		Attenuation1( Attenuation1 ),
		Attenuation2( Attenuation2 ),
		Theta( Theta ),
		Phi( Phi )
	{}

	constexpr DLIGHT::DLIGHT( LIGHTTYPE Type,
					float Diffuse_r,   float Diffuse_g,   float Diffuse_b,   float Diffuse_a,
					float Specular_r,  float Specular_g,  float Specular_b,  float Specular_a,
					float Ambient_r,   float Ambient_g,   float Ambient_b,   float Ambient_a,
					float Position_x,  float Position_y,  float Position_z,
					float Direction_x, float Direction_y, float Direction_z,
					float Range, float Falloff,
					float Attenuation0, float Attenuation1, float Attenuation2,
					float Theta, float Phi ) :
		Type( Type ),
		Diffuse(  Diffuse_r, Diffuse_g, Diffuse_b, Diffuse_a  ),
		Specular( Specular_r,Specular_g,Specular_b,Specular_a ),
		Ambient(  Ambient_r, Ambient_g, Ambient_b, Ambient_a  ),
		Position(  Position_x, Position_y, Position_z  ),
		Direction( Direction_x,Direction_y,Direction_z ),
		Range( Range ),
		Falloff( Falloff ),
		Attenuation0( Attenuation0 ),
		Attenuation1( Attenuation1 ),
		Attenuation2( Attenuation2 ),
		Theta( Theta ),
		Phi( Phi )
	{}

	constexpr DLIGHT::DLIGHT( const DLIGHT& d ) :
		Type( d.Type ),
		Diffuse(  d.Diffuse  ),
		Specular( d.Specular ),
		Ambient(  d.Ambient  ),
		Position(  d.Position  ),
		Direction( d.Direction ),
		Range( d.Range ),
		Falloff( d.Falloff ),
		Attenuation0( d.Attenuation0 ),
		Attenuation1( d.Attenuation1 ),
		Attenuation2( d.Attenuation2 ),
		Theta( d.Theta ),
		Phi( d.Phi )
	{}
}

// End Of File
