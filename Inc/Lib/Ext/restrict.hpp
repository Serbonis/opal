#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/minmax.hpp"

//========================================
// �͈͐���
//========================================
namespace opal {
	class RESTRICT {
	public:
		using ST = std::pair<float,float>;

		RESTRICT( float n, float m ) : nxp{n,m},nxs{0},nxr{0} {}
		RESTRICT(          float m ) : nxp{0,m},nxs{0},nxr{0} {}
		RESTRICT(                  ) : nxp{},   nxs{0},nxr{0} {}

	private:
		MIN_MAX<float>	nxp;	// position
		MIN_MAX<float>	nxs;	// size
		MIN_MAX<float>	nxr;	// reflex

	public:
		virtual void  SetMin( float n ){ nxp.SetMin( n ); }
		virtual void  SetMax( float m ){ nxp.SetMax( m ); }
		virtual void  AddMin( float n ){ nxp.AddMin( n ); }
		virtual void  AddMax( float m ){ nxp.AddMax( m ); }
		virtual float GetMin( void ) const { return nxp.GetMin(); }
		virtual float GetMax( void ) const { return nxp.GetMax(); }

		virtual void  SetMinMax( float n, float m ){ nxp.SetMinMax( n, m ); }
		virtual void  SetMinMax(          float m ){ nxp.SetMinMax(    m ); }
		virtual void  AddMinMax( float n, float m ){ nxp.AddMinMax( n, m ); }
		virtual void  AddMinMax(          float m ){ nxp.AddMinMax(    m ); }
		virtual auto  GetMinMax( void ) const->ST{ return nxp.GetMinMax(); }

	public:
		virtual void  SetMinSize( float n ){ nxs.SetMin( n ); }
		virtual void  SetMaxSize( float m ){ nxs.SetMax( m ); }
		virtual float GetMinSize( void ) const { return nxs.GetMin(); }
		virtual float GetMaxSize( void ) const { return nxs.GetMax(); }

		virtual void  SetMinMaxSize( float n, float m ){ nxs.SetMinMax( n, m ); }
		virtual void  SetMinMaxSize( float s          ){ nxs.SetMinMax( s, s ); }
		virtual auto  GetMinMaxSize( void ) const->ST{ return nxs.GetMinMax(); }

	public:
		virtual void  SetMinReflex( float n ){ nxr.SetMin( n ); }
		virtual void  SetMaxReflex( float m ){ nxr.SetMax( m ); }
		virtual float GetMinReflex( void ) const { return nxr.GetMin(); }
		virtual float GetMaxReflex( void ) const { return nxr.GetMax(); }

		virtual void  SetMinMaxReflex( float n, float m ){ nxr.SetMinMax( n, m ); }
		virtual void  SetMinMaxReflex( float r          ){ nxr.SetMinMax( r, r ); }
		virtual auto  GetMinMaxReflex( void ) const->ST{ return nxr.GetMinMax(); }

	public:
		virtual ST Min( float t, float s, float d, float r ){ return Min_( t, s, d,            r            );	}
		virtual ST Max( float t, float s, float d, float r ){ return Max_( t, s, d,            r            );	}
		virtual ST Min( float t, float s, float d          ){ return Min_( t, s, d,            nxr.GetMin() );	}
		virtual ST Max( float t, float s, float d          ){ return Max_( t, s, d,            nxr.GetMax() );	}
		virtual ST Min( float t, float s                   ){ return Min_( t, s, nxs.GetMin(), nxr.GetMin() );	}
		virtual ST Max( float t, float s                   ){ return Max_( t, s, nxs.GetMin(), nxr.GetMax() );	}

		virtual ST MinMax( float t, float s, float n, float m ){ return MinMax_( t, s, n,            m            );	}
		virtual ST MinMax( float t, float s                   ){ return MinMax_( t, s, nxs.GetMin(), nxs.GetMax() );	}

	protected:
		virtual ST Min_( float t, float s, float d, float r ){ return min( t, nxp.GetMin(), s, d, r );	}
		virtual ST Max_( float t, float s, float d, float r ){ return max( t, nxp.GetMax(), s, d, r );	}

		virtual ST MinMax_( float t, float s, float n, float m ){

			if ( const auto p = nxp.GetMin(); min_c( t, p, s, n ) ) { const auto r = nxr.GetMin(); return { -s*r, min_t( t, p, s, n, r ) }; }
			if ( const auto p = nxp.GetMax(); max_c( t, p, s, m ) ) { const auto r = nxr.GetMax(); return { -s*r, max_t( t, p, s, m, r ) }; }

			return { s, t+s };
		}

	private:
		static bool  min_c( float t, float p, float s, float d ){ return (t-d) + s < p; }
		static bool  max_c( float t, float p, float s, float d ){ return (t+d) + s > p; }
		static float min_t( float t, float p, float s, float d, float r ){ const auto q = p - (t-d); return (p+d) + r*(q-s);	}
		static float max_t( float t, float p, float s, float d, float r ){ const auto q = p - (t+d); return (p-d) + r*(q-s);	}
		static ST    min(   float t, float p, float s, float d, float r ){ return min_c( t, p, s, d ) ? ST{ -s*r, min_t( t, p, s, d, r ) } : ST{s,t+s};	}
		static ST    max(   float t, float p, float s, float d, float r ){ return max_c( t, p, s, d ) ? ST{ -s*r, max_t( t, p, s, d, r ) } : ST{s,t+s};	}
	};
}

// End Of File
