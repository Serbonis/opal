#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "new.hpp"
#include "etc.hpp"

//========================================
// 結果判定クラス
//========================================
namespace opal {
	class RESULT {
	public:
		constexpr explicit RESULT( int c = 0 ) :
			pointer{nullptr},
			result{c}
		{}
		template<typename TYPE=void>constexpr explicit RESULT( TYPE* p, int c = true ) :
			pointer{p},
			result{c}
		{}
		virtual ~RESULT() = default;

	private:
		void*	pointer;
		int		result;

	public:
		void Pointer( void* p ){ pointer = p;	}
		auto Pointer( void ) const->void*{ return pointer;	}

	public:
		void Result( int c ){ result = c;	}
		auto Result( void ) const->int{ return result;	}

	public:
		bool Execute( void ) const { return result == 0; }
		bool Succeed( void ) const { return result >  0; }
		bool Failure( void ) const { return result <  0; }

	public:
		constexpr auto operator =( int   c )->auto{ return result  = c;	}
		constexpr auto operator =( void* p )->auto{ return pointer = p;	}

		constexpr operator int( void ) const { return result;	}
	};
}

// End Of File
