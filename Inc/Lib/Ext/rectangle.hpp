#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// ��`�p�N���X:�����o�[�p
//========================================
//----------------------------------------
// �P��
//----------------------------------------
namespace opal {
	template<typename TYPE>
	struct SRECT {
		constexpr SRECT( const SRECT* r ) :
			x( r ? r->x : 0 ),
			y( r ? r->y : 0 ),
			w( r ? r->w : 0 ),
			h( r ? r->h : 0 )
		{}
		constexpr SRECT( TYPE x, TYPE y, TYPE w, TYPE h ) : x( x ),y( y ),w( w ),h( h ) {}
		constexpr SRECT(                 TYPE w, TYPE h ) : x( 0 ),y( 0 ),w( w ),h( h ) {}
		constexpr SRECT() : x( 0 ),y( 0 ),w( 0 ),h( 0 ) {}

		TYPE	x,y,w,h;

		operator TYPE() const { return w*h; }
	};

	using SRECTF	= SRECT<float>;
	using SRECTI	= SRECT<int>;
	using SRECTU	= SRECT<UINT>;
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	template<UINT N, typename TYPE>
	struct SRECTS {
		constexpr SRECTS( TYPE x, TYPE y, TYPE w, TYPE h ) { for ( auto i = 0UL; i < N; i++ ) { new ( &rect[i] ) S( x, y, w, h ); }	}
		constexpr SRECTS(                 TYPE w, TYPE h ) { for ( auto i = 0UL; i < N; i++ ) { new ( &rect[i] ) S( 0, 0, w, h ); }	}

		struct S {
			S( TYPE x, TYPE y, TYPE w, TYPE h ) : s( x, y, w, h ) {}
			S(                 TYPE w, TYPE h ) : s( 0, 0, w, h ) {}
			S() = default;

			SRECT<TYPE>	s;
		}		rect[N];

		SRECT<TYPE> operator []( UINT n ) const { return rect[n]; }

		operator UINT() const { return N; }
	};

	template<UINT N>struct SRECTSF : public SRECTS<N,float>	{};
	template<UINT N>struct SRECTSI : public SRECTS<N,int>	{};
	template<UINT N>struct SRECTSU : public SRECTS<N,UINT> 	{};
}

//========================================
// ��`�p�N���X:�p���p
//========================================
//----------------------------------------
// �P��
//----------------------------------------
namespace opal {
	template<typename TYPE>class CRECT {
	public:
		explicit CRECT( TYPE x, TYPE y, TYPE w, TYPE h ) : rect( x, y, w, h ) {}
		explicit CRECT(                 TYPE w, TYPE h ) : rect(       w, h ) {}
		explicit CRECT() = default;
		virtual ~CRECT() = default;

	protected:
		SRECT<TYPE>	rect;

	public:
		virtual void X( TYPE x ){ rect.x = x; }
		virtual void Y( TYPE y ){ rect.y = y; }
		virtual void W( TYPE w ){ rect.w = w; }
		virtual void H( TYPE h ){ rect.h = h; }
		virtual TYPE X( void ) const { return rect.x; }
		virtual TYPE Y( void ) const { return rect.y; }
		virtual TYPE W( void ) const { return rect.w; }
		virtual TYPE H( void ) const { return rect.h; }

	protected:
		virtual void SetRect_( TYPE x, TYPE y, TYPE w, TYPE h ){
			rect.x = x;
			rect.y = y;
			rect.w = w;
			rect.h = h;
		}

		virtual void GetRect_( TYPE* x, TYPE* y, TYPE* w, TYPE* h ) const {
			if ( x ) { *x = rect.x; }
			if ( y ) { *y = rect.y; }
			if ( w ) { *w = rect.w; }
			if ( h ) { *h = rect.h; }
		}

	public:
		virtual void SetRect(   TYPE x, TYPE y, TYPE w, TYPE h ){ SetRect_( x, y, w, h );	}
		virtual void SetRect(                   TYPE w, TYPE h ){ SetRect_( rect.x, rect.y, w, h ); }
		virtual void SetRectXY( TYPE x, TYPE y                 ){ SetRect_( x, y, rect.w, rect.h ); }
		virtual void SetRectWH(                 TYPE w, TYPE h ){ SetRect_( rect.x, rect.y, w, h ); }
		virtual void SetRectWH(                 TYPE s         ){ SetRect_( rect.x, rect.y, s, s ); }
		virtual void SetRectX(  TYPE x                         ){ SetRect_( x, rect.y, rect.w, rect.h ); }
		virtual void SetRectY(         TYPE y                  ){ SetRect_( rect.x, y, rect.w, rect.h ); }
		virtual void SetRectW(                  TYPE w         ){ SetRect_( rect.x, rect.y, w, rect.h ); }
		virtual void SetRectH(                          TYPE h ){ SetRect_( rect.x, rect.y, rect.w, h ); }

		virtual void SetRect( const SRECT<TYPE>* r ) { if ( r ) { SetRect_( r->x, r->y, r->w, r->h ); } }
		virtual void SetRect( const SRECT<TYPE>& r ) { SetRect_( r.x, r.y, r.w, r.h ); }

	public:
		virtual void GetRect(   TYPE* x, TYPE* y, TYPE* w, TYPE* h ) const { GetRect_( x, y, w, h );	}
		virtual void GetRectXY( TYPE* x, TYPE* y                   ) const { GetRect_( x, y, nullptr, nullptr );	}
		virtual void GetRectWH(                   TYPE* w, TYPE* h ) const { GetRect_( nullptr, nullptr, w, h );	}
		virtual void GetRectX(  TYPE* x                            ) const { GetRect_( x, nullptr, nullptr, nullptr );	}
		virtual void GetRectY(           TYPE* y                   ) const { GetRect_( nullptr, y, nullptr, nullptr );	}
		virtual void GetRectW(                    TYPE* w          ) const { GetRect_( nullptr, nullptr, w, nullptr );	}
		virtual void GetRectH(                             TYPE* h ) const { GetRect_( nullptr, nullptr, nullptr, h );	}

		virtual void GetRect( SRECT<TYPE>* r ) const { if ( r ) { GetRect_( &r->x, &r->y, &r->w, &r->h ); } }
		virtual void GetRect( SRECT<TYPE>& r ) const { GetRect_( &r.x, &r.y, &r.w, &r.h ); }

		virtual const SRECT<TYPE> GetRect( void ) const {

			TYPE	x, y, w, h;

			GetRect_( &x, &y, &w, &h );

			return {x, y, w, h};
		}

		virtual TYPE GetRectX( void ) const { TYPE	x;	GetRect_( &x, nullptr, nullptr, nullptr );	return x; }
		virtual TYPE GetRectY( void ) const { TYPE	y;	GetRect_( nullptr, &y, nullptr, nullptr );	return y; }
		virtual TYPE GetRectW( void ) const { TYPE	w;	GetRect_( nullptr, nullptr, &w, nullptr );	return w; }
		virtual TYPE GetRectH( void ) const { TYPE	h;	GetRect_( nullptr, nullptr, nullptr, &h );	return h; }

	public:
		operator SRECT<TYPE>( void ) const { return rect; }
	};

	using CRECTF	= CRECT<float>;
	using CRECTI	= CRECT<int>;
	using CRECTU	= CRECT<UINT>;
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	template<UINT N,typename TYPE>class CRECTS {
	public:
		explicit CRECTS( TYPE x, TYPE y, TYPE w, TYPE h ) { for ( auto i = 0UL; i < N; i++ ) { new ( &rect[i] ) S( x, y, w, h );	}	}
		explicit CRECTS(                 TYPE w, TYPE h ) { for ( auto i = 0UL; i < N; i++ ) { new ( &rect[i] ) S( 0, 0, w, h );	}	}
		explicit CRECTS() = default;
		virtual ~CRECTS() = default;

	protected:
		struct S {
			S( TYPE x, TYPE y, TYPE w, TYPE h ) : s( x, y, w, h ) {}
			S(                 TYPE w, TYPE h ) : s( 0, 0, w, h ) {}
			S() = default;

			SRECT<TYPE>	s;
		}		rect[N];

	public:
		virtual void X( UINT n, TYPE x ){ rect[n].x = x; }
		virtual void Y( UINT n, TYPE y ){ rect[n].y = y; }
		virtual void W( UINT n, TYPE w ){ rect[n].w = w; }
		virtual void H( UINT n, TYPE h ){ rect[n].h = h; }
		virtual TYPE X( UINT n ) const { return rect[n].x; }
		virtual TYPE Y( UINT n ) const { return rect[n].y; }
		virtual TYPE W( UINT n ) const { return rect[n].w; }
		virtual TYPE H( UINT n ) const { return rect[n].h; }

	public:
		virtual void SetRect(   UINT n, TYPE x, TYPE y, TYPE w, TYPE h ){
			rect[n].s.x = x;
			rect[n].s.y = y;
			rect[n].s.w = w;
			rect[n].s.h = h;
		}
		virtual void SetRectXY( UINT n, TYPE x, TYPE y ){ rect[n].s.x = x; rect[n].s.y = y; }
		virtual void SetRectWH( UINT n, TYPE w, TYPE h ){ rect[n].s.w = w; rect[n].s.h = h; }
		virtual void SetRectWH( UINT n, TYPE w ){ rect[n].s.w = w; rect[n].s.h = w; }
		virtual void SetRectX(  UINT n, TYPE x ){ rect[n].s.x = x; }
		virtual void SetRectY(  UINT n, TYPE y ){ rect[n].s.y = y; }
		virtual void SetRectW(  UINT n, TYPE w ){ rect[n].s.w = w; }
		virtual void SetRectH(  UINT n, TYPE h ){ rect[n].s.h = h; }

		virtual void SetRect( UINT n, const SRECT<TYPE>* r ) { if ( r ) { rect[n].s = *r; } }
		virtual void SetRect( UINT n, const SRECT<TYPE>& r ) { rect[n].s = r; }

	public:
		virtual void GetRect( UINT n, TYPE* x, TYPE* y, TYPE* w, TYPE* h ) const {
			if ( x ) { *x = rect[n].s.x; }
			if ( y ) { *y = rect[n].s.y; }
			if ( w ) { *w = rect[n].s.w; }
			if ( h ) { *h = rect[n].s.h; }
		}
		virtual void GetRectXY( UINT n, TYPE* x, TYPE* y ) const { if ( x ) { *x = rect[n].s.x; } if ( y ) { *y = rect[n].s.y; } }
		virtual void GetRectWH( UINT n, TYPE* w, TYPE* h ) const { if ( w ) { *w = rect[n].s.w; } if ( h ) { *h = rect[n].s.h; } }
		virtual void GetRectX(  UINT n, TYPE* x ) const { if ( x ) { *x = rect[n].s.x; } }
		virtual void GetRectY(  UINT n, TYPE* y ) const { if ( y ) { *y = rect[n].s.y; } }
		virtual void GetRectW(  UINT n, TYPE* w ) const { if ( w ) { *w = rect[n].s.w; } }
		virtual void GetRectH(  UINT n, TYPE* h ) const { if ( h ) { *h = rect[n].s.h; } }

		virtual void GetRect( UINT n, SRECT<TYPE>* r ) const { if ( r ) { *r = rect[n].s; } }

		virtual const SRECT<TYPE>* GetRect( UINT n ) const { return &rect[n].s; }

		virtual TYPE GetRectX( UINT n ) const { return rect[n].s.x; }
		virtual TYPE GetRectY( UINT n ) const { return rect[n].s.y; }
		virtual TYPE GetRectW( UINT n ) const { return rect[n].s.w; }
		virtual TYPE GetRectH( UINT n ) const { return rect[n].s.h; }

		virtual UINT GetRect( void ) const { return N; }
	};

	template<UINT N>class CRECTSF : public CRECTS<N,float>		{};
	template<UINT N>class CRECTSI : public CRECTS<N,int>		{};
	template<UINT N>class CRECTSU : public CRECTS<N,UINT>		{};
}

//========================================
// �����蔻��
//========================================
//----------------------------------------
// ==
// !=
//----------------------------------------
// ������������
// ���@�@�@����
// ���@�@�@����
// ���@�@�@����
// ������������
//----------------------------------------
namespace opal {
	template<typename TYPE>bool operator == ( const SRECT<TYPE>& a, const SRECT<TYPE>& b ){

		return ( a.x == b.x &&
				 a.y == b.y &&
				 a.w == b.w &&
				 a.h == b.h ) ? true : false;
	}
	template<typename TYPE>bool operator != ( const SRECT<TYPE>& a, const SRECT<TYPE>& b ){ return ( a == b ) ? false : true; }
}

//----------------------------------------
// &&
// ||
//----------------------------------------
// ����������
// ���@�@�@��
// ���@����������
// ���@���@���@��
// �����������@��
// �@�@���@�@�@��
// �@�@����������
//----------------------------------------
namespace opal {
	template<typename TYPE>bool operator && ( const SRECT<TYPE>& a, const SRECT<TYPE>& b ){

		const auto	axs = a.x;
		const auto	ays = a.y;
		const auto	axe = a.x + a.w;
		const auto	aye = a.y + a.h;
		const auto	bxs = b.x;
		const auto	bys = b.y;
		const auto	bxe = b.x + b.w;
		const auto	bye = b.y + b.h;

		return ( ( axs <= bxe ) &&
				 ( bxs <= axe ) &&
				 ( ays <= bye ) &&
				 ( bys <= aye ) ) ? true : false;
	}

	template<typename TYPE>bool operator || ( const SRECT<TYPE>& a, const SRECT<TYPE>& b ){ return ( a && b ) ? false : true; }
}

//----------------------------------------
// +=
// -=
//----------------------------------------
// �_
// �@�_
// �@�@����������
// �@�@���@�@�@��
// �@�@���@�@�@��
// �@�@���@�@�@��
// �@�@����������
//----------------------------------------
namespace opal {
	//----------------------------------------
	// +=
	//----------------------------------------
	template<typename TYPE, typename VECT>SRECT<TYPE>& operator += ( SRECT<TYPE>& a, const VECT& b ){

		a.x += b.x;
		a.y += b.y;

		return a;
	}

	template<typename TYPE, typename VECT>SRECT<TYPE> operator + ( const SRECT<TYPE>& a, const VECT& b ){

		return SRECT<TYPE>( a.x + b.x, a.y + b.y, a.w, a.h );
	}

	template<typename TYPE, typename VECT>SRECT<TYPE> operator + ( const VECT& b, const SRECT<TYPE>& a ){

		return SRECT<TYPE>( a.x + b.x, a.y + b.y, a.w, a.h );
	}

	//----------------------------------------
	// -=
	//----------------------------------------
	template<typename TYPE, typename VECT>SRECT<TYPE>& operator -= ( SRECT<TYPE>& a, const VECT& b ){

		a.x -= b.x;
		a.y -= b.y;

		return a;
	}

	template<typename TYPE, typename VECT>SRECT<TYPE> operator - ( const SRECT<TYPE>& a, const VECT& b ){

		return SRECT<TYPE>( a.x - b.x, a.y - b.y, a.w, a.h );
	}

	template<typename TYPE, typename VECT>SRECT<TYPE> operator - ( const VECT& b, const SRECT<TYPE>& a ){

		return SRECT<TYPE>( a.x - b.x, a.y - b.y, a.w, a.h );
	}
}

// End Of File

