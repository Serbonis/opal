#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 極座標
//========================================
namespace opal {
	struct POLAR2;
	struct POLAR3;
}

//----------------------------------------
// ポーラー2
//----------------------------------------
namespace opal {
	struct POLAR2 {
		float	r;
		float	p;

		POLAR2();
		POLAR2( float, float = 0.0f );
		POLAR2( const POLAR2*  );
		POLAR2( const POLAR2&  );
		POLAR2( const POLAR3*  );
		POLAR2( const POLAR3&  );

		POLAR2& operator =( const POLAR2& );
		POLAR2& operator =( const POLAR3& );

		POLAR2 operator +() const;
		POLAR2 operator -() const;

		POLAR2 operator +( float ) const;
		POLAR2 operator -( float ) const;
		POLAR2 operator *( float ) const;
		POLAR2 operator /( float ) const;
		POLAR2 operator +( int   ) const;
		POLAR2 operator -( int   ) const;
		POLAR2 operator *( int   ) const;
		POLAR2 operator /( int   ) const;

		friend POLAR2 operator * ( float, const POLAR2& );
		friend POLAR2 operator * ( int,   const POLAR2& );

		bool operator ==( const POLAR2& ) const;
		bool operator !=( const POLAR2& ) const;
		bool operator ==( const POLAR3& ) const;
		bool operator !=( const POLAR3& ) const;

		operator float( void ) const;
	};

#ifdef OPAL_DEBUG
	extern void PolarPrintd( const POLAR2* );
	extern void PolarPrintd( const POLAR2& );
	extern void PolarPrintf( const POLAR2* );
	extern void PolarPrintf( const POLAR2& );
#endif
}

//----------------------------------------
// ポーラー3
//----------------------------------------
namespace opal {
	struct POLAR3 {
		float	r;
		float	p;
		float	q;

		POLAR3();
		POLAR3( float, float = 0.0f, float = 0.0f );
		POLAR3( const POLAR2*, float = 0.0f );
		POLAR3( const POLAR2&, float = 0.0f );
		POLAR3( const POLAR3*  );
		POLAR3( const POLAR3&  );

		POLAR3& operator =( const POLAR2& );
		POLAR3& operator =( const POLAR3& );

		POLAR3 operator +() const;
		POLAR3 operator -() const;

		POLAR3 operator +( float ) const;
		POLAR3 operator -( float ) const;
		POLAR3 operator *( float ) const;
		POLAR3 operator /( float ) const;
		POLAR3 operator +( int   ) const;
		POLAR3 operator -( int   ) const;
		POLAR3 operator *( int   ) const;
		POLAR3 operator /( int   ) const;

		friend POLAR3 operator *( float, const POLAR3& );
		friend POLAR3 operator *( int,   const POLAR3& );

		bool operator ==( const POLAR2& ) const;
		bool operator !=( const POLAR2& ) const;
		bool operator ==( const POLAR3& ) const;
		bool operator !=( const POLAR3& ) const;

		operator float( void ) const;
	};

#ifdef OPAL_DEBUG
	extern void PolarPrintd( const POLAR3* );
	extern void PolarPrintd( const POLAR3& );
	extern void PolarPrintf( const POLAR3* );
	extern void PolarPrintf( const POLAR3& );
#endif
}

//----------------------------------------
// 変換関数
//----------------------------------------
namespace opal {
	extern POLAR2  POLAR2_POLAR2( const POLAR2* );
	extern POLAR2  POLAR2_POLAR2( const POLAR2& );
	extern POLAR3  POLAR2_POLAR3( const POLAR2*, float = 0.f );
	extern POLAR3  POLAR2_POLAR3( const POLAR2&, float = 0.f );

	extern POLAR3  POLAR3_POLAR3( const POLAR3* );
	extern POLAR3  POLAR3_POLAR3( const POLAR3& );
	extern POLAR2  POLAR3_POLAR2( const POLAR3* );
	extern POLAR2  POLAR3_POLAR2( const POLAR3& );
}

// End Of File
