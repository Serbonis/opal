#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 座標
//========================================
//----------------------------------------
// 座標2
//----------------------------------------
namespace opal {
	template<typename TYPE>struct POINT2 {
		constexpr POINT2( TYPE x, TYPE y  ) : x{ x },y{ y } {}
		constexpr POINT2( const POINT2& v ) : x{v.x},y{v.y} {}
		constexpr POINT2() : x{0},y{0} {}

		TYPE	x;
		TYPE	y;

		constexpr operator TYPE() const { return x*x + y*y; }

		constexpr bool operator ==( const POINT2& a ) const { return (  FEQUAL( x, a.x ) &&  FEQUAL( y, a.y ) ) ? true : false; }
		constexpr bool operator !=( const POINT2& a ) const { return ( !FEQUAL( x, a.x ) || !FEQUAL( y, a.y ) ) ? true : false; }

		constexpr POINT2 operator +( const POINT2& p ) const { return  { x+p.x, y+p.y };	}
		constexpr POINT2 operator -( const POINT2& p ) const { return  { x-p.x, y-p.y };	}
		constexpr POINT2 operator *( TYPE a ) const { return { x*a, y*a }; }
		constexpr POINT2 operator /( TYPE a ) const { return { x/a, y/a }; }

		constexpr POINT2& operator +=( const POINT2& p ){ x += p.x, y += p.y; return *this;	}
		constexpr POINT2& operator -=( const POINT2& p ){ x -= p.x, y -= p.y; return *this;	}
	};

	using POINT2F	= POINT2<float>;
	using POINT2I	= POINT2<int>;
	using POINT2U	= POINT2<UINT>;

	template<>constexpr bool POINT2<float>::operator ==( const POINT2<float>& a ) const { return FEQUAL( x, a.x ) && FEQUAL( y, a.y );	}
}

//----------------------------------------
// 座標3
//----------------------------------------
namespace opal {
	template<typename TYPE>struct POINT3 {
		using POINT2 = POINT2<TYPE>;

		constexpr POINT3( TYPE x, TYPE y, TYPE z = 0 ) : x{x},y{y},z{z} {}
		constexpr POINT3( const POINT2& v ) : x{v.x},y{v.y},z{ 0 } {}
		constexpr POINT3( const POINT3& v ) : x{v.x},y{v.y},z{v.z} {}
		constexpr POINT3() : x{0},y{0},z{0} {}

		TYPE	x;
		TYPE	y;
		TYPE	z;

		constexpr operator TYPE() const { return x*x + y*y + z*z; }

		constexpr bool operator ==( const POINT3& a ) const { return (  FEQUAL( x, a.x ) &&  FEQUAL( y, a.y ) &&  FEQUAL( z, a.z ) ) ? true : false; }
		constexpr bool operator !=( const POINT3& a ) const { return ( !FEQUAL( x, a.x ) || !FEQUAL( y, a.y ) || !FEQUAL( z, a.z ) ) ? true : false; }

		constexpr POINT3 operator +( const POINT3& p ) const { return  { x+p.x, y+p.y, z+p.z };	}
		constexpr POINT3 operator -( const POINT3& p ) const { return  { x-p.x, y-p.y, z-p.z };	}
		constexpr POINT3 operator *( TYPE a ) const { return { x*a, y*a }; }
		constexpr POINT3 operator /( TYPE a ) const { return { x/a, y/a }; }

		constexpr POINT3& operator +=( const POINT3& p ){ x += p.x, y += p.y; z += p.z; return *this;	}
		constexpr POINT3& operator -=( const POINT3& p ){ x -= p.x, y -= p.y; z -= p.z; return *this;	}
	};

	using POINT3F	= POINT3<float>;
	using POINT3I	= POINT3<int>;
	using POINT3U	= POINT3<UINT>;

	template<>constexpr bool POINT3<float>::operator ==( const POINT3<float>& a ) const { return FEQUAL( x, a.x ) && FEQUAL( y, a.y ) && FEQUAL( z, a.z ); }
}

// End Of File
