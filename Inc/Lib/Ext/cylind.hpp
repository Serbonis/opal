#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 円柱座標
//========================================
//----------------------------------------
// シリンダー
//----------------------------------------
namespace opal {
	struct CYLIND {
		float	h;
		float	r;
		float	p;

		CYLIND();
		CYLIND( float, float = 0.0f, float = 0.0f );
		CYLIND( const CYLIND*  );
		CYLIND( const CYLIND&  );

		CYLIND& operator =( const CYLIND& );

		CYLIND operator +() const;
		CYLIND operator -() const;

		CYLIND operator +( float ) const;
		CYLIND operator -( float ) const;
		CYLIND operator *( float ) const;
		CYLIND operator /( float ) const;
		CYLIND operator +( int   ) const;
		CYLIND operator -( int   ) const;
		CYLIND operator *( int   ) const;
		CYLIND operator /( int   ) const;

		friend CYLIND operator * ( float, const CYLIND& );
		friend CYLIND operator * ( int,   const CYLIND& );

		bool operator ==( const CYLIND& ) const;
		bool operator !=( const CYLIND& ) const;

		operator float( void ) const;
	};

#ifdef OPAL_DEBUG
	extern void CylindPrintd( const CYLIND* );
	extern void CylindPrintd( const CYLIND& );
	extern void CylindPrintf( const CYLIND* );
	extern void CylindPrintf( const CYLIND& );
#endif
}

//----------------------------------------
// 変換関数
//----------------------------------------
namespace opal {
	extern CYLIND CYLIND_CYLIND( const CYLIND* );
	extern CYLIND CYLIND_CYLIND( const CYLIND& );
}

// End Of File
