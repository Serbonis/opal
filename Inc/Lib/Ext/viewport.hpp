#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include <d3d9types.h>

//========================================
// ビューポート
//========================================
namespace opal {
	class MATRIX;

	struct IVIEWPORT {
		virtual MATRIX* Matrix(  MATRIX* m ) const = 0;
		virtual MATRIX* Inverse( MATRIX* m ) const = 0;

		static MATRIX* Matrix(  MATRIX* m, float w, float h );
		static MATRIX* Inverse( MATRIX* m, float w, float h );

		static MATRIX Matrix(  float w, float h );
		static MATRIX Inverse( float w, float h );
	};
}

// End Of File
