#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// マトリックス
//========================================
namespace opal {
	struct MATRIX {
		constexpr MATRIX();
		constexpr MATRIX( float, float, float, float,
						  float, float, float, float,
						  float, float, float, float,
						  float, float, float, float );
		constexpr MATRIX( const MATRIX* );
		constexpr MATRIX( const MATRIX& );
		constexpr MATRIX( const float* );

		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;

		float& operator ()( UINT r, UINT c );
		float  operator ()( UINT r, UINT c ) const;

		operator float*();
		operator const float*() const;

		MATRIX operator +() const;
		MATRIX operator -() const;

		MATRIX operator *( const MATRIX& ) const;
		MATRIX operator +( const MATRIX& ) const;
		MATRIX operator -( const MATRIX& ) const;
		MATRIX operator *( float ) const;
		MATRIX operator /( float ) const;

		MATRIX& operator *=( const MATRIX& );
		MATRIX& operator +=( const MATRIX& );
		MATRIX& operator -=( const MATRIX& );
		MATRIX& operator *=( float );
		MATRIX& operator /=( float );

		friend MATRIX operator *( float, const MATRIX& );

#define EQ(a,b)	  FEQUAL(a,b)
#define NE(a,b)	 !FEQUAL(a,b)
		bool operator ==( const MATRIX& m ) const {

			return ( EQ( _11, m._11 ) && EQ( _12, m._12 ) && EQ( _13, m._13 ) && EQ( _14, m._14 ) &&
					 EQ( _21, m._21 ) && EQ( _22, m._22 ) && EQ( _23, m._23 ) && EQ( _24, m._24 ) &&
					 EQ( _31, m._31 ) && EQ( _32, m._32 ) && EQ( _33, m._33 ) && EQ( _34, m._34 ) &&
					 EQ( _41, m._41 ) && EQ( _42, m._42 ) && EQ( _43, m._43 ) && EQ( _44, m._44 ) );
		}

		bool operator !=( const MATRIX& m ) const {

			return ( NE( _11, m._11 ) || NE( _12, m._12 ) || NE( _13, m._13 ) || NE( _14, m._14 ) ||
					 NE( _21, m._21 ) || NE( _22, m._22 ) || NE( _23, m._23 ) || NE( _24, m._24 ) ||
					 NE( _31, m._31 ) || NE( _32, m._32 ) || NE( _33, m._33 ) || NE( _34, m._34 ) ||
					 NE( _41, m._41 ) || NE( _42, m._42 ) || NE( _43, m._43 ) || NE( _44, m._44 ) );
		}
#undef EQ
#undef NE

		static float  Determinant( const MATRIX& );
		static MATRIX Inverse(     const MATRIX& );
		static MATRIX Transpose(   const MATRIX& );
	};

#ifdef OPAL_DEBUG
	extern void MatrixPrintd( const MATRIX* );
	extern void MatrixPrintd( const MATRIX& );
	extern void MatrixPrintf( const MATRIX* );
	extern void MatrixPrintf( const MATRIX& );
#endif
}

//----------------------------------------
// constructor
//----------------------------------------
namespace opal {
	constexpr MATRIX::MATRIX() :
		_11(0), _12(0), _13(0), _14(0),
		_21(0), _22(0), _23(0), _24(0),
		_31(0), _32(0), _33(0), _34(0),
		_41(0), _42(0), _43(0), _44(0)
	{}
	constexpr MATRIX::MATRIX( float _11, float _12, float _13, float _14,
							  float _21, float _22, float _23, float _24,
							  float _31, float _32, float _33, float _34,
							  float _41, float _42, float _43, float _44 ) :
		_11(_11), _12(_12), _13(_13), _14(_14),
		_21(_21), _22(_22), _23(_23), _24(_24),
		_31(_31), _32(_32), _33(_33), _34(_34),
		_41(_41), _42(_42), _43(_43), _44(_44)
	{}
	constexpr MATRIX::MATRIX( const MATRIX* m ) : MATRIX( m ? *m : MATRIX{} ) {}
	constexpr MATRIX::MATRIX( const MATRIX& m ) : MATRIX( m._11,m._12,m._13,m._14,
														  m._21,m._22,m._23,m._24,
														  m._31,m._32,m._33,m._34,
														  m._41,m._42,m._43,m._44 )
	{}
	constexpr MATRIX::MATRIX( const float*  m ) : MATRIX( !m
														  ? MATRIX{}
														  : MATRIX{ m[ 0],m[ 1],m[ 2],m[ 3],
																	m[ 4],m[ 5],m[ 6],m[ 7],
																	m[ 8],m[ 9],m[10],m[11],
																	m[12],m[13],m[14],m[15] } ) {}
}

//----------------------------------------
// operator
//----------------------------------------
namespace opal {
	inline float& MATRIX::operator ()( UINT r, UINT c ){ return ( ( float(*)[4] )this )[r][c]; }
	inline float  MATRIX::operator ()( UINT r, UINT c ) const { return ( ( const float(*)[4] )this )[r][c]; }

	inline MATRIX::operator float*(){ return ( float* )this; }
	inline MATRIX::operator const float*() const { return ( const float* )this; }

	inline MATRIX MATRIX::operator +() const {

		return {+_11, +_12, +_13, +_14,
				+_21, +_22, +_23, +_24,
				+_31, +_32, +_33, +_34,
				+_41, +_42, +_43, +_44};
	}

	inline MATRIX MATRIX::operator -() const {

		return {-_11, -_12, -_13, -_14,
				-_21, -_22, -_23, -_24,
				-_31, -_32, -_33, -_34,
				-_41, -_42, -_43, -_44};
	}

	inline MATRIX MATRIX::operator *( const MATRIX& m ) const {

		return {( _11 * m._11 )+( _12 * m._21 )+( _13 * m._31 )+( _14 * m._41 ),
				( _11 * m._12 )+( _12 * m._22 )+( _13 * m._32 )+( _14 * m._42 ),
				( _11 * m._13 )+( _12 * m._23 )+( _13 * m._33 )+( _14 * m._43 ),
				( _11 * m._14 )+( _12 * m._24 )+( _13 * m._34 )+( _14 * m._44 ),
				( _21 * m._11 )+( _22 * m._21 )+( _23 * m._31 )+( _24 * m._41 ),
				( _21 * m._12 )+( _22 * m._22 )+( _23 * m._32 )+( _24 * m._42 ),
				( _21 * m._13 )+( _22 * m._23 )+( _23 * m._33 )+( _24 * m._43 ),
				( _21 * m._14 )+( _22 * m._24 )+( _23 * m._34 )+( _24 * m._44 ),
				( _31 * m._11 )+( _32 * m._21 )+( _33 * m._31 )+( _34 * m._41 ),
				( _31 * m._12 )+( _32 * m._22 )+( _33 * m._32 )+( _34 * m._42 ),
				( _31 * m._13 )+( _32 * m._23 )+( _33 * m._33 )+( _34 * m._43 ),
				( _31 * m._14 )+( _32 * m._24 )+( _33 * m._34 )+( _34 * m._44 ),
				( _41 * m._11 )+( _42 * m._21 )+( _43 * m._31 )+( _44 * m._41 ),
				( _41 * m._12 )+( _42 * m._22 )+( _43 * m._32 )+( _44 * m._42 ),
				( _41 * m._13 )+( _42 * m._23 )+( _43 * m._33 )+( _44 * m._43 ),
				( _41 * m._14 )+( _42 * m._24 )+( _43 * m._34 )+( _44 * m._44 )};
	}

	inline MATRIX MATRIX::operator +( const MATRIX& m ) const {

		return {_11+m._11, _12+m._12, _13+m._13, _14+m._14,
				_21+m._21, _22+m._22, _23+m._23, _24+m._24,
				_31+m._31, _32+m._32, _33+m._33, _34+m._34,
				_41+m._41, _42+m._42, _43+m._43, _44+m._44};
	}

	inline MATRIX MATRIX::operator -( const MATRIX& m ) const {

		return {_11-m._11, _12-m._12, _13-m._13, _14-m._14,
				_21-m._21, _22-m._22, _23-m._23, _24-m._24,
				_31-m._31, _32-m._32, _33-m._33, _34-m._34,
				_41-m._41, _42-m._42, _43-m._43, _44-m._44};
	}

	inline MATRIX MATRIX::operator *( float r ) const {

		return {_11*r, _12*r, _13*r, _14*r,
				_21*r, _22*r, _23*r, _24*r,
				_31*r, _32*r, _33*r, _34*r,
				_41*r, _42*r, _43*r, _44*r};
	}

	inline MATRIX MATRIX::operator /( float r ) const {

		return {_11/r, _12/r, _13/r, _14/r,
				_21/r, _22/r, _23/r, _24/r,
				_31/r, _32/r, _33/r, _34/r,
				_41/r, _42/r, _43/r, _44/r};
	}

	inline MATRIX operator *( float r, const MATRIX& m ){ return MATRIX{ m * r }; }

	inline MATRIX& MATRIX::operator *=( const MATRIX& m ){ return *this = *this * m;	}
	inline MATRIX& MATRIX::operator +=( const MATRIX& m ){ return *this = *this + m;	}
	inline MATRIX& MATRIX::operator -=( const MATRIX& m ){ return *this = *this - m;	}
	inline MATRIX& MATRIX::operator *=( float r ){ return *this = *this * r;	}
	inline MATRIX& MATRIX::operator /=( float r ){ return *this = *this / r;	}
}

// End Of File
