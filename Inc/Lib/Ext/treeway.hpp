#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// スリーウェイ
//========================================
namespace opal {
	class TREEWAY {
	public:
		enum class WAY : int {
			M = -1,
			Z =  0,
			P = +1,
		};

	public:
		constexpr explicit TREEWAY() : way{WAY::Z}{}
		template<typename TYPE>constexpr explicit TREEWAY( TYPE w ) : way{Val2Way(w)}{}

	private:
		WAY	way;

	public:
		explicit operator bool( void ) const { return ( way == WAY::Z ) ? false : true;	}

	public:
		template<typename TYPE>void operator ()( TYPE w ){ way = Val2Way( w );	}
		virtual int operator ()( void ) const final{ return Way2Val( way );	}

	public:
		static constexpr auto Val2Way( WAY w )->WAY{ return w;	}

		template<typename TYPE>static constexpr auto Val2Way( TYPE w )->WAY{

			if ( w == 0 ) { return WAY::Z;	}
			if ( w <  0 ) { return WAY::M;	}
			if ( w >  0 ) { return WAY::P;	}

			return WAY::Z;
		}

	public:
		static constexpr auto Way2Val( WAY w )->int{

			switch ( w ) {
			case WAY::M: return -1;
			case WAY::Z: return  0;
			case WAY::P: return +1;
			}

			return 0;
		}

		template<typename TYPE>static constexpr auto Way2Val( TYPE w )->int{

			if ( w == 0 ) { return  0;	}
			if ( w <  0 ) { return -1;	}
			if ( w >  0 ) { return +1;	}

			return 0;
		}
	};
}

namespace opal {
	template<>constexpr auto TREEWAY::Val2Way( float w )->WAY{

		if ( FZERO( w ) ) { return WAY::Z;	}
		if ( w <  0.f   ) { return WAY::M;	}
		if ( w >  0.f   ) { return WAY::P;	}

		return WAY::Z;
	}

	template<>constexpr auto TREEWAY::Way2Val( float w )->int{

		if ( FZERO( w ) ) { return  0;	}
		if ( w <  0.f   ) { return -1;	}
		if ( w >  0.f   ) { return +1;	}

		return 0;
	}
}

// End Of File
