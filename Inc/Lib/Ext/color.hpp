#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 成分抽出
//========================================
namespace opal {
	constexpr UCHAR COLOR_A( UINT c ){ return ( c >> 24 ) & 0xff; }
	constexpr UCHAR COLOR_R( UINT c ){ return ( c >> 16 ) & 0xff; }
	constexpr UCHAR COLOR_G( UINT c ){ return ( c >>  8 ) & 0xff; }
	constexpr UCHAR COLOR_B( UINT c ){ return ( c >>  0 ) & 0xff; }
	constexpr UINT  COLOR_C( UINT c, UCHAR a ){ return ((a<<24)|(c&0x00ffffff));	}
	constexpr UINT  COLOR_V( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ return ((a<<24)|(r<<16)|(g<<8)|(b));	}
}

//========================================
// 色相
//========================================
#undef RGB
namespace opal {
	struct RGB {
		constexpr RGB();
		constexpr RGB( UCHAR, UCHAR, UCHAR, UCHAR = 0xff );

		UCHAR	a;	// 透明度				0x00-0xff
		UCHAR	r;	// 赤成分(Red)			0x00-0xff
		UCHAR	g;	// 緑成分(Green)		0x00-0xff
		UCHAR	b;	// 青成分(Blue)			0x00-0xff

	public:
		constexpr bool operator ==( const RGB& c ) const { return ( a == c.a ) && ( r == c.r ) && ( g == c.g ) && ( b == c.b ); }
		constexpr bool operator !=( const RGB& c ) const { return ( a != c.a ) || ( r != c.r ) || ( g != c.g ) || ( b != c.b ); }
	};

	struct HSV {
		constexpr HSV();
		constexpr HSV( float, UCHAR, UCHAR, UCHAR = 0xff );

		UCHAR	a;	// 透明度				0x00-0xff
		float	h;	// 彩度(Hue)			degree
		UCHAR	s;	// 色相(Saturation)		0x00-0xff
		UCHAR	v;	// 明度(Value)			0x00-0xff

	public:
#define EQ(a,b)	  FEQUAL(a,b)
#define NE(a,b)	 !FEQUAL(a,b)
		constexpr bool operator ==( const HSV& c ) const { return ( a == c.a ) && EQ( s, c.s ) && ( s == c.v ) && ( v == c.v ); }
		constexpr bool operator !=( const HSV& c ) const { return ( a != c.a ) || NE( s, c.s ) || ( s != c.v ) || ( v != c.v ); }
#undef EQ
#undef NE
	};
}

namespace opal {
	constexpr UCHAR rgb_max( const RGB& v ){ return ( v.r >= v.g ) ? ( v.r >= v.b ) ? v.r : v.b : ( v.g >= v.b ) ? v.g : v.b;	}
	constexpr UCHAR rgb_min( const RGB& v ){ return ( v.b <= v.g ) ? ( v.b <= v.r ) ? v.b : v.r : ( v.g <= v.r ) ? v.g : v.r;	}

	constexpr HSV rgb2hsv( const RGB& rgb ){


		const auto	a = rgb.a;
		const auto	r = rgb.r;
		const auto	g = rgb.g;
		const auto	b = rgb.b;
		const auto	m = rgb_max( rgb );
		const auto	n = rgb_min( rgb );
		const float	d = m - n;
		const float	v = m;
		const float	s = ( m ) ? ( 0xff * ( d / m ) ) : 0;
		auto		h = 0.0f;

		if ( d > 0 ) {
			if ( m == r ) { h = ( g - b ) * 60 / d +   0; 	}
			if ( m == g ) { h = ( b - r ) * 60 / d + 120;	}
			if ( m == b ) { h = ( r - g ) * 60 / d + 240;	}
			if ( h < 0  ) { h = ::fmodf( h+360, 360 ); }
		}

		return HSV( h, s, v, a );
	}

	constexpr RGB hsv2rgb( const HSV& hsv ){

		const auto	a = hsv.a;
		//[vs2019]const auto	h = std::fmodf( hsv.h, 360 );
		const auto	h = ( int )hsv.h%360;
		const auto	s = hsv.s;
		const auto	v = hsv.v;
		const auto	m = v;
		const auto	n = m - ( s / 0xff * m );
		const auto	d = m - n;
		const auto	u = UINT( h ) / 60;
		auto		r = 0.0f;
		auto		g = 0.0f;
		auto		b = 0.0f;

		switch ( u ) {
		case 0: g = ( h       ) * d / 60 + n; r = m; b = n;	break;
		case 1: r = ( 120 - h ) * d / 60 + n; g = m; b = n;	break;
		case 2: b = ( h - 120 ) * d / 60 + n; g = m; r = n;	break;
		case 3: g = ( 240 - h ) * d / 60 + n; b = m; r = n;	break;
		case 4: r = ( h - 240 ) * d / 60 + n; b = m; g = n;	break;
		case 5: b = ( 360 - h ) * d / 60 + n; r = m; g = n;	break;
		}

		return RGB( UCHAR( ::round( r ) ), UCHAR( ::round( g ) ), UCHAR( ::round( b ) ), a );
 	}

	constexpr HSV rgb2hsv( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ return rgb2hsv( RGB( r, g, b, a ) ); }
	constexpr RGB hsv2rgb( float h, float s, float v, UCHAR a ){ return hsv2rgb( HSV( h, s, v, a ) ); }
}

namespace opal {
	constexpr RGB::RGB() : a(0),r(0),g(0),b(0) {}
	constexpr HSV::HSV() : a(0),h(0),s(0),v(0) {}
	constexpr RGB::RGB( UCHAR r, UCHAR g, UCHAR b, UCHAR a ) : a(a),r(r),g(g),b(b) {}
	constexpr HSV::HSV( float h, UCHAR s, UCHAR v, UCHAR a ) : a(a),h(h),s(s),v(v) {}
}

//========================================
// カラー
//========================================
namespace opal {
	struct VCOLOR {
	public:
		constexpr VCOLOR();
		constexpr VCOLOR( float, float, float, float = 1.0f );
		constexpr VCOLOR( const VCOLOR& );

		float r,g,b,a;

	public:
#define EQ(a,b)	  FEQUAL(a,b)
#define NE(a,b)	 !FEQUAL(a,b)
		constexpr bool operator ==( const VCOLOR& c ) const { return EQ( a, c.a ) && EQ( r, c.r ) && EQ( g, c.g ) && EQ( b, c.b ); }
		constexpr bool operator !=( const VCOLOR& c ) const { return NE( a, c.a ) || NE( r, c.r ) || NE( g, c.g ) || NE( b, c.b ); }
#undef EQ
#undef NE
	};

	constexpr VCOLOR::VCOLOR() : r(1.0f), g(1.0f), b(1.0f),a(1.0f) {}
	constexpr VCOLOR::VCOLOR( float r, float g, float b, float a ) : r(r), g(g), b(b),a(a) {}
	constexpr VCOLOR::VCOLOR( const VCOLOR& v ) : VCOLOR( v.r, v.b, v.g, v.a ) {}
}

namespace opal {
	class COLOR {
	public:
		constexpr COLOR() : rgb(0,0,0,0xff), hsv(rgb2hsv(0,0,0,0xff)) {}
		constexpr COLOR( UCHAR r, UCHAR g, UCHAR b, UCHAR a = 0xff ) : rgb(r,g,b,a), hsv(rgb2hsv(r,g,b,a)) {}
		constexpr COLOR( RGB    v ) : rgb(v), hsv(rgb2hsv(v)) {}
		constexpr COLOR( HSV    v ) : rgb(hsv2rgb(v)), hsv(v) {}
		constexpr COLOR( UINT   v ) : rgb(COLOR_R(v),COLOR_G(v),COLOR_B(v),COLOR_A(v)), hsv(rgb2hsv(rgb)) {}
		constexpr COLOR( VCOLOR v ) : rgb(UCHAR(v.a*0xff),UCHAR(v.g*0xff),UCHAR(v.b*0xff),UCHAR(v.r*0xff)), hsv(rgb2hsv(rgb)) {}

	private:
		RGB rgb;
		HSV hsv;

	public:
		constexpr bool operator ==( const COLOR& c ) const { return ( rgb == c.rgb ) && ( hsv == c.hsv );	}
		constexpr bool operator !=( const COLOR& c ) const { return ( rgb != c.rgb ) || ( hsv != c.hsv );	}
		constexpr bool operator ==( UINT c ) const { return *this == COLOR( c );	}
		constexpr bool operator !=( UINT c ) const { return *this != COLOR( c );	}

	public:
		constexpr operator RGB(    void ) const { return rgb;	}
		constexpr operator HSV(    void ) const { return hsv;	}
		constexpr operator UINT(   void ) const { return COLOR_V( rgb.r, rgb.g, rgb.b, rgb.a ); }
		constexpr operator VCOLOR( void ) const { return VCOLOR( rgb.r/0xff, rgb.g/0xff, rgb.b/0xff, rgb.a/0xff );	}

	public:
		virtual COLOR GetColor_( void    ) const { return *this;		}
		virtual void  SetColor_( COLOR v ){ hsv = rgb2hsv( rgb = v );	}

	public:
		virtual COLOR GetColor( void    ) const { return GetColor_();	}
		virtual void  SetColor( COLOR c ){ SetColor_(        c   );	}
		virtual void  SetColor( RGB   v ){ SetColor_( COLOR( v ) );	}
		virtual void  SetColor( HSV   v ){ SetColor_( COLOR( v ) );	}
		virtual void  SetColor( void    ){ SetColor_( COLOR()    );	}
		virtual void  SetColor( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ SetColor_( COLOR( r, g, b, a )     );	}
		virtual void  SetColor( UCHAR r, UCHAR g, UCHAR b          ){ SetColor_( COLOR( r, g, b, rgb.a ) );	}

	public:
		virtual void  SetColorRGB( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ SetColor_( RGB( r, g, b, a ) );	}
		virtual void  SetColorHSV( float h, UCHAR s, UCHAR v, UCHAR a ){ SetColor_( HSV( h, s, v, a ) );	}
		virtual RGB   GetColorRGB( void  ) const { return GetColor_();	}
		virtual HSV   GetColorHSV( void  ) const { return GetColor_();	}

	public:
		virtual void  SetColorR( UCHAR r ){ SetColor_( COLOR( r, rgb.g, rgb.b, rgb.a ) ); }
		virtual void  SetColorG( UCHAR g ){ SetColor_( COLOR( rgb.r, g, rgb.b, rgb.a ) ); }
		virtual void  SetColorB( UCHAR b ){ SetColor_( COLOR( rgb.r, rgb.g, b, rgb.a ) ); }
		virtual void  SetColorA( UCHAR a ){ SetColor_( COLOR( rgb.r, rgb.g, rgb.b, a ) ); }
		virtual void  AddColorR( short r ){ SetColor_( COLOR( rgb.r+r, rgb.g, rgb.b, rgb.a ) ); }
		virtual void  AddColorG( short g ){ SetColor_( COLOR( rgb.r, rgb.g+g, rgb.b, rgb.a ) ); }
		virtual void  AddColorB( short b ){ SetColor_( COLOR( rgb.r, rgb.g, rgb.b+b, rgb.a ) ); }
		virtual void  AddColorA( short a ){ SetColor_( COLOR( rgb.r, rgb.g, rgb.b, rgb.a+a ) ); }
		virtual UCHAR GetColorR( void ) const { return static_cast<RGB>( GetColor_() ).r;	}
		virtual UCHAR GetColorG( void ) const { return static_cast<RGB>( GetColor_() ).g;	}
		virtual UCHAR GetColorB( void ) const { return static_cast<RGB>( GetColor_() ).b;	}
		virtual UCHAR GetColorA( void ) const { return static_cast<RGB>( GetColor_() ).a;	}

	public:
		virtual float R( void ) const { return ( float )static_cast<RGB>( GetColor_() ).r/0xff;	}
		virtual float G( void ) const { return ( float )static_cast<RGB>( GetColor_() ).g/0xff;	}
		virtual float B( void ) const { return ( float )static_cast<RGB>( GetColor_() ).b/0xff;	}
		virtual float A( void ) const { return ( float )static_cast<RGB>( GetColor_() ).a/0xff;	}

	public:
		virtual void  SetColorH( float h ){ SetColor_( COLOR( HSV( h, hsv.s, hsv.v, hsv.a ) ) );	}
		virtual void  SetColorS( float s ){ SetColor_( COLOR( HSV( hsv.h, s, hsv.v, hsv.a ) ) );	}
		virtual void  SetColorV( float v ){ SetColor_( COLOR( HSV( hsv.h, hsv.s, v, hsv.a ) ) );	}
		virtual void  AddColorH( float h ){ SetColor_( COLOR( HSV( hsv.h+h, hsv.s, hsv.v, hsv.a ) ) );	}
		virtual void  AddColorS( float s ){ SetColor_( COLOR( HSV( hsv.h, hsv.s+s, hsv.v, hsv.a ) ) );	}
		virtual void  AddColorV( float v ){ SetColor_( COLOR( HSV( hsv.h, hsv.s, hsv.v+v, hsv.a ) ) );	}
		virtual float GetColorH( void ) const { return static_cast<HSV>( GetColor_() ).h;	}
		virtual float GetColorS( void ) const { return static_cast<HSV>( GetColor_() ).s;	}
		virtual float GetColorV( void ) const { return static_cast<HSV>( GetColor_() ).v;	}

	public:
		virtual void SetColor( UINT   v ){ SetColor_( COLOR( v ) );	}
		virtual void SetColor( VCOLOR v ){ SetColor_( COLOR( UCHAR( v.a*0xff ), UCHAR( v.g*0xff ), UCHAR( v.b*0xff ), UCHAR( v.r*0xff ) ) );	}

	public:
		virtual COLOR& operator =( RGB    v ){ SetColor_( v ); return *this;	}
		virtual COLOR& operator =( HSV    v ){ SetColor_( v ); return *this;	}
		virtual COLOR& operator =( UINT   v ){ SetColor_( v ); return *this;	}
		virtual COLOR& operator =( VCOLOR v ){ SetColor_( v ); return *this;	}
	};
}

//========================================
// 色生成
//========================================
namespace opal {
	constexpr COLOR COLOR_RGB( UCHAR r, UCHAR g, UCHAR b, UCHAR a = 0xff ){ return COLOR( r, g, b, a );	 }
	constexpr COLOR COLOR_HSV( float h, UCHAR s, UCHAR v, UCHAR a = 0xff ){ return COLOR( h, s, v, a );	 }

	constexpr float COLOR_C2V( UCHAR c ){ return ( float )c/0xff;	}
	constexpr UCHAR COLOR_V2C( float c ){ return ( UCHAR )c*0xff;	}

	constexpr float COLOR_C2R( UINT c ){ return COLOR_C2V( COLOR_R( c ) );	}
	constexpr float COLOR_C2G( UINT c ){ return COLOR_C2V( COLOR_G( c ) );	}
	constexpr float COLOR_C2B( UINT c ){ return COLOR_C2V( COLOR_B( c ) );	}
	constexpr float COLOR_C2A( UINT c ){ return COLOR_C2V( COLOR_A( c ) );	}

	constexpr UCHAR COLOR_V2R( float c ){ return COLOR_R( COLOR_V2C( c ) );	}
	constexpr UCHAR COLOR_V2G( float c ){ return COLOR_G( COLOR_V2C( c ) );	}
	constexpr UCHAR COLOR_V2B( float c ){ return COLOR_B( COLOR_V2C( c ) );	}
	constexpr UCHAR COLOR_V2A( float c ){ return COLOR_A( COLOR_V2C( c ) );	}
}

namespace opal {
	constexpr COLOR COLOR_Rev( const COLOR& v ){

		const auto	p = ( RGB )v;
		const auto	a = p.a;
		const auto	r = p.r;
		const auto	g = p.g;
		const auto	b = p.b;
		const auto	x = 0xff;

		return COLOR( x-r, x-g, x-b, a );
	}

	constexpr COLOR COLOR_Com( const COLOR& v ){

		const auto	p = ( RGB )v;
		const auto	a = p.a;
		const auto	r = p.r;
		const auto	g = p.g;
		const auto	b = p.b;
		const auto	m = ( r > g ) ? ( ( r > b ) ? r : b ) : ( ( g > b ) ? g : b );
		const auto	n = ( r < g ) ? ( ( r < b ) ? r : b ) : ( ( g < b ) ? g : b );
		const auto	x = m + n;

		return COLOR( x-r, x-g, x-b, a );
	}

	constexpr COLOR COLOR_Rev( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ return COLOR_Rev( COLOR( r, g, b, a ) );	}
	constexpr COLOR COLOR_Com( UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ return COLOR_Com( COLOR( r, g, b, a ) );	}
}

//========================================
// カラーコード
//========================================
namespace opal {
	constexpr UINT COLOR_Alpha(   UINT c, UCHAR a ){ return ((c)&0x00ffffff)|((a)<<24); }
	constexpr UINT COLOR_NoAlpha( UINT c          ){ return COLOR_Alpha( c, 0xff ); }
	constexpr UINT COLOR_Alpha(   UCHAR r, UCHAR g, UCHAR b, UCHAR a ){ return COLOR_Alpha(   COLOR_RGB( r, g, b ), a ); }
	constexpr UINT COLOR_NoAlpha( UCHAR r, UCHAR g, UCHAR b          ){ return COLOR_NoAlpha( COLOR_RGB( r, g, b )    ); }
	extern const char* COLOR_Name( UINT c );
	extern UINT COLOR_Code( const std::string& n );
	extern UINT COLOR_Code( const char* n );

	enum COLOR_CODE {
		BLANK						= 0x00000000,
		BLACK						= 0xff000000,
		SKYBLUE						= 0xff87ceeb,
		KHAKI						= 0xfff0e68c,
		NAVY						= 0xff000080,
		LIGHTSKYBLUE				= 0xff87cefa,
		ALICEBLUE					= 0xfff0f8ff,
		DARKBLUE					= 0xff00008b,
		BLUEVIOLET					= 0xff8a2be2,
		HONEYDEW					= 0xfff0fff0,
		MEDIUMBLUE					= 0xff0000cd,
		DARKRED						= 0xff8b0000,
		AZURE						= 0xfff0ffff,
		BLUE						= 0xff0000ff,
		DARKMAGENTA					= 0xff8b008b,
		SANDYBROWN					= 0xfff4a460,
		DARKGREEN					= 0xff006400,
		SADDLEBROWN					= 0xff8b4513,
		WHEAT						= 0xfff5deb3,
		GREEN						= 0xff008000,
		DARKSEAGREEN				= 0xff8fbc8f,
		BEIGE						= 0xfff5f5dc,
		TEAL						= 0xff008080,
		LIGHTGREEN					= 0xff90ee90,
		WHITESMOKE					= 0xfff5f5f5,
		DARKCYAN					= 0xff008b8b,
		MEDIUMPURPLE				= 0xff9370db,
		MINTCREAM					= 0xfff5fffa,
		DEEPSKYBLUE					= 0xff00bfff,
		DARKVIOLET					= 0xff9400d3,
		GHOSTWHITE					= 0xfff8f8ff,
		DARKTURQUOISE				= 0xff00ced1,
		PALEGREEN					= 0xff98fb98,
		SALMON						= 0xfffa8072,
		MEDIUMSPRINGGREEN			= 0xff00fa9a,
		DARKORCHID					= 0xff9932cc,
		ANTIQUEWHITE				= 0xfffaebd7,
		LIME						= 0xff00ff00,
		YELLOWGREEN					= 0xff9acd32,
		LINEN						= 0xfffaf0e6,
		SPRINGGREEN					= 0xff00ff7f,
		SIENNA						= 0xffa0522d,
		LIGHTGOLDENRODYELLOW		= 0xfffafad2,
		AQUA						= 0xff00ffff,
		BROWN						= 0xffa52a2a,
		OLDLACE						= 0xfffdf5e6,
		CYAN						= 0xff00ffff,
		DARKGRAY					= 0xffa9a9a9,
		RED							= 0xffff0000,
		MIDNIGHTBLUE				= 0xff191970,
		LIGHTBLUE					= 0xffadd8e6,
		FUCHSIA						= 0xffff00ff,
		DODGERBLUE					= 0xff1e90ff,
		GREENYELLOW					= 0xffadff2f,
		MAGENTA						= 0xffff00ff,
		LIGHTSEAGREEN				= 0xff20b2aa,
		PALETURQUOISE				= 0xffafeeee,
		DEEPPINK					= 0xffff1493,
		FORESTGREEN					= 0xff228b22,
		LIGHTSTEELBLUE				= 0xffb0c4de,
		ORANGERED					= 0xffff4500,
		SEAGREEN					= 0xff2e8b57,
		POWDERBLUE					= 0xffb0e0e6,
		TOMATO						= 0xffff6347,
		DARKSLATEGRAY				= 0xff2f4f4f,
		FIREBRICK					= 0xffb22222,
		HOTPINK						= 0xffff69b4,
		LIMEGREEN					= 0xff32cd32,
		DARKGOLDENROD				= 0xffb8860b,
		CORAL						= 0xffff7f50,
		MEDIUMSEAGREEN				= 0xff3cb371,
		MEDIUMORCHID				= 0xffba55d3,
		DARKORANGE					= 0xffff8c00,
		TURQUOISE					= 0xff40e0d0,
		ROSYBROWN					= 0xffbc8f8f,
		LIGHTSALMON					= 0xffffa07a,
		ROYALBLUE					= 0xff4169e1,
		DARKKHAKI					= 0xffbdb76b,
		ORANGE						= 0xffffa500,
		STEELBLUE					= 0xff4682b4,
		SILVER						= 0xffc0c0c0,
		LIGHTPINK					= 0xffffb6c1,
		DARKSLATEBLUE				= 0xff483d8b,
		MEDIUMVIOLETRED				= 0xffc71585,
		PINK						= 0xffffc0cb,
		MEDIUMTURQUOISE				= 0xff48d1cc,
		INDIANRED					= 0xffcd5c5c,
		GOLD						= 0xffffd700,
		INDIGO						= 0xff4b0082,
		PERU						= 0xffcd853f,
		PEACHPUFF					= 0xffffdab9,
		DARKOLIVEGREEN				= 0xff556b2f,
		CHOCOLATE					= 0xffd2691e,
		NAVAJOWHITE					= 0xffffdead,
		CADETBLUE					= 0xff5f9ea0,
		TAN							= 0xffd2b48c,
		MOCCASIN					= 0xffffe4b5,
		CORNFLOWERBLUE				= 0xff6495ed,
		LIGHTGREY					= 0xffd3d3d3,
		BISQUE						= 0xffffe4c4,
		MEDIUMAQUAMARINE			= 0xff66cdaa,
		THISTLE						= 0xffd8bfd8,
		MISTYROSE					= 0xffffe4e1,
		DIMGRAY						= 0xff696969,
		ORCHID						= 0xffda70d6,
		BLANCHEDALMOND				= 0xffffebcd,
		SLATEBLUE					= 0xff6a5acd,
		GOLDENROD					= 0xffdaa520,
		PAPAYAWHIP					= 0xffffefd5,
		OLIVEDRAB					= 0xff6b8e23,
		PALEVIOLETRED				= 0xffdb7093,
		LAVENDERBLUSH				= 0xfffff0f5,
		SLATEGRAY					= 0xff708090,
		CRIMSON						= 0xffdc143c,
		SEASHELL					= 0xfffff5ee,
		LIGHTSLATEGRAY				= 0xff778899,
		GAINSBORO					= 0xffdcdcdc,
		CORNSILK					= 0xfffff8dc,
		MEDIUMSLATEBLUE				= 0xff7b68ee,
		PLUM						= 0xffdda0dd,
		LEMONCHIFFON				= 0xfffffacd,
		LAWNGREEN					= 0xff7cfc00,
		BURLYWOOD					= 0xffdeb887,
		FLORALWHITE					= 0xfffffaf0,
		CHARTREUSE					= 0xff7fff00,
		LIGHTCYAN					= 0xffe0ffff,
		SNOW						= 0xfffffafa,
		AQUAMARINE					= 0xff7fffd4,
		LAVENDER					= 0xffe6e6fa,
		YELLOW						= 0xffffff00,
		MAROON						= 0xff800000,
		DARKSALMON					= 0xffe9967a,
		LIGHTYELLOW					= 0xffffffe0,
		PURPLE						= 0xff800080,
		VIOLET						= 0xffee82ee,
		IVORY						= 0xfffffff0,
		OLIVE						= 0xff808000,
		PALEGOLDENROD				= 0xffeee8aa,
		WHITE						= 0xffffffff,
		GRAY						= 0xff808080,
		LIGHTCORAL					= 0xfff08080,
		DARKYELLOW					= 0xff9b870c
	};
}

// End Of File
