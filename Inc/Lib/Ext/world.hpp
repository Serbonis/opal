#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// ワールド変換行列から各要素を抽出
//========================================
namespace opal {
	struct VECTOR3;
	struct MATRIX;

	extern VECTOR3* WorldScale(  const MATRIX* m, VECTOR3* v = nullptr );
	extern VECTOR3* WorldRotate( const MATRIX* m, VECTOR3* v = nullptr );
	extern VECTOR3* WorldTrans(  const MATRIX* m, VECTOR3* v = nullptr );
}

//========================================
// ローカルの座標軸のワールドでの向き
//========================================
namespace opal {
	struct VECTOR3;
	struct MATRIX;

	extern VECTOR3* LocalAxisX( const MATRIX* m, VECTOR3* v = nullptr );
	extern VECTOR3* LocalAxisY( const MATRIX* m, VECTOR3* v = nullptr );
	extern VECTOR3* LocalAxisZ( const MATRIX* m, VECTOR3* v = nullptr );
}

// End Of File
