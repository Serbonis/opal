#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/color.hpp"

//========================================
// マテリアル
//========================================
namespace opal {
	struct MATERIAL {
		constexpr MATERIAL();
		constexpr MATERIAL( VCOLOR,									// Diffuse;
							VCOLOR,									// Ambient;
							VCOLOR,									// Specular;
							VCOLOR,									// Emissive;
							float = 1.0f );							// Power
		constexpr MATERIAL( float, float, float, float,				// Diffuse;
							float, float, float, float,				// Ambient;
							float, float, float, float,				// Specular;
							float, float, float, float,				// Emissive;
							float = 1.0f );							// Power
		constexpr MATERIAL( const MATERIAL& );
		constexpr MATERIAL( float, float, float, float = 1.0f );	// Diffuse;
		constexpr MATERIAL( float, float, float, 					// Diffuse
							float, float, float );					// Ambient
		constexpr MATERIAL( float, float, float, float,				// Diffuse
							float, float, float, float );			// Ambient
		constexpr MATERIAL( VCOLOR );								// Diffuse;
		constexpr MATERIAL( VCOLOR, VCOLOR );						// Diffuse & Ambient

		VCOLOR	Diffuse;
		VCOLOR	Ambient;
		VCOLOR	Specular;
		VCOLOR	Emissive;
		float   Power;

		virtual void SetDiffuse(  float, float, float, float = 1.0f );
		virtual void SetAmbient(  float, float, float, float = 1.0f );
		virtual void SetSpecular( float, float, float, float = 1.0f );
		virtual void SetEmissive( float, float, float, float = 1.0f );
		virtual void SetDiffuse(  VCOLOR );
		virtual void SetAmbient(  VCOLOR );
		virtual void SetSpecular( VCOLOR );
		virtual void SetEmissive( VCOLOR );
		virtual void SetPower( float = 0.0f );

		virtual VCOLOR GetDiffuse(  void ) const;
		virtual VCOLOR GetAmbient(  void ) const;
		virtual VCOLOR GetSpecular( void ) const;
		virtual VCOLOR GetEmissive( void ) const;
		virtual float  GetPower(    void ) const;

		virtual MATERIAL& DIFFUSE(  float, float, float, float = 1.0f );
		virtual MATERIAL& AMBIENT(  float, float, float, float = 1.0f );
		virtual MATERIAL& SPECULAR( float, float, float, float = 1.0f );
		virtual MATERIAL& EMISSIVE( float, float, float, float = 1.0f );
		virtual MATERIAL& EMISSIVE( float, float, float, float, float );
		virtual MATERIAL& DIFFUSE(  VCOLOR );
		virtual MATERIAL& AMBIENT(  VCOLOR );
		virtual MATERIAL& SPECULAR( VCOLOR );
		virtual MATERIAL& EMISSIVE( VCOLOR );
		virtual MATERIAL& EMISSIVE( VCOLOR, float );

#define EQ(a,b)	  FEQUAL(a,b)
#define NE(a,b)	 !FEQUAL(a,b)
		bool operator ==( const MATERIAL& m ) const {

			return ( ( Diffuse  == m.Diffuse  ) &&
					 ( Emissive == m.Emissive ) &&
					 ( Ambient  == m.Ambient  ) &&
					 ( Specular == m.Specular ) &&
					 ( EQ( Power, m.Power )   ) );
		}

		bool operator !=( const MATERIAL& m ) const {

			return ( ( Diffuse  != m.Diffuse  ) ||
					 ( Emissive != m.Emissive ) ||
					 ( Ambient  != m.Ambient  ) ||
					 ( Specular != m.Specular ) ||
					 ( NE( Power, m.Power )   ) );
		}
#undef EQ
#undef NE
	};

	extern void MaterialDebug( const MATERIAL* );
	extern void MaterialDebug( const MATERIAL& );
	extern void MaterialDump(  const MATERIAL* );
	extern void MaterialDump(  const MATERIAL& );
}

//----------------------------------------
// constructor
//----------------------------------------
namespace opal {
	constexpr MATERIAL::MATERIAL() :
		Diffuse( 0,0,0,0),
		Ambient( 0,0,0,0),
		Specular(0,0,0,0),
		Emissive(0,0,0,0),
		Power( 1.0f )
	{}

	constexpr MATERIAL::MATERIAL( VCOLOR d,
						VCOLOR a,
						VCOLOR s,
						VCOLOR e,
						float  p ) :
		Diffuse(  d ),
		Ambient(  a ),
		Specular( s ),
		Emissive( e ),
		Power(    p )
	{}

	constexpr MATERIAL::MATERIAL( float dr, float dg, float db, float da,
								  float ar, float ag, float ab, float aa,
								  float sr, float sg, float sb, float sa,
								  float er, float eg, float eb, float ea,
								  float p ) :
		Diffuse(  dr, dg, db, da ),
		Ambient(  ar, ag, ab, aa ),
		Specular( sr, sg, sb, sa ),
		Emissive( er, eg, eb, ea ),
		Power(    p )
	{}

	constexpr MATERIAL::MATERIAL( const MATERIAL& d ) :
		Diffuse(  d.Diffuse.r, d.Diffuse.g, d.Diffuse.b, d.Diffuse.a  ),
		Ambient(  d.Ambient.r, d.Ambient.g, d.Ambient.b, d.Ambient.a  ),
		Specular( d.Specular.r,d.Specular.g,d.Specular.b,d.Specular.a ),
		Emissive( d.Emissive.r,d.Emissive.g,d.Emissive.b,d.Emissive.a ),
		Power(    d.Power )
	{}

	constexpr MATERIAL::MATERIAL( float dr, float dg, float db, float da ) :
		Diffuse( dr,dg,db,da),
		Ambient(  0, 0, 0, 0),
		Specular( 0, 0, 0, 0),
		Emissive( 0, 0, 0, 0),
		Power( 1.0f )
	{}

	constexpr MATERIAL::MATERIAL( float dr, float dg, float db,
						float ar, float ag, float ab ) :
		Diffuse( dr,dg,db, 1),
		Ambient( ar,ag,ab, 1),
		Specular( 0, 0, 0, 0),
		Emissive( 0, 0, 0, 0),
		Power( 1.0f )
	{}

	constexpr MATERIAL::MATERIAL( float dr, float dg, float db, float da,
						float ar, float ag, float ab, float aa ) :
		Diffuse( dr,dg,db,da),
		Ambient( ar,ag,ab,aa),
		Specular( 0, 0, 0, 0),
		Emissive( 0, 0, 0, 0),
		Power( 1.0f )
	{}

	constexpr MATERIAL::MATERIAL( VCOLOR dv ) :
		Diffuse( dv ),
		Ambient(  0, 0, 0, 0),
		Specular( 0, 0, 0, 0),
		Emissive( 0, 0, 0, 0),
		Power( 1.0f )
	{}

	constexpr MATERIAL::MATERIAL( VCOLOR dv, VCOLOR av ) :
		Diffuse( dv ),
		Ambient( av ),
		Specular( 0, 0, 0, 0),
		Emissive( 0, 0, 0, 0),
		Power( 1.0f )
	{}
}

// End Of File
