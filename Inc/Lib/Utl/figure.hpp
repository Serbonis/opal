#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Ext/vector.hpp"
#include "Ext/matrix.hpp"

//========================================
// �}�`
//========================================
//----------------------------------------
// ������
//----------------------------------------
namespace opal {
	extern VECTOR3 EquationLine(   const VECTOR2&, const VECTOR2& );
	extern VECTOR3 EquationCircle( const VECTOR2&, const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	extern float DistancePoint(  const VECTOR2& );
	extern float DistancePoint2( const VECTOR2& );
	extern float DistancePointPoint(  const VECTOR2&, const VECTOR2& );
	extern float DistancePointPoint2( const VECTOR2&, const VECTOR2& );
	extern float DistancePointLine(   const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern float DistancePointLine2(  const VECTOR2&, const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// �p�x
//----------------------------------------
namespace opal {
	extern float AngleLine( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern float AngleLine( const VECTOR2&, const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// �P��/����
//----------------------------------------
namespace opal {
	extern VECTOR2 UnitLine( const VECTOR2&, const VECTOR2& );
	extern VECTOR2 OrthLine( const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// �ʒu
//----------------------------------------
namespace opal {
	extern std::vector<float> RelationLineLine( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );

	extern bool RelationLineLineAA( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern bool RelationLineLineAR( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern bool RelationLineLineAS( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern bool RelationLineLineSS( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern bool RelationLineLineRS( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern bool RelationLineLineRR( const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// ����
//----------------------------------------
namespace opal {
	extern int SequencePoint( const VECTOR2&, const VECTOR2&, const VECTOR2& );
}

//----------------------------------------
// ��_
//----------------------------------------
namespace opal {
	extern std::vector<VECTOR2> IntersectionLineLine(   const VECTOR2&, const VECTOR2&, const VECTOR2&, const VECTOR2& );
	extern std::vector<VECTOR2> IntersectionLineCircle( const VECTOR2&, const VECTOR2&, const VECTOR2&, float );
}

// End Of File
