#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// イージング
//========================================
namespace opal {
	namespace EASING {
		using FUNC = std::function<float(float,float)>;

		extern float easing( FUNC f, float c, float t );

		extern float linear(            float c, float t );
		extern float quadratic_in(      float c, float t );
		extern float quadratic_out(     float c, float t );
		extern float quadratic_inout(   float c, float t );
		extern float cubic_in(          float c, float t );
		extern float cubic_out(         float c, float t );
		extern float cubic_inout(       float c, float t );
		extern float quartic_in(        float c, float t );
		extern float quartic_out(       float c, float t );
		extern float quartic_inout(     float c, float t );
		extern float quintic_in(        float c, float t );
		extern float quintic_out(       float c, float t );
		extern float quintic_inout(     float c, float t );
		extern float sinusoidal_in(     float c, float t );
		extern float sinusoidal_out(    float c, float t );
		extern float sinusoidal_inout(  float c, float t );
		extern float exponential_in(    float c, float t );
		extern float exponential_out(   float c, float t );
		extern float exponential_inout( float c, float t );
		extern float circular_in(       float c, float t );
		extern float circular_out(      float c, float t );
		extern float circular_inout(    float c, float t );
	}
}

// End Of File
