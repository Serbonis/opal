#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

#include "Std/const.hpp"

//========================================
// 角度
//========================================
//----------------------------------------
// RADIAN & DEGREE
//----------------------------------------
namespace opal {
	constexpr float RADIAN( float d ){ return d * OPAL_PI / 180.0f; }
	constexpr float DEGREE( float r ){ return r * 180.0f / OPAL_PI; }
}

#define RAD( a )	opal::RADIAN( a )
#define DEG( a )	opal::DEGREE( a )

//----------------------------------------
// 軸
//----------------------------------------
namespace opal {
	enum {
		  AXIS_O = -1,
		  AXIS_X,		// X軸
		  AXIS_Y,		// Y軸
		  AXIS_Z,		// Z軸
		  AXIS_O_MAX,
	};

	enum {
		  AXIS_OO = -1,
		  AXIS_PX,
		  AXIS_MX,
		  AXIS_PY,
		  AXIS_MY,
		  AXIS_PZ,
		  AXIS_MZ,
		  AXIS_OO_MAX,
	};

	enum {
		  DICE_0 	= AXIS_OO,
		  DICE_1	= AXIS_PY,	// 天一
		  DICE_2	= AXIS_MX,	// 西二
		  DICE_3	= AXIS_MZ,	// 南三
		  DICE_4	= AXIS_PZ,	// 北四
		  DICE_5	= AXIS_PX,	// 東五
		  DICE_6	= AXIS_MY,	// 地六
		  DICE_MAX	= AXIS_OO_MAX,
	};
}

//----------------------------------------
// 左右
//----------------------------------------
namespace opal {
	enum SIDE {
		SIDE_X =  0,
		SIDE_L = -1,
		SIDE_R = +1,
	};
}

//----------------------------------------
// 方向
//----------------------------------------
//     ↑N
//     0
//     │+z
//-x   │    +x
//3──┼──→1E
//     │
//     │-z
//     2
//----------------------------------------
namespace opal {
	enum DIX {
		DIX_0,
		DIX_1,
		DIX_2,
		DIX_3,
		DIX_MAX,

		DIX_N	= DIX_0,
		DIX_E	= DIX_1,
		DIX_S	= DIX_2,
		DIX_W	= DIX_3,

		DIX_X	= -1,
		DIX_U	= DIX_N,
		DIX_D	= DIX_S,
		DIX_L	= DIX_W,
		DIX_R	= DIX_E,

		DIX_F	= DIX_N,
		DIX_B	= DIX_S,
	};
}

namespace opal {
	enum {
		  DIR_X		= 0L,			// 無
		  DIR_N		= 1 << DIX_N,	// 北
		  DIR_E		= 1 << DIX_E,	// 東
		  DIR_S		= 1 << DIX_S,	// 南
		  DIR_W		= 1 << DIX_W,	// 西
		  DIR_A		= DIR_N|DIR_E|DIR_S|DIR_W,

		  DIR_NE	= DIR_N|DIR_E,
		  DIR_NW	= DIR_N|DIR_W,
		  DIR_SE	= DIR_S|DIR_E,
		  DIR_SW	= DIR_S|DIR_W,
		  DIR_EN	= DIR_N|DIR_E,
		  DIR_WN	= DIR_N|DIR_W,
		  DIR_ES	= DIR_S|DIR_E,
		  DIR_WS	= DIR_S|DIR_W,
		  DIR_EW	= DIR_E|DIR_W,
		  DIR_WE	= DIR_W|DIR_E,
		  DIR_NS	= DIR_N|DIR_S,
		  DIR_SN	= DIR_S|DIR_N,

		  DIR_NES	= DIR_N|DIR_E|DIR_S,
		  DIR_NWS	= DIR_N|DIR_W|DIR_S,
		  DIR_ESW	= DIR_E|DIR_S|DIR_W,
		  DIR_ENW	= DIR_E|DIR_N|DIR_W,
		  DIR_ESN	= DIR_E|DIR_S|DIR_N,
		  DIR_WSN	= DIR_W|DIR_S|DIR_N,
		  DIR_SWE	= DIR_S|DIR_W|DIR_E,
		  DIR_NWE	= DIR_N|DIR_W|DIR_E,
		  DIR_SNE	= DIR_S|DIR_N|DIR_E,
		  DIR_SNW	= DIR_S|DIR_N|DIR_W,
		  DIR_WES	= DIR_W|DIR_E|DIR_S,
		  DIR_WEN	= DIR_W|DIR_E|DIR_N,
		  DIR_NSE	= DIR_N|DIR_S|DIR_E,
		  DIR_NSW	= DIR_N|DIR_S|DIR_W,
		  DIR_EWS	= DIR_E|DIR_W|DIR_S,
		  DIR_EWN	= DIR_E|DIR_W|DIR_N,
		  DIR_SEN	= DIR_S|DIR_E|DIR_N,
		  DIR_SWN	= DIR_S|DIR_W|DIR_N,
		  DIR_WSE	= DIR_W|DIR_S|DIR_E,
		  DIR_WNE	= DIR_W|DIR_N|DIR_E,
		  DIR_ENS	= DIR_E|DIR_N|DIR_S,
		  DIR_WNS	= DIR_W|DIR_N|DIR_S,
		  DIR_SEW	= DIR_S|DIR_E|DIR_W,
		  DIR_NEW	= DIR_N|DIR_E|DIR_W,

		  DIR_NESW	= DIR_N|DIR_E|DIR_S|DIR_W,
		  DIR_NEWS	= DIR_N|DIR_E|DIR_W|DIR_S,
		  DIR_NWSE	= DIR_N|DIR_W|DIR_S|DIR_E,
		  DIR_NWES	= DIR_N|DIR_W|DIR_E|DIR_S,
		  DIR_NSEW	= DIR_N|DIR_S|DIR_E|DIR_W,
		  DIR_NSWE	= DIR_N|DIR_S|DIR_E|DIR_W,
		  DIR_ENSW	= DIR_E|DIR_N|DIR_S|DIR_W,
		  DIR_ENWS	= DIR_E|DIR_N|DIR_W|DIR_S,
		  DIR_WNSE	= DIR_W|DIR_N|DIR_S|DIR_E,
		  DIR_WNES	= DIR_W|DIR_N|DIR_E|DIR_S,
		  DIR_SNEW	= DIR_S|DIR_N|DIR_E|DIR_W,
		  DIR_SNWE	= DIR_S|DIR_N|DIR_E|DIR_W,
		  DIR_ESNW	= DIR_E|DIR_S|DIR_N|DIR_W,
		  DIR_EWNS	= DIR_E|DIR_W|DIR_N|DIR_S,
		  DIR_WSNE	= DIR_W|DIR_S|DIR_N|DIR_E,
		  DIR_WENS	= DIR_W|DIR_E|DIR_N|DIR_S,
		  DIR_SENW	= DIR_S|DIR_E|DIR_N|DIR_W,
		  DIR_SWNE	= DIR_S|DIR_E|DIR_N|DIR_W,
		  DIR_ESWN	= DIR_E|DIR_S|DIR_W|DIR_N,
		  DIR_EWSN	= DIR_E|DIR_W|DIR_S|DIR_N,
		  DIR_WSEN	= DIR_W|DIR_S|DIR_E|DIR_N,
		  DIR_WESN	= DIR_W|DIR_E|DIR_S|DIR_N,
		  DIR_SEWN	= DIR_S|DIR_E|DIR_W|DIR_N,
		  DIR_SWEN  = DIR_S|DIR_E|DIR_W|DIR_N,

		  DIR_U		= DIR_N,
		  DIR_D		= DIR_S,
		  DIR_L		= DIR_W,
		  DIR_R		= DIR_E,
		  DIR_UL	= DIR_U|DIR_L,
		  DIR_UR	= DIR_U|DIR_R,
		  DIR_DL	= DIR_D|DIR_L,
		  DIR_DR	= DIR_D|DIR_R,
		  DIR_UD	= DIR_U|DIR_D,
		  DIR_LR	= DIR_L|DIR_R,

		  DIR_H		= DIR_L|DIR_R,
		  DIR_V		= DIR_U|DIR_D,

		  DIR_F		= DIR_U,
		  DIR_B		= DIR_D,
		  DIR_FL	= DIR_UL,
		  DIR_FR	= DIR_UR,
		  DIR_BL	= DIR_DL,
		  DIR_BR	= DIR_DR,
		  DIR_FB	= DIR_UD,
	};
}

//----------------------------------------
// 四つ角
//----------------------------------------
//	II━UT
//	┃　┃
//	HS━TM
//----------------------------------------
namespace opal {
	enum {
		  DIC_X		= 0L,			// 無
		  DIC_I		= 1 << DIX_0,	// 北西 乾 いぬい
		  DIC_U		= 1 << DIX_1,	// 北東	艮 うしとら
		  DIC_T		= 1 << DIX_2,	// 南東 巽 たつみ
		  DIC_H		= 1 << DIX_3,	// 西北 坤 ひつじさる
		  DIC_A		= DIC_I|DIC_U|DIC_T|DIC_H,

		  DIC_IU	= DIC_I|DIC_U,
		  DIC_IH	= DIC_I|DIC_H,
		  DIC_TU	= DIC_T|DIC_U,
		  DIC_TH	= DIC_T|DIC_H,
		  DIC_UI	= DIC_I|DIC_U,
		  DIC_HI	= DIC_I|DIC_H,
		  DIC_UT	= DIC_T|DIC_U,
		  DIC_HT	= DIC_T|DIC_H,
		  DIC_UH	= DIC_U|DIC_H,
		  DIC_HU	= DIC_H|DIC_U,
		  DIC_IT	= DIC_I|DIC_T,
		  DIC_TI	= DIC_T|DIC_I,

		  DIC_IUT	= DIC_I|DIC_U|DIC_T,
		  DIC_IHT	= DIC_I|DIC_H|DIC_T,
		  DIC_UTH	= DIC_U|DIC_T|DIC_H,
		  DIC_UIH	= DIC_U|DIC_I|DIC_H,
		  DIC_UTI	= DIC_U|DIC_T|DIC_I,
		  DIC_HTI	= DIC_H|DIC_T|DIC_I,
		  DIC_THU	= DIC_T|DIC_H|DIC_U,
		  DIC_IHU	= DIC_I|DIC_H|DIC_U,
		  DIC_TIU	= DIC_T|DIC_I|DIC_U,
		  DIC_TIH	= DIC_T|DIC_I|DIC_H,
		  DIC_HUT	= DIC_H|DIC_U|DIC_T,
		  DIC_HUI	= DIC_H|DIC_U|DIC_I,
		  DIC_ITU	= DIC_I|DIC_T|DIC_U,
		  DIC_ITH	= DIC_I|DIC_T|DIC_H,
		  DIC_UHT	= DIC_U|DIC_H|DIC_T,
		  DIC_UHI	= DIC_U|DIC_H|DIC_I,
		  DIC_TUI	= DIC_T|DIC_U|DIC_I,
		  DIC_THI	= DIC_T|DIC_H|DIC_I,
		  DIC_HTU	= DIC_H|DIC_T|DIC_U,
		  DIC_HIU	= DIC_H|DIC_I|DIC_U,
		  DIC_UIT	= DIC_U|DIC_I|DIC_T,
		  DIC_HIT	= DIC_H|DIC_I|DIC_T,
		  DIC_TUH	= DIC_T|DIC_U|DIC_H,
		  DIC_IUH	= DIC_I|DIC_U|DIC_H,

		  DIC_IUTH	= DIC_I|DIC_U|DIC_T|DIC_H,
		  DIC_IUHT	= DIC_I|DIC_U|DIC_H|DIC_T,
		  DIC_IHTU	= DIC_I|DIC_H|DIC_T|DIC_U,
		  DIC_IHUT	= DIC_I|DIC_H|DIC_U|DIC_T,
		  DIC_ITUH	= DIC_I|DIC_T|DIC_U|DIC_H,
		  DIC_ITHU	= DIC_I|DIC_T|DIC_U|DIC_H,
		  DIC_UITH	= DIC_U|DIC_I|DIC_T|DIC_H,
		  DIC_UIHT	= DIC_U|DIC_I|DIC_H|DIC_T,
		  DIC_HITU	= DIC_H|DIC_I|DIC_T|DIC_U,
		  DIC_HIUT	= DIC_H|DIC_I|DIC_U|DIC_T,
		  DIC_TIUH	= DIC_T|DIC_I|DIC_U|DIC_H,
		  DIC_TIHU	= DIC_T|DIC_I|DIC_U|DIC_H,
		  DIC_UTIH	= DIC_U|DIC_T|DIC_I|DIC_H,
		  DIC_UHIT	= DIC_U|DIC_H|DIC_I|DIC_T,
		  DIC_HTIU	= DIC_H|DIC_T|DIC_I|DIC_U,
		  DIC_HUIT	= DIC_H|DIC_U|DIC_I|DIC_T,
		  DIC_TUIH	= DIC_T|DIC_U|DIC_I|DIC_H,
		  DIC_THIU	= DIC_T|DIC_U|DIC_I|DIC_H,
		  DIC_UTHI	= DIC_U|DIC_T|DIC_H|DIC_I,
		  DIC_UHTI	= DIC_U|DIC_H|DIC_T|DIC_I,
		  DIC_HTUI	= DIC_H|DIC_T|DIC_U|DIC_I,
		  DIC_HUTI	= DIC_H|DIC_U|DIC_T|DIC_I,
		  DIC_TUHI	= DIC_T|DIC_U|DIC_H|DIC_I,
		  DIC_THUI  = DIC_T|DIC_U|DIC_H|DIC_I,

		  DIC_N		= DIC_I|DIC_U,
		  DIC_E		= DIC_U|DIC_T,
		  DIC_S		= DIC_T|DIC_H,
		  DIC_W		= DIC_H|DIC_I,

		  DIC_MAX	= DIX_MAX,
	};
}

//----------------------------------------
// ３×３
//----------------------------------------
// ┌─┬─┐
// │１５２│
// ├８０６┤
// │４７３│
// └─┴─┘
//----------------------------------------
namespace opal {
	enum DIM {
		DIM_0,
		DIM_1,
		DIM_2,
		DIM_3,
		DIM_4,
		DIM_5,
		DIM_6,
		DIM_7,
		DIM_8,
		DIM_MAX,

		DIM_TL = DIM_1,
		DIM_TC = DIM_5,
		DIM_TR = DIM_2,

		DIM_ML = DIM_8,
		DIM_MC = DIM_0,
		DIM_MR = DIM_6,

		DIM_BL = DIM_4,
		DIM_BC = DIM_7,
		DIM_BR = DIM_3,
	};

	constexpr auto DIM_Offset( DIM m, float s )->std::pair<float,float>{

		switch ( m ) {
		default:break;
		case DIM_1: return {-s/4,+s/4};
		case DIM_2: return {+s/4,+s/4};
		case DIM_3: return {-s/4,-s/4};
		case DIM_4: return {+s/4,-s/4};
		case DIM_5: return {  0, +s/4};
		case DIM_6: return {+s/4,  0 };
		case DIM_7: return {  0, -s/4};
		case DIM_8: return {-s/4,  0 };
		}

		return {0,0};
	}

	constexpr auto DIM_Offset( float x, float y, float s, float d = 0.f )->DIM{

		if ( s > 0 && !( d < 0 ) ) {
			const auto h = s/2;
			const auto delta = []( float v, float h, float d ){ return FLSS( std::abs( v - h ), d ); };

			if ( delta( x, h, d ) ) {
				if ( delta( y, h, d ) ) { return DIM_0; }
				if ( FREE(  y, 0, h ) ) { return DIM_5;	}
				if ( FREE(  y, h, s ) ) { return DIM_7;	}
			} else if ( 0 < x && x < h ) {
				if ( delta( y, h, d ) ) { return DIM_8; }
				if ( FREE(  y, 0, h ) ) { return DIM_1;	}
				if ( FREE(  y, h, s ) ) { return DIM_4;	}
			} else if ( h < x && x < s ) {
				if ( delta( y, h, d ) ) { return DIM_6; }
				if ( FREE(  y, 0, h ) ) { return DIM_2;	}
				if ( FREE(  y, h, s ) ) { return DIM_3;	}
			}
		}

		return DIM_MAX;
	}
}

//----------------------------------------
// 象限
//----------------------------------------
//----------------------------------------
// Quadrant
//----------------------------------------
//   ＼  ０  ／
//     ＼│／
//   ３─┼─１
//     ／│＼
//   ／  ２  ＼
//----------------------------------------
namespace opal {
	enum DIQ {
		DIQ_0,
		DIQ_1,
		DIQ_2,
		DIQ_3,
		DIQ_MAX,

		DIQ_X	= -1,
		DIQ_N	= DIQ_0,
		DIQ_E	= DIQ_1,
		DIQ_S	= DIQ_2,
		DIQ_W	= DIQ_3,

		DIQ_U	= DIQ_N,
		DIQ_D	= DIQ_S,
		DIQ_L	= DIR_W,
		DIQ_R	= DIR_E,

		DIQ_F	= DIQ_U,
		DIQ_B	= DIQ_D,
	};
}

namespace opal {
	extern int Quadrant( float );
	extern int Quadrant( float, float );
	extern int Quadrant( const VECTOR2* );
	extern int Quadrant( const VECTOR2& );
}

//----------------------------------------
// Octant
//----------------------------------------
// ┌─┬─┬─┐
// │７│０│１│
// ├─┼─┼─┤
// │６│↑│２│
// ├─┼─┼─┤
// │５│４│３│
// └─┴─┴─┘
//----------------------------------------
namespace opal {
	enum DIO {
		DIO_0,
		DIO_1,
		DIO_2,
		DIO_3,
		DIO_4,
		DIO_5,
		DIO_6,
		DIO_7,
		DIO_MAX,

		DIO_X	= -1,
		DIO_N	= DIO_0,
		DIO_NE	= DIO_1,
		DIO_E	= DIO_2,
		DIO_SE	= DIO_3,
		DIO_S	= DIO_4,
		DIO_SW	= DIO_5,
		DIO_W	= DIO_6,
		DIO_NW	= DIO_7,

		DIO_U	= DIO_N,
		DIO_D	= DIO_S,
		DIO_L	= DIR_W,
		DIO_R	= DIR_E,
		DIO_UL	= DIO_NW,
		DIO_UR	= DIO_NE,
		DIO_DL	= DIO_SW,
		DIO_DR	= DIO_SE,

		DIO_F	= DIO_U,
		DIO_B	= DIO_D,
		DIO_FL	= DIO_UL,
		DIO_FR	= DIO_UR,
		DIO_BL	= DIO_DL,
		DIO_BR	= DIO_DR,
	};
}

namespace opal {
	extern int Octant( float );
	extern int Octant( float, float );
	extern int Octant( const VECTOR2* );
	extern int Octant( const VECTOR2& );
}

//----------------------------------------
// DIX/DIO 正規化
//----------------------------------------
namespace opal {
	extern UINT NormalDix( int );
	extern UINT NormalDio( int );

	extern UINT CounterDix( int );
	extern UINT CounterDio( int );
}

//----------------------------------------
// DIR/DIX/DIO -> RAD/DEG
//----------------------------------------
namespace opal {
	constexpr float DirRadian( int d ){

		switch ( d ) {
		case DIR_N : return RADIAN(   0.f );
		case DIR_NE: return RADIAN(  45.f );
		case DIR_E : return RADIAN(  90.f );
		case DIR_SE: return RADIAN( 135.f );
		case DIR_S : return RADIAN( 180.f );
		case DIR_SW: return RADIAN( 225.f );
		case DIR_W : return RADIAN( 270.f );
		case DIR_NW: return RADIAN( 315.f );
		}
		return -1;
	}

	constexpr float DirDegree( int d ){

		switch ( d ) {
		case DIR_N : return    0.f;
		case DIR_NE: return   45.f;
		case DIR_E : return   90.f;
		case DIR_SE: return  135.f;
		case DIR_S : return  180.f;
		case DIR_SW: return  225.f;
		case DIR_W : return  270.f;
		case DIR_NW: return  315.f;
		}
		return -1;
	}

	constexpr float DixRadian( int d ){

		switch ( d ) {
		case DIX_N: return RADIAN(   0.f );
		case DIX_E: return RADIAN(  90.f );
		case DIX_S: return RADIAN( 180.f );
		case DIX_W: return RADIAN( 270.f );
		}
		return DIX_X;

	}

	constexpr float DixDegree( int d ){

		switch ( d ) {
		case DIX_N: return    0.f;
		case DIX_E: return   90.f;
		case DIX_S: return  180.f;
		case DIX_W: return  270.f;
		}
		return DIX_X;
	}

	constexpr float DioRadian( int d ){

		switch ( d ) {
		case DIO_N : return RADIAN(   0.f );
		case DIO_NE: return RADIAN(  45.f );
		case DIO_E : return RADIAN(  90.f );
		case DIO_SE: return RADIAN( 135.f );
		case DIO_S : return RADIAN( 180.f );
		case DIO_SW: return RADIAN( 225.f );
		case DIO_W : return RADIAN( 270.f );
		case DIO_NW: return RADIAN( 315.f );
		}
		return DIO_X;

	}

	constexpr float DioDegree( int d ){

		switch ( d ) {
		case DIO_N : return    0.f;
		case DIO_NE: return   45.f;
		case DIO_E : return   90.f;
		case DIO_SE: return  135.f;
		case DIO_S : return  180.f;
		case DIO_SW: return  225.f;
		case DIO_W : return  270.f;
		case DIO_NW: return  315.f;
		}
		return DIO_X;
	}
}

//----------------------------------------
// DIR -> DIX/DIO
//----------------------------------------
namespace opal {
	constexpr int DirToDix( int d ){

		switch ( d ) {
		case DIR_N:		return DIX_N;
		case DIR_E:		return DIX_E;
		case DIR_S:		return DIX_S;
		case DIR_W:		return DIX_W;
		}

		return DIX_X;
	}

	constexpr int DirToDio( int d ){

		switch ( d ) {
		case DIR_N :	return DIO_N;
		case DIR_NE:	return DIO_NE;
		case DIR_E :	return DIO_E;
		case DIR_SE:	return DIO_SE;
		case DIR_S :	return DIO_S;
		case DIR_SW:	return DIO_SW;
		case DIR_W :	return DIO_W;
		case DIR_NW:	return DIO_NW;
		}

		return DIO_X;
	}
}

//----------------------------------------
// 方向転換
//----------------------------------------
namespace opal {
	constexpr UINT CounterDir( UINT d ) {

		switch ( d ) {
		case DIR_X				: return DIR_A;
		case DIR_N				: return DIR_S;
		case DIR_E				: return DIR_W;
		case DIR_S				: return DIR_N;
		case DIR_W				: return DIR_E;
		case DIR_N|DIR_E		: return DIR_S|DIR_W;
		case DIR_N|DIR_W		: return DIR_S|DIR_E;
		case DIR_S|DIR_E		: return DIR_N|DIR_W;
		case DIR_S|DIR_W		: return DIR_N|DIR_E;
		case DIR_N|DIR_S		: return DIR_E|DIR_W;
		case DIR_E|DIR_W		: return DIR_N|DIR_S;
		case DIR_N|DIR_E|DIR_S	: return DIR_W;
		case DIR_N|DIR_E|DIR_W	: return DIR_S;
		case DIR_N|DIR_S|DIR_W	: return DIR_E;
		case DIR_E|DIR_S|DIR_W	: return DIR_N;
		}
		return DIR_X;
	}
}

namespace opal {
	constexpr UINT TurnDir( UINT d, int r ){

		switch ( r%4 ) {
		case 0:
			break;

		case +1:
		case -3:
			switch ( d ) {
			case DIR_U: return DIR_R;
			case DIR_D: return DIR_L;
			case DIR_L: return DIR_U;
			case DIR_R: return DIR_D;
			}
			break;

		case +2:
		case -2:
			switch ( d ) {
			case DIR_U: return DIR_D;
			case DIR_D: return DIR_U;
			case DIR_L: return DIR_R;
			case DIR_R: return DIR_L;
			}
			break;

		case +3:
		case -1:
			switch ( d ) {
			case DIR_U: return DIR_L;
			case DIR_D: return DIR_R;
			case DIR_L: return DIR_D;
			case DIR_R: return DIR_U;
			}
			break;

		}
		return d;
	}
}

namespace opal {
	constexpr DIX DixSide( DIX d, SIDE s ){

		if ( s != SIDE_X ) {
			switch ( s ) {
			default:break;
			case SIDE_L:
				switch ( d ) {
				default:break;
				case DIX_N: return DIX_W;
				case DIX_E: return DIX_N;
				case DIX_S: return DIX_E;
				case DIX_W: return DIX_S;
				}
				break;

			case SIDE_R:
				switch ( d ) {
				default:break;
				case DIX_N: return DIX_E;
				case DIX_E: return DIX_S;
				case DIX_S: return DIX_W;
				case DIX_W: return DIX_N;
				}
				break;
			}
		}

		return DIX_MAX;
	}
}

//----------------------------------------
// Radian ⇔ DIX
//----------------------------------------
namespace opal {
#ifdef _MSC_VER
	extern auto Dix2Rad( int d )->float;
	extern auto Rad2Dix( float r )->int;
	extern auto Rad2Dio( float r )->int;
#else
	constexpr auto Dix2Rad( int d )->float{ return ( ( d < 0 ? DIX_MAX    : 0 ) + d%DIX_MAX                  )*RAD( 90.f );	}
	constexpr auto Rad2Dix( float r )->int{ return ( ( r < 0 ? RAD( 360 ) : 0 ) + std::fmod( r, RAD( 360 ) ) )/RAD( 90.f );	}
	constexpr auto Rad2Dio( float r )->int{ return ( ( r < 0 ? RAD( 360 ) : 0 ) + std::fmod( r, RAD( 360 ) ) )/RAD( 45.f );	}
#endif
}

// End Of File
