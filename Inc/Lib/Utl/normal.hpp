#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// 正規化
//========================================
//----------------------------------------
// 正規化
//----------------------------------------
namespace opal {
	extern float Normal( float v, float n );

	extern float NormalRadian360( float r );
	extern float NormalDegree360( float d );
	extern float NormalRadian180( float r );
	extern float NormalDegree180( float d );

	extern bool  NormalSpeed( UINT d, float& x, float& y );
}

//----------------------------------------
// 2次元座標と1次元配列
//----------------------------------------
namespace opal {
	extern UINT Point2Index( const UINT x, const UINT y, const UINT w, const UINT h );
	extern UINT Point2Index( const UINT x, const UINT y, const UINT w               );
}

// End Of File
