#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// スプライン
//========================================
namespace opal {
	template<typename TYPE>class SPLINE {
	public:
		explicit SPLINE();
		virtual ~SPLINE();

	private:
		size_t				s;
		std::vector<TYPE>	a;
		std::vector<TYPE>	b;
		std::vector<TYPE>	c;
		std::vector<TYPE>	d;

	public:
		virtual void Coefficient( const std::vector<TYPE>& );
		virtual TYPE Interpolate( float ) const;
	};
}

// End Of File
