#pragma once
//========================================
// OPAL Game Library
//========================================
#include "lib.hpp"
#include "dbg.hpp"
#include "etc.hpp"

//========================================
// ���
//========================================
namespace opal {
	namespace TWEEN {
		extern float bezier( float ax, float ay, float bx, float by, float vx );
		extern float lerp( float a, float b, float t );
	}
}

// End Of File
