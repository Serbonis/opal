#pragma once
//========================================
// OPAL Game Library
//========================================

//========================================
// SAL(Standard Annotation Language)�̖�����
//========================================
#ifndef _MSC_VER
#define __in
#define __out
#define __inout
#define __in_opt
#define __out_opt
#define __inout_opt
#define __deref_in
#define __deref_out
#define __deref_in_opt
#define __deref_out_opt
#define __deref_inout_opt
//#define __in_bcount(a)
//#define __out_bcount(a)
//#define __in_bcount_opt(a)
//#define __out_bcount_opt(a)
//#define __in_bcount_part(a,b)
//#define __out_bcount_part(a,b)
//#define __in_ecount(a)
//#define __out_ecount(a)
//#define __in_ecount_opt(a)
//#define __out_ecount_opt(a)
//#define __inout_ecount_full(a)
//#define __in_ecount_part(a,b)
//#define __out_ecount_part(a,b)
#define __field_ecount_opt(a)
#define __deref_in_range(a,b)
#define __deref_out_range(a,b)
#define __range(a,b)
#define __success(a)
#define __control_entrypoint(a)
#define __in_range(a)
#define __out_range(a,b)
#define __reserved
#define _In_bytecount_x_(a)
#define _Out_bytecap_x_(a)
#define _In_count_c_(a)
#define _Out_cap_c_(a)
#define __in_opt

#ifdef SAL_INLINE
#define __inline	inline
#else
#define __inline
#endif

#endif

// End Of File
